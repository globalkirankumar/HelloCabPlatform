-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 16, 2017 at 07:35 AM
-- Server version: 10.1.24-MariaDB
-- PHP Version: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hellocab_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `authkeydetails`
--

CREATE TABLE `authkeydetails` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `userId` int(11) UNSIGNED NOT NULL,
  `authKey` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `level` int(2) NOT NULL,
  `ignoreLimits` tinyint(1) NOT NULL DEFAULT '0',
  `isPrivateKey` tinyint(1) NOT NULL DEFAULT '0',
  `ipAddress` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `userType` enum('N','P','M','D','E','O') COLLATE utf8_unicode_ci NOT NULL COMMENT '''N''=None,''P''=passenger,''D''=Driver,''M''=Merchant,''E''=Partner/Franchise,''O''=Others',
  `createdDatetime` datetime NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `timestamp` varchar(150) NOT NULL,
  `data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `isoCode` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `telephoneCode` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `currencyCode` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `currencySymbol` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `timeZone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `createdBy` int(11) UNSIGNED NOT NULL,
  `updatedBy` int(11) UNSIGNED NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `name`, `isoCode`, `telephoneCode`, `currencyCode`, `currencySymbol`, `timeZone`, `status`, `createdBy`, `updatedBy`, `createdDatetime`, `updatedDatetime`) VALUES
(1, 'Mynamar', 'MYN', '+95', 'MMK', 'K', 'mynamar', 1, 1, 0, '2017-07-26 00:00:00', '2017-08-11 23:48:43');

-- --------------------------------------------------------

--
-- Table structure for table `dataattributes`
--

CREATE TABLE `dataattributes` (
  `id` int(11) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tableName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `internalName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sequenceOrder` int(4) NOT NULL,
  `isVisible` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dataattributes`
--

INSERT INTO `dataattributes` (`id`, `description`, `tableName`, `internalName`, `sequenceOrder`, `isVisible`) VALUES
(1, 'Super Admin', 'ROLE_TYPE', 'super admin', 1, 0),
(2, 'Admin', 'ROLE_TYPE', 'admin', 2, 1),
(3, 'Manager', 'ROLE_TYPE', 'manager', 3, 1),
(4, 'Supervisor', 'ROLE_TYPE', 'supervisor', 4, 1),
(5, 'Staff', 'ROLE_TYPE', 'staff', 5, 1),
(6, 'Accountant', 'ROLE_TYPE', 'accountant', 6, 1),
(7, 'Partner', 'ROLE_TYPE', 'partner', 7, 1),
(8, 'Female', 'GENDER_TYPE', 'female', 1, 1),
(9, 'Male', 'GENDER_TYPE', 'male', 2, 1),
(10, 'Entity', 'ENTITY_TYPE', 'entity', 2, 1),
(11, 'Partner', 'ENTITY_TYPE', 'partner', 3, 1),
(12, 'No Device', 'DEVICE_TYPE', 'no device', 1, 1),
(13, 'Android', 'DEVICE_TYPE', 'android', 2, 1),
(14, 'Iphone', 'DEVICE_TYPE', 'iphone', 3, 1),
(15, 'Windows', 'DEVICE_TYPE', 'windows', 4, 0),
(16, 'Others', 'DEVICE_TYPE', 'others', 5, 0),
(17, 'None', 'SIGNUP_TYPE', 'none', 1, 1),
(18, 'Website', 'SIGNUP_TYPE', 'website', 2, 1),
(19, 'Mobile', 'SIGNUP_TYPE', 'mobile', 3, 1),
(20, 'Backend', 'SIGNUP_TYPE', 'backend', 4, 1),
(21, 'None', 'REGISTER_TYPE', 'none', 1, 1),
(22, 'Manual', 'REGISTER_TYPE', 'manual', 2, 1),
(23, 'Facebook', 'REGISTER_TYPE', 'facebook', 3, 1),
(24, 'Google', 'REGISTER_TYPE', 'google', 4, 1),
(25, 'Others', 'REGISTER_TYPE', 'others', 5, 1),
(26, 'None', 'LICENSE_TYPE', 'none', 1, 1),
(27, 'Non Transport', 'LICENSE_TYPE', 'non transport', 2, 1),
(28, 'Transport', 'LICENSE_TYPE', 'transport', 3, 1),
(29, 'None', 'DRIVER_ACCEPTED_STATUS', 'none', 1, 1),
(30, 'Accepted', 'DRIVER_ACCEPTED_STATUS', 'accepted', 2, 1),
(31, 'Rejected', 'DRIVER_ACCEPTED_STATUS', 'rejected', 3, 1),
(32, 'Time Out', 'DRIVER_ACCEPTED_STATUS', 'time out', 4, 1),
(33, 'Available Trip', 'TAXI_REQUEST_STATUS', 'available trip', 1, 1),
(34, 'Driver Not Found', 'TAXI_REQUEST_STATUS', 'driver not found', 2, 1),
(35, 'Driver Accepted', 'TAXI_REQUEST_STATUS', 'driver accepted', 3, 1),
(36, 'Driver Rejected', 'TAXI_REQUEST_STATUS', 'driver rejected', 4, 1),
(37, 'Passenger Cancelled', 'TAXI_REQUEST_STATUS', 'passenger cancelled', 5, 1),
(38, 'Completed Trip', 'TAXI_REQUEST_STATUS', 'completed trip', 6, 1),
(39, 'Not Available', 'TAXI_AVAILABLE_STATUS', 'not available', 1, 1),
(40, 'Free', 'TAXI_AVAILABLE_STATUS', 'free', 2, 1),
(41, 'Busy', 'TAXI_AVAILABLE_STATUS', 'busy', 3, 1),
(42, 'Shift In', 'DRIVER_SHIFT_STATUS', 'shift in', 1, 1),
(43, 'Shift Out', 'DRIVER_SHIFT_STATUS', 'shift out', 2, 1),
(44, 'Ride Now', 'TRIP_TYPE', 'ride now', 1, 1),
(45, 'Ride Later', 'TRIP_TYPE', 'ride later', 2, 1),
(46, 'None', 'TRIP_STATUS', 'none', 1, 0),
(47, 'Booked', 'TRIP_STATUS', 'booked', 2, 1),
(48, 'Driver Initiated', 'TRIP_STATUS', 'driver initiated', 3, 1),
(49, 'Driver Accepted', 'TRIP_STATUS', 'driver accepted', 4, 1),
(50, 'Driver Arrived', 'TRIP_STATUS', 'driver arrived', 5, 1),
(51, 'In Progress', 'TRIP_STATUS', 'in progress', 6, 1),
(52, 'Waiting For Payment', 'TRIP_STATUS', 'waiting for payment', 7, 0),
(53, 'Trip Completed', 'TRIP_STATUS', 'trip completed', 8, 1),
(54, 'Cancelled By Passenger', 'TRIP_STATUS', 'cancelled by passenger', 9, 1),
(55, 'Cancelled By Driver', 'TRIP_STATUS', 'cancelled by driver', 10, 1),
(56, 'Driver Not Found', 'TRIP_STATUS', 'driver not found', 11, 1),
(57, 'No notification', 'TRIP_STATUS', 'no notification', 12, 0),
(58, 'None', 'TRANSACTION_MODE', 'none', 1, 0),
(59, 'Credit', 'TRANSACTION_MODE', 'credit', 2, 1),
(60, 'Debit', 'TRANSACTION_MODE', 'debit', 3, 1),
(61, 'None', 'TRANSACTION_TYPE', 'none', 1, 0),
(62, 'Referral', 'TRANSACTION_TYPE', 'referral', 2, 1),
(63, 'Easy Points', 'TRANSACTION_TYPE', 'easy points', 3, 1),
(64, 'Trip', 'TRANSACTION_TYPE', 'trip', 4, 1),
(65, 'Promocode', 'TRANSACTION_TYPE', 'promocode', 5, 1),
(66, 'Manual', 'TRANSACTION_TYPE', 'manual', 7, 1),
(67, 'Others', 'TRANSACTION_TYPE', 'others', 8, 1),
(68, 'None', 'PAYMENT_METHOD', 'none', 1, 1),
(69, 'Direct', 'PAYMENT_METHOD', 'direct', 2, 1),
(70, 'Indirect', 'PAYMENT_METHOD', 'indirect', 3, 1),
(71, 'None', 'PAYMENT_TYPE', 'none', 1, 0),
(72, 'Amount', 'PAYMENT_TYPE', 'amount', 2, 1),
(73, 'Percentage', 'PAYMENT_TYPE', 'percentage', 3, 1),
(74, 'None', 'PAYMENT_MODE', 'none', 1, 0),
(75, 'Cash', 'PAYMENT_MODE', 'cash', 2, 1),
(76, 'Wallet', 'PAYMENT_MODE', 'wallet', 3, 1),
(77, 'AGD', 'PAYMENT_MODE', 'agd', 4, 0),
(78, 'Invoice', 'PAYMENT_MODE', 'invoice', 5, 0),
(79, 'None', 'TAXI_TYPE', 'none', 1, 0),
(80, 'Standard', 'TAXI_TYPE', 'standard', 2, 1),
(81, 'Hatchback', 'TAXI_TYPE', 'hatchback', 3, 0),
(82, 'Sedan', 'TAXI_TYPE', 'sedan', 4, 0),
(83, 'MUV/MPV', 'TAXI_TYPE', 'MUV/MPV', 5, 0),
(84, 'SUV ', 'TAXI_TYPE', 'SUV ', 6, 0),
(85, 'Luxury', 'TAXI_TYPE', 'luxury', 7, 0),
(86, 'Others', 'TAXI_TYPE', 'others', 8, 0),
(87, 'None', 'TRANSMISSION_TYPE', 'none', 1, 1),
(88, 'Manual', 'TRANSMISSION_TYPE', 'manual', 2, 1),
(89, 'Automatic', 'TRANSMISSION_TYPE', 'automatic', 3, 1),
(90, 'None', 'DEVICE_STATUS', 'none', 0, 0),
(91, 'Open', 'DEVICE_STATUS', 'open', 2, 1),
(92, 'On-Service', 'DEVICE_STATUS', 'on-service', 3, 1),
(93, 'On-Repair', 'DEVICE_STATUS', 'on-repair', 4, 1),
(94, 'Damaged', 'DEVICE_STATUS', 'damaged', 5, 1),
(95, 'Open', 'EMERGENCY_STATUS', 'open', 1, 1),
(96, 'In Action', 'EMERGENCY_STATUS', 'in action', 2, 1),
(97, 'Pending', 'EMERGENCY_STATUS', 'pending', 3, 1),
(98, 'Wrong Request', 'EMERGENCY_STATUS', 'wrong request', 4, 1),
(99, 'Closed', 'EMERGENCY_STATUS', 'closed', 5, 1),
(100, 'Less than 1 Year', 'EXPERIENCE', 'less than 1 Year', 1, 1),
(101, '1-3 Years', 'EXPERIENCE', '1-3 Years', 2, 1),
(102, '3-5 Years', 'EXPERIENCE', '3-5 Years', 3, 1),
(103, '5-10 Years', 'EXPERIENCE', '5-10 Years', 4, 1),
(104, 'Above 10 Years', 'EXPERIENCE', 'Above 10 Years', 5, 1),
(105, 'None', 'SLAB_TYPE', 'none', 1, 1),
(106, 'Distance', 'SLAB_TYPE', 'distance', 2, 1),
(107, 'Time', 'SLAB_TYPE', 'time', 3, 1),
(108, 'Surge', 'SLAB_TYPE', 'surge', 4, 1),
(109, 'Queuing', 'TRIP_STATUS', 'queuing', 12, 0),
(110, 'Trip Commission', 'TRANSACTION_TYPE', 'trip commission', 9, 1),
(111, 'Trip Wallet', 'TRANSACTION_TYPE', 'trip wallet', 10, 1),
(112, 'Bank', 'TRANSACTION_TYPE', 'bank', 6, 1),
(113, 'Round robin', 'DISPATCH_TYPE', 'round robin', 1, 1),
(114, 'Fastest finger first', 'DISPATCH_TYPE', 'fastest finger first', 2, 0),
(115, 'Driver rejected trip', 'TRIP_STATUS', 'driver rejected trip', 13, 1),
(116, 'Trip Reject Penalty', 'TRANSACTION_TYPE', 'trip reject penalty', 11, 1),
(117, '2C2P', 'TRANSACTION_TYPE', '2C2P', 12, 1),
(118, 'Wallet Account', 'TRANSACTION_FROM', 'wallet account', 1, 1),
(119, 'Credit Account', 'TRANSACTION_FROM', 'credit account', 2, 1),
(120, 'Booking Charge', 'TRANSACTION_TYPE', 'booking charge', 13, 1),
(121, 'Passenger Penalty Charge', 'TRANSACTION_TYPE', 'passenger penalty charge', 14, 1),
(122, 'Admin Commission', 'TRANSACTION_TYPE', 'admin commission', 15, 1),
(123, 'Cash', 'TRANSACTION_FROM', 'cash', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `driver`
--

CREATE TABLE `driver` (
  `id` int(11) UNSIGNED NOT NULL,
  `driverCode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `firstName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `otp` int(6) NOT NULL,
  `profileImage` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `licenseNo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `licenseExpiryDate` date NOT NULL,
  `licenseImage` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `licenseType` int(11) UNSIGNED NOT NULL,
  `experience` int(11) UNSIGNED NOT NULL,
  `languageKnown` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `isVerified` tinyint(1) NOT NULL DEFAULT '0',
  `policeRecommendationImage` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deviceId` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deviceType` int(11) UNSIGNED NOT NULL,
  `signupFrom` int(11) UNSIGNED NOT NULL,
  `registerType` int(11) UNSIGNED NOT NULL,
  `driverWallet` decimal(10,2) NOT NULL DEFAULT '0.00',
  `driverCredit` decimal(10,2) NOT NULL DEFAULT '0.00',
  `loginStatus` tinyint(1) NOT NULL DEFAULT '0',
  `lastLoggedIn` datetime NOT NULL,
  `zoneId` int(11) UNSIGNED NOT NULL,
  `entityId` int(11) UNSIGNED NOT NULL,
  `gcmId` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `appVersion` float(2,1) NOT NULL DEFAULT '0.0',
  `currentLatitude` float(10,6) NOT NULL DEFAULT '0.000000',
  `currentLongitude` float(10,6) NOT NULL DEFAULT '0.000000',
  `currentBearing` float(6,3) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `isNotified` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) UNSIGNED NOT NULL,
  `updatedBy` int(11) UNSIGNED NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `driverdispatchdetails`
--

CREATE TABLE `driverdispatchdetails` (
  `driverId` int(11) NOT NULL,
  `tripId` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `avgRating` float(3,1) NOT NULL DEFAULT '0.0',
  `walletBalance` decimal(10,2) NOT NULL DEFAULT '0.00',
  `avgTripDistance` float(6,2) NOT NULL DEFAULT '0.00',
  `driverLatitude` float(10,6) NOT NULL DEFAULT '0.000000',
  `driverLongitude` float(10,6) NOT NULL DEFAULT '0.000000',
  `totalCompletedTrip` int(11) NOT NULL DEFAULT '0',
  `weekCompletedTrip` int(11) NOT NULL DEFAULT '0',
  `totalRejectedTrip` int(11) NOT NULL DEFAULT '0',
  `weekRejectedTrip` int(11) NOT NULL DEFAULT '0',
  `consecutiveRejectCount` int(11) NOT NULL DEFAULT '0',
  `availabilityStatus` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `driverpersonaldetails`
--

CREATE TABLE `driverpersonaldetails` (
  `id` int(11) UNSIGNED NOT NULL,
  `driverId` int(11) UNSIGNED NOT NULL,
  `gender` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `dob` date NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `qualification` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nationRegistrationNo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nationRegistrationImage` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `familyMemberListImage` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `homeRecommendationImage` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `securityDeposit` decimal(10,2) NOT NULL DEFAULT '0.00',
  `contractPaperImage` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `addressLatitude` float(10,6) NOT NULL DEFAULT '0.000000',
  `addressLongitude` float(10,6) NOT NULL DEFAULT '0.000000',
  `groupType` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `behaviourType` text COLLATE utf8_unicode_ci NOT NULL,
  `bankName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bankBranch` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bankAccountNo` bigint(20) NOT NULL,
  `createdBy` int(11) UNSIGNED NOT NULL,
  `updatedBy` int(11) UNSIGNED NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `drivershifthistory`
--

CREATE TABLE `drivershifthistory` (
  `id` int(11) UNSIGNED NOT NULL,
  `driverId` int(11) UNSIGNED NOT NULL,
  `inLatitude` float(10,6) NOT NULL DEFAULT '0.000000',
  `inLongitude` float(10,6) NOT NULL DEFAULT '0.000000',
  `outLatitude` float(10,6) NOT NULL DEFAULT '0.000000',
  `outLongitude` float(10,6) NOT NULL DEFAULT '0.000000',
  `availabilityStatus` int(11) UNSIGNED NOT NULL,
  `shiftStatus` int(11) UNSIGNED NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `drivertaximapping`
--

CREATE TABLE `drivertaximapping` (
  `id` int(11) UNSIGNED NOT NULL,
  `driverId` int(11) UNSIGNED NOT NULL,
  `taxiId` int(11) UNSIGNED NOT NULL,
  `taxiOwnerId` int(11) UNSIGNED NOT NULL,
  `taxiDeviceId` int(11) UNSIGNED NOT NULL,
  `createdBy` int(11) UNSIGNED NOT NULL,
  `updatedBy` int(11) UNSIGNED NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `drivertransactiondetails`
--

CREATE TABLE `drivertransactiondetails` (
  `id` int(11) UNSIGNED NOT NULL,
  `driverId` int(11) UNSIGNED NOT NULL,
  `referralId` int(11) UNSIGNED NOT NULL,
  `tripId` bigint(20) UNSIGNED NOT NULL,
  `transactionAmount` decimal(10,2) NOT NULL,
  `previousAmount` decimal(10,2) NOT NULL,
  `currentAmount` decimal(10,2) NOT NULL,
  `transactionStatus` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `transactionType` int(11) UNSIGNED NOT NULL,
  `transactionMode` int(11) UNSIGNED NOT NULL,
  `transactionFrom` int(11) UNSIGNED NOT NULL,
  `transactionId` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comments` text COLLATE utf8_unicode_ci NOT NULL,
  `createdBy` int(11) UNSIGNED NOT NULL,
  `updatedBy` int(11) UNSIGNED NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `emergencyrequestdetails`
--

CREATE TABLE `emergencyrequestdetails` (
  `id` int(11) UNSIGNED NOT NULL,
  `userId` int(11) UNSIGNED NOT NULL,
  `userType` enum('N','P','D','O') COLLATE utf8_unicode_ci NOT NULL COMMENT '''N''=None,''P''=passenger,''D''=Driver,''O''=Others',
  `location` text COLLATE utf8_unicode_ci NOT NULL,
  `latitude` float(10,6) NOT NULL DEFAULT '0.000000',
  `longitude` float(10,6) NOT NULL DEFAULT '0.000000',
  `emergencyStatus` int(11) UNSIGNED NOT NULL,
  `comments` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `createdBy` int(11) UNSIGNED NOT NULL,
  `updatedBy` int(11) UNSIGNED NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `entityconfigdetails`
--

CREATE TABLE `entityconfigdetails` (
  `id` int(11) UNSIGNED NOT NULL,
  `entityId` int(11) UNSIGNED NOT NULL,
  `tax` decimal(4,2) NOT NULL,
  `adminCommission` decimal(10,2) NOT NULL DEFAULT '0.00',
  `passengerReferralDiscount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `driverReferralDiscount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `passengerRejectionCharge` decimal(10,2) NOT NULL DEFAULT '0.00',
  `driverMinWallet` decimal(10,2) NOT NULL DEFAULT '0.00',
  `driverMaxWallet` decimal(10,2) NOT NULL DEFAULT '0.00',
  `driverSecurityAmount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `adminBookingCharge` decimal(10,2) NOT NULL DEFAULT '0.00',
  `drtPromoAmount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `drtPromoDaysLimit` int(2) NOT NULL DEFAULT '0',
  `drtFirstLimit` int(2) NOT NULL DEFAULT '0',
  `drtFirstlLimitCharge` decimal(10,2) NOT NULL DEFAULT '0.00',
  `drtSecondLimit` int(2) NOT NULL DEFAULT '0',
  `sendSms` tinyint(1) NOT NULL,
  `sendEmail` tinyint(1) NOT NULL,
  `passengerAndroidAppUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `passengerIphoneAppUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `passengerAndroidAppVer` float(4,2) NOT NULL DEFAULT '1.00',
  `passengerIphoneAppVer` float(4,2) NOT NULL DEFAULT '1.00',
  `driverAndroidAppUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `driverIphoneAppUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `driverAndroidAppVer` float(4,2) NOT NULL DEFAULT '1.00',
  `driverIphoneAppVer` float(4,2) NOT NULL DEFAULT '1.00',
  `createdBy` int(11) UNSIGNED NOT NULL,
  `updatedBy` int(11) UNSIGNED NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `entityconfigdetails`
--

INSERT INTO `entityconfigdetails` (`id`, `entityId`, `tax`, `adminCommission`, `passengerReferralDiscount`, `driverReferralDiscount`, `passengerRejectionCharge`, `driverMinWallet`, `driverMaxWallet`, `driverSecurityAmount`, `adminBookingCharge`, `drtPromoAmount`, `drtPromoDaysLimit`, `drtFirstLimit`, `drtFirstlLimitCharge`, `drtSecondLimit`, `sendSms`, `sendEmail`, `passengerAndroidAppUrl`, `passengerIphoneAppUrl`, `passengerAndroidAppVer`, `passengerIphoneAppVer`, `driverAndroidAppUrl`, `driverIphoneAppUrl`, `driverAndroidAppVer`, `driverIphoneAppVer`, `createdBy`, `updatedBy`, `createdDatetime`, `updatedDatetime`) VALUES
(1, 1, '0.00', '5.00', '100.00', '100.00', '1000.00', '-5000.00', '5000.00', '50000.00', '300.00', '1000.00', 2, 5, '10.00', 7, 1, 1, 'asdfdsdf', '', 1.00, 1.00, 'asds', '', 1.00, 1.00, 1, 11, '2017-07-26 00:00:00', '2017-10-31 14:23:55');

-- --------------------------------------------------------

--
-- Table structure for table `entitydetails`
--

CREATE TABLE `entitydetails` (
  `id` int(11) UNSIGNED NOT NULL,
  `entityCode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entityPrefix` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `metaKeyword` text COLLATE utf8_unicode_ci NOT NULL,
  `apisSecretKey` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tagLine` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `websiteUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `emergencyEmail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `emergencyPhone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `tollFreeNo` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `entityType` int(11) UNSIGNED NOT NULL,
  `copyRights` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `emailLogo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `favicon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `countryId` int(11) UNSIGNED NOT NULL,
  `zoneId` int(11) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) UNSIGNED NOT NULL,
  `updatedBy` int(11) UNSIGNED NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `entitydetails`
--

INSERT INTO `entitydetails` (`id`, `entityCode`, `entityPrefix`, `name`, `description`, `metaKeyword`, `apisSecretKey`, `tagLine`, `email`, `phone`, `websiteUrl`, `address`, `emergencyEmail`, `emergencyPhone`, `tollFreeNo`, `entityType`, `copyRights`, `logo`, `emailLogo`, `favicon`, `countryId`, `zoneId`, `status`, `isDeleted`, `createdBy`, `updatedBy`, `createdDatetime`, `updatedDatetime`) VALUES
(1, 'ENT0001', 'HC', 'Hellocab', 'Hellocab', 'Hellocab Taxi Service', '', 'Taxi on demand', 'info@hellocab.com', '9594144417', 'hellocab.com', 'Mynamar', 'support@hellocab.com', '9800180088', '9800180088', 10, 'hellocabs dragon logistics', 'LOGO-1-0.png', 'EMAIL-LOGO-1-0.png', 'Favicon-1-0.png', 1, 1, 1, 0, 1, 11, '2017-07-26 00:00:00', '2017-10-31 14:23:54');

-- --------------------------------------------------------

--
-- Table structure for table `locks`
--

CREATE TABLE `locks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `primaryKey` int(11) NOT NULL,
  `tableName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createdBy` int(11) NOT NULL,
  `updatedBy` int(11) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `locks`
--

INSERT INTO `locks` (`id`, `primaryKey`, `tableName`, `createdBy`, `updatedBy`, `createdDatetime`, `updatedDatetime`) VALUES
(27, 16880, 'PASSENGER', 11, 11, '2017-11-10 13:03:30', '2017-11-10 13:03:30');

-- --------------------------------------------------------

--
-- Table structure for table `menucontentdetails`
--

CREATE TABLE `menucontentdetails` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `metaKeyword` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `linkName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `sequenceOrder` int(3) NOT NULL,
  `menuId` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `createdBy` int(11) NOT NULL,
  `updatedBy` int(11) NOT NULL,
  `createdDate` datetime NOT NULL,
  `updatedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menucontentdetails`
--

INSERT INTO `menucontentdetails` (`id`, `name`, `title`, `metaKeyword`, `description`, `linkName`, `content`, `sequenceOrder`, `menuId`, `status`, `createdBy`, `updatedBy`, `createdDate`, `updatedDate`) VALUES
(1, 'termsconditions', 'Terms and Conditions', 'Terms and Conditions', 'Terms and Conditions', 'termsconditions', '<div class=\"key_block_outer1 clearfix\">\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				Terms and Conditions (\"Terms\") \r\n			</h2>\r\n			<p>\r\n			Please read these Terms and Conditions (\"Terms\", \"Terms and Conditions\") carefully before using the <a href=\"http://zuver.in/\" title=\"http://zuver.in/\">http://zuver.in/</a> website (the \"Service\") operated by Jobbie Services India Private Limited (\"us\", \"we\", or \"our\").\r\n			</p>\r\n			<p>\r\n			Your access to and use of the Service is conditioned on your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use the Service.\r\n			</p>\r\n			<p>\r\n			By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you may not access the Service.\r\n			</p>\r\n			<p>\r\n			The conditions and terms declared here-in (mutually, the \"Agreement\") consists of a lawful contract between you and also Jobbie Services India Private Limited, a Firm organized personally (the \"Firm\")   with a view to utilize the program (outlined below) as well as the related software (mentioned below) you have to accept the terms which has been set out below. By utilizing or acquiring any facilities provided to you by organization (combined, the \"service\") and also installing, preserving or accessing any related program provided by the corporation which function will be to allow you to apply the services (together, \"the Program\"), you hereby explicitly admit and also accept to be legally bound by the conditions and terms of the contract, and also virtually any upcoming amendments and even inclusions to this binding agreement as publicized periodically at <a href=\"http://zuver.in/\" title=\"http://zuver.in/\">http://zuver.in/</a> or via the support.\r\n			</p>\r\n			<p>\r\n			THE FIRM RESERVES ALL RIGHTS TO IMPROVE OR CHANGE THE TERMS OF THIS AGREEMENT OR SOME REGULATIONS CONCERNING THE SERVICES OR SOFTWARE ANY TIME, EFFECTUAL UPON PUBLISHING OF AN UPGRADED VERSION OF THIS CONTRACT ON THE SUPPORT OR EVEN SOFTWARE. YOU WILL BE ACCOUNTABLE FOR ROUTINELY EXAMINING THIS AGREEMENT. PROLONGED USAGE OF THE PRODUCT OR PERHAPS PROGRAM AFTER SUCH ADJUSTMENTS MIGHT COMPRISE YOUR PERMIT TO SUCH TYPE OF ALTERATIONS.\r\n			</p>\r\n			<p>\r\n			THE FIRM WILL NOT RENDER SHIPPING ASSISTANCE. AND ALSO THE ORGANISATION IS NOT A SHIPPING PROVIDER. IT IS ACTUALLY UP TO THE THIRD PARTY (3RD) SHIPPING SERVICE PROVIDER. DRIVERS OR EVEN AUTOMOBILE PROVIDER TO PROVIDE TRANSPORTATION EXPERTISE WHICH CAN BE PROPOSED THROUGH UTILIZATION OF THE SOFTWARE OR PROGRAM.THE COMPANY PROVIDES DATA AS WELL AS A STRATEGY TO ACQUIRE SUCH TYPE OF EXTERNAL SHIPPING ASSISTANCE. BUT HOWEVER WILL NOT AND MUST NOT DESIRE TO AFFORD SHIPPING SOLUTIONS OR BEHAVE BY ALL MEANS AS A SHIPPING PROVIDER OR VENDOR, AND HAS NO OBLIGATION OR RESPONSIBILITY FOR VIRTUALLY ANY TRANSPORTATION ASSISTANCE OFFERED TO YOU BY SUCH 3RD PARTIES.\r\n			</p>\r\n			<p>\r\n			WE STRONGLY ADVISE YOU TO READ THE TERMS AND CONDITIONS AND PRIVACY POLICIES OF ANY THIRD-PARTY WEB SITES OR SERVICES THAT YOU VISIT.\r\n			</p>\r\n			\r\n		</div>\r\n		\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				FUNDAMENTAL CONTENT-RELATED RULES: \r\n			</h2>\r\n			<p>\r\n			Content \"indicates words, animations, graphics, songs, software programs, (apart from the Application), digital, video clip, information or any other additional materials.\r\n			</p>\r\n			<p>\r\n			\"Company Content\" indicates content that company produces and offered via the support or Application, for instance any Article certified or registered from a 3rd party, but yet excluding end user content.\r\n			</p>\r\n			<p>\r\n			\"User\" signifies an individual who accesses or even makes use of the program or software.\r\n			</p>\r\n			<p>\r\n			\"User content\" implies information that a consumer publishes, uploads, distributes, contributes, or broadcasts, to be made accessible via the provider or application.\r\n			</p>\r\n			<p>\r\n			\"Collective Content\" suggests, mutually, corporation Articles as well as subscriber content.\r\n			</p>\r\n			<p>\r\n			\"Driver\" usually means the unbiased that is hired by the service provider and is offering transport solutions using the company\'s technique.\r\n			</p>\r\n			<p>\r\n			*The Driver* denotes the length of period whilst the driver is franchised for between the begin point of the reserving as well as the ending period.it consists however is not restricted to the period whenever the Driver is basically in charge of the car.\r\n			</p>\r\n			\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				REPRESENTATIONS WITH GUARANTEES: \r\n			</h2>\r\n			<p>\r\n			By applying the program or services, you precisely symbolize and justify that you are lawfully eligible to join this Agreement. If you live in a jurisdiction that limits the usage of the program due to your age, or prevents the potential to venture into the transaction for example this one as a result of age, you need to adhere by such age limitations; therefore you must not use the software and support. Without restricting the foregoing, the service and program is unavailable to children (individuals under the age of {eighteen} 18.) By utilizing the software or program, you signify and guarantee that you are not less than 18 years aged. By also using the Application or the program, you signify and also justify you have the right, permission, and capability to enter into this contract and also will abide by the rules and agreement of this contract. Your involvement in using the program and - or software is entirely your singular, individual use. You might not authorize other people to use your own end user status; you might not allot or otherwise exchange your subscriber accounts to another person or organization. When making use of the software or even facility you accept to adhere with all relevant regulations from your country, the nation, state and town in which you are found while using the Software or service.\r\n			</p>\r\n			<p>\r\n			You might merely have access to the service using approved means. It is your choice to check to always be certain to download the relevant program for your unit. The company might not be held liable should you not have an appropriate device or if you have in any way downloaded the incorrect version of the Application for your device. The Firm reserves the right to cancel this Terms if you are using the program or software with an incompatible or even felonious device.\r\n			</p>\r\n			<p style=\"font-weight: bold;\">\r\n			By making use of the software or program, you concur that:\r\n			</p>\r\n			<p>\r\n			You will merely use the program or software for legitimate reasons; you are not going to use the services for transmitting or saving any illegal substance or for deceptive reasons.\r\n			</p>\r\n			<p>\r\n			You will not employ the program or Application to induce problems, aggravation or nuisance.\r\n			</p>\r\n			<p>\r\n			You will not likely hinder the correct performance of the system.\r\n			</p>\r\n			<p>\r\n			You would not seek to abuse the service or program by any means at all.\r\n			</p>\r\n			<p>\r\n			You need not replicate, or share the software or any other information without prior drafted consent from the company.\r\n			</p>\r\n			<p>\r\n			You can only apply the Application and software for your own personal usage and cannot re -sell it to 3rd party.\r\n			</p>\r\n			<p>\r\n			You would keep safe and secret your account pass-word or any identity we give you that allows entry to the service.\r\n			</p>\r\n			<p>\r\n			You will definitely render us with what - ever evidence of identification we may logically demand.\r\n			</p>\r\n			<p>\r\n			You should always use an entry point or 3G/4G information account which you are accredited to use.\r\n			</p>\r\n			<p>\r\n			You know that when asking for transportation services by TEXT, regular messaging fees will apply.\r\n			</p>\r\n			<p>\r\n			In case of any Accident during the drive, the Firm and the driver will not accept liability for any incidents, issues, reparations, or any illicit prosecutions. Aside from any individual illicit incrimination that accrues to the driver based on the motor vehicle act and regulation/guideline, you take accountability for permitting the Driver to take charge of the vehicle. Any loss to the automobile shall be borne by the client and the client only will organize for any insurance reports.\r\n			</p>\r\n			<p>\r\n			Perhaps you may take control or manage the vehicle any time as it is your lawful right during the Drive.Nonetheless, you portray that the Automobile is in good functioning condition and also secure to use under suitable business criteria and abide to all automobile specifications based on the regional town or municipal vehicle Act.\r\n			</p>\r\n			\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				PERMIT APPROVAL, LIMITATIONS AND COPYRIGHT RULES:\r\n			</h2>\r\n			<p style=\"font-weight: bold;\">\r\n			LICENCES APPROVED BY FIRM TO FIRM ARTICLE AND USER ARTICLE\r\n			</p>\r\n			<p>\r\n			Susceptible to your conformity with the stipulations of this Contract, Firm offers you a modest, nonexclusive, and non-transferable permit: (i) to observe, copy and print any Firm content purely for your personal and non- commercial needs; and (ii) to watch any end- user content to which you are allowed to view entirely for your personal and non-commercial objectives. You possess no right to sub-license the permit rights permitted in this area.\r\n			</p>\r\n			<p>\r\n			You should not exercise, duplicate, adjust, adapt, create derivative functions based on, deliver, permit, recycle, exchange, freely exhibit, openly execute, broadcast, supply, transmit, or else manipulate the program, application or mutual Article, apart from as explicitly allowed in this contract. No licenses or rights are allowed to you by indication or else under any rational assets purchased or handled by Firm, such as company logo, monogram and deal identities or its licensors, apart from the licenses and legal rights explicitly permitted in this Contract.\r\n			</p>\r\n			<p>\r\n			Company offers individual driver referring to its client via an on demand support and a projected later support. You affirm company to function and take control of your own vehicle throughout the drive. Firm will not at this time supply driver with a TR or Transportation Permit. This implies only private car drivers are supplied and the end- user undertakes that no industrial exercise shall be performed in the vehicle or the Driver will not stand for any business intentions by the end- user.\r\n			</p>\r\n			\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				PERMIT ALLOWED BY USER:\r\n			</h2>\r\n			<p>\r\n			We might, in our own primary decision, allow users to publish, submit, upload, post, or transfer User Article, By offering any user Article on or via the program or software, you hereby allow  to Firm a global, impending, everlasting, non- inclusive, royalty free-license, transferable, with the right to sub-license, to utilize, observe ,reproduce, adjust, change, deliver, license, promote, transport, publicly exhibit, openly perform, broadcast, flow, aired and otherwise manage such kind consumer Articles only on, via or by means of the support or program. Firm will not claim any possession rights in any end- user Article and nothing in this contract will be considered to limit any rights that you will have to utilize and manage any user Article.\r\n			</p>\r\n			<p>\r\n			You admit and concur that you are entirely accountable for all customer information that you provide via the program or software. Approximately you signify and guarantee that : (1) you possibly are the primary and unique holder of all user content that you provide through the support or program or you have all lawful rights, licenses, consents and launches that are essential to allow the firm and to rights in such User Articles, as considered under this contract; and (2) neither the consumer nor your publishing, publication, uploading, submission, distribution, or transmittal of the End - user Article or Firm\'s use of the End- user content ( or any part thereof ) on, due to by means of the program or software may infringe or misappropriate/breach  a 3rd party\'s patent, trademark, copyright, trade strategy, ethical rights or all other rational assets right, or rights or privacy or publicity, or lead to the violation of any relevant law or legislation.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				SOFTWARE LICENCE:\r\n			</h2>\r\n			<p>\r\n			Governed by your conformity with this Agreement, Firm offers you a modest non- transferable permit to acquire and set up a version of the software on an individual mobile gadget or computer that you possess or manage and to run such duplicate of the software entirely for your own personal usage. Moreover, with regards to any software found via or downloaded the Apple App Store or the Google Play Store you will need to use App Store or Play Store procured Application only: (i) on only an Apple/ Google/ branded merchandise that runs iOS (Apple\'s exclusive operating - system software program) Android (Google\'s patented operating system software); and (ii) as authorized by the utilization Guidelines established in the Apple App/Google Play Store Terms and Conditions. Company reserves every lawful right in and to the software not exclusively permitted to you under this Term.\r\n			</p>\r\n			<p style=\"font-weight: bold;\">\r\n			OBTANING AND COPYING THE.APPLICATION FROM THE ITUNES/PLAY STORE:\r\n			</p>\r\n			<p>\r\n			The following applicable to every App Store/Play Store Acquired Application:\r\n			</p>\r\n			<p>\r\n			You accept and confirm that (i) this Contract is decided between you and Firm only, and not Apple/Google, and (ii) Company, not Apple/Google, is exclusively accountable for the App Store derived Application and Article thereof. You usage of the App/Play Store procured Application has to adhere to the APP/Play Store service terms.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				COPYRIGHT RULES:\r\n			</h2>\r\n			<p>\r\n			Company values copyright regulations and requires its users to also do the same. It is always Company\'s policy to discontinue in suitable situations users or other account owners who constantly infringe or are regarded to be frequently infringing of copyright owners. mi.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				TRANSACTION TERMS AND TERMINATION RULES:\r\n			</h2>\r\n			<p>\r\n			Every fees that the company might charge you for the software or the program: are due instantly and non-refundable. This non-refundable plan shall apply all the time, irrespective of your choice to discontinue your usage, our choice to end your usage, hazard induced to our Application or program both structured. Unintentional or calculated, or any explanation either. The company reserves the right to ascertain final existing costs - kindly note the charges information shared on the web-site might not indicate the existing pricing.\r\n			</p>\r\n			<p>\r\n			The companies, at its primary wariness, render marketing plans with diverse features and various prices to any of our numerous clients. These marketing plans, except created to you, might have no effect either on your offer or deal. The Firm will alter the costs for our services or software, as we assume essential to our enterprise. We advise you to confirm back at our website occasionally if you are curious about how we are charging for the Service of Application.\r\n			</p>\r\n			<p>\r\n			The Driver can using wariness begins the travel between 10 -15 minutes of reaching the location or after having strike the I possess button, which yet is later. You must receive a confirmation within the App telling of this or contact the driver.\r\n			</p>\r\n			<p>\r\n			Every termination from your end should take place before the driver departs or within 15 minutes of the reservation for driver right now. For driver later on you need to make a termination 90 minutes before the reservation. For any reservations that have not been suspended in such behavior you need to pay for the driver\'s transit costs/expenses. If the reservation is halted in the course of the drive subsequently the trip fees must implement as per the cost card along with the transit charge and duties. Not settling the termination costs is in violation of this term.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				INTELLECTUAL ASSETS POSSESSION:\r\n			</h2>\r\n			<p>\r\n			The Firm itself (and also its licensors, wherein relevant) must own all right, deed and attention, such as all relevant cognitive estate rights, in and to the Software and the program and any other additional options, suggestions, development requests, opinions, advices, or any other guidelines supplied by you or other party concerning to the Application or program. This Agreement is not an offer and does not reveal to you any privileges of possession in or connected with the Application or the service, or any aesthetic assets rights acquired by the Firm. The company\'s name, the company\'s logo, plus the device names related to the Application and service are characteristics of company or third parties, and no other right or permit is offered to use them.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				THIRD PARTY COMMUNICATIONS:\r\n			</h2>\r\n			<p>\r\n			While in use of the application and program, you might enter into feedback with, acquire goods and/ or expertise from, or take part In campaigns of 3rd party program providers, marketers or contributors exhibiting their products and/or solutions via the application or service. Any of such exercise, and any phrase, situations, guarantees, or representations related to such action, is entirely between you and the relevant third party. The Firm plus its licensors will have no culpability, commitment, or obligation for any such feedback. Obtain, negotiation, or publicity between you or any such 3rd parties. The Firm does not support any online resources that are not connected through the program or software, and in no occasion shall the company or its licensors be liable for any substance, equipment, or any other components on or offered from such web-sites or third part merchants. The Firm presents the Application and service to you pursuant to the terns of this Contract. You realize , though, that some third party carriers of products and/or solutions might need your own binding agreement to supplement or various terms and conditions before you use of or access such stuff or solutions, and the Firm disclaims any or all liability or legal responsibility due to such contracts between you and the 3rd party vendors. The Firm may trust 3rd party marketing and promotion provided through the Application or program and other apparatus to waive the Application or service. By collaborating to these terms and conditions you accept to be receiving such advertising campaigns. If for any reason you wish not receives such promotion you ought to inform us in writing. The Firm reserves the right to charge you a greater amount for the support or program in case you opt out not to receive such promotion services.\r\n			</p>\r\n			<p>\r\n			This increased fee, if appropriate, would be uploaded on the company web-site situated at <a href=\"http://zuver.in/\" title=\"http://zuver.in/\">http://zuver.in/</a> The Firm might compose and launch information about you and your usage of  the Application or service on an unidentified method as part of a client platform  or related report or research. You accept that it is your Job to take realistic measures in all moves and contacts with any 3rd party you consult with through the service.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				INDEMNIFICATION:\r\n			</h2>\r\n			<p>\r\n			By engaging into this agreement thus using the software and program, you accept that you must protect, indemnify and secure the Firm, plus its licensors and each party\'s parent associations, subsidiaries, associates, officers, directors, consumers, personnel, attorneys and dealers from safe and against all claims, expenses, reparations, deficits, risks, or costs, (which includes attorney\'s costs and fees)emanating out of or with regards to : (a) your violation or disregard of any phrase of this Contract or any relevant rules or standards, regardless of if not referenced here-in ; (b) any violation of any legal right of any third party, such as vendors of transport providers organized through the program or software, or (c) your usage or abuse of the software or program.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				DISCLAIMER OF WARRANTY:\r\n			</h2>\r\n			<p>\r\n			THE FIRM BREWS NO REPRESENTATIVE, WARANTEE, OR ASSURANCE AS TO THE STABILITY, TIMELINESS, EXCELLENT, RELEVANCE, ACCESSIBILITY, PRECISION, OR TOTALITY OF THE PROGRAM OR SOFTWARE. THE COMPANY WILL NOT SYMBOLISE OR GUARANTEE THAT (A) THE UTILIZATION OF THE SERVICE OR APPLICATION WOULD BE SAFE, APPROPRIATE, NONSTOP OR ERROR FREE OR FUNCTION IN TANDEM WITH ANY OTHER EQUIPMENT, SOFTWARE, SYSTEM OR INFORMATION, (B), THE SERVICE OR SOFTWARE CAN SATISFY YOUR DESIRE OR ANTICIPATIONS, (C) EVERY SAVED RECORD WILL BE RELIABLE EFFICIENT , (D) THE CALIBER OR STANDARD OF ANY DEVICES, SERVICES, DATA, OR ADDITIONAL MATERIAL BOUGHT OR ACQUIRED BY YOU VIA THE PROVIDER WILL SUIT YOUR NEEDS OR GOALS, (E) FLAWS OR DISORDERS IN THE SERVICE OR APPLICATION WILL BE ADJUSTED, OR (F) THE FACILITY OR THE SERVER(S) WHICH MAKE THE PROGRAM ACCESSIBLE ARE FREE FROM MALWARES OR VARIOUS DESTRUCTIVE EQUIPMENTS, THE PROGRAM AND SOFTWARE IS OFFERED TO YOU PURELY ON AN \"AS IS\" METHOD, ALL FACTORS, REPRESENTATIONS AND WARANTY, EITHER IMPART, INDICTED,, STATUTORY OR NOT, SUCH AS, WITHOUT DAMPER, ANY SIGNIFIED ASSURANCE OF MERCHANTABILITY, HEALTH FOR A SPECIFIC INTENTION, OR NON INFRINGEMENT OF 3RD PARTY POLICIES, ARE HEREBY DISCLAIMED TO THE OPTIMUM DEGREE ALLOWED BY RELEVANT LAW BY THE FIRM. THE COMPANY MAKES NO DEPICTION, WARANTIES, OR ASSURANCE AS TO THE STABILITY, SAFETY, TIMELINESS, STANDARD, RELEVANCE OR PREVELANCE OF ANY EXPERTISE, DEVICES OR PRODUCTS ACQUIRED BY THIRD PARTIES BY EMPLOYING THE SERVICE OR APPLICATION.YOU ADMIT AND CONCUR THAT THE WHOLE DANGER EMANATING OUT OF YOUR USAGE OF THE SOFTWARE AND PROGRAM, AND ANY THIRD PARTY PRODUCTS OR SERVICES STAYS ENTIRELY WITH YOU, TO THE UTMOST SCOPE ALLOWED BY LAW.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				ONLINE DELAYS:\r\n			</h2>\r\n			<p>\r\n			THE FIRM\'S SERVICE AND APPLICATION CAN BE SUSCEPTIBLE BY RESTRICTIONS, DELAYS, AND OTHER CHALLENGES INTRINSIC IN THE USE OF THE WEB AND DIGITAL CONNECTIVITY. THE FIRM IS NOT LIABLE FOR ANY DELAY, SHIPPING FAILURES, OR ADDITIONAL DAMAGES CAUSED BY SUCH ISSUES.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				RESTRICTION OF LIABILITY:\r\n			</h2>\r\n			<p>\r\n			ABSOLUTLY NO EVENT SHALL THE FIRM AND/ORITS LICENSORS/ASSOCIATES BE VULNERABLE TO ANYBODY FOR ANY PUNITIVE, INDIRECT, UNIQUE, IDEAL, POINTLESS, CRUCIAL, OR OTHER HARM OF ANY KIND OR SORT (SUCH AS INDIVITUAL INJURY, LOSING OF DATA, EARNINGS, REVENUES, USAGE OF ANY OTHER MONETARY ADVANTAGE). THE FIRM AND ITS LICENSORS MUST NOT BE ACCOUNTABLE FOR ANY SHORTAGE, HARM, OR AN INJURY  WHICH MIGHT BE SUSTAINED BY YOU.ALTHOUG NOT RESTRICTED TO SHORTAGE, HARM OR INJURY EMANATING OUT OF, OR IN ANY WAY LINKED WITH THE PROGRAM OR SOFTWARE, INCLUDING BUT NOT RESTRICTED TO THE USAGE OR FAILURE TO USE THE PROGRAM OR SOFTWARE, ANY DEPENDENCY PLACED BY YOU ON THE ENTIRETY, PRECISION OR AVAILABILITY OF ANY MARKETING,OR DUE TO ANY PARTNERSHIP OR DEAL BETWEEN YOU AND ANY 3RD PARTY SERVICE VENDOR, ADVERTISER OR UPLINE WHOSE PROMOTIONS OCCURS ON THE WEB-SITE OR IS CALLED BY THE SERVICE OR APPLICATION.\r\n			</p>\r\n			<p>\r\n			THE FIRM MAY LET YOU KNOW 3RD PARTY SHIPPING MERCHANTS FOR THE INTENTIONS OF OFFERING TRANSPORTATION SERVICES. WE CAN NOT EXAMINE THE CAPABILITY, LEGALITY, OR APTITUDE OF ANY THIRD PARTY TRANSPORTATION VENDORS AND YOU PREDOMINANTLY SUSPEND AND INDEMNIFY THE FIRM FROM ANY AND ALL ANY CULPABILITY, ASSERTS OR DAMAGES CAUSED BY OR IN ANY WAY ASSOCIATED WITH THE THIRD PARTY SHIPPING VENDOR. YOU ADMIT THAT THIRD PARTY TRANSPORT VENDORS OFFERING TRANSPORT SOLUTIONS REQUIRED VIA Zuver MIGHT PROVIDE RIDESHARING OR PEER - TO PEER TRANSPORT SOLUTION AND MAY NOT BE TECHNICALLY CERTIFIED OR GRANTED. THE FIRM WILL NOT BE ANY PARTY TO CONFLICTS, DISCAUSSIONS OF CONFLICTS BETWEEN YOU AND ANY THIRD PARTY VENDORS.WE DO NOT AND WILL NOT PLAY ANY PART IN HANDLING PAYMENTS BETWEEN YOU AND THE 3RD PARTY VENDORS. LIABILITY FOR THE CHOICES YOU MADE CONCERNING SERVICES PROVIDED THROUGH THE SOFTWARE OR PROGRAM (WITH ALL OF THE EFFECTS) IS PLACED ENTIRELY ON YOU. WE CANNOT EVALUATE THE CAPABILITY, LEGALITY OR POTENTIAL OF ANY SUCH THIRD PARTIES AND YOU PRECISELY DEFER AND DISCHARGE THE COMPANY FROM ANY AND ALL THE DANGERS, STATEMENTS, CAUSE OF ACTION, OR HARM EMANATING FROM YOUR USAGE OF THE SOFTWARE OR SERVICE, OR IN ANY SORT CONNECTED WITH THE THIRD PARTIES PRESENTED TO YOU BY THE SOFTWARE OR PROGRAM. YOU SIMPLY SURRENDER AND EMIT ANY AND ALL RIGHTS AND ADVANTAGES WHICH READS AS PROVIDED BELOW: \"A UNIVERSAL DISCHARGE DOES NOT STRETCH TO ASSERTS WHICH THE LENDER/BROKER DOES NOT KNOW OR ALLEGE TO OCCUR IN HIS BENEFIT AT THE TIME OF PERFORMING THE LAUNCHING, WHICH, IF RECOGNIZED BY HIM, SHOULD HAVE MATERIALLY INFLUENCED HIS RESOLUTION WITH THE DEBTOR. \"\r\n			</p>\r\n			<p>\r\n			THE VALUE OF THE TRANSPORTATION COMPANIES ROUTINE VIA THE USAGE OF THE SUPPORT OR APPLICATION IS WHOLLY THE DUTIES OF THE THIRD PARTY CARRIER WHO EVENTUALLY OFFERS SUCH SHIPMENT SERVICES TO YOU.\r\n			</p>\r\n			<p>\r\n			YOU REALISE, CONSQUENTLY, THAT BY USING THE SOFTWARE AND SERVICE, YOU MIGHT BE SUBJECTED TO TRANSPORTATION THAT IS POSSIBLY HARMFUL, INJURIOUS, IRRITATING TO OFFSPRINGS, DANGEROUS OR AT LEAST UNDESIRABLE, WHICH YOU USE THE SOFTWARE AND SERVICE AT YOUR OWN PERSONAL RISK.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				NOTICE:\r\n			</h2>\r\n			<p>\r\n			The firm might give notice through using a broad notice on the program, e-mail to your mail account on the file in the firm\'s accounts data, or by drafted interaction delivered through top - class mail or pre- paid mail to your home address on record in the firm\'s accounts data. Such notice ought to be considered to have been presented upon the termination of forty-eight hours after posting or mailing (If delivered by first - class mail or prepaid post) or twelve hours after sending (if sent through e-mail).\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				RESPONSIBILITY:\r\n			</h2>\r\n			<p>\r\n			This Contract might not be designated by you without the earlier drafted authorization of the company however might be ascribed without your permission by the company to (i) a parent or subsidiary, (ii) a successor by merger (iii) an acquirer of valuables, virtually any purported project in violation of this portion is probably invalid.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				EXPORT REGULATION:\r\n			</h2>\r\n			<p>\r\n			You accept to abide fully by the all Indian and International export legal guidelines to be certain that neither the software nor all proficient information relevant thereto nor any immediate merchandise thereof is exported or re-exported straightly or indirectly in violation of, and in any reasons forbidden by, such legal guidelines. By utilizing the App/Play Store procured software, you suggest and justify that: (i) you are not within a nation that is susceptible to an Indian Government embargo, or that has been selected by the Indian Government as a terrorist nation; and (ii) you are not in any way outlined on any Indian Government list of illegal or restrained parties.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				JURISDICTION:\r\n			</h2>\r\n			<p>\r\n			The Courts of Mumbai, India will possess the primary and distinct jurisdiction in value of any factors, arbitration &suits owing to what is here-in found in this contract and consented to between the parties.\r\n			</p>\r\n		</div>\r\n		\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				GENERAL:\r\n			</h2>\r\n			<p>\r\n			No partnership, alliance, profession, or bureau partnership exists between you, the Firm or any 3rd party vendor as a result of this Contract or usage of the program or software. If any condition of this Contract is retained to be outdated or unenforceable, such condition could be struck and the outstanding conditions shall be imposed to the maximum degree under law. The malfunction of the Firm to enforce every right or condition in this Contract shall not csist a waiver of such right or clause unless recognized and consented to by the company in print. This contract entails the entire Agreement between you and the Firm and supersedes every previous or any contemporaneous discussion, dialogues, or Agreements, either drafted or oral, between the parties concerning the material included here-in.\r\n			</p>\r\n		</div>\r\n</div>', 1, 0, 1, 1, 1, '2017-07-26 00:00:00', '2017-07-26 00:00:00');
INSERT INTO `menucontentdetails` (`id`, `name`, `title`, `metaKeyword`, `description`, `linkName`, `content`, `sequenceOrder`, `menuId`, `status`, `createdBy`, `updatedBy`, `createdDate`, `updatedDate`) VALUES
(2, 'aboutus', 'About Us', 'About Us', 'About Us', 'aboutus', '<div class=\"key_block_outer1 clearfix\">\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				Terms and Conditions (\"Terms\") \r\n			</h2>\r\n			<p>\r\n			Please read these Terms and Conditions (\"Terms\", \"Terms and Conditions\") carefully before using the <a href=\"http://zuver.in/\" title=\"http://zuver.in/\">http://zuver.in/</a> website (the \"Service\") operated by Jobbie Services India Private Limited (\"us\", \"we\", or \"our\").\r\n			</p>\r\n			<p>\r\n			Your access to and use of the Service is conditioned on your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use the Service.\r\n			</p>\r\n			<p>\r\n			By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you may not access the Service.\r\n			</p>\r\n			<p>\r\n			The conditions and terms declared here-in (mutually, the \"Agreement\") consists of a lawful contract between you and also Jobbie Services India Private Limited, a Firm organized personally (the \"Firm\")   with a view to utilize the program (outlined below) as well as the related software (mentioned below) you have to accept the terms which has been set out below. By utilizing or acquiring any facilities provided to you by organization (combined, the \"service\") and also installing, preserving or accessing any related program provided by the corporation which function will be to allow you to apply the services (together, \"the Program\"), you hereby explicitly admit and also accept to be legally bound by the conditions and terms of the contract, and also virtually any upcoming amendments and even inclusions to this binding agreement as publicized periodically at <a href=\"http://zuver.in/\" title=\"http://zuver.in/\">http://zuver.in/</a> or via the support.\r\n			</p>\r\n			<p>\r\n			THE FIRM RESERVES ALL RIGHTS TO IMPROVE OR CHANGE THE TERMS OF THIS AGREEMENT OR SOME REGULATIONS CONCERNING THE SERVICES OR SOFTWARE ANY TIME, EFFECTUAL UPON PUBLISHING OF AN UPGRADED VERSION OF THIS CONTRACT ON THE SUPPORT OR EVEN SOFTWARE. YOU WILL BE ACCOUNTABLE FOR ROUTINELY EXAMINING THIS AGREEMENT. PROLONGED USAGE OF THE PRODUCT OR PERHAPS PROGRAM AFTER SUCH ADJUSTMENTS MIGHT COMPRISE YOUR PERMIT TO SUCH TYPE OF ALTERATIONS.\r\n			</p>\r\n			<p>\r\n			THE FIRM WILL NOT RENDER SHIPPING ASSISTANCE. AND ALSO THE ORGANISATION IS NOT A SHIPPING PROVIDER. IT IS ACTUALLY UP TO THE THIRD PARTY (3RD) SHIPPING SERVICE PROVIDER. DRIVERS OR EVEN AUTOMOBILE PROVIDER TO PROVIDE TRANSPORTATION EXPERTISE WHICH CAN BE PROPOSED THROUGH UTILIZATION OF THE SOFTWARE OR PROGRAM.THE COMPANY PROVIDES DATA AS WELL AS A STRATEGY TO ACQUIRE SUCH TYPE OF EXTERNAL SHIPPING ASSISTANCE. BUT HOWEVER WILL NOT AND MUST NOT DESIRE TO AFFORD SHIPPING SOLUTIONS OR BEHAVE BY ALL MEANS AS A SHIPPING PROVIDER OR VENDOR, AND HAS NO OBLIGATION OR RESPONSIBILITY FOR VIRTUALLY ANY TRANSPORTATION ASSISTANCE OFFERED TO YOU BY SUCH 3RD PARTIES.\r\n			</p>\r\n			<p>\r\n			WE STRONGLY ADVISE YOU TO READ THE TERMS AND CONDITIONS AND PRIVACY POLICIES OF ANY THIRD-PARTY WEB SITES OR SERVICES THAT YOU VISIT.\r\n			</p>\r\n			\r\n		</div>\r\n		\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				FUNDAMENTAL CONTENT-RELATED RULES: \r\n			</h2>\r\n			<p>\r\n			Content \"indicates words, animations, graphics, songs, software programs, (apart from the Application), digital, video clip, information or any other additional materials.\r\n			</p>\r\n			<p>\r\n			\"Company Content\" indicates content that company produces and offered via the support or Application, for instance any Article certified or registered from a 3rd party, but yet excluding end user content.\r\n			</p>\r\n			<p>\r\n			\"User\" signifies an individual who accesses or even makes use of the program or software.\r\n			</p>\r\n			<p>\r\n			\"User content\" implies information that a consumer publishes, uploads, distributes, contributes, or broadcasts, to be made accessible via the provider or application.\r\n			</p>\r\n			<p>\r\n			\"Collective Content\" suggests, mutually, corporation Articles as well as subscriber content.\r\n			</p>\r\n			<p>\r\n			\"Driver\" usually means the unbiased that is hired by the service provider and is offering transport solutions using the company\'s technique.\r\n			</p>\r\n			<p>\r\n			*The Driver* denotes the length of period whilst the driver is franchised for between the begin point of the reserving as well as the ending period.it consists however is not restricted to the period whenever the Driver is basically in charge of the car.\r\n			</p>\r\n			\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				REPRESENTATIONS WITH GUARANTEES: \r\n			</h2>\r\n			<p>\r\n			By applying the program or services, you precisely symbolize and justify that you are lawfully eligible to join this Agreement. If you live in a jurisdiction that limits the usage of the program due to your age, or prevents the potential to venture into the transaction for example this one as a result of age, you need to adhere by such age limitations; therefore you must not use the software and support. Without restricting the foregoing, the service and program is unavailable to children (individuals under the age of {eighteen} 18.) By utilizing the software or program, you signify and guarantee that you are not less than 18 years aged. By also using the Application or the program, you signify and also justify you have the right, permission, and capability to enter into this contract and also will abide by the rules and agreement of this contract. Your involvement in using the program and - or software is entirely your singular, individual use. You might not authorize other people to use your own end user status; you might not allot or otherwise exchange your subscriber accounts to another person or organization. When making use of the software or even facility you accept to adhere with all relevant regulations from your country, the nation, state and town in which you are found while using the Software or service.\r\n			</p>\r\n			<p>\r\n			You might merely have access to the service using approved means. It is your choice to check to always be certain to download the relevant program for your unit. The company might not be held liable should you not have an appropriate device or if you have in any way downloaded the incorrect version of the Application for your device. The Firm reserves the right to cancel this Terms if you are using the program or software with an incompatible or even felonious device.\r\n			</p>\r\n			<p style=\"font-weight: bold;\">\r\n			By making use of the software or program, you concur that:\r\n			</p>\r\n			<p>\r\n			You will merely use the program or software for legitimate reasons; you are not going to use the services for transmitting or saving any illegal substance or for deceptive reasons.\r\n			</p>\r\n			<p>\r\n			You will not employ the program or Application to induce problems, aggravation or nuisance.\r\n			</p>\r\n			<p>\r\n			You will not likely hinder the correct performance of the system.\r\n			</p>\r\n			<p>\r\n			You would not seek to abuse the service or program by any means at all.\r\n			</p>\r\n			<p>\r\n			You need not replicate, or share the software or any other information without prior drafted consent from the company.\r\n			</p>\r\n			<p>\r\n			You can only apply the Application and software for your own personal usage and cannot re -sell it to 3rd party.\r\n			</p>\r\n			<p>\r\n			You would keep safe and secret your account pass-word or any identity we give you that allows entry to the service.\r\n			</p>\r\n			<p>\r\n			You will definitely render us with what - ever evidence of identification we may logically demand.\r\n			</p>\r\n			<p>\r\n			You should always use an entry point or 3G/4G information account which you are accredited to use.\r\n			</p>\r\n			<p>\r\n			You know that when asking for transportation services by TEXT, regular messaging fees will apply.\r\n			</p>\r\n			<p>\r\n			In case of any Accident during the drive, the Firm and the driver will not accept liability for any incidents, issues, reparations, or any illicit prosecutions. Aside from any individual illicit incrimination that accrues to the driver based on the motor vehicle act and regulation/guideline, you take accountability for permitting the Driver to take charge of the vehicle. Any loss to the automobile shall be borne by the client and the client only will organize for any insurance reports.\r\n			</p>\r\n			<p>\r\n			Perhaps you may take control or manage the vehicle any time as it is your lawful right during the Drive.Nonetheless, you portray that the Automobile is in good functioning condition and also secure to use under suitable business criteria and abide to all automobile specifications based on the regional town or municipal vehicle Act.\r\n			</p>\r\n			\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				PERMIT APPROVAL, LIMITATIONS AND COPYRIGHT RULES:\r\n			</h2>\r\n			<p style=\"font-weight: bold;\">\r\n			LICENCES APPROVED BY FIRM TO FIRM ARTICLE AND USER ARTICLE\r\n			</p>\r\n			<p>\r\n			Susceptible to your conformity with the stipulations of this Contract, Firm offers you a modest, nonexclusive, and non-transferable permit: (i) to observe, copy and print any Firm content purely for your personal and non- commercial needs; and (ii) to watch any end- user content to which you are allowed to view entirely for your personal and non-commercial objectives. You possess no right to sub-license the permit rights permitted in this area.\r\n			</p>\r\n			<p>\r\n			You should not exercise, duplicate, adjust, adapt, create derivative functions based on, deliver, permit, recycle, exchange, freely exhibit, openly execute, broadcast, supply, transmit, or else manipulate the program, application or mutual Article, apart from as explicitly allowed in this contract. No licenses or rights are allowed to you by indication or else under any rational assets purchased or handled by Firm, such as company logo, monogram and deal identities or its licensors, apart from the licenses and legal rights explicitly permitted in this Contract.\r\n			</p>\r\n			<p>\r\n			Company offers individual driver referring to its client via an on demand support and a projected later support. You affirm company to function and take control of your own vehicle throughout the drive. Firm will not at this time supply driver with a TR or Transportation Permit. This implies only private car drivers are supplied and the end- user undertakes that no industrial exercise shall be performed in the vehicle or the Driver will not stand for any business intentions by the end- user.\r\n			</p>\r\n			\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				PERMIT ALLOWED BY USER:\r\n			</h2>\r\n			<p>\r\n			We might, in our own primary decision, allow users to publish, submit, upload, post, or transfer User Article, By offering any user Article on or via the program or software, you hereby allow  to Firm a global, impending, everlasting, non- inclusive, royalty free-license, transferable, with the right to sub-license, to utilize, observe ,reproduce, adjust, change, deliver, license, promote, transport, publicly exhibit, openly perform, broadcast, flow, aired and otherwise manage such kind consumer Articles only on, via or by means of the support or program. Firm will not claim any possession rights in any end- user Article and nothing in this contract will be considered to limit any rights that you will have to utilize and manage any user Article.\r\n			</p>\r\n			<p>\r\n			You admit and concur that you are entirely accountable for all customer information that you provide via the program or software. Approximately you signify and guarantee that : (1) you possibly are the primary and unique holder of all user content that you provide through the support or program or you have all lawful rights, licenses, consents and launches that are essential to allow the firm and to rights in such User Articles, as considered under this contract; and (2) neither the consumer nor your publishing, publication, uploading, submission, distribution, or transmittal of the End - user Article or Firm\'s use of the End- user content ( or any part thereof ) on, due to by means of the program or software may infringe or misappropriate/breach  a 3rd party\'s patent, trademark, copyright, trade strategy, ethical rights or all other rational assets right, or rights or privacy or publicity, or lead to the violation of any relevant law or legislation.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				SOFTWARE LICENCE:\r\n			</h2>\r\n			<p>\r\n			Governed by your conformity with this Agreement, Firm offers you a modest non- transferable permit to acquire and set up a version of the software on an individual mobile gadget or computer that you possess or manage and to run such duplicate of the software entirely for your own personal usage. Moreover, with regards to any software found via or downloaded the Apple App Store or the Google Play Store you will need to use App Store or Play Store procured Application only: (i) on only an Apple/ Google/ branded merchandise that runs iOS (Apple\'s exclusive operating - system software program) Android (Google\'s patented operating system software); and (ii) as authorized by the utilization Guidelines established in the Apple App/Google Play Store Terms and Conditions. Company reserves every lawful right in and to the software not exclusively permitted to you under this Term.\r\n			</p>\r\n			<p style=\"font-weight: bold;\">\r\n			OBTANING AND COPYING THE.APPLICATION FROM THE ITUNES/PLAY STORE:\r\n			</p>\r\n			<p>\r\n			The following applicable to every App Store/Play Store Acquired Application:\r\n			</p>\r\n			<p>\r\n			You accept and confirm that (i) this Contract is decided between you and Firm only, and not Apple/Google, and (ii) Company, not Apple/Google, is exclusively accountable for the App Store derived Application and Article thereof. You usage of the App/Play Store procured Application has to adhere to the APP/Play Store service terms.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				COPYRIGHT RULES:\r\n			</h2>\r\n			<p>\r\n			Company values copyright regulations and requires its users to also do the same. It is always Company\'s policy to discontinue in suitable situations users or other account owners who constantly infringe or are regarded to be frequently infringing of copyright owners. mi.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				TRANSACTION TERMS AND TERMINATION RULES:\r\n			</h2>\r\n			<p>\r\n			Every fees that the company might charge you for the software or the program: are due instantly and non-refundable. This non-refundable plan shall apply all the time, irrespective of your choice to discontinue your usage, our choice to end your usage, hazard induced to our Application or program both structured. Unintentional or calculated, or any explanation either. The company reserves the right to ascertain final existing costs - kindly note the charges information shared on the web-site might not indicate the existing pricing.\r\n			</p>\r\n			<p>\r\n			The companies, at its primary wariness, render marketing plans with diverse features and various prices to any of our numerous clients. These marketing plans, except created to you, might have no effect either on your offer or deal. The Firm will alter the costs for our services or software, as we assume essential to our enterprise. We advise you to confirm back at our website occasionally if you are curious about how we are charging for the Service of Application.\r\n			</p>\r\n			<p>\r\n			The Driver can using wariness begins the travel between 10 -15 minutes of reaching the location or after having strike the I possess button, which yet is later. You must receive a confirmation within the App telling of this or contact the driver.\r\n			</p>\r\n			<p>\r\n			Every termination from your end should take place before the driver departs or within 15 minutes of the reservation for driver right now. For driver later on you need to make a termination 90 minutes before the reservation. For any reservations that have not been suspended in such behavior you need to pay for the driver\'s transit costs/expenses. If the reservation is halted in the course of the drive subsequently the trip fees must implement as per the cost card along with the transit charge and duties. Not settling the termination costs is in violation of this term.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				INTELLECTUAL ASSETS POSSESSION:\r\n			</h2>\r\n			<p>\r\n			The Firm itself (and also its licensors, wherein relevant) must own all right, deed and attention, such as all relevant cognitive estate rights, in and to the Software and the program and any other additional options, suggestions, development requests, opinions, advices, or any other guidelines supplied by you or other party concerning to the Application or program. This Agreement is not an offer and does not reveal to you any privileges of possession in or connected with the Application or the service, or any aesthetic assets rights acquired by the Firm. The company\'s name, the company\'s logo, plus the device names related to the Application and service are characteristics of company or third parties, and no other right or permit is offered to use them.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				THIRD PARTY COMMUNICATIONS:\r\n			</h2>\r\n			<p>\r\n			While in use of the application and program, you might enter into feedback with, acquire goods and/ or expertise from, or take part In campaigns of 3rd party program providers, marketers or contributors exhibiting their products and/or solutions via the application or service. Any of such exercise, and any phrase, situations, guarantees, or representations related to such action, is entirely between you and the relevant third party. The Firm plus its licensors will have no culpability, commitment, or obligation for any such feedback. Obtain, negotiation, or publicity between you or any such 3rd parties. The Firm does not support any online resources that are not connected through the program or software, and in no occasion shall the company or its licensors be liable for any substance, equipment, or any other components on or offered from such web-sites or third part merchants. The Firm presents the Application and service to you pursuant to the terns of this Contract. You realize , though, that some third party carriers of products and/or solutions might need your own binding agreement to supplement or various terms and conditions before you use of or access such stuff or solutions, and the Firm disclaims any or all liability or legal responsibility due to such contracts between you and the 3rd party vendors. The Firm may trust 3rd party marketing and promotion provided through the Application or program and other apparatus to waive the Application or service. By collaborating to these terms and conditions you accept to be receiving such advertising campaigns. If for any reason you wish not receives such promotion you ought to inform us in writing. The Firm reserves the right to charge you a greater amount for the support or program in case you opt out not to receive such promotion services.\r\n			</p>\r\n			<p>\r\n			This increased fee, if appropriate, would be uploaded on the company web-site situated at <a href=\"http://zuver.in/\" title=\"http://zuver.in/\">http://zuver.in/</a> The Firm might compose and launch information about you and your usage of  the Application or service on an unidentified method as part of a client platform  or related report or research. You accept that it is your Job to take realistic measures in all moves and contacts with any 3rd party you consult with through the service.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				INDEMNIFICATION:\r\n			</h2>\r\n			<p>\r\n			By engaging into this agreement thus using the software and program, you accept that you must protect, indemnify and secure the Firm, plus its licensors and each party\'s parent associations, subsidiaries, associates, officers, directors, consumers, personnel, attorneys and dealers from safe and against all claims, expenses, reparations, deficits, risks, or costs, (which includes attorney\'s costs and fees)emanating out of or with regards to : (a) your violation or disregard of any phrase of this Contract or any relevant rules or standards, regardless of if not referenced here-in ; (b) any violation of any legal right of any third party, such as vendors of transport providers organized through the program or software, or (c) your usage or abuse of the software or program.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				DISCLAIMER OF WARRANTY:\r\n			</h2>\r\n			<p>\r\n			THE FIRM BREWS NO REPRESENTATIVE, WARANTEE, OR ASSURANCE AS TO THE STABILITY, TIMELINESS, EXCELLENT, RELEVANCE, ACCESSIBILITY, PRECISION, OR TOTALITY OF THE PROGRAM OR SOFTWARE. THE COMPANY WILL NOT SYMBOLISE OR GUARANTEE THAT (A) THE UTILIZATION OF THE SERVICE OR APPLICATION WOULD BE SAFE, APPROPRIATE, NONSTOP OR ERROR FREE OR FUNCTION IN TANDEM WITH ANY OTHER EQUIPMENT, SOFTWARE, SYSTEM OR INFORMATION, (B), THE SERVICE OR SOFTWARE CAN SATISFY YOUR DESIRE OR ANTICIPATIONS, (C) EVERY SAVED RECORD WILL BE RELIABLE EFFICIENT , (D) THE CALIBER OR STANDARD OF ANY DEVICES, SERVICES, DATA, OR ADDITIONAL MATERIAL BOUGHT OR ACQUIRED BY YOU VIA THE PROVIDER WILL SUIT YOUR NEEDS OR GOALS, (E) FLAWS OR DISORDERS IN THE SERVICE OR APPLICATION WILL BE ADJUSTED, OR (F) THE FACILITY OR THE SERVER(S) WHICH MAKE THE PROGRAM ACCESSIBLE ARE FREE FROM MALWARES OR VARIOUS DESTRUCTIVE EQUIPMENTS, THE PROGRAM AND SOFTWARE IS OFFERED TO YOU PURELY ON AN \"AS IS\" METHOD, ALL FACTORS, REPRESENTATIONS AND WARANTY, EITHER IMPART, INDICTED,, STATUTORY OR NOT, SUCH AS, WITHOUT DAMPER, ANY SIGNIFIED ASSURANCE OF MERCHANTABILITY, HEALTH FOR A SPECIFIC INTENTION, OR NON INFRINGEMENT OF 3RD PARTY POLICIES, ARE HEREBY DISCLAIMED TO THE OPTIMUM DEGREE ALLOWED BY RELEVANT LAW BY THE FIRM. THE COMPANY MAKES NO DEPICTION, WARANTIES, OR ASSURANCE AS TO THE STABILITY, SAFETY, TIMELINESS, STANDARD, RELEVANCE OR PREVELANCE OF ANY EXPERTISE, DEVICES OR PRODUCTS ACQUIRED BY THIRD PARTIES BY EMPLOYING THE SERVICE OR APPLICATION.YOU ADMIT AND CONCUR THAT THE WHOLE DANGER EMANATING OUT OF YOUR USAGE OF THE SOFTWARE AND PROGRAM, AND ANY THIRD PARTY PRODUCTS OR SERVICES STAYS ENTIRELY WITH YOU, TO THE UTMOST SCOPE ALLOWED BY LAW.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				ONLINE DELAYS:\r\n			</h2>\r\n			<p>\r\n			THE FIRM\'S SERVICE AND APPLICATION CAN BE SUSCEPTIBLE BY RESTRICTIONS, DELAYS, AND OTHER CHALLENGES INTRINSIC IN THE USE OF THE WEB AND DIGITAL CONNECTIVITY. THE FIRM IS NOT LIABLE FOR ANY DELAY, SHIPPING FAILURES, OR ADDITIONAL DAMAGES CAUSED BY SUCH ISSUES.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				RESTRICTION OF LIABILITY:\r\n			</h2>\r\n			<p>\r\n			ABSOLUTLY NO EVENT SHALL THE FIRM AND/ORITS LICENSORS/ASSOCIATES BE VULNERABLE TO ANYBODY FOR ANY PUNITIVE, INDIRECT, UNIQUE, IDEAL, POINTLESS, CRUCIAL, OR OTHER HARM OF ANY KIND OR SORT (SUCH AS INDIVITUAL INJURY, LOSING OF DATA, EARNINGS, REVENUES, USAGE OF ANY OTHER MONETARY ADVANTAGE). THE FIRM AND ITS LICENSORS MUST NOT BE ACCOUNTABLE FOR ANY SHORTAGE, HARM, OR AN INJURY  WHICH MIGHT BE SUSTAINED BY YOU.ALTHOUG NOT RESTRICTED TO SHORTAGE, HARM OR INJURY EMANATING OUT OF, OR IN ANY WAY LINKED WITH THE PROGRAM OR SOFTWARE, INCLUDING BUT NOT RESTRICTED TO THE USAGE OR FAILURE TO USE THE PROGRAM OR SOFTWARE, ANY DEPENDENCY PLACED BY YOU ON THE ENTIRETY, PRECISION OR AVAILABILITY OF ANY MARKETING,OR DUE TO ANY PARTNERSHIP OR DEAL BETWEEN YOU AND ANY 3RD PARTY SERVICE VENDOR, ADVERTISER OR UPLINE WHOSE PROMOTIONS OCCURS ON THE WEB-SITE OR IS CALLED BY THE SERVICE OR APPLICATION.\r\n			</p>\r\n			<p>\r\n			THE FIRM MAY LET YOU KNOW 3RD PARTY SHIPPING MERCHANTS FOR THE INTENTIONS OF OFFERING TRANSPORTATION SERVICES. WE CAN NOT EXAMINE THE CAPABILITY, LEGALITY, OR APTITUDE OF ANY THIRD PARTY TRANSPORTATION VENDORS AND YOU PREDOMINANTLY SUSPEND AND INDEMNIFY THE FIRM FROM ANY AND ALL ANY CULPABILITY, ASSERTS OR DAMAGES CAUSED BY OR IN ANY WAY ASSOCIATED WITH THE THIRD PARTY SHIPPING VENDOR. YOU ADMIT THAT THIRD PARTY TRANSPORT VENDORS OFFERING TRANSPORT SOLUTIONS REQUIRED VIA Zuver MIGHT PROVIDE RIDESHARING OR PEER - TO PEER TRANSPORT SOLUTION AND MAY NOT BE TECHNICALLY CERTIFIED OR GRANTED. THE FIRM WILL NOT BE ANY PARTY TO CONFLICTS, DISCAUSSIONS OF CONFLICTS BETWEEN YOU AND ANY THIRD PARTY VENDORS.WE DO NOT AND WILL NOT PLAY ANY PART IN HANDLING PAYMENTS BETWEEN YOU AND THE 3RD PARTY VENDORS. LIABILITY FOR THE CHOICES YOU MADE CONCERNING SERVICES PROVIDED THROUGH THE SOFTWARE OR PROGRAM (WITH ALL OF THE EFFECTS) IS PLACED ENTIRELY ON YOU. WE CANNOT EVALUATE THE CAPABILITY, LEGALITY OR POTENTIAL OF ANY SUCH THIRD PARTIES AND YOU PRECISELY DEFER AND DISCHARGE THE COMPANY FROM ANY AND ALL THE DANGERS, STATEMENTS, CAUSE OF ACTION, OR HARM EMANATING FROM YOUR USAGE OF THE SOFTWARE OR SERVICE, OR IN ANY SORT CONNECTED WITH THE THIRD PARTIES PRESENTED TO YOU BY THE SOFTWARE OR PROGRAM. YOU SIMPLY SURRENDER AND EMIT ANY AND ALL RIGHTS AND ADVANTAGES WHICH READS AS PROVIDED BELOW: \"A UNIVERSAL DISCHARGE DOES NOT STRETCH TO ASSERTS WHICH THE LENDER/BROKER DOES NOT KNOW OR ALLEGE TO OCCUR IN HIS BENEFIT AT THE TIME OF PERFORMING THE LAUNCHING, WHICH, IF RECOGNIZED BY HIM, SHOULD HAVE MATERIALLY INFLUENCED HIS RESOLUTION WITH THE DEBTOR. \"\r\n			</p>\r\n			<p>\r\n			THE VALUE OF THE TRANSPORTATION COMPANIES ROUTINE VIA THE USAGE OF THE SUPPORT OR APPLICATION IS WHOLLY THE DUTIES OF THE THIRD PARTY CARRIER WHO EVENTUALLY OFFERS SUCH SHIPMENT SERVICES TO YOU.\r\n			</p>\r\n			<p>\r\n			YOU REALISE, CONSQUENTLY, THAT BY USING THE SOFTWARE AND SERVICE, YOU MIGHT BE SUBJECTED TO TRANSPORTATION THAT IS POSSIBLY HARMFUL, INJURIOUS, IRRITATING TO OFFSPRINGS, DANGEROUS OR AT LEAST UNDESIRABLE, WHICH YOU USE THE SOFTWARE AND SERVICE AT YOUR OWN PERSONAL RISK.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				NOTICE:\r\n			</h2>\r\n			<p>\r\n			The firm might give notice through using a broad notice on the program, e-mail to your mail account on the file in the firm\'s accounts data, or by drafted interaction delivered through top - class mail or pre- paid mail to your home address on record in the firm\'s accounts data. Such notice ought to be considered to have been presented upon the termination of forty-eight hours after posting or mailing (If delivered by first - class mail or prepaid post) or twelve hours after sending (if sent through e-mail).\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				RESPONSIBILITY:\r\n			</h2>\r\n			<p>\r\n			This Contract might not be designated by you without the earlier drafted authorization of the company however might be ascribed without your permission by the company to (i) a parent or subsidiary, (ii) a successor by merger (iii) an acquirer of valuables, virtually any purported project in violation of this portion is probably invalid.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				EXPORT REGULATION:\r\n			</h2>\r\n			<p>\r\n			You accept to abide fully by the all Indian and International export legal guidelines to be certain that neither the software nor all proficient information relevant thereto nor any immediate merchandise thereof is exported or re-exported straightly or indirectly in violation of, and in any reasons forbidden by, such legal guidelines. By utilizing the App/Play Store procured software, you suggest and justify that: (i) you are not within a nation that is susceptible to an Indian Government embargo, or that has been selected by the Indian Government as a terrorist nation; and (ii) you are not in any way outlined on any Indian Government list of illegal or restrained parties.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				JURISDICTION:\r\n			</h2>\r\n			<p>\r\n			The Courts of Mumbai, India will possess the primary and distinct jurisdiction in value of any factors, arbitration &suits owing to what is here-in found in this contract and consented to between the parties.\r\n			</p>\r\n		</div>\r\n		\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				GENERAL:\r\n			</h2>\r\n			<p>\r\n			No partnership, alliance, profession, or bureau partnership exists between you, the Firm or any 3rd party vendor as a result of this Contract or usage of the program or software. If any condition of this Contract is retained to be outdated or unenforceable, such condition could be struck and the outstanding conditions shall be imposed to the maximum degree under law. The malfunction of the Firm to enforce every right or condition in this Contract shall not csist a waiver of such right or clause unless recognized and consented to by the company in print. This contract entails the entire Agreement between you and the Firm and supersedes every previous or any contemporaneous discussion, dialogues, or Agreements, either drafted or oral, between the parties concerning the material included here-in.\r\n			</p>\r\n		</div>\r\n</div>', 1, 0, 1, 1, 1, '2017-07-26 00:00:00', '2017-07-26 00:00:00');
INSERT INTO `menucontentdetails` (`id`, `name`, `title`, `metaKeyword`, `description`, `linkName`, `content`, `sequenceOrder`, `menuId`, `status`, `createdBy`, `updatedBy`, `createdDate`, `updatedDate`) VALUES
(3, 'privacypolicy', 'Privacy Policy', 'Privacy Policys', 'Privacy Policy', 'privacypolicy', '<div class=\"key_block_outer1 clearfix\">\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				Terms and Conditions (\"Terms\") \r\n			</h2>\r\n			<p>\r\n			Please read these Terms and Conditions (\"Terms\", \"Terms and Conditions\") carefully before using the <a href=\"http://zuver.in/\" title=\"http://zuver.in/\">http://zuver.in/</a> website (the \"Service\") operated by Jobbie Services India Private Limited (\"us\", \"we\", or \"our\").\r\n			</p>\r\n			<p>\r\n			Your access to and use of the Service is conditioned on your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use the Service.\r\n			</p>\r\n			<p>\r\n			By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you may not access the Service.\r\n			</p>\r\n			<p>\r\n			The conditions and terms declared here-in (mutually, the \"Agreement\") consists of a lawful contract between you and also Jobbie Services India Private Limited, a Firm organized personally (the \"Firm\")   with a view to utilize the program (outlined below) as well as the related software (mentioned below) you have to accept the terms which has been set out below. By utilizing or acquiring any facilities provided to you by organization (combined, the \"service\") and also installing, preserving or accessing any related program provided by the corporation which function will be to allow you to apply the services (together, \"the Program\"), you hereby explicitly admit and also accept to be legally bound by the conditions and terms of the contract, and also virtually any upcoming amendments and even inclusions to this binding agreement as publicized periodically at <a href=\"http://zuver.in/\" title=\"http://zuver.in/\">http://zuver.in/</a> or via the support.\r\n			</p>\r\n			<p>\r\n			THE FIRM RESERVES ALL RIGHTS TO IMPROVE OR CHANGE THE TERMS OF THIS AGREEMENT OR SOME REGULATIONS CONCERNING THE SERVICES OR SOFTWARE ANY TIME, EFFECTUAL UPON PUBLISHING OF AN UPGRADED VERSION OF THIS CONTRACT ON THE SUPPORT OR EVEN SOFTWARE. YOU WILL BE ACCOUNTABLE FOR ROUTINELY EXAMINING THIS AGREEMENT. PROLONGED USAGE OF THE PRODUCT OR PERHAPS PROGRAM AFTER SUCH ADJUSTMENTS MIGHT COMPRISE YOUR PERMIT TO SUCH TYPE OF ALTERATIONS.\r\n			</p>\r\n			<p>\r\n			THE FIRM WILL NOT RENDER SHIPPING ASSISTANCE. AND ALSO THE ORGANISATION IS NOT A SHIPPING PROVIDER. IT IS ACTUALLY UP TO THE THIRD PARTY (3RD) SHIPPING SERVICE PROVIDER. DRIVERS OR EVEN AUTOMOBILE PROVIDER TO PROVIDE TRANSPORTATION EXPERTISE WHICH CAN BE PROPOSED THROUGH UTILIZATION OF THE SOFTWARE OR PROGRAM.THE COMPANY PROVIDES DATA AS WELL AS A STRATEGY TO ACQUIRE SUCH TYPE OF EXTERNAL SHIPPING ASSISTANCE. BUT HOWEVER WILL NOT AND MUST NOT DESIRE TO AFFORD SHIPPING SOLUTIONS OR BEHAVE BY ALL MEANS AS A SHIPPING PROVIDER OR VENDOR, AND HAS NO OBLIGATION OR RESPONSIBILITY FOR VIRTUALLY ANY TRANSPORTATION ASSISTANCE OFFERED TO YOU BY SUCH 3RD PARTIES.\r\n			</p>\r\n			<p>\r\n			WE STRONGLY ADVISE YOU TO READ THE TERMS AND CONDITIONS AND PRIVACY POLICIES OF ANY THIRD-PARTY WEB SITES OR SERVICES THAT YOU VISIT.\r\n			</p>\r\n			\r\n		</div>\r\n		\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				FUNDAMENTAL CONTENT-RELATED RULES: \r\n			</h2>\r\n			<p>\r\n			Content \"indicates words, animations, graphics, songs, software programs, (apart from the Application), digital, video clip, information or any other additional materials.\r\n			</p>\r\n			<p>\r\n			\"Company Content\" indicates content that company produces and offered via the support or Application, for instance any Article certified or registered from a 3rd party, but yet excluding end user content.\r\n			</p>\r\n			<p>\r\n			\"User\" signifies an individual who accesses or even makes use of the program or software.\r\n			</p>\r\n			<p>\r\n			\"User content\" implies information that a consumer publishes, uploads, distributes, contributes, or broadcasts, to be made accessible via the provider or application.\r\n			</p>\r\n			<p>\r\n			\"Collective Content\" suggests, mutually, corporation Articles as well as subscriber content.\r\n			</p>\r\n			<p>\r\n			\"Driver\" usually means the unbiased that is hired by the service provider and is offering transport solutions using the company\'s technique.\r\n			</p>\r\n			<p>\r\n			*The Driver* denotes the length of period whilst the driver is franchised for between the begin point of the reserving as well as the ending period.it consists however is not restricted to the period whenever the Driver is basically in charge of the car.\r\n			</p>\r\n			\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				REPRESENTATIONS WITH GUARANTEES: \r\n			</h2>\r\n			<p>\r\n			By applying the program or services, you precisely symbolize and justify that you are lawfully eligible to join this Agreement. If you live in a jurisdiction that limits the usage of the program due to your age, or prevents the potential to venture into the transaction for example this one as a result of age, you need to adhere by such age limitations; therefore you must not use the software and support. Without restricting the foregoing, the service and program is unavailable to children (individuals under the age of {eighteen} 18.) By utilizing the software or program, you signify and guarantee that you are not less than 18 years aged. By also using the Application or the program, you signify and also justify you have the right, permission, and capability to enter into this contract and also will abide by the rules and agreement of this contract. Your involvement in using the program and - or software is entirely your singular, individual use. You might not authorize other people to use your own end user status; you might not allot or otherwise exchange your subscriber accounts to another person or organization. When making use of the software or even facility you accept to adhere with all relevant regulations from your country, the nation, state and town in which you are found while using the Software or service.\r\n			</p>\r\n			<p>\r\n			You might merely have access to the service using approved means. It is your choice to check to always be certain to download the relevant program for your unit. The company might not be held liable should you not have an appropriate device or if you have in any way downloaded the incorrect version of the Application for your device. The Firm reserves the right to cancel this Terms if you are using the program or software with an incompatible or even felonious device.\r\n			</p>\r\n			<p style=\"font-weight: bold;\">\r\n			By making use of the software or program, you concur that:\r\n			</p>\r\n			<p>\r\n			You will merely use the program or software for legitimate reasons; you are not going to use the services for transmitting or saving any illegal substance or for deceptive reasons.\r\n			</p>\r\n			<p>\r\n			You will not employ the program or Application to induce problems, aggravation or nuisance.\r\n			</p>\r\n			<p>\r\n			You will not likely hinder the correct performance of the system.\r\n			</p>\r\n			<p>\r\n			You would not seek to abuse the service or program by any means at all.\r\n			</p>\r\n			<p>\r\n			You need not replicate, or share the software or any other information without prior drafted consent from the company.\r\n			</p>\r\n			<p>\r\n			You can only apply the Application and software for your own personal usage and cannot re -sell it to 3rd party.\r\n			</p>\r\n			<p>\r\n			You would keep safe and secret your account pass-word or any identity we give you that allows entry to the service.\r\n			</p>\r\n			<p>\r\n			You will definitely render us with what - ever evidence of identification we may logically demand.\r\n			</p>\r\n			<p>\r\n			You should always use an entry point or 3G/4G information account which you are accredited to use.\r\n			</p>\r\n			<p>\r\n			You know that when asking for transportation services by TEXT, regular messaging fees will apply.\r\n			</p>\r\n			<p>\r\n			In case of any Accident during the drive, the Firm and the driver will not accept liability for any incidents, issues, reparations, or any illicit prosecutions. Aside from any individual illicit incrimination that accrues to the driver based on the motor vehicle act and regulation/guideline, you take accountability for permitting the Driver to take charge of the vehicle. Any loss to the automobile shall be borne by the client and the client only will organize for any insurance reports.\r\n			</p>\r\n			<p>\r\n			Perhaps you may take control or manage the vehicle any time as it is your lawful right during the Drive.Nonetheless, you portray that the Automobile is in good functioning condition and also secure to use under suitable business criteria and abide to all automobile specifications based on the regional town or municipal vehicle Act.\r\n			</p>\r\n			\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				PERMIT APPROVAL, LIMITATIONS AND COPYRIGHT RULES:\r\n			</h2>\r\n			<p style=\"font-weight: bold;\">\r\n			LICENCES APPROVED BY FIRM TO FIRM ARTICLE AND USER ARTICLE\r\n			</p>\r\n			<p>\r\n			Susceptible to your conformity with the stipulations of this Contract, Firm offers you a modest, nonexclusive, and non-transferable permit: (i) to observe, copy and print any Firm content purely for your personal and non- commercial needs; and (ii) to watch any end- user content to which you are allowed to view entirely for your personal and non-commercial objectives. You possess no right to sub-license the permit rights permitted in this area.\r\n			</p>\r\n			<p>\r\n			You should not exercise, duplicate, adjust, adapt, create derivative functions based on, deliver, permit, recycle, exchange, freely exhibit, openly execute, broadcast, supply, transmit, or else manipulate the program, application or mutual Article, apart from as explicitly allowed in this contract. No licenses or rights are allowed to you by indication or else under any rational assets purchased or handled by Firm, such as company logo, monogram and deal identities or its licensors, apart from the licenses and legal rights explicitly permitted in this Contract.\r\n			</p>\r\n			<p>\r\n			Company offers individual driver referring to its client via an on demand support and a projected later support. You affirm company to function and take control of your own vehicle throughout the drive. Firm will not at this time supply driver with a TR or Transportation Permit. This implies only private car drivers are supplied and the end- user undertakes that no industrial exercise shall be performed in the vehicle or the Driver will not stand for any business intentions by the end- user.\r\n			</p>\r\n			\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				PERMIT ALLOWED BY USER:\r\n			</h2>\r\n			<p>\r\n			We might, in our own primary decision, allow users to publish, submit, upload, post, or transfer User Article, By offering any user Article on or via the program or software, you hereby allow  to Firm a global, impending, everlasting, non- inclusive, royalty free-license, transferable, with the right to sub-license, to utilize, observe ,reproduce, adjust, change, deliver, license, promote, transport, publicly exhibit, openly perform, broadcast, flow, aired and otherwise manage such kind consumer Articles only on, via or by means of the support or program. Firm will not claim any possession rights in any end- user Article and nothing in this contract will be considered to limit any rights that you will have to utilize and manage any user Article.\r\n			</p>\r\n			<p>\r\n			You admit and concur that you are entirely accountable for all customer information that you provide via the program or software. Approximately you signify and guarantee that : (1) you possibly are the primary and unique holder of all user content that you provide through the support or program or you have all lawful rights, licenses, consents and launches that are essential to allow the firm and to rights in such User Articles, as considered under this contract; and (2) neither the consumer nor your publishing, publication, uploading, submission, distribution, or transmittal of the End - user Article or Firm\'s use of the End- user content ( or any part thereof ) on, due to by means of the program or software may infringe or misappropriate/breach  a 3rd party\'s patent, trademark, copyright, trade strategy, ethical rights or all other rational assets right, or rights or privacy or publicity, or lead to the violation of any relevant law or legislation.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				SOFTWARE LICENCE:\r\n			</h2>\r\n			<p>\r\n			Governed by your conformity with this Agreement, Firm offers you a modest non- transferable permit to acquire and set up a version of the software on an individual mobile gadget or computer that you possess or manage and to run such duplicate of the software entirely for your own personal usage. Moreover, with regards to any software found via or downloaded the Apple App Store or the Google Play Store you will need to use App Store or Play Store procured Application only: (i) on only an Apple/ Google/ branded merchandise that runs iOS (Apple\'s exclusive operating - system software program) Android (Google\'s patented operating system software); and (ii) as authorized by the utilization Guidelines established in the Apple App/Google Play Store Terms and Conditions. Company reserves every lawful right in and to the software not exclusively permitted to you under this Term.\r\n			</p>\r\n			<p style=\"font-weight: bold;\">\r\n			OBTANING AND COPYING THE.APPLICATION FROM THE ITUNES/PLAY STORE:\r\n			</p>\r\n			<p>\r\n			The following applicable to every App Store/Play Store Acquired Application:\r\n			</p>\r\n			<p>\r\n			You accept and confirm that (i) this Contract is decided between you and Firm only, and not Apple/Google, and (ii) Company, not Apple/Google, is exclusively accountable for the App Store derived Application and Article thereof. You usage of the App/Play Store procured Application has to adhere to the APP/Play Store service terms.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				COPYRIGHT RULES:\r\n			</h2>\r\n			<p>\r\n			Company values copyright regulations and requires its users to also do the same. It is always Company\'s policy to discontinue in suitable situations users or other account owners who constantly infringe or are regarded to be frequently infringing of copyright owners. mi.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				TRANSACTION TERMS AND TERMINATION RULES:\r\n			</h2>\r\n			<p>\r\n			Every fees that the company might charge you for the software or the program: are due instantly and non-refundable. This non-refundable plan shall apply all the time, irrespective of your choice to discontinue your usage, our choice to end your usage, hazard induced to our Application or program both structured. Unintentional or calculated, or any explanation either. The company reserves the right to ascertain final existing costs - kindly note the charges information shared on the web-site might not indicate the existing pricing.\r\n			</p>\r\n			<p>\r\n			The companies, at its primary wariness, render marketing plans with diverse features and various prices to any of our numerous clients. These marketing plans, except created to you, might have no effect either on your offer or deal. The Firm will alter the costs for our services or software, as we assume essential to our enterprise. We advise you to confirm back at our website occasionally if you are curious about how we are charging for the Service of Application.\r\n			</p>\r\n			<p>\r\n			The Driver can using wariness begins the travel between 10 -15 minutes of reaching the location or after having strike the I possess button, which yet is later. You must receive a confirmation within the App telling of this or contact the driver.\r\n			</p>\r\n			<p>\r\n			Every termination from your end should take place before the driver departs or within 15 minutes of the reservation for driver right now. For driver later on you need to make a termination 90 minutes before the reservation. For any reservations that have not been suspended in such behavior you need to pay for the driver\'s transit costs/expenses. If the reservation is halted in the course of the drive subsequently the trip fees must implement as per the cost card along with the transit charge and duties. Not settling the termination costs is in violation of this term.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				INTELLECTUAL ASSETS POSSESSION:\r\n			</h2>\r\n			<p>\r\n			The Firm itself (and also its licensors, wherein relevant) must own all right, deed and attention, such as all relevant cognitive estate rights, in and to the Software and the program and any other additional options, suggestions, development requests, opinions, advices, or any other guidelines supplied by you or other party concerning to the Application or program. This Agreement is not an offer and does not reveal to you any privileges of possession in or connected with the Application or the service, or any aesthetic assets rights acquired by the Firm. The company\'s name, the company\'s logo, plus the device names related to the Application and service are characteristics of company or third parties, and no other right or permit is offered to use them.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				THIRD PARTY COMMUNICATIONS:\r\n			</h2>\r\n			<p>\r\n			While in use of the application and program, you might enter into feedback with, acquire goods and/ or expertise from, or take part In campaigns of 3rd party program providers, marketers or contributors exhibiting their products and/or solutions via the application or service. Any of such exercise, and any phrase, situations, guarantees, or representations related to such action, is entirely between you and the relevant third party. The Firm plus its licensors will have no culpability, commitment, or obligation for any such feedback. Obtain, negotiation, or publicity between you or any such 3rd parties. The Firm does not support any online resources that are not connected through the program or software, and in no occasion shall the company or its licensors be liable for any substance, equipment, or any other components on or offered from such web-sites or third part merchants. The Firm presents the Application and service to you pursuant to the terns of this Contract. You realize , though, that some third party carriers of products and/or solutions might need your own binding agreement to supplement or various terms and conditions before you use of or access such stuff or solutions, and the Firm disclaims any or all liability or legal responsibility due to such contracts between you and the 3rd party vendors. The Firm may trust 3rd party marketing and promotion provided through the Application or program and other apparatus to waive the Application or service. By collaborating to these terms and conditions you accept to be receiving such advertising campaigns. If for any reason you wish not receives such promotion you ought to inform us in writing. The Firm reserves the right to charge you a greater amount for the support or program in case you opt out not to receive such promotion services.\r\n			</p>\r\n			<p>\r\n			This increased fee, if appropriate, would be uploaded on the company web-site situated at <a href=\"http://zuver.in/\" title=\"http://zuver.in/\">http://zuver.in/</a> The Firm might compose and launch information about you and your usage of  the Application or service on an unidentified method as part of a client platform  or related report or research. You accept that it is your Job to take realistic measures in all moves and contacts with any 3rd party you consult with through the service.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				INDEMNIFICATION:\r\n			</h2>\r\n			<p>\r\n			By engaging into this agreement thus using the software and program, you accept that you must protect, indemnify and secure the Firm, plus its licensors and each party\'s parent associations, subsidiaries, associates, officers, directors, consumers, personnel, attorneys and dealers from safe and against all claims, expenses, reparations, deficits, risks, or costs, (which includes attorney\'s costs and fees)emanating out of or with regards to : (a) your violation or disregard of any phrase of this Contract or any relevant rules or standards, regardless of if not referenced here-in ; (b) any violation of any legal right of any third party, such as vendors of transport providers organized through the program or software, or (c) your usage or abuse of the software or program.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				DISCLAIMER OF WARRANTY:\r\n			</h2>\r\n			<p>\r\n			THE FIRM BREWS NO REPRESENTATIVE, WARANTEE, OR ASSURANCE AS TO THE STABILITY, TIMELINESS, EXCELLENT, RELEVANCE, ACCESSIBILITY, PRECISION, OR TOTALITY OF THE PROGRAM OR SOFTWARE. THE COMPANY WILL NOT SYMBOLISE OR GUARANTEE THAT (A) THE UTILIZATION OF THE SERVICE OR APPLICATION WOULD BE SAFE, APPROPRIATE, NONSTOP OR ERROR FREE OR FUNCTION IN TANDEM WITH ANY OTHER EQUIPMENT, SOFTWARE, SYSTEM OR INFORMATION, (B), THE SERVICE OR SOFTWARE CAN SATISFY YOUR DESIRE OR ANTICIPATIONS, (C) EVERY SAVED RECORD WILL BE RELIABLE EFFICIENT , (D) THE CALIBER OR STANDARD OF ANY DEVICES, SERVICES, DATA, OR ADDITIONAL MATERIAL BOUGHT OR ACQUIRED BY YOU VIA THE PROVIDER WILL SUIT YOUR NEEDS OR GOALS, (E) FLAWS OR DISORDERS IN THE SERVICE OR APPLICATION WILL BE ADJUSTED, OR (F) THE FACILITY OR THE SERVER(S) WHICH MAKE THE PROGRAM ACCESSIBLE ARE FREE FROM MALWARES OR VARIOUS DESTRUCTIVE EQUIPMENTS, THE PROGRAM AND SOFTWARE IS OFFERED TO YOU PURELY ON AN \"AS IS\" METHOD, ALL FACTORS, REPRESENTATIONS AND WARANTY, EITHER IMPART, INDICTED,, STATUTORY OR NOT, SUCH AS, WITHOUT DAMPER, ANY SIGNIFIED ASSURANCE OF MERCHANTABILITY, HEALTH FOR A SPECIFIC INTENTION, OR NON INFRINGEMENT OF 3RD PARTY POLICIES, ARE HEREBY DISCLAIMED TO THE OPTIMUM DEGREE ALLOWED BY RELEVANT LAW BY THE FIRM. THE COMPANY MAKES NO DEPICTION, WARANTIES, OR ASSURANCE AS TO THE STABILITY, SAFETY, TIMELINESS, STANDARD, RELEVANCE OR PREVELANCE OF ANY EXPERTISE, DEVICES OR PRODUCTS ACQUIRED BY THIRD PARTIES BY EMPLOYING THE SERVICE OR APPLICATION.YOU ADMIT AND CONCUR THAT THE WHOLE DANGER EMANATING OUT OF YOUR USAGE OF THE SOFTWARE AND PROGRAM, AND ANY THIRD PARTY PRODUCTS OR SERVICES STAYS ENTIRELY WITH YOU, TO THE UTMOST SCOPE ALLOWED BY LAW.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				ONLINE DELAYS:\r\n			</h2>\r\n			<p>\r\n			THE FIRM\'S SERVICE AND APPLICATION CAN BE SUSCEPTIBLE BY RESTRICTIONS, DELAYS, AND OTHER CHALLENGES INTRINSIC IN THE USE OF THE WEB AND DIGITAL CONNECTIVITY. THE FIRM IS NOT LIABLE FOR ANY DELAY, SHIPPING FAILURES, OR ADDITIONAL DAMAGES CAUSED BY SUCH ISSUES.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				RESTRICTION OF LIABILITY:\r\n			</h2>\r\n			<p>\r\n			ABSOLUTLY NO EVENT SHALL THE FIRM AND/ORITS LICENSORS/ASSOCIATES BE VULNERABLE TO ANYBODY FOR ANY PUNITIVE, INDIRECT, UNIQUE, IDEAL, POINTLESS, CRUCIAL, OR OTHER HARM OF ANY KIND OR SORT (SUCH AS INDIVITUAL INJURY, LOSING OF DATA, EARNINGS, REVENUES, USAGE OF ANY OTHER MONETARY ADVANTAGE). THE FIRM AND ITS LICENSORS MUST NOT BE ACCOUNTABLE FOR ANY SHORTAGE, HARM, OR AN INJURY  WHICH MIGHT BE SUSTAINED BY YOU.ALTHOUG NOT RESTRICTED TO SHORTAGE, HARM OR INJURY EMANATING OUT OF, OR IN ANY WAY LINKED WITH THE PROGRAM OR SOFTWARE, INCLUDING BUT NOT RESTRICTED TO THE USAGE OR FAILURE TO USE THE PROGRAM OR SOFTWARE, ANY DEPENDENCY PLACED BY YOU ON THE ENTIRETY, PRECISION OR AVAILABILITY OF ANY MARKETING,OR DUE TO ANY PARTNERSHIP OR DEAL BETWEEN YOU AND ANY 3RD PARTY SERVICE VENDOR, ADVERTISER OR UPLINE WHOSE PROMOTIONS OCCURS ON THE WEB-SITE OR IS CALLED BY THE SERVICE OR APPLICATION.\r\n			</p>\r\n			<p>\r\n			THE FIRM MAY LET YOU KNOW 3RD PARTY SHIPPING MERCHANTS FOR THE INTENTIONS OF OFFERING TRANSPORTATION SERVICES. WE CAN NOT EXAMINE THE CAPABILITY, LEGALITY, OR APTITUDE OF ANY THIRD PARTY TRANSPORTATION VENDORS AND YOU PREDOMINANTLY SUSPEND AND INDEMNIFY THE FIRM FROM ANY AND ALL ANY CULPABILITY, ASSERTS OR DAMAGES CAUSED BY OR IN ANY WAY ASSOCIATED WITH THE THIRD PARTY SHIPPING VENDOR. YOU ADMIT THAT THIRD PARTY TRANSPORT VENDORS OFFERING TRANSPORT SOLUTIONS REQUIRED VIA Zuver MIGHT PROVIDE RIDESHARING OR PEER - TO PEER TRANSPORT SOLUTION AND MAY NOT BE TECHNICALLY CERTIFIED OR GRANTED. THE FIRM WILL NOT BE ANY PARTY TO CONFLICTS, DISCAUSSIONS OF CONFLICTS BETWEEN YOU AND ANY THIRD PARTY VENDORS.WE DO NOT AND WILL NOT PLAY ANY PART IN HANDLING PAYMENTS BETWEEN YOU AND THE 3RD PARTY VENDORS. LIABILITY FOR THE CHOICES YOU MADE CONCERNING SERVICES PROVIDED THROUGH THE SOFTWARE OR PROGRAM (WITH ALL OF THE EFFECTS) IS PLACED ENTIRELY ON YOU. WE CANNOT EVALUATE THE CAPABILITY, LEGALITY OR POTENTIAL OF ANY SUCH THIRD PARTIES AND YOU PRECISELY DEFER AND DISCHARGE THE COMPANY FROM ANY AND ALL THE DANGERS, STATEMENTS, CAUSE OF ACTION, OR HARM EMANATING FROM YOUR USAGE OF THE SOFTWARE OR SERVICE, OR IN ANY SORT CONNECTED WITH THE THIRD PARTIES PRESENTED TO YOU BY THE SOFTWARE OR PROGRAM. YOU SIMPLY SURRENDER AND EMIT ANY AND ALL RIGHTS AND ADVANTAGES WHICH READS AS PROVIDED BELOW: \"A UNIVERSAL DISCHARGE DOES NOT STRETCH TO ASSERTS WHICH THE LENDER/BROKER DOES NOT KNOW OR ALLEGE TO OCCUR IN HIS BENEFIT AT THE TIME OF PERFORMING THE LAUNCHING, WHICH, IF RECOGNIZED BY HIM, SHOULD HAVE MATERIALLY INFLUENCED HIS RESOLUTION WITH THE DEBTOR. \"\r\n			</p>\r\n			<p>\r\n			THE VALUE OF THE TRANSPORTATION COMPANIES ROUTINE VIA THE USAGE OF THE SUPPORT OR APPLICATION IS WHOLLY THE DUTIES OF THE THIRD PARTY CARRIER WHO EVENTUALLY OFFERS SUCH SHIPMENT SERVICES TO YOU.\r\n			</p>\r\n			<p>\r\n			YOU REALISE, CONSQUENTLY, THAT BY USING THE SOFTWARE AND SERVICE, YOU MIGHT BE SUBJECTED TO TRANSPORTATION THAT IS POSSIBLY HARMFUL, INJURIOUS, IRRITATING TO OFFSPRINGS, DANGEROUS OR AT LEAST UNDESIRABLE, WHICH YOU USE THE SOFTWARE AND SERVICE AT YOUR OWN PERSONAL RISK.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				NOTICE:\r\n			</h2>\r\n			<p>\r\n			The firm might give notice through using a broad notice on the program, e-mail to your mail account on the file in the firm\'s accounts data, or by drafted interaction delivered through top - class mail or pre- paid mail to your home address on record in the firm\'s accounts data. Such notice ought to be considered to have been presented upon the termination of forty-eight hours after posting or mailing (If delivered by first - class mail or prepaid post) or twelve hours after sending (if sent through e-mail).\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				RESPONSIBILITY:\r\n			</h2>\r\n			<p>\r\n			This Contract might not be designated by you without the earlier drafted authorization of the company however might be ascribed without your permission by the company to (i) a parent or subsidiary, (ii) a successor by merger (iii) an acquirer of valuables, virtually any purported project in violation of this portion is probably invalid.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				EXPORT REGULATION:\r\n			</h2>\r\n			<p>\r\n			You accept to abide fully by the all Indian and International export legal guidelines to be certain that neither the software nor all proficient information relevant thereto nor any immediate merchandise thereof is exported or re-exported straightly or indirectly in violation of, and in any reasons forbidden by, such legal guidelines. By utilizing the App/Play Store procured software, you suggest and justify that: (i) you are not within a nation that is susceptible to an Indian Government embargo, or that has been selected by the Indian Government as a terrorist nation; and (ii) you are not in any way outlined on any Indian Government list of illegal or restrained parties.\r\n			</p>\r\n		</div>\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				JURISDICTION:\r\n			</h2>\r\n			<p>\r\n			The Courts of Mumbai, India will possess the primary and distinct jurisdiction in value of any factors, arbitration &suits owing to what is here-in found in this contract and consented to between the parties.\r\n			</p>\r\n		</div>\r\n		\r\n		<div class=\"terms_info_block\">\r\n			<h2 class=\"terms_title\">\r\n				GENERAL:\r\n			</h2>\r\n			<p>\r\n			No partnership, alliance, profession, or bureau partnership exists between you, the Firm or any 3rd party vendor as a result of this Contract or usage of the program or software. If any condition of this Contract is retained to be outdated or unenforceable, such condition could be struck and the outstanding conditions shall be imposed to the maximum degree under law. The malfunction of the Firm to enforce every right or condition in this Contract shall not csist a waiver of such right or clause unless recognized and consented to by the company in print. This contract entails the entire Agreement between you and the Firm and supersedes every previous or any contemporaneous discussion, dialogues, or Agreements, either drafted or oral, between the parties concerning the material included here-in.\r\n			</p>\r\n		</div>\r\n</div>', 1, 0, 1, 1, 1, '2017-07-26 00:00:00', '2017-07-26 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `moduleaccesspermission`
--

CREATE TABLE `moduleaccesspermission` (
  `id` int(11) UNSIGNED NOT NULL,
  `moduleId` int(11) UNSIGNED NOT NULL,
  `roleType` int(11) UNSIGNED NOT NULL,
  `isAccess` tinyint(1) NOT NULL DEFAULT '1',
  `isRead` tinyint(1) NOT NULL DEFAULT '1',
  `isWrite` tinyint(1) NOT NULL DEFAULT '1',
  `isCreate` tinyint(1) NOT NULL DEFAULT '1',
  `isDelete` tinyint(1) NOT NULL DEFAULT '1',
  `isFullAccess` tinyint(1) NOT NULL DEFAULT '1',
  `createdBy` int(11) UNSIGNED NOT NULL,
  `updatedBy` int(11) UNSIGNED NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `moduleaccesspermission`
--

INSERT INTO `moduleaccesspermission` (`id`, `moduleId`, `roleType`, `isAccess`, `isRead`, `isWrite`, `isCreate`, `isDelete`, `isFullAccess`, `createdBy`, `updatedBy`, `createdDatetime`, `updatedDatetime`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(3, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(4, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(5, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(6, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(7, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(8, 8, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(9, 9, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(10, 10, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(11, 11, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(12, 12, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(13, 13, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(15, 15, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(16, 16, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(17, 1, 2, 1, 1, 1, 1, 1, 0, 1, 11, '2017-08-15 00:00:00', '2017-08-23 21:23:48'),
(18, 2, 2, 1, 1, 1, 1, 1, 0, 1, 11, '2017-08-15 00:00:00', '2017-08-23 21:23:48'),
(19, 3, 2, 1, 1, 1, 1, 1, 1, 1, 11, '2017-08-15 00:00:00', '2017-08-23 21:23:48'),
(20, 4, 2, 1, 1, 1, 1, 1, 1, 1, 11, '2017-08-15 00:00:00', '2017-08-23 21:23:48'),
(21, 5, 2, 1, 1, 1, 1, 1, 1, 1, 11, '2017-08-15 00:00:00', '2017-08-23 21:23:48'),
(22, 6, 2, 1, 1, 1, 1, 1, 1, 1, 11, '2017-08-15 00:00:00', '2017-08-23 21:23:48'),
(23, 7, 2, 1, 1, 1, 1, 1, 1, 1, 11, '2017-08-15 00:00:00', '2017-08-23 21:23:48'),
(24, 8, 2, 1, 1, 1, 1, 1, 1, 1, 11, '2017-08-15 00:00:00', '2017-08-23 21:23:48'),
(25, 9, 2, 1, 1, 1, 1, 1, 1, 1, 11, '2017-08-15 00:00:00', '2017-08-23 21:23:48'),
(26, 10, 2, 1, 1, 1, 1, 1, 1, 1, 11, '2017-08-15 00:00:00', '2017-08-23 21:23:48'),
(27, 11, 2, 1, 1, 1, 1, 1, 1, 1, 11, '2017-08-15 00:00:00', '2017-08-23 21:23:48'),
(28, 12, 2, 1, 1, 1, 1, 1, 1, 1, 11, '2017-08-15 00:00:00', '2017-08-23 21:23:48'),
(29, 13, 2, 1, 1, 1, 1, 1, 1, 1, 11, '2017-08-15 00:00:00', '2017-08-23 21:23:48'),
(30, 14, 2, 1, 1, 1, 1, 1, 1, 1, 11, '2017-08-15 00:00:00', '2017-08-23 21:23:48'),
(31, 15, 2, 1, 1, 1, 1, 1, 1, 1, 11, '2017-08-15 00:00:00', '2017-08-23 21:23:49'),
(32, 16, 2, 1, 1, 1, 1, 1, 1, 1, 11, '2017-08-15 00:00:00', '2017-08-23 21:23:49'),
(33, 1, 3, 1, 1, 0, 0, 1, 1, 1, 0, '2017-08-15 00:00:00', '2017-08-16 10:26:01'),
(34, 2, 3, 1, 1, 0, 0, 1, 1, 1, 0, '2017-08-15 00:00:00', '2017-08-16 10:26:01'),
(35, 3, 3, 1, 1, 0, 0, 1, 1, 1, 0, '2017-08-15 00:00:00', '2017-08-16 10:26:01'),
(36, 4, 3, 1, 1, 1, 1, 1, 1, 1, 0, '2017-08-15 00:00:00', '2017-08-16 10:26:01'),
(37, 5, 3, 1, 1, 1, 1, 1, 1, 1, 0, '2017-08-15 00:00:00', '2017-08-16 10:26:01'),
(38, 6, 3, 1, 1, 1, 1, 1, 1, 1, 0, '2017-08-15 00:00:00', '2017-08-16 10:26:01'),
(39, 7, 3, 1, 1, 1, 1, 1, 1, 1, 0, '2017-08-15 00:00:00', '2017-08-16 10:26:01'),
(40, 8, 3, 1, 1, 1, 1, 1, 1, 1, 0, '2017-08-15 00:00:00', '2017-08-16 10:26:01'),
(41, 9, 3, 1, 1, 1, 1, 1, 1, 1, 0, '2017-08-15 00:00:00', '2017-08-16 10:26:01'),
(42, 10, 3, 1, 1, 1, 1, 1, 1, 1, 0, '2017-08-15 00:00:00', '2017-08-16 10:26:01'),
(43, 11, 3, 1, 1, 1, 1, 1, 1, 1, 0, '2017-08-15 00:00:00', '2017-08-16 10:26:01'),
(44, 12, 3, 1, 1, 1, 1, 1, 1, 1, 0, '2017-08-15 00:00:00', '2017-08-16 10:26:01'),
(45, 13, 3, 1, 1, 1, 1, 1, 1, 1, 0, '2017-08-15 00:00:00', '2017-08-16 10:26:02'),
(46, 14, 3, 1, 1, 1, 1, 1, 1, 1, 0, '2017-08-15 00:00:00', '2017-08-16 10:26:02'),
(47, 15, 3, 1, 1, 1, 1, 1, 1, 1, 0, '2017-08-15 00:00:00', '2017-08-16 10:26:02'),
(48, 16, 3, 1, 1, 1, 1, 1, 1, 1, 0, '2017-08-15 00:00:00', '2017-08-16 10:26:02'),
(49, 1, 4, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(50, 2, 4, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(51, 3, 4, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(52, 4, 4, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(53, 5, 4, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(54, 6, 4, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(55, 7, 4, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(56, 8, 4, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(57, 9, 4, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(58, 10, 4, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(59, 11, 4, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(60, 12, 4, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(61, 13, 4, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(62, 14, 4, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(63, 15, 4, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(64, 16, 4, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(65, 1, 5, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(66, 2, 5, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(67, 3, 5, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(68, 4, 5, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(69, 5, 5, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(70, 6, 5, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(71, 7, 5, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(72, 8, 5, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(73, 9, 5, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(74, 10, 5, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(75, 11, 5, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(76, 12, 5, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(77, 13, 5, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(78, 14, 5, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(79, 15, 5, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(80, 16, 5, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(81, 1, 6, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(82, 2, 6, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(83, 3, 6, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(84, 4, 6, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(85, 5, 6, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(86, 6, 6, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(87, 7, 6, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(88, 8, 6, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(89, 9, 6, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(90, 10, 6, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(91, 11, 6, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(92, 12, 6, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(93, 13, 6, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(94, 14, 6, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(95, 15, 6, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(96, 16, 6, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(97, 1, 7, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(98, 2, 7, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(99, 3, 7, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(100, 4, 7, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(101, 5, 7, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(102, 6, 7, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(103, 7, 7, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(104, 8, 7, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(105, 9, 7, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(106, 10, 7, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(107, 11, 7, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(108, 12, 7, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(109, 13, 7, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(110, 14, 7, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(111, 15, 7, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(112, 16, 7, 1, 1, 1, 1, 1, 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `moduledetails`
--

CREATE TABLE `moduledetails` (
  `id` int(11) UNSIGNED NOT NULL,
  `moduleName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `createdBy` int(11) UNSIGNED NOT NULL,
  `updatedBy` int(11) UNSIGNED NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `moduledetails`
--

INSERT INTO `moduledetails` (`id`, `moduleName`, `description`, `status`, `createdBy`, `updatedBy`, `createdDatetime`, `updatedDatetime`) VALUES
(1, 'Customer', 'Customer/Passenger Records', 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(2, 'Driver', 'Driver Records', 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(3, 'Taxi', 'Taxi Records', 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(4, 'Taxi device', 'Taxi Device Records', 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(5, 'Taxi Owner', 'Taxi Owner Records', 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(6, 'Trip', 'Trip Records', 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(7, 'Promocode', 'Promocode Records', 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(8, 'Users', 'Users Records', 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(9, 'Reports', 'Reports Records', 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(10, 'Taxi Tariff', 'Taxi Tariff Records', 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(11, 'SOS', 'SOS Records', 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(12, 'Entity', 'Entity Records', 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(13, 'Zone', 'Zone Records', 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(14, 'Country', 'Country Records', 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(15, 'Notification', 'Notification Records', 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(16, 'Module Access', 'Module Access Records', 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `startDatetime` datetime NOT NULL,
  `endDatetime` datetime NOT NULL,
  `notificationTo` set('N','P','U','D','O') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N' COMMENT 'N=None,P=Passenger,D=Driver,U=User,O=Others',
  `notificationType` set('N','P','S') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'P' COMMENT 'N=None,P=Google Push,S=SMS',
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `createdBy` int(11) NOT NULL,
  `updatedBy` int(11) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedDateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `title`, `description`, `startDatetime`, `endDatetime`, `notificationTo`, `notificationType`, `isDeleted`, `status`, `createdBy`, `updatedBy`, `createdDatetime`, `updatedDateTime`) VALUES
(1, 'First', 'No description', '2017-10-12 21:33:21', '2017-10-27 21:33:12', 'P', 'P', 0, 1, 1, 0, '2017-08-13 00:00:00', '2017-08-13 00:00:00'),
(2, 'Second', 'second test', '2017-09-08 20:40:57', '2017-09-24 20:41:00', 'D', 'S', 0, 1, 11, 11, '2017-09-08 20:41:06', '0000-00-00 00:00:00'),
(3, 'test test', 'test test', '2017-11-02 11:17:40', '2017-11-03 11:17:43', 'D', 'P', 0, 1, 11, 11, '2017-11-02 11:18:01', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `passenger`
--

CREATE TABLE `passenger` (
  `id` int(11) UNSIGNED NOT NULL,
  `passengerCode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `firstName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `dob` date NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `otp` int(6) NOT NULL,
  `profileImage` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deviceId` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deviceType` int(11) UNSIGNED NOT NULL,
  `signupFrom` int(11) UNSIGNED NOT NULL,
  `registerType` int(11) UNSIGNED NOT NULL,
  `gcmId` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `walletAmount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `zoneId` int(11) UNSIGNED NOT NULL,
  `entityId` int(11) UNSIGNED NOT NULL,
  `currentLatitude` float(10,6) NOT NULL DEFAULT '0.000000',
  `currentLongitude` float(10,6) NOT NULL DEFAULT '0.000000',
  `loginStatus` tinyint(1) NOT NULL DEFAULT '0',
  `lastLoggedIn` datetime NOT NULL,
  `penaltyTripCount` int(11) NOT NULL DEFAULT '0',
  `isEmailVerified` tinyint(1) NOT NULL DEFAULT '0',
  `activationStatus` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `createdBy` int(11) UNSIGNED NOT NULL,
  `updatedBy` int(11) UNSIGNED NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `passengertransactiondetails`
--

CREATE TABLE `passengertransactiondetails` (
  `id` int(11) UNSIGNED NOT NULL,
  `passengerId` int(11) UNSIGNED NOT NULL,
  `referralId` int(11) UNSIGNED NOT NULL,
  `tripId` bigint(20) UNSIGNED NOT NULL,
  `transactionAmount` decimal(10,2) NOT NULL,
  `previousAmount` decimal(10,2) NOT NULL,
  `currentAmount` decimal(10,2) NOT NULL,
  `transactionStatus` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `transactionType` int(11) UNSIGNED NOT NULL,
  `transactionMode` int(11) UNSIGNED NOT NULL,
  `transactionFrom` int(11) NOT NULL,
  `transactionId` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comments` text COLLATE utf8_unicode_ci NOT NULL,
  `createdBy` int(11) UNSIGNED NOT NULL,
  `updatedBy` int(11) UNSIGNED NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `promocodedetails`
--

CREATE TABLE `promocodedetails` (
  `id` int(11) UNSIGNED NOT NULL,
  `promoCode` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `passengerId` int(11) NOT NULL,
  `zoneId` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `entityId` int(11) UNSIGNED NOT NULL,
  `promoDiscountAmount` decimal(10,2) NOT NULL,
  `promoDiscountType` int(11) UNSIGNED NOT NULL,
  `promoStartDatetime` datetime NOT NULL,
  `promoEndDatetime` datetime NOT NULL,
  `promoCodeLimit` int(6) NOT NULL,
  `promoCodeUserLimit` int(6) NOT NULL,
  `isFirstFreeRide` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `createdBy` int(11) UNSIGNED NOT NULL,
  `updatedBy` int(11) UNSIGNED NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedDateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ratecarddetails`
--

CREATE TABLE `ratecarddetails` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `taxiCategoryType` int(11) UNSIGNED NOT NULL,
  `dayConvenienceCharge` decimal(10,2) NOT NULL,
  `nightConvenienceCharge` decimal(10,2) NOT NULL DEFAULT '0.00',
  `zoneId` int(11) UNSIGNED NOT NULL,
  `subZoneId` int(11) UNSIGNED NOT NULL,
  `entityId` int(11) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) UNSIGNED NOT NULL,
  `updatedBy` int(11) UNSIGNED NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ratecardslabdetails`
--

CREATE TABLE `ratecardslabdetails` (
  `id` int(11) UNSIGNED NOT NULL,
  `rateCardId` int(11) UNSIGNED NOT NULL,
  `slabStart` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slabEnd` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slabType` int(11) UNSIGNED NOT NULL,
  `slabChargeType` int(11) UNSIGNED NOT NULL,
  `slabCharge` decimal(10,2) NOT NULL DEFAULT '0.00',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `createdBy` int(11) UNSIGNED NOT NULL,
  `updatedBy` int(11) UNSIGNED NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `referraldetails`
--

CREATE TABLE `referraldetails` (
  `id` int(11) UNSIGNED NOT NULL,
  `referrerId` int(11) UNSIGNED NOT NULL,
  `registeredId` int(11) UNSIGNED NOT NULL,
  `isReferrerEarned` tinyint(1) DEFAULT '0',
  `isRegisteredEarned` tinyint(1) DEFAULT '0',
  `earnedAmount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `registeredTripId` bigint(20) UNSIGNED NOT NULL,
  `referrerType` enum('N','P','D','O') COLLATE latin1_general_ci NOT NULL DEFAULT 'N' COMMENT '''N''=None,''P''=passenger,''D''=Driver,''O''=Others',
  `userType` enum('N','P','D','O') COLLATE latin1_general_ci NOT NULL DEFAULT 'N' COMMENT '''N''=None,''P''=passenger,''D''=Driver,''O''=Others',
  `status` tinyint(1) DEFAULT '1',
  `createdDatetime` datetime NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `smstemplate`
--

CREATE TABLE `smstemplate` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `createdBy` int(11) UNSIGNED NOT NULL,
  `updatedBy` int(11) UNSIGNED NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `smstemplate`
--

INSERT INTO `smstemplate` (`id`, `title`, `description`, `content`, `status`, `createdBy`, `updatedBy`, `createdDatetime`, `updatedDatetime`) VALUES
(1, 'account_create_sms', 'Your Hellocab account has been successfully created! Username: ##USERNAME## Password:##PASSWORD## Download our app on ##APPLINK##', 'Your Hellocab account has been successfully created! Username: ##USERNAME## Password:##PASSWORD## Download our app on ##APPLINK##', 1, 1, 1, '2017-04-07 00:00:00', '2017-04-07 00:00:00'),
(2, 'forgot_password_sms', 'You told us you forgot your password. Your password is: ##PASSWORD##. Let us know if any issue.', 'You told us you forgot your password. Your password is: ##PASSWORD##. Let us know if any issue.', 1, 1, 1, '2017-04-07 00:00:00', '2017-04-07 00:00:00'),
(3, 'otp', 'Please use this OTP: ##OTP##.', 'Please use this OTP: ##OTP##.', 1, 1, 1, '2017-04-07 00:00:00', '2017-04-07 00:00:00'),
(4, 'welcome', 'Your Hellocab account has been successfully created! Username: ##USERNAME##', 'Your Hellocab account has been successfully created! Username: ##USERNAME##', 1, 1, 1, '2017-04-13 00:00:00', '2017-04-13 00:00:00'),
(5, 'referral_sms', 'Use my Hellocab invite code ##REFERRALCODE##, and get a worth Rs. ##REFERRALDISCOUNT##. Download the App & redeem it! App Store:  Play Store: https://play.google.com/store/apps/details?id=com.takearightcab.customer', 'Use my Hellocab invite code ##REFERRALCODE##, and get a worth Rs. ##REFERRALDISCOUNT##. Download the App & redeem it! App Store:  Play Store: https://play.google.com/store/apps/details?id=com.takearightcab.customer', 1, 1, 1, '2017-04-17 00:00:00', '2017-04-17 00:00:00'),
(6, 'share_sms', '##USERNAME## is using a Hellocab & is currently located at ##LOCATION##. The request to share this information with you was made by ##USERNAME## through the Hellocab App.', '##USERNAME## is using a Hellocab & is currently located at ##LOCATION##. The request to share this information with you was made by ##USERNAME## through the TakeARight App.', 1, 1, 1, '2017-04-17 00:00:00', '2017-04-17 00:00:00'),
(7, 'emergency_contact', 'Please contact ##PASSENGERNAME## immediately who indicated through the Hellocab app that you should call in time of an ongoing emergency. Their current location is ##LOCATION##. Google map link\r\n ##GOOGLELINK##.', 'Please contact ##PASSENGERNAME## immediately who indicated through the Hellocab app that you should call in time of an ongoing emergency. Their current location is ##LOCATION##. Google map link\r\n ##GOOGLELINK##.', 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(8, 'drive_later_booking_sms', 'Hi ##PASSENGERNAME##,  Your booking request Job card id is ##JOBCARDID##.Your taxi will dispatch before 15 minutes of your requested pickup time ##PICKUP_TIME##.', 'Hi ##PASSENGERNAME##,  Your booking request Job card id is ##JOBCARDID##.Your taxi will dispatch before 15 minutes of your requested pickup time ##PICKUP_TIME##.', 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(9, 'drive_now_booking_sms', 'Hi ##PASSENGERNAME##,  Your booking request Job card id is ##JOBCARDID##.', 'Hi ##PASSENGERNAME##,  Your booking request Job card id is ##JOBCARDID##.', 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(10, 'passenger_taxi_confirmed_sms', 'Hi ##PASSENGERNAME##,  Your booking request Job card id is ##JOBCARDID##, CRN is ##CRN## & Taxi no is ##TAXINO##. Your taxi driver name & mobile is ##DRIVERNAME## & ##DRIVERMOBILE##.', 'Hi ##PASSENGERNAME##,  Your booking request Job card id is ##JOBCARDID##, CRN is ##CRN## & Taxi no is ##TAXINO##. Your taxi driver name & mobile is ##DRIVERNAME## & ##DRIVERMOBILE##.', 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(11, 'driver_job_confirmed_sms', 'Hi ##DRIVERNAME##,  Your booking request Job card id is ##JOBCARDID##. Your passenger name & mobile is ##PASSENGERNAME## & ##PASSENGERMOBILE##.', 'Hi ##DRIVERNAME##,  Your booking request Job card id is ##JOBCARDID##. Your passenger name & mobile is ##PASSENGERNAME## & ##PASSENGERMOBILE##.', 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(12, 'drt_promocode_sms', 'Hi ##PASSENGERNAME##,  Your request is rejected, So use compensation promocode ##PROMOCODE## value of ##PROMOVALUE## MMK valid till ##PROMOVALID##.', 'Hi ##PASSENGERNAME##,  Your request is rejected, So use compensation promocode ##PROMOCODE## value of ##PROMOVALUE## MMK valid till ##PROMOVALID##.', 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(13, 'passenger_cancelled_driver_sms', 'Your Job ##JOBCARDID## has been cancelled by Passenger.', 'Your Job ##JOBCARDID## has been cancelled by Passenger.', 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(14, 'driver_later_cancelled_sms', 'Hi ##PASSENGERNAME##,  Your booking request Job card id is ##JOBCARDID##. Cancelled due to driver not being available.', 'Hi ##PASSENGERNAME##,  Your booking request Job card id is ##JOBCARDID##. Cancelled due to driver not being available.', 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(15, 'driver_ontrip_offline_sms', 'Hi ##DRIVERNAME##,  Your offline for more than a ##MINUTE## minutes.', 'Hi ##DRIVERNAME##,  Your offline for more than a ##MINUTE## minutes.', 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00'),
(16, 'driver_later_cancelled_sms', 'Hi ##PASSENGERNAME##,  Your booking request Job card id is ##JOBCARDID##. Cancelled due to driver not being available.', 'Hi ##PASSENGERNAME##,  Your booking request Job card id is ##JOBCARDID##. Cancelled due to driver not being available.', 1, 1, 1, '2017-08-15 00:00:00', '2017-08-15 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `taxidetails`
--

CREATE TABLE `taxidetails` (
  `id` int(11) UNSIGNED NOT NULL,
  `taxiOwnerId` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `manufacturer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `colour` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `taxiCategoryType` int(11) UNSIGNED NOT NULL,
  `registrationNo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `registrationImage` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `carImage` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `carDriverImage` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seatCapacity` int(2) NOT NULL,
  `insuranceNo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `insuranceExpiryDate` date NOT NULL,
  `nextFcDate` date NOT NULL,
  `transmissionType` int(11) UNSIGNED NOT NULL,
  `minSpeed` int(3) NOT NULL,
  `maxSpeed` int(3) NOT NULL,
  `wheelTaxImage` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zoneId` int(11) UNSIGNED NOT NULL,
  `entityId` int(11) UNSIGNED NOT NULL,
  `serviceStartDate` datetime NOT NULL,
  `isAllocated` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) UNSIGNED NOT NULL,
  `updatedBy` int(11) UNSIGNED NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `taxidevicedetails`
--

CREATE TABLE `taxidevicedetails` (
  `id` int(11) UNSIGNED NOT NULL,
  `deviceCode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imeiNo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deviceStatus` int(11) UNSIGNED NOT NULL,
  `isWarranty` tinyint(1) NOT NULL DEFAULT '1',
  `purchasedDate` date NOT NULL,
  `isAllocated` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) UNSIGNED NOT NULL,
  `updatedBy` int(11) UNSIGNED NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `taxideviceinstallhistory`
--

CREATE TABLE `taxideviceinstallhistory` (
  `id` int(11) NOT NULL,
  `taxiDeviceId` int(11) UNSIGNED NOT NULL,
  `taxiId` int(11) UNSIGNED NOT NULL,
  `installedDatetime` datetime NOT NULL,
  `uninstalledDatetime` datetime NOT NULL,
  `createdBy` int(11) UNSIGNED NOT NULL,
  `updatedBy` int(11) UNSIGNED NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `taxiownerdetails`
--

CREATE TABLE `taxiownerdetails` (
  `id` int(11) UNSIGNED NOT NULL,
  `taxiOwnerCode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `firstName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `isDriver` tinyint(1) NOT NULL DEFAULT '1',
  `zoneId` int(11) UNSIGNED NOT NULL,
  `entityId` int(11) UNSIGNED NOT NULL,
  `createdBy` int(11) UNSIGNED NOT NULL,
  `updatedBy` int(11) UNSIGNED NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedDatetime` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `taxirejectedtripdetails`
--

CREATE TABLE `taxirejectedtripdetails` (
  `id` int(11) UNSIGNED NOT NULL,
  `tripId` bigint(20) UNSIGNED NOT NULL,
  `taxiId` int(11) UNSIGNED NOT NULL,
  `driverId` int(11) UNSIGNED NOT NULL,
  `passengerId` int(11) UNSIGNED NOT NULL,
  `rejectionType` int(11) UNSIGNED NOT NULL,
  `rejectionReason` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createdBy` int(11) UNSIGNED NOT NULL,
  `updatedBy` int(11) UNSIGNED NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `taxirejectedtripdetails`
--

INSERT INTO `taxirejectedtripdetails` (`id`, `tripId`, `taxiId`, `driverId`, `passengerId`, `rejectionType`, `rejectionReason`, `createdBy`, `updatedBy`, `createdDatetime`, `updatedDatetime`) VALUES
(1, 1, 1, 1, 1, 32, '', 0, 0, '2017-07-26 14:42:30', '2017-10-27 15:07:43'),
(2, 1, 0, 1, 1, 32, 'no reason', 0, 0, '2017-07-26 14:45:59', '2017-10-27 15:07:43');

-- --------------------------------------------------------

--
-- Table structure for table `taxirequestdetails`
--

CREATE TABLE `taxirequestdetails` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tripId` bigint(20) UNSIGNED NOT NULL,
  `totalAvailableDriver` int(5) NOT NULL,
  `selectedDriverList` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `selectedDriverId` int(11) UNSIGNED NOT NULL,
  `rejectedDriverList` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `taxiRequestStatus` int(11) UNSIGNED NOT NULL,
  `createdBy` int(11) UNSIGNED NOT NULL,
  `updatedBy` int(11) UNSIGNED NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `testtripdetails`
--

CREATE TABLE `testtripdetails` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `passengerId` int(11) UNSIGNED NOT NULL,
  `driverId` int(11) UNSIGNED NOT NULL,
  `taxiId` int(11) UNSIGNED NOT NULL,
  `jobCardId` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zoneId` int(11) UNSIGNED NOT NULL,
  `subZoneId` int(11) UNSIGNED NOT NULL,
  `entityId` int(11) UNSIGNED NOT NULL,
  `estimatedDistance` float(7,3) NOT NULL DEFAULT '0.000',
  `estimatedTime` int(4) NOT NULL DEFAULT '0',
  `pickupLocation` text COLLATE utf8_unicode_ci NOT NULL,
  `pickupLatitude` float(10,6) NOT NULL DEFAULT '0.000000',
  `pickupLongitude` float(10,6) NOT NULL DEFAULT '0.000000',
  `pickupDatetime` datetime NOT NULL,
  `dispatchedDatetime` datetime NOT NULL,
  `driverAcceptedDatetime` datetime NOT NULL,
  `taxiArrivedDatetime` datetime NOT NULL,
  `actualPickupDatetime` datetime NOT NULL,
  `dropLocation` text COLLATE utf8_unicode_ci NOT NULL,
  `dropLatitude` float(10,6) NOT NULL DEFAULT '0.000000',
  `dropLongitude` float(10,6) NOT NULL DEFAULT '0.000000',
  `dropDatetime` datetime NOT NULL,
  `tripStatus` int(11) UNSIGNED NOT NULL,
  `notificationStatus` int(11) UNSIGNED NOT NULL,
  `driverAcceptedStatus` int(11) UNSIGNED NOT NULL,
  `taxiCategoryType` int(11) UNSIGNED NOT NULL,
  `tripType` int(11) UNSIGNED NOT NULL,
  `paymentMode` int(11) UNSIGNED NOT NULL,
  `bookedFrom` int(11) UNSIGNED NOT NULL,
  `driverRating` decimal(2,1) NOT NULL DEFAULT '0.0',
  `driverComments` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `passengerRating` decimal(2,1) NOT NULL DEFAULT '0.0',
  `passengerComments` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `passengerRejectComments` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `landmark` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `promoCode` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `customerReferenceCode` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `isCodeVerified` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) UNSIGNED NOT NULL,
  `updatedBy` int(11) UNSIGNED NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `testtripdetails`
--

INSERT INTO `testtripdetails` (`id`, `passengerId`, `driverId`, `taxiId`, `jobCardId`, `zoneId`, `subZoneId`, `entityId`, `estimatedDistance`, `estimatedTime`, `pickupLocation`, `pickupLatitude`, `pickupLongitude`, `pickupDatetime`, `dispatchedDatetime`, `driverAcceptedDatetime`, `taxiArrivedDatetime`, `actualPickupDatetime`, `dropLocation`, `dropLatitude`, `dropLongitude`, `dropDatetime`, `tripStatus`, `notificationStatus`, `driverAcceptedStatus`, `taxiCategoryType`, `tripType`, `paymentMode`, `bookedFrom`, `driverRating`, `driverComments`, `passengerRating`, `passengerComments`, `passengerRejectComments`, `landmark`, `promoCode`, `customerReferenceCode`, `isCodeVerified`, `createdBy`, `updatedBy`, `createdDatetime`, `updatedDatetime`) VALUES
(1, 1, 0, 0, '000001', 1, 0, 0, 0.000, 0, 'banshankari', 12.987650, 76.987648, '2017-09-27 17:19:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'basvangudi', 12.987600, 76.354401, '0000-00-00 00:00:00', 47, 47, 29, 80, 45, 75, 19, '0.0', '', '0.0', '', '', 'temple', 'KIRAN', '3943', 0, 0, 0, '2017-09-27 17:18:30', '2017-09-27 17:18:30'),
(2, 1, 0, 0, 'HC000002', 1, 0, 1, 0.000, 0, 'banshankari', 12.987650, 76.987648, '2017-09-27 17:21:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'basvangudi', 12.987600, 76.354401, '0000-00-00 00:00:00', 47, 47, 29, 80, 45, 75, 19, '0.0', '', '0.0', '', '', 'temple', 'KIRAN', '5089', 0, 0, 0, '2017-09-27 17:19:56', '2017-09-27 17:19:56'),
(3, 16879, 0, 0, 'HC000011', 17, 0, 1, 1.600, 5, 'စိန်ပန်းမြိုင်လမ်း, Yangon, Myanmar (Burma)', 16.865570, 96.188049, '2017-10-21 17:52:53', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Ngar Man Aung Paya St, Yangon, Myanmar (Burma)', 16.857679, 96.190109, '0000-00-00 00:00:00', 47, 47, 0, 80, 44, 75, 20, '0.0', '', '0.0', '', '', '', '', '4865', 0, 11, 11, '2017-10-21 17:51:54', '2017-10-21 17:51:54'),
(4, 16879, 0, 0, 'HC000015', 15, 0, 1, 0.000, 0, 'Unnamed Road, Yangon, Myanmar (Burma)', 16.907579, 96.124512, '2017-11-02 14:24:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0.000000, 0.000000, '0000-00-00 00:00:00', 47, 47, 0, 80, 44, 75, 20, '0.0', '', '0.0', '', '', '', '', '1631', 0, 11, 11, '2017-11-02 14:23:05', '2017-11-02 14:23:05'),
(5, 16879, 0, 0, 'HC000016', 15, 0, 1, 0.000, 0, 'Unnamed Road, Yangon, Myanmar (Burma)', 16.907579, 96.124512, '2017-11-02 14:25:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0.000000, 0.000000, '0000-00-00 00:00:00', 47, 47, 0, 80, 44, 75, 20, '0.0', '', '0.0', '', '', '', '', '6852', 0, 11, 11, '2017-11-02 14:24:20', '2017-11-02 14:24:20'),
(6, 16879, 0, 0, 'HC000017', 32, 15, 1, 11.400, 25, 'Yangon Airport Rd, Yangon, Myanmar (Burma)', 16.900129, 96.133720, '2017-11-02 15:04:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Mya Nandar, Yangon, Myanmar (Burma)', 16.834681, 96.172943, '0000-00-00 00:00:00', 47, 47, 0, 80, 44, 75, 20, '0.0', '', '0.0', '', '', '', '', '3623', 0, 11, 11, '2017-11-02 15:03:51', '2017-11-02 15:03:52'),
(7, 16879, 0, 0, 'HC000018', 32, 15, 1, 0.000, 0, 'Yangon International Airport Terminal 1, Yangon Airport Rd, Yangon, Myanmar (Burma)', 16.900700, 96.133720, '2017-11-02 15:06:24', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0.000000, 0.000000, '0000-00-00 00:00:00', 47, 47, 0, 80, 44, 75, 20, '0.0', '', '0.0', '', '', '', '', '6015', 0, 11, 11, '2017-11-02 15:05:25', '2017-11-02 15:05:25');

-- --------------------------------------------------------

--
-- Table structure for table `tripdetails`
--

CREATE TABLE `tripdetails` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `passengerId` int(11) UNSIGNED NOT NULL,
  `driverId` int(11) UNSIGNED NOT NULL,
  `taxiId` int(11) UNSIGNED NOT NULL,
  `jobCardId` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zoneId` int(11) UNSIGNED NOT NULL,
  `parentZoneId` int(11) UNSIGNED NOT NULL,
  `entityId` int(11) UNSIGNED NOT NULL,
  `estimatedDistance` float(7,3) NOT NULL DEFAULT '0.000',
  `estimatedTime` int(4) NOT NULL DEFAULT '0',
  `pickupLocation` text COLLATE utf8_unicode_ci NOT NULL,
  `pickupLatitude` float(10,6) NOT NULL DEFAULT '0.000000',
  `pickupLongitude` float(10,6) NOT NULL DEFAULT '0.000000',
  `pickupDatetime` datetime NOT NULL,
  `dispatchedDatetime` datetime NOT NULL,
  `driverAcceptedDatetime` datetime NOT NULL,
  `taxiArrivedDatetime` datetime NOT NULL,
  `actualPickupDatetime` datetime NOT NULL,
  `dropLocation` text COLLATE utf8_unicode_ci NOT NULL,
  `dropLatitude` float(10,6) NOT NULL DEFAULT '0.000000',
  `dropLongitude` float(10,6) NOT NULL DEFAULT '0.000000',
  `dropDatetime` datetime NOT NULL,
  `tripStatus` int(11) UNSIGNED NOT NULL,
  `notificationStatus` int(11) UNSIGNED NOT NULL,
  `driverAcceptedStatus` int(11) UNSIGNED NOT NULL,
  `taxiCategoryType` int(11) UNSIGNED NOT NULL,
  `tripType` int(11) UNSIGNED NOT NULL,
  `paymentMode` int(11) UNSIGNED NOT NULL,
  `bookedFrom` int(11) UNSIGNED NOT NULL,
  `driverRating` decimal(2,1) NOT NULL DEFAULT '0.0',
  `driverComments` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `passengerRating` decimal(2,1) NOT NULL DEFAULT '0.0',
  `passengerComments` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `passengerRejectComments` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `landmark` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `promoCode` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `customerReferenceCode` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `isCodeVerified` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) UNSIGNED NOT NULL,
  `updatedBy` int(11) UNSIGNED NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `triprejectreasonlist`
--

CREATE TABLE `triprejectreasonlist` (
  `id` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `createdBy` int(11) NOT NULL,
  `updatedBy` int(11) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `triptemptrackingdetails`
--

CREATE TABLE `triptemptrackingdetails` (
  `id` int(11) NOT NULL,
  `tripId` bigint(20) NOT NULL,
  `latitude` float(10,6) NOT NULL DEFAULT '0.000000',
  `longitude` float(10,6) NOT NULL DEFAULT '0.000000',
  `createdDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `triptrackeddetails`
--

CREATE TABLE `triptrackeddetails` (
  `id` int(11) NOT NULL,
  `tripId` bigint(20) NOT NULL,
  `latitudeLongitude` text COLLATE utf8_unicode_ci NOT NULL,
  `passengerId` int(11) NOT NULL,
  `driverId` int(11) NOT NULL,
  `taxiId` int(11) NOT NULL,
  `createdDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `triptransactiondetails`
--

CREATE TABLE `triptransactiondetails` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tripId` bigint(20) UNSIGNED NOT NULL,
  `travelledDistance` float(7,3) NOT NULL DEFAULT '0.000',
  `travelledPeriod` int(11) NOT NULL COMMENT 'In minutes',
  `convenienceCharge` decimal(10,2) NOT NULL DEFAULT '0.00',
  `travelCharge` decimal(10,2) NOT NULL DEFAULT '0.00',
  `parkingCharge` decimal(10,2) NOT NULL DEFAULT '0.00',
  `tollCharge` decimal(10,2) NOT NULL DEFAULT '0.00',
  `surgeCharge` decimal(10,2) NOT NULL DEFAULT '0.00',
  `penaltyCharge` decimal(10,2) NOT NULL DEFAULT '0.00',
  `bookingCharge` decimal(10,2) NOT NULL DEFAULT '0.00',
  `taxCharge` decimal(10,2) NOT NULL DEFAULT '0.00',
  `totalTripCharge` decimal(10,2) NOT NULL DEFAULT '0.00',
  `promoDiscountAmount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `walletPaymentAmount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `adminAmount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `taxPercentage` decimal(4,2) NOT NULL DEFAULT '0.00',
  `tripType` int(11) UNSIGNED NOT NULL,
  `paymentMode` int(11) UNSIGNED NOT NULL,
  `paymentStatus` tinyint(1) NOT NULL DEFAULT '0',
  `transactionId` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `invoiceNo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `driverEarning` decimal(10,2) NOT NULL DEFAULT '0.00',
  `offlineTransaction` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) UNSIGNED NOT NULL,
  `updatedBy` int(11) UNSIGNED NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `useremergencycontacts`
--

CREATE TABLE `useremergencycontacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `userId` int(11) UNSIGNED NOT NULL,
  `userType` enum('N','P','D','O') COLLATE utf8_unicode_ci NOT NULL COMMENT '''N''=None,''P''=passenger,''D''=Driver,''O''=Others',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `createdDatetime` datetime NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `userlogindetails`
--

CREATE TABLE `userlogindetails` (
  `id` int(11) UNSIGNED NOT NULL,
  `userIdentity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `firstName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `loginEmail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `designation` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `doj` date NOT NULL COMMENT 'date of joining',
  `dor` date NOT NULL COMMENT 'date of releave',
  `profileImage` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `roleType` int(11) UNSIGNED NOT NULL,
  `zoneId` int(11) UNSIGNED NOT NULL,
  `entityId` int(11) UNSIGNED NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `createdBy` int(11) UNSIGNED NOT NULL,
  `updatedBy` int(11) UNSIGNED NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `userlogindetails`
--

INSERT INTO `userlogindetails` (`id`, `userIdentity`, `firstName`, `lastName`, `loginEmail`, `password`, `designation`, `doj`, `dor`, `profileImage`, `roleType`, `zoneId`, `entityId`, `isDeleted`, `status`, `createdBy`, `updatedBy`, `createdDatetime`, `updatedDatetime`) VALUES
(11, 'HCAB000001', 'Kiran', 'Kumar', 'kiran.k@invenzolabs.com', '8f21df3850033bf3da278329cfb1510fa799131c2a91e75ea68b5cf77745da60', 'super admin', '2017-04-09', '2017-04-09', '', 1, 1, 1, 0, 1, 1, 11, '2017-04-02 00:00:00', '2017-09-07 11:36:32'),
(12, 'HCAB000002', 'Global', 'KK', 'kiran@invenzolabs.com', '8f21df3850033bf3da278329cfb1510fa799131c2a91e75ea68b5cf77745da60', 'super admin', '2017-04-09', '2017-04-09', '', 1, 1, 1, 0, 1, 1, 11, '2017-04-02 00:00:00', '2017-09-07 11:36:32');

-- --------------------------------------------------------

--
-- Table structure for table `userpersonaldetails`
--

CREATE TABLE `userpersonaldetails` (
  `id` int(11) UNSIGNED NOT NULL,
  `userId` int(11) UNSIGNED NOT NULL,
  `gender` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `dob` date NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `communicationAddress` text COLLATE utf8_unicode_ci NOT NULL,
  `permanentAddress` text COLLATE utf8_unicode_ci NOT NULL,
  `alternateMobile` bigint(20) NOT NULL,
  `qualification` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `experience` int(11) UNSIGNED NOT NULL,
  `createdBy` int(11) UNSIGNED NOT NULL,
  `updatedBy` int(11) UNSIGNED NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedDateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `userpersonaldetails`
--

INSERT INTO `userpersonaldetails` (`id`, `userId`, `gender`, `dob`, `mobile`, `email`, `communicationAddress`, `permanentAddress`, `alternateMobile`, `qualification`, `experience`, `createdBy`, `updatedBy`, `createdDatetime`, `updatedDateTime`) VALUES
(2, 11, 'Female', '1984-04-07', 9901206631, 'globalkirankumar@gmail.com', 'Hosakerehalli,Dwarkanagar,Bangalore-560085', 'Hosakerehalli,Dwarkanagar,Bangalore-560085', 9901206631, 'Bsc.IT', 100, 1, 11, '2017-04-07 00:00:00', '2017-04-07 00:00:00'),
(3, 12, 'Female', '1984-04-07', 9901206631, 'globalkirankumar@gmail.com', 'Hosakerehalli,Dwarkanagar,Bangalore-560085', 'Hosakerehalli,Dwarkanagar,Bangalore-560085', 9901206631, 'Bsc.IT', 100, 1, 11, '2017-04-07 00:00:00', '2017-04-07 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `zonedetails`
--

CREATE TABLE `zonedetails` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `polygonPoints` text COLLATE utf8_unicode_ci NOT NULL,
  `zoneId` int(11) UNSIGNED NOT NULL,
  `countryId` int(11) UNSIGNED NOT NULL,
  `dispatchType` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) UNSIGNED NOT NULL,
  `updatedBy` int(11) UNSIGNED NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `authkeydetails`
--
ALTER TABLE `authkeydetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `last_activity` (`last_activity`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`),
  ADD KEY `createdBy` (`createdBy`),
  ADD KEY `updatedBy` (`updatedBy`);

--
-- Indexes for table `dataattributes`
--
ALTER TABLE `dataattributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `driver`
--
ALTER TABLE `driver`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `driverCode` (`driverCode`),
  ADD KEY `fk_driver_createdBy` (`createdBy`),
  ADD KEY `fk_driver_updatedBy` (`updatedBy`),
  ADD KEY `fk_driver_deviceType` (`deviceType`),
  ADD KEY `fk_driver_signupFrom` (`signupFrom`),
  ADD KEY `fk_driver_registerType` (`registerType`),
  ADD KEY `fk_driver_licenseType` (`licenseType`),
  ADD KEY `fk_driver_experience` (`experience`),
  ADD KEY `fk_driver_zoneId` (`zoneId`),
  ADD KEY `fk_driver_entityId` (`entityId`);

--
-- Indexes for table `driverdispatchdetails`
--
ALTER TABLE `driverdispatchdetails`
  ADD UNIQUE KEY `driverId` (`driverId`);

--
-- Indexes for table `driverpersonaldetails`
--
ALTER TABLE `driverpersonaldetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_driverpersonaldetails_createdBy` (`createdBy`),
  ADD KEY `fk_driverpersonaldetails_updatedBy` (`updatedBy`),
  ADD KEY `fk_driverpersonaldetails_driverId` (`driverId`);

--
-- Indexes for table `drivershifthistory`
--
ALTER TABLE `drivershifthistory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_drivershifthistory_availabilityStatus` (`availabilityStatus`),
  ADD KEY `fk_drivershifthistory_shiftStatus` (`shiftStatus`),
  ADD KEY `fk_drivershifthistory_driverId` (`driverId`);

--
-- Indexes for table `drivertaximapping`
--
ALTER TABLE `drivertaximapping`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_drivertaximapping_createdBy` (`createdBy`),
  ADD KEY `fk_drivertaximapping_updatedBy` (`updatedBy`),
  ADD KEY `fk_drivertaximapping_driverId` (`driverId`),
  ADD KEY `fk_drivertaximapping_taxiId` (`taxiId`),
  ADD KEY `fk_drivertaximapping_taxiOwnerId` (`taxiOwnerId`),
  ADD KEY `fk_drivertaximapping_taxiDeviceId` (`taxiDeviceId`);

--
-- Indexes for table `drivertransactiondetails`
--
ALTER TABLE `drivertransactiondetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_drivertransactiondetails_createdBy` (`createdBy`),
  ADD KEY `fk_drivertransactiondetails_updatedBy` (`updatedBy`),
  ADD KEY `fk_drivertransactiondetails_driverId` (`driverId`),
  ADD KEY `fk_drivertransactiondetails_referralId` (`referralId`),
  ADD KEY `fk_drivertransactiondetails_tripId` (`tripId`),
  ADD KEY `fk_drivertransactiondetails_transactionType` (`transactionType`),
  ADD KEY `fk_drivertransactiondetails_transactionMode` (`transactionMode`),
  ADD KEY `transactionFrom` (`transactionFrom`);

--
-- Indexes for table `emergencyrequestdetails`
--
ALTER TABLE `emergencyrequestdetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_emergencyrequestdetails_createdBy` (`createdBy`),
  ADD KEY `fk_emergencyrequestdetails_updatedBy` (`updatedBy`),
  ADD KEY `fk_emergencyrequestdetails_driver_userId` (`userId`),
  ADD KEY `fk_emergencyrequestdetails_emergencyStatus` (`emergencyStatus`);

--
-- Indexes for table `entityconfigdetails`
--
ALTER TABLE `entityconfigdetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_entityconfigdetails_createdBy` (`createdBy`),
  ADD KEY `fk_entityconfigdetails_updatedBy` (`updatedBy`),
  ADD KEY `fk_entityconfigdetails_entityId` (`entityId`);

--
-- Indexes for table `entitydetails`
--
ALTER TABLE `entitydetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_entitydetails_createdBy` (`createdBy`),
  ADD KEY `fk_entitydetails_updatedBy` (`updatedBy`),
  ADD KEY `fk_entitydetails_zoneId` (`zoneId`),
  ADD KEY `fk_entitydetails_entityType` (`entityType`),
  ADD KEY `fk_entitydetails_countryId` (`countryId`);

--
-- Indexes for table `locks`
--
ALTER TABLE `locks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menucontentdetails`
--
ALTER TABLE `menucontentdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `moduleaccesspermission`
--
ALTER TABLE `moduleaccesspermission`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_moduleaccesspermission_createdBy` (`createdBy`),
  ADD KEY `fk_moduleaccesspermission_updatedBy` (`updatedBy`),
  ADD KEY `fk_moduleaccesspermission_moduleId` (`moduleId`),
  ADD KEY `fk_moduleaccesspermission_roleType` (`roleType`);

--
-- Indexes for table `moduledetails`
--
ALTER TABLE `moduledetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_moduledetails_createdBy` (`createdBy`),
  ADD KEY `fk_moduledetails_updatedBy` (`updatedBy`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `passenger`
--
ALTER TABLE `passenger`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_passenger_createdBy` (`createdBy`),
  ADD KEY `fk_passenger_updatedBy` (`updatedBy`),
  ADD KEY `fk_passenger_deviceType` (`deviceType`),
  ADD KEY `fk_passenger_signupFrom` (`signupFrom`),
  ADD KEY `fk_passenger_registerType` (`registerType`),
  ADD KEY `fk_passenger_zoneId` (`zoneId`),
  ADD KEY `fk_passenger_entityId` (`entityId`);

--
-- Indexes for table `passengertransactiondetails`
--
ALTER TABLE `passengertransactiondetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_passengertransactiondetails_createdBy` (`createdBy`),
  ADD KEY `fk_passengertransactiondetails_updatedBy` (`updatedBy`),
  ADD KEY `fk_passengertransactiondetails_passengerId` (`passengerId`),
  ADD KEY `fk_passengertransactiondetails_referralId` (`referralId`),
  ADD KEY `fk_passengertransactiondetails_tripId` (`tripId`),
  ADD KEY `fk_passengertransactiondetails_transactionType` (`transactionType`),
  ADD KEY `fk_passengertransactiondetails_transactionMode` (`transactionMode`),
  ADD KEY `transactionFrom` (`transactionFrom`);

--
-- Indexes for table `promocodedetails`
--
ALTER TABLE `promocodedetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_promocodedetails_createdBy` (`createdBy`),
  ADD KEY `fk_promocodedetails_updatedBy` (`updatedBy`),
  ADD KEY `fk_promocodedetails_zoneId` (`zoneId`),
  ADD KEY `fk_promocodedetails_entityId` (`entityId`),
  ADD KEY `fk_promocodedetails_promoDiscountType` (`promoDiscountType`);

--
-- Indexes for table `ratecarddetails`
--
ALTER TABLE `ratecarddetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_ratecarddetails_createdBy` (`createdBy`),
  ADD KEY `fk_ratecarddetails_updatedBy` (`updatedBy`),
  ADD KEY `fk_ratecarddetails_zoneId` (`zoneId`),
  ADD KEY `fk_ratecarddetails_subZoneId` (`subZoneId`),
  ADD KEY `fk_ratecarddetails_entityId` (`entityId`),
  ADD KEY `fk_ratecarddetails_taxiCategoryType` (`taxiCategoryType`);

--
-- Indexes for table `ratecardslabdetails`
--
ALTER TABLE `ratecardslabdetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_ratecardslabdetails_createdBy` (`createdBy`),
  ADD KEY `fk_ratecardslabdetails_updatedBy` (`updatedBy`),
  ADD KEY `fk_ratecardslabdetails_rateCardId` (`rateCardId`),
  ADD KEY `fk_ratecardslabdetails_slabType` (`slabType`),
  ADD KEY `fk_ratecardslabdetails_slabChargeType` (`slabChargeType`);

--
-- Indexes for table `referraldetails`
--
ALTER TABLE `referraldetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_referraldetails_driver_referrerId` (`referrerId`),
  ADD KEY `fk_referraldetails_driver_registeredId` (`registeredId`),
  ADD KEY `fk_referraldetails_registeredTripId` (`registeredTripId`);

--
-- Indexes for table `smstemplate`
--
ALTER TABLE `smstemplate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_smstemplate_createdBy` (`createdBy`),
  ADD KEY `fk_smstemplate_updatedBy` (`updatedBy`);

--
-- Indexes for table `taxidetails`
--
ALTER TABLE `taxidetails`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `registrationNo` (`registrationNo`),
  ADD KEY `fk_taxidetails_createdBy` (`createdBy`),
  ADD KEY `fk_taxidetails_updatedBy` (`updatedBy`),
  ADD KEY `fk_taxidetails_taxiOwnerId` (`taxiOwnerId`),
  ADD KEY `fk_taxidetails_taxiCategoryType` (`taxiCategoryType`),
  ADD KEY `fk_taxidetails_transmissionType` (`transmissionType`),
  ADD KEY `fk_taxidetails_zoneId` (`zoneId`),
  ADD KEY `fk_taxidetails_entityId` (`entityId`);

--
-- Indexes for table `taxidevicedetails`
--
ALTER TABLE `taxidevicedetails`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `deviceCode` (`deviceCode`),
  ADD UNIQUE KEY `mobile` (`mobile`),
  ADD KEY `fk_taxidevicedetails_createdBy` (`createdBy`),
  ADD KEY `fk_taxidevicedetails_updatedBy` (`updatedBy`),
  ADD KEY `fk_taxidevicedetails_deviceStatus` (`deviceStatus`);

--
-- Indexes for table `taxideviceinstallhistory`
--
ALTER TABLE `taxideviceinstallhistory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_taxideviceinstallhistory_createdBy` (`createdBy`),
  ADD KEY `fk_taxideviceinstallhistory_updatedBy` (`updatedBy`),
  ADD KEY `fk_taxideviceinstallhistory_taxiId` (`taxiId`),
  ADD KEY `fk_taxideviceinstallhistory_taxiDeviceId` (`taxiDeviceId`);

--
-- Indexes for table `taxiownerdetails`
--
ALTER TABLE `taxiownerdetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_taxiownerdetails_createdBy` (`createdBy`),
  ADD KEY `fk_taxiownerdetails_updatedBy` (`updatedBy`),
  ADD KEY `fk_taxiownerdetails_zoneId` (`zoneId`),
  ADD KEY `fk_taxiownerdetails_entityId` (`entityId`);

--
-- Indexes for table `taxirejectedtripdetails`
--
ALTER TABLE `taxirejectedtripdetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_taxirejectedtripdetails_createdBy` (`createdBy`),
  ADD KEY `fk_taxirejectedtripdetails_updatedBy` (`updatedBy`),
  ADD KEY `fk_taxirejectedtripdetails_tripId` (`tripId`),
  ADD KEY `fk_taxirejectedtripdetails_taxiId` (`taxiId`),
  ADD KEY `fk_taxirejectedtripdetails_passengerId` (`passengerId`),
  ADD KEY `fk_taxirejectedtripdetails_driverId` (`driverId`),
  ADD KEY `fk_taxirejectedtripdetails_rejectionType` (`rejectionType`);

--
-- Indexes for table `taxirequestdetails`
--
ALTER TABLE `taxirequestdetails`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tripId` (`tripId`),
  ADD KEY `fk_taxirequestdetails_createdBy` (`createdBy`),
  ADD KEY `fk_taxirequestdetails_updatedBy` (`updatedBy`),
  ADD KEY `fk_taxirequestdetails_selectedDriverId` (`selectedDriverId`),
  ADD KEY `fk_taxirequestdetails_taxiRequestStatus` (`taxiRequestStatus`);

--
-- Indexes for table `testtripdetails`
--
ALTER TABLE `testtripdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tripdetails`
--
ALTER TABLE `tripdetails`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `jobCardId` (`jobCardId`),
  ADD KEY `fk_tripdetails_createdBy` (`createdBy`),
  ADD KEY `fk_tripdetails_updatedBy` (`updatedBy`),
  ADD KEY `fk_tripdetails_taxiId` (`taxiId`),
  ADD KEY `fk_tripdetails_passengerId` (`passengerId`),
  ADD KEY `fk_tripdetails_driverId` (`driverId`),
  ADD KEY `fk_tripdetails_zoneId` (`zoneId`),
  ADD KEY `fk_tripdetails_subZoneId` (`parentZoneId`),
  ADD KEY `fk_tripdetails_entityId` (`entityId`),
  ADD KEY `fk_tripdetails_tripStatus` (`tripStatus`),
  ADD KEY `fk_tripdetails_notificationStatus` (`notificationStatus`),
  ADD KEY `fk_tripdetails_driverAcceptedStatus` (`driverAcceptedStatus`),
  ADD KEY `fk_tripdetails_taxiCategoryType` (`taxiCategoryType`),
  ADD KEY `fk_tripdetails_tripType` (`tripType`),
  ADD KEY `fk_tripdetails_paymentMode` (`paymentMode`),
  ADD KEY `fk_tripdetails_bookedFrom` (`bookedFrom`);

--
-- Indexes for table `triprejectreasonlist`
--
ALTER TABLE `triprejectreasonlist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `triptemptrackingdetails`
--
ALTER TABLE `triptemptrackingdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `triptrackeddetails`
--
ALTER TABLE `triptrackeddetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `triptransactiondetails`
--
ALTER TABLE `triptransactiondetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_triptransactiondetails_createdBy` (`createdBy`),
  ADD KEY `fk_triptransactiondetails_updatedBy` (`updatedBy`),
  ADD KEY `fk_triptransactiondetails_tripId` (`tripId`),
  ADD KEY `fk_triptransactiondetails_tripType` (`tripType`),
  ADD KEY `fk_triptransactiondetails_paymentMode` (`paymentMode`);

--
-- Indexes for table `useremergencycontacts`
--
ALTER TABLE `useremergencycontacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_useremergencycontacts_driver_userId` (`userId`);

--
-- Indexes for table `userlogindetails`
--
ALTER TABLE `userlogindetails`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userIdentity` (`userIdentity`),
  ADD KEY `fk_userlogindetails_createdBy` (`createdBy`),
  ADD KEY `fk_userlogindetails_updatedBy` (`updatedBy`),
  ADD KEY `fk_userlogindetails_roleType` (`roleType`),
  ADD KEY `fk_userlogindetails_zoneId` (`zoneId`),
  ADD KEY `fk_userlogindetails_entityId` (`entityId`);

--
-- Indexes for table `userpersonaldetails`
--
ALTER TABLE `userpersonaldetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_userpersonaldetails_createdBy` (`createdBy`),
  ADD KEY `fk_userpersonaldetails_updatedBy` (`updatedBy`),
  ADD KEY `fk_userpersonaldetails_experience` (`experience`),
  ADD KEY `fk_userpersonaldetails_userId` (`userId`);

--
-- Indexes for table `zonedetails`
--
ALTER TABLE `zonedetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_zonedetails_createdBy` (`createdBy`),
  ADD KEY `fk_zonedetails_updatedBy` (`updatedBy`),
  ADD KEY `fk_zonedetails_zoneId` (`zoneId`),
  ADD KEY `fk_zonedetails_countryId` (`countryId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `authkeydetails`
--
ALTER TABLE `authkeydetails`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `dataattributes`
--
ALTER TABLE `dataattributes`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124;
--
-- AUTO_INCREMENT for table `driver`
--
ALTER TABLE `driver`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `driverpersonaldetails`
--
ALTER TABLE `driverpersonaldetails`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `drivershifthistory`
--
ALTER TABLE `drivershifthistory`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `drivertaximapping`
--
ALTER TABLE `drivertaximapping`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `drivertransactiondetails`
--
ALTER TABLE `drivertransactiondetails`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `emergencyrequestdetails`
--
ALTER TABLE `emergencyrequestdetails`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `entityconfigdetails`
--
ALTER TABLE `entityconfigdetails`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `entitydetails`
--
ALTER TABLE `entitydetails`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `locks`
--
ALTER TABLE `locks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `menucontentdetails`
--
ALTER TABLE `menucontentdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `moduleaccesspermission`
--
ALTER TABLE `moduleaccesspermission`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;
--
-- AUTO_INCREMENT for table `moduledetails`
--
ALTER TABLE `moduledetails`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `passenger`
--
ALTER TABLE `passenger`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `passengertransactiondetails`
--
ALTER TABLE `passengertransactiondetails`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `promocodedetails`
--
ALTER TABLE `promocodedetails`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ratecarddetails`
--
ALTER TABLE `ratecarddetails`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ratecardslabdetails`
--
ALTER TABLE `ratecardslabdetails`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `referraldetails`
--
ALTER TABLE `referraldetails`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `smstemplate`
--
ALTER TABLE `smstemplate`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `taxidetails`
--
ALTER TABLE `taxidetails`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `taxidevicedetails`
--
ALTER TABLE `taxidevicedetails`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `taxideviceinstallhistory`
--
ALTER TABLE `taxideviceinstallhistory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `taxiownerdetails`
--
ALTER TABLE `taxiownerdetails`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `taxirejectedtripdetails`
--
ALTER TABLE `taxirejectedtripdetails`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `taxirequestdetails`
--
ALTER TABLE `taxirequestdetails`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `testtripdetails`
--
ALTER TABLE `testtripdetails`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tripdetails`
--
ALTER TABLE `tripdetails`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `triprejectreasonlist`
--
ALTER TABLE `triprejectreasonlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `triptemptrackingdetails`
--
ALTER TABLE `triptemptrackingdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `triptrackeddetails`
--
ALTER TABLE `triptrackeddetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `triptransactiondetails`
--
ALTER TABLE `triptransactiondetails`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `useremergencycontacts`
--
ALTER TABLE `useremergencycontacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `userlogindetails`
--
ALTER TABLE `userlogindetails`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `userpersonaldetails`
--
ALTER TABLE `userpersonaldetails`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `zonedetails`
--
ALTER TABLE `zonedetails`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
