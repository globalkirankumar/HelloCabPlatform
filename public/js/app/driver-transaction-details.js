$(document).ready(function() {
     var now = new Date();
    var cur_date      =  [now.getFullYear(),'-',now.getMonth(),'-',now.getDate(),'--',now.getHours(),'-',now.getMinutes(),'-',now.getSeconds()].join('');
     var buttonCommon = {
        exportOptions: {
            format: {
                body: function ( data, row, column, node ) {
                    // Strip $ from salary column to make it numeric
                    return column === 5 ?
                        data.replace( /[$,]/g, '' ) :
                        data;
                }
            }
        }
    };
   var driver_id = location.href.substr(location.href.lastIndexOf('/') + 1);
	driverTransactionDependency();
	//alert('state');exit;
	//datatables
	table = $('#driver-transaction-data-table').DataTable({

		"scrollX" : true,
		"scrollY" : 400,
		"scrollCollapse" : true,
		"processing" : true, //Feature control the processing indicator.
		"serverSide" : true, //Feature control DataTables' server-side processing mode.
		"order" : [], //Initial no order.
                
                "dom": 'Bflrtip',
                "buttons": [
                            /*{
                            extend: 'copyHtml5',
                            exportOptions: {
                                columns: [ 0, ':visible' ]
                            }
                            },*/
                        {
                            extend: 'excelHtml5',
                            title: 'Drivers_Transaction_List_'+cur_date,
                            exportOptions: {
                            	columns: ':visible'
                            }
                        },
                        /*{
                            extend: 'pdfHtml5',
                            exportOptions: {
                                columns: [1, 2, 3,4,5,6,7,8,9 ]
                            }
                        },*/
                        //'colvis'
                ],
               
		// Load data for the table's content from an Ajax source
		"ajax" : {
			"url" : location.origin + '/driver/ajax_list_drivertransaction',
			"type" : "POST",
                        "data":{'driver_id':driver_id}
		},

		//Set column definition initialisation properties.s
		"columnDefs" : [ {
			"targets" : [ -1, 0, 8, 7, 9 ], //last column
			"orderable" : false, //set not orderable
		}, ],
		"lengthMenu" : [ [ 20, 50, 100 ], [ 20, 50, 100 ] ],
		"fnDrawCallback" : function(oSettings) {
			driverTransactionDependency();
		}
	});

	$('label input.toggle-vis').each(function() {
		var column = table.column($(this).attr('data-column'));
		if ($(this).prop('checked') == true) {
			//column.toggle(this.checked);
			column.visible(true);
		} else {
			column.visible(false);
		}

	});
	$('label input.toggle-vis').on('change', function(e) {
		e.preventDefault();
		// Get the column API object
		var column = table.column($(this).attr('data-column'));
		if ($(this).prop('checked') == true) {
			//column.toggle(this.checked);
			column.visible(true);
		} else {
			column.visible(false);
		}
		driverTransactionDependency();

	});
});
function driverTransactionDependency() {
        //date time picker script
	$('#save-driver-transaction-btn').click(function() {
		saveDriverTransaction();
	});
	$('#transactionMode').on('change',function(){
		claculateDriverTransaction();
	});
	$('#transactionAmount').on('blur',function(){
		claculateDriverTransaction();
	});
	$('#transactionFrom').on('change',function(){
		if($('#transactionFrom').val()==118)
		{
			$('#previousAmount').val($('#driverWallet').val());

			claculateDriverTransaction();
		}
		else if($('#transactionFrom').val()==119){
			$('#previousAmount').val($('#driverCredit').val());

			claculateDriverTransaction();
		}
		else
		{
			$('#previousAmount').val(0);
			$('#currentAmount').val(0);
		}
		
	});
	$('#cancel-driver-transaction-btn').click(function() {
		// loadUserList(); 
		//window.location.href = location.origin + '/driver/getDriverList';
                window.history.back();
	});
	
//	$('[id^=viewdriver]').click(function() {
//		var arr = $(this).attr('id').split('-');
//		var driver_id = arr[1];
//		loadDriver(driver_id, 'view');
//	});
	$('[id^=viewdrivertransaction]').click(function() {
		var arr = $(this).attr('id').split('-');
		var driver_id = arr[1];
		loadDriverTransaction(driver_id, 'view');
	});
	$('[id^=deletedriver]').click(function() {
		var arr = $(this).attr('id').split('-');
		var driver_id = arr[1];
		deleteDriver(driver_id);
	});
	
	
	    
}
function loadDriverList() {
	// Validate the passenger value again in case the user changed the input
	var url = location.origin + '/driver/getDriverList';
	jQuery.ajax({
		method : "POST",
		url : url,
	//data:{'get_type':'ajax_call'},
	}).done(function(response) {
           
		//jQuery('#content').html(response);
	});
}


function loadDriverTransaction(driver_transaction_id, view_mode) {
	//var url = location.origin + '/user/getDetailsById/' + driver_id + '/' + view_mode;
	window.location.href = location.origin + '/driver/getTransactionDetailsById/' + driver_transaction_id + '/' + view_mode;
	
}

function saveDriverTransaction() {

	var isValidate = false;
	// Validate the user details again in case the user changed the input
	var url = location.origin + '/driver/saveDriverTransaction/';

	var userData = {};
	var userData = new window.FormData($('#edit_driver_form')[0]);
	/*jQuery( jQuery('#edit_passenger_form :input').serializeArray() ).each(function( x , y ){
		passengerData[y.name] = y.value;
	});*/

	isValidate = validate('edit_driver_form');
	if (isValidate) {
		jQuery.ajax({
			xhr : function() {
				return $.ajaxSettings.xhr();
			},
			type : "POST",
			data : userData,
			cache : false,
			contentType : false,
			processData : false,
			url : url,
		}).done(
				function(response) {
					response = jQuery.parseJSON(response);
					if (response.driver_transaction_id) {
						successMessage(response.msg);
						//loadUserList();
						//window.location.href = location.origin + '/driver/getDriverList';
                                                window.history.back();
					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {
			failureMessage(response.msg);

		});
	}

}
function claculateDriverTransaction()
{
	var transaction_mode=$('#transactionMode').val();
	var transaction_amount=parseFloat($('#transactionAmount').val()).toFixed(2);
	var previous_amount=parseFloat($('#previousAmount').val()).toFixed(2);
	var current_amount=0;
	if(transaction_amount!='' && transaction_amount!=0 && transaction_mode)
	{
	if(transaction_mode==59)
	{
		current_amount=parseFloat(previous_amount)+parseFloat(transaction_amount);
		$('#currentAmount').val(current_amount);
	}
	else if(transaction_mode==60)
	{
		current_amount=parseFloat(previous_amount)-parseFloat(transaction_amount);
		$('#currentAmount').val(current_amount);
	}
	else
	{
		$('#currentAmount').val(current_amount);
	}
	}
}








