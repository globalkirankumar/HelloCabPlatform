$(document).ready(function() {
	moduleDependency();
});

function moduleDependency()
{
	$('#roleType').on('change', function(){
		var role_id=$('#roleType').val();
		if(role_id)
		{
			loadModule(role_id,'edit');
		}
		else
		{
			failureMessage("Please select role type");
		}
		
	});
	$('#save-module-access-btn').click(function() {
		saveModulePermission();
	});
	$('#cancel-module-access-btn').click(
			function() {
				window.location.href = location.origin
						+ '/Dashboard/';
			});
	
	$('input[id^="isAccess"]').click(function() {
		
		var arr = $(this).attr('id').split('-');
		var index = arr[1];
		if ($("#isAccess-"+index+":checked").length > 0)
		{
			$("#isAccess-"+index).val(1);
			$("#isCreate-"+index).prop({ disabled: false, checked: false });
			$("#isWrite-"+index).prop({ disabled: false, checked: false });
			$("#isRead-"+index).prop({ checked: true });
			$("#isRead-"+index).val(1);
			$("#isDelete-"+index).prop({ disabled: false, checked: false });
			$("#isFullAccess-"+index).prop({ disabled: false, checked: false });
		}
		else
		{
			$("#isAccess-"+index).val(0);
			$("#isCreate-"+index).prop({ disabled: true, checked: false });
			$("#isCreate-"+index).val(0);
			$("#isWrite-"+index).prop({ disabled: true, checked: false });
			$("#isWrite-"+index).val(0);
			$("#isRead-"+index).prop({ disabled: true, checked: false });
			$("#isRead-"+index).val(0);
			$("#isDelete-"+index).prop({ disabled: true, checked: false });
			$("#isDelete-"+index).val(0);
			$("#isFullAccess-"+index).prop({ disabled: true, checked: false });
			$("#isFullAccess-"+index).val(0);
			
		}
		
	});
	

}
function loadModule( role_id , view_mode)
{
    // Validate the project bhu value again in case the user changed the input
    var url = location.origin + '/Module/getDetailsById/' + view_mode;
    
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {id: role_id}
    }).done(function (response) {
        
        jQuery('#module-information').html(response.html);
        moduleDependency();
        
    });
}

function saveModulePermission() {
	if (confirm("Make sure changes save/updated ?")) {
	$('input[id^="isRead-"]').each(function () {
	$(this).removeAttr('disabled');
	});
	var url = location.origin + '/Module/saveModulePermission/';

	var moduleData = {};
	var moduleData = new FormData($('#edit_module_access_form')[0]);
	
		$("#overlay").removeClass('hidden');
		jQuery.ajax({
			xhr : function() {
				return $.ajaxSettings.xhr();
			},
			type : "POST",
			data : moduleData,
			cache : false,
			contentType : false,
			processData : false,
			url : url,
		}).done(
				function(response) {
					$("#overlay").addClass('hidden');
					response = jQuery.parseJSON(response);
					if (response.role_type) {
						successMessage(response.msg);
						window.location.href = location.origin
								+ '/Module/';
					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {
					$("#overlay").addClass('hidden');
			failureMessage(response.msg);

		});
	}
}