$(document).ready(function() {
	$('#toggle-table-columns').click(function(){
		toogleTableColumns(); 
	});	
});

function validateEmail(email) {
    var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(email);
}

function validateMobile(mobile) {
    var regex = /^[0]?[789]\d{7,9}$/;
    return regex.test(mobile);
}
function validateRegistrationNo(registrationNo) {
    var regex = /^[A-Z0-9]{2}[\s]{1}\d{1,4}$/;
    return regex.test(registrationNo);
}

function toogleTableColumns() {
	if($('#table-columns').hasClass('hidden'))
	{
		$('#table-columns').removeClass('hidden');
		$('#toggle-table-columns').html('Hide Columns List');
	}
	else
	{
		$('#table-columns').addClass('hidden');
		$('#toggle-table-columns').html('Show Columns List');
	}
}

