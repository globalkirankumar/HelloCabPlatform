function taxiDependency()
{ 
	 $('#save_taxi_btn').on('click', saveTaxi); 
	 $('#cancel_taxi_btn').on('click', loadTaxiList);
     
     var url = location.origin + '/taxi/getTaxiList/';
     
    /* $('#passenger-data-table').DataTable( {
         "processing": true,
         "serverSide": true,
         "ajax": "scripts/objects.php",
         "columns": [
             { "data": "first_name" },
             { "data": "last_name" },
             { "data": "position" },
             { "data": "office" },
             { "data": "start_date" },
             { "data": "salary" }
         ]
     } );*/
     
     $('#insuranceExpiryDate').datetimepicker({
	       dayOfWeekStart : 1,
	       lang           : 'en',
	       startDate      : '0',
	       timepicker     : false,
	       closeOnDateSelect : true,
	       format         : 'Y-m-d',
	       scrollMonth : false,
	       scrollInput : false,
	       maxDate:0
	   });
	   
	   $('#nextFcDate').datetimepicker({
	       dayOfWeekStart : 1,
	       lang           : 'en',
	       startDate      : '0',
	       timepicker     : false,
	       closeOnDateSelect : true,
	       scrollMonth : false,
	       scrollInput : false,
	       format         : 'Y-m-d',
	       minDate:0
	   });
	   $('#serviceStartDate').datetimepicker({
	       dayOfWeekStart : 1,
	       lang           : 'en',
	       startDate      : '0',
	       timepicker     : false,
	       closeOnDateSelect : true,
	       scrollMonth : false,
	       scrollInput : false,
	       format         : 'Y-m-d',
	       minDate:0
	   });
}
function viewTaxi(id){
    var arr = id.split('-');
    var taxi_id = arr[1];
    loadtaxi(taxi_id,'view');
}
function editTaxi(id){
   var arr = id.split('-');
   var taxi_id = arr[1];
   loadPassenger(taxi_id,'edit'); 
}
function transctionHistory(id){
   var arr = id.split('-');
   var taxi_id = arr[1];
   loadTransctionHistory(taxi_id); 
}


function changeTaxiStatus(id)
{
	var taxi_id=0;
	var status='';
	
	var arr = id.split('-');
    taxi_id = arr[1];
    
    if(arr[0]=='active')
    {
    	status='Y';
    }
    else if(arr[0]=='deactive')
    {
    	status='N';
    }
    var url = location.origin + '/taxi/changeTaxiStatus/';
    
    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'id':taxi_id,'status':status},
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response)
        {
        	
        	successMessage( response.msg);
        	loadtaxiList();
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	
    	failureMessage( 'Status change failed.. Please try again later.');
    	
    });
    
}

function loadTaxiList()
{
    // Validate the passenger value again in case the user changed the input
    var url = location.origin + '/taxi/getTaxiList/';
    var ctaxi_id = $('#filter_taxiId').val();
    jQuery.ajax({
        method: "POST",
        url: url,
        data:{'get_type':'ajax_call','taxi_id':taxi_id},
    }).done(function (response) {
        jQuery('#taxi-details-information').replaceWith(response);
        dataTableLoader();
    });
}

function loadTaxi( taxi_id , view_mode)
{
    var url = location.origin + '/taxi/getDetailsById/' + taxi_id + '/' + view_mode;
    window.location.href =location.origin + '/taxi/getDetailsById/' + taxi_id + '/' + view_mode;
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {id: taxi_id}
    }).done(function (response) {
        jQuery('#taxi-details-information').replaceWith(response.html);
        taxiDependency();
    });
}
function loadTransctionHistory( taxi_id )
{
    var url = location.origin + '/wallet/getWalletList/';
    
   var response =  jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {'taxi_id': taxi_id,'get_type':'ajax_call'},
        async:false
    }).responseText;
    if(response){
        jQuery('#taxi-details-information').replaceWith(response);
        dataTableLoader();
    }else{
        failureMessage('Please Try Again Later');
    }
}

function saveTaxi() {
    var isValidate=false;
 // Validate the passenger details again in case the user changed the input
    var url = location.origin + '/taxi/saveTaxi/';
    
    var taxiData = {};
    var taxiData = new window.FormData($('#edit_taxi_form')[0]);
    /*jQuery( jQuery('#edit_passenger_form :input').serializeArray() ).each(function( x , y ){
    	passengerData[y.name] = y.value;
    });*/
    isValidate=validate('edit_taxi_form');
    if(isValidate)
    {
    $('#save_taxi_btn').button('loading');
    
    jQuery.ajax({
        xhr: function () {  
            return $.ajaxSettings.xhr();
        },
        type: "POST",
        data: taxiData,
        cache: false,
        contentType: false,
        processData: false,
        url: url,
    }).done(function (response) {
          response = jQuery.parseJSON(response);
        if(response.taxi_id)
        {
        	//successMessage( response.msg);
        	//loadPassengerList();
        	window.location.href = location.origin + '/taxi/getTaxiList/';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	failureMessage( response.msg);
    	
    });
    }
    
}