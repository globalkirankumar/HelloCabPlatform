$(document).ready(function() {
	$('#resetpassword').click(function() {

		var index = $(this).index();
		$('#resetblock').eq(index).toggle().siblings('#loginblock').hide();
		$('#resetblock').addClass('animated fadeInUp');
	});

	$('#signin').click(function() {

		var index = $(this).index();
		$('#loginblock').eq(index).toggle().siblings('#resetblock').hide();
		$('#loginblock').addClass('animated fadeInUp');
	});

	$('#reset_btn').click(function() {
		resetPassword();
	});

	$('#login_btn').click(function() {
		login();
	});

});

function login() {
	var url = location.origin + '/login/userLogin/';
	
	var login_email=$('#loginEmail').val();
	var password=$('#password').val();
	if(login_email !='' && password != '' || (validateEmail(login_email) || validateMobile(login_email)))
	{
	$.ajax({
		method: "POST",
        url: url,
        data:{'loginEmail':login_email,'password':password},
		success : function(response) {
			 var response = $.parseJSON(response);
			if (response) {
				if(response.status > 0)
				{
					window.location.href = location.origin + '/Dashboard';
				}
				else if(response.status < 0)
				{
					$('#login-msg').html(response.message).show().delay(2000).fadeOut('slow');
					$('#loginEmail').val('');
					$('#password').val('');
				}
			}

		}
	});
	}
	else
	{
		$('#login-msg').html('Please enter valid Email/Mobile & Password').show().delay(2000).fadeOut('slow');
	}
}

function resetPassword() {
	var url = location.origin + '/login/resetPassword/';
	
	var reset_email=$('#resetEmail').val();
	if(reset_email !='' || validateEmail(reset_email) || validateMobile(reset_email))
	{
	$.ajax({
		method: "POST",
        url: url,
        data:{'loginEmail':reset_email},
		success : function(response) {
			var response = $.parseJSON(response);
			if (response) {
				if(response.status > 0)
				{
					$('#reset-msg').html(response.message).show().delay(2000).fadeOut('slow');
					//window.location.href = location.origin + '/login';
				}
				else if(response.status < 0)
				{
					$('#reset-msg').html(response.message).show().delay(2000).fadeOut('slow');
					$('#resetEmail').val('');
				}
			}

		}
	});
	}
	else
	{
		$('#reset-msg').html('Please enter valid Email/Mobile').show().delay(5000).fadeOut('slow');
	}
}