$(document).ready(function() {

	taxiTariffDependency();

});

function taxiTariffDependency() {
	$('#save-tariff-btn').click(function() {
		savetariff();
	});
	$('#cancel-tariff-btn').click(
			function() {
				window.location.href = location.origin
						+ '/TaxiTariff/getTaxiTariffList';
			});
	$('[id^=status]').click(function() {
		var status = $(this).attr('status');
		var arr = $(this).attr('id').split('-');
		var tariff_id = arr[1];

		changeTaxiTariffStatus(tariff_id, status);
	});
	$('[id^=viewtaxitariff]').click(function() {
		var arr = $(this).attr('id').split('-');
		var tariff_id = arr[1];
		loadTariff(tariff_id, 'view');
	});
	$('[id^=edittaxitariff]').click(function() {
		var arr = $(this).attr('id').split('-');
		var tariff_id = arr[1];
		loadTariff(tariff_id, 'edit');
	});
	$('[id^=deletetaxitariff]').click(function() {
		var arr = $(this).attr('id').split('-');
		var tariff_id = arr[1];
		deleteTaxiTariff(tariff_id);
	});
	$('[id^=surge-slabStart-]').blur(function(event) {
		
		event.preventDefault();
		var selector = $(this).closest('tr').find(".slab-count").attr(
				'name');
		var index_value = selector.match(/\d+/)[0];
		validateSurgeSlab(index_value);
	});
	$('[id^=surge-slabEnd-]').blur(function(event) {
		event.preventDefault();
		var selector = $(this).closest('tr').find(".slab-count").attr(
				'name');
		var index_value = selector.match(/\d+/)[0];
		validateSurgeSlab(index_value);
	});
	$('[id^=surge-slabChargeType-]').blur(function(event) {
		event.preventDefault();
		var selector = $(this).closest('tr').find(".slab-count").attr(
				'name');
		var index_value = selector.match(/\d+/)[0];
		validateSurgeSlab(index_value);
	});
	$('[id^=surge-slabCharge-]').blur(function(event) {
		event.preventDefault();
		var selector = $(this).closest('tr').find(".slab-count").attr(
				'name');
		var index_value = selector.match(/\d+/)[0];
		validateSurgeSlab(index_value);
	});
	$('#distance-slab-table').on(
			'click',
			"[id^='del-distance-slab-']",
			function(event) {
				event.preventDefault();
				var selector = $(this).closest('tr').find(".slab-count").attr(
						'name');
				var index_value = selector.match(/\d+/)[0];
				var previousIndexValue = parseInt(index_value, 10) - 1;
				var thisRow = $(this).closest('tr').remove();

				$('#add-distance-slab-' + previousIndexValue).removeClass(
						'hidden');
				if (previousIndexValue == 0) {
					$('#del-distance-slab-' + previousIndexValue).addClass(
							'hidden');
					$('#distance-slabEnd-' + previousIndexValue).attr('readonly',false);
				} else {
					$('#distance-slabEnd-' + previousIndexValue).val(0);

					$('#del-distance-slab-' + previousIndexValue).removeClass(
							'hidden');
					$('#distance-slabEnd-' + previousIndexValue).attr('readonly',false);
				}
				// $('#distance-slab-table').deleteRow(delete_row);
			});
	$('#time-slab-table')
			.on(
					'click',
					"[id^='del-time-slab-']",
					function(event) {
						event.preventDefault();
						var selector = $(this).closest('tr')
								.find(".slab-count").attr('name');
						var index_value = selector.match(/\d+/)[0];
						var previousIndexValue = parseInt(index_value, 10) - 1;
						var thisRow = $(this).closest('tr').remove();

						$('#add-time-slab-' + previousIndexValue).removeClass(
								'hidden');
						if (previousIndexValue == 0) {
							$('#del-time-slab-' + previousIndexValue).addClass(
									'hidden');
							$('#time-slabEnd-' + previousIndexValue).attr('readonly',false);
						} else {
							$('#time-slabEnd-' + previousIndexValue).val(0);

							$('#del-time-slab-' + previousIndexValue)
									.removeClass('hidden');
							$('#time-slabEnd-' + previousIndexValue).attr('readonly',false);
						}
						// $('#distance-slab-table').deleteRow(delete_row);
					});
	$('#surge-slab-table').on(
			'click',
			"[id^='del-surge-slab-']",
			function(event) {
				event.preventDefault();
				var selector = $(this).closest('tr').find(".slab-count").attr(
						'name');
				var index_value = selector.match(/\d+/)[0];
				var previousIndexValue = parseInt(index_value, 10) - 1;
				var thisRow = $(this).closest('tr').remove();

				$('#add-surge-slab-' + previousIndexValue)
						.removeClass('hidden');
				if (previousIndexValue == 0) {
					$('#del-surge-slab-' + previousIndexValue).addClass(
							'hidden');
				} else {

					$('#del-surge-slab-' + previousIndexValue).removeClass(
							'hidden');
				}
				timePickerDependency();
				// $('#distance-slab-table').deleteRow(delete_row);
			});
	$('#distance-slab-table')
			.on(
					'click',
					"[id^='add-distance-slab-']",
					function(event) {
						event.preventDefault();
						var selector = $(this).closest('tr')
								.find(".slab-count").attr('name');
						var index_value = selector.match(/\d+/)[0];
						var nextIndexValue = parseInt(index_value, 10) + 1;
						var current_slab_start = $(
								'#distance-slabStart-' + index_value).val();
						var current_slab_end = $(
								'#distance-slabEnd-' + index_value).val();
						var current_slab_charge = $(
								'#distance-slabCharge-' + index_value).val();
						if (index_value != 0) {
							if ((parseInt(current_slab_end, 10) < parseInt(
									current_slab_start, 10))
									&& (parseInt(current_slab_end, 10) != parseInt(
											current_slab_start, 10))) {
								infoMessage("Please Enter slab end greater than slab charge");
								return false;
							}
						}
						if ((current_slab_end > 0) && (current_slab_charge > 0 || index_value==0)) {

							var thisRow = $(this).closest('tr');
							$(thisRow).clone().insertAfter(thisRow).find(
									'input,textarea,select,button').each(
									function() {
										this.name = this.name.replace(selector
												.match(/\d+/)[0],
												nextIndexValue);
										this.id = this.id.replace(selector
												.match(/\d+/)[0],
												nextIndexValue);
									});
							$('#del-distance-slab-' + nextIndexValue)
									.removeClass('hidden');
							$('#add-distance-slab-' + index_value).addClass(
									'hidden');
							$('#del-distance-slab-' + index_value).addClass(
									'hidden');
							$('#distance-slabStart-' + nextIndexValue)
									.val(
											$(
													'#distance-slabEnd-'
															+ index_value)
													.val());
							$('#distance-slabId-' + nextIndexValue).val(-1);
							$('#distance-slabEnd-' + nextIndexValue).val(0);
							$('#distance-slabCharge-' + nextIndexValue).val('');
							$('#distance-slabEnd-' + index_value).attr('readonly',true);
						} else {
							infoMessage("Please Enter slab end & slab charge");
						}
						/*
						 * } else{ failureMessage("Slab end less than Slab
						 * start"); }
						 */
					});
	$('#time-slab-table')
			.on(
					'click',
					"[id^='add-time-slab-']",
					function(event) {
						event.preventDefault();
						var selector = $(this).closest('tr')
								.find(".slab-count").attr('name');
						var index_value = selector.match(/\d+/)[0];
						var nextIndexValue = parseInt(index_value, 10) + 1;
						var current_slab_start = $(
								'#time-slabStart-' + index_value).val();
						var current_slab_end = $('#time-slabEnd-' + index_value)
								.val();
						var current_slab_charge = $(
								'#time-slabCharge-' + index_value).val();
						if (index_value != 0) {
							if ((parseInt(current_slab_end, 10) < parseInt(
									current_slab_start, 10))
									&& (parseInt(current_slab_end, 10) != parseInt(
											current_slab_start, 10))) {
								infoMessage("Please Enter slab end greater than slab charge");
								return false;
							}
						}
						if ((current_slab_end > 0) && (current_slab_charge > 0 || index_value==0)) {
							var thisRow = $(this).closest('tr');
							$(thisRow).clone().insertAfter(thisRow).find(
									'input,textarea,select,button').each(
									function() {
										this.name = this.name.replace(selector
												.match(/\d+/)[0],
												nextIndexValue);
										this.id = this.id.replace(selector
												.match(/\d+/)[0],
												nextIndexValue);
									})
							$('#del-time-slab-' + nextIndexValue).removeClass(
									'hidden');
							$('#add-time-slab-' + index_value).addClass(
									'hidden');
							$('#del-time-slab-' + index_value).addClass(
									'hidden');
							$('#time-slabStart-' + nextIndexValue).val(
									$('#time-slabEnd-' + index_value).val());
							$('#time-slabEnd-' + nextIndexValue).val(0);
							$('#time-slabId-' + nextIndexValue).val(-1);
							$('#time-slabCharge-' + nextIndexValue).val('');
							$('#time-slabEnd-' + index_value).attr('readonly',true);
						} else {
							infoMessage("Please Enter slab end & slab charge");
						}

					});
	$('#surge-slab-table')
			.on(
					'click',
					"[id^='add-surge-slab-']",
					function(event) {
						event.preventDefault();
						var selector = $(this).closest('tr')
								.find(".slab-count").attr('name');
						var index_value = selector.match(/\d+/)[0];
						var nextIndexValue = parseInt(index_value, 10) + 1;
						var current_slab_start = $(
								'#surge-slabStart-' + index_value).val();
						var current_slab_end = $(
								'#surge-slabEnd-' + index_value).val();
						var current_slab_charge = $(
								'#surge-slabCharge-' + index_value).val();
						var current_slab_charge_type = $(
								'#surge-slabChargeType-' + index_value).val();
						if ((current_slab_start) && (current_slab_end)
								&& (current_slab_charge > 0)
								&& (current_slab_charge_type > 0)) {
							var thisRow = $(this).closest('tr');
							$(thisRow).clone().insertAfter(thisRow).find(
									'input,textarea,select,button').each(
									function() {
										this.name = this.name.replace(selector
												.match(/\d+/)[0],
												nextIndexValue);
										this.id = this.id.replace(selector
												.match(/\d+/)[0],
												nextIndexValue);
										timePickerDependency();
									})
							$('#del-surge-slab-' + nextIndexValue).removeClass(
									'hidden');
							$('#add-surge-slab-' + index_value).addClass(
									'hidden');
							$('#del-surge-slab-' + index_value).addClass(
									'hidden');
							$('#surge-slabStart-' + nextIndexValue).val('');
							$('#surge-slabId-' + nextIndexValue).val(-1);
							$('#surge-slabEnd-' + nextIndexValue).val('');
							$('#surge-slabCharge-' + nextIndexValue).val('');
							$('#surge-slabChargeType-' + nextIndexValue)
									.val('');
						} else {
							infoMessage("Please Enter slab details");
						}
					});
	timePickerDependency();

}
function timePickerDependency() {
	$("[id^='surge-slabEnd']").datetimepicker({
		lang : 'en',
		startTime : '0',
		timepicker : true,
		datepicker : false,
		closeOnDateSelect : true,
		format : 'H:i',
		scrollInput : false,
		step : 10

	});
	$("[id^='surge-slabStart']").datetimepicker(
			{
				lang : 'en',
				startTime : '0',
				timepicker : true,
				datepicker : false,
				closeOnDateSelect : true,
				format : 'H:i',
				scrollInput : false,
				step : 10,
				onShow : function(ct) {
					if ($('#promoStartDatetime').val() != '') {
						this
								.setOptions({

									minTime : jQuery("[id^='surge-slabEnd']")
											.val() ? jQuery(
											"[id^='surge-slabEnd']").val()
											: false,
								})
					}
				}
			});
}
function savetariff() {
	var isValidate = false;
	// Validate the taxi tariff details in case the user changed the input
	var url = location.origin + '/TaxiTariff/saveTaxiTariff/';

	var tariffData = {};
	var tariffData = new FormData($('#edit_taxiTariff_form')[0]);
	isValidate = validate('edit_taxiTariff_form');
	if (isValidate) {
		$("#overlay").removeClass('hidden');
		jQuery.ajax({
			xhr : function() {
				return $.ajaxSettings.xhr();
			},
			type : "POST",
			data : tariffData,
			cache : false,
			contentType : false,
			processData : false,
			url : url,
		}).done(
				function(response) {
					$("#overlay").addClass('hidden');
					response = jQuery.parseJSON(response);
					if (response.taxi_tariff_id) {
						successMessage(response.msg);
						window.location.href = location.origin
								+ '/TaxiTariff/getTaxiTariffList/';
					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {
					$("#overlay").addClass('hidden');
			failureMessage(response.msg);

		});
	}
}
function loadTariff(tariff_id, view_mode) {
	// var url = location.origin + '/user/getDetailsById/' + driver_id + '/' +
	// view_mode;
	window.location.href = location.origin + '/TaxiTariff/getDetailsById/'
			+ tariff_id + '/' + view_mode;
	/*
	 * jQuery.ajax({ method: "POST", url: url, dataType: 'json', data: {id:
	 * user_id} }).done(function (response) {
	 * jQuery('#container').html(response.html); userDependency(); });
	 */
}
function deleteTaxiTariff(tariff_id) {
	if (confirm("Make sure before deleting driver?.")) {
		var url = location.origin + '/TaxiTariff/deleteTaxiTariff/';
		$("#overlay").removeClass('hidden');
		jQuery.ajax({
			method : "POST",
			url : url,
			data : {
				'id' : tariff_id
			},
		}).done(
				function(response) {
					$("#overlay").addClass('hidden');
					response = jQuery.parseJSON(response);
					if (response.status > 0) {

						successMessage(response.msg);
						// loadDriverList();
						window.location.href = location.origin
								+ '/TaxiTariff/getTaxiTariffList';
					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {
					$("#overlay").addClass('hidden');
			failureMessage('Delete failed.. Please try again later.');

		});
	}

}
function changeTaxiTariffStatus(tariff_id, status) {
	if (confirm("Make sure before changing tariff status?.")) {
		if (status == 1) {
			status = 0;
		} else {
			status = 1;
		}

		var url = location.origin + '/TaxiTariff/changeTaxiTariffStatus/';
		$("#overlay").removeClass('hidden');
		jQuery.ajax({
			method : "POST",
			url : url,
			data : {
				'id' : tariff_id,
				'status' : status
			},
		}).done(
				function(response) {
					$("#overlay").addClass('hidden');
					response = jQuery.parseJSON(response);
					if (response.status > 0) {

						successMessage(response.msg);
						// loadUserList();
						window.location.href = location.origin
								+ '/TaxiTariff/getTaxiTariffList';
					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {
					$("#overlay").addClass('hidden');
			failureMessage('Status change failed.. Please try again later.');

		});
	}

}
function validateSurgeSlab(index_value)
{
	var surge_start=$('#surge-slabStart-' + index_value).val();
	var surge_end=$('#surge-slabEnd-' + index_value).val();
	var surge_charge=$('#surge-slabCharge-' + index_value).val();
	var surge_charge_type=$('#surge-slabChargeType-' + index_value).val();
	if(surge_start || surge_end || surge_charge || surge_charge_type)
	{
		$('#surge-slabStart-' + index_value).attr('required',true);
		$('#surge-slabEnd-' + index_value).attr('required',true);
		$('#surge-slabCharge-' + index_value).attr('required',true);
		$('#surge-slabChargeType-' + index_value).attr('required',true);
	}
	else
	{
		$('#surge-slabStart-' + index_value).attr('required',false);
		$('#surge-slabEnd-' + index_value).attr('required',false);
		$('#surge-slabCharge-' + index_value).attr('required',false);
		$('#surge-slabCharge-' + index_value).attr('pattern','\d{1,2}');
		$('#surge-slabChargeType-' + index_value).attr('required',false);
	}
}