$(document).ready(function() {
	entityDependency();
	//datatables
	table = $('#entity-data-table').DataTable({

		"scrollX" : true,
		"scrollY" : 400,
		"scrollCollapse" : true,
		"processing" : true, //Feature control the processing indicator.
		"serverSide" : true, //Feature control DataTables' server-side processing mode.
		"order" : [], //Initial no order.
		"dom": 'Bflrtip',
        
        "buttons": [
                    /*{
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: [ 0, ':visible' ]
                    }
                    },*/
                 'colvis',
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: [ 1,2, 3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34 ]
                    }
                },
                /*{
                    extend: 'pdfHtml5',
                    exportOptions: {
                    	columns: ':visible'
                    }
                },*/
                
        ],
		// Load data for the table's content from an Ajax source
		"ajax" : {
			"url" : location.origin + '/entity/ajax_list',
			"type" : "POST"
		},

		//Set column definition initialisation properties.s
		"columnDefs" : [ {
			"targets" : [ -1, 0, 23, 24, 22 ], //last column
			"orderable" : false, //set not orderable
		}, ],
		"lengthMenu" : [ [ 20, 50, 100 ], [ 20, 50, 100 ] ],
		"fnDrawCallback" : function(oSettings) {
			entityDependency();
		}
	});

	$('label input.toggle-vis').each(function() {
		var column = table.column($(this).attr('data-column'));
		if ($(this).prop('checked') == true) {
			//column.toggle(this.checked);
			column.visible(true);
		} else {
			column.visible(false);
		}

	});
	$('label input.toggle-vis').on('change', function(e) {
		e.preventDefault();
		// Get the column API object
		var column = table.column($(this).attr('data-column'));
		if ($(this).prop('checked') == true) {
			//column.toggle(this.checked);
			column.visible(true);
		} else {
			column.visible(false);
		}
		entityDependency();

	});
});

function entityDependency() {
	$('#save-entity-btn').click(function() {
		saveEntity();
	});
	$('#cancel-entity-btn').click(function() {
		// loadUserList(); 
		window.location.href = location.origin + '/entity/getEntityList';
	});
	$('[id^=status]').click(function() {
		var status = $(this).attr('status');
		var arr = $(this).attr('id').split('-');
		var entity_id = arr[1];

		changeEntityStatus(entity_id, status);
	});
	$('[id^=isverify]').click(function() {
		var status = $(this).attr('status');
		var arr = $(this).attr('id').split('-');
		var entity_id = arr[1];

		changeDriverVerifyStatus(entity_id, status);
	});
	
	$('[id^=viewEntity]').click(function() {
		var arr = $(this).attr('id').split('-');
		var entity_id = arr[1];
		loadEntity(entity_id, 'view');
	});
	$('[id^=editEntity]').click(function() {
		var arr = $(this).attr('id').split('-');
		var entity_id = arr[1];
		loadEntity(entity_id, 'edit');
	});
	$('[id^=deleteEntity]').click(function() {
		var arr = $(this).attr('id').split('-');
		var entity_id = arr[1];
		deleteEntity(entity_id);
	});
	$('#entityPrefix').blur(function(){
		
		 checkEntityPrefix(); 
	});
	
        $('#sendSms').click(function(){
            var sendsms_value = $('#sendSms:checkbox:checked').length;
            var setval ;
            if(sendsms_value > 0){
                setval = 1;
            }else{
                setval = 0;
            }
            $('#sendSms').val(setval);
        });
        $('#sentEmail').click(function(){
            var sendemail_value = $('#sentEmail:checkbox:checked').length;
            $('#sentEmail').val(sendemail_value);
        });
	
}

function loadEntityList() {
	// Validate the passenger value again in case the user changed the input
	var url = location.origin + '/entity/getEntityList';
	jQuery.ajax({
		method : "POST",
		url : url,
	//data:{'get_type':'ajax_call'},
	}).done(function(response) {
           
		//jQuery('#content').html(response);
	});
}

function loadEntity(entity_id, view_mode) {
	// var url = location.origin + '/user/getDetailsById/' + entity_id + '/' + view_mode;
	window.location.href = location.origin + '/entity/getDetailsById/'
			+ entity_id + '/' + view_mode;
	/*jQuery.ajax({
	    method: "POST",
	    url: url,
	    dataType: 'json',
	    data: {id: user_id}
	}).done(function (response) {
	    jQuery('#container').html(response.html);
	    userDependency();
	});*/
}

function saveEntity() {

	var isValidate = false;
	// Validate the user details again in case the user changed the input
	var url = location.origin + '/entity/saveEntity/';

	var userData = {};
	var userData = new window.FormData($('#edit_entity_form')[0]);
	/*jQuery( jQuery('#edit_passenger_form :input').serializeArray() ).each(function( x , y ){
		passengerData[y.name] = y.value;
	});*/

	isValidate = validate('edit_entity_form');
	if (isValidate) {
		$("#overlay").removeClass('hidden');
		jQuery.ajax({
			xhr : function() {
				return $.ajaxSettings.xhr();
			},
			type : "POST",
			data : userData,
			cache : false,
			contentType : false,
			processData : false,
			url : url,
		}).done(
				function(response) {
					$("#overlay").addClass('hidden');
					response = jQuery.parseJSON(response);
					if (response.entity_id) {
						successMessage(response.msg);
						//loadUserList();
						window.location.href = location.origin + '/entity/getEntityList';
                                                //location.reload();
                                                
					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {
					$("#overlay").addClass('hidden');
			failureMessage(response.msg);

		});
	}

}

function changeEntityStatus(entity_id, status) {
	if (confirm("Make sure before changing entity status?.")) {
		if (status == 1) {
			status = 0;
		} else {
			status = 1;
		}

		var url = location.origin + '/entity/changeEntityStatus/';
		$("#overlay").removeClass('hidden');
		jQuery.ajax({
			method : "POST",
			url : url,
			data : {
				'id' : entity_id,
				'status' : status
			},
		}).done(
				function(response) {
					$("#overlay").addClass('hidden');
					response = jQuery.parseJSON(response);
					if (response.status > 0) {

						successMessage(response.msg);
						//loadUserList();
						window.location.href = location.origin
								+ '/entity/getEntityList';
					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {
					$("#overlay").addClass('hidden');
			failureMessage('Status change failed.. Please try again later.');

		});
	}

}
function changeDriverVerifyStatus(entity_id, status) {
	if (confirm("Make sure before changing entity verify status?.")) {
		if (status == 1) {
			status = 0;
		} else {
			status = 1;
		}

		var url = location.origin + '/entity/changeDriverVerifyStatus/';

		jQuery.ajax({
			method : "POST",
			url : url,
			data : {
				'id' : entity_id,
				'status' : status
			},
		}).done(
				function(response) {
					response = jQuery.parseJSON(response);
					if (response.status > 0) {

						successMessage(response.msg);
						//loadUserList();
						window.location.href = location.origin
								+ '/entity/getEntityList';
					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {

			failureMessage('Status change failed.. Please try again later.');

		});
	}

}


function deleteEntity(entity_id) {
	if (confirm("Make sure before deleting entity?.")) {
		var url = location.origin + '/entity/deleteEntity/';
		$("#overlay").removeClass('hidden');
		jQuery.ajax({
			method : "POST",
			url : url,
			data : {
				'id' : entity_id
			},
		}).done(
				function(response) {
					$("#overlay").addClass('hidden');
					response = jQuery.parseJSON(response);
					if (response.status > 0) {

						successMessage(response.msg);
						//loadEntityList();
						window.location.href = location.origin
								+ '/entity/getEntityList';
					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {
					$("#overlay").addClass('hidden');
			failureMessage('Delete failed.. Please try again later.');

		});
	}

}
function checkEntityEmail() {
	var email = $('#email').val();

	if (validateEmail(email)) {
		if (email != '') {
			$('#save-entity-btn').prop('disabled', true);
			var url = location.origin + '/entity/checkEntityEmail/';
			$("#overlay").removeClass('hidden');
			jQuery.ajax({
				method : "POST",
				data : {
					'email' : email
				},
				url : url
			}).done(function(response) {
				$("#overlay").addClass('hidden');
				response = jQuery.parseJSON(response);
				if (response.status == 1) {
					$('#email').val('');
				}
				$('#save-entity-btn').prop('disabled', false);
				infoMessage(response.msg, 1, '#email');
			});
		}
	}
}
function checkEntityMobile() {
	var mobile = $('#mobile').val();

	if (validateMobile(mobile)) {
		if (mobile != '') {
			$('#save-entity-btn').prop('disabled', true);
			var url = location.origin + '/entity/checkEntityMobile/';
			$("#overlay").removeClass('hidden');
			jQuery.ajax({
				method : "POST",
				data : {
					'mobile' : mobile
				},
				url : url
			}).done(function(response) {
				$("#overlay").addClass('hidden');
				response = jQuery.parseJSON(response);
				if (response.status == 1) {
					$('#mobile').val('');
				}
				$('#save-entity-btn').prop('disabled', false);
				infoMessage(response.msg, 1, '#mobile');
			});
		}
	}
}
function checkEntityPrefix()
{
	var prefix=$('#entityPrefix').val();
	if(prefix != '')
	{
    var url = location.origin + '/Entity/checkEntityPrefix/'; 
    $("#overlay").removeClass('hidden');
    jQuery.ajax({
        method: "POST",
        data:{'entityPrefix':prefix},
        url: url
    }).done(function (response) {
    	$("#overlay").addClass('hidden');
    	response=jQuery.parseJSON(response);
    	if(response.status==1)
    	{
    		$('#entityPrefix').val('');
    	}
    	infoMessage(response.msg,1,'#entityPrefix');
    });
	}
	
}
