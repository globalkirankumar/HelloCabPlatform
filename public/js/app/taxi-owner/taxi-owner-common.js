function taxiOwnerDependency() {
	
	$('#save-taxiowner-btn').click(function() {
		saveTaxiOwner();
		});
		$('#cancel-taxiowner-btn').click(function() {
			// loadUserList(); 
			window.location.href = location.origin + '/taxiOwner/getTaxiOwnerList';
		});
	$('[id^=viewtaxiowner]').click(function() {
		var arr = $(this).attr('id').split('-');
		var taxiowner_id = arr[1];
		loadTaxiOwner(taxiowner_id, 'view');
	});
	$('[id^=edittaxiowner]').click(function() {
		var arr = $(this).attr('id').split('-');
		var taxiowner_id = arr[1];
		loadTaxiOwner(taxiowner_id, 'edit');
	});
	$('[id^=deletetaxiowner]').click(function() {
		var arr = $(this).attr('id').split('-');
		var taxiowner_id = arr[1];
		deleteTaxiOwner(taxiowner_id);
	});
	
	var url = location.origin + '/taxiOwner/getTaxiOwnerList/';

	/* $('#passenger-data-table').DataTable( {
	     "processing": true,
	     "serverSide": true,
	     "ajax": "scripts/objects.php",
	     "columns": [
	         { "data": "first_name" },
	         { "data": "last_name" },
	         { "data": "position" },
	         { "data": "office" },
	         { "data": "start_date" },
	         { "data": "salary" }
	     ]
	 } );*/
}
function transctionHistory(id) {
	var arr = id.split('-');
	var taxiOwner_id = arr[1];
	loadTransctionHistory(taxiOwner_id);
}

function changeTaxiOwnerStatus(id) {
	var taxiOwner_id = 0;
	var status = '';

	var arr = id.split('-');
	taxiOwner_id = arr[1];

	if (arr[0] == 'active') {
		status = 'Y';
	} else if (arr[0] == 'deactive') {
		status = 'N';
	}
	var url = location.origin + '/taxiOwner/changeTaxiOwnerStatus/';

	jQuery.ajax({
		method : "POST",
		url : url,
		data : {
			'id' : taxiOwner_id,
			'status' : status
		},
	}).done(function(response) {
		response = jQuery.parseJSON(response);
		if (response) {

			successMessage(response.msg);
			loadTaxiOwnerList();
		} else {
			failureMessage(response.msg);
		}

	}).fail(function(jqXHR, textStatus, errorThrown) {

		failureMessage('Status change failed.. Please try again later.');

	});

}

function loadTaxiOwnerList() {
	// Validate the passenger value again in case the user changed the input
	var url = location.origin + '/taxiOwner/getTaxiOwnerList/';
	jQuery.ajax({
		method : "POST",
		url : url,
	//data:{'get_type':'ajax_call'},
	}).done(function(response) {
		//jQuery('#content').html(response);
	});
}

function loadTaxiOwner(taxiOwner_id, view_mode) {
	var url = location.origin + '/taxiOwner/getDetailsById/' + taxiOwner_id
			+ '/' + view_mode;
	window.location.href = location.origin + '/taxiOwner/getDetailsById/'
			+ taxiOwner_id + '/' + view_mode;
	jQuery.ajax({
		method : "POST",
		url : url,
		dataType : 'json',
		data : {
			id : taxiOwner_id
		}
	}).done(function(response) {
		jQuery('#taxiOwner-details-information').replaceWith(response.html);
		TaxiOwnerDependency();
	});
}
function loadTransctionHistory(taxiOwner_id) {
	var url = location.origin + '/wallet/getWalletList/';

	var response = jQuery.ajax({
		method : "POST",
		url : url,
		dataType : 'json',
		data : {
			'taxiOwner_id' : taxiOwner_id,
			'get_type' : 'ajax_call'
		},
		async : false
	}).responseText;
	if (response) {
		jQuery('#taxiOwner-details-information').replaceWith(response);
		dataTableLoader();
	} else {
		failureMessage('Please Try Again Later');
	}
}

function saveTaxiOwner() {
	var isValidate = false;
	// Validate the passenger details again in case the user changed the input
	var url = location.origin + '/taxiOwnerDetails/saveTaxiOwner/';

	var taxiOwnerData = {};
	var taxiOwnerData = new window.FormData($('#edit_taxiowner_form')[0]);
	/*jQuery( jQuery('#edit_passenger_form :input').serializeArray() ).each(function( x , y ){
		passengerData[y.name] = y.value;
	});*/
	isValidate = validate('edit_taxiowner_form');
	if (isValidate) {
		$('#save_taxiOwner_btn').button('loading');

		jQuery.ajax({
			xhr : function() {
				return $.ajaxSettings.xhr();
			},
			type : "POST",
			data : taxiOwnerData,
			cache : false,
			contentType : false,
			processData : false,
			url : url,
		}).done(
				function(response) {
					response = jQuery.parseJSON(response);
					if (response.taxiOwner_id) {
						//successMessage( response.msg);
						//loadPassengerList();
						window.location.href = location.origin
								+ '/taxiOwnerDetails/getTaxiOwnerList/';
					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {
			failureMessage(response.msg);

		});
	}

}