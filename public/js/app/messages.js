function successMessage( message , isSection , context ){
    doShowMessage( message , 'alert-success' , isSection , context );
}

function infoMessage( message , isSection , context ){
    doShowMessage( message , 'alert-info' , isSection , context );
}

function failureMessage( message , isSection , context ){
    doShowMessage( message , 'alert-danger' , isSection , context );
}
function warningMessage( message , isSection , context ){
    doShowMessage( message , 'alert-warning' , isSection , context );
}

function doShowMessage( message , classname , isSection , context )
{

    if( typeof(isSection) == 'undefined'){
        isSection = 0;
    }
    if( typeof(context) == 'undefined'){
        context = null;
    }
    if( isSection == 1 ){

        var section_context = jQuery(context);

        if( section_context.hasClass('section-container')){
            var section_container = section_context;
        }
        else{
        	var section_container = section_context.parents('div.section-container');
        }
        var section_message = section_container.find('div.section-message');



        var html =  '<div class="alert alert-dismissible '+classname+'"  role="alert">' +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
            '<span id="app-message">'+message+'</span></div>';
        
        $('html, body').animate({
            scrollTop: (section_container.offset().top - 125 )  // 125 pixels to compensate for the header
        }, 400 );  // Scroll to the .section-container div
        section_message.html( html ).show(0).delay(5000).fadeOut('slow');   // wait 5 seconds, then fade out the sectional message

    }else{
        jQuery('html, body').animate({ scrollTop: 0 }, 400 );   // Scroll to the top of the screen
        jQuery('#controller-messages div.alert').removeClass('alert-success,alert-info,alert-warning,alert-error').addClass( classname );
        jQuery('#app-message').html( message );
        jQuery('#controller-messages').show().delay(5000).fadeOut('slow');
    }
}