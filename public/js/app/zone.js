$(document).ready(function() {
    var now = new Date();
    var cur_date      =  [now.getFullYear(),'-',now.getMonth(),'-',now.getDate(),'--',now.getHours(),'-',now.getMinutes(),'-',now.getSeconds()].join('');
	zoneDependency();
	//datatables
	table = $('#zone-data-table').DataTable({

		"scrollX" : true,
		"scrollY" : 400,
		"scrollCollapse" : true,
		"processing" : true, //Feature control the processing indicator.
		"serverSide" : true, //Feature control DataTables' server-side processing mode.
		"order" : [], //Initial no order.
"dom": 'Bflrtip',
        
        "buttons": [
                    /*{
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: [ 0, ':visible' ]
                    }
                    },*/
                // 'colvis',
			     
                {
                    extend: 'excelHtml5',
                     title: 'Zone_List_'+cur_date,
                    exportOptions: {
                        columns: [ 1,2, 3,4,5,6 ]
                    }
                },
                /*{
                    extend: 'pdfHtml5',
                    exportOptions: {
                    	columns: ':visible'
                    }
                },*/
                
        ],
		// Load data for the table's content from an Ajax source
		"ajax" : {
			"url" : location.origin + '/zone/ajax_list',
			"type" : "POST"
		},

		//Set column definition initialisation properties.s
		"columnDefs" : [ {
			"targets" : [ -1, 0, 4, 5, 3 ], //last column
			"orderable" : false, //set not orderable
		}, ],
		"lengthMenu" : [ [ 20, 50, 100 ], [ 20, 50, 100 ] ],
		"fnDrawCallback" : function(oSettings) {
			zoneDependency();
		}
	});
        

	$('label input.toggle-vis').each(function() {
		var column = table.column($(this).attr('data-column'));
		if ($(this).prop('checked') == true) {
			//column.toggle(this.checked);
			column.visible(true);
		} else {
			column.visible(false);
		}

	});
	$('label input.toggle-vis').on('change', function(e) {
		e.preventDefault();
		// Get the column API object
		var column = table.column($(this).attr('data-column'));
		if ($(this).prop('checked') == true) {
			//column.toggle(this.checked);
			column.visible(true);
		} else {
			column.visible(false);
		}
		zoneDependency();

	});
});

function zoneDependency() {
	$('#save-zone-btn').click(function() {
		saveZone();
	});
	$('#cancel-zone-btn').click(function() {
		// loadUserList(); 
		window.location.href = location.origin + '/zone/getZoneList';
	});
	$('[id^=status]').click(function() {
		var status = $(this).attr('status');
		var arr = $(this).attr('id').split('-');
		var zone_id = arr[1];

		changeEntityStatus(zone_id, status);
	});
	
	  $('[id^=viewzone]').click(function() {
		var arr = $(this).attr('id').split('-');
		var zone_id = arr[1];
		loadZone(zone_id, 'view');
	});
	$('[id^=editzone]').click(function() {
		var arr = $(this).attr('id').split('-');
		var zone_id = arr[1];
		loadZone(zone_id, 'edit');
	});
	$('[id^=deletezone]').click(function() {
		var arr = $(this).attr('id').split('-');
		var zone_id = arr[1];
		deleteZone(zone_id);
	});
		
	
	
	$('#mobile').blur(function() {

		checkEntityMobile();
	});
	$('#email').blur(function() {

		checkZoneEmail();
	});

	/*$('#dob').datetimepicker({
		dayOfWeekStart : 1,
		lang : 'en',
		startDate : '0',
		timepicker : false,
		closeOnDateSelect : true,
		format : 'Y-m-d',
		scrollMonth : false,
		scrollInput : false,
		maxDate : 0
	});

	$('#licenseExpiryDate').datetimepicker({
		dayOfWeekStart : 1,
		lang : 'en',
		startDate : '',
		timepicker : false,
		closeOnDateSelect : true,
		scrollMonth : false,
		scrollInput : false,
		format : 'Y-m-d',
		minDate : 0
	});*/
}

function loadEntityList() {
	// Validate the passenger value again in case the user changed the input
	var url = location.origin + '/zone/getZoneList';
	jQuery.ajax({
		method : "POST",
		url : url,
	//data:{'get_type':'ajax_call'},
	}).done(function(response) {
           
		//jQuery('#content').html(response);
	});
}

function loadZone(zone_id, view_mode) {
	// var url = location.origin + '/user/getDetailsById/' + zone_id + '/' + view_mode;
	window.location.href = location.origin + '/zone/getDetailsById/'
			+ zone_id + '/' + view_mode;
	/*jQuery.ajax({
	    method: "POST",
	    url: url,
	    dataType: 'json',
	    data: {id: user_id}
	}).done(function (response) {
	    jQuery('#container').html(response.html);
	    userDependency();
	});*/
}
function deleteZone(zone_id) {
	if (confirm("Make sure before deleting zone?.")) {
		var url = location.origin + '/zone/deletezone/';
		$("#overlay").removeClass('hidden');
		jQuery.ajax({
			method : "POST",
			url : url,
			data : {
				'id' : zone_id
			},
		}).done(
				function(response) {
					$("#overlay").addClass('hidden');
					response = jQuery.parseJSON(response);
					if (response.status > 0) {

						successMessage(response.msg);
						//loadEntityList();
						window.location.href = location.origin
								+ '/entity/getEntityList';
					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {
					$("#overlay").addClass('hidden');
			failureMessage('Delete failed.. Please try again later.');

		});
	}

}
function saveZone() {

	var isValidate = false;
	// Validate the user details again in case the user changed the input
	var url = location.origin + '/zone/saveZone/';

	var userData = {};
	var userData = new window.FormData($('#edit_zone_form')[0]);
	/*jQuery( jQuery('#edit_passenger_form :input').serializeArray() ).each(function( x , y ){
		passengerData[y.name] = y.value;
	});*/

	isValidate = validate('edit_zone_form');
	if (isValidate) {
		$("#overlay").removeClass('hidden');
		jQuery.ajax({
			xhr : function() {
				return $.ajaxSettings.xhr();
			},
			type : "POST",
			data : userData,
			cache : false,
			contentType : false,
			processData : false,
			url : url,
		}).done(
				function(response) {
					$("#overlay").addClass('hidden');
					response = jQuery.parseJSON(response);
					if (response.zone_id) {
						successMessage(response.msg);
						//loadUserList();
						window.location.href = location.origin + '/zone/getZoneList';
                                                
					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {
					$("#overlay").addClass('hidden');
			failureMessage(response.msg);

		});
	}

}

function changeEntityStatus(zone_id, status) {
	if (confirm("Make sure before changing zone status?.")) {
		if (status == 1) {
			status = 0;
		} else {
			status = 1;
		}

		var url = location.origin + '/zone/changeZoneStatus/';
		$("#overlay").removeClass('hidden');
		jQuery.ajax({
			method : "POST",
			url : url,
			data : {
				'id' : zone_id,
				'status' : status
			},
		}).done(
				function(response) {
					$("#overlay").addClass('hidden');
					response = jQuery.parseJSON(response);
					if (response.status > 0) {

						successMessage(response.msg);
						//loadUserList();
						window.location.href = location.origin
								+ '/zone/getZoneList';
					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {
					$("#overlay").addClass('hidden');
			failureMessage('Status change failed.. Please try again later.');

		});
	}

}
function changeDriverVerifyStatus(zone_id, status) {
	if (confirm("Make sure before changing entity verify status?.")) {
		if (status == 1) {
			status = 0;
		} else {
			status = 1;
		}

		var url = location.origin + '/zone/changeDriverVerifyStatus/';
		$("#overlay").removeClass('hidden');
		jQuery.ajax({
			method : "POST",
			url : url,
			data : {
				'id' : zone_id,
				'status' : status
			},
		}).done(
				function(response) {
					$("#overlay").addClass('hidden');
					response = jQuery.parseJSON(response);
					if (response.status > 0) {

						successMessage(response.msg);
						//loadUserList();
						window.location.href = location.origin
								+ '/zone/getZoneList';
					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {
					$("#overlay").addClass('hidden');
			failureMessage('Status change failed.. Please try again later.');

		});
	}

}


function deleteZone(zone_id) {
	if (confirm("Make sure before deleting zone?.")) {
		var url = location.origin + '/zone/deleteZone/';
		$("#overlay").removeClass('hidden');
		jQuery.ajax({
			method : "POST",
			url : url,
			data : {
				'id' : zone_id
			},
		}).done(
				function(response) {
					$("#overlay").addClass('hidden');
					response = jQuery.parseJSON(response);
					if (response.status > 0) {

						successMessage(response.msg);
						//loadEntityList();
						window.location.href = location.origin
								+ '/zone/getZoneList';
					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {
					$("#overlay").addClass('hidden');
			failureMessage('Delete failed.. Please try again later.');

		});
	}

}
function checkZoneEmail() {
	var email = $('#email').val();

	if (validateEmail(email)) {
		if (email != '') {
			$('#save-zone-btn').prop('disabled', true);
			var url = location.origin + '/zone/checkZoneEmail/';
			jQuery.ajax({
				method : "POST",
				data : {
					'email' : email
				},
				url : url
			}).done(function(response) {
				response = jQuery.parseJSON(response);
				if (response.status == 1) {
					$('#email').val('');
				}
				$('#save-zone-btn').prop('disabled', false);
				infoMessage(response.msg, 1, '#email');
			});
		}
	}
}
function checkEntityMobile() {
	var mobile = $('#mobile').val();

	if (validateMobile(mobile)) {
		if (mobile != '') {
			$('#save-zone-btn').prop('disabled', true);
			var url = location.origin + '/zone/checkEntityMobile/';
			jQuery.ajax({
				method : "POST",
				data : {
					'mobile' : mobile
				},
				url : url
			}).done(function(response) {
				response = jQuery.parseJSON(response);
				if (response.status == 1) {
					$('#mobile').val('');
				}
				$('#save-zone-btn').prop('disabled', false);
				infoMessage(response.msg, 1, '#mobile');
			});
		}
	}
}

