$(document).ready(function() {
	userDependency();
    //datatables
    table = $('#user-data-table').DataTable({ 
 
    	/*"initComplete": function () {
            this.api().columns().every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        },*/
    	 "scrollX": true,
    	 "scrollY": 400,
    	"scrollCollapse": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
"dom": 'Bflrtip',
        
        "buttons": [
                    /*{
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: [ 0, ':visible' ]
                    }
                    },*/
                 'colvis',
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdfHtml5',
                    exportOptions: {
                    	columns: ':visible'
                    }
                },
                
        ],
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": location.origin +'/user/ajax_list',
            "type": "POST"
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1,0,16], //last column
            "orderable": false, //set not orderable
        },
        
        ],
        "lengthMenu": [[20, 50, 100], [20, 50, 100]],
        "fnDrawCallback": function( oSettings ) {
            userDependency();
          }
 
    });
    $('label input.toggle-vis').each(function(){
    	var column = table.column( $(this).attr('data-column') );
        if($(this).prop('checked')==true)
        {
        	//column.toggle(this.checked);
        	column.visible(true);
        }
        else
        {
        	column.visible( false);
        }
    });
    $('label input.toggle-vis').on( 'change', function (e) {
        e.preventDefault();
        // Get the column API object
        var column = table.column( $(this).attr('data-column') );
        if($(this).prop('checked')==true)
        {
        	//column.toggle(this.checked);
        	column.visible(true);
        }
        else
        {
        	column.visible( false);
        }
        userDependency();
    });
    
});

function userDependency()
{
	$('#change-password-btn').click(function(){
		changeUserPassword(); 
	});
	$('#save-user-btn').click(function(){
		 saveUser(); 
	});
	$('#cancel-user-btn').click(function(){
		// loadUserList(); 
		 window.location.href = location.origin + '/user/getUserList';
	});
	$('[id^=status]').click(function(){
		var status=$(this).attr('status');
		var arr = $(this).attr('id').split('-');
	    var user_id = arr[1];
	    changeUserStatus(user_id,status); 
	});
	$('[id^=viewuser]').click(function(){
		var arr = $(this).attr('id').split('-');
	    var user_id = arr[1];
		 loadUser(user_id,'view'); 
	});
	$('[id^=edituser]').click(function(){
		var arr = $(this).attr('id').split('-');
	    var user_id = arr[1];
		 loadUser(user_id,'edit'); 
	});
	$('[id^=deleteuser]').click(function(){
		var arr = $(this).attr('id').split('-');
	    var user_id = arr[1];
	    deleteUser(user_id);
	});
	$('#mobile').blur(function(){
		
		 checkUserMobile(); 
	});
	$('#loginEmail').blur(function(){
		
		checkUserLoginEmail(); 
	});

	var now = new Date();
    var currentDateTime = [
        now.getFullYear()-18,
        '/',
        now.getMonth(),
        '/',
        now.getDate(),
      ].join('');

 
	$('#dob').datetimepicker({
		dayOfWeekStart : 1,
		lang : 'en',
		timepicker : false,
		closeOnDateSelect : true,
		format : 'Y-m-d',
		scrollMonth : false,
		scrollInput : false,
		startDate:currentDateTime,
		maxDate:currentDateTime
		
	});
	   
	   $('#doj').datetimepicker({
	       dayOfWeekStart : 1,
	       lang           : 'en',
	       startDate      : '',
	       timepicker     : false,
	       closeOnDateSelect : true,
	       scrollMonth : false,
	       scrollInput : false,
	       format         : 'Y-m-d'
	    	  
	       
	   });
	   $('#dor').datetimepicker({
	       dayOfWeekStart : 1,
	       lang           : 'en',
	       startDate      : '',
	       timepicker     : false,
	       closeOnDateSelect : true,
	       scrollMonth : false,
	       scrollInput : false,
	       format         : 'Y-m-d',
           onShow:function( ct ){
               if($('#doj').val()!='') {
                   this.setOptions({
                    minDate:jQuery('#doj').val()?jQuery('#doj').val():false,
                   })
               }
              }
	   });
	   
}

function loadUserList()
{
    // Validate the passenger value again in case the user changed the input
    var url = location.origin + '/user/getUserList';
    jQuery.ajax({
        method: "POST",
        url: url,
        //data:{'get_type':'ajax_call'},
    }).done(function (response) {
        //jQuery('#content').html(response);
    });
}

function loadUser( user_id , view_mode)
{
   // var url = location.origin + '/user/getDetailsById/' + user_id + '/' + view_mode;
    window.location.href =location.origin + '/user/getDetailsById/' + user_id + '/' + view_mode;
    /*jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {id: user_id}
    }).done(function (response) {
        jQuery('#container').html(response.html);
        userDependency();
    });*/
}

function saveUser() {
	
    var isValidate=false;
 // Validate the user details again in case the user changed the input
    var url = location.origin + '/user/saveUser/';
    
    var userData = {};
    var userData = new window.FormData($('#edit_user_form')[0]);
    /*jQuery( jQuery('#edit_passenger_form :input').serializeArray() ).each(function( x , y ){
    	passengerData[y.name] = y.value;
    });*/
   
    isValidate=validate('edit_user_form');
    if(isValidate)
    {
    	$("#overlay").removeClass('hidden');
    jQuery.ajax({
        xhr: function () {  
            return $.ajaxSettings.xhr();
        },
        type: "POST",
        data: userData,
        cache: false,
        contentType: false,
        processData: false,
        url: url,
    }).done(function (response) {
    	$("#overlay").addClass('hidden');
          response = jQuery.parseJSON(response);
        if(response.user_id)
        {
        	successMessage( response.msg);
        	//loadUserList();
        	window.location.href = location.origin + '/user/getUserList';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	$("#overlay").addClass('hidden');
    	failureMessage( response.msg);
    	
    });
    }
    
}
function changeUserPassword() {
	
    var isValidate=false;
 // Validate the user details again in case the user changed the input
    var url = location.origin + '/user/changeUserPassword/';
    
    var userData = {};
    var userData = new window.FormData($('#edit_change_password_form')[0]);
    /*jQuery( jQuery('#edit_passenger_form :input').serializeArray() ).each(function( x , y ){
    	passengerData[y.name] = y.value;
    });*/
   
    isValidate=validate('edit_change_password_form');
    if(isValidate)
    {
    	$("#overlay").removeClass('hidden');
    jQuery.ajax({
        xhr: function () {  
            return $.ajaxSettings.xhr();
        },
        type: "POST",
        data: userData,
        cache: false,
        contentType: false,
        processData: false,
        url: url,
    }).done(function (response) {
    	$("#overlay").addClass('hidden');
          response = jQuery.parseJSON(response);
        if(response.status > 0)
        {
        	successMessage( response.msg);
        	//loadUserList();
        	window.location.href = location.origin + '/dashboard/';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	$("#overlay").addClass('hidden');
    	failureMessage( response.msg);
    	
    });
    }
    
}
function changeUserStatus(user_id,status)
{
	if (confirm("Make sure before changing user status?."))
    {
    if(status==1)
    {
    	status=0;
    }
    else
    {
    	status=1;
    }
    
    var url = location.origin + '/user/changeUserStatus/';
    $("#overlay").removeClass('hidden');
    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'id':user_id,'status':status},
    }).done(function (response) {
    	$("#overlay").addClass('hidden');
    	response=jQuery.parseJSON(response);
    	if(response.status > 0)
        {
        	
        	successMessage( response.msg);
        	//loadUserList();
        	window.location.href = location.origin + '/user/getUserList';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	$("#overlay").addClass('hidden');
    	failureMessage( 'Status change failed.. Please try again later.');
    	
    });
    }
    
}

function deleteUser(user_id)
{
	if (confirm("Make sure before deleting user?."))
    {
    var url = location.origin + '/user/deleteUser/';
    $("#overlay").removeClass('hidden');
    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'id':user_id},
    }).done(function (response) {
    	$("#overlay").addClass('hidden');
    	response=jQuery.parseJSON(response);
    	if(response.status > 0)
        {
        	
        	successMessage( response.msg);
        	//loadUserList();
        	window.location.href = location.origin + '/user/getUserList';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	$("#overlay").addClass('hidden');
    	failureMessage( 'Delete failed.. Please try again later.');
    	
    });
    }
    
}
function checkUserLoginEmail()
{
	var email=$('#loginEmail').val();
	var user_id=$('#user_id').val();
	if (validateEmail(email)) {
	if(email != '')
	{
		$('#save-user-btn').prop('disabled', true);
    var url = location.origin + '/user/checkUserLoginEmail/'; 
    $("#overlay").removeClass('hidden');
    jQuery.ajax({
        method: "POST",
        data:{'email':email,'user_id':user_id},
        url: url
    }).done(function (response) {
    	$("#overlay").addClass('hidden');
    	response=jQuery.parseJSON(response);
    	if(response.status==1)
    	{
    		$('#loginEmail').val('');
    	}
    	$('#save-user-btn').prop('disabled', false);
    	infoMessage(response.msg,1,'#loginEmail');
    });
	}
	}
}
function checkUserMobile()
{
	var mobile=$('#mobile').val();
	var user_id=$('#user_id').val();
	if (validateMobile(mobile)) {
	if(mobile != '')
	{
		$('#save-user-btn').prop('disabled', true);
    var url = location.origin + '/user/checkUserMobile/'; 
    $("#overlay").removeClass('hidden');
    jQuery.ajax({
        method: "POST",
        data:{'mobile':mobile,'user_id':user_id},
        url: url
    }).done(function (response) {
    	$("#overlay").addClass('hidden');
    	response=jQuery.parseJSON(response);
    	if(response.status==1)
    	{
    		$('#mobile').val('');
    	}
    	$('#save-user-btn').prop('disabled', false);
    	infoMessage(response.msg,1,'#mobile');
    });
	}
	}
}
