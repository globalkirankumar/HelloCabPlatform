$(document).ready(
		function() {
			var now = new Date();
			var cur_date = [ now.getFullYear(), '-', now.getMonth(), '-',
					now.getDate(), '--', now.getHours(), '-', now.getMinutes(),
					'-', now.getSeconds() ].join('');
			var buttonCommon = {
				exportOptions : {
					format : {
						body : function(data, row, column, node) {
							// Strip $ from salary column to make it numeric
							return column === 5 ? data.replace(/[$,]/g, '')
									: data;
						}
					}
				}
			};
			driverDependency();
			// alert('state');exit;
			// datatables
			table = $('#driver-data-table').DataTable(
					{

						"scrollX" : true,
						"scrollY" : 400,
						"scrollCollapse" : true,
						"processing" : true, // Feature control the processing indicator.
						"serverSide" : true, // Feature control DataTables' server-side
						// processing mode.
						"order" : [], // Initial no order.

						"dom" : 'CBflrtip',
						
						"buttons" : [
								/*
								 * { extend: 'copyHtml5', exportOptions: { columns: [ 0,
								 * ':visible' ] } },
								 */
								'colvis',
								/*{
									extend : 'colvis',
									text : 'Column Visibility',
									columns : ':not(:first-child)',
								},*/
								{
									extend : 'excelHtml5',
									title : 'Drivers_List_' + cur_date,
									exportOptions : {
										columns: ':visible'
										/*columns : [ 2, 3, 4, 5, 6, 7, 8, 9, 10,
												11, 12, 13, 14, 15, 16, 17, 19,
												20, 21, 22, 23, 24 ]*/
									}
								},
						/*
						 * { extend: 'pdfHtml5', exportOptions: { columns: [1, 2,
						 * 3,4,5,6,7,8,9 ] } },
						 */

						],
						// Load data for the table's content from an Ajax source
						"ajax" : {
							"url" : location.origin + '/driver/ajax_list',
							"type" : "POST"
						},

						// Set column definition initialisation properties.s
						"columnDefs" : [ {
							"targets" : [ -1, 0, 23, 24, 22 ], // last column
							"orderable" : false, // set not orderable
						}, ],
						"lengthMenu" : [ [ 20, 50, 100,-1 ], [ 20, 50, 100,'All' ] ],
						"fnDrawCallback" : function(oSettings) {
							driverDependency();
						},
						
					});

			$('label input.toggle-vis').each(function() {
				var column = table.column($(this).attr('data-column'));
				if ($(this).prop('checked') == true) {
					// column.toggle(this.checked);
					column.visible(true);
				} else {
					column.visible(false);
				}

			});
			$('label input.toggle-vis').on('change', function(e) {
				e.preventDefault();
				// Get the column API object
				var column = table.column($(this).attr('data-column'));
				if ($(this).prop('checked') == true) {
					// column.toggle(this.checked);
					column.visible(true);
				} else {
					column.visible(false);
				}
				driverDependency();

			});
		});

function driverDependency() {
	// date time picker script
	
	$('#save-driver-btn').click(function() {
		saveDriver();
	});
	$('#cancel-driver-btn').click(function() {
		// loadUserList();
		window.location.href = location.origin + '/driver/getDriverList';
	});
	$('[id^=status]').click(function() {
		var status = $(this).attr('status');
		var arr = $(this).attr('id').split('-');
		var driver_id = arr[1];

		changeDriverStatus(driver_id, status);
	});
	$('[id^=resign]').click(function() {
		var arr = $(this).attr('id').split('-');
		var driver_id = arr[1];
		resignDriver(driver_id);
	});
	$('[id^=isverify]').click(function() {
		var status = $(this).attr('status');
		var arr = $(this).attr('id').split('-');
		var driver_id = arr[1];

		changeDriverVerifyStatus(driver_id, status);
	});
	$('[id^=logstatus]').click(function() {
		var status = $(this).attr('status');
		var arr = $(this).attr('id').split('-');
		var driver_id = arr[1];

		changeDriverLogStatus(driver_id, status);
	});
	
	$('[id^=viewdriver]').click(function() {
		var arr = $(this).attr('id').split('-');
		var driver_id = arr[1];
		loadDriver(driver_id, 'view');
	});
	$('[id^=editdriver]').click(function() {
		var arr = $(this).attr('id').split('-');
		var driver_id = arr[1];
		loadDriver(driver_id, 'edit');
	});
	$('[id^=deletedriver]').click(function() {
		var arr = $(this).attr('id').split('-');
		var driver_id = arr[1];
		deleteDriver(driver_id);
	});
	/** * View Drive Transacion Details ** */
	$('[id^=viewtransaction]').click(function() {
		var arr = $(this).attr('id').split('-');
		var driver_id = arr[1];
		loadDriverTranscationDetails(driver_id);
	});
	
	/** * View Drive Log History Details ** */
	$('[id^=viewloghistory]').click(function() {
		var arr = $(this).attr('id').split('-');
		var driver_id = arr[1];
		loadDriverLogHistory(driver_id);
	});
	
	$('[id^=allocatetaxi]').click(
			function() {
				var allocate_status = $(this).attr('allocateStatus');
				var driver_name = $(this).attr('driverName');
				var taxi_device_id = $(this).attr('taxiDeviceId');
				var taxi_device_code = $(this).attr('taxiDeviceCode');
				var taxi_registration_no = $(this).attr('taxiRegistrationNo');
				var taxi_id = $(this).attr('taxiId');
				var arr = $(this).attr('id').split('-');

				var driver_id = arr[1];
				$('#allocate-taxi-device').modal('show');
				$('#tempDriverId').val(driver_id);
				$('#show-driver-name').html(driver_name);
				if (allocate_status == STATUS_ACTIVE) {
					$('#taxiId').append($('<option>', {
						value : taxi_id,
						text : taxi_registration_no
					}));
					$('#taxiDeviceId').append($('<option>', {
						value : taxi_device_id,
						text : taxi_device_code
					}));
				}

				$("#taxiId option[value=" + taxi_id + "]").attr("selected",
						"selected");
				$("#taxiDeviceId option[value=" + taxi_device_id + "]").attr(
						"selected", "selected");

			});
	$('#allocate-taxi-device').on('hidden.bs.modal', function() {
		location.reload();
	});
	$('#save-allocate-taxi-btn').click(function() {
		allocateTaxiToDriver();
	});

	$('#mobile').blur(function() {

		checkDriverMobile();
	});
	$('#email').blur(function() {

		checkDriverEmail();
	});

	var now = new Date();
    var currentDateTime = [
        now.getFullYear()-18,
        '/',
        now.getMonth(),
        '/',
        now.getDate(),
      ].join('');

 
	$('#dob').datetimepicker({
		dayOfWeekStart : 1,
		lang : 'en',
		timepicker : false,
		closeOnDateSelect : true,
		format : 'Y-m-d',
		scrollMonth : false,
		scrollInput : false,
		startDate:currentDateTime,
		maxDate:currentDateTime
		
	});

	$('#licenseExpiryDate').datetimepicker({
		dayOfWeekStart : 1,
		lang : 'en',
		startDate : '',
		timepicker : false,
		closeOnDateSelect : true,
		scrollMonth : false,
		scrollInput : false,
		format : 'Y-m-d',
		minDate : 0
	});
	/** * Driver Lighbox Image intialze * */
	$('a[data-imagelightbox="demo"]').imageLightbox({
		selector : 'a[data-imagelightbox]',
		id : 'imagelightbox',
		allowedTypes : 'png|jpg|jpeg|gif',
		animationSpeed : 250,
		activity : false,
		arrows : true,
		button : true,
		caption : true,
		enableKeyboard : true,
		lockBody : true,
		navigation : true,
		overlay : true,
		preloadNext : true,
		quitOnEnd : true,
		quitOnImgClick : false,
		quitOnDocClick : true,
		quitOnEscKey : true
	});
	$('a[data-imagelightbox="listview"]').imageLightbox({
		selector : 'a[data-imagelightbox]',
		id : 'imagelightbox',
		allowedTypes : 'png|jpg|jpeg|gif',
		animationSpeed : 250,
		activity : false,
		arrows : false,
		button : true,
		caption : true,
		enableKeyboard : false,
		lockBody : true,
		navigation : false,
		overlay : true,
		preloadNext : false,
		quitOnEnd : false,
		quitOnImgClick : true,
		quitOnDocClick : true,
		quitOnEscKey : true
	});
}

function loadDriverList() {
	// Validate the passenger value again in case the user changed the input
	var url = location.origin + '/driver/getDriverList';
	jQuery.ajax({
		method : "POST",
		url : url,
	// data:{'get_type':'ajax_call'},
	}).done(function(response) {

		// jQuery('#content').html(response);
	});
}

function loadDriver(driver_id, view_mode) {
	// var url = location.origin + '/user/getDetailsById/' + driver_id + '/' +
	// view_mode;
	window.location.href = location.origin + '/driver/getDetailsById/'
			+ driver_id + '/' + view_mode;
	/*
	 * jQuery.ajax({ method: "POST", url: url, dataType: 'json', data: {id:
	 * user_id} }).done(function (response) {
	 * jQuery('#container').html(response.html); userDependency(); });
	 */
}
function loadDriverTranscationDetails(driver_id) {
	window.location.href = location.origin
			+ '/driver/getDriverTransactionDetailsList/' + driver_id;
}
function loadDriverLogHistory(driver_id) {
	window.location.href = location.origin
			+ '/driver/getDriverShiftHistoryList/' + driver_id;
}
function saveDriver() {

	var isValidate = false;
	// Validate the user details again in case the user changed the input
	var url = location.origin + '/driver/saveDriver/';

	var userData = {};
	var userData = new window.FormData($('#edit_driver_form')[0]);
	/*
	 * jQuery( jQuery('#edit_passenger_form :input').serializeArray()
	 * ).each(function( x , y ){ passengerData[y.name] = y.value; });
	 */

	isValidate = validate('edit_driver_form');
	if (isValidate) {
		$("#overlay").removeClass('hidden');
		jQuery.ajax({
			xhr : function() {
				return $.ajaxSettings.xhr();
			},
			type : "POST",
			data : userData,
			cache : false,
			contentType : false,
			processData : false,
			url : url,
		}).done(

				function(response) {
					$("#overlay").addClass('hidden');
					response = jQuery.parseJSON(response);
					if (response.driver_id) {

						successMessage(response.msg);
						$('#edit_driver_form')[0].reset();
						// loadUserList();
						setTimeout(function() {
							window.location.href = location.origin
									+ '/driver/getDriverList';
						}, 1000);

					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {
			$("#overlay").addClass('hidden');
			failureMessage(response.msg);

		});
	}

}

function changeDriverStatus(driver_id, status) {
	if (confirm("Make sure before changing driver status?.")) {
		if (status == 1) {
			status = 0;
		} else {
			status = 1;
		}

		var url = location.origin + '/driver/changeDriverStatus/';
		$("#overlay").removeClass('hidden');
		jQuery.ajax({
			method : "POST",
			url : url,
			data : {
				'id' : driver_id,
				'status' : status
			},
		}).done(

				function(response) {
					$("#overlay").addClass('hidden');
					response = jQuery.parseJSON(response);
					if (response.status > 0) {

						successMessage(response.msg);
						// loadUserList();
						window.location.href = location.origin
								+ '/driver/getDriverList';
					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {
			$("#overlay").addClass('hidden');
			failureMessage('Status change failed.. Please try again later.');

		});
	}

}
function changeDriverVerifyStatus(driver_id, status) {
	if (confirm("Make sure before changing driver verify status?.")) {
		if (status == 1) {
			status = 0;
		} else {
			status = 1;
		}

		var url = location.origin + '/driver/changeDriverVerifyStatus/';
		$("#overlay").removeClass('hidden');
		jQuery.ajax({
			method : "POST",
			url : url,
			data : {
				'id' : driver_id,
				'status' : status
			},
		}).done(

				function(response) {
					$("#overlay").addClass('hidden');
					response = jQuery.parseJSON(response);
					if (response.status > 0) {

						successMessage(response.msg);
						// loadUserList();
						window.location.href = location.origin
								+ '/driver/getDriverList';
					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {
			$("#overlay").addClass('hidden');
			failureMessage('Status change failed.. Please try again later.');

		});
	}

}
function changeDriverLogStatus(driver_id, status) {
	if (confirm("Make sure before changing driver log status?.")) {
		if (status == 1) {
			status = 0;
		} else {
			status = 1;
		}

		var url = location.origin + '/driver/changeDriverLogStatus/';

		jQuery.ajax({
			method : "POST",
			url : url,
			data : {
				'id' : driver_id,
				'status' : status
			},
		}).done(
				function(response) {
					response = jQuery.parseJSON(response);
					if (response.status > 0) {

						successMessage(response.msg);
						// loadDriverList();
						window.location.href = location.origin
								+ '/driver/getDriverList';
					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {

			failureMessage('Status change failed.. Please try again later.');

		});
	}

}

function deleteDriver(driver_id) {
	if (confirm("Make sure before deleting driver?.")) {
		var url = location.origin + '/driver/deleteDriver/';
		$("#overlay").removeClass('hidden');
		jQuery.ajax({
			method : "POST",
			url : url,
			data : {
				'id' : driver_id
			},
		}).done(

				function(response) {
					$("#overlay").addClass('hidden');
					response = jQuery.parseJSON(response);
					if (response.status > 0) {

						successMessage(response.msg);
						// loadDriverList();
						window.location.href = location.origin
								+ '/driver/getDriverList';
					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {
			$("#overlay").addClass('hidden');
			failureMessage('Delete failed.. Please try again later.');

		});
	}

}

function resignDriver(driver_id) {
	if (confirm("Make sure before resigning a driver?.")) {
		var url = location.origin + '/driver/resignDriver/';
		$("#overlay").removeClass('hidden');
		jQuery.ajax({
			method : "POST",
			url : url,
			data : {
				'id' : driver_id
			},
		}).done(

				function(response) {
					$("#overlay").addClass('hidden');
					response = jQuery.parseJSON(response);
					if (response.status > 0) {

						successMessage(response.msg);
						// loadDriverList();
						window.location.href = location.origin
								+ '/driver/getDriverList';
					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {
			$("#overlay").addClass('hidden');
			failureMessage('Delete failed.. Please try again later.');

		});
	}

}

function checkDriverEmail() {
	var email = $('#email').val();
	var driver_id = $('#driver_id').val();
	if (validateEmail(email)) {
		if (email != '') {
			$('#save-driver-btn').prop('disabled', true);
			var url = location.origin + '/driver/checkDriverEmail/';
			$("#overlay").removeClass('hidden');
			jQuery.ajax({
				method : "POST",
				data : {
					'email' : email,
					'driver_id' : driver_id
				},
				url : url
			}).done(function(response) {
				$("#overlay").addClass('hidden');
				response = jQuery.parseJSON(response);
				if (response.status == 1) {
					$('#email').val('');
				}
				$('#save-driver-btn').prop('disabled', false);
				infoMessage(response.msg, 1, '#email');
			});
		}
	}
}
function checkDriverMobile() {
	var mobile = $('#mobile').val();
	var driver_id = $('#driver_id').val();
	if (validateMobile(mobile)) {
		if (mobile != '') {
			$('#save-driver-btn').prop('disabled', true);
			var url = location.origin + '/driver/checkDriverMobile/';
			$("#overlay").removeClass('hidden');
			jQuery.ajax({
				method : "POST",
				data : {
					'mobile' : mobile,
					'driver_id' : driver_id
				},
				url : url
			}).done(function(response) {
				$("#overlay").addClass('hidden');
				response = jQuery.parseJSON(response);
				if (response.status == 1) {
					$('#mobile').val('');
				}
				$('#save-driver-btn').prop('disabled', false);
				infoMessage(response.msg, 1, '#mobile');
			});
		}
	}
}

function allocateTaxiToDriver() {

	var isValidate = false;
	// Validate the driver allocate details
	var url = location.origin + '/driver/AllocateTaxiToDriver/';

	var allocateData = {};
	var allocateData = new window.FormData($('#edit_drive_allocate_form')[0]);

	isValidate = validate('edit_drive_allocate_form');
	if (isValidate) {
		jQuery.ajax({
			xhr : function() {
				return $.ajaxSettings.xhr();
			},
			type : "POST",
			data : allocateData,
			cache : false,
			contentType : false,
			processData : false,
			url : url,
		}).done(
				function(response) {
					response = jQuery.parseJSON(response);
					if (response.status > STATUS_DEACTIVE) {
						$('#allocate-taxi-device').modal('hide');

						window.location.href = location.origin
								+ '/driver/getDriverList';
					} else if (response.status == STATUS_DEACTIVE) {
						$('#allocate-taxi-device').modal('hide');
						infoMessage(response.msg);
						window.location.href = location.origin
								+ '/driver/getDriverList';
					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {

		});
	}
}
