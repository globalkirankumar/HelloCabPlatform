$(document).ready(function() {
	passengerDependency();
 //alert('state');exit;
    //datatables
    table = $('#passenger-data-table').DataTable({ 
 
    	"scrollX": true,
   	 "scrollY": 400,
   	"scrollCollapse": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
"dom": 'Bflrtip',
"pagingType": "full_numbers",
        "buttons": [
                    /*{
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: [ 0, ':visible' ]
                    }
                    },*/
                 'colvis',
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdfHtml5',
                    exportOptions: {
                    	columns: ':visible'
                    }
                },
                
        ],
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": location.origin +'/passenger/ajax_list',
            "type": "POST"
        },
 
        //Set column definition initialisation properties.s
        "columnDefs": [
        { 
            "targets": [ -1,12,0,1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
        "lengthMenu": [[20, 50, 100], [20, 50, 100]],
        "fnDrawCallback": function( oSettings ) {
            passengerDependency();
          }
 
    });
    
    $('label input.toggle-vis').each(function(){
    	var column = table.column( $(this).attr('data-column') );
        if($(this).prop('checked')==true)
        {
        	//column.toggle(this.checked);
        	column.visible(true);
        }
        else
        {
        	column.visible( false);
        }
    });
    $('label input.toggle-vis').on( 'change', function (e) {
        e.preventDefault();
        // Get the column API object
        var column = table.column( $(this).attr('data-column') );
        if($(this).prop('checked')==true)
        {
        	//column.toggle(this.checked);
        	column.visible(true);
        }
        else
        {
        	column.visible( false);
        }
        passengerDependency();
    } );
});

function passengerDependency()
{ 
	
     $('#save-passenger-btn').click(function(){
		 savePassenger(); 
	});
	$('#cancel-passenger-btn').click(function(){
		// loadUserList(); 
		 window.location.href = location.origin + '/passenger/getPassengerList';
	});
	$('[id^=status]').click(function(){
		var status=$(this).attr('status');
		var arr = $(this).attr('id').split('-');
	    var passenger_id = arr[1];
	    changePassengerStatus(passenger_id,status); 
	});
	
	$('[id^=viewpassenger]').click(function(){
		var arr = $(this).attr('id').split('-');
	    var passenger_id = arr[1];
	    loadPassenger(passenger_id,'view'); 
	});
	$('[id^=editpassenger]').click(function(){
		var arr = $(this).attr('id').split('-');
	    var passenger_id = arr[1];
	    loadPassenger(passenger_id,'edit'); 
	});
	$('[id^=logstatus]').click(function() {
		var status = $(this).attr('status');
		var arr = $(this).attr('id').split('-');
		var passenger_id = arr[1];

		changePassengerLogStatus(passenger_id, status);
	});
	/*$('[id^=deletepassenger]').click(function(){
		var arr = $(this).attr('id').split('-');
	    var passenger_id = arr[1];
	    deleteDriver(passenger_id);
	});*/
	/** * View Passenger Transacion Details ** */
	$('[id^=viewpassengertransaction]').click(function() {
		var arr = $(this).attr('id').split('-');
		var passenger_id = arr[1];
		loadPassengerTranscationDetails(passenger_id, 'view');
	});
	$('#mobile').blur(function(){
		
		 checkPassengerMobile(); 
	});
	$('#email').blur(function(){
		
		checkPassengerEmail(); 
	});
        $('#dob').datetimepicker({
		dayOfWeekStart : 1,
		lang : 'en',
		startDate : '0',
		timepicker : false,
		closeOnDateSelect : true,
		format : 'Y-m-d',
		scrollMonth : false,
		scrollInput : false,
		maxDate : 0
	});
        
        $('a[data-imagelightbox="listview"]').imageLightbox({
            selector:       'a[data-imagelightbox]',
            id:             'imagelightbox',
            allowedTypes:   'png|jpg|jpeg|gif',
            animationSpeed: 250,
            activity:       false,
            arrows:         false,
            button:         true,
            caption:        true,
            enableKeyboard: false,
            lockBody:       true,
            navigation:     false,
            overlay:        true,
            preloadNext:    false,
            quitOnEnd:      false,
            quitOnImgClick: true,
            quitOnDocClick: true,
            quitOnEscKey:   true
    });
        
}

function checkPassengerEmail()
{
	var email=$('#email').val();
	if (validateEmail(email)) {
	if(email != '')
	{
		$('#save-passenger-btn').prop('disabled', true);
    var url = location.origin + '/passenger/checkPassengerEmail/'; 
    $("#overlay").removeClass('hidden');
    jQuery.ajax({
        method: "POST",
        data:{'email':email},
        url: url
    }).done(function (response) {
    	$("#overlay").addClass('hidden');
    	response=jQuery.parseJSON(response);
    	if(response.status==1)
    	{
    		$('#email').val('');
    	}
    	$('#save-passenger-btn').prop('disabled', false);
    	infoMessage(response.msg,1,'#email');
    });
	}
	}
}
function loadPassengerTranscationDetails(passenger_id, view_mode) {
	window.location.href = location.origin
			+ '/Passenger/getPassengerTransactionDetailsList/' + passenger_id;
}
function checkPassengerMobile()
{
	var mobile=$('#mobile').val();
	if (validateMobile(mobile)) {
	if(mobile != '')
	{
		$('#save-passenger-btn').prop('disabled', true);
    var url = location.origin + '/passenger/checkPassengerMobile/'; 
    $("#overlay").removeClass('hidden');
    jQuery.ajax({
        method: "POST",
        data:{'mobile':mobile},
        url: url
    }).done(function (response) {
    	$("#overlay").addClass('hidden');
    	response=jQuery.parseJSON(response);
    	if(response.status==1)
    	{
    		$('#mobile').val('');
    	}
    	$('#save-passenger-btn').prop('disabled', false);
    	infoMessage(response.msg,1,'#mobile');
    });
	}
	}
}
function changePassengerStatus(id)
{
	if (confirm("Make sure before changing passenger status?.")) {
    var passenger_id = 0;
    var status='';
    var status_val  = $('#status-'+id).html();
    if(status_val=='Active')
    {
    	status=0;
    }
    else if(status_val=='Inactive')
    {
    	status=1;
    }
    var url = location.origin + '/passenger/changePassengerStatus/';
    $("#overlay").removeClass('hidden');
    jQuery.ajax({
        method: 'POST',
        url: url,
        data:{'id':id,'status':status},
    }).done(function (response) {
    	$("#overlay").addClass('hidden');
    	response=jQuery.parseJSON(response);
    	if(response)
        {
        	successMessage( response.msg);
        	//loadPassengerList();
                location.reload();
        }
        else
        {
        	failureMessage( response.msg);
        }
//        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	$("#overlay").addClass('hidden');
    	failureMessage( 'Status change failed.. Please try again later.');
    	
    });
	} 
}

function loadPassengerList()
{
    var url = location.origin + '/passenger/getPassengerList/';
    jQuery.ajax({
        method: "POST",
        url: url,
    }).done(function (response) {
        jQuery('#content').html(response);
        dataTableLoader();
    });
}

function loadPassenger( passenger_id , view_mode)
{
  //  var url = location.origin + '/passenger/getDetailsById/' + passenger_id + '/' + view_mode;
    window.location.href =location.origin + '/passenger/getDetailsById/' + passenger_id + '/' + view_mode;
   /* jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {id: passenger_id}
    }).done(function (response) {
        jQuery('#content').html(response.html);
        passengerDependency();
    });*/
}
function loadTransctionHistory( passenger_id )
{
    var url = location.origin + '/wallet/getWalletList/';
    
   var response =  jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {'passenger_id': passenger_id,'get_type':'ajax_call'},
        async:false
    }).responseText;
    if(response){
        jQuery('#passenger-details-information').replaceWith(response);
        dataTableLoader();
    }else{
        failureMessage('Please Try Again Later');
    }
}

function savePassenger() {
    var isValidate=false;
    // Validate the passenger details again in case the user changed the input
    var url = location.origin + '/passenger/savePassenger/';
    
    var passengerData = {};
    var passengerData = new FormData($('#edit_passenger_form')[0]);
    //passengerData.append($('#profileImage').files[0], file);
    /*jQuery( jQuery('#edit_passenger_form :input').serializeArray() ).each(function( x , y ){
    	passengerData[y.name] = y.value;
    });*/
    isValidate=validate('edit_passenger_form');
    if(isValidate)
    {
    //$('#save_psassenger_btn').button('loading');
    	$("#overlay").removeClass('hidden');
    jQuery.ajax({
    	xhr: function () {  
            return $.ajaxSettings.xhr();
        },
        type: "POST",
        data: passengerData,
        cache: false,
        contentType: false,
        processData: false,
        url: url,
    }).done(function (response) {
    	$("#overlay").addClass('hidden');
          response = jQuery.parseJSON(response);
        if(response.passenger_id)
        {
        	successMessage( response.msg);
        	//loadPassengerList();
        	window.location.href = location.origin + '/passenger/getPassengerList/';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	$("#overlay").addClass('hidden');
    	failureMessage( response.msg);
    	
    });
    }
    
}
function changePassengerLogStatus(passenger_id, status) {
	if (confirm("Make sure before changing passenger log status?.")) {
		if (status == 1) {
			status = 0;
		} else {
			status = 1;
		}

		var url = location.origin + '/Passenger/changePassengerLogStatus/';
		$("#overlay").removeClass('hidden');
		jQuery.ajax({
			method : "POST",
			url : url,
			data : {
				'id' : passenger_id,
				'status' : status
			},
		}).done(

				function(response) {
					$("#overlay").addClass('hidden');
					response = jQuery.parseJSON(response);
					if (response.status > 0) {

						successMessage(response.msg);
						// loadUserList();
						window.location.href = location.origin
								+ '/Passenger/getPassengerList';
					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {
			$("#overlay").addClass('hidden');
			failureMessage('Status change failed.. Please try again later.');

		});
	}

}