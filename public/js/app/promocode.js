$(document).ready(function() {
	promocodeDependency();
    //datatables
		table = $('#promocode-data-table').DataTable({ 
 
    	 "scrollX": true,
    	 "scrollY": 400,
    	"scrollCollapse": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
"dom": 'Bflrtip',
        
        "buttons": [
                    /*{
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: [ 0, ':visible' ]
                    }
                    },*/
                 'colvis',
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdfHtml5',
                    exportOptions: {
                    	columns: ':visible'
                    }
                },
                
        ],
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": location.origin +'/promocode/ajax_list',
            "type": "POST"
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1,0], //last column
            "orderable": false, //set not orderable
        },
        
        ],
        "lengthMenu": [[20, 50, 100], [20, 50, 100]],
        "fnDrawCallback": function( oSettings ) {
        	promocodeDependency();
          }
        
 
    });
    $('label input.toggle-vis').each(function(){
    	var column = table.column( $(this).attr('data-column') );
        if($(this).prop('checked')==true)
        {
        	//column.toggle(this.checked);
        	column.visible(true);
        }
        else
        {
        	column.visible( false);
        }
    });
    $('label input.toggle-vis').on( 'change', function (e) {
        e.preventDefault();
        // Get the column API object
        var column = table.column( $(this).attr('data-column') );
        if($(this).prop('checked')==true)
        {
        	//column.toggle(this.checked);
        	column.visible(true);
        }
        else
        {
        	column.visible( false);
        }
        promocodeDependency();
    });
});

function promocodeDependency()
{
	if($('#isCentPercentDiscount').is(':checked'))
	{
		$("#promoDiscountAmount").attr('readonly',true);
		$("#promoDiscountAmount2").attr('readonly',true);
		$("#promoDiscountAmount2").attr('required',false);
		$("#promoDiscountAmount2").removeAttr('pattern');
		$("#promoDiscountAmount").removeAttr('pattern');
		$("label[for='promoDiscountAmount2']").removeClass('required');
	}
	else
	{
		$("#promoDiscountAmount").attr('readonly',false);
		$("#promoDiscountAmount2").attr('readonly',false);
		$("#promoDiscountAmount2").attr('required',true);
		$("#promoDiscountAmount2").attr('pattern','^\\d{1,6}([\.]?(\\d{1,2}))?$');
		$("#promoDiscountAmount").attr('pattern','(?!^0*$)(?!^0*\.0*$)^\\d{1,2}([\.]?(\\d{1,2}))?$');
		$("label[for='promoDiscountAmount2']").addClass('required');
	}
	
	if($('#promocodeType').val()==PROMOCODE_TYPE_GENERIC)
	{
		$("#combination-promocode").addClass('hidden');
		$("#combination-promocode").attr('required',false);
		$("#genric-promocode").removeClass('hidden');
		$('#zoneId1').attr('required',false);
		$("#zone_id2").addClass('hidden');
		$('#zoneId2').attr('required',false);
		
	}
	else if($('#promocodeType').val()==PROMOCODE_TYPE_COMBINATION)
	{
		$("#combination-promocode").removeClass('hidden');
		$("#combination-promocode").attr('required',true);
		$("#genric-promocode").addClass('hidden');
		$('#zoneId1').attr('required',true);
		$("#zone_id2").removeClass('hidden');
		$('#zoneId2').attr('required',true);
		
	}
	
	if($('#promoStartDatetime').val())
	{
		$('#promoEndDatetime').attr('readonly',false);
	}
	else
	{
		$('#promoEndDatetime').attr('readonly',true);
	}
	
	$('#save-promocode-btn').click(function(){
		if(($('#isCentPercentDiscount').is(':checked')) && ($('#promoDiscountAmount').val()>0) && ($('#promoDiscountAmount').val()<=100))
		{
			savePromocode();
		}
		else if(!($('#isCentPercentDiscount').is(':checked')) && ($('#promoDiscountAmount').val()>0) && ($('#promoDiscountAmount').val()<=100) && ($('#promoDiscountAmount2').val()>0))
		{
			savePromocode();
		}
		else
		{
			infoMessage('Enter promo discount percentage b/w 0-100 and amount value greater than zero(0)');
		}
		  
	});
	$('#cancel-promocode-btn').click(function(){
		// loadUserList(); 
		 window.location.href = location.origin + '/promocode/getPromocodeList';
	});
	$('[id^=status]').click(function(){
		var status=$(this).attr('status');
		var arr = $(this).attr('id').split('-');
	    var promocode_id = arr[1];
	    changePromocodeStatus(promocode_id,status); 
	});
	
	$('[id^=viewpromocode]').click(function(){
		var arr = $(this).attr('id').split('-');
	    var promocode_id = arr[1];
		 loadPromocode(promocode_id,'view'); 
	});
	$('[id^=editpromocode]').click(function(){
		var arr = $(this).attr('id').split('-');
	    var promocode_id = arr[1];
		 loadPromocode(promocode_id,'edit'); 
	});
	$('[id^=deletepromocode]').click(function(){
		var arr = $(this).attr('id').split('-');
	    var promocode_id = arr[1];
	    deletePromocode(promocode_id);
	});
	$('#promoCode').blur(function(){
		 checkPromocode(); 
	});
	
	$('#promoDiscountType').on('change', function(){
		if($('#promoDiscountType').val()==PAYMENT_TYPE_AMOUNT)
		{
			$("label[for='promoDiscountAmount']").html("Promo Discount Amount");
			$('#promoDiscountAmount').attr('placeholder','Promo Discount Amount');
		}
		else if($('#promoDiscountType').val()==PAYMENT_TYPE_PERCENTAGE)
		{
			$("label[for='promoDiscountAmount']").html("Promo Discount Percentage");
			$('#promoDiscountAmount').attr('placeholder','Promo Discount Percentage');
		}
		
		
	});
	$('#promocodeType').on('change', function(){
		if($('#promocodeType').val()==PROMOCODE_TYPE_GENERIC)
		{
			$("#combination-promocode").addClass('hidden');
			$("#combination-promocode").attr('required',false);
			$('#zoneId1').attr('required',false);
			$("#genric-promocode").removeClass('hidden');
			$('#zoneId1').attr('required',false);
			$("#zone_id2").addClass('hidden');
			$('#zoneId2').attr('required',false);
			$('#zoneId2').val('');
		}
		else if($('#promocodeType').val()==PROMOCODE_TYPE_COMBINATION)
		{
			$("#combination-promocode").removeClass('hidden');
			$("#combination-promocode").attr('required',true);
			$('#zoneId1').attr('required',true);
			$("#genric-promocode").addClass('hidden');
			$('#zoneId1').attr('required',true);
			$("#zone_id2").removeClass('hidden');
			$('#zoneId2').attr('required',true);
			$('#zoneId2').val('');
		}
	});
	$('#isCentPercentDiscount').on('click', function(){
		if($(this).is(':checked'))
		{
			$("#promoDiscountAmount").val('100');
			$("#promoDiscountAmount2").val('0');
			$("#promoDiscountAmount").attr('readonly',true);
			$("#promoDiscountAmount2").attr('readonly',true);
			$("#promoDiscountAmount2").attr('required',false);
			$("#promoDiscountAmount2").removeAttr('pattern');
			$("#promoDiscountAmount").removeAttr('pattern');
			$("label[for='promoDiscountAmount2']").removeClass('required');
		}
		else
		{
			$("#promoDiscountAmount").val('0');
			$("#promoDiscountAmount2").val('0');
			$("#promoDiscountAmount").attr('readonly',false);
			$("#promoDiscountAmount2").attr('readonly',false);
			$("#promoDiscountAmount2").attr('required',true);
			$("#promoDiscountAmount2").attr('pattern','^\\d{1,6}([\.]?(\\d{1,2}))?$');
			$("#promoDiscountAmount").attr('pattern','(?!^0*$)(?!^0*\.0*$)^\\d{1,2}([\.]?(\\d{1,2}))?$');
			$("label[for='promoDiscountAmount2']").addClass('required');
		}
	});
	/*$('#promoStartDatetime').datetimepicker({
			dayOfWeekStart : 0,
	       lang           : 'en',
	       timepicker     : true,
	       format         : 'Y-m-d H:i',
	       step           :15,
	       minDate: 0,
	       minTime:0,
		  onShow:function( ct ){
		   this.setOptions({
		    maxDate:$('#promoEndDatetime').val()?$('#promoEndDatetime').val():false,
		    maxTime:$('#promoEndDatetime').val()?$('#promoEndDatetime').val():false
		   })
		  },
		  timepicker:false
		 });
		 $('#promoEndDatetime').datetimepicker({
			 dayOfWeekStart : 0,
		       lang           : 'en',
		       timepicker     : true,
		       format         : 'Y-m-d H:i',
		       step           :15,
		  onShow:function( ct ){
		   this.setOptions({
		    minDate:$('#promoStartDatetime').val()?$('#promoStartDatetime').val():false,
		    minTime:$('#promoStartDatetime').val()?$('#promoStartDatetime').val():false
		   })
		  },
		  timepicker:false
		 });*/
	
		 $('#promoStartDatetime').datetimepicker({
		       dayOfWeekStart : 1,
		       lang           : 'en',
		       startDate      : '0',
		       timepicker     : true,
		       format         : 'Y-m-d H:i:s',
		       step           :15,
		       minDate: 0,
		       onShow:function( ct ){
				   this.setOptions({
					minTime: 0,
				    maxDate:$('#promoEndDatetime').val()?$('#promoEndDatetime').val():false,
				    
				   })
		       },
		       onSelectDate:function(){
		    	   $('#promoEndDatetime').attr('readonly',false);
		            var d = $('#promoStartDatetime').datetimepicker('getValue');
		             var now = new Date();
		             var cur_date      =  [now.getFullYear(),'-',now.getMonth(),'-',now.getDate()].join('');
		             var selected_date = d.getFullYear()+'-'+d.getMonth()+'-'+d.getDate();
		                if(cur_date==selected_date){
		                   this.setOptions({
		                     minTime       : 0
		                 })
		              }else{
		                 this.setOptions({
		                     minTime       : 1
		                 }) 
		              }
		        }  
		   });
		 
		$('#promoEndDatetime').datetimepicker({
		       dayOfWeekStart : 1,
		       lang           : 'en',
		       startDate      : '0',
		       timepicker     : true,
		       format         : 'Y-m-d H:i:s',
		       step           :15,
		       
		           onShow:function( ct ){
		                   this.setOptions({
		                	
		                    minDate:$('#promoStartDatetime').val()?$('#promoStartDatetime').val():0,
		                    maxTime:$('#promoStartDatetime').val()?$('#promoStartDatetime').val():false,
		                   })
		               
		              },
		              onSelectDate:function(){
				            var d = $('#promoEndDatetime').datetimepicker('getValue');
				             var now = new Date();
				             var cur_date      =  [now.getFullYear(),'-',now.getMonth(),'-',now.getDate()].join('');
				             var selected_date = d.getFullYear()+'-'+d.getMonth()+'-'+d.getDate();
				                if(cur_date==selected_date){
				                   this.setOptions({
				                	   maxTime       : 0
				                 })
				              }else{
				                 this.setOptions({
				                	 maxTime       : 1
				                 }) 
				              }
				        } 
		   });
		
		
}

function changePromocodeStatus(promocode_id,status)
{
	if (confirm("Make sure before changing promocode status?."))
    {
    if(status==1)
    {
    	status=0;
    }
    else
    {
    	status=1;
    }
    
    var url = location.origin + '/promocode/changePromocodeStatus/';
    $("#overlay").removeClass('hidden');
    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'id':promocode_id,'status':status},
    }).done(function (response) {
    	$("#overlay").addClass('hidden');
    	response=jQuery.parseJSON(response);
    	if(response.status > 0)
        {
        	
        	successMessage( response.msg);
        	//loadUserList();
        	window.location.href = location.origin + '/promocode/getPromocodeList';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	$("#overlay").addClass('hidden');
    	failureMessage( 'Status change failed.. Please try again later.');
    	
    });
    }
    
}

function loadPromocodeList()
{
    // Validate the passenger value again in case the user changed the input
    var url = location.origin + '/promocode/getPromocodeList';
    jQuery.ajax({
        method: "POST",
        url: url,
        //data:{'get_type':'ajax_call'},
    }).done(function (response) {
        //jQuery('#content').html(response);
    });
}

function loadPromocode( promocode_id , view_mode)
{
   // var url = location.origin + '/user/getDetailsById/' + promocode_id + '/' + view_mode;
    window.location.href =location.origin + '/promocode/getDetailsById/' + promocode_id + '/' + view_mode;
    /*jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {id: user_id}
    }).done(function (response) {
        jQuery('#container').html(response.html);
        userDependency();
    });*/
}

function savePromocode() {
	
    var isValidate=false;
 // Validate the user details again in case the user changed the input
    var url = location.origin + '/Promocode/savePromocode/';
    
    var promocodeData = {};
    var promocodeData = new window.FormData($('#edit_promocode_form')[0]);
    /*jQuery( jQuery('#edit_passenger_form :input').serializeArray() ).each(function( x , y ){
    	passengerData[y.name] = y.value;
    });*/
   
    isValidate=validate('edit_promocode_form');
    if(isValidate)
    {
    	$("#overlay").removeClass('hidden');
    jQuery.ajax({
        xhr: function () {  
            return $.ajaxSettings.xhr();
        },
        type: "POST",
        data: promocodeData,
        cache: false,
        contentType: false,
        processData: false,
        url: url,
    }).done(function (response) {
    	$("#overlay").addClass('hidden');
          response = jQuery.parseJSON(response);
        if(response.promocode_id)
        {
        	successMessage( response.msg);
        	//loadUserList();
        	window.location.href = location.origin + '/Promocode/getPromocodeList';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	$("#overlay").addClass('hidden');
    	failureMessage( response.msg);
    	
    });
    }
    
}

function checkPromocode()
{
	var promocode=$('#promoCode').val();
	if(promocode != '')
	{
		$('#save-promocode-btn').prop('disabled', true);
    var url = location.origin + '/Promocode/checkPromocode/'; 
    $("#overlay").removeClass('hidden');
    jQuery.ajax({
        method: "POST",
        data:{'promocode':promocode},
        url: url
    }).done(function (response) {
    	$("#overlay").addClass('hidden');
    	response=jQuery.parseJSON(response);
    	if(response.status==1)
    	{
    		$('#promoCode').val('');
    	}
    	$('#save-promocode-btn').prop('disabled', false);
    	infoMessage(response.msg,1,'#promoCode');
    });
	}
}