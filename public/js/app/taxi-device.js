$(document).ready(function() {
 //alert('state');exit;
	taxiDeviceDependency();
    //datatables
    table = $('#taxi-device-data-table').DataTable({
    	"scrollX": true,
   	 "scrollY": 400,
   	"scrollCollapse": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
"dom": 'Bflrtip',
        
        "buttons": [
                    /*{
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: [ 0, ':visible' ]
                    }
                    },*/
                // 'colvis',
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdfHtml5',
                    exportOptions: {
                    	columns: ':visible'
                    }
                },
                
        ],
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": location.origin +'/TaxiDevice/ajax_list',
            "type": "POST"
            
        },
 
        //Set column definition initialisation properties.s
        "columnDefs": [
        { 
            "targets": [0,-1], //last column
            "orderable": false, //set not orderable
        },
        ],
        "lengthMenu": [[20, 50, 100], [20, 50, 100]],
        "fnDrawCallback": function( oSettings ) {
        	taxiDeviceDependency();
          }
 
    });
    
    $('label input.toggle-vis').each(function(){
    	var column = table.column( $(this).attr('data-column') );
        if($(this).prop('checked')==true)
        {
        	//column.toggle(this.checked);
        	column.visible(true);
        }
        else
        {
        	column.visible( false);
        }
    });
    $('label input.toggle-vis').on( 'change', function (e) {
        e.preventDefault();
        // Get the column API object
        var column = table.column( $(this).attr('data-column') );
        if($(this).prop('checked')==true)
        {
        	//column.toggle(this.checked);
        	column.visible(true);
        }
        else
        {
        	column.visible( false);
        }
        taxiDeviceDependency();
    } );
    
});

function taxiDeviceDependency()
{
	$('#save-taxidevice-btn').click(function(){
		 saveTaxiDevice(); 
	});
	$('#cancel-taxidevice-btn').click(function(){
		// loadUserList(); 
		 window.location.href = location.origin + '/taxiDevice/getTaxiDeviceList';
	});
	$('[id^=status]').click(function(){
		var status=$(this).attr('status');
		var arr = $(this).attr('id').split('-');
	    var taxi_device_id = arr[1];
	    changeTaxiDeviceStatus(taxi_device_id,status); 
	});
	$('[id^=viewtaxidevice]').click(function(){
		var arr = $(this).attr('id').split('-');
	    var taxi_device_id = arr[1];
		 loadTaxiDevice(taxi_device_id,'view'); 
	});
	$('[id^=edittaxidevice]').click(function(){
		var arr = $(this).attr('id').split('-');
	    var taxi_device_id = arr[1];
	    loadTaxiDevice(taxi_device_id,'edit'); 
	});
	$('[id^=deletetaxidevice]').click(function(){
		var arr = $(this).attr('id').split('-');
	    var taxi_device_id = arr[1];
	    deleteTaxiDevice(taxi_device_id);
	});
	$('#mobile').blur(function() {

		checkTaxiDeviceMobile();
	});
	
$('#purchasedDate').datetimepicker({
    dayOfWeekStart : 1,
    lang           : 'en',
    startDate      : '',
    timepicker     : false,
    closeOnDateSelect : true,
    scrollMonth : false,
    scrollInput : false,
    format         : 'Y-m-d'
});
	   
}

function loadTaxiDeviceList()
{
    // Validate the passenger value again in case the user changed the input
    var url = location.origin + '/taxiDevice/getTaxiDeviceList';
    jQuery.ajax({
        method: "POST",
        url: url,
        //data:{'get_type':'ajax_call'},
    }).done(function (response) {
        //jQuery('#content').html(response);
    });
}

function loadTaxiDevice( taxi_device_id , view_mode)
{
   // var url = location.origin + '/user/getDetailsById/' + user_id + '/' + view_mode;
    window.location.href =location.origin + '/taxiDevice/getDetailsById/' + taxi_device_id + '/' + view_mode;
    /*jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {id: user_id}
    }).done(function (response) {
        jQuery('#container').html(response.html);
        userDependency();
    });*/
}

function saveTaxiDevice() {
	
    var isValidate=false;
 // Validate the user details again in case the user changed the input
    var url = location.origin + '/taxiDevice/saveTaxiDevice/';
    
    var taxiDeviceData = {};
    var taxiDeviceData = new window.FormData($('#edit_taxi_device_form')[0]);
    /*jQuery( jQuery('#edit_passenger_form :input').serializeArray() ).each(function( x , y ){
    	passengerData[y.name] = y.value;
    });*/
   
    isValidate=validate('edit_taxi_device_form');
    if(isValidate)
    {
    	$("#overlay").removeClass('hidden');
    jQuery.ajax({
        xhr: function () {  
            return $.ajaxSettings.xhr();
        },
        type: "POST",
        data: taxiDeviceData,
        cache: false,
        contentType: false,
        processData: false,
        url: url,
    }).done(function (response) {
    	$("#overlay").addClass('hidden');
          response = jQuery.parseJSON(response);
        if(response.taxi_device_id)
        {
        	successMessage( response.msg);
        	//loadUserList();
        	window.location.href = location.origin + '/taxiDevice/getTaxiDeviceList';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	$("#overlay").addClass('hidden');
    	failureMessage( response.msg);
    	
    });
    }
    
}

function changeTaxiDeviceStatus(taxi_device_id,status)
{
	if (confirm("Make sure before changing taxi device status?."))
    {
    if(status==1)
    {
    	status=0;
    }
    else
    {
    	status=1;
    }
    
    var url = location.origin + '/taxiDevice/changeTaxiDeviceStatus/';
    $("#overlay").removeClass('hidden');
    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'id':taxi_device_id,'status':status},
    }).done(function (response) {
    	$("#overlay").addClass('hidden');
    	response=jQuery.parseJSON(response);
    	if(response.status > 0)
        {
        	
        	successMessage( response.msg);
        	//loadUserList();
        	window.location.href = location.origin + '/taxiDevice/getTaxiDeviceList';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	$("#overlay").addClass('hidden');
    	failureMessage( 'Status change failed.. Please try again later.');
    	
    });
    }
    
}

function deleteTaxiDevice(taxi_device_id)
{
	if (confirm("Make sure before deleting taxi device?."))
    {
    var url = location.origin + '/taxiDevice/deleteTaxiDevice/';
    $("#overlay").removeClass('hidden');
    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'id':taxi_device_id},
    }).done(function (response) {
    	$("#overlay").addClass('hidden');
    	response=jQuery.parseJSON(response);
    	if(response.status > 0)
        {
        	
        	successMessage( response.msg);
        	//loadUserList();
        	window.location.href = location.origin + '/TaxiDevice/getTaxiDeviceList';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	$("#overlay").addClass('hidden');
    	failureMessage( 'Delete failed.. Please try again later.');
    	
    });
    }
    
}

function checkTaxiDeviceMobile() {
	var mobile = $('#mobile').val();
	var taxi_device_id =$('#taxi_device_id').val();
	if (validateMobile(mobile)) {
		if (mobile != '') {
			$('#save-taxidevice-btn').prop('disabled', true);
			var url = location.origin + '/TaxiDevice/checkTaxiDeviceMobile/';
			$("#overlay").removeClass('hidden');
			jQuery.ajax({
				method : "POST",
				data : {
					'mobile' : mobile,
					'taxi_device_id':taxi_device_id
				},
				url : url
			}).done(function(response) {
				$("#overlay").addClass('hidden');
				response = jQuery.parseJSON(response);
				if (response.status == 1) {
					$('#mobile').val('');
				}
				$('#save-taxidevice-btn').prop('disabled', false);
				infoMessage(response.msg, 1, '#mobile');
			});
		}
	}
}