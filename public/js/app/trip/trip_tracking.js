/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    
    var pickup_latlong  = $('#tt_pickupLatLong').val();
    var drop_latlong    = $('#tt_dropLatLong').val();
    //var drop_longitude   = $('#tt_dropLatitude').val();
    //var drop_longitude   = $('#tt_dropLongitude').val();
    var trip_id           = $('#trip_id').val();   
     var p = pickup_latlong.split(",");
     var d = drop_latlong.split(",");
                var INTERVAL = 10000;
                //Used to remember markers
                var markerStore = {};
                var directionsDisplay;
                var directionsService = new google.maps.DirectionsService();
                var map;
                var infowindow        = new google.maps.InfoWindow();
                var marker, i;
                   
                function initializeT() {
                        map = new google.maps.Map(document.getElementById('map-canvasT'), {
                                zoom: 15,
                                center: new google.maps.LatLng(12.942117, 77.575363),
                                mapTypeId: google.maps.MapTypeId.ROADMAP
                    });
                        directionsDisplay = new google.maps.DirectionsRenderer();

                        directionsDisplay.setMap(map);
                      
                        var start   = new google.maps.LatLng(parseFloat(p[0]), parseFloat(p[1]));
                        var end     = new google.maps.LatLng(parseFloat(d[0]), parseFloat(d[1]));
                        var N       = '0.0333333';
                        var request = {
                            origin: start,
                            destination: end,
                            travelMode: 'DRIVING',
                                 drivingOptions: {
                                    departureTime: new Date(Date.now() + N),  // for the time N milliseconds from now.
                                    trafficModel: 'optimistic'
                                }
                        };
                        directionsService.route(request, function (response, status) {
                            if (status == google.maps.DirectionsStatus.OK) {
                                directionsDisplay.setDirections(response);
                            }
                        });
        
                    $('#tripTrackModal').on('show.bs.modal', function () {
                        //Must wait until the render of the modal appear, thats why we use the resizeMap and NOT resizingMap!! ;-)
                        getMarkers(map);
                    }); 
                   
                }
                
                //                function setMapOnAll(map) {
                //                    for (var i = 0; i < markers.length; i++) {
                //                        markers[i].setMap(map);
                //                    }
                //                }


                //                function clearMarkers() {
                //                    setMapOnAll(null);
                //                }


                google.maps.event.addDomListener(window, 'load', initializeT);
                google.maps.event.addDomListener(window, "resize", resizingMap());

                $('#tripTrackModal').on('show.bs.modal', function () {
                    //Must wait until the render of the modal appear, thats why we use the resizeMap and NOT resizingMap!! ;-)
                    resizeMap();
                });

                function resizeMap() {
                    if (typeof map == "undefined")
                        return;
                    setTimeout(function () {
                        resizingMap();
                    }, 400);
                }

                function resizingMap(pickup_latlong) {
                    if (typeof map == "undefined")
                        return;
                    var center = new google.maps.LatLng(parseFloat(p[0]), parseFloat(p[1]))
                    google.maps.event.trigger(map, "resize");
                    map.setCenter(center);
                    map.setZoom(12);
                }
                
                function getMarkers(map) {
                   
                    console.log('getMarkers');
                    $.post('../../getTripTrackingLatLong', {'trip_id':trip_id}, function(res,resp) {
			console.dir(res);
			for(var i=0, len=res.length; i<len; i++) {
                             var autocomplete_geocoder = new google.maps.Geocoder();
                                var coord1 = new google.maps.LatLng(res[i].position.lat, res[i].position.long);
                                var bounds = new google.maps.LatLngBounds();
                                                autocomplete_geocoder.geocode({
                                                                'latLng': coord1
                                                          }, function(results, status) {
                                                                if (status == google.maps.GeocoderStatus.OK) {
                                                                console.log(results);
                                                                  if (results[0]) {
                                                                        var arrAddress = results[0].address_components;
                                                                        var address = results[0].formatted_address;
                                                                         $('#marker_address').val(address);
                                                                  }
                                                                }
                                });
				//Do we have this marker already?
                                var infowindow = new google.maps.InfoWindow();
				if(markerStore.hasOwnProperty(res[i].id)) {
					console.log('just funna move it...');
					markerStore[res[i].id].setPosition(new google.maps.LatLng(res[i].position.lat,res[i].position.long));
                                        
                                     setTimeout(function(){
                                                        infowindow.close();
                                                    },5000);    
                                        
				} else {
					var marker = new google.maps.Marker({
						position: new google.maps.LatLng(res[i].position.lat,res[i].position.long),
						title:res[i].name,
						map:map
                                                //icon:'/public/images/app/taxi.png'
					});
                                      
                                        //var contentString = $('#marker_address').val();
                                            //var infowindow = new google.maps.InfoWindow();
                                                //infowindow.setContent(contentString);
                                                //infowindow.open(map, marker);
                                                google.maps.event.addListener(marker, 'click', function() {
                                                    infowindow.setContent($('#marker_address').val());
                                                    infowindow.open(marker.get('map'), marker);
                                                    setTimeout(function(){
                                                        infowindow.close();
                                                    },5000); 
                                                     

                                        });
                                       
					markerStore[res[i].id] = marker;
					console.log(marker.getTitle());
				}
                                
			}
		   var id = window.setTimeout(getMarkers,INTERVAL);
                   //clearTimeout(id);
                    $('#tripTrackModal').on('hidden.bs.modal', function () {
                        clearTimeout(id);
                   });
		}, "json");
	}

});