/*
	Auto Refresh Page with Time script
	By JavaScript Kit (javascriptkit.com)
	Over 200+ free scripts here!
	*/

	//enter refresh time in "minutes:seconds" Minutes should range from 0 to inifinity. Seconds should range from 0 to 59
	var limit="02:00"
	$('#manual-refresh').removeClass('hidden');
		$('#timer').removeClass('hidden');
	var parselimit=limit.split(":")
	parselimit=parselimit[0]*60+parselimit[1]*1

	

if (window.addEventListener)
		window.addEventListener("load", beginrefresh, false)
	else if (window.attachEvent)
		window.attachEvent("load", beginrefresh)

function beginrefresh(){
		if (parselimit==1)
			window.location.reload()
		else{ 
			parselimit-=1
			curmin=Math.floor(parselimit/60)
			cursec=parselimit%60
			if(curmin <= 9)
			{
				curmin='0'+curmin;
			}
			if(cursec <= 9)
			{
				cursec='0'+cursec;
			}
			if (curmin!=0)
				curtime=curmin+":"+cursec;
			else
				curtime="00:"+cursec;
			$('#timer_display').html(curtime);
			setTimeout("beginrefresh()",1000)
		}
	}
