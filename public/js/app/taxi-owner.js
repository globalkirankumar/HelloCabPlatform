$(document).ready(function() {
	taxiOwnerDependency();
	//alert('state');exit;
	//datatables
	table = $('#taxi-owner-data-table').DataTable({
		"scrollX" : true,
		"scrollY" : 400,
		"scrollCollapse" : true,
		"processing" : true, //Feature control the processing indicator.
		"serverSide" : true, //Feature control DataTables' server-side processing mode.
		"order" : [], //Initial no order.
"dom": 'Bflrtip',
        
        "buttons": [
                    /*{
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: [ 0, ':visible' ]
                    }
                    },*/
                // 'colvis',
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdfHtml5',
                    exportOptions: {
                    	columns: ':visible'
                    }
                },
                
        ],
		// Load data for the table's content from an Ajax source
		"ajax" : {
			"url" : location.origin + '/TaxiOwner/ajax_list',
			"type" : "POST"
		},

		//Set column definition initialisation properties.s
		
		"columnDefs": [
		               { 
		                   "targets": [ 0,-1,7], //last column
		                   "orderable": false, //set not orderable
		               },
		               ],
		               "lengthMenu": [[20, 50, 100], [20, 50, 100]],
		               "fnDrawCallback": function( oSettings ) {
		            	   taxiOwnerDependency();
		                 }

	});

	$('label input.toggle-vis').each(function() {
		var column = table.column($(this).attr('data-column'));
		if ($(this).prop('checked') == true) {
			//column.toggle(this.checked);
			column.visible(true);
		} else {
			column.visible(false);
		}
	});
	$('label input.toggle-vis').on('change', function(e) {
		e.preventDefault();
		// Get the column API object
		var column = table.column($(this).attr('data-column'));
		if ($(this).prop('checked') == true) {
			//column.toggle(this.checked);
			column.visible(true);
		} else {
			column.visible(false);
		}
		taxiOwnerDependency();
	});
});

function taxiOwnerDependency() {
	
	$('#save-taxiowner-btn').click(function() {
		saveTaxiOwner();
		});
		$('#cancel-taxiowner-btn').click(function() {
			// loadUserList(); 
			window.location.href = location.origin + '/TaxiOwner/getTaxiOwnerList';
		});
	$('[id^=viewtaxiowner]').click(function() {
		var arr = $(this).attr('id').split('-');
		var taxiowner_id = arr[1];
		loadTaxiOwner(taxiowner_id, 'view');
	});
	$('[id^=edittaxiowner]').click(function() {
		var arr = $(this).attr('id').split('-');
		var taxiowner_id = arr[1];
		loadTaxiOwner(taxiowner_id, 'edit');
	});
	$('[id^=deletetaxiowner]').click(function() {
		var arr = $(this).attr('id').split('-');
		var taxiowner_id = arr[1];
		deleteTaxiOwner(taxiowner_id);
	});
	
	$('#mobile').blur(function() {

		checkTaxiOwnerMobile();
	});
	$('#email').blur(function() {

		checkTaxiOwnerEmail();
	});

	/* $('#passenger-data-table').DataTable( {
	     "processing": true,
	     "serverSide": true,
	     "ajax": "scripts/objects.php",
	     "columns": [
	         { "data": "first_name" },
	         { "data": "last_name" },
	         { "data": "position" },
	         { "data": "office" },
	         { "data": "start_date" },
	         { "data": "salary" }
	     ]
	 } );*/
}

function transctionHistory(id) {
	var arr = id.split('-');
	var taxiOwner_id = arr[1];
	loadTransctionHistory(taxiOwner_id);
}

function changeTaxiOwnerStatus(id) {
	var taxiOwner_id = 0;
	var status = '';

	var arr = id.split('-');
	taxiOwner_id = arr[1];

	if (arr[0] == 'active') {
		status = 'Y';
	} else if (arr[0] == 'deactive') {
		status = 'N';
	}
	var url = location.origin + '/TaxiOwner/changeTaxiOwnerStatus/';
	$("#overlay").removeClass('hidden');
	jQuery.ajax({
		method : "POST",
		url : url,
		data : {
			'id' : taxiOwner_id,
			'status' : status
		},
	}).done(function(response) {
		$("#overlay").addClass('hidden');
		response = jQuery.parseJSON(response);
		if (response) {

			successMessage(response.msg);
			loadTaxiOwnerList();
		} else {
			failureMessage(response.msg);
		}

	}).fail(function(jqXHR, textStatus, errorThrown) {
		$("#overlay").addClass('hidden');
		failureMessage('Status change failed.. Please try again later.');

	});

}

function loadTaxiOwnerList() {
	// Validate the passenger value again in case the user changed the input
	var url = location.origin + '/TaxiOwner/getTaxiOwnerList/';
	jQuery.ajax({
		method : "POST",
		url : url,
	//data:{'get_type':'ajax_call'},
	}).done(function(response) {
		//jQuery('#content').html(response);
	});
}

function loadTaxiOwner(taxiOwner_id, view_mode) {
	
	window.location.href = location.origin + '/taxiOwner/getDetailsById/'
			+ taxiOwner_id + '/' + view_mode;
	/*
	 var url = location.origin + '/TaxiOwner/getDetailsById/' + taxiOwner_id
			+ '/' + view_mode;
	  jQuery.ajax({
		method : "POST",
		url : url,
		dataType : 'json',
		data : {
			id : taxiOwner_id
		}
	}).done(function(response) {
		jQuery('#taxiOwner-details-information').replaceWith(response.html);
		TaxiOwnerDependency();
	});*/
}

function saveTaxiOwner() {
	var isValidate = false;
	// Validate the passenger details again in case the user changed the input
	var url = location.origin + '/TaxiOwner/saveTaxiOwner/';

	var taxiOwnerData = {};
	var taxiOwnerData = new window.FormData($('#edit_taxiowner_form')[0]);
	/*jQuery( jQuery('#edit_passenger_form :input').serializeArray() ).each(function( x , y ){
		passengerData[y.name] = y.value;
	});*/
	isValidate = validate('edit_taxiowner_form');
	if (isValidate) {
		$("#overlay").removeClass('hidden');
		jQuery.ajax({
			xhr : function() {
				return $.ajaxSettings.xhr();
			},
			type : "POST",
			data : taxiOwnerData,
			cache : false,
			contentType : false,
			processData : false,
			url : url,
		}).done(
				function(response) {
					$("#overlay").addClass('hidden');
					response = jQuery.parseJSON(response);
					if (response.taxi_owner_id) {
						successMessage( response.msg);
						//loadPassengerList();
						window.location.href = location.origin + '/TaxiOwner/getTaxiOwnerList';
					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {
					$("#overlay").addClass('hidden');
			failureMessage(response.msg);

		});
	}

}
function checkTaxiOwnerEmail() {
	var email = $('#email').val();
	var taxi_owner_id =$('#taxi_owner_id').val();
	if (validateEmail(email)) {
		if (email != '') {
			$('#save-taxiowner-btn').prop('disabled', true);
			var url = location.origin + '/TaxiOwner/checkTaxiOwnerEmail/';
			$("#overlay").removeClass('hidden');
			jQuery.ajax({
				method : "POST",
				data : {
					'email' : email,
					'taxi_owner_id':taxi_owner_id
				},
				url : url
			}).done(function(response) {
				$("#overlay").addClass('hidden');
				response = jQuery.parseJSON(response);
				if (response.status == 1) {
					$('#email').val('');
				}
				$('#save-taxiowner-btn').prop('disabled', false);
				infoMessage(response.msg, 1, '#email');
			});
		}
	}
}
function checkTaxiOwnerMobile() {
	var mobile = $('#mobile').val();
	var taxi_owner_id =$('#taxi_owner_id').val();
	if (validateMobile(mobile)) {
		if (mobile != '') {
			$('#save-taxiowner-btn').prop('disabled', true);
			var url = location.origin + '/TaxiOwner/checkTaxiOwnerMobile/';
			$("#overlay").removeClass('hidden');
			jQuery.ajax({
				method : "POST",
				data : {
					'mobile' : mobile,
					'taxi_owner_id':taxi_owner_id
				},
				url : url
			}).done(function(response) {
				$("#overlay").addClass('hidden');
				response = jQuery.parseJSON(response);
				if (response.status == 1) {
					$('#mobile').val('');
				}
				$('#save-taxiowner-btn').prop('disabled', false);
				infoMessage(response.msg, 1, '#mobile');
			});
		}
	}
}
