$(document).ready(function () {
    tripDependency();
    //alert('state');exit;
    //datatables
    var entity_id = $('#entityId').val();
    var trip_type = $('#tripType').val();
    var trip_status = $('#tripStatus').val();
    var start_date = $('#startDate').val();
    var end_date = $('#endDate').val();
    var zone_id = $('#zoneListId').val();
    $('#dropZoneId').find('option').not(':first').remove();

    table = $('#trip-data-table').DataTable({
        "scrollX": true,
        "scrollY": 400,
        "scrollCollapse": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "dom": 'Bflrtip',
        "buttons": [
            /*{
             extend: 'copyHtml5',
             exportOptions: {
             columns: [ 0, ':visible' ]
             }
             },*/
            'colvis',
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },

        ],
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": location.origin + '/trip/ajax_list',
            "type": "POST",
            "data": {
                'trip_type': trip_type,
                'trip_status': trip_status,
                'entity_id': entity_id,
                'start_date': start_date,
                'end_date': end_date,
                'zone_id': zone_id
            }
        },

        //Set column definition initialisation properties.s
        "columnDefs": [{
            "targets": [0], //last column
            "orderable": false, //set not orderable
        },],
        "lengthMenu": [[20, 50, 100], [20, 50, 100]],
        "fnDrawCallback": function (oSettings) {
            tripDependency();
        },
        /*"createdRow": function(row, data, index) {
         // Add a class to the cell in the second column
         alert(data[0]);
         $(row).children(':nth-child(2)').addClass('clr');

         // Add a class to the row
         $(row).addClass('important');
         },*/
        "columnDefs": [
            {
                "targets": [1],
                "createdCell": function (td, cellData, rowData, row, col) {
                    //alert($(row).children(':nth-child(2)').val());
                    switch (cellData) {
                        case "None":
                            $(td).addClass('none');
                            break;
                        case "Booked":
                            $(td).addClass('booked');
                            break;
                        case "Driver Initiated":
                            $(td).addClass('driver-initiated');
                            break;
                        case "Driver Accepted":
                            $(td).addClass('driver-accepted');
                            break;
                        case "Driver Arrived":
                            $(td).addClass('driver-arrived');
                            break;
                        case "In Progress":
                            $(td).addClass('in-progress');
                            break;
                        case "Waiting For Payment":
                            $(td).addClass('waiting-for-payment');
                            break;
                        case "Trip Completed":
                            $(td).addClass('trip-completed');
                            break;
                        case "Cancelled By Passenger":
                            $(td).addClass('cancelled-by-passenger');
                            break;
                        case "Cancelled By Driver":
                            $(td).addClass('cancelled-by-driver');
                            break;
                        case "Driver Not Found":
                            $(td).addClass('driver-not-found');
                            break;
                        case "No notification":
                            $(td).addClass('no-notification');
                            break;
                        case "Queuing":
                            $(td).addClass('queuing');
                            break;
                        case "Driver rejected trip":
                            $(td).addClass('driver-rejected-trip');
                            break;

                    }
                }
            }
        ],
    });


    /***** Create Trip Details Google Map Starts ***/
    $('#load_gmap').click(function () {

        $('#load_gmap_modal').modal('show');
        $('#load_gmap_modal').on('shown.bs.modal', function () {
            google.maps.event.trigger(map, "resize");
        });

    });

    /***** Create Trip Details Google Map Ends ***/

});

function tripDependency() {
    //var table_driver=$('#driver-list-table').DataTable();
    /*** code to remove all zone option **/
    $('#zoneId').find('option').not(':first').remove();
    $('#subZoneId').find('option').not(':first').remove();
    $('#trip-details-btn').click(function () {
        table.destroy();
        getTripDetails();
    });
    $('#save-trip-btn').click(function () {
        if ($('#passengerId').val() != 0 && $('#passengerId').val() != '') {
            if ($('#dropLatitude').val() && $('#dropLongitude').val() && $('#dropLocation').val()) {
                saveTrip();
            }
            else {
                failureMessage('Please set Pickup & Drop Location');
            }
        }
        else {
            failureMessage('Please select existing passenger by autocomplete search / Create new passenger');
        }

    });
    $('#trip-estimate-btn').click(function () {
        tripEstimate();

    });
    $('#allocateTaxiToTrip').click(function () {
        var trip_id = $(this).attr('tripId');
        var driver_id = $(this).attr('driverId');
        var taxi_id = $(this).attr('taxiId');
        var driver_count = $(this).attr('driverCount');
        allocateTaxiToTrip(trip_id, driver_id, taxi_id, driver_count);
    });

    $('#driver-not-found-btn').click(function () {
        var trip_id = $(this).attr('tripId');
        driverNotFound(trip_id);
    });

    $('#cancel-trip-btn').click(function () {
        // loadUserList();
        window.location.href = location.origin + '/Trip/getTripList';
    });
    $('#driver-reject-btn').click(function () {
        if (confirm("Make sure before trip rejected by driver?.")) {
            driverTripReject();
        }
    });
    $('#driver-release-btn').click(function () {
        if (confirm("Make sure before release driver from trip Id?.")) {
            driverTripRelease();
        }
    });
    $('#passenger-reject-btn').click(function () {

        if (confirm("Make sure before trip rejected by passenger?.")) {
            passengerTripReject();
        }
    });
    $('#trip-start-btn').click(function () {

        if (confirm("Make sure before trip start?.")) {
            startTrip();
        }
    });
    $('#trip-end-btn').click(function () {

        if (confirm("Make sure before trip end?.")) {
            endTrip();
        }
    });

    $('[id^=viewtrip]').click(function () {
        var arr = $(this).attr('id').split('-');
        var trip_id = arr[1];
        loadTrip(trip_id, 'view');
    });
    $('[id^=edittrip]').click(function () {
        var arr = $(this).attr('id').split('-');
        var trip_id = arr[1];
        loadTrip(trip_id, 'edit');
    });


    $('#promoCode').blur(function () {

        validatePromocode();
    });
    $('#promoCode').focus(function () {
        $('#save-trip-btn').prop('disabled', true);
    });
   /* $('#passengerMobile').blur(function () {

        checkPassengerMobile();
    });*/
    $('#passengerEmail').blur(function () {

        checkPassengerEmail();
    });

    $('#startDate').datetimepicker({
        dayOfWeekStart: 1,
        lang: 'en',
        startDate: '0',
        timepicker: true,
        format: 'Y-m-d H:i',
        step: 60,
        maxDate: 0,
        onShow: function (ct) {
            this.setOptions({

                maxDate: $('#endDate').val() ? $('#endDate').val() : false,
                maxTime: $('#endDate').val() ? $('#endDate').val() : false,

            })
        },
        onSelectDate: function () {
            $('#endDate').attr('readonly', false);
            var d = $('#startDate').datetimepicker('getValue');

        }
    });

    $('#endDate').datetimepicker({
        dayOfWeekStart: 1,
        lang: 'en',
        startDate: '0',
        timepicker: true,
        format: 'Y-m-d H:i',
        step: 60,

        onShow: function (ct) {
            this.setOptions({

                minDate: $('#startDate').val() ? $('#startDate').val() : false,
                minTime: $('#startDate').val() ? $('#startDate').val() : false,

            })

        },
        onSelectDate: function () {
            var d = $('#endDate').datetimepicker('getValue');
            var now = new Date();
            var cur_date = [now.getFullYear(), '-', now.getMonth(), '-', now.getDate()].join('');

        }
    });

    $('#pickupDatetime').datetimepicker({
        dayOfWeekStart: 1,
        lang: 'en',
        startDate: '',
        format: 'Y-m-d H:i',
        minDate: 0,
        step: 15,
        onShow: function (ct) {
//           alert(ct);
            var trip_type = $('#tripType').val();
            var now = new Date();
            var currentDateTime = [
                now.getFullYear(),
                '-',
                now.getMonth() + 1,
                '-',
                now.getDate() + 7,
                ' ',
                now.getHours(),
                ':',
                now.getMinutes() + 60,
            ].join('');
            var mt = [now.getHours() + 1, ':', now.getMinutes()].join('');
            this.setOptions({
                minTime: 0
            })
            if (trip_type == 45) {
                this.setOptions({
                    maxDate: currentDateTime
                })
            }

        },
        onSelectDate: function () {


            var d = $('#pickupDatetime').datetimepicker('getValue');
            var now = new Date();
            var cur_date = [now.getFullYear(), '-', now.getMonth(), '-', now.getDate()].join('');
            //alert(cur_date);
            var selected_date = d.getFullYear() + '-' + d.getMonth() + '-' + d.getDate();
            if (cur_date == selected_date) {
                this.setOptions({
                    minTime: 0
                })
            } else {
                this.setOptions({
                    minTime: 1
                })
            }

        }
    });

    // get passenger Details
    $("#passengerMobile").keyup(function () {
        var keyword = $(this).val();
        if (keyword.length >= 3) {
            getPassengerDetailsForAutoSuggest(keyword, 'mobile');
        }
        if (keyword == '') {
            //$("#passengerMobile").val('');
            $("#passengerId").val('');
            //$("#passengerEmail").val('');
            //$("#passengerName").val('');
            $("#mobile-suggesstion-box").hide();

        }
    });
    $("#passengerEmail").keyup(function () {
        var keyword = $(this).val();
        if (keyword.length >= 3) {
            getPassengerDetailsForAutoSuggest(keyword, 'email');
        }
        if (keyword == '') {
            //$("#passengerMobile").val('');
            $("#passengerId").val('');
            //$("#passengerEmail").val('');
            //$("#passengerName").val('');
            $("#email-suggesstion-box").hide();
        }
    });

    $("#passengerName").keyup(function () {
        var keyword = $(this).val();
        if (keyword.length >= 3) {
            getPassengerDetailsForAutoSuggest(keyword, 'firstName');
        }
        if (keyword == '') {
            //$("#passengerMobile").val('');
            $("#passengerId").val('');
            //$("#passengerEmail").val('');
            //$("#passengerName").val('');
            $("#pname-suggesstion-box").hide();
        }
    });

    /***** Create Trip Details Google Map Starts ***/
    $('#load_gmap').click(function () {

        $('#load_gmap_modal').modal('show');
        $('#load_gmap_modal').on('shown.bs.modal', function () {
            google.maps.event.trigger(map, "resize");
        });

    });
    $('#myMapModal').on('hidden.bs.modal', function () {
        var latitude = $('#pickupLatitude').val();
        var longitude = $('#pickupLongitude').val();
        if (latitude != '' && longitude != '') {
            var address = $("#searchTextField").val();
            if (address != '') {
                $('#pickupLocation').val(address);
            }
            getZoneByLatLong(latitude, longitude);
            //getSubZoneByLatLong(latitude,longitude);

        }
    });
    $('#myMapModald').on('hidden.bs.modal', function () {
        var latitude = $('#dropLatitude').val();
        var longitude = $('#dropLongitude').val();
        if (latitude != '' && longitude != '') {
            var address = $("#dropTextField").val();
            if (address != '') {
                $('#dropLocation').val(address);
            }
            getDropZoneByLatLong(latitude, longitude);


        }
    });


    function getDropZoneByLatLong(latitude, longitude) {
        var url = location.origin + '/Zone/getZoneByLatLong';
        jQuery.ajax({
            method: "POST",
            url: url,
            data: {'latitude': latitude, 'longitude': longitude},
        }).done(function (response) {
            if (response) {
                var data = $.parseJSON(response);
                var zone_id = data['ZoneId'];
                var zone_name = data['Zone_name'];
                var subzone_id = data['subZoneId'];
                var subzone_name = data['subZone_name'];
                if (zone_id != '' && zone_id > 0) {
                    $('#dropZoneId').find('option').not(':first').remove();
                    $('#dropZoneId').append($('<option>', {
                        value: zone_id,
                        text: zone_name
                    }));
                    $('#dropZoneId').val(zone_id);
                } else {
                    // alert('No Zone Found');
                    $('#dropZoneId').val(1);
                    // return false;
                }
                if (subzone_id > 0) {
                    $('#dropSub_zone_sec').show();

                    $('#dropSubZoneId').append($('<option>', {
                        value: subzone_id,
                        text: subzone_name
                    }));
                    $('#dropSubZoneId').val(subzone_id);
                } else {
                    $('#dropSubZoneId').val('');
                    $('#dropSub_zone_sec').hide();
                }

            } else {
                //alert('No Zone Found');
                $('#dropZoneId').val(1);
            }
            //jQuery('#content').html(response);
        });
    }



    /***** Create Trip Details Google Map Ends ***/
    /** Manage PickupDate Time Based on Trip Type **/
    $('#tripType').on('change', function () {
        var trip_type = $(this).val();
        if (trip_type == 44) {
            var now = new Date();
            var currentDateTime = [
                now.getFullYear(),
                '-',
                now.getMonth() + 1,
                '-',
                now.getDate(),
                ' ',
                now.getHours(),
                ':',
                now.getMinutes() + 10,
            ].join('');
            $('#pickupDatetime').val(currentDateTime);
            $('#pickupDatetime').attr('readonly', true);
            $('#pickupDatetime').addClass('input-disabled');
        } else {
            $('#pickupDatetime').val('');
            $('#pickupDatetime').attr('readonly', false);
            $('#pickupDatetime').removeClass('input-disabled');
        }
    });
    /** Make pickup and Drop location readonly **/
    $('#pickupLocation').attr('readonly', true);
    $('#dropLocation').attr('readonly', true);


    /*** save guest passenger ***/
    $('#guest_passenger').on('click', function () {

        $('#passengerName').removeClass('error');
        $('#passengerMobile').removeClass('error');
        var g_passenger_name = $('#passengerName').val();
        var g_passenger_email = $('#passengerEmail').val();
        var g_passenger_mobile = $('#passengerMobile').val();
        var gflag;
        if (g_passenger_name == '') {
            $('#passengerName').addClass('error').focus();
            gflag = 0;
        } else if (g_passenger_mobile.length < 8) {
            $('#passengerMobile').addClass('error').focus();
            gflag = 0;
        } else {
            gflag = 1;
        }
        if (gflag > 0) {

            $("#overlay").removeClass('hidden');
            var url = location.origin + '/Passenger/save_guestpassenger';
            jQuery.ajax({
                method: "POST",
                url: url,
                data: {
                    'passengerName': g_passenger_name,
                    'passengerEmail': g_passenger_email,
                    'passengerMobile': g_passenger_mobile
                },
            }).done(function (response) {
                if (response > 0) {
                    $('#passengerId').val(response);
                    $("#overlay").addClass('hidden');
                    $('#guest_passenger').hide();
                    $('#passengerName').removeClass('error');
                    $('#passengerMobile').removeClass('error');
                    successMessage('Passenger Added Success');
                }
            });
        }


    });

}

function loadTripList() {
    // Validate the passenger value again in case the user changed the input
    var url = location.origin + '/driver/getDriverList';
    jQuery.ajax({
        method: "POST",
        url: url,
        //data:{'get_type':'ajax_call'},
    }).done(function (response) {

        //jQuery('#content').html(response);
    });
}

function loadTrip(trip_id, view_mode) {
    // var url = location.origin + '/user/getDetailsById/' + driver_id + '/' + view_mode;
    window.location.href = location.origin + '/Trip/getDetailsById/'
        + trip_id + '/' + view_mode;
    /*jQuery.ajax({
     method: "POST",
     url: url,
     dataType: 'json',
     data: {id: user_id}
     }).done(function (response) {
     jQuery('#container').html(response.html);
     userDependency();
     });*/
}

function saveTrip() {
    var table_driver = $('#driver-list-table').DataTable();
    var isValidate = false;
    // Validate the user details again in case the user changed the input
    var url = location.origin + '/Trip/saveTrip/';

    var userData = {};
    var userData = new window.FormData($('#edit_trip_form')[0]);
    /*jQuery( jQuery('#edit_passenger_form :input').serializeArray() ).each(function( x , y ){
     passengerData[y.name] = y.value;
     });*/

    isValidate = validate('edit_trip_form');
    if (isValidate) {
        $("#overlay").removeClass('hidden');
        jQuery.ajax({
            xhr: function () {
                return $.ajaxSettings.xhr();
            },
            type: "POST",
            data: userData,
            cache: false,
            contentType: false,
            processData: false,
            url: url,
        }).done(
            function (response) {
                $("#overlay").addClass('hidden');
                response = jQuery.parseJSON(response);

                if (response.trip_id > 0) {

                    if (response.dispatch_status > 0) {
                        successMessage(response.msg);
                        //loadUserList();
                        window.location.href = location.origin + '/Trip/getTripList';
                    }
                    else if (response.dispatch_status <= 0) {
                        $('#show-driver-list').addClass('hidden');
                        $('#show-driver-list').removeClass('hidden');
                        $('#driver-not-found-btn').attr('tripId', response.trip_id);
                        getDriverLists(response.trip_id);

                        $('#hide-trip-btn').removeClass('hidden');
                        $('#hide-trip-btn').addClass('hidden');
                        tripDependency();
                        /*$('#allocate-taxi-to-trip').modal('show');
                         //getDriverLists(response.trip_id);
                         $('#allocate-taxi-to-trip').on('shown.bs.modal', function ()
                         {
                         getDriverLists(response.trip_id);
                         });*/
                    }
                } else {
                    failureMessage(response.msg);
                }

            }).fail(function (jqXHR, textStatus, errorThrown) {
            $("#overlay").addClass('hidden');
            failureMessage(response.msg);

        });
    }

}

function tripEstimate() {

    var isValidate = false;
    // Validate the user details again in case the user changed the input
    var url = location.origin + '/Trip/tripEstimate/';

    var userData = {};
    var userData = new window.FormData($('#edit_trip_form')[0]);
    /*jQuery( jQuery('#edit_passenger_form :input').serializeArray() ).each(function( x , y ){
     passengerData[y.name] = y.value;
     });*/

    isValidate = validate('edit_trip_form');
    if (isValidate) {
        $("#overlay").removeClass('hidden');
        jQuery.ajax({
            xhr: function () {
                return $.ajaxSettings.xhr();
            },
            type: "POST",
            data: userData,
            cache: false,
            contentType: false,
            processData: false,
            url: url,
        }).done(
            function (response) {
                $("#overlay").addClass('hidden');
                response = jQuery.parseJSON(response);
                if (response.status) {
                    $('#estimate-distance').removeClass('hidden');
                    $('#estimate-amount').removeClass('hidden');
                    $('#estimate-time').removeClass('hidden');
                    $('#estimateDistance').val(response.distance);
                    $('#estimateAmount').val(response.total_trip_cost);
                    $('#estimateTime').val(response.time);
                    successMessage(response.message);

                } else {
                    failureMessage(response.message);
                }

            }).fail(function (jqXHR, textStatus, errorThrown) {
            $("#overlay").addClass('hidden');
            failureMessage(response.message);

        });
    }

}

function driverTripReject() {

    var url = location.origin + '/Trip/driverTripReject/';
    var trip_id = $('#trip_id').val();
    var driver_id = $('#driverId').val();
    var reject_type = STATUS_ACTIVE;
    var tripData = {'driver_id': driver_id, 'trip_id': trip_id, 'reject_type': reject_type};
    $("#overlay").removeClass('hidden');
    jQuery.ajax({
        method: "POST",
        data: tripData,
        url: url,
    }).done(
        function (response) {
            $("#overlay").addClass('hidden');
            response = jQuery.parseJSON(response);
            if (response.status >= STATUS_ACTIVE) {
                successMessage(response.message);
                //loadUserList();
                window.location.href = location.origin + '/Trip/getTripList';
            } else {
                failureMessage(response.message);
            }

        }).fail(function (jqXHR, textStatus, errorThrown) {
        $("#overlay").addClass('hidden');
        failureMessage(response.message);

    });

}
function driverTripRelease() {

    var url = location.origin + '/Trip/driverTripRelease/';
    var trip_id = $('#trip_id').val();
    var driver_id = $('#driverId').val();
    var reject_type = STATUS_ACTIVE;
    var tripData = {'driver_id': driver_id, 'trip_id': trip_id};
    $("#overlay").removeClass('hidden');
    jQuery.ajax({
        method: "POST",
        data: tripData,
        url: url,
    }).done(
        function (response) {
            $("#overlay").addClass('hidden');
            response = jQuery.parseJSON(response);
            if (response.status >= STATUS_ACTIVE) {
                successMessage(response.message);
                //loadUserList();
                window.location.href = location.origin + '/Trip/getTripList';
            } else {
                failureMessage(response.message);
            }

        }).fail(function (jqXHR, textStatus, errorThrown) {
        $("#overlay").addClass('hidden');
        failureMessage(response.message);

    });

}

function passengerTripReject() {

    var url = location.origin + '/Trip/passengerTripReject/';
    var trip_id = $('#trip_id').val();

    var tripData = {'trip_id': trip_id};
    $("#overlay").removeClass('hidden');
    jQuery.ajax({
        method: "POST",
        data: tripData,
        url: url,
    }).done(
        function (response) {
            $("#overlay").addClass('hidden');
            response = jQuery.parseJSON(response);
            if (response.status >= STATUS_ACTIVE) {
                successMessage(response.message);
                //loadUserList();
                window.location.href = location.origin + '/Trip/getTripList';
            } else {
                failureMessage(response.message);
            }

        }).fail(function (jqXHR, textStatus, errorThrown) {
        $("#overlay").addClass('hidden');
        failureMessage(response.message);

    });

}
function startTrip() {

    var url = location.origin + '/Trip/tripStart/';
    var trip_id = $('#trip_id').val();
    var driver_id = $('#driverId').val();
    var tripData = {'driver_id': driver_id, 'trip_id': trip_id};
    $("#overlay").removeClass('hidden');
    jQuery.ajax({
        method: "POST",
        data: tripData,
        url: url,
    }).done(
        function (response) {
            $("#overlay").addClass('hidden');
            response = jQuery.parseJSON(response);
            if (response.status >= STATUS_ACTIVE) {
                successMessage(response.message);
                //loadUserList();
                window.location.href = location.origin + '/Trip/getTripList';
            } else {
                failureMessage(response.message);
            }

        }).fail(function (jqXHR, textStatus, errorThrown) {
        $("#overlay").addClass('hidden');
        failureMessage(response.message);

    });

}
function endTrip() {

    var url = location.origin + '/Trip/tripEnd/';

    var trip_id = $('#trip_id').val();

    var tripData = {'trip_id': trip_id};
    $("#overlay").removeClass('hidden');
    jQuery.ajax({
        method: "POST",
        data: tripData,
        url: url,
    }).done(
        function (response) {
            $("#overlay").addClass('hidden');
            response = jQuery.parseJSON(response);
            if (response.status >= STATUS_ACTIVE) {
                successMessage(response.message);
                //loadUserList();
                window.location.href = location.origin + '/Trip/getTripList';
            } else {
                failureMessage(response.message);
            }

        }).fail(function (jqXHR, textStatus, errorThrown) {
        $("#overlay").addClass('hidden');
        failureMessage(response.message);

    });

}

function getPassengerDetailsForAutoSuggest(keyword, type) {

    var url = location.origin + '/trip/getPassengerInfo/';
    if (keyword.length >= 3) {
        $.ajax({
            type: "POST",
            url: url,
            data: {'keyword': keyword, 'type': type},
            success: function (data) {
                if (type == 'mobile') {
                    $("#mobile-suggesstion-box").show();
                    $("#mobile-suggesstion-box").html(data);
                    if (data == '') {
                        $('#guest_passenger').show();
                    } else {
                        $('#guest_passenger').hide();
                    }
                }
                if (type == 'email') {
                    $("#email-suggesstion-box").show();
                    $("#email-suggesstion-box").html(data);
                }
                if (type == 'firstName') {
                    $("#pname-suggesstion-box").show();
                    $("#pname-suggesstion-box").html(data);
                }

            }
        });
    }

}
function selectPassenger(passengerId, passengerMobile, passengerEmail, passengerName) {
    if (passengerId > 0) {
        $("#passengerMobile").val(passengerMobile);
        $("#passengerId").val(passengerId);
        $("#passengerEmail").val(passengerEmail);
        $("#passengerName").val(passengerName);
    }
    $("#mobile-suggesstion-box").hide();
    $("#email-suggesstion-box").hide();
    $("#pname-suggesstion-box").hide();
}

function getZoneByLatLong(latitude, longitude) {
    var url = location.origin + '/Zone/getZoneByLatLong';
    jQuery.ajax({
        method: "POST",
        url: url,
        data: {'latitude': latitude, 'longitude': longitude},
    }).done(function (response) {
        if (response) {
            var data = $.parseJSON(response);
            var zone_id = data['ZoneId'];
            var zone_name = data['Zone_name'];
            var subzone_id = data['subZoneId'];
            var subzone_name = data['subZone_name'];
            if (zone_id != '' && zone_id > 0) {
                $('#zoneId').find('option').not(':first').remove();
                $('#zoneId').append($('<option>', {
                    value: zone_id,
                    text: zone_name
                }));
                $('#zoneId').val(zone_id);
            } else {
                // alert('No Zone Found');
                $('#zoneId').val(1);
                // return false;
            }
            if (subzone_id > 0) {
                $('#sub_zone_sec').show();

                $('#subZoneId').append($('<option>', {
                    value: subzone_id,
                    text: subzone_name
                }));
                $('#subZoneId').val(subzone_id);
            } else {
                $('#subZoneId').val('');
                $('#sub_zone_sec').hide();
            }

        } else {
            //alert('No Zone Found');
            $('#zoneId').val(1);
        }
        //jQuery('#content').html(response);
    });
}

function getSubZoneByLatLong(latitude, longitude) {
    var url = location.origin + '/Zone/getSubZoneByLatLong';
    jQuery.ajax({
        method: "POST",
        url: url,
        data: {'latitude': latitude, 'longitude': longitude},
    }).done(function (response) {
        if (response) {
            var data = $.parseJSON(response);
            var subzone_id = data['ZoneId'];
            if (subzone_id != '' && subzone_id > 0) {
                $('#sub_zone_sec').show();
                $('#subZoneId').val(subzone_id);
            }
        } else {
            $('#subZoneId').val('');
            $('#sub_zone_sec').hide();
        }
        //jQuery('#content').html(response);
    });
}

function validatePromocode() {
    var promocode = $('#promoCode').val();
    var passenger_id = ($('#passengerId').val()) ? $('#passengerId').val() : 0;
    var entity_id = $('#entityId').val();
    var zone_id = ($('#subZoneId').val())?$('#subZoneId').val():$('#zoneId').val();
    $('#save-trip-btn').prop('disabled', false);
    if (promocode != '') {

        var url = location.origin + '/Trip/validatePromocode/';

        jQuery.ajax({
            method: "POST",
            data: {'promocode': promocode, 'passenger_id': passenger_id,'zoneId':zone_id,'entityId':entity_id},
            url: url
        }).done(function (response) {

            response = jQuery.parseJSON(response);
            if (response.status == 1) {
                successMessage(response.message, 1, '#promoCode');
            }
            else if (response.status < 1) {
                $('#promoCode').val('');
                failureMessage(response.message, 1, '#promoCode');
            }

        });
    }

}

function checkPassengerEmail() {
    var email = $('#passengerEmail').val();
    if (validateEmail(email)) {
        if (email != '') {

            var url = location.origin + '/passenger/checkPassengerEmail/';
            $("#overlay").removeClass('hidden');
            jQuery.ajax({
                method: "POST",
                data: {'email': email},
                url: url
            }).done(function (response) {
                $("#overlay").addClass('hidden');
                response = jQuery.parseJSON(response);
                if (response.status == 1) {
                    $('#passengerEmail').val('');
                }

                infoMessage(response.msg, 1, '#passengerEmail');
            });
        }
    }
}
function checkPassengerMobile() {
    var mobile = $('#passengerMobile').val();
    if (validateMobile(mobile)) {
        if (mobile != '') {

            var url = location.origin + '/passenger/checkPassengerMobile/';
            $("#overlay").removeClass('hidden');
            jQuery.ajax({
                method: "POST",
                data: {'mobile': mobile},
                url: url
            }).done(function (response) {
                $("#overlay").addClass('hidden');
                response = jQuery.parseJSON(response);
                if (response.status == 1) {
                    $('#passengerMobile').val('');
                }

                infoMessage(response.msg, 1, '#passengerMobile');
            });
        }
    }
}

function getTripDetails() {

    var entity_id = $('#entityId').val();
    var trip_type = $('#tripType').val();
    var trip_status = $('#tripStatus').val();
    var start_date = $('#startDate').val();
    var end_date = $('#endDate').val();
    var zone_id = $('#zoneListId').val();

    table = $('#trip-data-table').DataTable({
        "scrollX": true,
        "scrollY": 400,
        "scrollCollapse": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "dom": 'Bflrtip',
        "buttons": [
            /*{
             extend: 'copyHtml5',
             exportOptions: {
             columns: [ 0, ':visible' ]
             }
             },*/
            'colvis',
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },

        ],
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": location.origin + '/trip/ajax_list',
            "type": "POST",
            "data": {
                'trip_type': trip_type,
                'trip_status': trip_status,
                'entity_id': entity_id,
                'start_date': start_date,
                'end_date': end_date,
                'zone_id': zone_id
            }
        },

        //Set column definition initialisation properties.s
        "columnDefs": [{
            "targets": [0], //last column
            "orderable": false, //set not orderable
        },],
        "lengthMenu": [[20, 50, 100], [20, 50, 100]],
        "fnDrawCallback": function (oSettings) {
            tripDependency();
        },
        /*"createdRow": function(row, data, index) {
         // Add a class to the cell in the second column
         alert(data[0]);
         $(row).children(':nth-child(2)').addClass('clr');

         // Add a class to the row
         $(row).addClass('important');
         },*/
        "columnDefs": [
            {
                "targets": [1],
                "createdCell": function (td, cellData, rowData, row, col) {
                    //alert($(row).children(':nth-child(2)').val());
                    switch (cellData) {
                        case "None":
                            $(td).addClass('none');
                            break;
                        case "Booked":
                            $(td).addClass('booked');
                            break;
                        case "Driver Initiated":
                            $(td).addClass('driver-initiated');
                            break;
                        case "Driver Accepted":
                            $(td).addClass('driver-accepted');
                            break;
                        case "Driver Arrived":
                            $(td).addClass('driver-arrived');
                            break;
                        case "In Progress":
                            $(td).addClass('in-progress');
                            break;
                        case "Waiting For Payment":
                            $(td).addClass('waiting-for-payment');
                            break;
                        case "Trip Completed":
                            $(td).addClass('trip-completed');
                            break;
                        case "Cancelled By Passenger":
                            $(td).addClass('cancelled-by-passenger');
                            break;
                        case "Cancelled By Driver":
                            $(td).addClass('cancelled-by-driver');
                            break;
                        case "Driver Not Found":
                            $(td).addClass('driver-not-found');
                            break;
                        case "No notification":
                            $(td).addClass('no-notification');
                            break;
                        case "Queuing":
                            $(td).addClass('queuing');
                            break;
                        case "Driver rejected trip":
                            $(td).addClass('driver-rejected-trip');
                            break;

                    }
                }
            }
        ],
    });

}

function getDriverLists(trip_id) {
    var table_driver = $('#driver-list-table').DataTable({
        "destroy": true,
        "scrollX": true,
        "scrollY": 400,
        "scrollCollapse": true,
        "processing": true, // Feature control the processing indicator.
        "serverSide": true, // Feature control DataTables' server-side
        //"responsive": true,
        // processing mode.
        "order": [], // Initial no order.

        //"dom" : 'CBflrtip',
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": location.origin + '/Trip/trip_driver_ajax_list',
            "type": "POST",
            "data": {'trip_id': trip_id}
        },

        // Set column definition initialisation properties.s
        "columnDefs": [{
            "targets": [-1, 0], // last column
            "orderable": false, // set not orderable
        },],
        "lengthMenu": [[20, 50, 100, -1], [20, 50, 100, 'All']],
        "fnDrawCallback": function (oSettings) {
            tripDependency();
        },

    });
}

function allocateTaxiToTrip(trip_id, driver_id, taxi_id, driver_count) {
    // Validate the user details again in case the user changed the input
    var url = location.origin + '/Trip/allocateTaxiToTrip/';
    if (trip_id && driver_id && taxi_id && driver_count) {
        $("#overlay").removeClass('hidden');
        jQuery.ajax({
            type: "POST",
            data: {"trip_id": trip_id, "driver_id": driver_id, "taxi_id": taxi_id, "driver_count": driver_count},
            url: url,
        }).done(
            function (response) {
                $("#overlay").addClass('hidden');
                response = jQuery.parseJSON(response);
                if (response.status > 0) {
                    successMessage(response.msg);
                    $('#show-driver-list').addClass('hidden');
                    $('#hide-trip-btn').removeClass('hidden');
                    window.location.href = location.origin + '/Trip/getTripList';

                } else {
                    failureMessage(response.msg);
                }

            }).fail(function (jqXHR, textStatus, errorThrown) {
            $("#overlay").addClass('hidden');
            failureMessage(response.message);

        });
    }
}

function driverNotFound(trip_id) {
    // Validate the user details again in case the user changed the input
    var url = location.origin + '/Trip/driverNotFound/';

    if (trip_id) {
        $("#overlay").removeClass('hidden');
        jQuery.ajax({
            type: "POST",
            data: {"trip_id": trip_id},
            url: url,
        }).done(
            function (response) {
                $("#overlay").addClass('hidden');
                response = jQuery.parseJSON(response);
                if (response.status > 0) {
                    successMessage(response.msg);
                    $('#show-driver-list').addClass('hidden');
                    $('#hide-trip-btn').removeClass('hidden');
                    window.location.href = location.origin + '/Trip/getTripList';

                } else {
                    failureMessage(response.msg);
                }

            }).fail(function (jqXHR, textStatus, errorThrown) {
            $("#overlay").addClass('hidden');
            failureMessage(response.msg);

        });
    }
}