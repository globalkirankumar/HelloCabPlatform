/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
   getPZones(); 
   
});

function getPZones() {
        var url    = location.origin+'/Zone/getFullZoneList';
		jQuery.ajax({
                    method : "POST",
                    url : url,
                    data : {},
		}).done(
                function(response) {
                        response = jQuery.parseJSON(response);
                       $.each(response, function(key, val) {
                          var zone_id   = val['zoneId'];
                          var zone_name = val['name'];
                          var option = $('<option/>');
                            option.attr('value', zone_id).text(zone_name);
                            $('#header_zone').append(option);
                    });
                    $("#header_zone").selectpicker("refresh");
                    var url = location.href;
                    var segments = url.split( '/' );
                    var zone_select_filter    = segments[6];
                     if(zone_select_filter > 0){
                        $('#header_zone').selectpicker('val', zone_select_filter);
                     }else{
                        $('#header_zone').selectpicker('val', ''); 
                     }  
                   
                    $("#header_zone").prop("disabled",false);
                    }).fail(function(jqXHR, textStatus, errorThrown) {
                         failureMessage('Unable to Fetch.. Please try again later.');
                }); 
                $('#sub_zone_sec').hide();
    }