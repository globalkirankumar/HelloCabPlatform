function driverDependency() {
	$('#save-driver-btn').click(function() {
		saveDriver();
	});
	$('#cancel-driver-btn').click(function() {
		// loadUserList(); 
		window.location.href = location.origin + '/driver/getDriverList';
	});
	$('[id^=status]').click(function() {
		var status = $(this).attr('status');
		var arr = $(this).attr('id').split('-');
		var driver_id = arr[1];

		changeDriverStatus(driver_id, status);
	});
	$('[id^=isverify]').click(function() {
		var status = $(this).attr('status');
		var arr = $(this).attr('id').split('-');
		var driver_id = arr[1];

		changeDriverVerifyStatus(driver_id, status);
	});
	$('[id^=logstatus]').click(function() {
		var status = $(this).attr('status');
		var arr = $(this).attr('id').split('-');
		var driver_id = arr[1];

		changeDriverLogStatus(driver_id, status);
	});
	$('[id^=viewdriver]').click(function() {
		var arr = $(this).attr('id').split('-');
		var driver_id = arr[1];
		loadDriver(driver_id, 'view');
	});
	$('[id^=editdriver]').click(function() {
		var arr = $(this).attr('id').split('-');
		var driver_id = arr[1];
		loadDriver(driver_id, 'edit');
	});
	$('[id^=deletedriver]').click(function() {
		var arr = $(this).attr('id').split('-');
		var driver_id = arr[1];
		deleteDriver(driver_id);
	});
	
	$('[id^=allocatetaxi]').click(function(){
		var allocate_status=$(this).attr('allocateStatus');
		var driver_name=$(this).attr('driverName');
		var taxi_device_id=$(this).attr('taxiDeviceId');
		var taxi_device_code=$(this).attr('taxiDeviceCode');
		var taxi_registration_no=$(this).attr('taxiRegistrationNo');
		var taxi_id=$(this).attr('taxiId');
		var arr = $(this).attr('id').split('-');
		
	    var driver_id = arr[1];
	    $('#allocate-taxi-device').modal('show');
	    $('#tempDriverId').val(driver_id);
	    $('#show-driver-name').html(driver_name);
	    if(allocate_status == STATUS_ACTIVE)
	    {
		    $('#taxiId').append($('<option>', {
		        value: taxi_id,
		        text: taxi_registration_no
		    }));
		    $('#taxiDeviceId').append($('<option>', {
		        value: taxi_device_id,
		        text: taxi_device_code
		    }));
	    }
	 
	    $("#taxiId option[value="+taxi_id+"]").attr("selected", "selected");
	    $("#taxiDeviceId option[value="+taxi_device_id+"]").attr("selected", "selected");
	  
	});
	$('#save-allocate-taxi-btn').click(function() {
		allocateTaxiToDriver();
	});
	
	
	$('#mobile').blur(function() {

		checkDriverMobile();
	});
	$('#email').blur(function() {

		checkDriverEmail();
	});

	$('#dob').datetimepicker({
		dayOfWeekStart : 1,
		lang : 'en',
		startDate : '0',
		timepicker : false,
		closeOnDateSelect : true,
		format : 'Y-m-d',
		scrollMonth : false,
		scrollInput : false,
		maxDate : 0
	});

	$('#licenseExpiryDate').datetimepicker({
		dayOfWeekStart : 1,
		lang : 'en',
		startDate : '',
		timepicker : false,
		closeOnDateSelect : true,
		scrollMonth : false,
		scrollInput : false,
		format : 'Y-m-d',
		minDate : 0
	});
}

function loadDriverList() {
	// Validate the passenger value again in case the user changed the input
	var url = location.origin + '/driver/getDriverList';
	jQuery.ajax({
		method : "POST",
		url : url,
	//data:{'get_type':'ajax_call'},
	}).done(function(response) {
		//jQuery('#content').html(response);
	});
}

function loadDriver(driver_id, view_mode) {
	// var url = location.origin + '/user/getDetailsById/' + driver_id + '/' + view_mode;
	window.location.href = location.origin + '/driver/getDetailsById/'
			+ driver_id + '/' + view_mode;
	/*jQuery.ajax({
	    method: "POST",
	    url: url,
	    dataType: 'json',
	    data: {id: user_id}
	}).done(function (response) {
	    jQuery('#container').html(response.html);
	    userDependency();
	});*/
}

function saveDriver() {

	var isValidate = false;
	// Validate the user details again in case the user changed the input
	var url = location.origin + '/driver/saveDriver/';

	var userData = {};
	var userData = new window.FormData($('#edit_driver_form')[0]);
	/*jQuery( jQuery('#edit_passenger_form :input').serializeArray() ).each(function( x , y ){
		passengerData[y.name] = y.value;
	});*/

	isValidate = validate('edit_driver_form');
	if (isValidate) {
		jQuery.ajax({
			xhr : function() {
				return $.ajaxSettings.xhr();
			},
			type : "POST",
			data : userData,
			cache : false,
			contentType : false,
			processData : false,
			url : url,
		}).done(
				function(response) {
					response = jQuery.parseJSON(response);
					if (response.driver_id) {
						successMessage(response.msg);
						//loadUserList();
						window.location.href = location.origin
								+ '/driver/getDriverList';
					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {
			failureMessage(response.msg);

		});
	}

}

function changeDriverStatus(driver_id, status) {
	if (confirm("Make sure before changing driver status?.")) {
		if (status == 1) {
			status = 0;
		} else {
			status = 1;
		}

		var url = location.origin + '/driver/changeDriverStatus/';

		jQuery.ajax({
			method : "POST",
			url : url,
			data : {
				'id' : driver_id,
				'status' : status
			},
		}).done(
				function(response) {
					response = jQuery.parseJSON(response);
					if (response.status > 0) {

						successMessage(response.msg);
						//loadUserList();
						window.location.href = location.origin
								+ '/driver/getDriverList';
					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {

			failureMessage('Status change failed.. Please try again later.');

		});
	}

}
function changeDriverVerifyStatus(driver_id, status) {
	if (confirm("Make sure before changing driver verify status?.")) {
		if (status == 1) {
			status = 0;
		} else {
			status = 1;
		}

		var url = location.origin + '/driver/changeDriverVerifyStatus/';

		jQuery.ajax({
			method : "POST",
			url : url,
			data : {
				'id' : driver_id,
				'status' : status
			},
		}).done(
				function(response) {
					response = jQuery.parseJSON(response);
					if (response.status > 0) {

						successMessage(response.msg);
						//loadUserList();
						window.location.href = location.origin
								+ '/driver/getDriverList';
					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {

			failureMessage('Status change failed.. Please try again later.');

		});
	}

}
function changeDriverLogStatus(driver_id, status) {
	if (confirm("Make sure before changing driver log status?.")) {
		if (status == 1) {
			status = 0;
		} else {
			status = 1;
		}

		var url = location.origin + '/driver/changeDriverLogStatus/';

		jQuery.ajax({
			method : "POST",
			url : url,
			data : {
				'id' : driver_id,
				'status' : status
			},
		}).done(
				function(response) {
					response = jQuery.parseJSON(response);
					if (response.status > 0) {

						successMessage(response.msg);
						//loadDriverList();
						window.location.href = location.origin
								+ '/driver/getDriverList';
					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {

			failureMessage('Status change failed.. Please try again later.');

		});
	}

}

function deleteDriver(driver_id) {
	if (confirm("Make sure before deleting driver?.")) {
		var url = location.origin + '/driver/deleteDriver/';

		jQuery.ajax({
			method : "POST",
			url : url,
			data : {
				'id' : driver_id
			},
		}).done(
				function(response) {
					response = jQuery.parseJSON(response);
					if (response.status > 0) {

						successMessage(response.msg);
						//loadDriverList();
						window.location.href = location.origin
								+ '/driver/getDriverList';
					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {

			failureMessage('Delete failed.. Please try again later.');

		});
	}

}
function checkDriverEmail() {
	var email = $('#email').val();

	if (validateEmail(email)) {
		if (email != '') {
			$('#save-driver-btn').prop('disabled', true);
			var url = location.origin + '/driver/checkDriverEmail/';
			jQuery.ajax({
				method : "POST",
				data : {
					'email' : email
				},
				url : url
			}).done(function(response) {
				response = jQuery.parseJSON(response);
				if (response.status == 1) {
					$('#email').val('');
				}
				$('#save-driver-btn').prop('disabled', false);
				infoMessage(response.msg, 1, '#email');
			});
		}
	}
}
function checkDriverMobile() {
	var mobile = $('#mobile').val();

	if (validateMobile(mobile)) {
		if (mobile != '') {
			$('#save-driver-btn').prop('disabled', true);
			var url = location.origin + '/driver/checkDriverMobile/';
			jQuery.ajax({
				method : "POST",
				data : {
					'mobile' : mobile
				},
				url : url
			}).done(function(response) {
				response = jQuery.parseJSON(response);
				if (response.status == 1) {
					$('#mobile').val('');
				}
				$('#save-driver-btn').prop('disabled', false);
				infoMessage(response.msg, 1, '#mobile');
			});
		}
	}
}

function allocateTaxiToDriver() {

	var isValidate = false;
	// Validate the driver allocate details
	var url = location.origin + '/driver/AllocateTaxiToDriver/';

	var allocateData = {};
	var allocateData = new window.FormData($('#edit_drive_allocate_form')[0]);
	
	isValidate = validate('edit_drive_allocate_form');
	if (isValidate) {
		jQuery.ajax({
			xhr : function() {
				return $.ajaxSettings.xhr();
			},
			type : "POST",
			data : allocateData,
			cache : false,
			contentType : false,
			processData : false,
			url : url,
		}).done(
				function(response) {
					response = jQuery.parseJSON(response);
					if (response.status > STATUS_DEACTIVE ) {
						 $('#allocate-taxi-device').modal('hide');
						
						window.location.href = location.origin
								+ '/driver/getDriverList';
					} 
					else if(response.status == STATUS_DEACTIVE){
						 $('#allocate-taxi-device').modal('hide');
						infoMessage(response.msg);
						window.location.href = location.origin
						+ '/driver/getDriverList';
					}else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {
			

		});
	}

}