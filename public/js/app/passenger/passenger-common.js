function passengerDependency()
{ 
	
     $("#email").blur(function(){checkPassengerEmail();});
     $("#mobile").blur(function(){checkPassengerMobile();});
     
     $('#save-passenger-btn').click(function(){
		 savePassenger(); 
	});
	$('#cancel-passenger-btn').click(function(){
		// loadUserList(); 
		 window.location.href = location.origin + '/passenger/getPassengerList';
	});
	/*$('[id^=status]').click(function(){
		var status=$(this).attr('status');
		var arr = $(this).attr('id').split('-');
	    var passenger_id = arr[1];
	    changePassengerStatus(passenger_id,status); 
	});*/
	
	$('[id^=viewpassenger]').click(function(){
		var arr = $(this).attr('id').split('-');
	    var passenger_id = arr[1];
	    loadPassenger(passenger_id,'view'); 
	});
	$('[id^=editpassenger]').click(function(){
		var arr = $(this).attr('id').split('-');
	    var passenger_id = arr[1];
	    loadPassenger(passenger_id,'edit'); 
	});
	/*$('[id^=deletepassenger]').click(function(){
		var arr = $(this).attr('id').split('-');
	    var passenger_id = arr[1];
	    deleteDriver(passenger_id);
	});*/
	$('#mobile').blur(function(){
		
		 checkPassengerMobile(); 
	});
	$('#email').blur(function(){
		
		checkPassengerEmail(); 
	});
}

function checkPassengerEmail()
{
	var email=$('#email').val();
	if (validateEmail(email)) {
	if(email != '')
	{
		$('#save-passenger-btn').prop('disabled', true);
    var url = location.origin + '/passenger/checkPassengerEmail/'; 
    jQuery.ajax({
        method: "POST",
        data:{'email':email},
        url: url
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response.status==1)
    	{
    		$('#email').val('');
    	}
    	$('#save-passenger-btn').prop('disabled', false);
    	infoMessage(response.msg,1,'#email');
    });
	}
	}
}
function checkPassengerMobile()
{
	var mobile=$('#mobile').val();
	if (validateMobile(mobile)) {
	if(mobile != '')
	{
		$('#save-passenger-btn').prop('disabled', true);
    var url = location.origin + '/passenger/checkPassengerMobile/'; 
    jQuery.ajax({
        method: "POST",
        data:{'mobile':mobile},
        url: url
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response.status==1)
    	{
    		$('#mobile').val('');
    	}
    	$('#save-passenger-btn').prop('disabled', false);
    	infoMessage(response.msg,1,'#mobile');
    });
	}
	}
}
function changePassengerStatus(id)
{
	var passenger_id=0;
	var status='';
	
	var arr = id.split('-');
    passenger_id = arr[1];
    
    if(arr[0]=='active')
    {
    	status='Y';
    }
    else if(arr[0]=='deactive')
    {
    	status='N';
    }
    var url = location.origin + '/passenger/changePassengerStatus/';
    
    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'id':passenger_id,'status':status},
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response)
        {
        	
        	successMessage( response.msg);
        	loadPassengerList();
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	
    	failureMessage( 'Status change failed.. Please try again later.');
    	
    });
    
}

function loadPassengerList()
{
    var url = location.origin + '/passenger/getPassengerList/';
    jQuery.ajax({
        method: "POST",
        url: url,
    }).done(function (response) {
        jQuery('#content').html(response);
        dataTableLoader();
    });
}

function loadPassenger( passenger_id , view_mode)
{
  //  var url = location.origin + '/passenger/getDetailsById/' + passenger_id + '/' + view_mode;
    window.location.href =location.origin + '/passenger/getDetailsById/' + passenger_id + '/' + view_mode;
   /* jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {id: passenger_id}
    }).done(function (response) {
        jQuery('#content').html(response.html);
        passengerDependency();
    });*/
}
function loadTransctionHistory( passenger_id )
{
    var url = location.origin + '/wallet/getWalletList/';
    
   var response =  jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {'passenger_id': passenger_id,'get_type':'ajax_call'},
        async:false
    }).responseText;
    if(response){
        jQuery('#passenger-details-information').replaceWith(response);
        dataTableLoader();
    }else{
        failureMessage('Please Try Again Later');
    }
}

function savePassenger() {
    var isValidate=false;
 // Validate the passenger details again in case the user changed the input
    var url = location.origin + '/passenger/savePassenger/';
    
    var passengerData = {};
    var passengerData = new FormData($('#edit_passenger_form')[0]);
    //passengerData.append($('#profileImage').files[0], file);
    /*jQuery( jQuery('#edit_passenger_form :input').serializeArray() ).each(function( x , y ){
    	passengerData[y.name] = y.value;
    });*/
    isValidate=validate('edit_passenger_form');
    if(isValidate)
    {
    //$('#save_psassenger_btn').button('loading');
    
    jQuery.ajax({
    	xhr: function () {  
            return $.ajaxSettings.xhr();
        },
        type: "POST",
        data: passengerData,
        cache: false,
        contentType: false,
        processData: false,
        url: url,
    }).done(function (response) {
          response = jQuery.parseJSON(response);
        if(response.passenger_id)
        {
        	successMessage( response.msg);
        	//loadPassengerList();
        	window.location.href = location.origin + '/passenger/getPassengerList/';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	failureMessage( response.msg);
    	
    });
    }
    
}