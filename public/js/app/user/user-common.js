function userDependency()
{
	$('#save-user-btn').click(function(){
		 saveUser(); 
	});
	$('#cancel-user-btn').click(function(){
		// loadUserList(); 
		 window.location.href = location.origin + '/user/getUserList';
	});
	$('[id^=status]').click(function(){
		var status=$(this).attr('status');
		var arr = $(this).attr('id').split('-');
	    var user_id = arr[1];
	    changeUserStatus(user_id,status); 
	});
	$('[id^=viewuser]').click(function(){
		var arr = $(this).attr('id').split('-');
	    var user_id = arr[1];
		 loadUser(user_id,'view'); 
	});
	$('[id^=edituser]').click(function(){
		var arr = $(this).attr('id').split('-');
	    var user_id = arr[1];
		 loadUser(user_id,'edit'); 
	});
	$('[id^=deleteuser]').click(function(){
		var arr = $(this).attr('id').split('-');
	    var user_id = arr[1];
	    deleteUser(user_id);
	});
	$('#mobile').blur(function(){
		
		 checkUserMobile(); 
	});
	$('#loginEmail').blur(function(){
		
		checkUserLoginEmail(); 
	});
$('#roleType').change(function(){
		
		roleTypeControl(); 
	});
	
	$('#dob').datetimepicker({
	       dayOfWeekStart : 1,
	       lang           : 'en',
	       startDate      : '0',
	       timepicker     : false,
	       closeOnDateSelect : true,
	       format         : 'Y-m-d',
	       scrollMonth : false,
	       scrollInput : false,
	       maxDate:0
	   });
	   
	   $('#doj').datetimepicker({
	       dayOfWeekStart : 1,
	       lang           : 'en',
	       startDate      : '',
	       timepicker     : false,
	       closeOnDateSelect : true,
	       scrollMonth : false,
	       scrollInput : false,
	       format         : 'Y-m-d'
	    	  
	       
	   });
	   $('#dor').datetimepicker({
	       dayOfWeekStart : 1,
	       lang           : 'en',
	       startDate      : '',
	       timepicker     : false,
	       closeOnDateSelect : true,
	       scrollMonth : false,
	       scrollInput : false,
	       format         : 'Y-m-d',
           onShow:function( ct ){
               if($('#doj').val()!='') {
                   this.setOptions({
                    minDate:jQuery('#doj').val()?jQuery('#doj').val():false,
                   })
               }
              }
	   });
	   
}

function loadUserList()
{
    // Validate the passenger value again in case the user changed the input
    var url = location.origin + '/user/getUserList';
    jQuery.ajax({
        method: "POST",
        url: url,
        //data:{'get_type':'ajax_call'},
    }).done(function (response) {
        //jQuery('#content').html(response);
    });
}

function loadUser( user_id , view_mode)
{
   // var url = location.origin + '/user/getDetailsById/' + user_id + '/' + view_mode;
    window.location.href =location.origin + '/user/getDetailsById/' + user_id + '/' + view_mode;
    /*jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {id: user_id}
    }).done(function (response) {
        jQuery('#container').html(response.html);
        userDependency();
    });*/
}

function saveUser() {
	
    var isValidate=false;
 // Validate the user details again in case the user changed the input
    var url = location.origin + '/user/saveUser/';
    
    var userData = {};
    var userData = new window.FormData($('#edit_user_form')[0]);
    /*jQuery( jQuery('#edit_passenger_form :input').serializeArray() ).each(function( x , y ){
    	passengerData[y.name] = y.value;
    });*/
   
    isValidate=validate('edit_user_form');
    if(isValidate)
    {
    jQuery.ajax({
        xhr: function () {  
            return $.ajaxSettings.xhr();
        },
        type: "POST",
        data: userData,
        cache: false,
        contentType: false,
        processData: false,
        url: url,
    }).done(function (response) {
          response = jQuery.parseJSON(response);
        if(response.user_id)
        {
        	successMessage( response.msg);
        	//loadUserList();
        	window.location.href = location.origin + '/user/getUserList';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	failureMessage( response.msg);
    	
    });
    }
    
}

function changeUserStatus(user_id,status)
{
	if (confirm("Make sure before changing user status?."))
    {
    if(status==1)
    {
    	status=0;
    }
    else
    {
    	status=1;
    }
    
    var url = location.origin + '/user/changeUserStatus/';
    
    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'id':user_id,'status':status},
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response.status > 0)
        {
        	
        	successMessage( response.msg);
        	//loadUserList();
        	window.location.href = location.origin + '/user/getUserList';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	
    	failureMessage( 'Status change failed.. Please try again later.');
    	
    });
    }
    
}

function deleteUser(user_id)
{
	if (confirm("Make sure before deleting user?."))
    {
    var url = location.origin + '/user/deleteUser/';
    
    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'id':user_id},
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response.status > 0)
        {
        	
        	successMessage( response.msg);
        	//loadUserList();
        	window.location.href = location.origin + '/user/getUserList';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	
    	failureMessage( 'Delete failed.. Please try again later.');
    	
    });
    }
    
}
function checkUserLoginEmail()
{
	var email=$('#loginEmail').val();
	if (validateEmail(email)) {
	if(email != '')
	{
		$('#save-user-btn').prop('disabled', true);
    var url = location.origin + '/user/checkUserLoginEmail/'; 
    jQuery.ajax({
        method: "POST",
        data:{'email':email},
        url: url
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response.status==1)
    	{
    		$('#loginEmail').val('');
    	}
    	$('#save-user-btn').prop('disabled', false);
    	infoMessage(response.msg,1,'#loginEmail');
    });
	}
	}
}
function checkUserMobile()
{
	var mobile=$('#mobile').val();
	if (validateMobile(mobile)) {
	if(mobile != '')
	{
		$('#save-user-btn').prop('disabled', true);
    var url = location.origin + '/user/checkUserMobile/'; 
    jQuery.ajax({
        method: "POST",
        data:{'mobile':mobile},
        url: url
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response.status==1)
    	{
    		$('#mobile').val('');
    	}
    	$('#save-user-btn').prop('disabled', false);
    	infoMessage(response.msg,1,'#mobile');
    });
	}
	}
}
function roleTypeControl()
{
	var role_type=$('#roleType').val();
	$('#role-user').addClass('hidden');
	$('#video-file').addClass('hidden');
	if(role_type == 7)
	{
		$('#role-user').removeClass('hidden');
	}
	
}