$(document).ready(function() {
 var now = new Date();
    var cur_date      =  [now.getFullYear(),'-',now.getMonth(),'-',now.getDate(),'--',now.getHours(),'-',now.getMinutes(),'-',now.getSeconds()].join('');
	var buttonCommon = {
	        exportOptions: {
	            format: {
	                body: function ( data, row, column, node ) {
	                    // Strip $ from salary column to make it numeric
	                    return column === 5 ?
	                        data.replace( /[$,]/g, '' ) :
	                        data;
	                }
	            }
	        }
	    };
	notificationDependency();
	// alert('state');exit;
	// datatables
	table = $('#notification-data-table').DataTable({ 
		 
   	 "scrollX": true,
   	 "scrollY": 400,
   	"scrollCollapse": true,
       "processing": true, //Feature control the processing indicator.
       "serverSide": true, //Feature control DataTables' server-side processing mode.
       "order": [], //Initial no order.
       "dom": 'Bflrtip',
       
       "buttons": [
                   
               {
                   extend: 'excelHtml5',
                   exportOptions: {
                       columns:[1,2,3,4,5,6]
                   }
               },
              
       ],
       // Load data for the table's content from an Ajax source
       "ajax": {
           "url": location.origin +'/Notification/ajax_list',
           "type": "POST"
       },

       //Set column definition initialisation properties.
       "columnDefs": [
       { 
           "targets": [ -1,0], //last column
           "orderable": false, //set not orderable
       },
       
       ],
       "lengthMenu": [[20, 50, 100], [20, 50, 100]],
       "fnDrawCallback": function( oSettings ) {
    	   notificationDependency();
         }
       

   });
	
	$('label input.toggle-vis').each(function() {
		var column = table.column($(this).attr('data-column'));
		if ($(this).prop('checked') == true) {
			// column.toggle(this.checked);
			column.visible(true);
		} else {
			column.visible(false);
		}

	});
	$('label input.toggle-vis').on('change', function(e) {
		e.preventDefault();
		// Get the column API object
		var column = table.column($(this).attr('data-column'));
		if ($(this).prop('checked') == true) {
			// column.toggle(this.checked);
			column.visible(true);
		} else {
			column.visible(false);
		}
		notificationDependency();

	});
});

function notificationDependency() {
        // date time picker script
   
    
	$('#save-notification-btn').click(function() {
		saveNotification(1);
	});
	$('#send-notification-btn').click(function() {
		saveNotification(0);
	});
	$('#cancel-notification-btn').click(function() {
		// loadUserList();
		window.location.href = location.origin + '/Notification/getNotificationList';
	});
	$('[id^=status]').click(function() {
		var status = $(this).attr('status');
		var arr = $(this).attr('id').split('-');
		var notification_id = arr[1];

		changeNotificationStatus(notification_id, status);
	});
	
	$('[id^=viewnotification]').click(function() {
		var arr = $(this).attr('id').split('-');
		var notification_id = arr[1];
		loadNotification(notification_id, 'view');
	});
	$('[id^=editnotification]').click(function() {
		var arr = $(this).attr('id').split('-');
		var notification_id = arr[1];
		loadNotification(notification_id, 'edit');
	});
	$('[id^=deletenotification]').click(function() {
		var arr = $(this).attr('id').split('-');
		var notification_id = arr[1];
		deleteNotification(notification_id);
	});
	
	$('#startDatetime').datetimepicker({
	       dayOfWeekStart : 1,
	       lang           : 'en',
	       startDate      : '0',
	       timepicker     : true,
	       format         : 'Y-m-d H:i:s',
	       step           :15,
	       minDate: 0,
	       onShow:function( ct ){
			   this.setOptions({
				minTime: 0,
			    maxDate:$('#endDatetime').val()?$('#endDatetime').val():false,
			    
			   })
	       },
	       onSelectDate:function(){
	            var d = $('#startDatetime').datetimepicker('getValue');
	             var now = new Date();
	             var cur_date      =  [now.getFullYear(),'-',now.getMonth(),'-',now.getDate()].join('');
	             var selected_date = d.getFullYear()+'-'+d.getMonth()+'-'+d.getDate();
	                if(cur_date==selected_date){
	                   this.setOptions({
	                     minTime       : 0
	                 })
	              }else{
	                 this.setOptions({
	                     minTime       : 1
	                 }) 
	              }
	        }  
	   });
	   $('#endDatetime').datetimepicker({
	       dayOfWeekStart : 1,
	       lang           : 'en',
	       startDate      : '0',
	       timepicker     : true,
	       format         : 'Y-m-d H:i:s',
	       step           :15,  
	           onShow:function( ct ){
	                   this.setOptions({
	                    minDate:$('#startDatetime').val()?$('#startDatetime').val():false,
	                    maxTime:$('#startDatetime').val()?$('#startDatetime').val():false,
	                   })
	               
	              },
	              onSelectDate:function(){
			            var d = $('#endDatetime').datetimepicker('getValue');
			             var now = new Date();
			             var cur_date      =  [now.getFullYear(),'-',now.getMonth(),'-',now.getDate()].join('');
			             var selected_date = d.getFullYear()+'-'+d.getMonth()+'-'+d.getDate();
			                if(cur_date==selected_date){
			                   this.setOptions({
			                	   maxTime       : 0
			                 })
			              }else{
			                 this.setOptions({
			                	 maxTime       : 1
			                 }) 
			              }
			        } 
	   });

}

function loadNotificationList() {
	// Validate the passenger value again in case the user changed the input
	var url = location.origin + '/Notification/getNotificationList';
	jQuery.ajax({
		method : "POST",
		url : url,
	// data:{'get_type':'ajax_call'},
	}).done(function(response) {
           
		// jQuery('#content').html(response);
	});
}

function loadNotification(notification_id, view_mode) {
	// var url = location.origin + '/user/getDetailsById/' + driver_id + '/' +
	// view_mode;
	window.location.href = location.origin + '/Notification/getDetailsById/'
			+ notification_id + '/' + view_mode;
	/*
	 * jQuery.ajax({ method: "POST", url: url, dataType: 'json', data: {id:
	 * user_id} }).done(function (response) {
	 * jQuery('#container').html(response.html); userDependency(); });
	 */
}
 
function saveNotification($is_save) {

	var isValidate = false;
	// Validate the user details again in case the user changed the input
	var url = location.origin + '/Notification/saveNotification/'+$is_save;

	var userData = {};
	var userData = new window.FormData($('#edit_notification_form')[0]);
	/*
	 * jQuery( jQuery('#edit_passenger_form :input').serializeArray()
	 * ).each(function( x , y ){ passengerData[y.name] = y.value; });
	 */

	isValidate = validate('edit_notification_form');
	if (isValidate) {
		$("#overlay").removeClass('hidden');
		jQuery.ajax({
			xhr : function() {
				return $.ajaxSettings.xhr();
			},
			type : "POST",
			data : userData,
			cache : false,
			contentType : false,
			processData : false,
			url : url,
		}).done(
				
				function(response) {
					$("#overlay").addClass('hidden');
					response = jQuery.parseJSON(response);
					if (response.notification_id) {
						
                                                successMessage(response.msg);
                                                 $('#edit_notification_form')[0].reset();
						// loadUserList();
						 window.location.href = location.origin + '/Notification/getNotificationList';
                                                
					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {
					$("#overlay").addClass('hidden');
			failureMessage("Try agian later!!!!");

		});
	}

}

function changeNotificationStatus(notification_id, status) {
	if (confirm("Make sure before changing notification status?.")) {
		if (status == 1) {
			status = 0;
		} else {
			status = 1;
		}

		var url = location.origin + '/Notification/changeNotificationStatus/';
		$("#overlay").removeClass('hidden');
		jQuery.ajax({
			method : "POST",
			url : url,
			data : {
				'id' : notification_id,
				'status' : status
			},
		}).done(
				
				function(response) {
					$("#overlay").addClass('hidden');
					response = jQuery.parseJSON(response);
					if (response.status > 0) {

						successMessage(response.msg);
						// loadUserList();
						window.location.href = location.origin
								+ '/Notification/getNotificationList';
					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {
					$("#overlay").addClass('hidden');
			failureMessage('Status change failed.. Please try again later.');

		});
	}

}

function deleteNotification(notification_id) {
	if (confirm("Make sure before deleting notification?.")) {
		var url = location.origin + '/Notification/deleteNotification/';
		$("#overlay").removeClass('hidden');
		jQuery.ajax({
			method : "POST",
			url : url,
			data : {
				'id' : notification_id
			},
		}).done(
				
				function(response) {
					$("#overlay").addClass('hidden');
					response = jQuery.parseJSON(response);
					if (response.status > 0) {

						successMessage(response.msg);
						// loadDriverList();
						window.location.href = location.origin
								+ '/Notification/getNotificationList';
					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {
					$("#overlay").addClass('hidden');
			failureMessage('Delete failed.. Please try again later.');

		});
	}

}
