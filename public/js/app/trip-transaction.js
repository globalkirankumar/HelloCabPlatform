$(document).ready(function() {
	transactionDependency();
	//alert('state');exit;
	//datatables
	var entity_id=$('#entityId').val();
	var start_date=$('#startDate').val();
	var end_date=$('#endDate').val();
	var zone_id=$('#zoneListId').val();
	
	table = $('#trip-transaction-data-table').DataTable({

		"scrollX" : true,
		"scrollY" : 400,
		"scrollCollapse" : true,
		"processing" : true, //Feature control the processing indicator.
		"serverSide" : true, //Feature control DataTables' server-side processing mode.
		"order" : [], //Initial no order.
"dom": 'Bflrtip',
        
        "buttons": [
                    /*{
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: [ 0, ':visible' ]
                    }
                    },*/
                 'colvis',
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdfHtml5',
                    exportOptions: {
                    	columns: ':visible'
                    }
                },
                
        ],
		// Load data for the table's content from an Ajax source
		"ajax" : {
			"url" : location.origin + '/TripTransaction/ajax_list',
			"type" : "POST",
			 "data":{'entity_id':entity_id,'start_date':start_date,'end_date':end_date,'zone_id':zone_id}
		},

		//Set column definition initialisation properties.s
		"columnDefs" : [ {
			"targets" : [0], //last column
			"orderable" : false, //set not orderable
		}, ],
		"lengthMenu" : [ [ 20, 50, 100,-1 ], [ 20, 50, 100,'All' ] ],
		"fnDrawCallback" : function(oSettings) {
			transactionDependency();
		}
	});

	$('label input.toggle-vis').each(function() {
		var column = table.column($(this).attr('data-column'));
		if ($(this).prop('checked') == true) {
			//column.toggle(this.checked);
			column.visible(true);
		} else {
			column.visible(false);
		}

	});
	$('label input.toggle-vis').on('change', function(e) {
		e.preventDefault();
		// Get the column API object
		var column = table.column($(this).attr('data-column'));
		if ($(this).prop('checked') == true) {
			//column.toggle(this.checked);
			column.visible(true);
		} else {
			column.visible(false);
		}
		transactionDependency();

	});
});

function transactionDependency() {
	
	$('[id^=viewtriptransaction]').click(function() {
		var arr = $(this).attr('id').split('-');
		var transaction_id = arr[1];
		loadTransaction(transaction_id, 'view');
	});
	
	$('#transaction-details-btn').click(function() {
		table.destroy();
		getTransactionDetails();
	});
	 $('#startDate').datetimepicker({
	       dayOfWeekStart : 1,
	       lang           : 'en',
	       startDate      : '0',
	       timepicker     : true,
		   format         : 'Y-m-d H:i',
	       step           :60,
	       maxDate: 0,
	       onShow:function( ct ){
			   this.setOptions({
				
			    maxDate:$('#endDate').val()?$('#endDate').val():false,
			    maxTime:$('#endDate').val()?$('#endDate').val():false,
			    
			   })
	       },
	       onSelectDate:function(){
	    	   $('#endDate').attr('readonly',false);
	            var d = $('#startDate').datetimepicker('getValue');
	             
	        }  
	   });
	 
	$('#endDate').datetimepicker({
	       dayOfWeekStart : 1,
	       lang           : 'en',
	       startDate      : '0',
	       timepicker     : true,
	       format         : 'Y-m-d H:i',
	       step           :60,
	       
	           onShow:function( ct ){
	                   this.setOptions({
	                	
	                    minDate:$('#startDate').val()?$('#startDate').val():false,
	                    minTime:$('#startDate').val()?$('#startDate').val():false,
	                   
	                   })
	               
	              },
	              onSelectDate:function(){
			            var d = $('#endDate').datetimepicker('getValue');
			             var now = new Date();
			             var cur_date      =  [now.getFullYear(),'-',now.getMonth(),'-',now.getDate()].join('');
			            
			        } 
	   });
}

function loadTripTransactionList() {
	// Validate the passenger value again in case the user changed the input
	var url = location.origin + '/TripTransaction/getTripTransactionList';
	jQuery.ajax({
		method : "POST",
		url : url,
	//data:{'get_type':'ajax_call'},
	}).done(function(response) {
           
		//jQuery('#content').html(response);
	});
}

function loadTransaction(transaction_id, view_mode) {
	// var url = location.origin + '/user/getDetailsById/' + driver_id + '/' + view_mode;
	window.location.href = location.origin + '/TripTransaction/getDetailsById/'
			+ transaction_id + '/' + view_mode;
	/*jQuery.ajax({
	    method: "POST",
	    url: url,
	    dataType: 'json',
	    data: {id: user_id}
	}).done(function (response) {
	    jQuery('#container').html(response.html);
	    userDependency();
	});*/
}

function getTransactionDetails()
{
	var entity_id=$('#entityId').val();
	var start_date=$('#startDate').val();
	var end_date=$('#endDate').val();
	var zone_id=$('#zoneListId').val();
	
	table = $('#trip-transaction-data-table').DataTable({

		"scrollX" : true,
		"scrollY" : 400,
		"scrollCollapse" : true,
		"processing" : true, //Feature control the processing indicator.
		"serverSide" : true, //Feature control DataTables' server-side processing mode.
		"order" : [], //Initial no order.
		"dom": 'Bflrtip',
        
        "buttons": [
                    /*{
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: [ 0, ':visible' ]
                    }
                    },*/
                 'colvis',
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdfHtml5',
                    exportOptions: {
                    	columns: ':visible'
                    }
                },
                
        ],
		// Load data for the table's content from an Ajax source
		"ajax" : {
			"url" : location.origin + '/TripTransaction/ajax_list',
			"type" : "POST",
			 "data":{'entity_id':entity_id,'start_date':start_date,'end_date':end_date,'zone_id':zone_id}
		},

		//Set column definition initialisation properties.s
		"columnDefs" : [ {
			"targets" : [0], //last column
			"orderable" : false, //set not orderable
		}, ],
		"lengthMenu" : [ [ 20, 50, 100,-1 ], [ 20, 50, 100,'All' ] ],
		"fnDrawCallback" : function(oSettings) {
			transactionDependency();
		}
	});
}
