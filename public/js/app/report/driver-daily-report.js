$(document).ready(function() {

	driverDailyReportDependency();
	
});

function driverDailyReportDependency() {
	
	$('#driver-daily-report-btn').click(function() {
		getDriverDailyReport();
	});
	
	$('#reportDate').datetimepicker({
	       dayOfWeekStart : 1,
	       lang           : 'en',
	       startDate      : '0',
	       timepicker     : true,
	       format         : 'Y-m-d'
	   });
	/*$('#report_date').on('change',function(){
		getDriverDailyReport();
	});*/
}

function getDriverDailyReport() {

	var isValidate = false;
	isValidate = validate('driver_daily_report_form');
	//isValidate=1;
	if (isValidate) {
		//alert('test');
		var report_date=$('#reportDate').val();
		table = $('#driver-daily-data-table').DataTable({
			 "destroy": true,
			"scrollX" : true,
			"scrollY" : 400,
			"scrollCollapse" : true,
			"processing" : true, //Feature control the processing indicator.
			"serverSide" : true, //Feature control DataTables' server-side processing mode.
			"order" : [], //Initial no order.
	               
	               "dom": 'Bflrtip',
	               "buttons": [
	                           /*{
	                           extend: 'copyHtml5',
	                           exportOptions: {
	                               columns: [ 0, ':visible' ]
	                           }
	                           },*/
	                       {
	                           extend: 'excelHtml5',
	                           title: 'Cancelled_Trip_List',
	                           exportOptions: {
	                        	   columns: ':visible'
	                           }
	                       },
	                       /*{
	                           extend: 'pdfHtml5',
	                           exportOptions: {
	                               columns: [1, 2, 3,4,5,6,7,8,9 ]
	                           }
	                       },*/
	                       //'colvis'
	               ],
	              
			// Load data for the table's content from an Ajax source
			"ajax" : {
				"url" : location.origin + '/Report/ajax_list_driverdaily',
				"type" : "POST",
				"data":{'report_date':report_date}
	                     
			},

			//Set column definition initialisation properties.s
			"columnDefs" : [ {
				"targets" : [], //last column
				"orderable" : false, //set not orderable
			}, ],
			"lengthMenu" : [ [ 100, 200, 500,-1 ], [ 100, 200, 500,'All' ] ]
			
		});
	}
	else
	{
		failuremessage("Pleses select report date");
	}
}