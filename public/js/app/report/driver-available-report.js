$(document).ready(function() {
	driverAvailableReportDependency();
	
	var log_status=$('#logStatus').val();
	var available_status=$('#availableStatus').val();
	//var time_period=$('#timePeriod').val();
	table = $('#driver-available-data-table').DataTable({
		 "destroy": true,
		"scrollX" : true,
		"scrollY" : 400,
		"scrollCollapse" : true,
		"processing" : true, //Feature control the processing indicator.
		"serverSide" : true, //Feature control DataTables' server-side processing mode.
		"order" : [], //Initial no order.
                
                "dom": 'Bflrtip',
                "buttons": [
                            /*{
                            extend: 'copyHtml5',
                            exportOptions: {
                                columns: [ 0, ':visible' ]
                            }
                            },*/
                        {
                            extend: 'excelHtml5',
                            title: 'Driver_Available_Report',
                            exportOptions: {
                            	columns: ':visible'
                            }
                        },
                        /*{
                            extend: 'pdfHtml5',
                            exportOptions: {
                                columns: [1, 2, 3,4,5,6,7,8,9 ]
                            }
                        },*/
                        //'colvis'
                ],
               
		// Load data for the table's content from an Ajax source
		"ajax" : {
			"url" : location.origin + '/Report/ajax_list_driveravailable',
			"type" : "POST",
                       "data":{'log_status':log_status,'available_status':available_status}//'time_period':time_period,
		},

		//Set column definition initialisation properties.s
		"columnDefs" : [ {
			"targets" : [], //last column
			"orderable" : false, //set not orderable
		}, ],
		"lengthMenu" : [ [ 100, 200, 500,-1 ], [ 100, 200, 500,'All' ] ]
		
	});

	
});

function driverAvailableReportDependency() {
	
	$('#driver-available-report-btn').click(function() {
		getDriverAvailableReport();
	});
	$('#logStatus').on('change',function(){//alert('test');
		$('#availableStatus').val('');
		$('#available-status').removeClass('hidden');
		if($('#logStatus').val()>0)
		{
			$('#available-status').removeClass('hidden');
		}
		else
		{
			$('#available-status').addClass('hidden');
		}
	});
}

function getDriverAvailableReport() {

	var isValidate = false;
	/*var reportData = {};
	var reportData = new window.FormData($('#driver_transaction_report_form')[0]);
	
	 * jQuery( jQuery('#edit_passenger_form :input').serializeArray()
	 * ).each(function( x , y ){ passengerData[y.name] = y.value; });
	 */
	//console.log(reportData);
	isValidate = validate('driver_available_report_form');
	//isValidate=1;
	if (isValidate) {
		//alert('test');
		var log_status=$('#logStatus').val();
		var available_status=$('#availableStatus').val();
		//var time_period=$('#timePeriod').val();
		table = $('#driver-available-data-table').DataTable({
			 "destroy": true,
			"scrollX" : true,
			"scrollY" : 400,
			"scrollCollapse" : true,
			"processing" : true, //Feature control the processing indicator.
			"serverSide" : true, //Feature control DataTables' server-side processing mode.
			"order" : [], //Initial no order.
	                
	                "dom": 'Bflrtip',
	                "buttons": [
	                            /*{
	                            extend: 'copyHtml5',
	                            exportOptions: {
	                                columns: [ 0, ':visible' ]
	                            }
	                            },*/
	                        {
	                            extend: 'excelHtml5',
	                            title: 'Driver_Available_Report',
	                            exportOptions: {
	                            	columns: ':visible'
	                            }
	                        },
	                        /*{
	                            extend: 'pdfHtml5',
	                            exportOptions: {
	                                columns: [1, 2, 3,4,5,6,7,8,9 ]
	                            }
	                        },*/
	                        //'colvis'
	                ],
	               
			// Load data for the table's content from an Ajax source
			"ajax" : {
				"url" : location.origin + '/Report/ajax_list_driveravailable',
				"type" : "POST",
	                       "data":{'log_status':log_status,'available_status':available_status}//'time_period':time_period,
			},

			//Set column definition initialisation properties.s
			"columnDefs" : [ {
				"targets" : [], //last column
				"orderable" : false, //set not orderable
			}, ],
			"lengthMenu" : [ [ 100, 200, 500,-1 ], [ 100, 200, 500,'All' ] ]
			
		});
	}

}
