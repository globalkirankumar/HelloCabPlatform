$(document).ready(function() {
	driverRejectReportDependency();
	
});

function driverRejectReportDependency() {
	
	$('#driver-reject-report-btn').click(function() {
		getDriverRejectReport();
	});
	
}

function getDriverRejectReport() {

	var isValidate = false;
	/*var reportData = {};
	var reportData = new window.FormData($('#driver_transaction_report_form')[0]);
	
	 * jQuery( jQuery('#edit_passenger_form :input').serializeArray()
	 * ).each(function( x , y ){ passengerData[y.name] = y.value; });
	 */
	//console.log(reportData);
	isValidate = validate('driver_reject_report_form');
	//isValidate=1;
	if (isValidate) {
		//alert('test');
		var time_period=$('#timePeriod').val();
		var consecutive_reject=$('#consecutiveReject').val();
		table = $('#driver-reject-data-table').DataTable({
			 "destroy": true,
			"scrollX" : true,
			"scrollY" : 400,
			"scrollCollapse" : true,
			"processing" : true, //Feature control the processing indicator.
			"serverSide" : true, //Feature control DataTables' server-side processing mode.
			"order" : [], //Initial no order.
	                
	                "dom": 'Bflrtip',
	                "buttons": [
	                            /*{
	                            extend: 'copyHtml5',
	                            exportOptions: {
	                                columns: [ 0, ':visible' ]
	                            }
	                            },*/
	                        {
	                            extend: 'excelHtml5',
	                            title: 'Driver_Consecutive_Reject_Report',
	                            exportOptions: {
	                            	columns: ':visible'
	                            }
	                        },
	                        /*{
	                            extend: 'pdfHtml5',
	                            exportOptions: {
	                                columns: [1, 2, 3,4,5,6,7,8,9 ]
	                            }
	                        },*/
	                        //'colvis'
	                ],
	               
			// Load data for the table's content from an Ajax source
			"ajax" : {
				"url" : location.origin + '/Report/ajax_list_driverreject',
				"type" : "POST",
	                       "data":{'time_period':time_period,'consecutive_reject':consecutive_reject}
			},

			//Set column definition initialisation properties.s
			"columnDefs" : [ {
				"targets" : [], //last column
				"orderable" : false, //set not orderable
			}, ],
			"lengthMenu" : [ [ 100, 200, 500,-1 ], [ 100, 200, 500,'All' ] ]
			
		});
	}

}
