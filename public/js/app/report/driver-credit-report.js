$(document).ready(function() {
	driverCreditReportDependency();
	table = $('#driver-credit-data-table').DataTable({
		 "destroy": true,
		"scrollX" : true,
		"scrollY" : 400,
		"scrollCollapse" : true,
		"processing" : true, //Feature control the processing indicator.
		"serverSide" : true, //Feature control DataTables' server-side processing mode.
		"order" : [], //Initial no order.
               
               "dom": 'Bflrtip',
               "buttons": [
                           /*{
                           extend: 'copyHtml5',
                           exportOptions: {
                               columns: [ 0, ':visible' ]
                           }
                           },*/
                       {
                           extend: 'excelHtml5',
                           title: 'Drivers_Bank_Credit',
                           exportOptions: {
                        	   columns: ':visible'
                           }
                       },
                       {
                           extend: 'csv',
                           title: 'Drivers_Bank_Credit',
                           exportOptions: {
                        	   columns: ':visible'
                           }
                       }
                       //'colvis'
               ],
              
		// Load data for the table's content from an Ajax source
		"ajax" : {
			"url" : location.origin + '/Report/ajax_list_drivercredit',
			"type" : "POST"
                     
		},

		//Set column definition initialisation properties.s
		"columnDefs" : [ {
			"targets" : [0,1], //last column
			"orderable" : false, //set not orderable
		}, ],
		"lengthMenu" : [ [ 100, 200, 500,-1 ], [ 100, 200, 500,'All' ] ],
		"fnDrawCallback" : function(oSettings) {
			driverCreditReportDependency();
		},
		
	});
	
});
function driverCreditReportDependency() {
	$('[id^=clearcredit]').click(function() {
		var arr = $(this).attr('id').split('-');
		var driver_id = arr[1];
		
		clearDriverCredit(driver_id);
	});
	
}

function clearDriverCredit(driver_id) 
{

	if (confirm("Make sure before clear the credit balance?.")) {
		var url = location.origin + '/Report/clearDriverCredit/';
		$("#overlay").removeClass('hidden');
		jQuery.ajax({
			method : "POST",
			url : url,
			data : {
				'id' : driver_id
			},
		}).done(

				function(response) {
					$("#overlay").addClass('hidden');
					response = jQuery.parseJSON(response);
					if (response.status > 0) {

						successMessage(response.msg);
						// loadDriverList();
						window.location.href = location.origin
								+ '/Report/getDriverCreditReport';
					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {
			$("#overlay").addClass('hidden');
			failureMessage('Delete failed.. Please try again later.');

		});
	}


}
