$(document).ready(function() {
	cancelledTripReportDependency();
	
});

function cancelledTripReportDependency() {
	
	$('#cancelled-trip-report-btn').click(function() {
		getCancelledTripReport();
	});
	
	
	 $('#startDate').datetimepicker({
	       dayOfWeekStart : 1,
	       lang           : 'en',
	       startDate      : '0',
	       timepicker     : true,
	       format         : 'Y-m-d H:i:s',
	       step           :60,
	       maxDate: 0,
	       onShow:function( ct ){
			   this.setOptions({
				
			    maxDate:$('#endDate').val()?$('#endDate').val():false,
			    maxTime:$('#endDate').val()?$('#endDate').val():false,
			    
			   })
	       },
	       onSelectDate:function(){
	    	   $('#endDate').attr('readonly',false);
	            var d = $('#startDate').datetimepicker('getValue');
	             
	        }  
	   });
	 
	$('#endDate').datetimepicker({
	       dayOfWeekStart : 1,
	       lang           : 'en',
	       startDate      : '0',
	       timepicker     : true,
	       format         : 'Y-m-d H:i:s',
	       step           :60,
	       
	           onShow:function( ct ){
	                   this.setOptions({
	                	
	                    minDate:$('#startDate').val()?$('#startDate').val():false,
	                    minTime:$('#startDate').val()?$('#startDate').val():false,
	                   
	                   })
	               
	              },
	              onSelectDate:function(){
			            var d = $('#endDate').datetimepicker('getValue');
			             var now = new Date();
			             var cur_date      =  [now.getFullYear(),'-',now.getMonth(),'-',now.getDate()].join('');
			            
			        } 
	   });
}

function getCancelledTripReport() {

	var isValidate = false;
	isValidate = validate('cancelled_trip_report_form');
	//isValidate=1;
	if (isValidate) {
		//alert('test');
		var start_date=$('#startDate').val();
		var end_date=$('#endDate').val();
		var trip_type=$('#tripType').val();
		var trip_status=$('#tripStatus').val();
		table = $('#cancelled-trip-data-table').DataTable({
			 "destroy": true,
			"scrollX" : true,
			"scrollY" : 400,
			"scrollCollapse" : true,
			"processing" : true, //Feature control the processing indicator.
			"serverSide" : true, //Feature control DataTables' server-side processing mode.
			"order" : [], //Initial no order.
	               
	               "dom": 'Bflrtip',
	               "buttons": [
	                           /*{
	                           extend: 'copyHtml5',
	                           exportOptions: {
	                               columns: [ 0, ':visible' ]
	                           }
	                           },*/
	                       {
	                           extend: 'excelHtml5',
	                           title: 'Cancelled_Trip_List',
	                           exportOptions: {
	                        	   columns: ':visible'
	                           }
	                       },
	                       /*{
	                           extend: 'pdfHtml5',
	                           exportOptions: {
	                               columns: [1, 2, 3,4,5,6,7,8,9 ]
	                           }
	                       },*/
	                       //'colvis'
	               ],
	              
			// Load data for the table's content from an Ajax source
			"ajax" : {
				"url" : location.origin + '/Report/ajax_list_cancelledtrip',
				"type" : "POST",
				"data":{'start_date':start_date,'end_date':end_date,'trip_type':trip_type,'trip_status':trip_status}
	                     
			},

			//Set column definition initialisation properties.s
			"columnDefs" : [ {
				"targets" : [], //last column
				"orderable" : false, //set not orderable
			}, ],
			"lengthMenu" : [ [ 100, 200, 500,-1 ], [ 100, 200, 500,'All' ] ]
			
		});
	}

}