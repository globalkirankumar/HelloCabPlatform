$(document).ready(function() {
	driverTaxiMappingReportDependency();
	table = $('#driver-taxi-mapping-data-table').DataTable({
		 "destroy": true,
		"scrollX" : true,
		"scrollY" : 400,
		"scrollCollapse" : true,
		"processing" : true, //Feature control the processing indicator.
		"serverSide" : true, //Feature control DataTables' server-side processing mode.
		"order" : [], //Initial no order.
               
               "dom": 'Bflrtip',
               "buttons": [
                           /*{
                           extend: 'copyHtml5',
                           exportOptions: {
                               columns: [ 0, ':visible' ]
                           }
                           },*/
                       {
                           extend: 'excelHtml5',
                           title: 'Drivers_Transaction_List',
                           exportOptions: {
                        	   columns: ':visible'
                           }
                       },
                       /*{
                           extend: 'pdfHtml5',
                           exportOptions: {
                               columns: [1, 2, 3,4,5,6,7,8,9 ]
                           }
                       },*/
                       //'colvis'
               ],
              
		// Load data for the table's content from an Ajax source
		"ajax" : {
			"url" : location.origin + '/Report/ajax_list_drivertaximapping',
			"type" : "POST"
                     
		},

		//Set column definition initialisation properties.s
		"columnDefs" : [ {
			"targets" : [], //last column
			"orderable" : false, //set not orderable
		}, ],
		"lengthMenu" : [ [ 100, 200, 500,-1 ], [ 100, 200, 500,'All' ] ]
		
	});
	
});

function driverTaxiMappingReportDependency() {
	
	$('#driver-transaction-report-btn').click(function() {
		getDriverTransactionReport();
	});
	
	
	 $('#startDate').datetimepicker({
	       dayOfWeekStart : 1,
	       lang           : 'en',
	       startDate      : '0',
	       timepicker     : true,
	       format         : 'Y-m-d H:i:s',
	       step           :60,
	       maxDate: 0,
	       onShow:function( ct ){
			   this.setOptions({
				
			    maxDate:$('#endDate').val()?$('#endDate').val():false,
			    maxTime:$('#endDate').val()?$('#endDate').val():false,
			    
			   })
	       },
	       onSelectDate:function(){
	    	   $('#endDate').attr('readonly',false);
	            var d = $('#startDate').datetimepicker('getValue');
	             
	        }  
	   });
	 
	$('#endDate').datetimepicker({
	       dayOfWeekStart : 1,
	       lang           : 'en',
	       startDate      : '0',
	       timepicker     : true,
	       format         : 'Y-m-d H:i:s',
	       step           :60,
	       
	           onShow:function( ct ){
	                   this.setOptions({
	                	
	                    minDate:$('#startDate').val()?$('#startDate').val():false,
	                    minTime:$('#startDate').val()?$('#startDate').val():false,
	                   
	                   })
	               
	              },
	              onSelectDate:function(){
			            var d = $('#endDate').datetimepicker('getValue');
			             var now = new Date();
			             var cur_date      =  [now.getFullYear(),'-',now.getMonth(),'-',now.getDate()].join('');
			            
			        } 
	   });
	
	 $("#driverDetail").keyup(function(){
		 $("#driverId").val('');
		 var keyword = $(this).val();
         if(keyword.length>=3){
        	 getDriverDetails(keyword);
         } 
         else
        {
        	 $("#driverId").val('');
        }
	 });
}

function getDriverTransactionReport() {

	var isValidate = false;
	/*var reportData = {};
	var reportData = new window.FormData($('#driver_transaction_report_form')[0]);
	
	 * jQuery( jQuery('#edit_passenger_form :input').serializeArray()
	 * ).each(function( x , y ){ passengerData[y.name] = y.value; });
	 */
	//console.log(reportData);
	isValidate = validate('driver_transaction_report_form');
	//isValidate=1;
	if (isValidate) {
		//alert('test');
		
		table = $('#driver-taxi-mapping-data-table').DataTable({
			 "destroy": true,
			"scrollX" : true,
			"scrollY" : 400,
			"scrollCollapse" : true,
			"processing" : true, //Feature control the processing indicator.
			"serverSide" : true, //Feature control DataTables' server-side processing mode.
			"order" : [], //Initial no order.
	               
	               "dom": 'Bflrtip',
	               "buttons": [
	                           /*{
	                           extend: 'copyHtml5',
	                           exportOptions: {
	                               columns: [ 0, ':visible' ]
	                           }
	                           },*/
	                       {
	                           extend: 'excelHtml5',
	                           title: 'Drivers_Transaction_List',
	                           exportOptions: {
	                              columns: [1, 2, 3,4,5,6,7,8,9 ]
	                           }
	                       },
	                       /*{
	                           extend: 'pdfHtml5',
	                           exportOptions: {
	                               columns: [1, 2, 3,4,5,6,7,8,9 ]
	                           }
	                       },*/
	                       //'colvis'
	               ],
	              
			// Load data for the table's content from an Ajax source
			"ajax" : {
				"url" : location.origin + '/Report/ajax_list_drivertaximapping',
				"type" : "POST"
	                     
			},

			//Set column definition initialisation properties.s
			"columnDefs" : [ {
				"targets" : [], //last column
				"orderable" : false, //set not orderable
			}, ],
			"lengthMenu" : [ [ 100, 200, 500 ], [ 100, 200, 500 ] ]
			
		});
	}

}

function getDriverDetails(keyword) {
	
	
	if(keyword.length >= 3) {
	var url = location.origin + '/Report/getDriverDetails/';
	jQuery.ajax({
		method : "POST",
		url : url,
		data : {
			'keyword' : keyword
		}
	}).done(function(response) {
		
		$("#driver-suggesstion-box").show();
        $("#driver-suggesstion-box").html(response);

	}).fail(function(jqXHR, textStatus, errorThrown) {

	});

}
}

function selectDriver(driver_id,driver_mobile,driver_email,driver_name,driver_code) {
if(driver_id > 0){
	$("#driverId").val(driver_id);
	var driver_data=(driver_code.trim()!='')?driver_name+'('+driver_code+')':driver_name;
	 $("#driverDetail").val(driver_data);
    
}
else
{
	$("#driverId").val('');
}
$("#driver-suggesstion-box").hide();
}
