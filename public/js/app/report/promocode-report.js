$(document).ready(function() {
	promocodeReportDependency();
	
});

function promocodeReportDependency() {
	
	$('#promocode-report-btn').click(function() {
		getPromocodeReport();
	});
	$('#promoCode').on('change',function(){
		
		$('#promo-discount-percentage').addClass('hidden');
		$('#promo-discount-amount').addClass('hidden');
		$('#promo-start-datetime').addClass('hidden');
		$('#promo-end-datetime').addClass('hidden');
		$('#promo-code-limit').addClass('hidden');
		$('#promo-code-user-limit').addClass('hidden');
		
		var promocode=$('#promoCode').val();
		if(promocode != '')
		{
	    var url = location.origin + '/Report/getPromocodeDetails'; 
	    $("#overlay").removeClass('hidden');
	    jQuery.ajax({
	        method: "POST",
	        data:{'promocode':promocode},
	        url: url
	    }).done(function (response) {
	    	$("#overlay").addClass('hidden');
	    	response=jQuery.parseJSON(response);
	    	if(response.status==1)
	    	{
	    		$('#promo-discount-percentage').removeClass('hidden');
	    		$('#promo-discount-amount').removeClass('hidden');
	    		$('#promocode-type').removeClass('hidden');
	    		if(response.data.promocodeType==PROMOCODE_TYPE_GENERIC)
	    		{
	    			$('#zone-name1').removeClass('hidden');
		    		$('#zone-name2').addClass('hidden');
	    		}
	    		else if(response.data.promocodeType==PROMOCODE_TYPE_COMBINATION)
	    		{
	    			$('#zone-name1').removeClass('hidden');
		    		$('#zone-name2').removeClass('hidden');
	    		}
	    		$('#promo-discount-amount').removeClass('hidden');
	    		$('#promo-start-datetime').removeClass('hidden');
	    		$('#promo-end-datetime').removeClass('hidden');
	    		$('#promo-code-limit').removeClass('hidden');
	    		$('#promo-code-user-limit').removeClass('hidden');
	    		$('#promo-entity').removeClass('hidden');
	    		
	    		
	    		//$('#promoDiscountType').val(response.data.promoDiscountType);
	    		$('#promoDiscountAmount').val(response.data.promoDiscountAmount);
	    		$('#promoDiscountAmount2').val(response.data.promoDiscountAmount2);
	    		$('#promoStartDatetime').val(response.data.promoStartDatetime);
	    		$('#promoEndDatetime').val(response.data.promoEndDatetime);
	    		$('#promoCodeLimit').val(response.data.promoCodeLimit);
	    		$('#promoCodeUserLimit').val(response.data.promoCodeUserLimit);
	    		$('#promocodeTypeName').val(response.data.promocodeTypeName);
	    		$('#zoneName1').val(response.data.zoneName1);
	    		$('#zoneName2').val(response.data.zoneName2);
	    		$('#entityName').val(response.data.entityName);
	    	}
	    });
		}
	});
}

function getPromocodeReport() {

	var isValidate = false;
	isValidate = validate('promocode_report_form');
	//isValidate=1;
	if (isValidate) {
		//alert('test');
		var promocode=$('#promoCode').val();
		table = $('#promocode-data-table').DataTable({
			 "destroy": true,
			"scrollX" : true,
			"scrollY" : 400,
			"scrollCollapse" : true,
			"processing" : true, //Feature control the processing indicator.
			"serverSide" : true, //Feature control DataTables' server-side processing mode.
			"order" : [], //Initial no order.
	               
	               "dom": 'Bflrtip',
	               "buttons": [
	                           /*{
	                           extend: 'copyHtml5',
	                           exportOptions: {
	                               columns: [ 0, ':visible' ]
	                           }
	                           },*/
	                       {
	                           extend: 'excelHtml5',
	                           title: 'Cancelled_Trip_List',
	                           exportOptions: {
	                        	   columns: ':visible'
	                           }
	                       },
	                       /*{
	                           extend: 'pdfHtml5',
	                           exportOptions: {
	                               columns: [1, 2, 3,4,5,6,7,8,9 ]
	                           }
	                       },*/
	                       //'colvis'
	               ],
	              
			// Load data for the table's content from an Ajax source
			"ajax" : {
				"url" : location.origin + '/Report/ajax_list_promocode',
				"type" : "POST",
				"data":{'promocode':promocode}
	                     
			},

			//Set column definition initialisation properties.s
			"columnDefs" : [ {
				"targets" : [], //last column
				"orderable" : false, //set not orderable
			}, ],
			"lengthMenu" : [ [ 100, 200, 500,-1 ], [ 100, 200, 500,'All' ] ]
			
		});
	}

}