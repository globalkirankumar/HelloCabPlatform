$(document).ready(function() {
	driverTransactionReportDependency();
	
});

function driverTransactionReportDependency() {
	
	$('#driver-transaction-report-btn').click(function() {
		getDriverTransactionReport();
	});
	
	
	 $('#startDate').datetimepicker({
	       dayOfWeekStart : 1,
	       lang           : 'en',
	       startDate      : '0',
	       timepicker     : true,
	       format         : 'Y-m-d H:i:s',
	       step           :60,
	       maxDate: 0,
	       onShow:function( ct ){
			   this.setOptions({
				
			    maxDate:$('#endDate').val()?$('#endDate').val():false,
			    maxTime:$('#endDate').val()?$('#endDate').val():false,
			    
			   })
	       },
	       onSelectDate:function(){
	    	   $('#endDate').attr('readonly',false);
	            var d = $('#startDate').datetimepicker('getValue');
	             
	        }  
	   });
	 
	$('#endDate').datetimepicker({
	       dayOfWeekStart : 1,
	       lang           : 'en',
	       startDate      : '0',
	       timepicker     : true,
	       format         : 'Y-m-d H:i:s',
	       step           :60,
	       
	           onShow:function( ct ){
	                   this.setOptions({
	                	
	                    minDate:$('#startDate').val()?$('#startDate').val():false,
	                    minTime:$('#startDate').val()?$('#startDate').val():false,
	                   
	                   })
	               
	              },
	              onSelectDate:function(){
			            var d = $('#endDate').datetimepicker('getValue');
			             var now = new Date();
			             var cur_date      =  [now.getFullYear(),'-',now.getMonth(),'-',now.getDate()].join('');
			            
			        } 
	   });
	
	 $("#driverDetail").keyup(function(){
		 $("#driverId").val('');
		 var keyword = $(this).val();
         if(keyword.length>=3){
        	 getDriverDetails(keyword);
         } 
         else
        {
        	 $("#driverId").val('');
        }
	 });
}

function getDriverTransactionReport() {

	var isValidate = false;
	/*var reportData = {};
	var reportData = new window.FormData($('#driver_transaction_report_form')[0]);
	
	 * jQuery( jQuery('#edit_passenger_form :input').serializeArray()
	 * ).each(function( x , y ){ passengerData[y.name] = y.value; });
	 */
	//console.log(reportData);
	isValidate = validate('driver_transaction_report_form');
	//isValidate=1;
	if (isValidate) {
		//alert('test');
		var driver_id=$('#driverId').val();
		var transaction_from=$('#transactionFrom').val();
		var transaction_mode=$('#transactionMode').val();
		var transaction_type=$('#transactionType').val();
		var start_date=$('#startDate').val();
		var end_date=$('#endDate').val();
		var zone_id=$('#zoneId').val();
		table = $('#driver-transaction-data-table').DataTable({
			 "destroy": true,
			"scrollX" : true,
			"scrollY" : 400,
			"scrollCollapse" : true,
			"processing" : true, //Feature control the processing indicator.
			"serverSide" : true, //Feature control DataTables' server-side processing mode.
			"order" : [], //Initial no order.
	                
	                "dom": 'Bflrtip',
	                "buttons": [
	                            {
	                            	 extend: 'csv',
	                            	 title: 'Drivers_Transaction_List',  
	                            exportOptions: {
	                                columns: [ 0, ':visible' ]
	                            }
	                            },
	                        {
	                            extend: 'excelHtml5',
	                            title: 'Drivers_Transaction_List',
	                            exportOptions: {
	                            	columns: ':visible'
	                            }
	                        },
	                        {
	                            extend: 'pdfHtml5',
	                            orientation: 'landscape',
	                  			 pageSize: 'LEGAL',
	                            title: 'Drivers_Transaction_List',
	                            exportOptions: {
	                            	 columns: [1,3,4,5,6,7,8,9,11,13,14,15,16]
	                            }
	                        },
	                        //'colvis'
	                ],
	               
			// Load data for the table's content from an Ajax source
			"ajax" : {
				"url" : location.origin + '/Report/ajax_list_drivertransaction',
				"type" : "POST",
	                       "data":{'driver_id':driver_id,'transaction_from':transaction_from,'transaction_mode':transaction_mode,'transaction_type':transaction_type,'start_date':start_date,'end_date':end_date,'zone_id':zone_id}
			},

			//Set column definition initialisation properties.s
			"columnDefs" : [ {
				"targets" : [0], //last column
				"orderable" : false, //set not orderable
			}, ],
			"lengthMenu" : [ [ 100, 200, 500,-1 ], [ 100, 200, 500,'All' ] ]
			
		});
	}

}

function getDriverDetails(keyword) {
	
	
	if(keyword.length >= 3) {
	var url = location.origin + '/Report/getDriverDetails/';
	jQuery.ajax({
		method : "POST",
		url : url,
		data : {
			'keyword' : keyword
		}
	}).done(function(response) {
		
		$("#driver-suggesstion-box").show();
        $("#driver-suggesstion-box").html(response);

	}).fail(function(jqXHR, textStatus, errorThrown) {

	});

}
}

function selectDriver(driver_id,driver_mobile,driver_email,driver_name,driver_code) {
if(driver_id > 0){
	$("#driverId").val(driver_id);
	var driver_data=(driver_code.trim()!='')?driver_name+'('+driver_code+')':driver_name;
	 $("#driverDetail").val(driver_data);
    
}
else
{
	$("#driverId").val('');
}
$("#driver-suggesstion-box").hide();
}
