$(document).ready(function() {
	tripDetailsReportDependency();
	
});

function tripDetailsReportDependency() {
	
	$('#trip-details-report-btn').click(function() {
		getTripDetailsReport();
	});
	
	
	 $('#startDate').datetimepicker({
	       dayOfWeekStart : 1,
	       lang           : 'en',
	       startDate      : '0',
	       timepicker     : true,
	       format         : 'Y-m-d H:i:s',
	       step           :60,
	       maxDate: 0,
	       onShow:function( ct ){
			   this.setOptions({
				
			    maxDate:$('#endDate').val()?$('#endDate').val():false,
			    maxTime:$('#endDate').val()?$('#endDate').val():false,
			    
			   })
	       },
	       onSelectDate:function(){
	    	   $('#endDate').attr('readonly',false);
	            var d = $('#startDate').datetimepicker('getValue');
	             
	        }  
	   });
	 
	$('#endDate').datetimepicker({
	       dayOfWeekStart : 1,
	       lang           : 'en',
	       startDate      : '0',
	       timepicker     : true,
	       format         : 'Y-m-d H:i:s',
	       step           :60,
	       
	           onShow:function( ct ){
	                   this.setOptions({
	                	
	                    minDate:$('#startDate').val()?$('#startDate').val():false,
	                    minTime:$('#startDate').val()?$('#startDate').val():false,
	                   
	                   })
	               
	              },
	              onSelectDate:function(){
			            var d = $('#endDate').datetimepicker('getValue');
			             var now = new Date();
			             var cur_date      =  [now.getFullYear(),'-',now.getMonth(),'-',now.getDate()].join('');
			            
			        } 
	   });
	
}

function getTripDetailsReport() {

	var isValidate = false;
	/*var reportData = {};
	var reportData = new window.FormData($('#driver_transaction_report_form')[0]);
	
	 * jQuery( jQuery('#edit_passenger_form :input').serializeArray()
	 * ).each(function( x , y ){ passengerData[y.name] = y.value; });
	 */
	//console.log(reportData);
	isValidate = validate('trip_details_report_form');
	//isValidate=1;
	if (isValidate) {
		//alert('test');
		var entity_id=$('#entityId').val();
		var start_date=$('#startDate').val();
		var end_date=$('#endDate').val();
		var zone_id=$('#zoneId').val();
		table = $('#trip-details-data-table').DataTable({
			 "destroy": true,
			"scrollX" : true,
			"scrollY" : 400,
			"scrollCollapse" : true,
			"processing" : true, //Feature control the processing indicator.
			"serverSide" : true, //Feature control DataTables' server-side processing mode.
			"order" : [], //Initial no order.
	                
	                "dom": 'Bflrtip',
	                "buttons": [
	                            /*{
	                            extend: 'copyHtml5',
	                            exportOptions: {
	                                columns: [ 0, ':visible' ]
	                            }
	                            },*/
	                        {
	                            extend: 'excelHtml5',
	                            title: 'Trip_Details_Report',
	                            exportOptions: {
	                            	columns: ':visible'
	                            }
	                        },
	                        /*{
	                            extend: 'pdfHtml5',
	                            exportOptions: {
	                                columns: [1, 2, 3,4,5,6,7,8,9 ]
	                            }
	                        },*/
	                        //'colvis'
	                ],
	               
			// Load data for the table's content from an Ajax source
			"ajax" : {
				"url" : location.origin + '/Report/ajax_list_tripdetails',
				"type" : "POST",
	                       "data":{'entity_id':entity_id,'start_date':start_date,'end_date':end_date,'zone_id':zone_id}
			},

			//Set column definition initialisation properties.s
			"columnDefs" : [ {
				"targets" : [], //last column
				"orderable" : false, //set not orderable
			}, ],
			"lengthMenu" : [ [ -1], [ "All"] ]
			
		});
	}

}

function getDriverDetails(keyword) {
	
	
	if(keyword.length >= 3) {
	var url = location.origin + '/Report/getDriverDetails/';
	jQuery.ajax({
		method : "POST",
		url : url,
		data : {
			'keyword' : keyword
		}
	}).done(function(response) {
		
		$("#driver-suggesstion-box").show();
        $("#driver-suggesstion-box").html(response);

	}).fail(function(jqXHR, textStatus, errorThrown) {

	});

}
}

function selectDriver(driver_id,driver_mobile,driver_email,driver_name,driver_code) {
if(driver_id > 0){
	$("#driverId").val(driver_id);
	var driver_data=(driver_code.trim()!='')?driver_name+'('+driver_code+')':driver_name;
	 $("#driverDetail").val(driver_data);
    
}
else
{
	$("#driverId").val('');
}
$("#driver-suggesstion-box").hide();
}
