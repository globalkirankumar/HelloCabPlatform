$(document).ready(function() {
	table = $('#driver-wallet-warning-data-table').DataTable({
		 "destroy": true,
		"scrollX" : true,
		"scrollY" : 400,
		"scrollCollapse" : true,
		"processing" : true, //Feature control the processing indicator.
		"serverSide" : true, //Feature control DataTables' server-side processing mode.
		"order" : [], //Initial no order.
               
               "dom": 'Bflrtip',
               "buttons": [
                           /*{
                           extend: 'copyHtml5',
                           exportOptions: {
                               columns: [ 0, ':visible' ]
                           }
                           },*/
                       {
                           extend: 'excelHtml5',
                           title: 'Drivers_Wallet_Warning_List',
                           exportOptions: {
                        	   columns: ':visible'
                           }
                       },
                       /*{
                           extend: 'pdfHtml5',
                           exportOptions: {
                               columns: [1, 2, 3,4,5,6,7,8,9 ]
                           }
                       },*/
                       //'colvis'
               ],
              
		// Load data for the table's content from an Ajax source
		"ajax" : {
			"url" : location.origin + '/Report/ajax_list_driverwalletwarning',
			"type" : "POST"
                     
		},

		//Set column definition initialisation properties.s
		"columnDefs" : [ {
			"targets" : [], //last column
			"orderable" : false, //set not orderable
		}, ],
		"lengthMenu" : [ [ 100, 200, 500,-1 ], [ 100, 200, 500,'All' ] ]
		
	});
	
});
