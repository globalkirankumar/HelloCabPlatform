$(document).ready(function() {
	var buttonCommon = {
	        exportOptions: {
	            format: {
	                body: function ( data, row, column, node ) {
	                    // Strip $ from salary column to make it numeric
	                    return column === 5 ?
	                        data.replace( /[$,]/g, '' ) :
	                        data;
	                }
	            }
	        }
	    };
	taxiTariffDependency();
	//alert('state');exit;
	//datatables
	table = $('#taxi-tariff-data-table').DataTable({

		"scrollX" : true,
		"scrollY" : 400,
		"scrollCollapse" : true,
		"processing" : true, //Feature control the processing indicator.
		"serverSide" : true, //Feature control DataTables' server-side processing mode.
		"order" : [], //Initial no order.
		
        
        "dom": 'Bflrtip',
        "buttons": [
                    /*{
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: [ 0, ':visible' ]
                    }
                    },*/
					 // 'colvis',
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdfHtml5',
                    exportOptions: {
                        columns: [1,2, 3,4,5,6]
                    }
                },
              
        ],
		// Load data for the table's content from an Ajax source
		"ajax" : {
			"url" : location.origin + '/TaxiTariff/ajax_list',
			"type" : "POST"
		},

		//Set column definition initialisation properties.s
		"columnDefs" : [ {
			"targets" : [ -1, 0 ], //last column
			"orderable" : false, //set not orderable
		}, ],
		"lengthMenu" : [ [ 20, 50, 100 ], [ 20, 50, 100 ] ],
		"fnDrawCallback" : function(oSettings) {
			taxiTariffDependency();
		}
	});

	$('label input.toggle-vis').each(function() {
		var column = table.column($(this).attr('data-column'));
		if ($(this).prop('checked') == true) {
			//column.toggle(this.checked);
			column.visible(true);
		} else {
			column.visible(false);
		}

	});
	$('label input.toggle-vis').on('change', function(e) {
		e.preventDefault();
		// Get the column API object
		var column = table.column($(this).attr('data-column'));
		if ($(this).prop('checked') == true) {
			//column.toggle(this.checked);
			column.visible(true);
		} else {
			column.visible(false);
		}
		taxiTariffDependency();

	});
});