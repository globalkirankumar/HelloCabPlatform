function taxiDeviceDependency()
{
	$('#save-taxidevice-btn').click(function(){
		 saveTaxiDevice(); 
	});
	$('#cancel-taxidevice-btn').click(function(){
		// loadUserList(); 
		 window.location.href = location.origin + '/taxiDevice/getTaxiDeviceList';
	});
	$('[id^=status]').click(function(){
		var status=$(this).attr('status');
		var arr = $(this).attr('id').split('-');
	    var taxi_device_id = arr[1];
	    changeTaxiDeviceStatus(taxi_device_id,status); 
	});
	$('[id^=viewtaxidevice]').click(function(){
		var arr = $(this).attr('id').split('-');
	    var taxi_device_id = arr[1];
		 loadTaxiDevice(taxi_device_id,'view'); 
	});
	$('[id^=edittaxidevice]').click(function(){
		var arr = $(this).attr('id').split('-');
	    var taxi_device_id = arr[1];
	    loadTaxiDevice(taxi_device_id,'edit'); 
	});
	$('[id^=deletetaxidevice]').click(function(){
		var arr = $(this).attr('id').split('-');
	    var taxi_device_id = arr[1];
	    deleteTaxiDevice(taxi_device_id);
	});
	
$('#lastServicedDate').datetimepicker({
    dayOfWeekStart : 1,
    lang           : 'en',
    startDate      : '',
    timepicker     : false,
    closeOnDateSelect : true,
    scrollMonth : false,
    scrollInput : false,
    format         : 'Y-m-d',
    maxDate:0
});
	   
}

function loadTaxiDeviceList()
{
    // Validate the passenger value again in case the user changed the input
    var url = location.origin + '/taxiDevice/getTaxiDeviceList';
    jQuery.ajax({
        method: "POST",
        url: url,
        //data:{'get_type':'ajax_call'},
    }).done(function (response) {
        //jQuery('#content').html(response);
    });
}

function loadTaxiDevice( taxi_device_id , view_mode)
{
   // var url = location.origin + '/user/getDetailsById/' + user_id + '/' + view_mode;
    window.location.href =location.origin + '/taxiDevice/getDetailsById/' + taxi_device_id + '/' + view_mode;
    /*jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {id: user_id}
    }).done(function (response) {
        jQuery('#container').html(response.html);
        userDependency();
    });*/
}

function saveTaxiDevice() {
	
    var isValidate=false;
 // Validate the user details again in case the user changed the input
    var url = location.origin + '/taxiDevice/saveTaxiDevice/';
    
    var taxiDeviceData = {};
    var taxiDeviceData = new window.FormData($('#edit_taxi_device_form')[0]);
    /*jQuery( jQuery('#edit_passenger_form :input').serializeArray() ).each(function( x , y ){
    	passengerData[y.name] = y.value;
    });*/
   
    isValidate=validate('edit_taxi_device_form');
    if(isValidate)
    {
    jQuery.ajax({
        xhr: function () {  
            return $.ajaxSettings.xhr();
        },
        type: "POST",
        data: taxiDeviceData,
        cache: false,
        contentType: false,
        processData: false,
        url: url,
    }).done(function (response) {
          response = jQuery.parseJSON(response);
        if(response.taxi_device_id)
        {
        	successMessage( response.msg);
        	//loadUserList();
        	window.location.href = location.origin + '/taxiDevice/getTaxiDeviceList';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	failureMessage( response.msg);
    	
    });
    }
    
}

function changeTaxiDeviceStatus(taxi_device_id,status)
{
	if (confirm("Make sure before changing taxi device status?."))
    {
    if(status==1)
    {
    	status=0;
    }
    else
    {
    	status=1;
    }
    
    var url = location.origin + '/taxiDevice/changeTaxiDeviceStatus/';
    
    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'id':taxi_device_id,'status':status},
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response.status > 0)
        {
        	
        	successMessage( response.msg);
        	//loadUserList();
        	window.location.href = location.origin + '/taxiDevice/getTaxiDeviceList';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	
    	failureMessage( 'Status change failed.. Please try again later.');
    	
    });
    }
    
}

function deleteTaxiDevice(taxi_device_id)
{
	if (confirm("Make sure before deleting taxi device?."))
    {
    var url = location.origin + '/taxiDevice/deleteTaxiDevice/';
    
    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'id':taxi_device_id},
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response.status > 0)
        {
        	
        	successMessage( response.msg);
        	//loadUserList();
        	window.location.href = location.origin + '/taxiDevice/getTaxiDeviceList';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	
    	failureMessage( 'Delete failed.. Please try again later.');
    	
    });
    }
    
}