$(document).ready(function() {
    countryDependency();
	//alert('state');exit;
	//datatables
	table = $('#country-data-table').DataTable({

		"scrollX" : true,
		"scrollY" : 400,
		"scrollCollapse" : true,
		"processing" : true, //Feature control the processing indicator.
		"serverSide" : true, //Feature control DataTables' server-side processing mode.
		"order" : [], //Initial no order.
		
        
        "dom": 'Bflrtip',
        
        "buttons": [
                    /*{
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: [ 0, ':visible' ]
                    }
                    },*/
                // 'colvis',
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdfHtml5',
                    exportOptions: {
                    	columns: ':visible'
                    }
                },
                
        ],
		// Load data for the table's content from an Ajax source
		"ajax" : {
			"url" : location.origin + '/country/ajax_list',
			"type" : "POST"
		},

		//Set column definition initialisation properties.s
		"columnDefs" : [ {
			"targets" : [ -1, 0], //last column
			"orderable" : false, //set not orderable
		}, ],
		"lengthMenu" : [ [ 20, 50, 100 ], [ 20, 50, 100 ] ],
		"fnDrawCallback" : function(oSettings) {
			countryDependency();
		}
	});

	$('label input.toggle-vis').each(function() {
		var column = table.column($(this).attr('data-column'));
		if ($(this).prop('checked') == true) {
			//column.toggle(this.checked);
			column.visible(true);
		} else {
			column.visible(false);
		}

	});
	$('label input.toggle-vis').on('change', function(e) {
		e.preventDefault();
		// Get the column API object
		var column = table.column($(this).attr('data-column'));
		if ($(this).prop('checked') == true) {
			//column.toggle(this.checked);
			column.visible(true);
		} else {
			column.visible(false);
		}
		countryDependency();

	});
});

function countryDependency()
{ 
	 $('#save-country-btn').on('click', saveCountry); 
	 $('#cancel-country-btn').on('click', loadCountryList);
	 $('[id^=viewcountry]').click(function() {
			var arr = $(this).attr('id').split('-');
			var country_id = arr[1];
			loadCountry(country_id, 'view');
		});
		$('[id^=editcountry]').click(function() {
			var arr = $(this).attr('id').split('-');
			var country_id = arr[1];
			loadCountry(country_id, 'edit');
		});
		$('[id^=deletecountry]').click(function() {
			var arr = $(this).attr('id').split('-');
			var country_id = arr[1];
			deleteCountry(country_id);
		});
		
}

function changeCountryStatus(id)
{
	var country_id=0;
	var status='';
	
	var arr = id.split('-');
    country_id = arr[1];
    
    if(arr[0]=='active')
    {
    	status='Y';
    }
    else if(arr[0]=='deactive')
    {
    	status='N';
    }
    var url = location.origin + '/country/changeCountryStatus/';
    $("#overlay").removeClass('hidden');
    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'id':country_id,'status':status},
    }).done(function (response) {
    	$("#overlay").addClass('hidden');
    	response=jQuery.parseJSON(response);
    	if(response)
        {
        	
        	successMessage( response.msg);
        	loadcountryList();
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	$("#overlay").addClass('hidden');
    	failureMessage( 'Status change failed.. Please try again later.');
    	
    });
    
}

function loadCountryList()
{
	window.location.href = location.origin + '/country/getCountryList';
    // Validate the passenger value again in case the user changed the input
    /*var url = location.origin + '/country/getCountryList/';
    var ccountry_id = $('#filter_countryId').val();
    jQuery.ajax({
        method: "POST",
        url: url,
        data:{'get_type':'ajax_call','country_id':country_id},
    }).done(function (response) {
        jQuery('#country-details-information').replaceWith(response);
        dataTableLoader();
    });*/
}

function loadCountry( country_id , view_mode)
{
	window.location.href =location.origin + '/country/getDetailsById/' + country_id + '/' + view_mode;
    /*var url = location.origin + '/country/getDetailsById/' + country_id + '/' + view_mode;
    
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {id: country_id}
    }).done(function (response) {
        jQuery('#country-details-information').replaceWith(response.html);
        countryDependency();
    });*/
}

function saveCountry() {
    var isValidate=false;
 // Validate the passenger details again in case the user changed the input
    var url = location.origin + '/country/saveCountry/';
    
    var countryData = {};
    var countryData = new window.FormData($('#edit_country_form')[0]);
    /*jQuery( jQuery('#edit_passenger_form :input').serializeArray() ).each(function( x , y ){
    	passengerData[y.name] = y.value;
    });*/
    isValidate=validate('edit_country_form');
    if(isValidate)
    {
    	$("#overlay").removeClass('hidden');
    $('#save_country_btn').button('loading');
    
    jQuery.ajax({
        xhr: function () {  
            return $.ajaxSettings.xhr();
        },
        type: "POST",
        data: countryData,
        cache: false,
        contentType: false,
        processData: false,
        url: url,
    }).done(function (response) {
    	$("#overlay").addClass('hidden');
          response = jQuery.parseJSON(response);
        if(response.country_id)
        {
        	//successMessage( response.msg);
        	//loadPassengerList();
        	window.location.href = location.origin + '/country/getCountryList/';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	$("#overlay").addClass('hidden');
    	failureMessage( response.msg);
    	
    });
    }
    
}