$(document).ready(function() {
	taxiDependency();
 //alert('state');exit;
    //datatables
    table = $('#taxi-data-table').DataTable({ 
    	"scrollX": true,
   	 "scrollY": 400,
   	"scrollCollapse": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
"dom": 'Bflrtip',
        
        "buttons": [
                    /*{
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: [ 0, ':visible' ]
                    }
                    },*/
                 //'colvis',
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdfHtml5',
                    exportOptions: {
                    	columns: ':visible'
                    }
                },
                
        ],
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": location.origin +'/taxi/ajax_list',
            "type": "POST"
        },
 
        //Set column definition initialisation properties.s
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
        "lengthMenu": [[20, 50, 100], [20, 50, 100]],
        "fnDrawCallback": function( oSettings ) {
        	taxiDependency();
          }
 
    });
    
    $('label input.toggle-vis').each(function(){
    	var column = table.column( $(this).attr('data-column') );
        if($(this).prop('checked')==true)
        {
        	//column.toggle(this.checked);
        	column.visible(true);
        }
        else
        {
        	column.visible( false);
        }
    });
    $('label input.toggle-vis').on( 'change', function (e) {
        e.preventDefault();
        // Get the column API object
        var column = table.column( $(this).attr('data-column') );
        if($(this).prop('checked')==true)
        {
        	//column.toggle(this.checked);
        	column.visible(true);
        }
        else
        {
        	column.visible( false);
        }
        taxiDependency();
    });
});

function taxiDependency()
{ 
	 
	 $('#save-taxi-btn').click(function() {
		 if($('#taxiOwnerId').val()!='')
			{
				 saveTaxi();
			}
			 else
			{
				 failureMessage('Please select existing taxi owner by autocomplete search / create new taxi owner');
			}
		});
		$('#cancel-taxi-btn').click(function() {
			// loadUserList(); 
			window.location.href = location.origin + '/taxi/getTaxiList';
		});
	 $('[id^=viewtaxi]').click(function() {
			var arr = $(this).attr('id').split('-');
			var taxi_id = arr[1];
			loadTaxi(taxi_id, 'view');
		});
		$('[id^=edittaxi]').click(function() {
			var arr = $(this).attr('id').split('-');
			var taxi_id = arr[1];
			loadTaxi(taxi_id, 'edit');
		});
		$('[id^=deletetaxi]').click(function() {
			var arr = $(this).attr('id').split('-');
			var taxi_id = arr[1];
			deleteTaxi(taxi_id);
		});
		$('#registrationNo').blur(function() {

			checkTaxiRegistrationNo();
		});
     var url = location.origin + '/taxi/getTaxiList/';
     
    /* $('#passenger-data-table').DataTable( {
         "processing": true,
         "serverSide": true,
         "ajax": "scripts/objects.php",
         "columns": [
             { "data": "first_name" },
             { "data": "last_name" },
             { "data": "position" },
             { "data": "office" },
             { "data": "start_date" },
             { "data": "salary" }
         ]
     } );*/
     
     $('#insuranceExpiryDate').datetimepicker({
	       dayOfWeekStart : 1,
	       lang           : 'en',
	       startDate      : '0',
	       timepicker     : false,
	       closeOnDateSelect : true,
	       format         : 'Y-m-d',
	       scrollMonth : false,
	       scrollInput : false,
	       minDate:0
	   });
	   
	   $('#nextFcDate').datetimepicker({
	       dayOfWeekStart : 1,
	       lang           : 'en',
	       startDate      : '0',
	       timepicker     : false,
	       closeOnDateSelect : true,
	       scrollMonth : false,
	       scrollInput : false,
	       format         : 'Y-m-d',
	       minDate:0
	   });
	   $('#serviceStartDate').datetimepicker({
	       dayOfWeekStart : 1,
	       lang           : 'en',
	       startDate      : '0',
	       timepicker     : false,
	       closeOnDateSelect : true,
	       scrollMonth : false,
	       scrollInput : false,
	       format         : 'Y-m-d',
	       minDate:0
	   });
	   
	   $("#searchTaxiOwner").keyup(function(){
			 var keyword = $(this).val();
	         if(keyword.length>=3){
	        	 getTaxiOwnerDetails(keyword);
	         }
	        
			
	       
	 });
}
function viewTaxi(id){
    var arr = id.split('-');
    var taxi_id = arr[1];
    loadtaxi(taxi_id,'view');
}
function editTaxi(id){
   var arr = id.split('-');
   var taxi_id = arr[1];
   loadPassenger(taxi_id,'edit'); 
}
function transctionHistory(id){
   var arr = id.split('-');
   var taxi_id = arr[1];
   loadTransctionHistory(taxi_id); 
}


function changeTaxiStatus(id)
{
	var taxi_id=0;
	var status='';
	
	var arr = id.split('-');
    taxi_id = arr[1];
    
    if(arr[0]=='active')
    {
    	status='Y';
    }
    else if(arr[0]=='deactive')
    {
    	status='N';
    }
    var url = location.origin + '/taxi/changeTaxiStatus/';
    $("#overlay").removeClass('hidden');
    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'id':taxi_id,'status':status},
    }).done(function (response) {
    	$("#overlay").addClass('hidden');
    	response=jQuery.parseJSON(response);
    	if(response)
        {
        	
        	successMessage( response.msg);
        	loadtaxiList();
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	$("#overlay").addClass('hidden');
    	failureMessage( 'Status change failed.. Please try again later.');
    	
    });
    
}

function loadTaxiList() {
	// Validate the passenger value again in case the user changed the input
	var url = location.origin + '/taxi/getTaxiList/';
	jQuery.ajax({
		method : "POST",
		url : url,
	//data:{'get_type':'ajax_call'},
	}).done(function(response) {
		//jQuery('#content').html(response);
	});
}

function loadTaxi( taxi_id , view_mode)
{
   
    window.location.href =location.origin + '/taxi/getDetailsById/' + taxi_id + '/' + view_mode;
    /*
       var url = location.origin + '/taxi/getDetailsById/' + taxi_id + '/' + view_mode;
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {id: taxi_id}
    }).done(function (response) {
        jQuery('#taxi-details-information').replaceWith(response.html);
        taxiDependency();
    });*/
}
function loadTransctionHistory( taxi_id )
{
    var url = location.origin + '/wallet/getWalletList/';
    
   var response =  jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {'taxi_id': taxi_id,'get_type':'ajax_call'},
        async:false
    }).responseText;
    if(response){
        jQuery('#taxi-details-information').replaceWith(response);
        dataTableLoader();
    }else{
        failureMessage('Please Try Again Later');
    }
}

function saveTaxi() {
    var isValidate=false;
 // Validate the passenger details again in case the user changed the input
    var url = location.origin + '/taxi/saveTaxi/';
    
    var taxiData = {};
    var taxiData = new window.FormData($('#edit_taxi_form')[0]);
    /*jQuery( jQuery('#edit_passenger_form :input').serializeArray() ).each(function( x , y ){
    	passengerData[y.name] = y.value;
    });*/
    isValidate=validate('edit_taxi_form');
    if(isValidate)
    {
    	$("#overlay").removeClass('hidden');
    $('#save_taxi_btn').button('loading');
    
    jQuery.ajax({
        xhr: function () {  
            return $.ajaxSettings.xhr();
        },
        type: "POST",
        data: taxiData,
        cache: false,
        contentType: false,
        processData: false,
        url: url,
    }).done(function (response) {
    	$("#overlay").addClass('hidden');
          response = jQuery.parseJSON(response);
        if(response.taxi_id)
        {
        	//successMessage( response.msg);
        	//loadPassengerList();
        	window.location.href = location.origin + '/taxi/getTaxiList/';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	$("#overlay").addClass('hidden');
    	failureMessage( response.msg);
    	
    });
    }
    
}

function deleteTaxi(taxi_id) {
	if (confirm("Make sure before deleting taxi?.")) {
		var url = location.origin + '/taxi/deleteTaxi/';
		$("#overlay").removeClass('hidden');
		jQuery.ajax({
			method : "POST",
			url : url,
			data : {
				'id' : taxi_id
			},
		}).done(
				function(response) {
					$("#overlay").addClass('hidden');
					response = jQuery.parseJSON(response);
					if (response.status > 0) {

						successMessage(response.msg);
						//loadDriverList();
						window.location.href = location.origin
								+ '/taxi/getTaxiList/';
					} else {
						failureMessage(response.msg);
					}

				}).fail(function(jqXHR, textStatus, errorThrown) {
					$("#overlay").addClass('hidden');
			failureMessage('Delete failed.. Please try again later.');

		});
	}

}
function checkTaxiRegistrationNo() {
	var registrationNo = $('#registrationNo').val();
	var taxi_id = $('#taxi_id').val();
	if (validateRegistrationNo(registrationNo)) {
		if (registrationNo != '') {
			$('#save-taxi-btn').prop('disabled', true);
			var url = location.origin + '/Taxi/checkTaxiRegistrationNo/';
			$("#overlay").removeClass('hidden');
			jQuery.ajax({
				method : "POST",
				data : {
					'registrationNo' : registrationNo,
					'taxi_id':taxi_id
				},
				url : url
			}).done(function(response) {
				$("#overlay").addClass('hidden');
				response = jQuery.parseJSON(response);
				if (response.status == 1) {
					$('#registrationNo').val('');
				}
				$('#save-taxi-btn').prop('disabled', false);
				infoMessage(response.msg, 1, '#registrationNo');
			});
		}
	}
}

function getTaxiOwnerDetails(key) {
	
	
	if(key.length >= 3) {
	var url = location.origin + '/taxi/getTaxiOwnerDetails/';
	jQuery.ajax({
		method : "POST",
		url : url,
		data : {
			'key' : key
		}
	}).done(function(response) {
		
		$("#mobile-suggesstion-box").show();
        $("#mobile-suggesstion-box").html(response);

	}).fail(function(jqXHR, textStatus, errorThrown) {

	});

}
}
function selectTaxiOwner(taxi_owner_id,taxi_owner_mobile,taxi_owner_name) {
if(taxi_owner_id > 0){
	$("#taxiOwnerId").val(taxi_owner_id);
   
    var taxi_owner_data=(taxi_owner_mobile.trim()!='')?taxi_owner_name+'('+taxi_owner_mobile+')':taxi_owner_name;
    $("#searchTaxiOwner").val(taxi_owner_data); 
}   
$("#mobile-suggesstion-box").hide();
}