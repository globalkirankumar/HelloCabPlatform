function emergencyDependency()
{
	$('#save-emergency-btn').click(function(){
		 saveEmergency(); 
	});
	$('#cancel-emergency-btn').click(function(){
		// loadUserList(); 
		 window.location.href = location.origin + '/emergency/getEmergencylist';
	});
	$('[id^=status]').click(function(){
		var status=$(this).attr('status');
		var arr = $(this).attr('id').split('-');
	    var emergency_id = arr[1];
	    changeStatus(emergency_id,status); 
	});
	
	$('[id^=viewemergency]').click(function(){
		var arr = $(this).attr('id').split('-');
	    var emergency_id = arr[1];
		 loadEmergency(emergency_id,'view'); 
	});
	$('[id^=editemergency]').click(function(){
		var arr = $(this).attr('id').split('-');
	    var emergency_id = arr[1];
	    loadEmergency(emergency_id,'edit'); 
	});
	$('[id^=deleteemergency]').click(function(){
		var arr = $(this).attr('id').split('-');
	    var emergency_id = arr[1];
	    deleteEmergency(emergency_id);
	});
	$('[id^=emergencystatus]').click(function(){
		var emergency_status=$(this).attr('emergencyStatus');
		var arr = $(this).attr('id').split('-');
	    var emergency_id = arr[1];
	    $('#emergency-status').modal('show');
	    $('#tempEmergencyId').val(emergency_id); 
	    $('#emergencyStatus').val(emergency_status); 
	});
	$('#save-emergency-status-btn').click(function(){
		changeEmergencyStatus(); 
	});
}

function deleteEmergency(emergency_id)
{
	if (confirm("Make sure before deleting emergency request ?."))
    {
    var url = location.origin + '/emergency/deleteEmergency/';
    
    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'id':emergency_id},
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response.status > 0)
        {
        	
        	successMessage( response.msg);
        	//loadDriverList();
        	window.location.href = location.origin + '/emergency/getEmergencylist';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	
    	failureMessage( 'Delete failed.. Please try again later.');
    	
    });
    }
    
}
function loadEmergencyList()
{
    // Validate the passenger value again in case the user changed the input
    var url = location.origin + '/emergency/getEmergencylist/';
    var cmerchant_id = $('#filter_merchantId').val();
    jQuery.ajax({
        method: "POST",
        url: url,
        data:{'get_type':'ajax_call','emergency_id':emergency_id},
    }).done(function (response) {
        jQuery('#emergency-information').replaceWith(response);
       
    });
}

function loadEmergency( emergency_id , view_mode)
{
  //  var url = location.origin + '/emergency/getDetailsById/' + emergency_id + '/' + view_mode;
    window.location.href =location.origin + '/emergency/getDetailsById/' + emergency_id + '/' + view_mode;
   /* jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {id: emergency_id}
    }).done(function (response) {
        jQuery('#emergency-information').replaceWith(response.html);
        merchantDependency();
    });*/
}

function saveEmergency() {
    var isValidate=false;
 // Validate the passenger details again in case the user changed the input
    var url = location.origin + '/emergency/saveEmergency/';
    
    var emergencyData = {};
    var emergencyData = new window.FormData($('#edit_emergency_form')[0]);
    /*jQuery( jQuery('#edit_passenger_form :input').serializeArray() ).each(function( x , y ){
    	passengerData[y.name] = y.value;
    });*/
    isValidate=validate('edit_emergency_form');
    if(isValidate)
    {

    jQuery.ajax({
        xhr: function () {  
            return $.ajaxSettings.xhr();
        },
        type: "POST",
        data: emergencyData,
        cache: false,
        contentType: false,
        processData: false,
        url: url,
    }).done(function (response) {
          response = jQuery.parseJSON(response);
        if(response.emergency_id)
        {
        	//successMessage( response.msg);
        	//loadPassengerList();
        	window.location.href = location.origin + '/emergency/getEmergencylist/';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	failureMessage( response.msg);
    	
    });
    }
    
}

function changeEmergencyStatus() {
    var isValidate=false;
    var url = location.origin + '/emergency/changeEmergencyStatus/';
    
    var emergencyData = {};
    var emergencyData = new window.FormData($('#edit_emergency_status_form')[0]);
    
    isValidate=validate('edit_emergency_status_form');
    if(isValidate)
    {

    jQuery.ajax({
        xhr: function () {  
            return $.ajaxSettings.xhr();
        },
        type: "POST",
        data: emergencyData,
        cache: false,
        contentType: false,
        processData: false,
        url: url,
    }).done(function (response) {
          response = jQuery.parseJSON(response);
      	if(response.status > 0)
          {
      		$('#emergency-status').modal('hide');
          	successMessage( response.msg);
          	//loadUserList();
          	window.location.href = location.origin + '/emergency/getEmergencyList';
          }
          else
          {
          	failureMessage( response.msg);
          }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	failureMessage( response.msg);
    	
    });
    }
    
}

function changeStatus(emergency_id,status)
{
	if (confirm("Make sure before changing emergency status?."))
    {
    if(status==1)
    {
    	status=0;
    }
    else
    {
    	status=1;
    }
    
    var url = location.origin + '/emergency/changeStatus/';
    
    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'id':emergency_id,'status':status},
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response.status > 0)
        {
        	
        	successMessage( response.msg);
        	//loadUserList();
        	window.location.href = location.origin + '/emergency/getEmergencyList';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	
    	failureMessage( 'Status change failed.. Please try again later.');
    	
    });
    }
    
}