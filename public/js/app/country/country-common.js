function countryDependency()
{ 
	 $('#save_country_btn').on('click', saveCountry); 
	 $('#cancel_country_btn').on('click', loadCountryList);
     
     var url = location.origin + '/country/getCountryList/';
     
    /* $('#passenger-data-table').DataTable( {
         "processing": true,
         "serverSide": true,
         "ajax": "scripts/objects.php",
         "columns": [
             { "data": "first_name" },
             { "data": "last_name" },
             { "data": "position" },
             { "data": "office" },
             { "data": "start_date" },
             { "data": "salary" }
         ]
     } );*/
}
function viewCountry(id){
    var arr = id.split('-');
    var country_id = arr[1];
    loadCountry(country_id,'view');
}
function editCountry(id){
   var arr = id.split('-');
   var country_id = arr[1];
   loadPassenger(country_id,'edit'); 
}
function transctionHistory(id){
   var arr = id.split('-');
   var country_id = arr[1];
   loadTransctionHistory(country_id); 
}

function checkCountryMobile()
{
	var mobile=$('#mobile').val();
	if(mobile != '')
	{
    var url = location.origin + '/country/checkCountryMobile/'; 
    jQuery.ajax({
        method: "POST",
        data:{'mobile':mobile},
        url: url
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response.status==1)
    	{
    		$('#mobile').val('');
    	}
    	infoMessage(response.msg);
    });
	}
}
function changeCountryStatus(id)
{
	var country_id=0;
	var status='';
	
	var arr = id.split('-');
    country_id = arr[1];
    
    if(arr[0]=='active')
    {
    	status='Y';
    }
    else if(arr[0]=='deactive')
    {
    	status='N';
    }
    var url = location.origin + '/country/changeCountryStatus/';
    
    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'id':country_id,'status':status},
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response)
        {
        	
        	successMessage( response.msg);
        	loadcountryList();
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	
    	failureMessage( 'Status change failed.. Please try again later.');
    	
    });
    
}

function loadCountryList()
{
    // Validate the passenger value again in case the user changed the input
    var url = location.origin + '/country/getCountryList/';
    var ccountry_id = $('#filter_countryId').val();
    jQuery.ajax({
        method: "POST",
        url: url,
        data:{'get_type':'ajax_call','country_id':country_id},
    }).done(function (response) {
        jQuery('#country-details-information').replaceWith(response);
        dataTableLoader();
    });
}

function loadCountry( country_id , view_mode)
{
    var url = location.origin + '/country/getDetailsById/' + country_id + '/' + view_mode;
    window.location.href =location.origin + '/country/getDetailsById/' + country_id + '/' + view_mode;
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {id: country_id}
    }).done(function (response) {
        jQuery('#country-details-information').replaceWith(response.html);
        countryDependency();
    });
}
function loadTransctionHistory( country_id )
{
    var url = location.origin + '/wallet/getWalletList/';
    
   var response =  jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {'country_id': country_id,'get_type':'ajax_call'},
        async:false
    }).responseText;
    if(response){
        jQuery('#country-details-information').replaceWith(response);
        dataTableLoader();
    }else{
        failureMessage('Please Try Again Later');
    }
}

function saveCountry() {
    var isValidate=false;
 // Validate the passenger details again in case the user changed the input
    var url = location.origin + '/country/saveCountry/';
    
    var countryData = {};
    var countryData = new window.FormData($('#edit_country_form')[0]);
    /*jQuery( jQuery('#edit_passenger_form :input').serializeArray() ).each(function( x , y ){
    	passengerData[y.name] = y.value;
    });*/
    isValidate=validate('edit_country_form');
    if(isValidate)
    {
    $('#save_country_btn').button('loading');
    
    jQuery.ajax({
        xhr: function () {  
            return $.ajaxSettings.xhr();
        },
        type: "POST",
        data: countryData,
        cache: false,
        contentType: false,
        processData: false,
        url: url,
    }).done(function (response) {
          response = jQuery.parseJSON(response);
        if(response.country_id)
        {
        	//successMessage( response.msg);
        	//loadPassengerList();
        	window.location.href = location.origin + '/country/getCountryList/';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	failureMessage( response.msg);
    	
    });
    }
    
}