$(document).ready(function() {
     var now = new Date();
    var cur_date      =  [now.getFullYear(),'-',now.getMonth(),'-',now.getDate(),'--',now.getHours(),'-',now.getMinutes(),'-',now.getSeconds()].join('');
     var buttonCommon = {
        exportOptions: {
            format: {
                body: function ( data, row, column, node ) {
                    // Strip $ from salary column to make it numeric
                    return column === 5 ?
                        data.replace( /[$,]/g, '' ) :
                        data;
                }
            }
        }
    };
   var driver_id = location.href.substr(location.href.lastIndexOf('/') + 1);
   driverLogHistoryDependency();
	table = $('#driver-shift-history-data-table').DataTable({

		"scrollX" : true,
		"scrollY" : 400,
		"scrollCollapse" : true,
		"processing" : true, //Feature control the processing indicator.
		"serverSide" : true, //Feature control DataTables' server-side processing mode.
		"order" : [], //Initial no order.
                
                "dom": 'Blrtip',
                "buttons": [
                            /*{
                            extend: 'copyHtml5',
                            exportOptions: {
                                columns: [ 0, ':visible' ]
                            }
                            },
                        {
                            extend: 'excelHtml5',
                            title: 'Drivers_Log_History_List_'+cur_date,
                            exportOptions: {
                            	columns: ':visible'
                            }
                        },*/
                        /*{
                            extend: 'pdfHtml5',
                            exportOptions: {
                                columns: [1, 2, 3,4,5,6,7,8,9 ]
                            }
                        },*/
                        //'colvis'
                ],
               
		// Load data for the table's content from an Ajax source
		"ajax" : {
			"url" : location.origin + '/driver/ajax_list_driverloghistory',
			"type" : "POST",
                        "data":{'driver_id':driver_id}
		},

		//Set column definition initialisation properties.s
		"columnDefs" : [ {
			"targets" : [ -1,1,2,3,4,0], //last column
			"orderable" : false, //set not orderable
		}, ],
		"lengthMenu" : [ [ 50,100,-1 ], [ 50, 100,'All' ] ],
		"fnDrawCallback" : function(oSettings) {
			driverLogHistoryDependency();
		}
	});

	$('label input.toggle-vis').each(function() {
		var column = table.column($(this).attr('data-column'));
		if ($(this).prop('checked') == true) {
			//column.toggle(this.checked);
			column.visible(true);
		} else {
			column.visible(false);
		}

	});
	$('label input.toggle-vis').on('change', function(e) {
		e.preventDefault();
		// Get the column API object
		var column = table.column($(this).attr('data-column'));
		if ($(this).prop('checked') == true) {
			//column.toggle(this.checked);
			column.visible(true);
		} else {
			column.visible(false);
		}
		driverLogHistoryDependency();

	});
});
function driverLogHistoryDependency() {
        
}