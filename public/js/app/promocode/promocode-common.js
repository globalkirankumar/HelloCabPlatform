function promocodeDependency()
{
	$('#save-promocode-btn').click(function(){
		 saveUser(); 
	});
	$('#cancel-promocode-btn').click(function(){
		// loadUserList(); 
		 window.location.href = location.origin + '/promocode/getPromocodeList';
	});
	$('[id^=status]').click(function(){
		var status=$(this).attr('status');
		var arr = $(this).attr('id').split('-');
	    var promocode_id = arr[1];
	    changePromocodeStatus(promocode_id,status); 
	});
	
	$('[id^=viewpromocode]').click(function(){
		var arr = $(this).attr('id').split('-');
	    var promocode_id = arr[1];
		 loadPromocode(promocode_id,'view'); 
	});
	$('[id^=editpromocode]').click(function(){
		var arr = $(this).attr('id').split('-');
	    var promocode_id = arr[1];
		 loadPromocode(promocode_id,'edit'); 
	});
	$('[id^=deletepromocode]').click(function(){
		var arr = $(this).attr('id').split('-');
	    var promocode_id = arr[1];
	    deletePromocode(promocode_id);
	});
	$('#PromoCode').blur(function(){
		 checkPromocode(); 
	});
	
	$('#promoStartDatetime').datetimepicker({
	       dayOfWeekStart : 1,
	       lang           : 'en',
	       startDate      : '0',
	       timepicker     : true,
	       format         : 'Y-m-d H:i',
	       step           :15
	       
	   });
	   $('#promoEndDatetime').datetimepicker({
	       dayOfWeekStart : 1,
	       lang           : 'en',
	       startDate      : '0',
	       timepicker     : true,
	       format         : 'Y-m-d H:i',
	       step           :15,
	    	   minDate: 0,
	           //minTime: 0
	           onShow:function( ct ){
	               if($('#promoStartDatetime').val()!='') {
	                   this.setOptions({
	                    minDate:jQuery('#promoStartDatetime').val()?jQuery('#promoStartDatetime').val():false,
	                    minTime:jQuery('#promoStartDatetime').val()?jQuery('#promoStartDatetime').val():false,
	                   })
	               }
	              }
	   });
	
}

function changePromocodeStatus(promocode_id,status)
{
	if (confirm("Make sure before changing promocode status?."))
    {
    if(status==1)
    {
    	status=0;
    }
    else
    {
    	status=1;
    }
    
    var url = location.origin + '/promocode/changePromocodeStatus/';
    
    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'id':promocode_id,'status':status},
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response.status > 0)
        {
        	
        	successMessage( response.msg);
        	//loadUserList();
        	window.location.href = location.origin + '/promocode/getPromocodeList';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	
    	failureMessage( 'Status change failed.. Please try again later.');
    	
    });
    }
    
}

function loadPromocodeList()
{
    // Validate the passenger value again in case the user changed the input
    var url = location.origin + '/promocode/getPromocodeList';
    jQuery.ajax({
        method: "POST",
        url: url,
        //data:{'get_type':'ajax_call'},
    }).done(function (response) {
        //jQuery('#content').html(response);
    });
}

function loadPromocode( promocode_id , view_mode)
{
   // var url = location.origin + '/user/getDetailsById/' + promocode_id + '/' + view_mode;
    window.location.href =location.origin + '/promocode/getDetailsById/' + promocode_id + '/' + view_mode;
    /*jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {id: user_id}
    }).done(function (response) {
        jQuery('#container').html(response.html);
        userDependency();
    });*/
}

function savePromocode() {
	
    var isValidate=false;
 // Validate the user details again in case the user changed the input
    var url = location.origin + '/promocode/savePromocode/';
    
    var userData = {};
    var userData = new window.FormData($('#edit_promocode_form')[0]);
    /*jQuery( jQuery('#edit_passenger_form :input').serializeArray() ).each(function( x , y ){
    	passengerData[y.name] = y.value;
    });*/
   
    isValidate=validate('edit_promocode_form');
    if(isValidate)
    {
    jQuery.ajax({
        xhr: function () {  
            return $.ajaxSettings.xhr();
        },
        type: "POST",
        data: userData,
        cache: false,
        contentType: false,
        processData: false,
        url: url,
    }).done(function (response) {
          response = jQuery.parseJSON(response);
        if(response.driver_id)
        {
        	successMessage( response.msg);
        	//loadUserList();
        	window.location.href = location.origin + '/promocode/getPromocodeList';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	failureMessage( response.msg);
    	
    });
    }
    
}

function checkPromocode()
{
	var promocode=$('#promoCode').val();
	if(promocode != '')
	{
		$('#save-promocode-btn').prop('disabled', true);
    var url = location.origin + '/promocode/checkPromocode/'; 
    jQuery.ajax({
        method: "POST",
        data:{'promocode':promocode},
        url: url
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response.status==1)
    	{
    		$('#promoCode').val('');
    	}
    	$('#save-promocode-btn').prop('disabled', false);
    	infoMessage(response.msg,1,'#promoCode');
    });
	}
}