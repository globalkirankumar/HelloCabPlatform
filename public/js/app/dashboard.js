$(document).ready(function() {
    var now = new Date();
    var cur_date      =  [now.getFullYear(),'-',now.getMonth(),'-',now.getDate(),'--',now.getHours(),'-',now.getMinutes(),'-',now.getSeconds()].join('');
	//datatables
   
    $('#force-update-btn').click(
			function() {

				if (confirm("Do you want Driver APP to Force Update?")) {
					var url = location.origin + '/driver/forceUpdateAllDriver/';
					$("#overlay").removeClass('hidden');
					jQuery.ajax({
						method : "POST",
						url : url
					}).done(

							function(response) {
								$("#overlay").addClass('hidden');
								response = jQuery.parseJSON(response);
								if (response.status > 0) {
									var socket = io.connect("http://203.81.73.38:2002");

									socket.emit("force_update", "Let Force Update!");
									// Add a connect listener
								    socket.on('force_update',function() {
								    	successMessage(response.msg);
								      console.log('Let Force Update!');
								    });
								   
								} else {
									failureMessage(response.msg);
								}

							}).fail(function(jqXHR, textStatus, errorThrown) {
						$("#overlay").addClass('hidden');
						failureMessage(response.msg);

					});
				}
			});
	$('#trip-dispatch-btn').click(
			function() {
				var url = location.origin + '/Dashboard/tripDispatchNotification/';
				jQuery.ajax({
					method : "POST",
					url : url
				}).done(function(response) {
					response = jQuery.parseJSON(response);
					if (response.status == 1) {
						alert('Trip Dispatch success');
					}
					else
					{
						alert('Trip Dispatch failed');
					}
					
				});
			});
	$('#trip-start-btn').click(
			function() {
				var url = location.origin + '/Dashboard/tripStartNotification/';
				jQuery.ajax({
					method : "POST",
					url : url
				}).done(function(response) {
					response = jQuery.parseJSON(response);
					if (response.status == 1) {
						alert('Trip Start success')
					}
					else
					{
						alert('Trip Start failed');
					}
					
				});
			});
	$('#trip-end-btn').click(
			function() {
				var url = location.origin + '/Dashboard/tripEndNotification/';
				jQuery.ajax({
					method : "POST",
					url : url
				}).done(function(response) {
					response = jQuery.parseJSON(response);
					if (response.status == 1) {
						alert('Trip End success');
					}
					else
					{
						alert('Trip End failed');
					}
					
				});
			});
                        
/**** Chart Section Starts ***/
  

    

    
                        
 /**** Chart Section Ends ***/          
 
 /*** Header Zone and SubZone section starts ***/
    getZones(); //get parent zones
        
 $('#header_zone').on('change' , function(){
    var zone_id = $(this).val();
    var count = 1;
     getSubZones(zone_id,count);
 });
                        
});

	
function getSubZones(zone_id,count){
    if(zone_id > 0){
         var url    = location.origin+'/Zone/getSubZoneListByZoneId';
		jQuery.ajax({
                    method : "POST",
                    url : url,
                    data : {'zone_id':zone_id},
		}).done(
                function(response) {
                    response = jQuery.parseJSON(response);
                    count    = response['count'];
                    if(count !='0'){
                       $("#header_subzone").find('option').not(':first').remove();
                            $.each(response, function(key, val) {
                               var zone_id   = val['zoneId'];
                               var zone_name = val['name'];
                               var option = $('<option/>');
                                 option.attr('value', zone_id).text(zone_name);
                                 $('#header_subzone').append(option);
                         });
                        $("#header_subzone").selectpicker("refresh");
                         var url = location.href;
                        var segments = url.split( '/' );
                        var zone    = segments[6];
                        var subzone_select_filter    = segments[7];
                        //getSubZones(zone,1);
                         if(subzone_select_filter > 0){
                            $('#header_subzone').selectpicker('val', subzone_select_filter);
                         }else{
                            $('#header_subzone').selectpicker('val', ''); 
                         }
                        $("#header_subzone").prop("disabled",false);
                       // $('#header_subzone').selectpicker('toggle');
                    }else{
                        //$('#header_subzone').prop('disabled', true);
                        $("#header_subzone").find('option').not(':first').remove();
                        $("#header_subzone").selectpicker("refresh");
                    }
                    }).fail(function(jqXHR, textStatus, errorThrown) {
                         failureMessage('Unable to Fetch.. Please try again later.');
                }); 
     }
}
function getZones() {
        var url    = location.origin+'/Zone/getParentZoneList';
		jQuery.ajax({
                    method : "POST",
                    url : url,
                    data : {},
		}).done(
                function(response) {
                        response = jQuery.parseJSON(response);
                       $.each(response, function(key, val) {
                          var zone_id   = val['zoneId'];
                          var zone_name = val['name'];
                          var option = $('<option/>');
                            option.attr('value', zone_id).text(zone_name);
                            $('#header_zone').append(option);
                    });
                    $("#header_zone").selectpicker("refresh");
                    var url = location.href;
                    var segments = url.split( '/' );
                    var zone_select_filter    = segments[6];
                     if(zone_select_filter > 0){
                        $('#header_zone').selectpicker('val', zone_select_filter);
                     }else{
                        $('#header_zone').selectpicker('val', ''); 
                     }  
                   
                    $("#header_zone").prop("disabled",false);
                    }).fail(function(jqXHR, textStatus, errorThrown) {
                         failureMessage('Unable to Fetch.. Please try again later.');
                }); 
                
    }