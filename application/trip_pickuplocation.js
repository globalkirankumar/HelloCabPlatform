/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
                var markers = [];
                var map;
                var myCenter = new google.maps.LatLng(12.9716,77.5946);
                var marker = new google.maps.Marker({
                    position: myCenter
                });

                function initialize() {
                    var input = document.getElementById('searchTextField');
                    var bounds = new google.maps.LatLngBounds( );
                    var options = {
                        bounds: bounds,
                        types: ['geocode'],
                        componentRestrictions: {country: 'in'}
                    };
                    var autocomplete = new google.maps.places.Autocomplete(input, options);
                    google.maps.event.addListener(autocomplete, 'place_changed', function () {
                        var autocomplete_geocoder = new google.maps.Geocoder();
                        var place = autocomplete.getPlace();
                        $('#pickupLatitude').val(place.geometry.location.lat());
                        $('#pickupLongitude').val(place.geometry.location.lng());
                        var map;
                        var coord1 = new google.maps.LatLng(document.getElementById('pickupLatitude').value, document.getElementById('pickupLongitude').value);
                        var bounds = new google.maps.LatLngBounds();
                        bounds.extend(coord1);
                        clearMarkers();
                        addMarker(coord1);
                        autocomplete_geocoder.geocode({
                            'latLng': coord1
                        }, function (results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                console.log(results);
                                if (results[0]) {

                                    var arrAddress = results[0].address_components;
                                    var address = results[0].formatted_address;
                                    $("#pickupLocation").val(address);
                                    var itemRoute = '';
                                    var itemLocality = '';
                                    var itemCountry = '';
                                    var itemPc = '';
                                    var itemSnumber = '';
                                    $.each(arrAddress, function (i, address_component) {
                                        console.log('address_component:' + i);

                                        if (address_component.types[0] == "route") {
                                            console.log(i + ": route:" + address_component.long_name);
                                            itemRoute = address_component.long_name;
                                        }

                                        if (address_component.types[0] == "locality") {
                                            console.log("town:" + address_component.long_name);
                                            itemLocality = address_component.long_name;
                                            $("#locality").val(itemLocality);
                                        }
                                    });
                                }
                            }
                        });

                    });

                    var myLatlng = new google.maps.LatLng(12.9716,77.5946);
                    var myOptions = {
                        zoom: 12,
                        center: myLatlng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    }
                    map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
                    var geocoder = new google.maps.Geocoder();
                    google.maps.event.addListener(map, "click", function (event) {
                        // get lat/lon of click
                        var clickLat = event.latLng.lat();
                        var clickLon = event.latLng.lng();
                        //alert(event);
                        console.log(event);
                        // show in input box
                        $("#pickupLatitude").val(clickLat.toFixed(5));
                        $("#pickupLongitude").val(clickLon.toFixed(5));
                        clearMarkers();
                        addMarker(event.latLng);

                        fillDetails(event.latLng)

                    });
                }
                function addMarker(location) {
                    //alert(location);
                    var marker = new google.maps.Marker({
                        draggable: true,
                        position: location,
                        map: map,
                        animation: google.maps.Animation.DROP
                    });
                    markers.push(marker);
                    map.setCenter(location);
                    //map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
                    google.maps.event.addListener(marker, 'dragend', function (event) {
                     
                        var clickLat = event.latLng.lat();
                        var clickLon = event.latLng.lng();
                    
                        $("#pickupLatitude").val(clickLat.toFixed(5));
                        $("#pickupLongitude").val(clickLon.toFixed(5));
                        fillDetails(event.latLng);

                    });


                }
                function setMapOnAll(map) {
                    for (var i = 0; i < markers.length; i++) {
                        markers[i].setMap(map);
                    }
                }
                function fillDetails(latlang) {
                    var geocoder = new google.maps.Geocoder();

                    geocoder.geocode({
                        'latLng': latlang
                    }, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            console.log(results);
                            if (results[0]) {

                                var arrAddress = results[0].address_components;
                                var address = results[0].formatted_address;
                                $("#pickupLocation").val(address);
                                $("#searchTextField").val(address);

                                var itemRoute = '';
                                var itemLocality = '';
                                var itemCountry = '';
                                var itemPc = '';
                                var itemSnumber = '';
                                $.each(arrAddress, function (i, address_component) {
                                    console.log('address_component:' + i);

                                    if (address_component.types[0] == "route") {
                                        console.log(i + ": route:" + address_component.long_name);
                                        itemRoute = address_component.long_name;
                                       // alert(itemRoute);
                                    }

                                    if (address_component.types[0] == "locality") {
                                        console.log("town:" + address_component.short_name);
                                        itemLocality = address_component.short_name;
                                        //$('#locality').val(itemLocality);
                                    }
                                });
                            }
                        }
                    });
                }

                function clearMarkers() {
                    setMapOnAll(null);
                }
                google.maps.event.addDomListener(window, 'load', initialize);
                google.maps.event.addDomListener(window, "resize", resizingMap());

                $('#myMapModal').on('show.bs.modal', function () {
                    //Must wait until the render of the modal appear, thats why we use the resizeMap and NOT resizingMap!! ;-)
                    resizeMap();
                });

                function resizeMap() {
                    if (typeof map == "undefined")
                        return;
                    setTimeout(function () {
                        resizingMap();
                    }, 400);
                }

                function resizingMap() {
                    if (typeof map == "undefined")
                        return;
                    var center = map.getCenter();
                    google.maps.event.trigger(map, "resize");
                    map.setCenter(center);
                }

            });