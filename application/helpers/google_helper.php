<?php
/**
 * To Get the postal address by Latitude and Longitude
 * @param unknown $latitude
 * @param unknown $longitude
 * @return Ambigous <NULL, string>
 */
function getAddressFromLatLong($latitude, $longitude) {
	$address='';
	$geolocation = $latitude . ',' . $longitude;
	$request = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' . $geolocation . '&sensor=false';
	$file_contents = file_get_contents ( $request );
	$json_decode = json_decode ( $file_contents );
	if ($json_decode)
	{
	if ($json_decode->results [0]->formatted_address) {
			$address = $json_decode->results [0]->formatted_address;
	} 
	}
	return $address;
	
}