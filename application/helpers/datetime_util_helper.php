<?php

function getCurrentDate($timezone=TIMEZONE)
{
	date_default_timezone_set($timezone);
	return (1 ) ? date('Y-m-d') : $_SESSION['current_date'];
}
function getCurrentUnixDate($timezone=TIMEZONE)
{
	date_default_timezone_set($timezone);
	return strtotime(getCurrentDate());
}

function getCurrentDateTime($timezone=TIMEZONE)
{
	date_default_timezone_set($timezone);
	return (1) ? date('Y-m-d H:i:s') : $_SESSION['current_date'];
}
function getCurrentUnixDateTime($timezone=TIMEZONE)
{
	date_default_timezone_set($timezone);
	return strtotime(getCurrentDateTime());
}

function dateDropDownDisplayFormat($mysqlDate){
	if($mysqlDate)
	{
		$date = DateTime::createFromFormat(DATE_MYSQL_FORMAT, $mysqlDate);
		return $date->format(DATE_DROPDOWN_FORMAT);
	}
	return '';
}
function dateTimeDropDownDisplayFormat($mysqlDate){
	if($mysqlDate)
	{
		$date = DateTime::createFromFormat(DATETIME_MYSQL_FORMAT, $mysqlDate);
		return $date->format(DATETIME_DROPDOWN_FORMAT);
	}
	return '';
}
/**
 * 
 * @param type $mysqlDate
 * @return mysql date format
 */
function dateMySQLFormat($mysqlDate){
	if($mysqlDate)
	{
		$date = DateTime::createFromFormat(DATE_TABLE_FORMAT, $mysqlDate);
		return $date->format(DATE_MYSQL_FORMAT);
	}
	return '';
}
function datePageFormat($mysqlDate,$timestamp=true)
{
	if($mysqlDate && $timestamp==true)
	{
		return date(DATE_PAGE_FORMAT, $mysqlDate);
	}
	else if($mysqlDate){
		return date(DATE_PAGE_FORMAT, strtotime($mysqlDate));
	}
	return '';
}
function dateDisplayFormat($mysqlDate)
{
	if($mysqlDate)
	{	
		return date(DATE_DISPLAY_FORMAT, strtotime($mysqlDate));
	}
	return '';
}
function dateTimeMySQLFormat($mysqlDate){
	if($mysqlDate)
	{
		//$date = DateTime::createFromFormat(DATETIME_TABLE_FORMAT, $mysqlDate);
		//return $date->format(DATETIME_MYSQL_FORMAT);
		return date(DATETIME_MYSQL_FORMAT, strtotime($mysqlDate));
	}
	return '';
}
function dateTimePageFormat($mysqlDate,$timestamp=true)
{
	if($mysqlDate && $timestamp==true)
	{
		return date(DATETIME_PAGE_FORMAT, $mysqlDate);
	}
	else if($mysqlDate){
		return date(DATETIME_PAGE_FORMAT, strtotime($mysqlDate));
	}
	return '';
}
function dateTime24DisplayFormat($mysqlDate)
{
	if($mysqlDate)
	{
		return date(DATETIME_DISPLAY_24_FORMAT, strtotime($mysqlDate));
	}
	return '';
}
function dateTime12DisplayFormat($mysqlDate)
{
	if($mysqlDate)
	{
		return date(DATETIME_DISPLAY_12_FORMAT, strtotime($mysqlDate));
	}
	return '';
}

function TimeIsBetweenTwoTimes($start, $end, $current_time) {
	$current_time=date('H:i:s',strtotime($current_time));
	$start = DateTime::createFromFormat('H:i:s', $start);
	$end = DateTime::createFromFormat('H:i:s', $end);
	$current_time = DateTime::createFromFormat('H:i:s', $current_time);
	if ($start > $end) $end->modify('+1 day');
	return ($start <= $current_time && $current_time <= $end) || ($start <= $current_time->modify('+1 day') && $current_time <= $end);
}

function convertMinsInToHoursMins($time, $format = '%02d:%02d') {
	if ($time < 1) {
		return;
	}
	$hours = floor($time / 60);
	$minutes = ($time % 60);
	return sprintf($format, $hours, $minutes);
}