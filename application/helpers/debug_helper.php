<?php

function debug( $arg )
{
    echo "<pre>";
    print_r( $arg );
    echo "</pre>";
}

function debug_exit( $arg )
{
	echo "<pre>";
	print_r( $arg );
	echo "</pre>";
	exit();
}

function text($string)
{
	
    return '<span class="form-control col-md-7 col-xs-12 viewtext gp-alignleft">'.$string.'</span>';
}


function getDescriptionById($id)
{
	$sql='select description from dataattributes where id='.$id;
	$results=$this->db->query($sql);
	$result=$results->result_array();
	return $result[0];
}

function form_status_dropdown($name = '', $options = array(), $selected = array(), $extra = '')
{
    if ( ! is_array($selected))
    {
        $selected = array($selected);
    }

    // If no selected state was submitted we will attempt to set it automatically
    if (count($selected) === 0)
    {
        // If the form name appears in the $_POST array we have a winner!
        if (isset($_POST[$name]))
        {
            $selected = array($_POST[$name]);
        }
    }

    if ($extra != '') $extra = ' '.$extra;

    $extra .= ' class="selectpicker "';

    $multiple = (count($selected) > 1 && strpos($extra, 'multiple') === FALSE) ? ' multiple="multiple"' : '';

    $form = '<select name="'.$name.'"'.$extra.$multiple.">\n";

    foreach ($options as $key => $model )
    {
        $key = (string) $model->get('id');
        $val = (string) $model->get('description');
        if (is_array($val) && ! empty($val))
        {
            $form .= '<optgroup label="'.$key.'">'."\n";

            foreach ($val as $optgroup_key => $optgroup_val)
            {
                $sel = (in_array($optgroup_key, $selected)) ? ' selected="selected"' : '';

                $form .= '<option value="'.$optgroup_key.'"'.$sel.'>'.(string) $optgroup_val."</option>\n";
            }

            $form .= '</optgroup>'."\n";
        }
        else
        {
            $sel = (in_array($key, $selected)) ? ' selected="selected"' : '';
            $form .= ' <option data-content="<span class=\'btn-circle '.$model->get('internal_name').'\'></span> <span class=\'status-text\'>'. $val.'</span>" value="'.$key.'"'.$sel.'>'.(string) $val.'</option>';
        }
    }

    $form .= '</select>';

    return $form;
}

function extractModel( $models , $key , $value)
{
    foreach( $models AS $element )
    {
        if( $element->get($key) == $value ){
            return $element;
        }
        return FALSE;
    }


}
