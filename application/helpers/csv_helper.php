<?php
defined('BASEPATH') OR exit('No direct script access allowed');


 if (!function_exists('convert_to_csv')) {

	function convert_to_csv($input_array, $upload_path,$filename, $delimiter=",") {
		
		//modify header to be downloadable csv file
		//header('Content-Type: application/csv');
		//header('Content-Disposition: attachement; filename="' . $filename . '";');
		//debug_exit($input_array);
		if (! file_exists ($upload_path)) {
			mkdir ( $upload_path, 0777);
		}
		$file = $upload_path . "/" . $filename;
		// open raw memory as file, no need for temp files 
		$file_open = fopen($file, 'w');
		// loop through array 
		foreach ($input_array as $line) {
			//default php csv handler 
			fputcsv($file_open, $line, $delimiter);
		}
		// rewrind the "file" with the csv lines
		fclose($file_open);
		
	}

} 

