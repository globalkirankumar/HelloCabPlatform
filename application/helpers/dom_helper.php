<?php

function get_htmlDocument()
{
	$impl = new DOMImplementation();
	$doctype = $impl->createDocumentType('html', '-//W3C//DTD XHTML 1.1//EN', 'http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd');
	$doc = $impl->createDocument('http://www.w3.org/1999/xhtml', 'html', $doctype);
	$doc->formatOutput = true; // a bit more resource-intensive; consider turning off in production
	return $doc;
}

function dom_appendLabel($parent, &$attributes)
{
	if (isset($attributes['label']) && isset($attributes['id']))
	{
		$label = $parent->appendChild(new DOMElement('label', $attributes['label']));
		$label->setAttribute('for', $attributes['id']);
		unset($attributes['label']);
	}
}

function dom_addAttributes($dom_element, $attributes)
{
	foreach ($attributes as $key => $value)
	{
		if (is_scalar($value))
			$dom_element->setAttribute($key, $value);
	}
}

function appendInput($parent, $attributes)
{
	dom_appendLabel($parent, $attributes);
	$input = $parent->appendChild(new DOMElement('input'));
	dom_addAttributes($input, $attributes);
	
	return $input;
}

function appendTextArea($parent, $attributes)
{
	dom_appendLabel($parent, $attributes);
	$input = $parent->appendChild(new DOMElement('textarea', $attributes['value']));
	dom_addAttributes($input, $attributes);
	
	return $input;
}

function appendSelect($parent, $attributes)
{
	dom_appendLabel($parent, $attributes);
	$select = $parent->appendChild(new DOMElement('select'));
	
	$options = $attributes['options'];
	foreach($options as $value => $text)
	{
		// sometimes we get a list of objects with names and id's
		if (is_object($text) && !(isset($text->isGroup) && $text->isGroup)) {
			if (isset($text->id) && isset($text->name)) {
				$value = $text->id;
				$text = $text->name;
			}
			else {
				// got an object, but cant make sense of it
				log_message('error', '^^^^^^^^        appendSelect got an object it could not figure out: '.print_r($text, true));
			}
		}
		
		if(is_scalar($text)) {
			if (!is_string($text))
				$text = strval($text);
			if (strlen($text) == 0)
				$text=" ";
			$option = $select->appendChild(new DOMElement('option', htmlspecialchars($text)));
			$option->setAttribute('value', $value);
			if (isset($attributes['value']) && (string)$value == (string)$attributes['value'])
				$option->setAttribute('selected', 'selected');
		}
	}
	unset($attributes['value']);
	dom_addAttributes($select, $attributes);
	
	return $select;
}

function createElementWithAttributes ($document, $type, $attrs, $contents = NULL)
{
	if (isset($contents) && $contents != NULL)
		$element = $document->createElement($type, $contents);
	else
		$element = $document->createElement($type);
	foreach($attrs as $name => $value)
	{
		$element->setAttribute($name, $value);
	}
	return $element;
}

function appendElementWithAttributes ($parent, $type, $attrs, $contents = NULL)
{
	if (isset($contents) && $contents != NULL)
		$element = $parent->appendChild(new DOMElement($type, $contents));
	else
		$element = $parent->appendChild(new DOMElement($type));

	foreach($attrs as $name => $value)
	{
		$element->setAttribute($name, $value);
	}
	return $element;
}

function appendSpan($parent, $attrs, $contents = NULL) {
    return appendElementWithAttributes($parent, 'span', $attrs, $contents);
}

function appendImage ($parent, $src, $attrs=array())
{
	$element = $parent->appendChild(new DOMElement('img'));
	$element->setAttribute('src', image_url($src));
	foreach($attrs as $name => $value)
	{
		$element->setAttribute($name, $value);
	}
	return $element;
}

function appendAnchor($parent, $href, $attrs=array())
{
	$a = $parent->appendChild(new DOMElement('a'));
	$a->setAttribute('href', $href);
	foreach($attrs as $name => $value)
	{
		$a->setAttribute($name, $value);
	}
	return $a;
}

function appendLabelValuePairReadonly($parent, $field_name, $field_value, $label_text)
{
	$label = $parent->appendChild(new DOMElement('label', $label_text));
	$label->setAttribute('for', $field_name.'-field');
	$value = $parent->appendChild(new DOMElement('div'));
	$value->setAttribute('class', 'readonly');
	$value->setAttribute('id', $field_name.'-field');
	$value->appendChild(new DOMText($field_value));
}

function appendFormSpacer($parent)
{
	$spacer = $parent->appendChild(new DOMElement('div'));
	$spacer->setAttribute('class', 'form_spacer');
}

