<?php
   function file_upload($img_form_name, $value, $img_selected, $img_prefix, $upload_path,$model,$key) {
		if (! file_exists ( $upload_path )) {
			mkdir ( $upload_path, 0777 );
		}
	
		// $profile_upload_path = driver_profile_url();
		$upload = do_upload ( $img_form_name, $img_selected, $img_prefix, $upload_path );
		$upload_image_name = implode ( ',', $upload );
		if ($upload) {
			$data [$img_form_name] = $upload_image_name;
			$CI = & get_instance();
			$CI->load->model($model);
			$update_prof_imgname = $CI->$model->update ( $data, array (
					$key => $value
			) );
		}
	}
	
	function do_upload($img_form_name, $selected_img, $img_prefix, $upload_path) {
		$upload_image_name = array ();
		// Count # of uploaded files in array
		$total = count ( $_FILES [$img_form_name] ['name'] );
		// Loop through each file
		for($i = 0; $i < $total; $i ++) {
			// Get the temp file path
			$tmpFilePath = $_FILES [$img_form_name] ['tmp_name'] [$i];
	
			// Make sure we have a filepath
			if ($tmpFilePath != "") {
				// get image extension
				$img_ext = pathinfo ( $selected_img [$i], PATHINFO_EXTENSION );
				$image_full_name = $img_prefix . "-$i" . '-'.getCurrentUnixDateTime().'.'. $img_ext;
				$newFilePath = $upload_path . '/' . $image_full_name;
	
				$directory = $upload_path . '/';
				file_delete($directory, $img_prefix);
	
				// Upload the file into the temp dir
				if (move_uploaded_file ( $tmpFilePath, $newFilePath )) {
	
					$upload_image_name [] = $image_full_name;
				}
			}
		}
		return $upload_image_name;
	}
	function file_delete($directory,$file_prefix)
	{
		$files_list = glob($directory . $file_prefix."*");
	
		foreach ($files_list as $files){
			unlink($files);
		}
	}