<?php
/**** Code to Find Zone Based on Lat and Long ****/
function getZoneByLatLong($lat=NULL,$long=NULL){
	
	$polyX      =  array();//horizontal coordinates of corners
	$polyY      =  array();//vertical coordinates of corners
	$CI = & get_instance();
	$CI->load->model('Zone_Details_Model');
	$zone_list  = $CI->Zone_Details_Model->getZoneListQueryForTripCreation();
	$res = array();
	if($zone_list){
		foreach($zone_list as $list){
			$polygan_points =  $list->polygonPoints;
			$zone_name      =  $list->name;
			$zone_id        =  $list->zoneId;
                        $res[]          =  process_poylgan_data($polygan_points,$lat,$long,$zone_name,$zone_id);
		}
               $arr_res = array_filter($res);
               $resultarr                  =   array_reduce($arr_res, 'array_merge', array());
              if(count($resultarr) > 0){
                  return $resultarr[0];
              }else{
                  return 0;
              }
        }else{
            return 0;
        }
}// end of function

/*** Code to Get SubZone POlygan ***/
function getSubZoneByLatLong($lat=NULL,$long=NULL){
	
	$polyX      =  array();//horizontal coordinates of corners
	$polyY      =  array();//vertical coordinates of corners

	$CI = & get_instance();
	$CI->load->model('Zone_Details_Model');
	$zone_list  = $CI->Zone_Details_Model->getSubZoneListQueryForTripCreation();
	
	if($zone_list){
		foreach($zone_list as $list){
			$polygan_points =  $list->polygonPoints;
			$zone_name      =  $list->name;
			$zone_id        =  $list->zoneId;
			$res[] =  subzone_process_poylgan_data($polygan_points,$lat,$long,$zone_name,$zone_id);
		}
		return $res;
	}
}

function process_poylgan_data($polygan_points,$lat,$long,$zone_name,$zone_id){
	//$zonal      =   "16.78205,96.14767:16.78252,96.13659:16.77558,96.13608:16.7736,96.13544:16.77097,96.14895:16.76875,96.15818:16.78145,96.15878:16.78205,96.14767";
	$zonal      =   $polygan_points;

	$zonalArray =   explode("|",$zonal);
	$polySides  = count($zonalArray); //how many corners the polygon has
	foreach($zonalArray as $eachZoneLatLongs)
	{
		$latlongArray   =   explode(",",$eachZoneLatLongs);
		$polyX[]    =   isset($latlongArray[0]) ? $latlongArray[0] : '';
		$polyY[]    =   isset($latlongArray[1]) ? $latlongArray[1] : '';
	}

	$vertices_x = $polyX;                     // x-coordinates of the vertices of the polygon
	$vertices_y = $polyY;                     // y-coordinates of the vertices of the polygon
	$points_polygon = count($vertices_x);     // number vertices
	#Following Points lie inside this region
	$longitude_x = $lat ;                     // x-coordinate of the point to test
	$latitude_y  = $long;                     // y-coordinate of the point to test
        $zonearr     = array();
	if (is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y)){
		//echo "Is in polygon: $zone_name".'<br>';
              $subzone_data = array_filter(getSubZoneByLatLong($longitude_x,$latitude_y));
              $subzone_data = array_values($subzone_data);
                  if(count($subzone_data) > 0){
                      $subzone_arr = explode(',',@$subzone_data[0]);
                      $zonearr[]=  $subzone_arr[0];
                      //echo json_encode(array('ZoneId'=>$zone_id,'Zone_name'=>$zone_name,'subZoneId'=>@$subzone_arr[0],'subZone_name'=>@$subzone_arr[1]));
                  }else{
                       //echo json_encode(array('ZoneId'=>$zone_id,'Zone_name'=>$zone_name,'subZoneId'=>'0','subZone_name'=>'0'));
                       $zonearr[]= $zone_id;
                  }
                  
                  return $zonearr;
	}

}



function subzone_process_poylgan_data($polygan_points,$lat,$long,$zone_name,$zone_id){
	$zonal      =   $polygan_points;
	$zonalArray =   explode("|",$zonal);
	$polySides  = count($zonalArray); //how many corners the polygon has
	foreach($zonalArray as $eachZoneLatLongs)
	{
		$latlongArray   =   explode(",",$eachZoneLatLongs);
		$polyX[]    =   isset($latlongArray[0]) ? $latlongArray[0] : '';
		$polyY[]    =   isset($latlongArray[1]) ? $latlongArray[1] : '';
	}

	$vertices_x = $polyX;                     // x-coordinates of the vertices of the polygon
	$vertices_y = $polyY;                     // y-coordinates of the vertices of the polygon
	$points_polygon = count($vertices_x);     // number vertices
	#Following Points lie inside this region
	$longitude_x = $lat ;                     // x-coordinate of the point to test
	$latitude_y  = $long;                     // y-coordinate of the point to test

	if (is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y)){
		return $zone_id.','.$zone_name;
	}

}
function is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y){
	$i = $j = $c = 0;
	 
	for ($i = 0, $j = $points_polygon-1 ; $i < $points_polygon; $j = $i++) {
		$vertices_y[$i]=(float)$vertices_y[$i];
		$vertices_y[$j]=(float)$vertices_y[$j];
		$vertices_x[$i]=(float)$vertices_x[$i];
		$vertices_x[$j]=(float)$vertices_x[$j];
		if ( (($vertices_y[$i] > $latitude_y != ($vertices_y[$j] > $latitude_y)) &&
            ($longitude_x < ($vertices_x[$j] - $vertices_x[$i]) * ($latitude_y - $vertices_y[$i]) / ($vertices_y[$j] - $vertices_y[$i]) + $vertices_x[$i]) ) )
					$c = !$c;
	}
	return $c;
}

