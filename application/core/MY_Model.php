<?php
class MY_Model extends CI_Model {
	protected $_table;
	
	public function getAutoIncrementValue() {
		$sql = "SELECT AUTO_INCREMENT FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '" . $this->getTableName() . "' AND TABLE_SCHEMA = '" . $this->db->database . "'";
		$query = $this->db->query ( $sql )->result ();
		// print_r($query[0]->AUTO_INCREMENT);exit();
		if ($query [0]->AUTO_INCREMENT) {
			return $query [0]->AUTO_INCREMENT;
		} else {
			return false;
		}
	}
	public function getTableName($table_prefix='')
	{
		$db_table='';
		if ($table_prefix)
		{
			$db_table=$table_prefix.''.$this->_table;
		}
		else if($this->session->userdata ( 'table_prefix' ))
		{
			$db_table=$this->session->userdata ( 'table_prefix' )."_".$this->_table;
		}
		else 
		{
			$db_table=$this->_table;
		}
		return $db_table;
	}
	public function __construct($args = null) {
		parent::__construct ();
		
		// loading cache
		// $this->load->library('../cache/cachedata');
	}
	private function _getMethodName($key) {
		// Replace _ with space, capitalizes words, then remove spaces
		return str_replace ( ' ', '', ucwords ( str_replace ( '_', ' ', $key ) ) );
	}
	public function get($key) {
		$methodName = 'get' . $this->_getMethodName ( $key );
		if (method_exists ( $this, $methodName )) {
			return $this->$methodName ();
		}
		if (property_exists ( $this, $key )) {
			return $this->$key;
		}
		return FALSE;
	}
	public function set($key, $value) {
		if (property_exists ( $this, $key )) {
			$methodName = 'set' . $this->_getMethodName ( $key );
			if (method_exists ( $this, $methodName )) {
				return $this->$methodName ( $key, $value );
			} else {
				$this->$key = (is_array ( $value )) ? serialize ( $value ) : $value;
				return $this->$key;
			}
		}
	}
	public function getValueByColumnName($key, $value, $column, $count = false) {
		$this->db->select ( $column );
		$this->db->where ( $key, $value );
		$query = $this->db->get ( $this->getTableName() );
		if ($count == false) {
			return $query->row_array ();
		}
		return $query->result_array ();
	}
	public function getBlankModel() {
		$db_columns = $this->db->list_fields ( $this->getTableName() );
		$class = new ReflectionClass ( get_called_class () );
		$data = array_fill_keys ( $db_columns, '' );
		return $class->newInstance ( $data );
	}
	public function _cleanseDate($string) {
		if (strtotime ( $string ) === FALSE) {
			return '0000-00-00';
		}
		return date ( 'Y-m-d', strtotime ( $string ) );
	}
	
	/**
	 * Fetches all rows from table as an array of objects
	 *
	 * @return array
	 */
	public function getAll($orderby = NULL) {
		if ($orderby) {
			$this->db->order_by ( $orderby );
		}
		$query = $this->db->get ($this->getTableName() );
		return $this->fetchAll ( $query );
	}
	
	/**
	 * Given a CI query, it returns an array of objects
	 *
	 * @param $query CI
	 *        	query
	 * @return array
	 */
	public function fetchAll($query) {
		$result = array ();
		foreach ( $query->result () as $row ) {
			$class = new ReflectionClass ( get_called_class () );
			$result [] = $class->newInstance ( $row );
		}
		return $result;
	}
	
	/**
	 * Retrieves by a single where clause
	 *
	 * @param $key database
	 *        	key
	 * @param $value Value
	 *        	of where clause
	 * @return array
	 */
	public function getByKeyValue($key, $value, $orderby = '') {
		if (gettype ( $value ) == 'array') {
			$this->db->where_in ( $key, $value );
		} else {
			$this->db->where ( $key, $value );
		}
		if ($orderby != '') {
			$this->db->order_by ( $orderby );
		}
		$query = $this->db->get ( $this->getTableName() );
		return $this->fetchAll ( $query );
	}
	
	/**
	 * Retrieves by an array of where clauses
	 *
	 * @param $key database
	 *        	key
	 * @param $value Value
	 *        	of where clause
	 * @return array
	 */
	public function getByKeyValueArray($where_clauses, $orderby = '') {
		foreach ( $where_clauses as $key => $value ) {
			
			if (gettype ( $value ) == 'array') {
				$this->db->where_in ( $key, $value );
			} else {
				$this->db->where ( $key, $value );
			}
		}
		if ($orderby != '') {
			$this->db->order_by ( $orderby );
		}
		$query = $this->db->get ( $this->getTableName() );
		
		return $this->fetchAll ( $query );
	}
	
	/**
	 * Retrieves by a single where clause
	 *
	 * @param $key database
	 *        	key
	 * @param $value Value
	 *        	of where clause
	 * @return array
	 */
	public function getColumnByKeyValue($column, $key, $value, $orderby = '') {
		if ($column) {
			$this->db->select ( $column );
		}
		if (gettype ( $value ) == 'array') {
			$this->db->where_in ( $key, $value );
		} else {
			$this->db->where ( $key, $value );
		}
		if ($orderby != '') {
			$this->db->order_by ( $orderby );
		}
		$query = $this->db->get ( $this->getTableName() );
		return $this->fetchAll ( $query );
	}
	
	/**
	 * Retrieves by an array of where clauses
	 *
	 * @param $key database
	 *        	key
	 * @param $value Value
	 *        	of where clause
	 * @return array
	 */
	public function getColumnByKeyValueArray($column, $where_clauses, $orderby = '') {
		if ($column) {
			$this->db->select ( $column );
		}
		foreach ( $where_clauses as $key => $value ) {
			
			if (gettype ( $value ) == 'array') {
				$this->db->where_in ( $key, $value );
			} else {
				$this->db->where ( $key, $value );
			}
		}
		if ($orderby != '') {
			$this->db->order_by ( $orderby );
		}
		$query = $this->db->get ( $this->getTableName() );
		
		return $this->fetchAll ( $query );
	}
	public function getOneByKeyValueArray($where_clauses, $orderby = '') {
		foreach ( $where_clauses as $key => $value ) {
			
			if (gettype ( $value ) == 'array') {
				$this->db->where_in ( $key, $value );
			} else {
				$this->db->where ( $key, $value );
			}
		}
		if ($orderby != '') {
			$this->db->order_by ( $orderby );
		}
		$query = $this->db->get ( $this->getTableName() );
		$result = $this->fetchAll ( $query );
		if (count ( $result ) > 0) {
			return $result [0];
		}
		return FALSE;
	}
	
	/**
	 * If the result set is expected to have one row in the result set, then it returns the object and not an array
	 *
	 * @param $key Database
	 *        	key
	 * @param $value Value
	 *        	of where clause
	 * @return bool
	 */
	public function getOneByKeyValue($key, $value, $orderby = '') {
		$this->db->where ( $key, $value );
		if ($orderby != '') {
			$this->db->order_by ( $orderby );
		}
		$query = $this->db->get ( $this->getTableName() );
		$result = $this->fetchAll ( $query );
		if (count ( $result ) > 0) {
			return $result [0];
		}
		
		return FALSE;
	}
	
	/**
	 * Returns the single row as object by common primary key
	 *
	 * @param $id Value
	 *        	of id field
	 * @return mixed
	 */
	public function getById($id) {
		$row = $this->getOneByKeyValue ( 'id', $id );
		if ($row) {
			$class = new ReflectionClass ( get_called_class () );
			return $class->newInstance ( $row );
		}
		return NULL;
	}
	
	/**
	 * Wrapper around CI insert function without providing table name, and it returns the insert_id()
	 * This also filters out array elements that do not exist in the database table
	 *
	 * @param $data Array
	 *        	of data where array key is the db field and the array value is the db value
	 * @return mixed
	 */
	public function insert($data) {
		
		if (array_key_exists ( 'id', $data )) {
			unset ( $data ['id'] );
		}
		$db_columns = $this->db->list_fields ( $this->getTableName() );
		
		if (array_key_exists ( 'updatedDatetime', $data )) {
			$data ['updatedDatetime'] = $data ['updatedDatetime']; // date("Y-m-d H:i:s", time());
		}
		else if (in_array ( 'updatedDatetime', $db_columns )) {
			$data ['updatedDatetime'] = getCurrentDateTime (); // date("Y-m-d H:i:s", time());
		}
		if (array_key_exists ( 'createdDatetime', $data )) {
			$data ['createdDatetime'] = $data ['createdDatetime']; // date("Y-m-d H:i:s", time());
		}
		else if (in_array ( 'createdDatetime', $db_columns )) {
			$data ['createdDatetime'] = getCurrentDateTime (); // date("Y-m-d H:i:s", time());
		}
		if(array_key_exists( 'createdBy', $data ))
		{
			$data ['createdBy']=$data ['createdBy'];
		}
		else if (in_array ( 'createdBy', $db_columns )) {
			
			if (array_key_exists ( 'createdBy', $data )) {
				if ($data ['createdBy'] < 0 || $data ['createdBy'] == '') {
					$data ['createdBy'] = 0;
					if ($this->session->userdata ( 'user_id' )) {
						$data ['createdBy'] = $this->session->userdata ( 'user_id' );
					}
				}
			} else {
				$data ['createdBy'] = 0;
				if ($this->session->userdata ( 'user_id' )) {
					$data ['createdBy'] = $this->session->userdata ( 'user_id' );
				}
			}
		}
		//debug($data);echo"======";
		if(array_key_exists('updatedBy', $data )) {
			
			$data ['updatedBy']=$data ['updatedBy'];	
		}
		else if (in_array ( 'updatedBy', $db_columns )) {
			
			if (array_key_exists ( 'updatedBy', $data )) {
				if ($data ['updatedBy'] < 0 || $data ['updatedBy'] == '') {
					$data ['updatedBy'] = 0;
					if ($this->session->userdata ( 'user_id' )) {
						$data ['updatedBy'] = $this->session->userdata ( 'user_id' );
					}
				}
			} else {
				$data ['updatedBy'] = 0;
				if ($this->session->userdata ( 'user_id' )) {
					$data ['updatedBy'] = $this->session->userdata ( 'user_id' );
				}
			}
		}
		
		$insert = array_intersect_key ( $data, array_flip ( $db_columns ) );
		//debug_exit($data);
		log_message ( 'debug', print_r ( $insert, TRUE ) );
		
		 if ($this->getTableName()=='passenger')
		{
			$url="http://35.167.147.253/Passenger/curlPassengerUpdate";
			$result=$this->curlFunction($url,$data);
		}
		if ($this->getTableName()=='authkeydetails' && $data['userType']=='P')
		{
			$url="http://35.167.147.253/Passenger/curlAuthKeyDetailsUpdate";
			$result=$this->curlFunction($url,$data);
		}
		 
		if (count ( $insert ) > 0) {
			
			$this->db->insert ( $this->getTableName(), $insert );
			return $this->db->insert_id ();
		}
		
		return FALSE;
	}
	
	/**
	 * Wrapper around CI update function without providing table name
	 *
	 * @param
	 *        	$data
	 * @param
	 *        	$where
	 * @return mixed
	 */
	public function update($data, $where) {
		$db_columns = $this->db->list_fields ( $this->getTableName() );
		if (array_key_exists ( 'updatedDatetime', $data )) {
			$data ['updatedDatetime'] =$data ['updatedDatetime'];
		}
		else if (in_array ( 'updatedDatetime', $db_columns )) {
			$data ['updatedDatetime'] = getCurrentDateTime (); // date("Y-m-d H:i:s", time());
		}
		if(array_key_exists ( 'updatedBy', $data )) {
			$data ['updatedBy'] =$data ['updatedBy'];
		}
		else if (in_array ( 'updatedBy', $db_columns )) {
			if (array_key_exists ( 'updatedBy', $data )) {
				if ($data ['updatedBy'] < 0 || $data ['updatedBy'] == '') {
					$data ['updatedBy'] = 0;
					if ($this->session->userdata ( 'user_id' )) {
						$data ['updatedBy'] = $this->session->userdata ( 'user_id' );
					}
				}
			} else {
				$data ['updatedBy'] = 0;
				if ($this->session->userdata ( 'user_id' )) {
					$data ['updatedBy'] = $this->session->userdata ( 'user_id' );
				}
			}
		}
		
		if (array_key_exists ( 'id', $data )) {
			unset ( $data ['id'] );
		}
		
		$update = array_intersect_key ( $data, array_flip ( $db_columns ) );
		log_message ( 'debug', print_r ( $update, TRUE ) );
		
		if ($this->getTableName()=='passenger')
		{
			if (array_key_exists('firstName', $data) || array_key_exists('lastName', $data) || array_key_exists('mobile', $data) || array_key_exists('email', $data) || array_key_exists('password', $data) || array_key_exists('otp', $data) || array_key_exists('profileImage', $data) || array_key_exists('gcmId', $data) || array_key_exists('walletAmount', $data) || array_key_exists('loginStatus', $data) || array_key_exists('lastLoggedIn', $data)){
			$url="http://35.167.147.253/Passenger/curlPassengerUpdate";
			$result=$this->curlFunction($url,$data,$where);
			}
		}
		
		if ($this->getTableName()=='authkeydetails')
		{
			//debug($where);debug($data);
			if (array_key_exists('userType', $data)  && $data['userType']=='P')
			{
			$url="http://35.167.147.253/Passenger/curlAuthKeyDetailsUpdate";
			$result=$this->curlFunction($url,$data,$where);
			}
			else if (array_key_exists('userType', $where) && $where['userType']=='P' && $where['userId'])
			{
				//debug_exit($where['userType']);
				$url="http://35.167.147.253/Passenger/curlAuthKeyDetailsUpdate";
				$result=$this->curlFunction($url,$data,$where);
			}
		} 
		
		return $this->db->update ( $this->getTableName(), $update, $where );
	}
	/**
	 * Wrapper around CI batch insert function
	 *
	 * @param
	 *        	$data
	 * @return mixed
	 */
	public function batchInsert($data, $clone_flag = false) {
		$db_columns = $this->db->list_fields ( $this->getTableName() );
		if ($clone_flag == false) {
			foreach ( $data as $key => $val ) {
				if (array_key_exists ( 'updatedDatetime', $data ))
				{
					$data [$key] ['updatedDatetime']=$data [$key] [$val];
				}
				else if (in_array ( 'updatedDatetime', $db_columns )) {
					$data [$key] ['updatedDatetime'] = getCurrentDateTime (); // date ( "Y-m-d H:i:s", time () );
				}
				if (array_key_exists ( 'createdDatetime', $data ))
				{
					$data [$key] ['createdDatetime']=$data [$key] [$val];
				}
				else if (in_array ( 'createdDatetime', $db_columns )) {
					$data [$key] ['createdDatetime'] = getCurrentDateTime (); // date ( "Y-m-d H:i:s", time () );
				}
				
				if (array_key_exists ( 'createdBy', $data )) {
					$data [$key] ['createdBy'] = $data [$key] [$val];
				}
				else if (in_array ( 'createdBy', $db_columns )) {
					if (array_key_exists ( 'createdBy', $data )) {
						if ($data ['createdBy'] < 0 || $data ['createdBy'] == '') {
							$data ['createdBy'] = 0;
							if ($this->session->userdata ( 'user_id' )) {
								$data ['createdBy'] = $this->session->userdata ( 'user_id' );
							}
						}
					} else {
						$data ['createdBy'] = 0;
						if ($this->session->userdata ( 'user_id' )) {
							$data ['createdBy'] = $this->session->userdata ( 'user_id' );
						}
					}
				}
				if (array_key_exists ( 'updatedBy', $data )) {
					$data [$key] ['updatedBy'] = $data [$key] [$val];
				}
				
				else if (in_array ( 'updatedBy', $db_columns )) {
				if (array_key_exists ( 'updatedBy', $data )) {
					if ($data ['updatedBy'] < 0 || $data ['updatedBy'] == '') {
							$data ['updatedBy'] = 0;
							if ($this->session->userdata ( 'user_id' )) {
								$data ['updatedBy'] = $this->session->userdata ( 'user_id' );
							}
						}
					} else {
						$data ['updatedBy'] = 0;
						if ($this->session->userdata ( 'user_id' )) {
							$data ['updatedBy'] = $this->session->userdata ( 'user_id' );
						}
					}
				}
			}
		}
		return $this->db->insert_batch ( $this->getTableName(), $data );
	}
	
	/**
	 * Wrapper around CI batch update function
	 *
	 * @param
	 *        	$data
	 * @param string $key        	
	 * @return mixed
	 */
	public function batchUpdate($data, $key = 'id') {
		foreach ( $data as $row ) {
			$row ['updatedDatetime'] = date ( "Y-m-d H:i:s", time () );
			$row ['updatedBy'] = 0;
			if ($this->session->userdata ( 'user_id' )) {
				$row ['updatedBy'] = $this->session->userdata ( 'user_id' );
			}
		}
		return $this->db->update_batch ( $this->getTableName(), $data, $key );
	}
	public function delete($where) {
		if ($where) {
			return $this->db->delete ( $this->getTableName(), $where );
		}
	}
	public function searchBy($columns = NULL, $key, $value) {
		if ($columns != NULL) {
			$this->db->select ( $columns );
		}
		$this->db->like ( $key, $value, 'after' );
		$query = $this->db->get ( $this->getTableName() );
		return $this->fetchAll ( $query );
	}
	public function getSelectDropdownOptions($where_clauses = array(), $orderby = NULL, $option = NULL) {
		$key = $this->getKeyName ();
		$val = $this->getValueName ();
		$this->db->select ( $key . ', ' . $val );
		
		if (count ( $where_clauses ) > 0) {
			foreach ( $where_clauses as $k => $v ) {
				if (gettype ( $v ) == 'array') {
					$this->db->where_in ( $k, $v );
				} else {
					$this->db->where ( $k, $v );
				}
			}
		}
		if ($orderby) {
			$this->db->order_by ( $orderby );
		}
		$query = $this->db->get ( $this->getTableName() );
		// to set first option text/default
		if ($option != NULL) {
			$options = array (
					'' => '--' . $option . '--' 
			);
		} else {
			$options = array (
					'' => NULL_SELECT_OPTION_TEXT 
			);
		}
		foreach ( $query->result () as $row ) {
			$options [$row->$key] = $row->$val;
		}
		
		return $options;
	}
	
	/*
	 * Method used to get the Locked date
	 */
	public function getLockedDate($id = 0) {
		$lockedDate = null;
		if ($id <= 0) {
			// Then getting the id using get() method
			$id = $this->get ( 'id' );
		}
		
		if ($id > 0) {
			$ci = & get_instance ();
			$ci->load->model ( 'Locks_Model' );
			$result = $ci->Locks_Model->getOneByKeyValueArray ( array (
					'tableName' => strtoupper ( $this->getTableName() ),
					'primaryKey' => $id 
			) );
			if (! empty ( $result )) {
				$lockedDate = $result->updatedDatetime;
			}
		}
		return $lockedDate;
	}
	/**
	 *
	 * @param number $id        	
	 * @return string
	 */
	public function getLockedUser($id = 0) {
		$lockedUser = null;
		// Checking if $id is passed as methd param or not
		if ($id <= 0) {
			// Then getting the id using get() method
			$id = $this->get ( 'id' );
		}
		
		// checing $id is valid or not
		if ($id > 0) {
			$ci = & get_instance ();
			$ci->load->model ( 'Locks_Model' );
			$result = $ci->Locks_Model->getOneByKeyValueArray ( array (
					'tableName' => strtoupper ( $this->getTableName() ),
					'primaryKey' => $id 
			) );
			if (! empty ( $result )) {
				$ci = & get_instance ();
				$ci->load->model ( 'User_Login_Details_Model' );
				$user_details = $ci->User_Login_Details_Model->getById ( $result->updatedBy );
				$lockedUser = $user_details->firstName . ' ' . $user_details->lastName;
			}
		}
		return $lockedUser;
	}
	public function isLocked($id = 0) {
		$results=NULL;
		// Checking if $id is passed as methd param or not
		if ($id <= 0) {
			// Then getting the id using get() method
			$id = $this->get ( 'id' );
		}
		$is_data_locked=0;
		if ($id > 0) {
			// if the locked user not same as logged in user
			$ci = & get_instance ();
			$ci->load->model ( 'Locks_Model' );
			$results = $ci->Locks_Model->getOneByKeyValueArray ( array (
					'tableName' => strtoupper ( $this->getTableName() ),
					'primaryKey' => $id,
					'updatedBy !=' => $this->session->userdata ( 'user_id' ) 
			) );
			//debug($this->session->userdata ( 'user_id' ));
			//debug($results);
			if ($results) {
				// @todo check lock period if it exists 15 minute unlock & allow to other user to lock
				$locked_period = 0;
				$interval=abs ( strtotime ( $results->updatedDatetime ) - getCurrentUnixDateTime () );
				$locked_period = round ( $interval / 60 );
				//debug_exit($locked_period);
				if ($results->updatedBy != $this->session->userdata ( 'user_id' ) && $locked_period >= DATA_LOCK_PERIOD) {
					if ($ci->Locks_Model->delete ( array (
							'tableName' => strtoupper ( $this->getTableName() ),
							'primaryKey' => $id 
					) )) {
						$is_data_locked = 0;
					}
				} else {
					
					$is_data_locked=1;
				}
				
			}
		}
		
		return $is_data_locked;
	}
	
	/*
	 * parameter id specially used if table has multiple rows and cannot be identified by a single id (primary key)
	 * in this case pass Forigen key to lock the data with current table name.
	 * Example. PLC Status or Realease Team members has PK but user edit in a group, there is no way to lock these tables with single id
	 * in this scenario Lock PLC STATUS table with status_report.id and release_team_members with release.id but table should be of child type.
	 */
	public function lock($id = 0) {
		
		// Checking if $id is passed as methd param or not
		if ($id <= 0) {
			// Then getting the id using get() method
			$id = $this->get ( 'id' );
		}
		
		if ($id <= 0) {
			// if id is 0 then nothing to lock. simply return success
			return 1;
		}
		
		$lockedSuccessfully = 0;
		$insert_id = NULL;
		// loading the locks_model
		$ci = & get_instance ();
		$ci->load->model ( 'Locks_Model' );
		$data ['primaryKey'] = $id;
		$data ['tableName'] = strtoupper ( $this->getTableName() );
		$lockedUser = $this->getLockedUser ( $id );
		
		if ($lockedUser == null) {
			// no lock record found for user then check is any one else locked
			if (! $this->isLocked ( $id )) {
				$insert_id = $ci->Locks_Model->insert ( $data );
				if ($insert_id) {
					// locked successfully
					$lockedSuccessfully = 1;
				}
			}
		} else {
			// locked by user then just updated the time
			$ci->Locks_Model->update ( $data, array (
					'primaryKey' => $id,
					'tableName' => strtoupper ( $this->getTableName() ) 
			) );
			$lockedSuccessfully = 1;
		}
		
		return $lockedSuccessfully;
	}
	public function unlock($id = 0) {
		
		// Checking if $id is passed as methd param or not
		if ($id <= 0) {
			// Then getting the id using get() method
			$id = $this->get ( 'id' );
		}
		
		if ($id <= 0) {
			return 1;
		}
		
		$unlockedSuccessfully = 0;
		// loading the locks_model
		$ci = & get_instance ();
		$ci->load->model ( 'Locks_Model' );
		$results = $ci->Locks_Model->getOneByKeyValueArray ( array (
				'tableName' => strtoupper ( $this->getTableName() ),
				'primaryKey' => $id,
				'updatedBy' => $this->session->userdata ( 'user_id' ) 
		) );
		// unlock only if it is locked by current user
		if ($results->updatedBy == $this->session->userdata ( 'user_id' )) {
			if ($ci->Locks_Model->delete ( array (
					'tableName' => strtoupper ( $this->getTableName() ),
					'primaryKey' => $id 
			) )) {
				$unlockedSuccessfully = 1;
			}
		}
		
		return $unlockedSuccessfully;
	}
	private function _getDataTableQuery($sql_query = NULL) {
		$query = '';
		$group_by_clause = '';
		// Set Actual query
		if ($sql_query) {
			
			$query = $sql_query;
		}
		
		if ($query) {
			$i = 0;
			// loop each column
			foreach ( $this->_column_search as $item ) {
				// if datatable send POST for search
				if ($_POST ['search'] ['value']) {
					// if query contains group by first remove group by clause add once search where clause completed in query
					if (stripos ( $query, 'group by' )) {
						$group_by_clause = substr ( $query, stripos ( $query, 'group by' ) );
						$query = str_replace ( $group_by_clause, '', $query );
					}
					// first iteration loop
					if ($i === 0) {
						// Check If query already contains where clause or not if exists Concate "AND" to query
						if (stripos ( $query, 'where' ) !== FALSE) {
							$query .= ' AND ';
						} else {
							$query .= ' WHERE ';
						}
						// open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
						$query .= '(' . $item . ' LIKE "%' . $_POST ['search'] ['value'] . '%"';
					} else {
						$query .= ' OR ' . $item . ' LIKE "%' . $_POST ['search'] ['value'] . '%"';
					}
					
					// last iteration loop
					if (count ( $this->_column_search ) - 1 == $i) {
						// close bracket
						$query .= ')';
					}
					// adding group by clause at end of search where clause completed in query
					if ($group_by_clause) {
						$query .= ' ' . $group_by_clause;
					}
				}
				$i ++;
			}
			// here order processing
			if (isset ( $_POST ['order'] )) {
				$query .= ' order by ' . $this->_column_order [$_POST ['order'] ['0'] ['column']] . ' ' . $_POST ['order'] ['0'] ['dir'];
			} else if (isset ( $this->_order )) {
				
				$query .= ' order by ';
				$i = 0;
				foreach ( $this->_order as $key => $val ) {
					if ((count ( $this->_order ) - 1) == $i) {
						$query .= $key . ' ' . $val;
					} else if ((count ( $this->_order ) - 1) > $i) {
						$query .= ' ' . $key . ' ' . $val . ', ';
					}
					
					$i ++;
				}
			}
		}
		return $query;
	}
	public function getDataTable($sql_query = NULL) {
		$query = $this->_getDataTableQuery ( $sql_query );
		if ($query) {
			if ($_POST ['length'] != - 1) {
				$query .= ' LIMIT ' . $_POST ['start'] . ',' . $_POST ['length'];
			}
		}
		$result = $this->db->query ( $query );
		return $this->fetchAll ( $result );
	}
	public function getDataTableFilterCount($sql_query = NULL) {
		$query = $this->_getDataTableQuery ( $sql_query );
		$result = $this->db->query ( $query );
		return count ( $this->fetchAll ( $result ) );
	}
	public function getDataTableAllCount($sql_query = NULL) {
		$query = $this->_getDataTableQuery ( $sql_query );
		$result = $this->db->query ( $query );
		return count ( $this->fetchAll ( $result ) );
	}
	public function callStoredProcedure($statement, $data = '') {
		// $this->db->reconnect();
		if ($data == '') {
			$data = array ();
		}
		
		$query = $this->db->query ( $statement, $data );
		return $this->fetchAll ( $query );
	}
	
	private  function curlFunction($url,$data,$where=array())
	{
		
		$post_data=array();
		$post_data['data']=$data;
		$post_data['where']=$where;
		$post_data = json_encode ( $post_data );
		$curl = curl_init ();
		curl_setopt ( $curl, CURLOPT_URL, $url );
		curl_setopt ( $curl, CURLOPT_HTTPHEADER , array('Content-Type:application/json'));
		curl_setopt ( $curl, CURLOPT_POST, 1 );
		curl_setopt ( $curl, CURLOPT_POSTFIELDS, $post_data );
		$response = curl_exec ( $curl );
		
		if ($response === FALSE) {
			die("Curl failed: " . curL_error($curl));
		}
		$reponseInfo = array();
		$reponseInfo['info'] = curl_getinfo($curl);
		$reponseInfo['error'] = curl_error($curl);
		$result['reponseInfo'] = $reponseInfo;
		
		curl_close ( $curl );
		//debug_exit($result['reponseInfo']);
		return $response;
	}
}