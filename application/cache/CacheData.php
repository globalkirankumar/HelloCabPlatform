<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CacheData  {

	public function __construct($config = array())
	{
		$CI = &get_instance();
		//load the cache config
		$CI->config->load('cache');
		//load the cache driver
		 $CI->load->driver('cache',$CI->config->item('cache_config'));
	}

	/**
	 * Fetching the data from the cache
	 * @param string $key : key of the data to be fetched
	 * @return mixed :: returns data if it is available in the cache else it returns false
	 */
	function get($key)
	{
		$CI =&get_instance();
		
		if (!$cached_data = $CI->cache->get($key))
		{
			
			return false;
		}
		return $cached_data;
	}
	
	/**
	 * 
	 * Stores the data in APC cache
	 * @param string $key : key to map the data
	 * @param mixed:: $data : data to be stored
	 * @return bool :: True if successfully saved else false
	 */
	function storeData($key,$data)
	{
		$CI =&get_instance();
		return $CI->cache->save($key,$data);
	}
	
	/**
	 * 
	 * Clean the entire cache 
	 * @return bool:: TRUE on successfull clearing of cache else false
	 */
	function clearAllCache()
	{
		//clean the entire cache
		return $this->cache->clean();
		
	}
}
?>