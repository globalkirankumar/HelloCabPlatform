<?php
$view_mode = $mode;
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
        <?php
								$form_attr = array (
										'name' => 'edit_notification_form',
										'id' => 'edit_notification_form',
										'method' => 'POST' 
								);
								echo form_open_multipart ( base_url ( '' ), $form_attr );
								// passenger id by default is -1
								echo form_input ( array (
										'type' => 'hidden',
										'id' => 'notification_id',
										'name' => 'id',
										'value' => ($notification_model->get ( 'id' )) ? $notification_model->get ( 'id' ) : - 1 
								) );
								
								?>
        <div class="page-bar ">
			<ul class="page-breadcrumb ">
				<li><a href="<?php echo base_url('Dashboard'); ?>">Home</a> <i
					class="fa fa-circle"></i></li>
				<li><a href="<?php echo base_url('Notification/getNotificationList'); ?>">Notification
						List</a> <i class="fa fa-circle"></i></li>
				<li><a href="<?php echo base_url(uri_string()); ?>"><?php echo ($notification_model->get ( 'id' )) ?(($view_mode == VIEW_MODE)?'View':'Edit'): 'Create'; ?> Notification</a></li>

			</ul>

		</div>
		<!---- Page Bar ends ---->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
				<!--                                <div class="portlet box green">
                                                <div class="portlet-title">
                                                        <div class="caption">
                                                            <i class="fa fa-comments"></i>Driver List </div>
                                                       
                                                    </div>
                                                 
                                                    
                                                </div>-->
				<div class="portlet-body">
					<div class="form-body gp-form">
						<div class="card-header bg-white">
							<h1><?php echo ($notification_model->get('id')) ? (($view_mode == VIEW_MODE) ? 'View' : 'Edit') : 'Create'; ?> Notification</h1>
						</div>

						<div class="row">
							<div class="col-lg-3 input_field_sections nowarp">
                                <?php
																																echo form_label ( 'Title:', 'title', array (
																																		'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																																) );
																																
																																// validation for passenger first name
																																if ($view_mode == EDIT_MODE) {
																																	echo form_input ( array (
																																			'id' => 'title',
																																			'name' => 'title',
																																			'class' => 'form-control',
																																			'required' => 'required',
																																			//'pattern' => '[a-zA-Z\s]{3,15}',
																																			'minlength'=>'3',
																																			'maxlength'=>'15',
																																			'placeholder' => 'Title',
																																			'value' => ($notification_model->get ( 'title' )) ? $notification_model->get ( 'title' ) : '' 
																																	) );
																																} else {
																																	echo text ( $notification_model->get ( 'title' ) );
																																}
																																?>

                            </div>
                            <div class="col-lg-3 input_field_sections nowarp">
                                <?php
																																echo form_label ( 'Description:', 'description', array (
																																		'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																																) );
																																
																																if ($view_mode == EDIT_MODE) {
																																	echo form_textarea ( array (
																																			'id' => 'description',
																																			'name' => 'description',
																																			'rows' => '2',
																																			'class' => 'form-control',
																																			'required' => 'required',
																																			'value' => ($notification_model->get ( 'description' )) ? $notification_model->get ( 'description' ) : '' 
																																	) );
																																} else {
																																	echo form_textarea ( array (
																																			'id' => 'description',
																																			'name' => 'description',
																																			'class' => 'form-control',
																																			'rows' => '2',
																																			'readonly' => 'readonly',
																																			'value' => ($notification_model->get ( 'description' )) ? $notification_model->get ( 'description' ) : '' 
																																	) );
																																}
																																?>

                            </div>
							<div class="col-lg-3 input_field_sections nowarp">
                                
                                <?php
																																echo form_label ( 'Notification To:', 'notificationTo', array (
																																		'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																																) );
																																
																																if ($view_mode == EDIT_MODE) {
																																	
																																		echo '<div class="mt-radio-inline">';
																																		
																																			echo '<label class="mt-radio" style="padding-right:10px;">';
																																			// $is_checked=($notification_model->get('gender')==$list->description) ? TRUE : FALSE;
																																			echo form_radio ( array (
																																					'id' => 'notificationTo-P',
																																					'name' => 'notificationTo',
																																					'value' => 'P',
																																					'class' => 'flat',
																																					'required' => 'required',
																																					'checked' => ($notification_model->get ( 'notificationTo' ) == User_Type_Enum::PASSENGER) ? TRUE : FALSE 
																																			) );
																																			echo '<span></span>Passenger</label>';
																																			echo '<label class="mt-radio" style="padding-right:10px;">';
																																			// $is_checked=($notification_model->get('gender')==$list->description) ? TRUE : FALSE;
																																			echo form_radio ( array (
																																					'id' => 'notificationTo-D',
																																					'name' => 'notificationTo',
																																					'value' => 'D',
																																					'class' => 'flat',
																																					'required' => 'required',
																																					'checked' => ($notification_model->get ( 'notificationTo' ) == User_Type_Enum::DRIVER) ? TRUE : FALSE
																																			) );
																																			echo '<span></span>Driver</label>';
																																		
																																		echo '</div>';
																																	
																																} else {
																																	
																																	echo text ( ($notification_model->get ( 'notificationTo' )==User_Type_Enum::DRIVER)?'Driver':'Passenger');
																																}
																																?>

                            </div>
                            <div class="col-lg-3 input_field_sections nowarp">
                                
                                <?php
																																echo form_label ( 'Notification Type:', 'notificationType', array (
																																		'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																																) );
																																
																																if ($view_mode == EDIT_MODE) {
																																	
																																		echo '<div class="mt-radio-inline">';
																																		
																																			echo '<label class="mt-radio" style="padding-right:10px;">';
																																			// $is_checked=($notification_model->get('gender')==$list->description) ? TRUE : FALSE;
																																			echo form_radio ( array (
																																					'id' => 'notificationType-P',
																																					'name' => 'notificationType',
																																					'value' => 'P',
																																					'class' => 'flat',
																																					'required' => 'required',
																																					'checked' => ($notification_model->get ( 'notificationType' ) == 'P') ? TRUE : FALSE 
																																			) );
																																			echo '<span></span>Google Push</label>';
																																			echo '<label class="mt-radio" style="padding-right:10px;">';
																																			// $is_checked=($notification_model->get('gender')==$list->description) ? TRUE : FALSE;
																																			echo form_radio ( array (
																																					'id' => 'notificationType-S',
																																					'name' => 'notificationType',
																																					'value' => 'S',
																																					'class' => 'flat',
																																					'required' => 'required',
																																					'checked' => ($notification_model->get ( 'notificationType' ) == 'S') ? TRUE : FALSE
																																			) );
																																			echo '<span></span>SMS</label>';
																																		
																																		echo '</div>';
																																	
																																} else {
																																	
																																	echo text ( ($notification_model->get ( 'notificationType' )=='P')?'Google Push':'SMS');
																																}
																																?>

                            </div>
                           
							
						</div>
						<div class="row">
						 <div class="col-lg-2 input_field_sections nowarp">
                                <?php
																																echo form_label ( 'Start Datetime:', 'startDatetime', array (
																																		'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																																) );
																																
																																if ($view_mode == EDIT_MODE) {
																																	echo form_input ( array (
																																			'id' => 'startDatetime',
																																			'name' => 'startDatetime',
																																			'class' => 'form-control',
																																			'required' => 'required',
																																			'placeholder' => 'Start Datetime',
																																			'value' => ($notification_model->get ( 'startDatetime' )) ? $notification_model->get ( 'startDatetime' ) : '' 
																																	) );
																																} else {
																																	echo text ( $notification_model->get ( 'startDatetime' ) );
																																}
																																?>

                            </div>
								<div class="col-lg-2 input_field_sections nowarp">
                                <?php
																																echo form_label ( 'End Datetime:', 'endDatetime', array (
																																		'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																																) );
																																
																																if ($view_mode == EDIT_MODE) {
																																	echo form_input ( array (
																																			'id' => 'endDatetime',
																																			'name' => 'endDatetime',
																																			'class' => 'form-control',
																																			'required' => 'required',
																																			'placeholder' => 'End Datetime',
																																			'value' => ($notification_model->get ( 'endDatetime' )) ? $notification_model->get ( 'endDatetime' ) : '' 
																																	) );
																																} else {
																																	echo text ( $notification_model->get ( 'endDatetime' ) );
																																}
																																?>

                            </div>
						</div>
							<div class="form-actions" style="margin-top: 22px;">
								<div class="row">
									<div class="col-md-12">
                                    <?php if ($view_mode == EDIT_MODE) { ?>
                                        <div class="gp-cen">

											<button type="button" id="cancel-notification-btn"
												class="btn grey-salsa btn-outline">Cancel</button>
											<button type="button" id="send-notification-btn"
												class="btn gpblue">Send</button>
											<button type="button" id="save-notification-btn"
												class="btn gpblue">Save &#38; Send</button>
											
										</div>
                                <?php } else { ?>
                                                           <div
											class="gp-cen">
											<button type="button" id="cancel-notification-btn"
												class="btn btn-danger">Back</button>
										</div>
                                    <?php } ?>
                                   
                                                                </div>
								</div>
							</div>
                                <?php
																																echo form_close ();
																																?>

                    </div>
					</div>
				</div>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>




