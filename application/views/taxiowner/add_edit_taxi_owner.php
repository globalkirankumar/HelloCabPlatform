<?php
$view_mode = $mode;
?>
<!-- BEGIN CONTENT -->

<!-- BEGIN CONTENT BODY -->
<div class="page-content">
<?php
$form_attr = array (
		'name' => 'edit_taxiowner_form',
		'id' => 'edit_taxiowner_form',
		'method' => 'POST' 
);
echo form_open_multipart ( base_url ( '' ), $form_attr );
// driver id by default is -1
echo form_input ( array (
		'type' => 'hidden',
		'id' => 'taxi_owner_id',
		'name' => 'id',
		'value' => ($taxiowner_model->get ( 'id' )) ? $taxiowner_model->get ( 'id' ) : - 1 
) );

?>
        <div class="page-bar ">
		<ul class="page-breadcrumb ">
			<li><a href="<?php echo base_url('Dashboard'); ?>">Home</a> <i
				class="fa fa-circle"></i></li>
			<li><a href="<?php echo base_url('TaxiOwner/getTaxiOwnerList'); ?>">Taxi Owner
					List</a> <i class="fa fa-circle"></i></li>
			<li><a href="<?php echo base_url(uri_string()); ?>"><?php echo ($taxiowner_model->get ( 'id' )) ?(($view_mode == VIEW_MODE)?'View':'Edit'): 'Create'; ?> Taxi Owner</a></li>

		</ul>

	</div>
	<!---- Page Bar ends ---->
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->

			<div class="portlet-body">
				<div class="form-body gp-form">
					<div class="card-header bg-white">
						<h1>
<?php echo ($taxiowner_model->get ( 'id' )) ?(($view_mode == VIEW_MODE)?'View':'Edit'): 'Create'; ?> Taxi Owner</h1>
					</div>


					<div class="row">
						<div class="col-lg-3 input_field_sections nowarp">
								<?php
								
								echo form_label ( 'First Name:', 'firstName', array (
										'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
								) );
								
								if ($view_mode == EDIT_MODE) {
									echo form_input ( array (
											'id' => 'firstName',
											'name' => 'firstName',
											'class' => 'form-control',
											'required' => 'required',
											'pattern' => '[a-zA-Z\s]{3,15}',
											'placeholder' => 'First Name',
											'value' => ($taxiowner_model->get ( 'firstName' )) ? $taxiowner_model->get ( 'firstName' ) : '' 
									) );
								} else {
									echo text ( $taxiowner_model->get ( 'firstName' ) );
								}
								?>
								
							</div>
						<div class="col-lg-3 input_field_sections nowarp">
								
									<?php
									echo form_label ( 'Last Name:', 'lastName', array (
											'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
									) );
									
									if ($view_mode == EDIT_MODE) {
										echo form_input ( array (
												'id' => 'lastName',
												'name' => 'lastName',
												'class' => 'form-control',
												//'required' => 'required',
												//'pattern' => '[a-zA-Z\s]{1,15}',
												'placeholder' => 'Last Name',
												'value' => ($taxiowner_model->get ( 'lastName' )) ? $taxiowner_model->get ( 'lastName' ) : '' 
										) );
									} else {
										echo text ( $taxiowner_model->get ( 'lastName' ) );
									}
									?>
								
							</div>
						<div class="col-lg-3 input_field_sections nowarp section-container">
								<div class="section-message"></div>
									<?php
									echo form_label ( 'Email:', 'email', array (
											'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
									) );
									
									if ($view_mode == EDIT_MODE) {
										echo form_input ( array (
												'id' => 'email',
												'name' => 'email',
												'class' => 'form-control',
												//'required' => 'required',
												//'pattern' => '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$',
												'placeholder' => 'Email',
												'value' => ($taxiowner_model->get ( 'email' )) ? $taxiowner_model->get ( 'email' ) : '' 
										) );
									} else {
										echo text ( $taxiowner_model->get ( 'email' ) );
									}
									?>
								
							</div>
						<div class="col-lg-3 input_field_sections nowarp section-container">
								<div class="section-message"></div>
									<?php
									echo form_label ( 'Mobile:', 'mobile', array (
											'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
									) );
									
									if ($view_mode == EDIT_MODE) {
										echo form_input ( array (
												'id' => 'mobile',
												'name' => 'mobile',
												'maxlength'=>'11',
												'class' => 'form-control',
												'required' => 'required',
												'pattern' => '^[0]?[789]\d{9}$',
												'placeholder' => 'Mobile',
												'value' => ($taxiowner_model->get ( 'mobile' )) ? $taxiowner_model->get ( 'mobile' ) : '' 
										) );
									} else {
										echo text ( $taxiowner_model->get ( 'mobile' ) );
									}
									?>
								
							</div>


						<div class="col-lg-3 input_field_sections nowarp">
								
									<?php
									echo form_label ( 'Address:', 'address', array (
											'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
									) );
									
									if ($view_mode == EDIT_MODE) {
										echo form_textarea ( array (
												'id' => 'address',
												'name' => 'address',
												'class' => 'form-control',
												'rows' => '2',
												'required' => 'required',
												'value' => ($taxiowner_model->get ( 'address' )) ? $taxiowner_model->get ( 'address' ) : '' 
										) );
									} else {
										echo form_textarea ( array (
												'id' => 'address',
												'name' => 'address',
												'class' => 'form-control',
												'rows' => '2',
												'readonly' => 'readonly',
												'value' => ($taxiowner_model->get ( 'address' )) ? $taxiowner_model->get ( 'address' ) : '' 
										) );
									}
									?>
								
							</div>

						<div class="col-lg-3 input_field_sections nowarp">
								
								<?php
								echo form_label ( 'Zone Name:', 'zoneId', array (
										'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
								) );
								// validation for passenger first name
								if ($view_mode == EDIT_MODE) {
									echo form_dropdown ( 'zoneId', $zone_list, $taxiowner_model->get ( 'zoneId' ), array (
											'id' => 'zoneId',
											'class' => 'form-control',
											'required' => 'required' 
									) );
								} else {
									echo text ( $zone_list [$taxiowner_model->get ( 'zoneId' )] );
								}
								
								?>
							</div>
						<div class="col-lg-3 input_field_sections nowarp">
								
								<?php
								echo form_label ( 'Entity Name:', 'entityId', array (
										'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
								) );
								// validation for passenger first name
								if ($view_mode == EDIT_MODE) {
									echo form_dropdown ( 'entityId', $entity_list, $taxiowner_model->get ( 'entityId' ), array (
											'id' => 'entityId',
											'class' => 'form-control',
											'required' => 'required' 
									) );
								} else {
									echo text ( $entity_list [$taxiowner_model->get ( 'entityId' )] );
								}
								
								?>
							</div>
							<div class="col-md-3 input_field_sections nowarp gp-nomar">
                                <?php
																																echo form_label ( 'Is Driver:', 'isDriver', array (
																																		'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																																) );
																																?>
                                <div class="">
								<label class="mt-checkbox">
                                        <?php
                                        $is_checked = $taxiowner_model->get ( 'isDriver' ) ? TRUE : FALSE;
                                        $checked = $taxiowner_model->get ( 'isDriver' ) ? 1 : 0;
                                        if ($view_mode == EDIT_MODE) {
                                        	echo form_checkbox ( array (
                                        			'id' => 'isDriver',
                                        			'name' => 'isDriver',
                                        			'class' => 'flat'
                                        	)
                                        			// 'required' => 'required',
                                        			, $checked, $is_checked );
                                        } else {
                                        	echo form_checkbox ( array (
                                        			'id' => 'isDriver',
                                        			'name' => 'isDriver',
                                        			'class' => 'flat',
                                        			'disabled' => 'disabled'
                                        	)
                                        			// 'required' => 'required',
                                        			, $checked, $is_checked );
                                        }
																																								
																																								?> Is Driver
                                        <span></span>
								</label>
							</div>
						</div>
						
					</div>

					
					<div class="form-actions" style="margin-top: 22px;">
						<div class="row">
							<div class="col-md-12">
                                            <?php if ($view_mode == EDIT_MODE) { ?>
                                                <div class="gp-cen">
									<button type="button" id="cancel-taxiowner-btn"
										class="btn grey-salsa btn-outline">Cancel</button>
									<button type="button" id="save-taxiowner-btn"
										class="btn gpblue">Save</button>

								</div>
                                            <?php } else { ?>
                                            <div class="gp-cen">
                                                <button type="button"
									id="cancel-taxiowner-btn" class="btn btn-danger">Back</button></div>
                                            <?php } ?>
                                        </div>
						</div>
					</div>

				</div>
			</div>
		</div>
		<?php
		echo form_close ();
		?>
	</div>
</div>
