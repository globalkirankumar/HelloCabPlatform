<?php
$view_mode = $mode;
?>
<!-- BEGIN CONTENT -->

<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <?php
    $form_attr = array(
        'name' => 'edit_zone_form',
        'id' => 'edit_zone_form',
        'method' => 'POST'
    );
    echo form_open_multipart(base_url(''), $form_attr);
    // driver id by default is -1
    echo form_input(array(
        'type' => 'hidden',
        'id' => 'zone_id',
        'name' => 'id',
        'value' => ($zone_model->get('id')) ? $zone_model->get('id') : - 1
    ));
    ?>
    <div class="page-bar ">
        <ul class="page-breadcrumb ">
            <li><a href="<?php echo base_url('Dashboard'); ?>">Home</a> <i
                    class="fa fa-circle"></i></li>
            <li><a href="<?php echo base_url('Zone/getZoneList'); ?>">Zone List</a>
                <i class="fa fa-circle"></i></li>
            <li><a href="<?php echo base_url(uri_string()); ?>"><?php echo ($zone_model->get('id')) ? (($view_mode == VIEW_MODE) ? 'View' : 'Edit') : 'Create'; ?> Zone Details</a></li>

        </ul>


    </div>
    <!---- Page Bar ends ---->
    <div class="row">
        <div class="col-md-12">

            <!-- /.inner -->

            <div class="portlet-body">
                <div class="form-body gp-form">
                    <div class="card-header bg-white">
                        <h1><?php
    echo ($zone_model->get('id')) ? (($view_mode == VIEW_MODE) ? 'View' : 'Edit') : 'Create';
    ?> Zone Details</h1>
                    </div>

                    <div class="row">
                        <div class="col-lg-3 input_field_sections">
<?php
echo form_label('Name:', 'name', array(
    'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
));

if ($view_mode == EDIT_MODE) {
    echo form_input(array(
        'id' => 'name',
        'name' => 'name',
        'class' => 'form-control',
        'required' => 'required',
        'placeholder' => 'name',
        'value' => ($zone_model->get('name')) ? $zone_model->get('name') : ''
    ));
} else {
    echo text($zone_model->get('name'));
}
?>

                        </div>
                        <div class="col-lg-3 input_field_sections">
<?php
echo form_label('Polygon Points:', 'polygonPoints', array(
    'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
));

if ($view_mode == EDIT_MODE) {
    echo form_textarea(array(
        'id' => 'polygonPoints',
        'name' => 'polygonPoints',
        'class' => 'form-control',
        'rows' => '3',
        'required' => 'required',
        'value' => ($zone_model->get('polygonPoints')) ? $zone_model->get('polygonPoints') : ''
    ));
} else {
    echo form_textarea(array(
        'id' => 'polygonPoints',
        'name' => 'polygonPoints',
        'class' => 'form-control',
        'rows' => '3',
        'readonly' => 'readonly',
        'value' => ($zone_model->get('polygonPoints')) ? $zone_model->get('polygonPoints') : ''
    ));
}
?>
                        </div>
                        <div class="col-lg-3 input_field_sections">

                        <?php
                        echo form_label('Parent Zone Name:', 'zoneId', array(
                            'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                        ));
                        
                        // validation for passenger first name
                        if ($view_mode == EDIT_MODE) {
                            echo form_dropdown('zoneId', $zone_list, $zone_model->get('zoneId'), array(
                                'id' => 'zoneId',
                                'class' => 'form-control'
                            ));
                        } else {
                            echo text(@$zone_list [$zone_model->get('zoneId')]);
                        }
                        ?>

                        </div>
                        <div class="col-lg-3 input_field_sections">
                            <?php
                            echo form_label('Country Name:', 'countryId', array(
                                'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                            ));
                            
                            // validation for passenger first name
                            if ($view_mode == EDIT_MODE) {
                                echo form_dropdown('countryId', $country_list, $zone_model->get('countryId'), array(
                                    'id' => 'countryId',
                                    'class' => 'form-control',
                                    'required' => 'required'
                                ));
                            } else {
                                echo text(@$country_list [$zone_model->get('countryId')]);
                            }
                            ?>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-lg-3 input_field_sections">
                            <?php
                            echo form_label('Dispatch Type:', 'dispatchType', array(
                                'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                            ));
                            // validation for passenger first name
                            if ($view_mode == EDIT_MODE) {
                                echo form_dropdown('dispatchType', $dispatch_type_list, $zone_model->get('dispatchType'), array(
                                    'id' => 'dispatchType',
                                    'class' => 'form-control',
                                    'required' => 'required'
                                ));
                            } else {
                                echo text(@$dispatch_type_list [$zone_model->get('dispatchType')]);
                            }
                            ?>
                        </div>

                    </div>




                    <div class="form-actions" style="margin-top: 22px;">
                        <div class="row">
                            <div class="col-md-12">
<?php if ($view_mode == EDIT_MODE) { ?>
                                    <div class="gp-cen">

                                        <button type="button" id="cancel-zone-btn"
                                                class="btn grey-salsa btn-outline">Cancel</button>
                                        <button type="button" id="save-zone-btn" class="btn gpblue">Save</button>
                                    </div>
<?php } else { ?>
                                    <div class="gp-cen">
                                        <button type="button" id="cancel-zone-btn"
                                                class="btn btn-danger">Back</button>
                                    </div>
<?php } ?>
                            </div>
                        </div>
                    </div>



                </div>
            </div>
        </div>



<?php
echo form_close();
?> 
    </div>
<!------ Google Polygan Map ---->
 <?php if($view_mode=='view') : ?>
    <?php echo $map['js']; ?>
    <?php echo $map['html']; ?>
  <?php endif ;?>  
</div>
