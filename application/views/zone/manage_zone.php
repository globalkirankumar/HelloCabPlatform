<style>
    .label.label-sm {
    font-size: 13px;
    padding: 2px 5px;
    cursor:pointer;
}
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">

        <div class="page-bar ">
            <ul class="page-breadcrumb ">
                <li>
                    <a href="<?php echo base_url('Dashboard');?>">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="<?php echo base_url('Zone/getZoneList');?>">Zone List</a>

                </li>

            </ul>

        </div>
        <!---- Page Bar ends ---->
        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-list"></i>Zone List </div>
                                       
                                    </div>
                                 
                                    <div class="portlet-body">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="btn-group">
                                                        <a href="<?php echo base_url('Zone/add');?>">
                                                        <button id="sample_editable_1_new" class="btn sbold green1"> Add New
                                                            <i class="fa fa-plus"></i>
                                                        </button>
                                                            </a>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        
                            <table class="table table-striped table-bordered table-hover table-checkable  order-column" id="zone-data-table" style="width:100%">
                                            <thead>
                                                <tr>
                                            <th>Action</th>
                                            <th>Name</th>
                                            <th>Polygon Points</th>
                                            <th>Parent Zone</th>
                                            <th>Country</th>
                                            <th>Dispatch Type</th>
                                            <th>Status</th>
                                           
                                            </thead>
                                            <tbody>
                                                   
                                            </tbody>
                                        </table>
                                            </div>
                                        </div>
                                                
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>



