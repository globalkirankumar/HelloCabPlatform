
<!-- BEGIN CONTENT -->

<!-- BEGIN CONTENT BODY -->
<div class="page-content">
<?php
$form_attr = array (
		'name' => 'edit_change_password_form',
		'id' => 'edit_change_password_form',
		'method' => 'POST' 
);
echo form_open_multipart ( base_url ( '' ), $form_attr );

?>
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN VALIDATION STATES-->


			<div class="portlet-body">
				<div class="form-body gp-form">
					<div class="card-header bg-white gp-cen">
						<h1>Change Password</h1>
					</div>

					
						<div class="row">
							<div class="col-lg-12 input_field_sections nowarp">
							<div class="gp-cent-text">
								<?php
								
								echo form_label ( 'Old Password:', 'oldPassword', array (
										'class' =>'required' 
								) );
								
								
									echo form_password ( array (
											'id' => 'oldPassword',
											'name' => 'oldPassword',
											'class' => 'form-control',
											'required' => 'required',
											
											'placeholder' => 'Old Password',
											'value' => '' 
									) );
								
								?>
								</div>
							</div>
							
							

						</div>
						<div class="row">
							<div class="col-lg-12 input_field_sections nowarp">
								<div class="gp-cent-text">
									<?php
									echo form_label ( 'New Password:', 'newPassword', array (
											'class' => 'required'  
									) );
									
									
										echo form_password ( array (
												'id' => 'newPassword',
												'name' => 'newPassword',
												'class' => 'form-control',
												'placeholder' => 'New Password',
												'value' =>'' 
										) );
									
									?>
								</div>
							</div>
						</div>
						<div class="row">
						
						<div
								class="col-lg-12 gp-ceninput_field_sections nowarp section-container">
								<div class="gp-cent-text">
								
									<?php
									echo form_label ( 'Confirm Password:', 'confirmPassword', array (
											'class' => 'required' 
									) );
									
									
										echo form_password ( array (
												'id' => 'confirmPassword',
												'name' => 'confirmPassword',
												'class' => 'form-control',
												'required' => 'required',
												'placeholder' => 'Confirm Password',
												'value' =>  '' 
										) );
									
									?>
									</div>
								
							</div>
							
							
						</div>
					
			

		<div class="row">
			<div class="col-lg-12 m-t-35">

				<div class="form-actions" style="margin-top: 22px;">
					<div class="row">
						<div class="col-md-12">
                                           
                                                <div class="gp-cen">

								
								<button type="button" id="change-password-btn" class="btn gpblue">Change Password</button>
							</div>
                                            
                                        </div>
					</div>


				</div>
			</div>
			</div>
			</div>
			</div>
		</div>
		<?php
		echo form_close ();
		?>
	</div>
</div>