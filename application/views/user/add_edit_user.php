<?php
$view_mode = $mode;
?>
<!-- BEGIN CONTENT -->

<!-- BEGIN CONTENT BODY -->
<div class="page-content">
<?php
$form_attr = array (
		'name' => 'edit_user_form',
		'id' => 'edit_user_form',
		'method' => 'POST' 
);
echo form_open_multipart ( base_url ( '' ), $form_attr );
// driver id by default is -1
echo form_input ( array (
		'type' => 'hidden',
		'id' => 'user_id',
		'name' => 'id',
		'value' => ($user_model->get ( 'id' )) ? $user_model->get ( 'id' ) : - 1 
) );
echo form_input ( array (
		'type' => 'hidden',
		'id' => 'entityId',
		'name' => 'entityId',
		'value' => DEFAULT_COMPANY_ID 
) );
?>
        <div class="page-bar ">
		<ul class="page-breadcrumb ">
			<li><a href="<?php echo base_url('Dashboard'); ?>">Home</a> <i
				class="fa fa-circle"></i></li>
			<li><a href="<?php echo base_url('User/getUserList'); ?>">User List</a>
				<i class="fa fa-circle"></i></li>
			<li><a href="<?php echo base_url(uri_string()); ?>"><?php echo ($user_model->get('id')) ? (($view_mode == VIEW_MODE) ? 'View' : 'Edit') : 'Create'; ?> User</a></li>

		</ul>

	</div>
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN VALIDATION STATES-->


			<div class="portlet-body">
				<div class="form-body gp-form">
					<div class="card-header bg-white">
						<h1>
<?php echo ($user_model->get ( 'id' )) ?(($view_mode == VIEW_MODE)?'View':'Edit'): 'Create'; ?> User</h1>
					</div>

					
						<div class="row">
							<div class="col-lg-3 input_field_sections nowarp">
								<?php
								
								echo form_label ( 'First Name:', 'firstName', array (
										'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
								) );
								
								if ($view_mode == EDIT_MODE) {
									echo form_input ( array (
											'id' => 'firstName',
											'name' => 'firstName',
											'class' => 'form-control',
											'required' => 'required',
											'pattern' => '[a-zA-Z\s]{3,15}',
											'placeholder' => 'First Name',
											'value' => ($user_model->get ( 'firstName' )) ? $user_model->get ( 'firstName' ) : '' 
									) );
								} else {
									echo text ( $user_model->get ( 'firstName' ) );
								}
								?>
								
							</div>
							<div class="col-lg-3 input_field_sections nowarp">
								
									<?php
									echo form_label ( 'Last Name:', 'lastName', array (
											'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
									) );
									
									if ($view_mode == EDIT_MODE) {
										echo form_input ( array (
												'id' => 'lastName',
												'name' => 'lastName',
												'class' => 'form-control',
												
												// 'required' => 'required',
												// 'pattern' => '[a-zA-Z\s]{1,15}',
												'placeholder' => 'Last Name',
												'value' => ($user_model->get ( 'lastName' )) ? $user_model->get ( 'lastName' ) : '' 
										) );
									} else {
										echo text ( $user_model->get ( 'lastName' ) );
									}
									?>
								
							</div>
							<div
								class="col-lg-3 input_field_sections nowarp section-container">
								<div class="section-message hidden"></div>
									<?php
									echo form_label ( 'Login Email:', 'loginEmail', array (
											'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
									) );
									
									if ($view_mode == EDIT_MODE) {
										echo form_input ( array (
												'id' => 'loginEmail',
												'name' => 'loginEmail',
												'class' => 'form-control',
												'required' => 'required',
												'pattern' => '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$',
												'placeholder' => 'Email Id',
												'value' => ($user_model->get ( 'loginEmail' )) ? $user_model->get ( 'loginEmail' ) : '' 
										) );
									} else {
										echo text ( $user_model->get ( 'loginEmail' ) );
									}
									?>
								
							</div>
							<div class="col-lg-3 input_field_sections nowarp">
									<?php
									echo form_label ( 'Designation:', 'designation', array (
											'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
									) );
									
									if ($view_mode == EDIT_MODE) {
										echo form_input ( array (
												'id' => 'designation',
												'name' => 'designation',
												'class' => 'form-control',
												'required' => 'required',
												'pattern' => '[a-zA-Z\s]{2,30}',
												'placeholder' => 'Designation',
												'value' => ($user_model->get ( 'designation' )) ? $user_model->get ( 'designation' ) : '' 
										) );
									} else {
										echo text ( $user_model->get ( 'designation' ) );
									}
									?>
								
							</div>

						</div>
						<div class="row">
							<div class="col-lg-3 input_field_sections nowarp">
								<div class="fileinput fileinput-new" data-provides="fileinput"
									style="width: 100%">

									<label>Profile Image:</label>
                                            <?php if ($view_mode == EDIT_MODE) { ?>
                                            <div>
										<span class="btn default btn-file" style="width: 53%;"> <span
											class="fileinput-new"> Select image </span> <span
											class="fileinput-exists"> Change </span>
                                                    <?php
																																													echo form_upload ( array (
																																															'id' => 'profileImage',
																																															'name' => 'profileImage[]',
																																															'accept' => 'image/*',
																																															
																																															// 'required' => 'required',
																																															'placeholder' => 'Address Proof Image',
																																															'value' => ($user_model->get ( 'profileImage' )) ? user_content_url ( $user_model->get ( 'id' ) . '/' . $user_model->get ( 'profileImage' ) ) : '' 
																																													) );
																																													?>
                                                </span> <a
											href="javascript:void(0);" class="btn red fileinput-exists"
											data-dismiss="fileinput"> Remove </a>

										<div class="fileinput-new thumbnail"
											style="width: 70px; height: 32px; float: right;">
											<img
												src="<?php
																																													
																																													echo ($user_model->get ( 'profileImage' )) ? user_content_url ( $user_model->get ( 'id' ) . '/' . $user_model->get ( 'profileImage' ) ) : image_url ( 'app/no_image.png' );
																																													?>"
												alt="">
										</div>
										<div class="fileinput-preview fileinput-exists thumbnail"
											style="max-width: 70px; max-height: 70px;"></div>
									</div>
                                            <?php } else{ ?>
                                                <?php echo text ( $user_model->get ( 'profileImage' ) ); ?>
                                            <div
										class="col-lg-3 chose-img">
										<img
											src="<?php echo ($user_model->get ( 'profileImage' )) ? user_content_url($user_model->get ( 'id' ).'/'.$user_model->get ( 'profileImage' )) : image_url('app/no_image.png'); ?>">
									</div>
                                            <?php } ?>
                                        </div>

							</div>

							<div class="col-lg-3 input_field_sections nowarp">
								
									<?php
									echo form_label ( 'DOJ:', 'doj', array (
											'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
									) );
									
									if ($view_mode == EDIT_MODE) {
										echo form_input ( array (
												'id' => 'doj',
												'name' => 'doj',
												'class' => 'form-control',
												'required' => 'required',
												'placeholder' => 'Date of Joining',
												'value' => ($user_model->get ( 'doj' )) ? $user_model->get ( 'doj' ) : '' 
										) );
									} else {
										echo text ( $user_model->get ( 'doj' ) );
									}
									?>
								
							</div>
							<div class="col-lg-3 input_field_sections nowarp">
									<?php
									echo form_label ( 'DOR:', 'dor', array (
											'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
									) );
									
									if ($view_mode == EDIT_MODE) {
										echo form_input ( array (
												'id' => 'dor',
												'name' => 'dor',
												'class' => 'form-control',
												'placeholder' => 'Date of Releave',
												'value' => ($user_model->get ( 'dor' )) ? $user_model->get ( 'dor' ) : '' 
										) );
									} else {
										echo text ( $user_model->get ( 'dor' ) );
									}
									?>
								
							</div>

							<div class="col-lg-3 input_field_sections nowarp">
								
								<?php
								echo form_label ( 'Role Type:', 'roleType', array (
										'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
								) );
								// validation for passenger first name
								if ($view_mode == EDIT_MODE) {
									echo form_dropdown ( 'roleType', $role_type_list, $user_model->get ( 'roleType' ), array (
											'id' => 'roleType',
											'class' => 'form-control',
											'required' => 'required' 
									) );
								} else {
									echo text ( $role_type_list [$user_model->get ( 'roleType' )] );
								}
								
								?>
							</div>


						</div>
						<div class="row">
							<div class="col-lg-3 input_field_sections nowarp">
								
								<?php
								echo form_label ( 'Zone:', 'zoneId', array (
										'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
								) );
								// validation for passenger first name
								if ($view_mode == EDIT_MODE) {
									echo form_dropdown ( 'zoneId', $zone_list, $user_model->get ( 'zoneId' ), array (
											'id' => 'zoneId',
											'class' => 'form-control',
											'required' => 'required' 
									) );
								} else {
									echo text ( @$zone_list [$user_model->get ( 'zoneId' )] );
								}
								
								?>
							</div>
							<div class="col-lg-3 input_field_sections nowarp">
								
								<?php
								echo form_label ( 'Entity:', 'entityId', array (
										'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
								) );
								// validation for passenger first name
								if ($view_mode == EDIT_MODE) {
									echo form_dropdown ( 'entityId', $entity_list, ($user_model->get ( 'entityId' ))?$user_model->get ( 'entityId' ):DEFAULT_COMPANY_ID, array (
											'id' => 'entityId',
											'class' => 'form-control',
											'required' => 'required' 
									) );
								} else {
									echo text ( @$entity_list [$user_model->get ( 'entityId' )] );
								}
								
								?>
							</div>
						</div>
					
			

		<div class="row">
			<div class="col-lg-12 m-t-35">

				<div class="card-header bg-white">
					<h1><?php echo ($user_model->get ( 'id' )) ?(($view_mode == VIEW_MODE)?'View':'Edit'): 'Create'; ?> User Personal</h1>
				</div>


				<div class="row">
					
							<div class="col-md-3 input_field_sections nowarp">
                                    <?php
                                    echo form_label('Gender:', 'gender', array(
                                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                                    ));
                                    if ($view_mode == EDIT_MODE) {
                                        if (is_array($gender_list) && count($gender_list)) {
                                            echo '<div class="mt-radio-inline">';
                                            foreach ($gender_list as $list) {
                                                echo '<label class="mt-radio" style="padding-right:10px;">';
                                                $is_checked = ($user_personal_model->get ('gender')==$list->description) ? TRUE : FALSE;
                                                $checked_value =$list->description;
                                              
                                                	echo form_radio ( array (
                                                			'id' => 'gender' . $list->description,
                                                			'name' => 'gender',
                                                			'class' => 'flat',
                                                			'required' => 'required'
                                                	), $checked_value, $is_checked );
                                               
                                                
                                                echo '<span></span>' . $list->description . '</label>';
                                            }
                                            echo '</div>';
                                        }
                                    } else {
                                        echo text($user_personal_model->get('gender'));
                                    }
                                    ?>
                                </div>

					<div class="col-lg-3 input_field_sections nowarp">
								
									<?php
									echo form_label ( 'DOB:', 'dob', array (
											'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
									) );
									
									if ($view_mode == EDIT_MODE) {
										echo form_input ( array (
												'id' => 'dob',
												'name' => 'dob',
												'class' => 'form-control',
												'required' => 'required',
												'placeholder' => 'DOB',
												'value' => ($user_personal_model->get ( 'dob' )) ? $user_personal_model->get ( 'dob' ) : '' 
										) );
									} else {
										echo text ( $user_personal_model->get ( 'dob' ) );
									}
									?>
								
							</div>
							<div class="col-lg-3 input_field_sections nowarp">
									<?php
									echo form_label ( 'Email:', 'email', array (
											'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
									) );
									
									if ($view_mode == EDIT_MODE) {
										echo form_input ( array (
												'id' => 'email',
												'name' => 'email',
												'class' => 'form-control',
												'required' => 'required',
												'pattern' => '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$',
												'placeholder' => 'Email Id',
												'value' => ($user_personal_model->get ( 'email' )) ? $user_personal_model->get ( 'email' ) : '' 
										) );
									} else {
										echo text ( $user_personal_model->get ( 'email' ) );
									}
									?>
							</div>
					<div class="col-lg-3 input_field_sections nowarp section-container">
						<div class="section-message hidden"></div>
									<?php
									echo form_label ( 'Mobile:', 'mobile', array (
											'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
									) );
									
									if ($view_mode == EDIT_MODE) {
										echo form_input ( array (
												'id' => 'mobile',
												'name' => 'mobile',
												'maxlength'=>'11',
												'class' => 'form-control',
												'required' => 'required',
												'pattern' => '^[0]?[789]\d{7,9}$',
												'placeholder' => 'Mobile',
												'value' => ($user_personal_model->get ( 'mobile' )) ? $user_personal_model->get ( 'mobile' ) : '' 
										) );
									} else {
										echo text ( $user_personal_model->get ( 'mobile' ) );
									}
									
									?>
							</div>
					
					

				</div>

				<div class="row">
					<div class="col-lg-3 input_field_sections nowarp">
									<?php
									echo form_label ( 'Alternate Mobile:', 'alternateMobile', array (
											'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
									) );
									
									if ($view_mode == EDIT_MODE) {
										echo form_input ( array (
												'id' => 'alternateMobile',
												'name' => 'alternateMobile',
												'class' => 'form-control',
												
												// 'required' => 'required',
												// 'pattern' => '[789]\d{9}',
												'placeholder' => ' Mobile',
												'value' => ($user_personal_model->get ( 'alternateMobile' )) ? $user_personal_model->get ( 'alternateMobile' ) : '' 
										) );
									} else {
										echo text ( $user_personal_model->get ( 'alternateMobile' ) );
									}
									
									?>
							</div>
					<div class="col-lg-3 input_field_sections nowarp">
								
									<?php
									echo form_label ( 'Qualification:', 'qualification', array (
											'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
									) );
									// validation for passenger first name
									if ($view_mode == EDIT_MODE) {
										echo form_input ( array (
												'id' => 'qualification',
												'name' => 'qualification',
												'class' => 'form-control',
												
												// 'required' => 'required',
												// 'pattern' => '[a-z0-9.]{2,20}',
												'placeholder' => 'Qualification',
												'value' => ($user_personal_model->get ( 'qualification' )) ? $user_personal_model->get ( 'qualification' ) : '' 
										) );
									} else {
										echo text ( $user_personal_model->get ( 'qualification' ) );
									}
									?>
								
							</div>


					<div class="col-lg-3 input_field_sections nowarp">
								
									<?php
									echo form_label ( 'Experience:', 'experience', array (
											'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
									) );
									// validation for passenger first name
									if ($view_mode == EDIT_MODE) {
										echo form_dropdown ( 'experience', $experience_list, $user_personal_model->get ( 'experience' ), array (
												'id' => 'experience',
												'class' => 'form-control',
												'required' => 'required'
										) );
									} else {
										echo text ( $experience_list [$user_personal_model->get ( 'experience' )] );
									}
									
									?>
								
							</div>
					<div class="col-lg-3 input_field_sections nowarp">
									<?php
									echo form_label ( 'Communication Address:', 'communicationAddress', array (
											'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
									) );
									
									if ($view_mode == EDIT_MODE) {
										echo form_textarea ( array (
												'id' => 'communicationAddress',
												'name' => 'communicationAddress',
												'class' => 'form-control',
												'rows' => '2',
												'required' => 'required',
												'value' => ($user_personal_model->get ( 'communicationAddress' )) ? $user_personal_model->get ( 'communicationAddress' ) : '' 
										) );
									} else {
										echo form_textarea ( array (
												'id' => 'communicationAddress',
												'name' => 'communicationAddress',
												'class' => 'form-control',
												'rows' => '2',
												'readonly' => 'readonly',
												'value' => ($user_personal_model->get ( 'communicationAddress' )) ? $user_personal_model->get ( 'communicationAddress' ) : '' 
										) );
									}
									?>
							</div>
					
				</div>
				<div class="row">
				<div class="col-lg-3 input_field_sections nowarp">
									<?php
									echo form_label ( 'Permanent Address:', 'permanentAddress', array (
											'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
									) );
									
									if ($view_mode == EDIT_MODE) {
										echo form_textarea ( array (
												'id' => 'permanentAddress',
												'name' => 'permanentAddress',
												'class' => 'form-control',
												'rows' => '2',
												// 'required' => 'required',
												'value' => ($user_personal_model->get ( 'permanentAddress' )) ? $user_personal_model->get ( 'permanentAddress' ) : '' 
										) );
									} else {
										echo form_textarea ( array (
												'id' => 'permanentAddress',
												'name' => 'permanentAddress',
												'class' => 'form-control',
												'rows' => '2',
												'readonly' => 'readonly',
												'value' => ($user_personal_model->get ( 'permanentAddress' )) ? $user_personal_model->get ( 'permanentAddress' ) : '' 
										) );
									}
									?>
							</div>
				</div>
				<div class="form-actions" style="margin-top: 22px;">
					<div class="row">
						<div class="col-md-12">
                                            <?php if ($view_mode == EDIT_MODE) { ?>
                                                <div class="gp-cen">

								<button type="button" id="cancel-user-btn"
									class="btn grey-salsa btn-outline">Cancel</button>
								<button type="button" id="save-user-btn" class="btn gpblue">Save</button>
							</div>
                                            <?php } else { ?>
                                            <div class="gp-cen">
								<button type="button" id="cancel-user-btn"
									class="btn btn-danger">Back</button>
							</div>
                                            <?php } ?>
                                        </div>
					</div>


				</div>
			</div>
			</div>
			</div>
			</div>
		</div>
		<?php
		echo form_close ();
		?>
	</div>
</div>