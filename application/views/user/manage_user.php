<style>
    .label.label-sm {
    font-size: 13px;
    padding: 2px 5px;
    cursor:pointer;
}
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">

        <div class="page-bar ">
            <ul class="page-breadcrumb ">
                <li>
                    <a href="<?php echo base_url('Dashboard');?>">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="<?php echo base_url('User/getUserList');?>">User List</a>

                </li>

            </ul>

        </div>
        <!---- Page Bar ends ---->
        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-list" aria-hidden="true"></i>User List </div>
                                       
                                    </div>
                                 
                                    <div class="portlet-body">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="col-md-3 btn-group gp-pad1">
                                                        <a href="<?php echo base_url('User/add');?>">
                                                        <button id="sample_editable_1_new" class="btn sbold green1"> Add New
                                                            <i class="fa fa-plus"></i>
                                                        </button>
                                                            </a>
                                                    </div>
                                                    
                                                    
                                                </div>
                                                
                                            </div>
                                        </div>
							<div id="table-columns" class="hidden">
							
									<label class="">
			                          <?php
											echo form_checkbox ( array (
													'class' => 'flat toggle-vis',
													'data-column'=>'9'
											) );
																													
									?>
			                          Qualification
									</label>
							
									<label class="">
			                          <?php
											echo form_checkbox ( array (
													'class' => 'flat toggle-vis',
													'data-column'=>'10',
											) );
																													
									?>
			                          Experience
									</label>
							
									<label class="">
			                          <?php
											echo form_checkbox ( array (
													'class' => 'flat toggle-vis',
													'data-column'=>'11', 
											) );
																													
									?>
			                          Address
									</label>
									<label class="">
			                          <?php
											echo form_checkbox ( array (
													'class' => 'flat toggle-vis',
													'data-column'=>'12',
											) );
																													
									?>
			                           Alternate Mobile
									</label>
							
									<label class="">
			                          <?php
											echo form_checkbox ( array (
													'class' => 'flat toggle-vis',
													'data-column'=>'13',
											) );
																													
									?>
			                          DOB
									</label>
							
									<label class="">
			                          <?php
											echo form_checkbox ( array (
													'class' => 'flat toggle-vis',
													'data-column'=>'14', 
											) );
																													
									?>
			                          DOJ
									</label>
									<label class="">
			                          <?php
											echo form_checkbox ( array (
													'class' => 'flat toggle-vis',
													'data-column'=>'15',
											) );
																													
									?>
			                          Gender
									</label>
									<label class="">
			                          <?php
											echo form_checkbox ( array (
													'class' => 'flat toggle-vis',
													'data-column'=>'16',
													
											) );
																													
									?>
			                         Zone
									</label>
									<label class="">
			                          <?php
											echo form_checkbox ( array (
													'class' => 'flat toggle-vis',
													'data-column'=>'17',
													
											) );
																													
									?>
			                         Entity
									</label>
							</div>
							<table class="table table-striped table-bordered table-hover table-checkable order-column" id="user-data-table" style="width:100%">
								
									<thead>
										<tr>
											<th>Action</th>
											<th>Image</th>
											<th>First Name</th>
											<th>Last Name</th>
											<th>Login Email</th>
											<th>Designation</th>
											<th>Mobile</th>
											<th>Email</th>
											<th>Role</th>
											<th>Qualification</th>
											<th>Experience</th>
											<th>Address</th>
											<th>Alternate Mobile</th>
											<th>DOB</th>
											<th>DOJ</th>
											<th>Gender</th>
											<th>Zone</th>
											<th>Entity</th>
											<th>Status</th>
											
										</tr>
									</thead>
									<tbody>
									<!-- <tbody class="fontaws"> -->
									</tbody>
									
								</table>
							 </div>
                                        </div>
                                                
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>