<div class="page-sidebar navbar-collapse collapse">
	<!-- BEGIN SIDEBAR MENU -->

	<ul class="page-sidebar-menu  page-header-fixed gp-span"
		data-keep-expanded="false" data-auto-scroll="true"
		data-slide-speed="200" style="padding-top: 10px">

		<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
		<li class="sidebar-toggler-wrapper hide">
			<div class="sidebar-toggler">
				<span></span>
			</div>
		</li>
		<!-- END SIDEBAR TOGGLER BUTTON -->
		<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
		<li class="sidebar-search-wrapper">
			<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->

			<!--<form class="sidebar-search  sidebar-search-bordered"
				action="page_general_search_3.html" method="POST">
				<a href="#" class="remove"> <i class="icon-close"></i>
				</a>
				<div class="input-group">
					<input type="text" class="form-control" placeholder="Search..."> <span
						class="input-group-btn" style="padding-left: 0px;"> <a href="#"
						class="btn submit"> <i class="icon-magnifier"></i>
					</a>
					</span>
				</div>
			</form>--> <!-- END RESPONSIVE QUICK SEARCH FORM -->
		</li>
		<li <?php echo ($active_menu=='dashboard')?'class="nav-item active"':'class="nav-item "'; ?>><a href="<?php echo base_url('Dashboard');?>"
			class="nav-link nav-toggle"> <img
				src="<?php echo image_url('app/home.png'); ?>"> <span class="title">Dashboard</span>

		</a></li>
		<?php if($this->User_Access_Model->showModule ($this->session->userdata('role_type'),Module_Name_Enum::CUSTOMER)): ?> 
		<li <?php echo ($active_menu=='passenger')?'class="nav-item active"':'class="nav-item"'; ?>>
                    <a href="<?php echo base_url('Passenger/getPassengerList');?>" class="nav-link nav-toggle">
                        <img src="<?php echo image_url('app/customer.png'); ?>"> 
                        <span class="title">Customers</span>

		</a></li>
		<?php endif;?>
		<?php if($this->User_Access_Model->showModule ($this->session->userdata('role_type'),Module_Name_Enum::DRIVER)):?> 
		 <li <?php echo ($active_menu=='driver')?'class="nav-item active"':'class="nav-item"'; ?>>
                    <a href="javascript:;" class="nav-link nav-toggle">
                       <img src="<?php echo image_url('app/drivers.png'); ?>"> 
                       <span class="title">Drivers</span><span class="arrow"></span>
                       
                    </a>
                    <ul class="sub-menu">
                    
                    <li class="nav-item  "><a href="<?php echo base_url('Driver/getDriverList'); ?>" class="nav-link "> <span
						class="title">Driver List</span>
				</a></li>
				<li class="nav-item  "><a href="<?php echo base_url('Driver/trackDrivers')?>" class="nav-link "> <span
						class="title">Driver's Tracking</span>
				</a></li>
			    </ul>
                </li>
               
		<?php endif;?>
		<?php if($this->User_Access_Model->showModule ($this->session->userdata('role_type'),Module_Name_Enum::TAXI)||
				$this->User_Access_Model->showModule ($this->session->userdata('role_type'),Module_Name_Enum::TAXI_DEVICE)||
				$this->User_Access_Model->showModule ($this->session->userdata('role_type'),Module_Name_Enum::TAXI_OWNER)):?> 
		
		<li <?php echo ($active_menu=='taxi'|| $active_menu=='taxiowner' || $active_menu=='taxidevice')?'class="nav-item active"':'class="nav-item"'; ?>><a href="javascript:;" class="nav-link nav-toggle"> <img
				src="<?php echo image_url('app/taxi-details.png'); ?>"> <span
				class="title">Taxi Info</span><span class="arrow"></span>

		</a>
		
		<ul class="sub-menu">
				
		
		<?php if($this->User_Access_Model->showModule ($this->session->userdata('role_type'),Module_Name_Enum::TAXI_OWNER)):?> 
				<li class="nav-item  "><a href="<?php echo base_url('TaxiOwner/getTaxiOwnerList'); ?>" class="nav-link "> <span
						class="title">Taxi Owner List</span>
				</a></li>
				<?php endif;?>
		<?php if($this->User_Access_Model->showModule ($this->session->userdata('role_type'),Module_Name_Enum::TAXI_DEVICE)):?> 
				<li class="nav-item  "><a href="<?php echo base_url('TaxiDevice/getTaxiDeviceList'); ?>" class="nav-link "> <span
						class="title">Taxi Device List</span>
				</a></li>
				<?php endif;?>
				<?php if($this->User_Access_Model->showModule ($this->session->userdata('role_type'),Module_Name_Enum::TAXI)):?> 
				<li class="nav-item  "><a href="<?php echo base_url('Taxi/getTaxiList'); ?>" class="nav-link "> <span
						class="title">Taxi List</span>
				</a></li>
				<?php endif;?>
		
			</ul>
		</li>
		<?php endif;?>
		<?php if($this->User_Access_Model->showModule ($this->session->userdata('role_type'),Module_Name_Enum::TRIP)):?> 
		<li <?php echo ($active_menu=='trip')?'class="nav-item active"':'class="nav-item"'; ?>><a href="javascript:;" class="nav-link nav-toggle"> <img
				src="<?php echo image_url('app/trip-details.png'); ?>"> <span
				class="title">Trip Info</span><span class="arrow"></span>

		</a>
		<ul class="sub-menu">
		
				<li class="nav-item  "><a href="<?php echo base_url('Trip/getTripList'); ?>" class="nav-link "> <span
						class="title">Trip List</span>
				</a></li>
				
				<li class="nav-item  "><a href="<?php echo base_url('TripTransaction/getTripTransactionList'); ?>" class="nav-link "> <span
						class="title">Completed Trip List</span>
				</a></li>
				
		
				
			</ul>
		
		</li>
		<?php endif;?>
		<?php if($this->User_Access_Model->showModule ($this->session->userdata('role_type'),Module_Name_Enum::PROMOCODE)):?> 
		<li <?php echo ($active_menu=='promocode')?'class="nav-item active"':'class="nav-item"'; ?>><a href="<?php echo base_url('Promocode/getPromocodeList')?>" class="nav-link nav-toggle"> <img
				src="<?php echo image_url('app/promocode.png'); ?>"> <span
				class="title">Promocode</span>

		</a></li>
		<?php endif;?>
		<?php if($this->User_Access_Model->showModule ($this->session->userdata('role_type'),Module_Name_Enum::USERS)):?> 
		<li <?php echo ($active_menu=='user')?'class="nav-item active"':'class="nav-item"'; ?>><a href="<?php echo base_url('User/getUserList')?>" class="nav-link nav-toggle"> <img
				src="<?php echo image_url('app/users.png'); ?>"> <span
				class="title">Users</span>

		</a></li>
		<?php endif;?>
		<?php if($this->User_Access_Model->showModule ($this->session->userdata('role_type'),Module_Name_Enum::REPORTS)):?> 
		<li <?php echo ($active_menu=='report')?'class="nav-item active"':'class="nav-item"'; ?>><a href="?p=" class="nav-link nav-toggle"> <img
				src="<?php echo image_url('app/report.png'); ?>"> <span
				class="title">Reports</span> <span class="arrow"></span>
				
		</a>
			<ul class="sub-menu">
				<li class="nav-item  "><a href="<?php echo base_url('Report/getDriverTransactionReport')?>" class="nav-link "> <span
						class="title">Driver Transaction</span>
				</a></li>
				<li class="nav-item  "><a href="<?php echo base_url('Report/getDriverTaxiMappingReport')?>" class="nav-link "> <span
						class="title">Driver Taxi</span>
				</a></li>
				<li class="nav-item  "><a href="<?php echo base_url('Report/getDriverAvailableReport')?>" class="nav-link "> <span
						class="title">Driver Available Status</span>
				</a></li>
				<li class="nav-item  "><a href="<?php echo base_url('Report/getDriverRejectReport')?>" class="nav-link "> <span
						class="title">Driver Consecutive Reject</span>
				</a></li>
				<li class="nav-item  "><a href="<?php echo base_url('Report/getDriverWalletReport')?>" class="nav-link "> <span
						class="title">Driver Wallet Warning</span>
				</a></li>
				<li class="nav-item  "><a href="<?php echo base_url('Report/getDriverCreditReport')?>" class="nav-link "> <span
						class="title">Driver Credit Report</span>
				</a></li>
				<li class="nav-item  "><a href="<?php echo base_url('Report/getTripDetailsReport')?>" class="nav-link "> <span
						class="title">Trip Details Report</span>
				</a></li>
				<li class="nav-item  "><a href="<?php echo base_url('Report/getCancelledTripReport')?>" class="nav-link "> <span
						class="title">Cancelled Trip Report</span>
				</a></li>
				<li class="nav-item "><a href="<?php echo base_url('Report/getPromocodeReport')?>" class="nav-link "> <span
						class="title">Promocode Report</span>
				</a></li>
				<li class="nav-item "><a href="<?php echo base_url('Report/getDriverDailyReport')?>" class="nav-link "> <span
						class="title">Driver Daily Work Report</span>
				</a></li>
				
			</ul></li>
		<?php endif;?>
		<?php if($this->User_Access_Model->showModule ($this->session->userdata('role_type'),Module_Name_Enum::TAXI_TARIFF)):?> 
		<li <?php echo ($active_menu=='taxitariff')?'class="nav-item active"':'class="nav-item"'; ?>><a href="<?php echo base_url('TaxiTariff/getTaxiTariffList')?>" class="nav-link nav-toggle"> <img
				src="<?php echo image_url('app/rate-card.png'); ?>"> <span
				class="title">Rate Card</span></a></li>
		<?php endif;?>
		<?php if($this->User_Access_Model->showModule ($this->session->userdata('role_type'),Module_Name_Enum::SOS)):?> 
		<li <?php echo ($active_menu=='emergency')?'class="nav-item active"':'class="nav-item"'; ?>><a href="<?php echo base_url('Emergency/getEmergencyList')?>" class="nav-link nav-toggle"> <img
				src="<?php echo image_url('app/sos.png'); ?>"> <span class="title">SOS</span>

		</a></li>
		<?php endif;?>
		<?php if($this->User_Access_Model->showModule ($this->session->userdata('role_type'),Module_Name_Enum::COUNTRY)||
				$this->User_Access_Model->showModule ($this->session->userdata('role_type'),Module_Name_Enum::ENTITY)||
				$this->User_Access_Model->showModule ($this->session->userdata('role_type'),Module_Name_Enum::NOTIFICATION)||
				$this->User_Access_Model->showModule ($this->session->userdata('role_type'),Module_Name_Enum::MODULE)||
				$this->User_Access_Model->showModule ($this->session->userdata('role_type'),Module_Name_Enum::ZONE)):?>  
		<li <?php echo ($active_menu=='country' || $active_menu=='entity' || $active_menu=='notification' || $active_menu=='module' || $active_menu=='zone')?'class="nav-item active"':'class="nav-item"'; ?>>
                    <a href="#" class="nav-link nav-toggle"> 
                       <img src="<?php echo image_url('app/settings.png'); ?>"> 
                       <span class="title">Settings</span>
                       <span class="arrow"></span>
                    </a>
                        <ul class="sub-menu">
                        
		<?php if($this->User_Access_Model->showModule ($this->session->userdata('role_type'),Module_Name_Enum::ENTITY)):?>  
				<li class="nav-item  ">
                                    <a href="<?php echo base_url('Entity/getEntityList')?>" class="nav-link "> 
                                        <span class="title">Entity List</span>
				   </a>
                                </li>
                                <?php endif;?>
		<?php if($this->User_Access_Model->showModule ($this->session->userdata('role_type'),Module_Name_Enum::ZONE)):?> 
				<li class="nav-item  ">
                                    <a href="<?php echo base_url('Zone/getZoneList')?>" class="nav-link "> 
                                        <span class="title">Zone List</span>
				   </a>
                                </li>
                                <?php endif;?>
		<?php if($this->User_Access_Model->showModule ($this->session->userdata('role_type'),Module_Name_Enum::COUNTRY)):?> 
                  <li class="nav-item  ">
                                    <a href="<?php echo base_url('Country/getCountryList')?>" class="nav-link "> 
                                        <span class="title">Country List</span>
				   </a>
                                </li>
                                <?php endif;?>
		<?php if($this->User_Access_Model->showModule ($this->session->userdata('role_type'),Module_Name_Enum::NOTIFICATION)):?> 
                    <li class="nav-item  ">
                                    <a href="<?php echo base_url('Notification/getNotificationList')?>" class="nav-link "> 
                                        <span class="title">Notification List</span>
				   </a>
                                </li>
                                <?php endif;?>
		<?php if($this->User_Access_Model->showModule ($this->session->userdata('role_type'),Module_Name_Enum::MODULE)):?> 
                   <li class="nav-item  ">
                                    <a href="<?php echo base_url('Module')?>" class="nav-link "> 
                                        <span class="title">Module Permission</span>
				   </a>
                                </li>
                                <?php endif;?>
		
			</ul>
                </li>

<?php endif;?>
		

	</ul>
	
	<!-- END SIDEBAR MENU -->
	<!-- END SIDEBAR MENU -->
</div>