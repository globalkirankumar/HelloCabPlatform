<!DOCTYPE html>

<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Hello Cabs | Dashboard</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />

        <meta content="" name="author" />

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"
              rel="stylesheet" type="text/css" />
        <link href="<?php echo css_url('app/font-awesome.css'); ?>" rel="stylesheet"
              type="text/css" />
        <link href="<?php echo css_url('app/simple-line-icons.css'); ?>"
              rel="stylesheet" type="text/css" />
        <link href="<?php echo css_url('app/bootstrap.min.css'); ?>"
              rel="stylesheet" type="text/css" />
        <link href="<?php echo css_url('app/bootstrap-switch.min.css'); ?>"
              rel="stylesheet" type="text/css" />
        <link href="<?php echo css_url('app/bootstrap-select.css'); ?>"
              rel="stylesheet" type="text/css" />
         
        <!-- END GLOBAL MANDATORY STYLES -->

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link
            href="<?php echo css_url('vendors/datatables/css/datatables.min.css'); ?>"
            rel="stylesheet" type="text/css" />
        <link
            href="<?php echo css_url('vendors/datatables/css/datatables.bootstrap.css'); ?>"
            rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->


        <!-- BEGIN THEME GLOBAL STYLES -->
         <?php if($this->uri->segment(2) !='trackDrivers') :?>
        <link href="<?php echo css_url('app/components.css'); ?>" rel="stylesheet" id="style_components" type="text/css" />
        <?php endif;?>
        <link href="<?php echo css_url('app/plugins.css'); ?>" rel="stylesheet"
              type="text/css" />
        <!-- END THEME GLOBAL STYLES -->



        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?php echo css_url('app/layout.css'); ?>" rel="stylesheet"
              type="text/css" />
        <link href="<?php echo css_url('app/darkblue.css'); ?>" rel="stylesheet"
              type="text/css" id="style_color" />
        <link href="<?php echo css_url('app/custom.css'); ?>" rel="stylesheet"
              type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        
             <!-- Begin select file -->
         <link href="<?php echo css_url('vendors/file-choose/css/bootstrap-fileinput.css'); ?>" rel="stylesheet"
              type="text/css" />
        <!-- END select file -->

  <!-- Begin datepicker -->
         <link href="<?php echo css_url('vendors/datetimepicker/css/jquery.datetimepicker.css'); ?>" rel="stylesheet"
              type="text/css" />
               
        <!-- END date picker -->


        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->

    </head>
    <!-- END HEAD -->


    <body
        class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <div class="page-header navbar navbar-fixed-top">
                <!-- BEGIN HEADER INNER -->
                <?php
                if (isset($template['partials']['header'])) {
                    
                    echo $template['partials']['header'];
                  
                }
                ?>
                <!-- END HEADER INNER -->
            </div>
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"></div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
            <div class="bg-overlay hidden" id="overlay" > </div>
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar-wrapper">
                    <!-- BEGIN SIDEBAR -->

                    <?php
                    if (isset($template ['partials'] ['side_bar_menu'])) {
                        echo $template ['partials'] ['side_bar_menu'];
                    }
                    ?>
                    <!-- END SIDEBAR -->
                </div>
                <!-- END SIDEBAR -->

                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper" id="loading">
				<div id="controller-messages" class="gp-alert" style="display: none;">
					<div class="alert alert-success alert-dismissible fade in"
						role="alert">
						<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<span id="app-message"></span>
					</div>
				</div>
                    <!-- BEGIN CONTENT BODY -->
<?php
if (isset($template['body'])) {

    echo $template['body'];
}
?>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->

            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            <div class="page-footer">
<?php
if (isset($template['partials']['footer'])) {
    echo $template['partials']['footer'];
}
?>
            </div>
            <!-- END FOOTER -->
        </div>
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?php //echo js_url('jquery-3.2.1.min.js'); ?>"></script>
        <script src="<?php echo js_url('jquery.min.js'); ?>"></script>
        <script src="<?php echo js_url('bootstrap.min.js'); ?>"
        type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        
        <script src="<?php echo js_url('app.js'); ?>" type="text/javascript"></script>
       
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?php echo js_url('layout.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo js_url('demo.js'); ?>" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->


        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script
            src="<?php //echo js_url('vendors/datatables/js/datatable.js'); ?>"
        type="text/javascript"></script>
        <script
            src="<?php echo js_url('vendors/datatables/js/datatables.min.js'); ?>"
        type="text/javascript"></script>
        <script
            src="<?php //echo js_url('vendors/datatables/js/datatables.bootstrap.js'); ?>"
        type="text/javascript"></script>
        <script src="<?php echo js_url('bootstrap-select.min.js'); ?>"
        type="text/javascript"></script>

        <!-- END PAGE LEVEL PLUGINS -->

        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script
            src="<?php //echo js_url('vendors/datatables/js/table-datatables-managed.js'); ?>"
        type="text/javascript"></script>
        
        <!-- END PAGE LEVEL SCRIPTS -->
        
        
         <!-- Begin select file -->
         
          <script
            src="<?php echo js_url('vendors/file-choose/js/bootstrap-fileinput.js'); ?>"
        type="text/javascript"></script>
        
        <!-- end select file -->
        
        
        <!-- Begin select file -->
        
        <script
            src="<?php echo js_url('vendors/datetimepicker/js/jquery.datetimepicker.full.js'); ?>"
        type="text/javascript"></script>
         
        
        <!-- end select file -->
        
        <?php echo $template['metadata']; ?>
    </body>

</html>