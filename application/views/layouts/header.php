<div class="page-header-inner ">
	<!-- BEGIN LOGO -->
	<div class="page-logo">
		<a href="<?php echo base_url('Dashboard')?>"> <img
			src="<?php echo image_url('app/logo-main.png'); ?>" alt="logo"
			class="logo-default" />
		</a>

	</div>
	<!-- END LOGO -->
	<!-- BEGIN RESPONSIVE MENU TOGGLER -->
	<a href="#" class="menu-toggler responsive-toggler"
		data-toggle="collapse" data-target=".navbar-collapse"> <span></span>
	</a>

	<!-- END RESPONSIVE MENU TOGGLER -->
	<!-- BEGIN TOP NAVIGATION MENU -->

	<div class="menu-toggler sidebar-toggler pull-left">
		<span></span>
	</div>
	<?php if ($this->uri->segment(1)=='Dashboard' || $this->uri->segment(1)=='dashboard' || ($this->uri->segment(1)=='Driver' && $this->uri->segment(2)=='trackDrivers')):?>
	<div class="col-md-2 gp-select" style="max-width: 200px;">

		<select id="header_zone" class="selectpicker show-tick form-control">
			<option value=''>Select Zone</option>
		</select>
	</div>

	<div class="col-md-2 gp-select" style="max-width: 200px;" id='sub_zone_sec'>

		<select id="header_subzone" class="selectpicker show-tick form-control">
			<option value=''>Select sub Zone</option>
                </select>
	</div>
	<?php endif; ?>
	<?php if(($this->session->userdata ('role_type')==Role_Type_Enum::SUPER_ADMIN) && ($this->uri->segment(1)=='Dashboard' || $this->uri->segment(1)=='dashboard')):?>
	<div class="col-md-2 gp-select hidden" style="max-width: 200px;" id='force_update'>

		<button type="button" id="force-update-btn" class="form-control btn red btn-block uppercase" style="padding: 5px 10px; margin-top: 5px;">Force Update</button>
	</div>
	<?php endif;?>
	<div class="top-menu">

		<ul class="nav navbar-nav pull-right">
			<!-- BEGIN NOTIFICATION DROPDOWN -->
			<li class="btn red hidden" id="manual-refresh" style="padding: 5px 10px; margin-top: 5px; font-size: 18px;
font-weight: bold;" onclick="location.reload();"><i class="fa fa-refresh" aria-hidden="true"></i>
                        </li>
                        <li class="btn red hidden fa fa-refresh" id="timer" style="cursor: unset;padding: 5px 10px; margin-top: 5px; font-size: 18px;
font-weight: bold;">&nbsp;Auto:<i id="timer_display" class="timerDisplay"></i>
                        </li>
                       

			<li class="dropdown dropdown-extended dropdown-notification"
				id="header_notification_bar"><a href="#" class="dropdown-toggle"
				data-toggle="dropdown" data-hover="dropdown"
				data-close-others="true"> <i class="icon-bell"></i> <span
					class="badge badge-default"> 7 </span>
			</a>
				<ul class="dropdown-menu">
					<li class="external">
						<h3>
							<span class="bold">Notifications</span>
						</h3> <a href="page_user_profile_1.html">view all</a>
					</li>
					<li>
						<ul class="dropdown-menu-list scroller" style="height: 250px;"
							data-handle-color="#637283">
							<li><a href="#"> <span class="time">just now</span> <span
									class="details"> <span
										class="label label-sm label-icon label-success"> <i
											class="fa fa-plus"></i>
									</span> Notifications 1
								</span>
							</a></li>





						</ul>
					</li>
				</ul></li>
			<!-- END NOTIFICATION DROPDOWN -->
			<!-- BEGIN INBOX DROPDOWN -->

			<li class="dropdown dropdown-extended dropdown-inbox"
				id="header_inbox_bar"><a href="#" class="dropdown-toggle"
				data-toggle="dropdown" data-hover="dropdown"
				data-close-others="true"> <i class="icon-envelope-open"></i> <span
					class="badge badge-default"> 4 </span>
			</a>
				<ul class="dropdown-menu">
					<li class="external">
						<h3>
							You have <span class="bold"> Messages </span>
						</h3> <a href="app_inbox.html">view all</a>
					</li>
					<!-- <li>
                                        <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                                            <li>
                                                <a href="#">
                                                    <span class="photo">
                                                        <img src="../assets/layouts/layout3/img/avatar2.jpg" class="img-circle" alt=""> </span>
                                                    <span class="subject">
                                                        <span class="from"> Lisa Wong </span>
                                                        <span class="time">Just Now </span>
                                                    </span>
                                                    <span class="message"> Vivamus sed auctor nibh congue nibh. auctor nibh auctor nibh... </span>
                                                </a>
                                            </li>
                                           
                                            
                                        </ul>
                                    </li>-->
				</ul></li>
			<!-- END INBOX DROPDOWN -->
			<!-- BEGIN TODO DROPDOWN -->

			<li class="dropdown dropdown-extended dropdown-tasks"
				id="header_task_bar"><a href="#" class="dropdown-toggle"
				data-toggle="dropdown" data-hover="dropdown"
				data-close-others="true"> <i class="icon-calendar"></i> <span
					class="badge badge-default"> 3 </span>
			</a>
				<ul class="dropdown-menu extended tasks">
					<li class="external">
						<h3>
							You have <span class="bold">Tasks<br></span>
						</h3> <a href="app_todo.html">view all</a>
					</li>
					<!-- <li>
                                        <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                                            <li>
                                                <a href="#">
                                                    <span class="task">
                                                        <span class="desc">New release v1.2 </span>
                                                        <span class="percent">30%</span>
                                                    </span>
                                                    <span class="progress">
                                                        <span style="width: 40%;" class="progress-bar progress-bar-success" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">40% Complete</span>
                                                        </span>
                                                    </span>
                                                </a>
                                            </li>
                                           
                                        </ul>
                                    </li>-->
				</ul></li>
			<!-- END TODO DROPDOWN -->
			<!-- BEGIN USER LOGIN DROPDOWN -->

			<li class="dropdown dropdown-user"><a href="#"
				class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
				data-close-others="true"> <img alt="" class="img-circle"
					src="<?php echo $this->session->userdata('user_profile') ;?>" />
                                <span class="username username-hide-on-mobile">
                                    <?php echo $this->session->userdata('user_name') ;?>
                                </span> 
                                <i class="fa fa-angle-down"></i>
			</a>
				<ul class="dropdown-menu dropdown-menu-default">
					<li><a href="#"> <i class="icon-user"></i>
							My Profile
					</a></li>
					<li><a href=<?php echo base_url('User/changePassword');?>> <i class="icon-key"></i> Change Password
					</a></li>

					<li><a href=<?php echo base_url("Logout/signOut");?>> <i class="icon-key"></i> Log
							Out
					</a></li>
				</ul></li>
			<!-- END USER LOGIN DROPDOWN -->
			<!-- BEGIN QUICK SIDEBAR TOGGLER -->


			<!-- END QUICK SIDEBAR TOGGLER -->
		</ul>
	</div>
	<!-- END TOP NAVIGATION MENU -->
</div>
