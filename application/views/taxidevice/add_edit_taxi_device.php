<?php
$view_mode = $mode;
?>
<!-- BEGIN CONTENT -->

<!-- BEGIN CONTENT BODY -->
<div class="page-content">
<?php
$form_attr = array (
		'name' => 'edit_taxi_device_form',
		'id' => 'edit_taxi_device_form',
		'method' => 'POST' 
);
echo form_open_multipart ( base_url ( '' ), $form_attr );
// driver id by default is -1
echo form_input ( array (
		'type' => 'hidden',
		'id' => 'taxi_device_id',
		'name' => 'id',
		'value' => ($taxidevice_model->get ( 'id' )) ? $taxidevice_model->get ( 'id' ) : - 1 
) );

?>
        <div class="page-bar ">
		<ul class="page-breadcrumb ">
			<li><a href="<?php echo base_url('Dashboard'); ?>">Home</a> <i
				class="fa fa-circle"></i></li>
			<li><a href="<?php echo base_url('TaxiDevice/getTaxiDeviceList'); ?>">Taxi Device
					List</a> <i class="fa fa-circle"></i></li>
			<li><a href="<?php echo base_url(uri_string()); ?>"><?php echo ($taxidevice_model->get ( 'id' )) ?(($view_mode == VIEW_MODE)?'View':'Edit'): 'Create'; ?> Taxi Device</a></li>

		</ul>

	</div>
	<!---- Page Bar ends ---->
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->

			<div class="portlet-body">
				<div class="form-body gp-form">
					<div class="card-header bg-white">
						<h1>
<?php echo ($taxidevice_model->get ( 'id' )) ?(($view_mode == VIEW_MODE)?'View':'Edit'): 'Create'; ?> Taxi Device</h1>
					</div>


					<div class="row">
						<div class="col-lg-3 input_field_sections nowarp">
								<?php
								
								echo form_label ( 'IMEI Number:', 'imeiNo', array (
										'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
								) );
								
								if ($view_mode == EDIT_MODE) {
									echo form_input ( array (
											'id' => 'imeiNo',
											'name' => 'imeiNo',
											'class' => 'form-control',
											'required' => 'required',
											'pattern' => '[a-zA-Z0-9\s-:]{3,15}',
											'placeholder' => 'IMEI Number',
											'value' => ($taxidevice_model->get ( 'imeiNo' )) ? $taxidevice_model->get ( 'imeiNo' ) : '' 
									) );
								} else {
									echo text ( $taxidevice_model->get ( 'imeiNo' ) );
								}
								?>
								
							</div>
						<div class="col-lg-3 input_field_sections nowarp section-container">
								<div class="section-message"></div>
									<?php
									echo form_label ( 'Mobile No:', 'mobile', array (
											'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
									) );
									
									if ($view_mode == EDIT_MODE) {
										echo form_input ( array (
												'id' => 'mobile',
												'name' => 'mobile',
												'maxlength'=>'11',
												'class' => 'form-control',
												'required' => 'required',
												'pattern' => '^[0]?[789]\d{7,9}$',
                                        		'placeholder' => 'Mobile',
												'value' => ($taxidevice_model->get ( 'mobile' )) ? $taxidevice_model->get ( 'mobile' ) : '' 
										) );
									} else {
										echo text ( $taxidevice_model->get ( 'mobile' ) );
									}
									?>
								
							</div>

						<div class="col-lg-3 input_field_sections nowarp">
								
									<?php
									echo form_label ( 'Device Status:', 'deviceStatus', array (
											'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
									) );
									
									if ($view_mode == EDIT_MODE) {
										echo form_dropdown ( 'deviceStatus', $device_status_list, $taxidevice_model->get ( 'deviceStatus' ), array (
												'id' => 'deviceStatus',
												'class' => 'form-control',
												'required' => 'required' 
										) );
									} else {
										echo text ( $device_status_list[$taxidevice_model->get ( 'deviceStatus' )] );
									}
									
									?>
									
								
							</div>
						<div class="col-lg-3 input_field_sections nowarp">
								
									<?php
									echo form_label ( 'Purchased Date:', 'purchasedDate', array (
											'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
									) );
									
									if ($view_mode == EDIT_MODE) {
										echo form_input ( array (
												'id' => 'purchasedDate',
												'name' => 'purchasedDate',
												'class' => 'form-control',
												
												// 'required' => 'required',
												'value' => ($taxidevice_model->get ( 'purchasedDate' )) ? $taxidevice_model->get ( 'purchasedDate' ) : '' 
										) );
									} else {
										echo text ( $taxidevice_model->get ( 'purchasedDate' ) );
									}
									?>
								
							</div>

					</div>
					<div class="row">

						<div class="col-md-3 input_field_sections nowarp gp-nomar">
                                <?php
																																echo form_label ( 'Warranty:', 'isWarranty', array (
																																		'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																																) );
																																?>
                                <div class="">
								<label class="mt-checkbox">
                                        <?php
                                        


                                        $is_checked = $taxidevice_model->get ( 'isWarranty' ) ? TRUE : FALSE;
                                        $checked = $taxidevice_model->get ( 'isWarranty' ) ? 1 : 0;
                                        if ($view_mode == EDIT_MODE) {
                                        	echo form_checkbox ( array (
                                        			'id' => 'isWarranty',
                                        			'name' => 'isWarranty',
                                        			'class' => 'flat'
                                        	)
                                        			// 'required' => 'required',
                                        			, $checked, $is_checked );
                                        } else {
                                        	echo form_checkbox ( array (
                                        			'id' => 'isWarranty',
                                        			'name' => 'isWarranty',
                                        			'class' => 'flat',
                                        			'disabled' => 'disabled'
                                        	)
                                        			// 'required' => 'required',
                                        			, $checked, $is_checked );
                                        }
																																								
																																								?> Warranty
                                        <span></span>
								</label>
							</div>
						</div>
						<?php if ($taxidevice_model->get ( 'isAllocated' )==TRUE):?>
						<div class="col-md-3 input_field_sections nowarp gp-nomar">
                                <?php
																																echo form_label ( 'Allocated:', 'isAllocated', array (
																																		'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																																) );
																																?>
                                <div class="">
								<label class="mt-checkbox">
                                        <?php
                                        $is_checked = $taxidevice_model->get ( 'isAllocated' ) ? TRUE : FALSE;
                                        $checked = $taxidevice_model->get ( 'isAllocated' ) ? 1 : 0;
                                        if ($view_mode == EDIT_MODE) {
                                        	echo form_checkbox ( array (
                                        			'id' => 'isAllocated',
                                        			'name' => 'isAllocated',
                                        			'class' => 'flat',
                                        			'disabled' => 'disabled'
                                        	)
                                        			// 'required' => 'required',
                                        			, $checked, $is_checked );
                                        } else {
                                        	echo form_checkbox ( array (
                                        			'id' => 'isAllocated',
                                        			'name' => 'isAllocated',
                                        			'class' => 'flat',
                                        			'disabled' => 'disabled'
                                        	)
                                        			// 'required' => 'required',
                                        			, $checked, $is_checked );
                                        }
																																								
																																								?> Allocated
                                        <span></span>
								</label>
							</div>
						</div>
						<?php endif;?>


					</div>
					<div class="form-actions" style="margin-top: 22px;">
						<div class="row">
							<div class="col-md-12">
                                            <?php if ($view_mode == EDIT_MODE) { ?>
                                                <div class="gp-cen">
									<button type="button" id="cancel-taxidevice-btn"
										class="btn grey-salsa btn-outline">Cancel</button>
									<button type="button" id="save-taxidevice-btn"
										class="btn gpblue">Save</button>

								</div>
                                            <?php } else { ?>
                                                                                 
                                       <div class="gp-cen">
                                                <button type="button"
                                                        id="cancel-taxidevice-btn" class="btn btn-danger">Back</button>
                                            </div>
                                            <?php } ?>
                                        </div>
						</div>
					</div>



				</div>
			</div>
		</div>
			<?php
			echo form_close ();
			?>
		</div>
</div>
