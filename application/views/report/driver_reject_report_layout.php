<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->

		<div class="row">
		<?php
		$form_attr = array (
				'name' => 'driver_reject_report_form',
				'id' => 'driver_reject_report_form',
				'method' => 'POST' 
		);
		echo form_open_multipart ( base_url ( '' ), $form_attr );
		// driver id by default is -1
		
		?>
	
			<div class="">

				<!-- BEGIN BORDERED TABLE PORTLET-->
				<div class="portlet box green gp-form">
					<div class="portlet-title">
						<div class="caption">Driver Details</div>

					</div>
					<div class="portlet-body">
						
						<div class="row">
							<div class="col-lg-3 input_field_sections nowarp">

                            <?php
																												echo form_label ( 'Time Period:', 'timePeriod', array (
																														'class' => 'required' 
																												) );
																												
																												echo form_dropdown ( 'timePeriod',$time_period_list , '', array (
																														'id' => 'timePeriod',
																														'class' => 'form-control',
																														'required' => 'required' 
																												) );
																												
																												?>

                        	</div>
							<div class="col-lg-3 input_field_sections nowarp">

                            <?php
																												echo form_label ( 'Consecutive Reject Count:', 'consecutiveReject', array (
																														'class' => '' 
																												) );
																												
																												echo form_dropdown ( 'consecutiveReject', $consecutive_reject_list, '', array (
																														'id' => 'consecutiveReject',
																														'class' => 'form-control',
																														//'required' => 'required' 
																												) );
																												
																												?>

                        </div>
                       
						</div>

						<div class="form-actions" style="margin-top: 22px;">
							<div class="row">
								<div class="col-md-12">

									<div class="gp-cen">
										<button type="button" id="driver-reject-report-btn"
											class="btn gpblue">Get Report</button>
									</div>
								</div>
							</div>
						</div>

					</div>
					<!-- END BORDERED TABLE PORTLET-->

				</div>

			</div>
			<?php echo form_close(); ?>
		</div>

		<div class="row">
			<div class="">
				<!-- BEGIN BORDERED TABLE PORTLET-->
				<div class="portlet box green gp-form">
					<div class="portlet-title">
						<div class="caption">Driver Consecutive Reject Report</div>

					</div>
					<div class="portlet-body">
						<table
							class="table table-striped table-bordered table-hover table-checkable order-column"
							id="driver-reject-data-table" style="width: 99%">
							<thead>
								<tr>
									<th>Driver Code</th>
									<th>Driver Name</th>
									<th>Driver Mobile</th>
									<th>Status</th>
									<th>Consecutive Reject Count</th>
									<th>Taxi No</th>
									<th>Taxi Device Code</th>
									<th>Taxi Device Mobile</th>
									
								</tr>
							</thead>
							<tbody>

							</tbody>
						</table>
					</div>

				</div>
			</div>
		</div>

	</div>

</div>