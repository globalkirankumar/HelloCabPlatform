<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->

		<div class="row">
		<?php
		$form_attr = array (
				'name' => 'trip_details_report_form',
				'id' => 'trip_details_report_form',
				'method' => 'POST' 
		);
		echo form_open_multipart ( base_url ( '' ), $form_attr );
		// driver id by default is -1
		
		?>
	
			<div class="">

				<!-- BEGIN BORDERED TABLE PORTLET-->
				<div class="portlet box green gp-form">
					<div class="portlet-title">
						<div class="caption">Trip Details</div>

					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-lg-3 input_field_sections nowarp">

                                <?php
																																echo form_label ( 'Start date:', 'startDate', array (
																																		'class' => 'required' 
																																) );
																																
																																echo form_input ( array (
																																		'id' => 'startDate',
																																		'name' => 'startDate',
																																		'class' => 'form-control',
																																		'required' => 'required',
																																		'placeholder' => 'Start Date',
																																		'value' => '' 
																																) );
																																
																																?>
                            </div>
							<div class="col-lg-3 input_field_sections nowarp">

                                <?php
																																echo form_label ( 'End Date:', 'endDate', array (
																																		'class' => 'required' 
																																) );
																																
																																echo form_input ( array (
																																		'id' => 'endDate',
																																		'name' => 'endDate',
																																		'class' => 'form-control',
																																		'required' => 'required',
																																		'placeholder' => 'End Date',
																																		'value' => '' 
																																) );
																																
																																?>
                            </div>
						</div>
						<div class="row">
							
							<div class="col-lg-3 input_field_sections nowarp">

                            <?php
																												echo form_label ( 'Entity:', 'entityId', array (
																														'class' => '' 
																												) );
																												
																												echo form_dropdown ( 'entityId', $entity_list, '', array (
																														'id' => 'entityId',
																														'class' => 'form-control',
																														//'required' => 'required' 
																												) );
																												
																												?>

                        </div>
                        <div class="col-lg-3 input_field_sections nowarp">

                            <?php
																												echo form_label ( 'Zone:', 'zoneId', array (
																														'class' => '' 
																												) );
																												
																												echo form_dropdown ( 'zoneId', $zone_list, '', array (
																														'id' => 'zoneId',
																														'class' => 'form-control',
																														//'required' => 'required' 
																												) );
																												
																												?>

                        </div>
						</div>

						<div class="form-actions" style="margin-top: 22px;">
							<div class="row">
								<div class="col-md-12">

									<div class="gp-cen">
										<button type="button" id="trip-details-report-btn"
											class="btn gpblue">Get Report</button>
									</div>
								</div>
							</div>
						</div>

					</div>
					<!-- END BORDERED TABLE PORTLET-->

				</div>

			</div>
			<?php echo form_close(); ?>
		</div>

		<div class="row">
			<div class="">
				<!-- BEGIN BORDERED TABLE PORTLET-->
				<div class="portlet box green gp-form">
					<div class="portlet-title">
						<div class="caption">Trip Details Report</div>

					</div>
					<div class="portlet-body">
						<table
							class="table table-striped table-bordered table-hover table-checkable order-column"
							id="trip-details-data-table" style="width: 99%">
							<thead>
								<tr>
									<th>Zone Name</th>
									
									<th>UnAssigned</th>
									<th>UpComing</th>
									<th>OnGoing</th>
									<th>Completed</th>
									<th>Passenger Cancelled</th>
									<th>Driver Cancelled</th>
									<th>Driver Rejected</th>
									<th>Driver Not Found</th>
									<th>Total Booking</th>
									<th>OPS Booking</th>
									<th>APP Booking</th>
								</tr>
							</thead>
							<tbody>

							</tbody>
						</table>
					</div>

				</div>
			</div>
		</div>

	</div>

</div>