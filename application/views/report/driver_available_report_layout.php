<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->

		<div class="row">
		<?php
		$form_attr = array (
				'name' => 'driver_available_report_form',
				'id' => 'driver_available_report_form',
				'method' => 'POST' 
		);
		echo form_open_multipart ( base_url ( '' ), $form_attr );
		// driver id by default is -1
		
		?>
	
			<div class="">

				<!-- BEGIN BORDERED TABLE PORTLET-->
				<div class="portlet box green gp-form">
					<div class="portlet-title">
						<div class="caption">Driver Details</div>

					</div>
					<div class="portlet-body">
						
						<div class="row">
							<div class="col-lg-3 input_field_sections nowarp hidden">

                            <?php
																												/* echo form_label ( 'Time Period:', 'timePeriod', array (
																														'class' => '' 
																												) );
																												
																												echo form_dropdown ( 'timePeriod',$time_period_list , '', array (
																														'id' => 'timePeriod',
																														'class' => 'form-control',
																														//'required' => 'required' 
																												) ); */
																												
																												?>

                        	</div>
							<div class="col-lg-3 input_field_sections nowarp">

                            <?php
																												echo form_label ( 'Log Status:', 'logStatus', array (
																														'class' => '' 
																												) );
																												
																												echo form_dropdown ( 'logStatus', $log_status_list, '', array (
																														'id' => 'logStatus',
																														'class' => 'form-control',
																														//'required' => 'required' 
																												) );
																												
																												?>

                        </div>
                        <div class="col-lg-3 input_field_sections nowarp" id="available-status">

                            <?php
																												echo form_label ( 'Available Status:', 'availableStatus', array (
																														'class' => '' 
																												) );
																												
																												echo form_dropdown ( 'availableStatus', $available_status_list, '', array (
																														'id' => 'availableStatus',
																														'class' => 'form-control',
																														//'required' => 'required' 
																												) );
																												
																												?>

                        </div>
						</div>

						<div class="form-actions" style="margin-top: 22px;">
							<div class="row">
								<div class="col-md-12">

									<div class="gp-cen">
										<button type="button" id="driver-available-report-btn"
											class="btn gpblue">Get Report</button>
									</div>
								</div>
							</div>
						</div>

					</div>
					<!-- END BORDERED TABLE PORTLET-->

				</div>

			</div>
			<?php echo form_close(); ?>
		</div>

		<div class="row">
			<div class="">
				<!-- BEGIN BORDERED TABLE PORTLET-->
				<div class="portlet box green gp-form">
					<div class="portlet-title">
						<div class="caption">Driver Available Report</div>

					</div>
					<div class="portlet-body">
						<table
							class="table table-striped table-bordered table-hover table-checkable order-column"
							id="driver-available-data-table" style="width: 99%">
							<thead>
								<tr>
									<th>Driver Code</th>
									<th>Driver Name</th>
									<th>Driver Mobile</th>
									<th>Driver Wallet</th>
									<th>App Version</th>
									<th>Log Status</th>
									<th>Last Logged In</th>
									<th>Availability Status</th>
									<th>Job Card Id</th>
									<th>Taxi No</th>
									<th>Taxi Device Code</th>
									<th>Taxi Device Mobile</th>
									
								</tr>
							</thead>
							<tbody>

							</tbody>
						</table>
					</div>

				</div>
			</div>
		</div>

	</div>

</div>