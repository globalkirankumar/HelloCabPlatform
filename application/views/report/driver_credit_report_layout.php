<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->

		<div class="row">
			<div class="">
				<!-- BEGIN BORDERED TABLE PORTLET-->
				<div class="portlet box green gp-form">
					<div class="portlet-title">
						<div class="caption">Driver Credit Report</div>

					</div>
					<div class="portlet-body">
						<table
							class="table table-striped table-bordered table-hover table-checkable order-column"
							id="driver-credit-data-table" style="width: 99%">
							<thead>
								<tr>
									<th>Sl no</th>
									<th>Action</th>
									<th>Driver Code</th>
									<th>Driver Name</th>
									<th>Driver Mobile</th>
									<th>Bank Account</th>
									<th>Bank Code</th>
									<th>Bank Branch Code</th>
									<th>Currency Code</th>
									<th>Driver Initial</th>
									<th>Driver Initial</th>
									<th>Transaction Type</th>
									<th>Credit Balanace</th>
								</tr>
							</thead>
							<tbody>

							</tbody>
						</table>
					</div>

				</div>
			</div>
		</div>

	</div>

</div>