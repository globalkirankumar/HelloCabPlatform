<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
		<?php
		$form_attr = array (
				'name' => 'promocode_report_form',
				'id' => 'promocode_report_form',
				'method' => 'POST' 
		);
		echo form_open_multipart ( base_url ( '' ), $form_attr );
		// driver id by default is -1
		
		?>
	
			<div class="">

				<!-- BEGIN BORDERED TABLE PORTLET-->
				<div class="portlet box green gp-form">
					<div class="portlet-title">
						<div class="caption">Select Promocode</div>

					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-lg-3 input_field_sections nowarp">

                                <?php
																																echo form_label ( 'PromoCode:', 'promoCode', array (
																																		'class' => 'required' 
																																) );
																																
																																
																																echo form_dropdown ( 'promoCode', $promocode_list, '', array (
																																		'id' => 'promoCode',
																																		'class' => 'form-control',
																																		'required' => 'required'
																																) );
																																?>
                            </div>
							<div class="col-lg-3 input_field_sections nowarp hidden" id="promo-discount-percentage">

                                <?php
																																echo form_label ( 'Promo Discount Percentage:', 'promoDiscountAmount', array (
																																		'class' => '' 
																																) );
																																echo form_input ( array (
																																		'id' => 'promoDiscountAmount',
																																		'name' => 'promoDiscountAmount',
																																		'class' => 'form-control',
																																		'readonly' => 'readonly',
																																		'value' => ''
																																) );
																																
																																?>
                            </div>
                            <div class="col-lg-3 input_field_sections nowarp hidden" id="promo-discount-amount">

                                <?php
																																echo form_label ( 'Promo Discount Amount:', 'promoDiscountAmount2', array (
																																		'class' => '' 
																																) );
																																
																																echo form_input ( array (
																																		'id' => 'promoDiscountAmount2',
																																		'name' => 'promoDiscountAmount2',
																																		'class' => 'form-control',
																																		'readonly' => 'readonly',
																																		'value' => '' 
																																) );
																																
																																?>
                            </div>
                            
                            <div class="col-lg-3 input_field_sections nowarp hidden" id="promocode-type">

                                <?php
																																echo form_label ( 'PromoCode Type:', 'promocodeTypeName', array (
																																		'class' => 'required' 
																																) );
																																
																																
																																echo form_input ( array (
																																		'id' => 'promocodeTypeName',
																																		'name' => 'promocodeTypeName',
																																		'class' => 'form-control',
																																		'readonly' => 'readonly',
																																		'value' => '' 
																																) );
																																?>
                            </div>
                            </div>
						<div class="row">
							<div class="col-lg-3 input_field_sections nowarp hidden" id="zone-name1">

                                <?php
																																echo form_label ( 'Zone Name1:', 'zoneName1', array (
																																		'class' => '' 
																																) );
																																echo form_input ( array (
																																		'id' => 'zoneName1',
																																		'name' => 'zoneName1',
																																		'class' => 'form-control',
																																		'readonly' => 'readonly',
																																		'value' => ''
																																) );
																																
																																?>
                            </div>
                            <div class="col-lg-3 input_field_sections nowarp hidden" id="zone-name2">

                                <?php
																																echo form_label ( 'Zone Name2:', 'zoneName2', array (
																																		'class' => '' 
																																) );
																																
																																echo form_input ( array (
																																		'id' => 'zoneName2',
																																		'name' => 'zoneName2',
																																		'class' => 'form-control',
																																		'readonly' => 'readonly',
																																		'value' => '' 
																																) );
																																
																																?>
                            </div>
                            <div class="col-lg-3 input_field_sections nowarp hidden" id="promo-start-datetime">

                                <?php
																																echo form_label ( 'Promo Start Datetime:', 'promoStartDatetime', array (
																																		'class' => '' 
																																) );
																																
																																echo form_input ( array (
																																		'id' => 'promoStartDatetime',
																																		'name' => 'promoStartDatetime',
																																		'class' => 'form-control',
																																		'readonly' => 'readonly',
																																		'value' => '' 
																																) );
																																
																																?>
                            </div>
                            <div class="col-lg-3 input_field_sections nowarp hidden" id="promo-end-datetime">

                                <?php
																																echo form_label ( 'Promo End Datetime:', 'promoEndDatetime', array (
																																		'class' => '' 
																																) );
																																
																																echo form_input ( array (
																																		'id' => 'promoEndDatetime',
																																		'name' => 'promoEndDatetime',
																																		'class' => 'form-control',
																																		'readonly' => 'readonly',
																																		'value' => '' 
																																) );
																																
																																?>
                            </div>
                            
						</div>
						<div class="row">
							
                            <div class="col-lg-3 input_field_sections nowarp hidden" id="promo-code-limit">

                                <?php
																																echo form_label ( 'Promo Code Limit:', 'promoCodeLimit', array (
																																		'class' => '' 
																																) );
																																
																																echo form_input ( array (
																																		'id' => 'promoCodeLimit',
																																		'name' => 'promoCodeLimit',
																																		'class' => 'form-control',
																																		'readonly' => 'readonly',
																																		'value' => '' 
																																) );
																																
																																?>
                            </div>
                            <div class="col-lg-3 input_field_sections nowarp hidden" id="promo-code-user-limit">

                                <?php
																																echo form_label ( 'Promo Code User Limit:', 'promoCodeUserLimit', array (
																																		'class' => '' 
																																) );
																																
																																echo form_input ( array (
																																		'id' => 'promoCodeUserLimit',
																																		'name' => 'promoCodeUserLimit',
																																		'class' => 'form-control',
																																		'readonly' => 'readonly',
																																		'value' => '' 
																																) );
																																
																																?>
                            </div>
                            <div class="col-lg-3 input_field_sections nowarp hidden" id="promo-entity">

                                <?php
																																echo form_label ( 'Entity:', 'entityName', array (
																																		'class' => '' 
																																) );
																																
																																echo form_input ( array (
																																		'id' => 'entityName',
																																		'name' => 'entityName',
																																		'class' => 'form-control',
																																		'readonly' => 'readonly',
																																		'value' => '' 
																																) );
																																
																																?>
                            </div>
						</div>

						<div class="form-actions" style="margin-top: 22px;">
							<div class="row">
								<div class="col-md-12">

									<div class="gp-cen">
										<button type="button" id="promocode-report-btn"
											class="btn gpblue">Get Report</button>
									</div>
								</div>
							</div>
						</div>

					</div>
					<!-- END BORDERED TABLE PORTLET-->

				</div>

			</div>
			<?php echo form_close(); ?>
		</div>
		<div class="row">
			<div class="">
				<!-- BEGIN BORDERED TABLE PORTLET-->
				<div class="portlet box green gp-form">
					<div class="portlet-title">
						<div class="caption">Promocode Report</div>

					</div>
					<div class="portlet-body">
						<table
							class="table table-striped table-bordered table-hover table-checkable order-column"
							id="promocode-data-table" style="width: 99%">
							<thead>
								<tr>
									<th>Invoice No</th>
									<th>Job Card Id</th>
									<th>Total Trip Bill</th>
									<th>Trip Promo Discount</th>
									<th>Paid Trip Bill</th>
									<th>Passenger Code</th>
									<th>Passenger Name</th>
									<th>Driver Code</th>
									<th>Driver Name</th>
								</tr>
							</thead>
							<tbody>

							</tbody>
						</table>
					</div>

				</div>
			</div>
		</div>

	</div>

</div>