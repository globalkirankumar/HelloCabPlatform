<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->

		<div class="row">
			<div class="">
				<!-- BEGIN BORDERED TABLE PORTLET-->
				<div class="portlet box green gp-form">
					<div class="portlet-title">
						<div class="caption">Driver Taxi Mapping Report</div>

					</div>
					<div class="portlet-body">
						<table
							class="table table-striped table-bordered table-hover table-checkable order-column"
							id="driver-taxi-mapping-data-table" style="width: 99%">
							<thead>
								<tr>
									<th>Driver Code</th>
									<th>Driver Name</th>
									<th>Driver Mobile</th>
									<th>Zone</th>
									<th>Taxi No</th>
									<th>Total Completed Trip</th>
									<th>Total Rejected Trip</th>
									<th>Weekly Completed Trip</th>
									<th>Weekly Rejected Trip</th>
									<th>Taxi Owner Code</th>
									<th>Taxi Owner Name</th>
									<th>Taxi Owner Mobile</th>
									<th>Taxi Device Code</th>
									<th>Taxi Device IMEI</th>
									<th>Taxi Device Mobile</th>
								</tr>
							</thead>
							<tbody>

							</tbody>
						</table>
					</div>

				</div>
			</div>
		</div>

	</div>

</div>