<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->

		<div class="row">
		<?php
		$form_attr = array (
				'name' => 'driver_transaction_report_form',
				'id' => 'driver_transaction_report_form',
				'method' => 'POST' 
		);
		echo form_open_multipart ( base_url ( '' ), $form_attr );
		// driver id by default is -1
		
		?>
	
			<div class="">

				<!-- BEGIN BORDERED TABLE PORTLET-->
				<div class="portlet box green gp-form">
					<div class="portlet-title">
						<div class="caption">Driver Details</div>

					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-lg-3 input_field_sections nowarp">

                                <?php
																																echo form_label ( 'Start date:', 'startDate', array (
																																		'class' => 'required' 
																																) );
																																
																																echo form_input ( array (
																																		'id' => 'startDate',
																																		'name' => 'startDate',
																																		'class' => 'form-control',
																																		'required' => 'required',
																																		'placeholder' => 'Start Date',
																																		'value' => '' 
																																) );
																																
																																?>
                            </div>
							<div class="col-lg-3 input_field_sections nowarp">

                                <?php
																																echo form_label ( 'End Date:', 'endDate', array (
																																		'class' => 'required' 
																																) );
																																
																																echo form_input ( array (
																																		'id' => 'endDate',
																																		'name' => 'endDate',
																																		'class' => 'form-control',
																																		'required' => 'required',
																																		'placeholder' => 'End Date',
																																		'value' => '' 
																																) );
																																
																																?>
                            </div>
                            <div class="col-lg-3 input_field_sections nowarp">

                                <?php
																																echo form_label ( 'Driver:', 'driverDetail', array (
																																		'class' => '' 
																																) );
																																
																																echo form_input ( array (
																																		'id' => 'driverDetail',
																																		'name' => 'driverDetail',
																																		'class' => 'form-control',
																																		'readyonly' => 'readyonly',
																																		//'required' => 'required',
																																		'placeholder' => 'Search by Name/Mobile/Drivercode',
																																		'value' => '' 
																																) );
																																echo form_input ( array (
																																		'type' => 'hidden',
																																		'id' => 'driverId',
																																		'name' => 'driverId',
																																		//'required' => 'required',
																																		'class' => 'form-control',
																																		'value' => '' 
																																) );
																																echo '<div id="driver-suggesstion-box"></div>';
																																?>
                            </div>
						</div>
						<div class="row">
							
							
							<div class="col-lg-3 input_field_sections nowarp">

                            <?php
																												echo form_label ( 'Transaction From:', 'transactionFrom', array (
																														'class' => '' 
																												) );
																												
																												echo form_dropdown ( 'transactionFrom', $transaction_from_list, '', array (
																														'id' => 'transactionFrom',
																														'class' => 'form-control',
																														//'required' => 'required' 
																												) );
																												
																												?>

                        </div>
							<div class="col-lg-3 input_field_sections nowarp">

                            <?php
																												echo form_label ( 'Transaction Mode:', 'transactionMode', array (
																														'class' => '' 
																												) );
																												
																												echo form_dropdown ( 'transactionMode', $transaction_mode_list, '', array (
																														'id' => 'transactionMode',
																														'class' => 'form-control',
																														//'required' => 'required' 
																												) );
																												
																												?>

                        </div>
                        <div class="col-lg-3 input_field_sections nowarp">

                            <?php
																												echo form_label ( 'Transaction Type:', 'transactionType', array (
																														'class' => '' 
																												) );
																												
																												echo form_dropdown ( 'transactionType', $transaction_type_list, '', array (
																														'id' => 'transactionType',
																														'class' => 'form-control',
																														//'required' => 'required' 
																												) );
																												
																												?>

                        </div>
                        <div class="col-lg-3 input_field_sections nowarp">

                            <?php
																												echo form_label ( 'Zone:', 'zoneId', array (
																														'class' => '' 
																												) );
																												
																												echo form_dropdown ( 'zoneId', $zone_list, '', array (
																														'id' => 'zoneId',
																														'class' => 'form-control',
																														//'required' => 'required' 
																												) );
																												
																												?>

                        </div>
						</div>

						<div class="form-actions" style="margin-top: 22px;">
							<div class="row">
								<div class="col-md-12">

									<div class="gp-cen">
										<button type="button" id="driver-transaction-report-btn"
											class="btn gpblue">Get Report</button>
									</div>
								</div>
							</div>
						</div>

					</div>
					<!-- END BORDERED TABLE PORTLET-->

				</div>

			</div>
			<?php echo form_close(); ?>
		</div>

		<div class="row">
			<div class="">
				<!-- BEGIN BORDERED TABLE PORTLET-->
				<div class="portlet box green gp-form">
					<div class="portlet-title">
						<div class="caption">Driver Transaction Report</div>

					</div>
					<div class="portlet-body">
						<table
							class="table table-striped table-bordered table-hover table-checkable order-column"
							id="driver-transaction-data-table" style="width: 99%">
							<thead>
								<tr>
									<th>Sl No</th>
									<th>Bank Account No</th>
									<th>Driver Code</th>
									<th>Driver Name</th>
									<th>Driver Mobile</th>
									<th>Taxi No</th>
									<th>Job Card Id</th>
									<th>Transaction Mode</th>
									<th>Previous Amount</th>
									<th>Transaction Amount</th>
									<th>Current Amount</th>
									<th>Transaction Status</th>
									<th>Transaction From</th>
									<th>Transaction Type</th>
									<th>Used Promocode</th>
									<th>Transaction Datetime</th>
									<th>Transaction Id</th>
									<th>Comments</th>
									<th>Zone</th>

								</tr>
							</thead>
							<tbody>

							</tbody>
						</table>
					</div>

				</div>
			</div>
		</div>

	</div>

</div>