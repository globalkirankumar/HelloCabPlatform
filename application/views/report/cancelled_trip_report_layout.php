<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
		<?php
		$form_attr = array (
				'name' => 'cancelled_trip_report_form',
				'id' => 'cancelled_trip_report_form',
				'method' => 'POST' 
		);
		echo form_open_multipart ( base_url ( '' ), $form_attr );
		// driver id by default is -1
		
		?>
	
			<div class="">

				<!-- BEGIN BORDERED TABLE PORTLET-->
				<div class="portlet box green gp-form">
					<div class="portlet-title">
						<div class="caption">Trip Details</div>

					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-lg-3 input_field_sections nowarp">

                                <?php
																																echo form_label ( 'Start date:', 'startDate', array (
																																		'class' => 'required' 
																																) );
																																
																																echo form_input ( array (
																																		'id' => 'startDate',
																																		'name' => 'startDate',
																																		'class' => 'form-control',
																																		'required' => 'required',
																																		'placeholder' => 'Start Date',
																																		'value' => '' 
																																) );
																																
																																?>
                            </div>
							<div class="col-lg-3 input_field_sections nowarp">

                                <?php
																																echo form_label ( 'End Date:', 'endDate', array (
																																		'class' => 'required' 
																																) );
																																
																																echo form_input ( array (
																																		'id' => 'endDate',
																																		'name' => 'endDate',
																																		'class' => 'form-control',
																																		'required' => 'required',
																																		'placeholder' => 'End Date',
																																		'value' => '' 
																																) );
																																
																																?>
                            </div>
                            
						</div>
						<div class="row">
							
							
							<div class="col-lg-3 input_field_sections nowarp">

                            <?php
																												echo form_label ( 'Trip Type:', 'tripType', array (
																														'class' => '' 
																												) );
																												
																												echo form_dropdown ( 'tripType', $trip_type_list, '', array (
																														'id' => 'tripType',
																														'class' => 'form-control',
																														//'required' => 'required' 
																												) );
																												
																												?>

                        </div>
							<div class="col-lg-3 input_field_sections nowarp">

                            <?php
																												echo form_label ( 'Trip Status:', 'tripStatus', array (
																														'class' => '' 
																												) );
																												
																												echo form_dropdown ( 'tripStatus', $trip_status_list, '', array (
																														'id' => 'tripStatus',
																														'class' => 'form-control',
																														//'required' => 'required' 
																												) );
																												
																												?>

                        </div>
         
						</div>

						<div class="form-actions" style="margin-top: 22px;">
							<div class="row">
								<div class="col-md-12">

									<div class="gp-cen">
										<button type="button" id="cancelled-trip-report-btn"
											class="btn gpblue">Get Report</button>
									</div>
								</div>
							</div>
						</div>

					</div>
					<!-- END BORDERED TABLE PORTLET-->

				</div>

			</div>
			<?php echo form_close(); ?>
		</div>
		<div class="row">
			<div class="">
				<!-- BEGIN BORDERED TABLE PORTLET-->
				<div class="portlet box green gp-form">
					<div class="portlet-title">
						<div class="caption">Cancelled Trip Report</div>

					</div>
					<div class="portlet-body">
						<table
							class="table table-striped table-bordered table-hover table-checkable order-column"
							id="cancelled-trip-data-table" style="width: 99%">
							<thead>
								<tr>
									<th>Job Card Id</th>
									<th>Driver Name</th>
									<th>Trip Type</th>
									<th>Trip Status</th>
									<th>Rejected Drivers</th>
									<th>Request Pickup Datetime</th>
									<th>Dispatched Datetime</th>
									<th>Last Cron Job</th>
								</tr>
							</thead>
							<tbody>

							</tbody>
						</table>
					</div>

				</div>
			</div>
		</div>

	</div>

</div>