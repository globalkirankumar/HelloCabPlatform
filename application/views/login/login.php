<!DOCTYPE html>

<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Hello Cabs | Login</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />

        <meta content="" name="author" />

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo css_url('app/font-awesome.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo css_url('app/simple-line-icons.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo css_url('app/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo css_url('app/bootstrap-switch.min.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?php echo css_url('app/select2.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo css_url('app/select2-bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->

        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo css_url('app/components.css'); ?>" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo css_url('app/plugins.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->

        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?php echo css_url('app/login-2.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->

        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->


        <!-- BEGIN CORE PLUGINS -->
        <script src="<?php echo js_url('jquery-3.2.1.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo js_url('bootstrap.min.js'); ?>" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php echo js_url('jquery.validate.js'); ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->

        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo js_url('app/login.js'); ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
    </head>
    <!-- END HEAD -->

    <body class=" login">

        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="index.html">
                <img src="<?php echo image_url('app/logo.png') ?>" alt="" /> </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <?php
            $form_attr = array(
                'name' => 'login_form',
                'class' => 'login-form',
                'id' => 'login_form',
                'method' => 'POST',
                'class' => 'login_validator',
                'action'=> ''
            );
            echo form_open('',$form_attr);
            ?>

            <div class="form-title">
                <span class="form-title">LOGIN TO HELLO CABS TDS.</span>

            </div>
            <div class="alert alert-danger display-hide">
                <button class="close" data-close="alert"></button>
                <span> Enter any username and password. </span>
            </div>
            <div class="form-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <!-- <label class="control-label visible-ie8 visible-ie9">Username</label> -->
                <?php
                echo form_label('Username', 'loginEmail', array(
                    'class' => 'control-label visible-ie8 visible-ie9'
                ));
                echo form_input(array(
                    'id' => 'loginEmail',
                    'name' => 'loginEmail',
                    'class' => 'form-control form-control-solid placeholder-no-fix',
                    'required' => 'required',
                    //'pattern' => '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$',
                    'placeholder' => 'UserName'
                ));
                ?>
                <?php
                ?> 
                <!-- <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username" />  --></div>
            <div class="form-group">
                <!-- <label class="control-label visible-ie8 visible-ie9">Password</label> -->
<?php
echo form_label('Password', 'password', array(
    'class' => 'control-label visible-ie8 visible-ie9'
));

echo form_input(array(
    'id' => 'password',
    'name' => 'password',
    'type' => 'password',
    'class' => 'form-control form-control-solid placeholder-no-fix',
    'required' => 'required',
    'placeholder' => 'Password'
));
?>
               <!--  <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" />  --></div>


            <div class="form-actions">

                <div class="pull-left">
                    <a href="#" id="forget-password" class="forget-password">Forgot Password?</a>
                </div>

                <div class="pull-right forget-password-block">
                    <button type="button" id="login_btn" class="btn red btn-block uppercase">Login</button>
                </div>

            </div>




<?php
echo form_close();
?>
            <!-- END LOGIN FORM -->
            <!-- BEGIN FORGOT PASSWORD FORM -->
            <?php
            $form_attr = array(
                'name' => 'user_reset_from',
                'class' => 'forget-form',
                'id' => 'user_reset_from',
                'method' => 'POST'
            );
            echo form_open(base_url(''), $form_attr);
            ?>

            <div class="form-title">
                <span class="form-title">Forget Password ?</span>
                <span class="form-subtitle">Enter your e-mail to reset it.</span>
            </div>
            <div class="form-group">
<?php
echo form_input(array(
    'id' => 'resetEmail',
    'name' => 'resetEmail',
    'class' => 'form-control placeholder-no-fix',
    'required' => 'required',
    //'pattern' => '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$',
    'placeholder' => 'EmailId'
));
?> 
               <!--  <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" /> --> </div>
            <div class="form-actions">
                <button type="button" id="back-btn" class="btn btn-default">Back</button>
                <button type="submit" class="btn btn-primary uppercase pull-right">Submit</button>
            </div>
                <?php
                echo form_close();
                ?>
            <!-- END FORGOT PASSWORD FORM -->

        </div>
        <div class="copyright hide"> 2017 � Invenzolabs India Pvt. Ltd. </div>
        <!-- END LOGIN -->
    </body>

</html>