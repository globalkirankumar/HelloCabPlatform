<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="UTF-8">
<title>Take A Right</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--global styles-->
<link href="<?php echo css_url('app/components.css'); ?>"
	rel="stylesheet">
<link href="<?php echo css_url('app/custom.css'); ?>" rel="stylesheet">
<!-- end of global styles-->

<!-- Vendor StyleSheet -->
<link href="<?php echo css_url('vendors/c3/css/c3.min.css'); ?>"
	rel="stylesheet">
<link href="<?php echo css_url('vendors/toastr/css/toastr.min.css'); ?>"
	rel="stylesheet">
<link
	href="<?php echo css_url('vendors/switchery/css/switchery.min.css'); ?>"
	rel="stylesheet">
<link href="<?php echo css_url('app/pages/new_dashboard.css'); ?>"
	rel="stylesheet">

<!-- global scripts-->
<script src="<?php echo js_url('jquery-3.2.1.min.js'); ?>"></script>
<script src="<?php echo js_url('components.js'); ?>"></script>
<script src="<?php echo js_url('custom.js'); ?>"></script>

<!-- global scripts end-->
<script src="<?php echo js_url('app/login.js'); ?>"></script>
</head>

<body class="fixed_menu login-bg">
	<div class="preloader"
		style="position: fixed; width: 100%; height: 100%; top: 0; left: 0; z-index: 100000; backface-visibility: hidden; background: #000;">
		<div class="preloader_img"
			style="width: 220px; height: 200px; position: absolute; left: 40%; top: 48%; background-position: center; z-index: 999999">
			<img src="<?php echo image_url('app/loader.gif"')?>"
				style="width: 100%;" alt="loading...">
		</div>
	</div>

	<div class="container wow fadeInDown login-panel" data-wow-delay="0.5s"
		data-wow-duration="2s">
		<div class="row">
			<div
				class="col-lg-8 push-lg-2 col-md-10 push-md-1 col-sm-10 push-sm-1 login_top_bottom">
				<div class="row">
					<div class="col-lg-8 push-lg-2 col-md-10 push-md-1 col-sm-12">
						<div class="login_logo login_border_radius1">
							<h3 class="text-xs-center">
								<img src="<?php echo image_url('app/logo.png')?>"
									alt="TakeARight" class="TakeARight">
							</h3>
						</div>
						<div id="loginblock" class="bg-black login_content login_border_radius">
                    		<?php
								$form_attr = array (
										'name' => 'login_form',
										'id' => 'login_form',
										'method' => 'POST',
										'class' => 'login_validator' 
								);
								echo form_open ( base_url ( '' ), $form_attr );
								
							?>
                            <!--</h3>-->
                            <span class="has-error"><h4 id="login-msg"></h4></span>
							<div class="form-group">
                            <?php
								echo form_label ( 'Email/Mobile:', 'loginEmail', array (
										'class' => 'form-control-label required' 
								) );
							?>
							    <div class="input-group">
									<span class="input-group-addon input_email"><i
										class="fa fa-envelope"></i></span>
                        		<?php
									echo form_input ( array (
											'id' => 'loginEmail',
											'name' => 'loginEmail',
											'class' => 'form-control form-control-md',
											'required' => 'required',
											//'pattern' => '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$',
											'placeholder' => 'EmailId/Mobile' 
									) );
								?>                                
								</div>
							</div>
							<div class="form-group">
                           <?php
							 	echo form_label ( 'Password:', 'password', array (
									'class' => 'form-control-label required' 
								) );
							?>
                                <div class="input-group">
									<span class="input-group-addon addon_password"><i
										class="fa fa-lock"></i></span>
                          <?php
								echo form_input ( array (
												'id' => 'password',
												'name' => 'password',
												'type' => 'password',
												'class' => 'form-control form-control-md',
												'required' => 'required',
												'placeholder' => 'Password' 
										) );
										
							?>   
								</div>
							</div>

							<div class="form-group">
								<div class="row">
									<div class="col-lg-12">
										<button type="button" id="login_btn" class="btn btn-primary btn-block login_button">Log In</button>
									</div>
								</div>
							</div>
                        <?php
							echo form_close ();
						?>
                        <div class="form-group">
								<div class="row">
									<div class="text-xs-center forgot_pwd">
										<div id="resetpassword"
											class="custom-control-description forgottxt_clr">Reset Your
											password?
										</div>
									</div>
								</div>
						</div>
						</div>
						<div id="resetblock" style="display: none;"
							class="bg-black login_content login_border_radius">
                        <?php
							$form_attr = array (
									'name' => 'user_reset_from',
									'id' => 'user_reset_from',
									'method' => 'POST',
									'class' => 'form-horizontal form-label-left' 
							);
							echo form_open ( base_url ( '' ), $form_attr );
							
						?>
						<span class="has-error"><h4 id="reset-msg"></h4></span>
						
                            <div class="form-group">
                                  <?php
									echo form_label ( 'Please enter your Email/Mobile to reset the password:', 'email', 
											array ('class' => 'form-control-label required') );
								  ?>
                                <div class="input-group">
									<span class="input-group-addon addon_email"><i
										class="fa fa-envelope"></i></span>
                                     <?php
																																					
											echo form_input ( array (
													'id' => 'resetEmail',
													'name' => 'resetEmail',
													'class' => 'form-control email_forgot form-control-md',
													'required' => 'required',
													//'pattern' => '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$',
													'placeholder' => 'EmailId/Mobile' 
											) );
											
									?>     
                                   
                                </div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-lg-12">
										<button type="button" id="reset_btn" class="btn btn-primary btn-block login_button">Submit</button>
									</div>
								</div>
							</div>
							
							
            <?php
												echo form_close ();
												?>                   
												 <div class="form-group">
								<div class="row">
									<div class="text-xs-center forgot_pwd">
										<div id="signin"
											class="custom-control-description forgottxt_clr">Sign In
										</div>
									</div>
								</div>
						</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /#wrap -->
	<script>

</script>
</body>
</html>