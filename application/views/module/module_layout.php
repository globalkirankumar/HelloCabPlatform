<?php
$view_mode = $mode;


$form_attr = array (
		'name' => 'edit_module_access_form',
		'id' => 'edit_module_access_form',
		'method' => 'POST'
);
echo form_open_multipart ( base_url ( '' ), $form_attr );
// driver id by default is -1

?>
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->





		<div class="row">
			<div class="">
				<!-- BEGIN BORDERED TABLE PORTLET-->
				<div class="portlet box green gp-form">
                <div class="portlet-title">
                                        <div class="caption">Role privilege </div>
                                       
                                    </div>
				<!--<div class="card-header bg-white">
                        <h1 style="padding-left:15px;">Role privilege</h1>
                    </div>-->
					
					<div class="portlet-body  gp-cen1">
                    <label for="name">Select Role Type</label>
						<?php
                                 if ($view_mode == EDIT_MODE) {
                                    echo form_dropdown('roleType', $role_type_list, '', array(
                                        'id' => 'roleType',
                                        'class' => 'form-control',
                                        'required' => 'required'
                                    ));
                                } 
                                ?>	
					</div>
					<div class="portlet-body gp-bgnone" id="module-information">
						
					</div>
				</div>
				<!-- END BORDERED TABLE PORTLET-->
			</div>
		</div>


	</div>

</div>