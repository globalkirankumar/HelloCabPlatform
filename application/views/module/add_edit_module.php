
<div class="table-scrollable">
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>Module</th>
				<th>Access Permission</th>
				<th>Create Permission</th>
				<th>Update Permission</th>
				<th>Read Permission</th>
				<th>Delete Permission</th>
				<th>Other Permission</th>
				
			</tr>
		</thead>
		<tbody>
		<?php $i=0;
		foreach ($module_access_model as $list): ?>
			<tr>
				<td><?php echo $list->moduleName;
				echo form_input ( array (
						'type' => 'hidden',
						'id' => 'moduleAccessId',
						'name' => 'module[' . $i . '][moduleAccessId]',
						'value' => ($list->get ( 'moduleAccessId' )) ? $list->get ( 'moduleAccessId' ) : - 1
				) );
				echo form_input ( array (
						'type' => 'hidden',
						'id' => 'moduleId',
						'name' => 'module[' . $i . '][moduleId]',
						'value' => ($list->get ( 'moduleId' )) ? $list->get ( 'moduleId' ) : - 1
				) );
				
				?></td>
				<td><label class="mt-checkbox mt-checkbox-outline">
                                        <?php
			$is_checked = $list->get ( 'isAccess' ) ? TRUE : FALSE;
			$checked = $list->get ( 'isAccess' ) ? 1 : 0;
			
			echo form_checkbox ( array (
					'id' => 'isAccess-'.$i,
					'name' => 'module[' . $i . '][isAccess]'
					
			), $checked, $is_checked );
			
			?>
                                        <span></span>
				</label></td>

				<td><label class="mt-checkbox mt-checkbox-outline">
                                        <?php
			$is_checked = $list->get ( 'isCreate' ) ? TRUE : FALSE;
			$checked = $list->get ( 'isCreate' ) ? 1 : 0;
			if ($list->get ( 'isAccess' ))
			{
			echo form_checkbox ( array (
					'id' => 'isCreate-'.$i,
					'name' => 'module[' . $i . '][isCreate]'
			), $checked, $is_checked );
			}
			else
			{
				echo form_checkbox ( array (
						'id' => 'isCreate-'.$i,
						'name' => 'module[' . $i . '][isCreate]',
						'disabled'=>'disabled'
				), $checked, $is_checked );
			}
			?>
                                        <span></span>
				</label></td>

				<td><label class="mt-checkbox mt-checkbox-outline">
                                        <?php
			$is_checked = $list->get ( 'isWrite' ) ? TRUE : FALSE;
			$checked = $list->get ( 'isWrite' ) ? 1 : 0;
			if ($list->get ( 'isAccess' ))
			{
			echo form_checkbox ( array (
					'id' => 'isWrite-'.$i,
					'name' => 'module[' . $i . '][isWrite]'
			), $checked, $is_checked );
			}
			else 
			{
				echo form_checkbox ( array (
						'id' => 'isWrite-'.$i,
						'name' => 'module[' . $i . '][isWrite]',
						'disabled'=>'disabled'
				), $checked, $is_checked );
			}
			?>
                                        <span></span>
				</label></td>

				<td><label class="mt-checkbox mt-checkbox-outline">
                                        <?php
			$is_checked = $list->get ( 'isRead' ) ? TRUE : FALSE;
			$checked = $list->get ( 'isRead' ) ? 1 : 0;
			
			echo form_checkbox ( array (
					'id' => 'isRead-'.$i,
					'name' => 'module[' . $i . '][isRead]',
					'disabled'=>'disabled',
			), $checked, $is_checked );
			
			?>
                                        <span></span>
				</label></td>

				<td><label class="mt-checkbox mt-checkbox-outline">
                                        <?php
			$is_checked = $list->get ( 'isDelete' ) ? TRUE : FALSE;
			$checked = $list->get ( 'isDelete' ) ? 1 : 0;
			if ($list->get ( 'isAccess' ))
			{
			echo form_checkbox ( array (
					'id' => 'isDelete-'.$i,
					'name' => 'module[' . $i . '][isDelete]'
			), $checked, $is_checked );
			}
			else 
			{
				echo form_checkbox ( array (
						'id' => 'isDelete-'.$i,
						'name' => 'module[' . $i . '][isDelete]',
						'disabled'=>'disabled'
				), $checked, $is_checked );
			}
			?>
                                        <span></span>
				</label></td>
				<td><label class="mt-checkbox mt-checkbox-outline">
                                        <?php
			$is_checked = $list->get ( 'isFullAccess' ) ? TRUE : FALSE;
			$checked = $list->get ( 'isFullAccess' ) ? 1 : 0;
			if ($list->get ( 'isAccess' ))
			{
			echo form_checkbox ( array (
					'id' => 'isFullAccess-'.$i,
					'name' => 'module[' . $i . '][isFullAccess]'
			), $checked, $is_checked );
			}
			else 
			{
				echo form_checkbox ( array (
						'id' => 'isFullAccess-'.$i,
						'name' => 'module[' . $i . '][isFullAccess]',
						'disabled'=>'disabled'
				), $checked, $is_checked );
			}
			?>
                                        <span></span>
				</label></td>
				
			</tr>
<?php $i++; endforeach; ?>


		</tbody>
	</table>
	

</div>
<div class="form-actions" style="margin-top: 22px;">
						<div class="row">
							<div class="col-md-12">
                                           
                                                <div class="gp-cen">
									<button type="button" id="cancel-module-access-btn"
										class="btn grey-salsa btn-outline">Cancel</button>
									<button type="button" id="save-module-access-btn"
										class="btn gpblue">Save</button>

								</div>
                                            
                                        </div>
						</div>
					</div>