<style>
.label.label-sm {
	font-size: 13px;
	padding: 2px 5px;
	cursor: pointer;
}
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">

		<div class="page-bar ">
			<ul class="page-breadcrumb ">
				<li><a href="<?php echo base_url('dashboard');?>">Home</a> <i
					class="fa fa-circle"></i></li>
				<li><a href="<?php echo base_url('TripTransaction/getTripTransactionList');;?>">Transaction List</a></li>

			</ul>

		</div>
		<!---- Page Bar ends ---->
		<div class="">

			<!-- BEGIN BORDERED TABLE PORTLET-->
			<div class="portlet box green gp-form">
				<div class="portlet-title">
					<div class="caption">Trip Completed Details</div>

				</div>
				<div class="portlet-body">
					<div class="row">
						<div class="col-lg-3 input_field_sections nowarp">

                                <?php
																																echo form_label ( 'Start date:', 'startDate', array (
																																		'class' => '' 
																																) );
																																
																																echo form_input ( array (
																																		'id' => 'startDate',
																																		'name' => 'startDate',
																																		'class' => 'form-control',
																																		
																																		// 'required' => 'required',
																																		'placeholder' => 'Start Date',
																																		'value' => '' 
																																) );
																																
																																?>
                            </div>
						<div class="col-lg-3 input_field_sections nowarp">

                                <?php
																																echo form_label ( 'End Date:', 'endDate', array (
																																		'class' => '' 
																																) );
																																
																																echo form_input ( array (
																																		'id' => 'endDate',
																																		'name' => 'endDate',
																																		'class' => 'form-control',
																																		
																																		// 'required' => 'required',
																																		'placeholder' => 'End Date',
																																		'value' => '' 
																																) );
																																
																																?>
                            </div>
                            <div class="col-lg-3 input_field_sections nowarp">

                            <?php
																												echo form_label ( 'Entity:', 'entityId', array (
																														'class' => '' 
																												) );
																												
																												echo form_dropdown ( 'entityId', $entity_list, '', array (
																														'id' => 'entityId',
																														'class' => 'form-control' 
																												) );
																												
																												?>

                        </div>
						<div class="col-lg-3 input_field_sections nowarp">

                            <?php
																												echo form_label ( 'Zone:', 'zoneListId', array (
																														'class' => '' 
																												) );
																												// debug_exit($zone_list);
																												echo form_dropdown ( 'zoneListId', $zone_list, '', array (
																														'id' => 'zoneListId',
																														'class' => 'form-control' 
																												) );
																												
																												?>

                        </div>
					</div>
					
					<div class="form-actions" style="margin-top: 22px;">
						<div class="row">
							<div class="col-md-12">

								<div class="gp-cen">
									<button type="button" id="transaction-details-btn" class="btn gpblue">Get
										Transaction List</button>
								</div>
							</div>
						</div>
					</div>

				</div>
				<!-- END BORDERED TABLE PORTLET-->

			</div>

		</div>
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-comments"></i>Trip Completed List
						</div>

					</div>

					<div class="portlet-body">
						<div class="table-toolbar">
							<div class="row">
								<div class="col-md-6">
									<div class="col-md-3 btn-group" style="visibility:hidden;">
										<a href="<?php echo base_url('trip/add');?>">
											<button id="sample_editable_1_new" class="btn sbold green1">
												Add New <i class="fa fa-plus"></i>
											</button>
										</a>
									</div>
									
								</div>
								
							</div>
						</div>
						
						<div id="table-columns" class="hidden">
							<label class="">
                                            <?php
																																												echo form_checkbox ( array (
																																														'class' => 'flat toggle-vis',
																																														'data-column' => '4' 
																																												) );
																																												?>
                                        Requested Pickup Datetime
                                    </label>
                             <label class="">
                                            <?php
																																												echo form_checkbox ( array (
																																														'class' => 'flat toggle-vis',
																																														'data-column' => '5' 
																																												) );
																																												?>
                                        Driver Dispatched Datetime
                                    </label>
                              <label class="">
                                            <?php
																																												echo form_checkbox ( array (
																																														'class' => 'flat toggle-vis',
																																														'data-column' => '6' 
																																												) );
																																												?>
                                        Driver Accepted Datetime
                                    </label>
                               <label class="">
                                            <?php
																																												echo form_checkbox ( array (
																																														'class' => 'flat toggle-vis',
																																														'data-column' => '7' 
																																												) );
																																												?>
                                        Driver Arrived Datetime
                                    </label>
							<label class="">
                                            <?php
																																												echo form_checkbox ( array (
																																														'class' => 'flat toggle-vis',
																																														'data-column' => '12' 
																																												) );
																																												?>
                                        Taxi Type
                                    </label> <label class="">
                                        <?php
																																								echo form_checkbox ( array (
																																										'class' => 'flat toggle-vis',
																																										'data-column' => '13' 
																																								) );
																																								?>
                                        Trip Type
                                    </label> 
                                    <label class="">
                                        <?php
																																								echo form_checkbox ( array (
																																										'class' => 'flat toggle-vis',
																																										'data-column' => '14' 
																																								) );
																																								?>
                                        Booked From
                                    </label>
                                    <label class="">
                                        <?php
																																								echo form_checkbox ( array (
																																										'class' => 'flat toggle-vis',
																																										'data-column' => '15' 
																																								) );
																																								?>
                                        Payment Mode
                                    </label> 
                                    <!-- <label class="">
                                               <?php
																																															//echo form_checkbox ( array ('class' => 'flat toggle-vis','data-column' => '19'));
																																															?>
                                         Travelled Distance
                                    </label> <label class="">
                                        <?php
																																								//echo form_checkbox ( array ('class' => 'flat toggle-vis','data-column' => '20'));
																																								?>
                                        Travelled Period
                                    </label> --> 
                                    <label class="">
                                        <?php
																																								echo form_checkbox ( array (
																																										'class' => 'flat toggle-vis',
																																										'data-column' => '26' 
																																								) );
																																								?>
                                        Promo Discount
                                    </label> <label class="">
                                        <?php
																																								echo form_checkbox ( array (
																																										'class' => 'flat toggle-vis',
																																										'data-column' => '27' 
																																								) );
																																								?>
                                        Wallet Payment
                                    </label> <label class="">
                                        <?php
																																								echo form_checkbox ( array (
																																										'class' => 'flat toggle-vis',
																																										'data-column' => '28' 
																																								) );
																																								?>
                                       Booking Charge
                                    </label> <label class="">
                                        <?php
																																								echo form_checkbox ( array (
																																										'class' => 'flat toggle-vis',
																																										'data-column' => '29' 
																																								) );
																																								?>
                                        Penalty Charge
                                    </label> 
                                    <label class="">
                                        <?php
																																								echo form_checkbox ( array (
																																										'class' => 'flat toggle-vis',
																																										'data-column' => '30' 
																																								) );
																																								?>
                                        Admin Amount
                                    </label> <label class="">
                                        <?php
																																								echo form_checkbox ( array (
																																										'class' => 'flat toggle-vis',
																																										'data-column' => '31' 
																																								) );
																																								?>
                                       Driver Earning
                                    </label> <label class="">
                                        <?php
																																								echo form_checkbox ( array (
																																										'class' => 'flat toggle-vis',
																																										'data-column' => '32' 
																																								) );
																																								?>
                                        Zone
                                    </label>

						</div>

						<table
							class="table table-striped table-bordered table-hover table-checkable order-column" id="trip-transaction-data-table" style="width:99%">
							<thead>
								<tr>
									<th>Action</th>
									<th>Job Card Id</th>
									<th>Entity</th>
									<th>Invoice No</th>
									<th>Pickup Location</th>
									<th>Requested Pickup Datetime</th>
									<th>Driver Dispatch Datetime</th>
									<th>Driver Accepted Datetime</th>
									<th>Driver Arrived Datetime</th>
									<th>Pickup Datetime</th>
									<th>Drop Location</th>
									<th>Drop Datetime</th>
									<th>Taxi Reg No</th>
									<th>Taxi Type</th>
									<th>Trip Type</th>
									<th>Booked From</th>
									<th>Payment Mode</th>
									<th>Passenger Info</th>
									<th>Driver Code</th>
									<th>Driver Info</th>
									<th>Promocode</th>
									<th>Total Trip Charge</th>
									<th>Travelled Distance</th>
									<th>Travelled Period</th>
									<th>Estimated Period</th>
									<th>Convenience Charge</th>
									<th>Travel Charge</th>
									<th>Promo Discount</th>
									<th>Wallet Payment</th>
									<th>Booking Charge</th>
									<th>Penalty Charge</th>
									<th>Admin Amount</th>
									<th>Driver Earning</th>
									<th>Zone</th>
									
								</tr>
							</thead>
							<tbody>

							</tbody>
						</table>
					</div>
				</div>

			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>



