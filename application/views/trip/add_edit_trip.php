<!--<script type='text/javascript' src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>-->
<script type='text/javascript'
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script
	src="http://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_MAP_API_KEY; ?>&sensor=false&amp;libraries=places"
	type="text/javascript"></script>
<style type="text/css">
html, body, #map-canvas {
	margin: 0;
	padding: 0;
	height: 100%;
}

#map-canvas {
	width: 500px;
	height: 480px;
}

.pac-container {
	z-index: 100000;
}
</style>

<style type="text/css">
html, body, #map-canvasd {
	margin: 0;
	padding: 0;
	height: 100%;
}

#map-canvasd {
	width: 500px;
	height: 480px;
}

html, body, #map-canvasT {
	margin: 0;
	padding: 0;
	height: 100%;
}

#map-canvasT {
	width: 500px;
	height: 480px;
}

.pac-container {
	z-index: 100000;
}
</style>
<?php
$view_mode = $mode;
?>
<!-- BEGIN CONTENT -->

<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <?php
				$form_attr = array (
						'name' => 'edit_trip_form',
						'id' => 'edit_trip_form',
						'method' => 'POST' 
				);
				echo form_open_multipart ( base_url ( '' ), $form_attr );
				// driver id by default is -1
				echo form_input ( array (
						'type' => 'hidden',
						'id' => 'trip_id',
						'name' => 'id',
						'value' => ($trip_model->get ( 'id' )) ? $trip_model->get ( 'id' ) : - 1 
				) );
				/**
				 * Hidden fileds for Trip traking *
				 */
				if ($view_mode == 'view') {
					echo form_input ( array (
							'type' => 'hidden',
							'id' => 'tt_pickupLatLong',
							'name' => 'tt_pickupLatLong',
							'value' => ($tt_pickupLatLong) ? $tt_pickupLatLong : 0 
					) );
					echo form_input ( array (
							'type' => 'hidden',
							'id' => 'tt_dropLatLong',
							'name' => 'tt_dropLatLong',
							'value' => ($tt_dropLatLong) ? $tt_dropLatLong : 0 
					) );
					
					echo form_input ( array (
							'type' => 'hidden',
							'id' => 'marker_address',
							'name' => 'marker_address',
							'value' => '' 
					) );
				}
				
				?>
    <div class="page-bar ">
		<ul class="page-breadcrumb ">
			<li><a href="<?php echo base_url('Dashboard'); ?>">Home</a> <i
				class="fa fa-circle"></i></li>
			<li><a href="<?php echo base_url('Trip/getTripList'); ?>">Trip List</a>
				<i class="fa fa-circle"></i></li>
			<li><a href="<?php echo base_url(uri_string()); ?>"><?php echo ($trip_model->get('id')) ? (($view_mode == VIEW_MODE) ? 'View' : 'Edit') : 'Create'; ?>
                    Trip</a></li>

		</ul>

	</div>
	<!---- Page Bar ends ---->
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->

			<div class="portlet-body">
				<div class="form-body gp-form">
					<div class="card-header bg-white">
						<h1><?php echo ($trip_model->get('id')) ? (($view_mode == VIEW_MODE) ? 'View' : 'Edit') : 'Create'; ?>
                            Trip
                            <?php
																												if ($view_mode == 'view') :
																													echo '<a href="#tripTrackModal" class="btn" data-toggle="modal" title="Trip Tracking">
                                  <img src="' . base_url ( 'public/images/app/map.png' ) . '"></a>';
																												
                            endif;
																												?>
                        </h1>

					</div>
                    <?php if ($trip_model->get('id')): ?>
                        <div class="row">
						<div class="col-lg-3 input_field_sections nowarp">

                                <?php
																					echo form_label ( 'Trip Status:', 'tripStatus', array (
																							'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																					) );
																					// validation for passenger first name
																					if ($view_mode == EDIT_MODE) {
																						echo form_dropdown ( 'tripStatus', $trip_status_list, $trip_model->get ( 'tripStatus' ), array (
																								'id' => 'tripStatus',
																								'class' => 'form-control',
																								'required' => 'required' 
																						) );
																					} else {
																						echo text ( $trip_status_list [$trip_model->get ( 'tripStatus' )] );
																					}
																					?>
                            </div>
						<div class="col-lg-3 input_field_sections nowarp">

                                <?php
																					echo form_label ( 'Booked Date time:', 'createdDatetime', array (
																							'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																					) );
																					if ($view_mode == EDIT_MODE) {
																						echo form_input ( array (
																								'id' => 'createdDatetime',
																								'name' => 'createdDatetime',
																								'class' => 'form-control',
																								'readonly' => 'readonly',
																								'placeholder' => 'Booked Date time',
																								'value' => ($trip_model->get ( 'createdDatetime' )) ? $trip_model->get ( 'createdDatetime' ) : '' 
																						) );
																					} else {
																						echo text ( $trip_model->get ( 'createdDatetime' ) );
																					}
																					?>
                            </div>
                            <?php if ($trip_model->get('bookedFrom') == Signup_Type_Enum::BACKEND): ?>
                                <div
							class="col-lg-3 input_field_sections nowarp">

                                    <?php
																						echo form_label ( 'Booked Staff/User Name:', 'createdBy', array (
																								'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																						) );
																						if ($view_mode == EDIT_MODE) {
																							echo form_input ( array (
																									'id' => 'createdBy',
																									'name' => 'createdBy',
																									'readonly' => 'readonly',
																									'class' => 'form-control',
																									'placeholder' => 'Booked User Name',
																									'value' => ($trip_model->get ( 'createdBy' )) ? $user_model->get ( 'firstName' ) . ' ' . $user_model->get ( 'lastName' ) : '' 
																							) );
																						} else {
																							echo text ( ($trip_model->get ( 'createdBy' )) ? $user_model->get ( 'firstName' ) . ' ' . $user_model->get ( 'lastName' ) : '' );
																						}
																						?>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                    <div class="row">

						<div class="col-lg-3 input_field_sections nowarp">
                            <?php
																												echo form_label ( 'Customer Name:', 'passengerName', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																												) );
																												
																												if ($view_mode == EDIT_MODE) {
																													
																													echo form_input ( array (
																															'id' => 'passengerName',
																															'name' => 'passengerName',
																															'class' => 'form-control',
																															'required' => 'required',
																															'pattern' => '[a-zA-Z\s]{3,15}',
																															'placeholder' => 'Customer Name',
																															'value' => ($passenger_model->get ( 'firstName' )) ? $passenger_model->get ( 'firstName' ) . ' ' . $passenger_model->get ( 'lastName' ) : '' 
																													) );
																													echo '<div id="pname-suggesstion-box"></div>';
																													echo form_input ( array (
																															'id' => 'passengerId',
																															'name' => 'passengerId',
																															'required' => 'required',
																															'class' => 'form-control',
																															'type' => 'hidden',
																															'value' => ($passenger_model->get ( 'id' )) ? $passenger_model->get ( 'id' ) : '' 
																													) );
																												} else {
																													
																													echo text ( ($passenger_model->get ( 'firstName' )) ? $passenger_model->get ( 'firstName' ) . ' ' . $passenger_model->get ( 'lastName' ) : '' );
																													echo form_input ( array (
																															'id' => 'passengerId',
																															'name' => 'passengerId',
																															'required' => 'required',
																															'class' => 'form-control',
																															'type' => 'hidden',
																															'value' => ($passenger_model->get ( 'id' )) ? $passenger_model->get ( 'id' ) : '' 
																													) );
																												}
																												?>

                        </div>
						<div
							class="col-lg-3 input_field_sections nowarp section-container">
							<div class="section-message"></div>
                            <?php
																												echo form_label ( 'Customer Email:', 'passengerEmail', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																												) );
																												
																												if ($view_mode == EDIT_MODE) {
																													echo form_input ( array (
																															'id' => 'passengerEmail',
																															'name' => 'passengerEmail',
																															'class' => 'form-control',
																															
																															// 'required' => 'required',
																															// 'pattern'=>'[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$',
																															'placeholder' => 'Customer Email',
																															'value' => ($passenger_model->get ( 'eamil' )) ? $passenger_model->get ( 'email' ) : '' 
																													) );
																													echo '<div id="email-suggesstion-box"></div>';
																												} else {
																													echo text ( $passenger_model->get ( 'email' ) );
																												}
																												?>

                        </div>
						<div
							class="col-lg-3 input_field_sections nowarp section-container">
							<div class="section-message"></div>
                            <?php
																												echo form_label ( 'Customer Mobile:', 'passengerMobile', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																												) );
																												
																												if ($view_mode == EDIT_MODE) {
																													echo form_input ( array (
																															'id' => 'passengerMobile',
																															'name' => 'passengerMobile',
																															'class' => 'form-control',
																															'maxlength' => '11',
																															'required' => 'required',
																															'pattern' => '^[0]?[789]\d{7,9}$',
																															'placeholder' => 'Customer Mobile',
																															'value' => ($passenger_model->get ( 'mobile' )) ? $passenger_model->get ( 'mobile' ) : '' 
																													) );
																													echo '<div id="mobile-suggesstion-box"></div>';
																												} else {
																													echo text ( ($passenger_model->get ( 'mobile' )) ? $passenger_model->get ( 'mobile' ) : '' );
																												}
																												?>

                        </div>
						<div class="col-lg-3 input_field_sections nowarp">
							<a href="javascript:void(0);" id="guest_passenger" class="iconPM"
								style="display: none;" title="Save Passenger"> <i
								class="fa fa-floppy-o fa-6" aria-hidden="true"></i></a>

						</div>
					</div>
                    <?php if ($driver_model->get('id')): ?>
                        <div class="row">

						<div class="col-lg-3 input_field_sections nowarp">
                                <?php
																					echo form_label ( 'Driver Name:', 'driverName', array (
																							'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																					) );
																					
																					if ($view_mode == EDIT_MODE) {
																						
																						echo form_input ( array (
																								'id' => 'driverName',
																								'name' => 'driverName',
																								'class' => 'form-control',
																								
																								// 'required' => 'required',
																								// 'pattern' => '[a-zA-Z\s]{3,15}',
																								'placeholder' => 'Passenger Name',
																								'value' => ($driver_model->get ( 'firstName' )) ? $driver_model->get ( 'firstName' ) . ' ' . $passenger_model->get ( 'lastName' ) : '' 
																						) );
																						echo form_input ( array (
																								'id' => 'driverId',
																								'name' => 'driverId',
																								'required' => 'required',
																								'class' => 'form-control',
																								'type' => 'hidden',
																								'value' => $driver_model->get ( 'id' ) 
																						) );
																					} else {
																						echo text ( ($driver_model->get ( 'firstName' )) ? $driver_model->get ( 'firstName' ) . ' ' . $passenger_model->get ( 'lastName' ) : '' );
																						echo form_input ( array (
																								'id' => 'driverId',
																								'name' => 'driverId',
																								'required' => 'required',
																								'class' => 'form-control',
																								'type' => 'hidden',
																								'value' => $driver_model->get ( 'id' ) 
																						) );
																					}
																					?>

                            </div>
						<div class="col-lg-3 input_field_sections nowarp">
                                <?php
																					echo form_label ( 'Driver Email:', 'driverEmail', array (
																							'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																					) );
																					
																					if ($view_mode == EDIT_MODE) {
																						echo form_input ( array (
																								'id' => 'driverEmail',
																								'name' => 'driverEmail',
																								'class' => 'form-control',
																								'readyonly' => 'readyonly',
																								'placeholder' => 'Driver Email',
																								'value' => ($driver_model->get ( 'eamil' )) ? $driver_model->get ( 'email' ) : '' 
																						) );
																					} else {
																						echo text ( ($driver_model->get ( 'eamil' )) ? $driver_model->get ( 'email' ) : '' );
																					}
																					?>

                            </div>
						<div class="col-lg-3 input_field_sections nowarp">

                                <?php
																					echo form_label ( 'Driver Mobile:', 'driverMobile', array (
																							'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																					) );
																					
																					if ($view_mode == EDIT_MODE) {
																						echo form_input ( array (
																								'id' => 'driverMobile',
																								'name' => 'driverMobile',
																								'class' => 'form-control',
																								'readyonly' => 'readyonly',
																								'placeholder' => 'Driver Mobile',
																								'value' => ($driver_model->get ( 'mobile' )) ? $driver_model->get ( 'mobile' ) : '' 
																						) );
																					} else {
																						echo text ( ($driver_model->get ( 'mobile' )) ? $driver_model->get ( 'mobile' ) : '' );
																					}
																					?>

                            </div>
					</div>
                    <?php endif; ?>
                    <div class="row">
						<div class="col-lg-3 input_field_sections nowarp">

                            <?php
																												echo form_label ( 'Trip Type:', 'tripType', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																												) );
																												if ($view_mode == EDIT_MODE) {
																													echo form_dropdown ( 'tripType', $trip_type_list, $trip_model->get ( 'tripType' ), array (
																															'id' => 'tripType',
																															'class' => 'form-control',
																															'required' => 'required' 
																													) );
																												} else {
																													echo text ( $trip_type_list [$trip_model->get ( 'tripType' )] );
																												}
																												?>

                        </div>
						<div class="col-lg-3 input_field_sections nowarp">

                            <?php
																												echo form_label ( 'Pickup Zone Name:', 'zoneId', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																												) );
																												// validation for passenger first name
																												if ($view_mode == EDIT_MODE) {
																													echo form_dropdown ( 'zoneId', $zone_list, $trip_model->get ( 'zoneId' ), array (
																															'id' => 'zoneId',
																															'class' => 'form-control',
																															'required' => 'required' 
																													) );
																												} else {
																													echo text ( ($trip_model->get ( 'zoneId' )) ? @$zone_list [$trip_model->get ( 'zoneId' )] : 'All Zone' );
																												}
																												?>
                            <div id='sub_zone_sec'
								style='display: none;'>
                                <?php
																																/* * * Sub Zone ** */
																																echo form_label ( 'Sub Zone Name:', 'subZoneId', array (
																																		'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																																) );
																																// validation for passenger first name
																																if ($view_mode == EDIT_MODE) {
																																	echo form_dropdown ( 'subZoneId', $sub_zone_list, $trip_model->get ( 'subZoneId' ), array (
																																			'id' => 'subZoneId',
																																			'class' => 'form-control' 
																																	) );
																																} else {
																																	echo text ( @$sub_zone_list [$trip_model->get ( 'subZoneId' )] );
																																}
																																?>
                            </div>

						</div>
						<div class="col-lg-3 input_field_sections nowarp">

                            <?php
																												echo form_label ( 'Pickup Location:', 'pickupLocation', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																												) );
																												// echo ' <a class="youtube" href="'.base_url('Trip/loadGmapForTrip').'" title="Google Map">Load Map</a>';
																												
																												if ($view_mode == EDIT_MODE) {
																													echo '<a href="#myMapModal" class="btn" data-toggle="modal"><img src="' . base_url ( 'public/images/app/map.png' ) . '"></a>';
																													echo form_textarea ( array (
																															'id' => 'pickupLocation',
																															'name' => 'pickupLocation',
																															'rows' => '2',
																															'class' => 'form-control',
																															'required' => 'required',
																															'value' => ($trip_model->get ( 'pickupLocation' )) ? $trip_model->get ( 'pickupLocation' ) : '' 
																													) );
																													echo form_input ( array (
																															'id' => 'pickupLatitude',
																															'name' => 'pickupLatitude',
																															'class' => 'form-control',
																															'required' => 'required',
																															'type' => 'hidden',
																															'value' => ($trip_model->get ( 'pickupLatitude' )) ? $trip_model->get ( 'pickupLatitude' ) : '' 
																													) );
																													echo form_input ( array (
																															'id' => 'pickupLongitude',
																															'name' => 'pickupLongitude',
																															'class' => 'form-control',
																															'required' => 'required',
																															'type' => 'hidden',
																															'value' => ($trip_model->get ( 'pickupLongitude' )) ? $trip_model->get ( 'pickupLongitude' ) : '' 
																													) );
																												} else {
																													echo form_textarea ( array (
																															'id' => 'pickupLocation',
																															'name' => 'pickupLocation',
																															'rows' => '2',
																															'class' => 'form-control',
																															'readonly' => 'readonly',
																															'value' => ($trip_model->get ( 'pickupLocation' )) ? $trip_model->get ( 'pickupLocation' ) : '' 
																													) );
																													echo form_input ( array (
																															'id' => 'pickupLatitude',
																															'name' => 'pickupLatitude',
																															'class' => 'form-control',
																															'required' => 'required',
																															'type' => 'hidden',
																															'value' => ($trip_model->get ( 'pickupLatitude' )) ? $trip_model->get ( 'pickupLatitude' ) : '' 
																													) );
																													echo form_input ( array (
																															'id' => 'pickupLongitude',
																															'name' => 'pickupLongitude',
																															'class' => 'form-control',
																															'required' => 'required',
																															'type' => 'hidden',
																															'value' => ($trip_model->get ( 'pickupLongitude' )) ? $trip_model->get ( 'pickupLongitude' ) : '' 
																													) );
																												}
																												?>

                        </div>
						<div class="col-lg-3 input_field_sections nowarp">

                            <?php
																												echo form_label ( 'Pickup Datetime:', 'pickupDatetime', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																												) );
																												
																												if ($view_mode == EDIT_MODE) {
																													echo form_input ( array (
																															'id' => 'pickupDatetime',
																															'name' => 'pickupDatetime',
																															'class' => 'form-control',
																															'required' => 'required',
																															'placeholder' => 'Pickup Datetime',
																															'value' => ($trip_model->get ( 'pickupDatetime' )) ? $trip_model->get ( 'pickupDatetime' ) : '' 
																													) );
																												} else {
																													echo text ( $trip_model->get ( 'pickupDatetime' ) );
																												}
																												?>

                        </div>


					</div>
					<div class="row">
						<div class="col-lg-3 input_field_sections nowarp">
                            <?php
																												echo form_label ( 'Drop Zone Name:', 'dropZoneId', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																												) );
																												// validation for passenger first name
																												if ($view_mode == EDIT_MODE) {
																													echo form_dropdown ( 'dropZoneId', $zone_list, $trip_model->get ( 'zoneId' ), array (
																															'id' => 'dropZoneId',
																															'class' => 'form-control',
																															'required' => 'required' 
																													) );
																												} else {
																													echo text ( ($trip_model->get ( 'zoneId' )) ? @$zone_list [$trip_model->get ( 'zoneId' )] : 'All Zone' );
																												}
																												?>
                            <div id='dropSub_zone_sec'
								style='display: none;'>
                                <?php
																																/* * * Sub Zone ** */
																																echo form_label ( 'Sub Zone Name:', 'dropSubZoneId', array (
																																		'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																																) );
																																// validation for passenger first name
																																if ($view_mode == EDIT_MODE) {
																																	echo form_dropdown ( 'dropSubZoneId', $sub_zone_list, $trip_model->get ( 'subZoneId' ), array (
																																			'id' => 'dropSubZoneId',
																																			'class' => 'form-control' 
																																	) );
																																} else {
																																	echo text ( @$sub_zone_list [$trip_model->get ( 'dropSubZoneId' )] );
																																}
																																?>
                            </div>

						</div>
						<div class="col-lg-3 input_field_sections nowarp">

                            <?php
																												echo form_label ( 'Drop Location:', 'dropLocation', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																												) );
																												
																												if ($view_mode == EDIT_MODE) {
																													echo '<a href="#myMapModald" class="btn" data-toggle="modal"><img src="' . base_url ( 'public/images/app/map.png' ) . '"></a>';
																													echo form_textarea ( array (
																															'id' => 'dropLocation',
																															'name' => 'dropLocation',
																															'rows' => '2',
																															'class' => 'form-control',
																															'required' => 'required',
																															'value' => ($trip_model->get ( 'dropLocation' )) ? $trip_model->get ( 'dropLocation' ) : '' 
																													) );
																													
																													echo form_input ( array (
																															'id' => 'dropLatitude',
																															'name' => 'dropLatitude',
																															'class' => 'form-control',
																															'required' => 'required',
																															'type' => 'hidden',
																															'value' => ($trip_model->get ( 'dropLatitude' )) ? $trip_model->get ( 'dropLatitude' ) : '' 
																													) );
																													echo form_input ( array (
																															'id' => 'dropLongitude',
																															'name' => 'dropLongitude',
																															'class' => 'form-control',
																															'required' => 'required',
																															'type' => 'hidden',
																															'value' => ($trip_model->get ( 'dropLongitude' )) ? $trip_model->get ( 'dropLongitude' ) : '' 
																													) );
																												} else {
																													echo form_textarea ( array (
																															'id' => 'dropLocation',
																															'name' => 'dropLocation',
																															'rows' => '2',
																															'class' => 'form-control',
																															'readonly' => 'readonly',
																															'value' => ($trip_model->get ( 'dropLocation' )) ? $trip_model->get ( 'dropLocation' ) : '' 
																													) );
																													echo form_input ( array (
																															'id' => 'dropLatitude',
																															'name' => 'dropLatitude',
																															'class' => 'form-control',
																															'required' => 'required',
																															'type' => 'hidden',
																															'value' => ($trip_model->get ( 'dropLatitude' )) ? $trip_model->get ( 'dropLatitude' ) : '' 
																													) );
																													echo form_input ( array (
																															'id' => 'dropLongitude',
																															'name' => 'dropLongitude',
																															'class' => 'form-control',
																															'required' => 'required',
																															'type' => 'hidden',
																															'value' => ($trip_model->get ( 'dropLongitude' )) ? $trip_model->get ( 'dropLongitude' ) : '' 
																													) );
																												}
																												?>

                        </div>
						<div class="col-lg-3 input_field_sections nowarp">

                            <?php
																												echo form_label ( 'Payment Mode:', 'paymentMode', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																												) );
																												if ($view_mode == EDIT_MODE) {
																													echo form_dropdown ( 'paymentMode', $payment_mode_list, $trip_model->get ( 'paymentMode' ), array (
																															'id' => 'paymentMode',
																															'class' => 'form-control',
																															'required' => 'required' 
																													) );
																												} else {
																													echo text ( $payment_mode_list [$trip_model->get ( 'paymentMode' )] );
																												}
																												?>

                        </div>
						<div class="col-lg-3 input_field_sections nowarp">

                            <?php
																												echo form_label ( 'Vehicle Type:', 'taxiCategoryType', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																												) );
																												if ($view_mode == EDIT_MODE) {
																													echo form_dropdown ( 'taxiCategoryType', $taxi_category_type_list, $trip_model->get ( 'taxiCategoryType' ), array (
																															'id' => 'taxiCategoryType',
																															'class' => 'form-control',
																															'required' => 'required' 
																													) );
																												} else {
																													echo text ( $taxi_category_type_list [$trip_model->get ( 'taxiCategoryType' )] );
																												}
																												?>

                        </div>

					</div>
					<div class="row">
						<div class="col-lg-3 input_field_sections nowarp">

                            <?php
																												echo form_label ( 'Entity Name:', 'entityId', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																												) );
																												// validation for passenger first name
																												if ($view_mode == EDIT_MODE) {
																													echo form_dropdown ( 'entityId', $entity_list, ($trip_model->get ( 'entityId' )) ? $trip_model->get ( 'entityId' ) : DEFAULT_COMPANY_ID, array (
																															'id' => 'entityId',
																															'class' => 'form-control',
																															'required' => 'required' 
																													) );
																												} else {
																													echo text ( ($trip_model->get ( 'entityId' )) ? @$entity_list [$trip_model->get ( 'entityId' )] : 'All Entity' );
																												}
																												?>
                        </div>

						<div
							class="col-lg-3 input_field_sections nowarp section-container">
							<div class="section-message"></div>
                            <?php
																												echo form_label ( 'Promocode:', 'promoCode', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																												) );
																												
																												if ($view_mode == EDIT_MODE) {
																													echo form_input ( array (
																															'id' => 'promoCode',
																															'name' => 'promoCode',
																															'class' => 'form-control',
																															'placeholder' => 'Promocode',
																															'value' => ($trip_model->get ( 'promoCode' )) ? $trip_model->get ( 'promoCode' ) : '' 
																													) );
																												} else {
																													echo text ( $trip_model->get ( 'promoCode' ) );
																												}
																												?>

                        </div>

						<div class="col-lg-3 input_field_sections nowarp">

                            <?php
																												echo form_label ( 'CRN:', 'customerReferenceCode', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																												) );
																												
																												if ($view_mode == EDIT_MODE) {
																													echo form_input ( array (
																															'id' => 'customerReferenceCode',
																															'name' => 'customerReferenceCode',
																															'class' => 'form-control',
																															'placeholder' => 'Customer Reference Number',
																															'value' => ($trip_model->get ( 'customerReferenceCode' )) ? $trip_model->get ( 'customerReferenceCode' ) : '' 
																													) );
																												} else {
																													echo text ( $trip_model->get ( 'customerReferenceCode' ) );
																												}
																												?>

                        </div>


						<div class="col-lg-3 input_field_sections nowarp hidden"
							id="estimate-distance">

                            <?php
																												echo form_label ( 'Estimate Distance:', 'estimateDistance', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																												) );
																												
																												if ($view_mode == EDIT_MODE) {
																													echo form_input ( array (
																															'id' => 'estimateDistance',
																															'name' => 'estimateDistance',
																															'class' => 'form-control',
																															'placeholder' => 'Estimate Distance',
																															'readonly' => 'readonly',
																															'value' => '' 
																													) );
																												}
																												?>

                        </div>
						<div class="col-lg-3 input_field_sections nowarp hidden"
							id="estimate-time">

                            <?php
																												echo form_label ( 'Estimate Time(minutes):', 'estimateTime', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																												) );
																												
																												if ($view_mode == EDIT_MODE) {
																													echo form_input ( array (
																															'id' => 'estimateTime',
																															'name' => 'estimateTime',
																															'class' => 'form-control',
																															'readonly' => 'readonly',
																															'placeholder' => 'Estimated Time',
																															'value' => '' 
																													) );
																												}
																												?>

                        </div>
						<div class="col-lg-3 input_field_sections nowarp hidden"
							id="estimate-amount">

                            <?php
																												echo form_label ( 'Estimate Bill Amount:', 'estimateAmount', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																												) );
																												
																												if ($view_mode == EDIT_MODE) {
																													echo form_input ( array (
																															'id' => 'estimateAmount',
																															'name' => 'estimateAmount',
																															'class' => 'form-control',
																															'readonly' => 'readonly',
																															'placeholder' => 'Estimated Bill Amount',
																															'value' => '' 
																													) );
																												}
																												?>

                        </div>

					</div>
					<div class="form-actions" style="margin-top: 22px;">
						<div class="row">
							<div class="col-md-12">
                                <?php if ($view_mode == EDIT_MODE) { ?>
                                    <div class="gp-cen">
									<button type="button" id="cancel-trip-btn"
										class="btn grey-salsa btn-outline">Cancel</button>
									<button type="button" id="trip-estimate-btn" class="btn gpblue">Estimate
									</button>
									<button type="button" id="save-trip-btn" class="btn gpblue">Book</button>

								</div>
                                    <?php
																																} else {
																																	echo '<div class="gp-cen">';
																																	echo '<button type="button" id="cancel-trip-btn" class="btn btn-danger">Back</button>';
																																	if ($trip_model->get ( 'tripStatus' ) == Trip_Status_Enum::DRIVER_ACCEPTED || $trip_model->get ( 'tripStatus' ) == Trip_Status_Enum::DRIVER_ARRIVED && $trip_model->get ( 'driverId' )) {
																																		if ($this->session->userdata ( 'role_type' ) == Role_Type_Enum::ADMIN || $this->session->userdata ( 'role_type' ) == Role_Type_Enum::SUPER_ADMIN) {
																																			echo '<button type="button" id="driver-reject-btn" class="btn btn-danger">Driver Reject</button>';
																																		}
																																	}
																																	if ($trip_model->get ( 'tripStatus' ) == Trip_Status_Enum::DRIVER_DISPATCHED) {
																																		if ($this->session->userdata ( 'role_type' ) == Role_Type_Enum::ADMIN || $this->session->userdata ( 'role_type' ) == Role_Type_Enum::SUPER_ADMIN) {
																																			echo '<button type="button" id="driver-release-btn" class="btn btn-danger">Driver Release</button>';
																																		}
																																	}
																																	if ($trip_model->get ( 'tripStatus' ) == Trip_Status_Enum::DRIVER_ACCEPTED || $trip_model->get ( 'tripStatus' ) == Trip_Status_Enum::DRIVER_ARRIVED && $trip_model->get ( 'driverId' ) || $trip_model->get ( 'tripStatus' ) == Trip_Status_Enum::BOOKED) {
																																		echo '<button type="button" id="passenger-reject-btn" class="btn btn-danger">Passenger Reject</button>';
																																	}
																																	if ($trip_model->get ( 'tripStatus' ) == Trip_Status_Enum::DRIVER_ACCEPTED || $trip_model->get ( 'tripStatus' ) == Trip_Status_Enum::DRIVER_ARRIVED && $trip_model->get ( 'driverId' )) {
																																		if ($this->session->userdata ( 'role_type' ) == Role_Type_Enum::ADMIN || $this->session->userdata ( 'role_type' ) == Role_Type_Enum::SUPER_ADMIN || $this->session->userdata ( 'role_type' ) == Role_Type_Enum::SUPERVISOR) {
																																			echo '<button type="button" id="trip-start-btn" class="btn btn-danger">Trip Start</button>';
																																		}
																																	}
																																	if ($trip_model->get ( 'tripStatus' ) == Trip_Status_Enum::IN_PROGRESS && $trip_model->get ( 'driverId' )) {
																																		if ($this->session->userdata ( 'role_type' ) == Role_Type_Enum::ADMIN || $this->session->userdata ( 'role_type' ) == Role_Type_Enum::SUPER_ADMIN || $this->session->userdata ( 'role_type' ) == Role_Type_Enum::SUPERVISOR) {
																																			echo '<button type="button" id="trip-end-btn" class="btn btn-danger">Trip End</button>';
																																		}
																																	}
																																	echo '</div>';
																																}
																																?>
                            </div>
						</div>
					</div>
<?php
echo form_close ();
?>
                </div>

			</div>

		</div>

	</div>
	<div class="row hidden" id="show-driver-list">
		<div class="col-md-12">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->
			<div class="portlet box green">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-comments"></i>Driver List For Trip
					</div>

				</div>

				<div class="portlet-body">

					<table
						class="table table-striped table-bordered table-hover table-checkable order-column gp-font"
						id="driver-list-table" style="width: 99%">
						<thead>
							<tr>
								<th>Action</th>
								<th>Driver Code</th>
								<th>Driver Name</th>
								<th>Mobile</th>
								<th>Driver Wallet</th>
								<th>Distance Away</th>
								<th>Time To Reach</th>
								<th>Taxi No</th>
								<th>Driver Rating</th>
							</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
					<div class="form-actions" style="margin-top: 22px;">
						<div class="row">
							<div class="col-md-12">

								<div class="gp-cen">
									<button type="button" id="driver-not-found-btn"
										class="btn gpblue">Driver Not Found</button>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
<!------------------ Load Google Pickup location Map starts ----------------------------->
<div class="modal fade" id="myMapModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">�</button>
				<h4 class="modal-title">Modal title</h4>
				<input id="searchTextField" type="text" size="50"
					placeholder="Enter a location" autocomplete="on" runat="server" />
			</div>
			<div class="modal-body">
				<div class="container">
					<div class="row">
						<div id="map-canvas" class=""></div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<!--<button type="button" class="btn btn-primary">Save changes</button>-->
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!------------------ Load Google Pickup location Map ends ----------------------------->

<!------------------ Load Google Drop location Map starts ----------------------------->
<div class="modal fade" id="myMapModald">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">�</button>
				<h4 class="modal-title">Modal title</h4>
				<input id="dropTextField" type="text" size="50"
					placeholder="Enter a location" autocomplete="on" runat="server" />
			</div>
			<div class="modal-body">
				<div class="container">
					<div class="row">
						<div id="map-canvasd" class=""></div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<!--<button type="button" class="btn btn-primary">Save changes</button>-->
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!------------------ Load Google Drop location Map ends ----------------------------->


<!------------------ Trip Tracking Map starts ----------------------------->
<div class="modal fade" id="tripTrackModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">�</button>
				<h4 class="modal-title">Trip Tracking</h4>
			</div>
			<div class="modal-body">
				<div class="container">
					<div class="row">
						<div id="map-canvasT" class=""></div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<!--<button type="button" class="btn btn-primary">Save changes</button>-->
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!------------------ Trip Tracking Map ends ----------------------------->
