<?php
$view_mode = $mode;
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">

	<table id="mainStructure"
		style="border: 1px solid #dedede; box-shadow: 8px 12px 23px -6px rgba(208, 210, 212, 1);"
		width="700" cellspacing="0" cellpadding="0" border="0" align="center">
		<!--START LAYOUT-1 ( View Online / Call )-->
		<tbody>
			<tr>
				<td class="container" style="background-color: #293896;"
					valign="top" align="center">
					<!-- start container -->

					<table class="container"
						style="min-width: 600px; margin: 0px auto; padding-left: 20px; padding-right: 20px; width: 600px; background-color: #293896;"
						width="600" cellspacing="0" cellpadding="0" border="0"
						align="center">
						<tbody>
							<tr>
								<td valign="top"><table class="full-width"
										style="margin: 0px auto; width: 560px;" width="560"
										cellspacing="0" cellpadding="0" border="0" align="center">
										<!-- start space -->
										<tbody>
											<tr>
												<td valign="top" height="10"></td>
											</tr>
											<!-- end space -->
											<tr>
												<td valign="top"><table width="100%" cellspacing="0"
														cellpadding="0" border="0" align="center">
														<tbody>
															<tr>
																<td valign="middle"><table class="full-width-center"
																		dir="ltr" width="auto" cellspacing="0" cellpadding="0"
																		border="0" align="left">
																		<tbody>
																			<tr>
																				<td dup="0" valign="top" align="center"><table
																						width="auto" cellspacing="0" cellpadding="0"
																						border="0" align="center">
																						<tbody>
																							<tr>
																								<td align="left"><img src="<?php echo image_url('/app/logo-bill.png')?>"
																									width="60"></td>
																							</tr>
																						</tbody>
																					</table></td>
																			</tr>
																		</tbody>
																	</table> <!--[if (gte mso 9)|(IE)]></td><td valign="top"><![endif]-->

																	<table class="full-width"
																		style="height: 1px; border-spacing: 0px; width: 20px;"
																		dir="ltr" width="20" cellspacing="0" cellpadding="0"
																		border="0" align="left">
																		<tbody>
																			<tr>
																				<td class="h-20" height="1"></td>
																			</tr>
																		</tbody>
																	</table> <!--[if (gte mso 9)|(IE)]></td><td valign="top"><![endif]-->

																	<table class="auto-center" dir="ltr" width="auto"
																		cellspacing="0" cellpadding="0" border="0"
																		align="right">
																		<tbody>
																			<tr>
																				<td dup="0" valign="top" align="left"><table
																						width="auto" cellspacing="0" cellpadding="0"
																						border="0" align="left">
																						<tbody>
																							<tr>
																								<td
																									style="font-size: 14px; line-height: 24px; color: rgb(255, 255, 255); font-weight: normal; text-align: left; font-family: 'Open Sans', Arial, Helvetica, sans-serif; word-break: break-word; vertical-align: middle"
																									align="left"><span
																									style="text-decoration: none; color: rgb(255, 255, 255); font-size: inherit; line-height: 24px; vertical-align: middle">Date : <?php echo date('d-m-Y',strtotime($transaction_model[0]->invoiceDatetime));?> <br> Invoice No. : <?php echo $transaction_model[0]->invoiceNo;?> </span></td>
																							</tr>
																						</tbody>
																					</table></td>
																			</tr>
																		</tbody>
																	</table></td>
															</tr>
														</tbody>
													</table></td>
											</tr>
											<!-- start space -->
											<tr>
												<td valign="top" height="5"></td>
											</tr>
											<!-- end space -->
										</tbody>
									</table></td>
							</tr>
						</tbody>
					</table> <!-- end container -->
				</td>
			</tr>
			<!--END LAYOUT-1 ( View Online / Call )-->

			<!--START LAYOUT-2 ( LOGO / MENU )-->
			<tr>
				<td class="container" style="background-color: #ffffff;"
					valign="top" align="center">
					<!-- start container -->

					<table class="container"
						style="min-width: 600px; padding-left: 20px; padding-right: 20px; width: 600px; background-color: rgb(255, 255, 255);"
						width="600" cellspacing="0" cellpadding="0" border="0"
						align="center">
						<tbody>
							<tr>
								<td valign="top"><table class="full-width"
										style="margin: 0 auto;" width="100%" cellspacing="0"
										cellpadding="0" border="0" align="center">
										<!-- start space -->
										<tbody>
											<tr>
												<td valign="top" height="15"></td>
											</tr>
											<!-- end space -->

										</tbody>
									</table></td>
							</tr>
						</tbody>
					</table> <!-- end container -->
				</td>
			</tr>
			<!--END LAYOUT-2 ( LOGO / MENU )-->

			<!--START LAYOUT-3 ( BG IMAGE / TEXT-CENTER / BUTTON )-->
			<tr>
				<td class="container"
					style="background-color: #fff; background-size: cover; background-position: 50% 100%; background-repeat: no-repeat !important;"
					valign="top" align="center">
					<!-- start container -->

					<table class="container"
						style="min-width: 600px; margin: 0px auto; width: 600px;"
						width="600" cellspacing="0" cellpadding="0" border="0"
						align="center">
						<tbody>
							<tr>
								<td valign="top" align="center"><table class="full-width"
										style="margin: 0 auto;" width="100%" cellspacing="0"
										cellpadding="0" border="0" align="center">
										<!-- start space -->

										<!-- end space -->

										<!-- start border content -->
										<tbody>
											<tr dup="0">
												<td class="clear-pad"
													style="padding-left: 20px; padding-right: 20px;"
													valign="top"><table class="auto-center" width="auto"
														cellspacing="0" cellpadding="0" border="0" align="center">
														<tbody>
															<tr>
																<td valign="top"><table width="100%" cellspacing="0"
																		cellpadding="0" border="0" align="left">
																		<!-- start border top  -->
																		<tbody>
																			<tr>
																				<td style="opacity: 0.8; background-color: #293896;"
																					valign="top" width="100%" height="5"><table
																						width="100%" cellspacing="0" cellpadding="0"
																						border="0" align="center">
																						<tbody>
																							<tr>
																								<td valign="top" width="100%" height="5"></td>
																							</tr>
																						</tbody>
																					</table></td>
																			</tr>
																			<!-- end border top -->
																			<tr>
																				<td valign="top"><table width="100%" cellspacing="0"
																						cellpadding="0" border="0" align="center">
																						<tbody>
																							<tr>
																								<!-- start border left  -->
																								<td
																									style="opacity: 0.8; width: 5px; background-color: #293896;"
																									valign="top" width="5" height="100%"><table
																										style="min-width: 5px !important; max-width: 5px !important; width: 5px;"
																										width="5" cellspacing="0" cellpadding="0"
																										border="0" align="center">
																										<tbody>
																											<tr>
																												<td
																													style="font-size: 0px; line-height: 0; border-collapse: collapse; width: 5px;"
																													valign="top" width="5" height="100%"></td>
																											</tr>
																										</tbody>
																									</table></td>
																								<!-- end border left -->
																								<td valign="top"><table width="100%"
																										cellspacing="0" cellpadding="0" border="0"
																										align="left">
																										<!-- start space -->
																										<tbody>
																											<tr>
																												<td valign="top" height="10"></td>
																											</tr>
																											<!-- end space -->
																											<tr>
																												<td
																													style="padding-left: 20px; padding-right: 20px;"
																													valign="top"><table width="100%"
																														cellspacing="0" cellpadding="0" border="0"
																														align="left">
																														<!-- start title -->
																														<tbody>
																															<tr dup="0">
																																<td valign="top" align="center"><table
																																		class="full-width-center"
																																		style="margin: 0 auto;" width="100%"
																																		cellspacing="0" cellpadding="0"
																																		border="0" align="center">
																																		<tbody>
																																			<tr>
																																				<td
																																					style="font-size: 22px; color: rgb(255, 255, 255); font-weight: bold; text-align: center; font-family: 'Open Sans', Arial, Helvetica, sans-serif; word-break: break-word;"
																																					align="center"><span
																																					style="color: #293896; font-size: 48px; font-weight: bold; line-height: 48px;"><?php echo $transaction_model[0]->totalTripCost;?></span>
																																					<span
																																					style="color: rgb(0, 0, 0); font-weight: bold; font-size: inherit; line-height: 24px;"><?php echo $transaction_model[0]->currencyCode;?>&nbsp;&nbsp;</span></td>
																																			</tr>
																																			<!-- start space -->
																																			<tr>
																																				<td valign="top" height="10"></td>
																																			</tr>
																																			<!-- end space -->
																																		</tbody>
																																	</table></td>
																															</tr>
																															<!-- end title -->
																														</tbody>
																													</table></td>
																											</tr>
																										</tbody>
																									</table></td>
																								<!-- start border right  -->
																								<td
																									style="opacity: 0.8; width: 5px; background-color: #293896;"
																									valign="top" width="5" height="100%"><table
																										style="min-width: 5px !important; max-width: 5px !important; width: 5px;"
																										width="5" cellspacing="0" cellpadding="0"
																										border="0" align="center">
																										<tbody>
																											<tr>
																												<td
																													style="font-size: 0px; line-height: 0; border-collapse: collapse; width: 5px;"
																													valign="top" width="5" height="100%"></td>
																											</tr>
																										</tbody>
																									</table></td>
																								<!-- end border right -->
																							</tr>
																						</tbody>
																					</table></td>
																			</tr>
																			<!-- start border bottom  -->
																			<tr>
																				<td style="opacity: 0.8; background-color: #293896;"
																					valign="top" width="100%" height="5"><table
																						width="100%" cellspacing="0" cellpadding="0"
																						border="0" align="center">
																						<tbody>
																							<tr>
																								<td valign="top" width="100%" height="5"></td>
																							</tr>
																						</tbody>
																					</table></td>
																			</tr>
																			<!-- end border bottom -->
																		</tbody>
																	</table></td>
															</tr>
														</tbody>
													</table></td>
											</tr>
											<!-- end border content -->




											<tr>
												<td height="15"></td>
											</tr>


											<tr>
												<td align="center">
													<table width="100%" cellspacing="0" cellpadding="0"
														border="0" align="center">
														<tbody>
															<tr>
																<td
																	style="text-align: center; vertical-align: top; font-size: 0;"
																	align="center">
																	<div
																		style="display: inline-block; vertical-align: top;">
																		<table cellspacing="0" cellpadding="0" border="0"
																			align="center">
																			<tbody>
																				<tr>
																					<td width="150" align="center">
																						<table style="border: 2px solid #5460AB"
																							width="95%" cellspacing="0" cellpadding="0"
																							border="0" align="center">
																							<tbody>
																								<tr>
																									<td
																										style="font-size: 16px; color: rgb(255, 255, 255); font-weight: bold; text-align: center; font-family: 'Open Sans', Arial, Helvetica, sans-serif; word-break: break-word;"
																										align="center"><span
																										style="color: rgb(0, 0, 0); font-weight: bold; font-size: inherit; line-height: 24px;">Travel
																											Distance</span><br>
																									<span
																										style="color: #293896; font-size: 25px; font-weight: bold; line-height: 25px;"><?php echo number_format($transaction_model[0]->travelledDistance,1);?></span>
																										<span
																										style="color: rgb(0, 0, 0); font-weight: bold; font-size: inherit; line-height: 10px;">Km&nbsp;&nbsp;</span></td>
																								</tr>
																							</tbody>
																						</table>
																					</td>
																				</tr>
																				<tr>
																					<td height="15"></td>
																				</tr>
																			</tbody>
																		</table>
																	</div> <!--[if (gte mso 9)|(IE)]>
                                </td>
                                <td align="center" style="text-align:center;vertical-align:top;font-size:0;">
                                <![endif]-->
																	<div
																		style="display: inline-block; vertical-align: top;">
																		<table cellspacing="0" cellpadding="0" border="0"
																			align="center">
																			<tbody>
																				<tr>
																					<td width="150" align="center">
																						<table style="border: 2px solid #5460AB"
																							width="95%" cellspacing="0" cellpadding="0"
																							border="0" align="center">
																							<tbody>
																								<tr>
																									<td
																										style="font-size: 16px; color: rgb(255, 255, 255); font-weight: bold; text-align: center; font-family: 'Open Sans', Arial, Helvetica, sans-serif; word-break: break-word;"
																										align="center"><span
																										style="color: rgb(0, 0, 0); font-weight: bold; font-size: inherit; line-height: 24px;">Travel
																											Period</span><br>
																									<span
																										style="color: #293896; font-size: 25px; font-weight: bold; line-height: 25px;"><?php echo $transaction_model[0]->travelledPeriod;?></span>
																										<span
																										style="color: rgb(0, 0, 0); font-weight: bold; font-size: inherit; line-height: 10px;">Mins</span></td>
																								</tr>
																							</tbody>
																						</table>
																					</td>
																				</tr>
																				<tr>
																					<td height="15"></td>
																				</tr>
																			</tbody>
																		</table>
																	</div>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>









											<tr>
												<td class="clear-pad"
													style="padding-left: 20px; padding-right: 20px;"
													valign="top" align="center"><table
														class="full-width-center" style="margin: 0 auto;"
														width="100%" cellspacing="0" cellpadding="0" border="0"
														align="center">
														<!-- start space -->

														<!-- end space -->
														<!-- start title -->
														<tbody>
															<tr dup="0">
																<td valign="top" align="center"><table
																		class="full-width-center" style="margin: 0 auto;"
																		width="100%" cellspacing="0" cellpadding="0"
																		border="0" align="center">
																		<tbody>
																			<tr>
																				<td
																					style="font-size: 15px; color: rgb(51, 51, 51); font-weight: bold; text-align: center; font-family: 'Open Sans', Arial, Helvetica, sans-serif; word-break: break-word;"
																					align="center"><span
																					style="color: rgb(51, 51, 51); font-weight: bold; font-size: inherit; line-height: 24px;">CRN  <?php echo $transaction_model[0]->customerReferenceCode;?></span></td>
																			</tr>
																			<!-- start space -->
																			<tr>
																				<td valign="top" height="5"></td>
																			</tr>
																			<!-- end space -->
																			<tr>
																				<td
																					style="font-size: 28px; color: rgb(51, 51, 51); font-weight: 300; text-align: center; font-family: 'Open Sans', Arial, Helvetica, sans-serif; word-break: break-word;"
																					align="center"><span
																					style="color: rgb(51, 51, 51); font-weight: 300; font-size: inherit; line-height: 28px;">Thanks
																						for travelling with us, <span
																						style="color: rgb(51, 51, 51); font-weight: bold; font-size: inherit; line-height: 24px;"> <?php echo $transaction_model[0]->passengerFullName;?> </span>
																				</span></td>
																			</tr>
																		</tbody>
																	</table></td>
															</tr>
															<!-- end title -->

														</tbody>
													</table></td>
											</tr>
											<!-- start space -->
											<tr>
												<td valign="top" height="20"></td>
											</tr>
											<!-- end space -->
										</tbody>
									</table></td>
							</tr>
						</tbody>
					</table> <!-- end container -->
				</td>
			</tr>
			<!--END LAYOUT-3 ( BG IMAGE / TEXT-CENTER / BUTTON )-->

			<!--START LAYOUT-4 ( 2-COL IMAGE / TEXT / BUTTON )-->




			<tr>
				<td class="container"
					style="background-color: #fff; background-size: cover; background-position: 50% 100%; background-repeat: no-repeat !important;"
					valign="top" align="center">
					<!-- start container -->

					<table class="container"
						style="min-width: 600px; margin: 0px auto; width: 600px;"
						width="600" cellspacing="0" cellpadding="0" border="0"
						align="center">



						<tbody>
							<tr>
								<td height="20">&nbsp;</td>
							</tr>
							<tr>
								<td bgcolor="#E0E5E7" align="center">
									<table cellspacing="0" cellpadding="0" border="0"
										align="center">
										<tbody>
											<tr>
												<td width="600" align="center">
													<table width="100%" cellspacing="0" cellpadding="0"
														border="0" align="center">
														<tbody>
															<tr>
																<td height="15"></td>
															</tr>
															<tr>
																<td
																	style="text-align: center; vertical-align: top; font-size: 0;"
																	align="center">
																	<!--img-->
																	<div
																		style="display: inline-block; vertical-align: top;">
																		<table cellspacing="0" cellpadding="0" border="0"
																			align="center">
																			<tbody>
																				<tr>
																					<td width="200" align="center">
																						<table width="90%" cellspacing="0" cellpadding="0"
																							border="0" align="center">
																							<tbody>
																								<tr>
                             <?php $driver_profile_image=($transaction_model[0]->driverProfileImage) ? (driver_content_url ( $transaction_model[0]->driverId . '/' . $transaction_model[0]->driverProfileImage )) : image_url ( 'app/user.png' ); ?>
                        <td valign="top" align="center"><img class=""
																										src="<?php echo ($transaction_model[0]->driverProfileImage) ? (driver_content_url ( $transaction_model[0]->driverId . '/' . $transaction_model[0]->driverProfileImage )) : image_url ( 'app/user.png' );?>"
																										alt=""
																										style="font-family: Arial, sans-serif; font-size: 20px; color: #000000;"
																										width="130" border="0"></td>
																								</tr>
																							</tbody>
																						</table>
																					</td>
																				</tr>
																				<tr>
																					<td height="15"></td>
																				</tr>
																			</tbody>
																		</table>
																	</div> <!--end img--> <!--[if (gte mso 9)|(IE)]>
                    </td>
                    <td align="center" style="text-align:center;vertical-align:top;font-size:0;">
                    <![endif]-->
																	<div
																		style="display: inline-block; vertical-align: top;">
																		<table cellspacing="0" cellpadding="0" border="0"
																			align="center">
																			<tbody>
																				<tr>
																					<td width="400" align="center">
																						<table width="90%" cellspacing="0" cellpadding="0"
																							border="0" align="center">
																							<!-- title -->
																							<tbody>
																								<tr>
																									<td
																										style="font-family: 'Open Sans', Arial, sans-serif; font-size: 18px; color: #3b3b3b; line-height: 30px; font-weight: bold;"
																										align="left"><?php echo $transaction_model[0]->driverFullName;?></td>
																								</tr>
																								<!-- end title -->

																								<!-- content -->
																								<tr>
																									<td
																										style="font-family: 'Open Sans', Arial, sans-serif; font-size: 14px; color: #7f8c8d; line-height: 28px;"
																										align="left"><?php echo $transaction_model[0]->taxiCategoryTypeName.' : '.$transaction_model[0]->taxiModel;?> 

</td>
																								</tr>

																								<tr>
																									<td
																										style="font-family: 'Open Sans', Arial, sans-serif; font-size: 14px; color: #7f8c8d; line-height: 28px;"
																										align="left">Taxi No : <?php echo $transaction_model[0]->taxiRegistrationNo;?>
</td>
																								</tr>
																								<!-- end content -->

																								<tr>
																									<td align="left">
																										<table class="table-full" cellspacing="0"
																											cellpadding="0" border="0" align="left">
																											<tbody>
																												<tr>
																													<td style="font-family: 'Open Sans', Arial, sans-serif; font-size: 14px; color: #7f8c8d; line-height: 28px;"
																										align="left">Rated : <span
																														style="font-family: 'Open Sans', Arial, sans-serif; font-size: 14px; color: #7f8c8d; line-height: 30px;"><?php echo $transaction_model[0]->driverRated;?></span>


																													</td>
																												</tr>
																											</tbody>
																										</table>
																									</td>
																								</tr>
																								<!-- end button -->

																							</tbody>
																						</table>
																					</td>
																				</tr>
																			</tbody>
																		</table>
																	</div>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
							<tr>
								<td height="5"></td>
							</tr>
						</tbody>
					</table>
				</td>

			</tr>





			<tr>
				<td class="container"
					style="background-color: #fff; background-size: cover; background-position: 50% 100%; background-repeat: no-repeat !important;"
					valign="top" align="center">
					<!-- start container -->

					<table width="100%" cellspacing="0" cellpadding="0" border="0"
						bgcolor="#FFFFFF" align="center">
						<tbody>
							<tr>
								<td align="center">
									<table style="max-width: 600px;" class="table-full" width="600"
										cellspacing="0" cellpadding="0" border="0" align="center">
										<tbody>
											<tr>
												<td height="15"></td>
											</tr>
											<!-- header -->
											<tr>
												<td>
													<table class="table-inner" width="100%" cellspacing="0"
														cellpadding="0" border="0">
														<tbody>
															<tr>
																<td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 17px; color: #000; line-height: 26px; text-transform: uppercase; font-weight: 700"
																	valign="top" width="300" align="left">Pickup Location</td>
																<td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 17px; color: #000; line-height: 26px; text-transform: uppercase; font-weight: 700"
																	valign="top" width="300" align="left">Drop Location</td>

															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<!-- end header -->
											<tr>
												<td style="border-bottom: 3px solid #bcbcbc;" height="10"></td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table> <!-- end title --> <!-- list -->
					<table width="100%" cellspacing="0" cellpadding="0" border="0"
						bgcolor="#FFFFFF" align="center">
						<tbody>
							<tr>
								<td align="center">
									<table style="max-width: 600px;" class="table-full" width="600"
										cellspacing="0" cellpadding="0" border="0">
										<tbody>
											<tr>
												<td height="15"></td>
											</tr>
											<tr>
												<td align="center">
													<table class="table-inner" width="100%" cellspacing="0"
														cellpadding="0" border="0">
														<tbody>
															<tr>

																<td valign="top" width="40" align="left"><img
																	src="<?php echo image_url('/app/pickup.png')?>"></td>
																<td
																	style="font-size: 13px; color: #aaaaaa; font-family: 'Lato', Arial, sans-serif; font-weight: 400;"
																	valign="top" width="260" align="left"><?php echo date("M jS, Y, h:i a",strtotime($transaction_model[0]->actualPickupDatetime));?><br>
																	<span
																	style="font-size: 15px; color: #2a2d32; font-family: 'Lato', Arial, sans-serif; font-weight: 700;"> <?php echo $transaction_model[0]->pickupLocation;?></span></td>
																<td valign="top" width="40" align="left"><img
																	src="<?php echo image_url('/app/drop.png')?>"></td>
																<td
																	style="font-size: 13px; color: #aaaaaa; font-family: 'Lato', Arial, sans-serif; font-weight: 400;"
																	valign="top" width="260" align="left"> <?php echo date("M jS, Y, h:i a",strtotime($transaction_model[0]->dropDatetime));?><br>
																	<span
																	style="font-size: 15px; color: #2a2d32; font-family: 'Lato', Arial, sans-serif; font-weight: 700;"> <?php echo $transaction_model[0]->dropLocation;?></span></td>


															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr>
												<td height="15"></td>
											</tr>
											<tr>
												<td style="border-bottom: 1px solid #ecf0f1;" height="5"></td>
											</tr>
											<tr>
												<td height="5"></td>
											</tr>
											<!-- detail -->

											<!-- end detail -->
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>


			<!-- Bill Start -->
			<tr>
				<td class="container"
					style="background-color: #fff; background-size: cover; background-position: 50% 100%; background-repeat: no-repeat !important;"
					valign="top" align="center">
					<!-- start container -->

					<table width="100%" cellspacing="0" cellpadding="0" border="0"
						bgcolor="#FFFFFF" align="center">
						<tbody>
							<tr>
								<td align="center">
									<table style="max-width: 600px;" class="table-full" width="600"
										cellspacing="0" cellpadding="0" border="0" align="center">
										<tbody>
											<tr>
												<td height="20"></td>
											</tr>
											<!-- header -->
											<tr>
												<td>
													<table class="table-inner" width="100%" cellspacing="0"
														cellpadding="0" border="0">
														<tbody>
															<tr>
																<td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 17px; color: #000; line-height: 26px; text-transform: uppercase; font-weight: 700"
																	valign="top" width="600" align="center">Travel Details</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<!-- end header -->
											<tr>
												<td style="border-bottom: 3px solid #bcbcbc;" height="10"></td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table> <!-- end title --> <!-- list -->
					<table width="100%" cellspacing="0" cellpadding="0" border="0"
						bgcolor="#FFFFFF" align="center">
						<tbody>
							<tr>
								<td align="center">
									<table style="max-width: 600px;" class="table-full" width="600"
										cellspacing="0" cellpadding="0" border="0">
										<tbody>
											<tr>
												<td height="15"></td>
											</tr>
											<tr>
												<td align="center">
													<table class="table-inner" width="100%" cellspacing="0"
														cellpadding="0" border="0">
														<tbody>
															<tr>
																<td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px;"
																	valign="top" width="300" align="left">Base Charge</td>

																<td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px;"
																	valign="top" width="300" align="right"><span
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px;"><?php echo $transaction_model[0]->convenienceCharge;?> </span> <?php echo $transaction_model[0]->currencyCode;?></td>
															</tr>
														</tbody>
													</table>
												</td>

											</tr>

											<tr>
												<td style="border-bottom: 1px solid #ecf0f1;" height="5"></td>
											</tr>
											<tr>
												<td height="5"></td>
											</tr>



											<tr>
												<td align="center">
													<table class="table-inner" width="100%" cellspacing="0"
														cellpadding="0" border="0">
														<tbody>
															<tr>
																<td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px;"
																	valign="top" width="300" align="left">Travel Charge</td>

																<td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px;"
																	valign="top" width="300" align="right"><span
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px;"><?php echo $transaction_model[0]->travelCharge;?> </span> <?php echo $transaction_model[0]->currencyCode;?></td>
															</tr>
														</tbody>
													</table>
												</td>

											</tr>

											<tr>
												<td style="border-bottom: 1px solid #ecf0f1;" height="5"></td>
											</tr>
											<tr>
												<td height="5"></td>
											</tr>
          
          
          <tr>
												<td align="center">
													<table class="table-inner" width="100%" cellspacing="0"
														cellpadding="0" border="0">
														<tbody>
															<tr>
                <?php round($total_bill_details=$transaction_model[0]->convenienceCharge+$transaction_model[0]->travelCharge,2); ?>
                  <td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px; font-weight: bold;"
																	valign="top" width="300" align="left">Total</td>
																<td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px; font-weight: bold;"
																	valign="top" width="300" align="right"><span
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px;"><?php echo number_format($total_bill_details,2);?> </span><?php echo $transaction_model[0]->currencyCode;?></td>
															</tr>
														</tbody>
													</table>
												</td>

											</tr>

											<tr>
												<td style="border-bottom: 1px solid #ecf0f1;" height="5"></td>
											</tr>
											<tr>
												<td height="5"></td>
											</tr>





										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table> <!-- total --> <!-- end total -->

				</td>
			</tr>
			<!-- Bill End -->
			<!-- Tax Start -->
			 <?php if($transaction_model[0]->taxCharge != 0):?>
			<tr>
				<td class="container"
					style="background-color: #fff; background-size: cover; background-position: 50% 100%; background-repeat: no-repeat !important;"
					valign="top" align="center">
					<!-- start container -->

					<table width="100%" cellspacing="0" cellpadding="0" border="0"
						bgcolor="#FFFFFF" align="center">
						<tbody>
							<tr>
								<td align="center">
									<table style="max-width: 600px;" class="table-full" width="600"
										cellspacing="0" cellpadding="0" border="0" align="center">
										<tbody>
											<tr>
												<td height="20"></td>
											</tr>
											<!-- header -->
											<tr>
												<td>
													<table class="table-inner" width="100%" cellspacing="0"
														cellpadding="0" border="0">
														<tbody>
															<tr>
																<td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 17px; color: #000; line-height: 26px; text-transform: uppercase; font-weight: 700"
																	valign="top" width="600" align="center">Tax Details</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<!-- end header -->
											<tr>
												<td style="border-bottom: 3px solid #bcbcbc;" height="10"></td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table> <!-- end title --> <!-- list -->
					<table width="100%" cellspacing="0" cellpadding="0" border="0"
						bgcolor="#FFFFFF" align="center">
						<tbody>
							<tr>
								<td align="center">
									<table style="max-width: 600px;" class="table-full" width="600"
										cellspacing="0" cellpadding="0" border="0">
										<tbody>
											<tr>
												<td height="15"></td>
											</tr>
											<tr>
												<td align="center">
													<table class="table-inner" width="100%" cellspacing="0"
														cellpadding="0" border="0">
														<tbody>
															<tr>
																<td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px;"
																	valign="top" width="300" align="left">Tax Charge(<?php echo $transaction_model[0]->taxPercentage;?>)%</td>

																<td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px;"
																	valign="top" width="300" align="right"><span
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px;"><?php echo $transaction_model[0]->taxCharge;?> </span> <?php echo $transaction_model[0]->currencyCode;?></td>
															</tr>
														</tbody>
													</table>
												</td>

											</tr>
											<tr>
												<td style="border-bottom: 1px solid #ecf0f1;" height="5"></td>
											</tr>
											<tr>
												<td height="5"></td>
											</tr>

          <tr>
												<td align="center">
													<table class="table-inner" width="100%" cellspacing="0"
														cellpadding="0" border="0">
														<tbody>
															<tr>
                
                  <td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px; font-weight: bold;"
																	valign="top" width="300" align="left">Total</td>
																<td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px; font-weight: bold;"
																	valign="top" width="300" align="right"><span
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px;"><?php echo $transaction_model[0]->taxCharge; ?> </span><?php echo $transaction_model[0]->currencyCode;?></td>
															</tr>
														</tbody>
													</table>
												</td>

											</tr>

											<tr>
												<td style="border-bottom: 1px solid #ecf0f1;" height="5"></td>
											</tr>
											<tr>
												<td height="5"></td>
											</tr>





										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table> <!-- total --> <!-- end total -->

				</td>
			</tr>
			<?php endif; ?>
			<!-- Tax End-->
			<!-- Promo Discount Start -->
			<?php if($transaction_model[0]->promoDiscountAmount !=0):?>
			<tr>
				<td class="container"
					style="background-color: #fff; background-size: cover; background-position: 50% 100%; background-repeat: no-repeat !important;"
					valign="top" align="center">
					<!-- start container -->

					<table width="100%" cellspacing="0" cellpadding="0" border="0"
						bgcolor="#FFFFFF" align="center">
						<tbody>
							<tr>
								<td align="center">
									<table style="max-width: 600px;" class="table-full" width="600"
										cellspacing="0" cellpadding="0" border="0" align="center">
										<tbody>
											<tr>
												<td height="20"></td>
											</tr>
											<!-- header -->
											<tr>
												<td>
													<table class="table-inner" width="100%" cellspacing="0"
														cellpadding="0" border="0">
														<tbody>
															<tr>
																<td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 17px; color: #000; line-height: 26px; text-transform: uppercase; font-weight: 700"
																	valign="top" width="600" align="center">Promocode Details</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<!-- end header -->
											<tr>
												<td style="border-bottom: 3px solid #bcbcbc;" height="10"></td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table> <!-- end title --> <!-- list -->
					<table width="100%" cellspacing="0" cellpadding="0" border="0"
						bgcolor="#FFFFFF" align="center">
						<tbody>
							<tr>
								<td align="center">
									<table style="max-width: 600px;" class="table-full" width="600"
										cellspacing="0" cellpadding="0" border="0">
										<tbody>
											<tr>
												<td height="15"></td>
											</tr>
											<tr>
												<td align="center">
													<table class="table-inner" width="100%" cellspacing="0"
														cellpadding="0" border="0">
														<tbody>
															<tr>
																<td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px;"
																	valign="top" width="300" align="left">Promo Discount</td>

																<td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px;"
																	valign="top" width="300" align="right"><span
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px;"><?php echo $transaction_model[0]->promoDiscountAmount;?> </span> <?php echo $transaction_model[0]->currencyCode;?></td>
															</tr>
														</tbody>
													</table>
												</td>

											</tr>

											<tr>
												<td style="border-bottom: 1px solid #ecf0f1;" height="5"></td>
											</tr>
											<tr>
												<td height="5"></td>
											</tr>



											
          
          <tr>
												<td align="center">
													<table class="table-inner" width="100%" cellspacing="0"
														cellpadding="0" border="0">
														<tbody>
															<tr>
               
                  <td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px; font-weight: bold;"
																	valign="top" width="300" align="left">Total</td>
																<td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px; font-weight: bold;"
																	valign="top" width="300" align="right"><span
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px;"><?php echo $transaction_model[0]->promoDiscountAmount;?> </span><?php echo $transaction_model[0]->currencyCode;?></td>
															</tr>
														</tbody>
													</table>
												</td>

											</tr>

											<tr>
												<td style="border-bottom: 1px solid #ecf0f1;" height="5"></td>
											</tr>
											<tr>
												<td height="5"></td>
											</tr>





										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table> <!-- total --> <!-- end total -->

				</td>
			</tr>
			<?php endif; ?>
			<!-- Promodiscount End -->
			<!-- Other Start -->
			<?php if($transaction_model[0]->bookingCharge !=0 || $transaction_model[0]->penaltyCharge !=0 || $transaction_model[0]->parkingCharge !=0 || $transaction_model[0]->tollCharge !=0):?>
			<tr>
				<td class="container"
					style="background-color: #fff; background-size: cover; background-position: 50% 100%; background-repeat: no-repeat !important;"
					valign="top" align="center">
					<!-- start container -->

					<table width="100%" cellspacing="0" cellpadding="0" border="0"
						bgcolor="#FFFFFF" align="center">
						<tbody>
							<tr>
								<td align="center">
									<table style="max-width: 600px;" class="table-full" width="600"
										cellspacing="0" cellpadding="0" border="0" align="center">
										<tbody>
											<tr>
												<td height="20"></td>
											</tr>
											<!-- header -->
											<tr>
												<td>
													<table class="table-inner" width="100%" cellspacing="0"
														cellpadding="0" border="0">
														<tbody>
															<tr>
																<td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 17px; color: #000; line-height: 26px; text-transform: uppercase; font-weight: 700"
																	valign="top" width="600" align="center">Other Details</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<!-- end header -->
											<tr>
												<td style="border-bottom: 3px solid #bcbcbc;" height="10"></td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table> <!-- end title --> <!-- list -->
					<table width="100%" cellspacing="0" cellpadding="0" border="0"
						bgcolor="#FFFFFF" align="center">
						<tbody>
							<tr>
								<td align="center">
									<table style="max-width: 600px;" class="table-full" width="600"
										cellspacing="0" cellpadding="0" border="0">
										<tbody>
											<tr>
												<td height="15"></td>
											</tr>
											
          <?php if($transaction_model[0]->bookingCharge!=0):?>
          <tr>
												<td align="center">
													<table class="table-inner" width="100%" cellspacing="0"
														cellpadding="0" border="0">
														<tbody>
															<tr>
																<td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px;"
																	valign="top" width="300" align="left">Booking Charge</td>

																<td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px;"
																	valign="top" width="300" align="right"><span
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px;"><?php echo $transaction_model[0]->bookingCharge;?> </span> <?php echo $transaction_model[0]->currencyCode;?></td>
															</tr>
														</tbody>
													</table>
												</td>

											</tr>

											<tr>
												<td style="border-bottom: 1px solid #ecf0f1;" height="5"></td>
											</tr>
											<tr>
												<td height="5"></td>
											</tr>
          <?php endif;?>
          <?php if($transaction_model[0]->penaltyCharge!=0):?>
          <tr>
												<td align="center">
													<table class="table-inner" width="100%" cellspacing="0"
														cellpadding="0" border="0">
														<tbody>
															<tr>
																<td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px;"
																	valign="top" width="300" align="left">Penalty Charge</td>

																<td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px;"
																	valign="top" width="300" align="right"><span
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px;"><?php echo $transaction_model[0]->penaltyCharge;?> </span><?php echo $transaction_model[0]->currencyCode;?></td>
															</tr>
														</tbody>
													</table>
												</td>

											</tr>

											<tr>
												<td style="border-bottom: 1px solid #ecf0f1;" height="5"></td>
											</tr>
											<tr>
												<td height="5"></td>
											</tr>
          <?php endif;?>
          <?php if($transaction_model[0]->parkingCharge!=0):?>
          <tr>
												<td align="center">
													<table class="table-inner" width="100%" cellspacing="0"
														cellpadding="0" border="0">
														<tbody>
															<tr>
																<td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px;"
																	valign="top" width="300" align="left">Parking Charge</td>

																<td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px;"
																	valign="top" width="300" align="right"><span
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px;"><?php echo $transaction_model[0]->parkingCharge;?> </span> <?php echo $transaction_model[0]->currencyCode;?></td>
															</tr>
														</tbody>
													</table>
												</td>

											</tr>

											<tr>
												<td style="border-bottom: 1px solid #ecf0f1;" height="5"></td>
											</tr>
											<tr>
												<td height="5"></td>
											</tr>
          <?php endif;?>
          <?php if($transaction_model[0]->tollCharge!=0):?>
          <tr>
												<td align="center">
													<table class="table-inner" width="100%" cellspacing="0"
														cellpadding="0" border="0">
														<tbody>
															<tr>
																<td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px;"
																	valign="top" width="300" align="left">Toll Charge</td>

																<td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px;"
																	valign="top" width="300" align="right"><span
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px;"><?php echo $transaction_model[0]->tollCharge;?> </span><?php echo $transaction_model[0]->currencyCode;?></td>
															</tr>
														</tbody>
													</table>
												</td>

											</tr>

											<tr>
												<td style="border-bottom: 1px solid #ecf0f1;" height="5"></td>
											</tr>
											<tr>
												<td height="5"></td>
											</tr>
          <?php endif;?>
          
          <tr>
												<td align="center">
													<table class="table-inner" width="100%" cellspacing="0"
														cellpadding="0" border="0">
														<tbody>
															<tr>
                <?php round($total_bill_details=$transaction_model[0]->bookingCharge+$transaction_model[0]->penaltyCharge+$transaction_model[0]->parkingCharge+$transaction_model[0]->tollCharge,2); ?>
                  <td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px; font-weight: bold;"
																	valign="top" width="300" align="left">Total</td>
																<td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px; font-weight: bold;"
																	valign="top" width="300" align="right"><span
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px;"><?php echo number_format($total_bill_details,2);?> </span><?php echo $transaction_model[0]->currencyCode;?></td>
															</tr>
														</tbody>
													</table>
												</td>

											</tr>

											<tr>
												<td style="border-bottom: 1px solid #ecf0f1;" height="5"></td>
											</tr>
											<tr>
												<td height="5"></td>
											</tr>





										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table> <!-- total --> <!-- end total -->

				</td>
			</tr>
			<?php endif;?>
			<!-- Other End -->
			<!-- Payment Start -->
			<tr>
				<td class="container"
					style="background-color: #fff; background-size: cover; background-position: 50% 100%; background-repeat: no-repeat !important;"
					valign="top" align="center">
					<!-- start container -->

					<table width="100%" cellspacing="0" cellpadding="0" border="0"
						bgcolor="#FFFFFF" align="center">
						<tbody>
							<tr>
								<td align="center">
									<table style="max-width: 600px;" class="table-full" width="600"
										cellspacing="0" cellpadding="0" border="0" align="center">
										<tbody>
											<tr>
												<td height="20"></td>
											</tr>
											<!-- header -->
											<tr>
												<td>
													<table class="table-inner" width="100%" cellspacing="0"
														cellpadding="0" border="0">
														<tbody>
															<tr>
																<td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 17px; color: #000; line-height: 26px; text-transform: uppercase; font-weight: 700"
																	valign="top" width="600" align="center">Payment Details</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<!-- end header -->
											<tr>
												<td style="border-bottom: 3px solid #bcbcbc;" height="10"></td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table> <!-- end title --> <!-- list -->
					<table width="100%" cellspacing="0" cellpadding="0" border="0"
						bgcolor="#FFFFFF" align="center">
						<tbody>
							<tr>
								<td align="center">
									<table style="max-width: 600px;" class="table-full" width="600"
										cellspacing="0" cellpadding="0" border="0">
										<tbody>
											<tr>
												<td height="15"></td>
											</tr>
											
          
          <tr>
												<td align="center">
													<table class="table-inner" width="100%" cellspacing="0"
														cellpadding="0" border="0">
														<tbody>
															<tr>
																<td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px; font-weight: bold;"
																	valign="top" width="300" align="left">Wallet</td>

																<td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px; font-weight: bold;"
																	valign="top" width="300" align="right"><span
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px; font-weight: bold;"><?php echo $transaction_model[0]->walletPaymentAmount;?> </span> <?php echo $transaction_model[0]->currencyCode;?></td>
															</tr>
														</tbody>
													</table>
												</td>

											</tr>

											<tr>
												<td style="border-bottom: 1px solid #ecf0f1;" height="5"></td>
											</tr>
											<tr>
												<td height="5"></td>
											</tr>
          
          
          <tr>
												<td align="center">
													<table class="table-inner" width="100%" cellspacing="0"
														cellpadding="0" border="0">
														<tbody>
															<tr>
																<td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px; font-weight: bold;"
																	valign="top" width="300" align="left">Cash</td>

																<td
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px; font-weight: bold;"
																	valign="top" width="300" align="right"><span
																	style="font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; color: #3b3b3b; line-height: 26px; font-weight: bold;"><?php echo number_format($transaction_model[0]->totalTripCost-$transaction_model[0]->walletPaymentAmount,2);?> </span><?php echo $transaction_model[0]->currencyCode;?></td>
															</tr>
														</tbody>
													</table>
												</td>

											</tr>

											
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table> <!-- total --> <!-- end total -->

				</td>
			</tr>
			<!-- Payment End -->
			<tr>
				<td class="container"
					style="background-color: #fff; border-bottom: 1px solid #ededed"
					valign="top" align="center">
					<!-- start container -->

					<table class="container"
						style="min-width: 600px; height =900px; margin: 0px auto; width: 600px; background-color: #fff;"
						width="600" cellspacing="0" cellpadding="0" border="0"
						align="center">
						<!-- start space -->
						<tbody>
							<tr>
								<td
									style="font-size: 0px; line-height: 0; border-collapse: collapse;"
									valign="top" height="20"></td>
							</tr>
							<!-- end space -->
							<!-- start content -->











							<tr>
								<td
									style="font-size: 0px; line-height: 0; border-collapse: collapse;"
									valign="top" height="30"></td>
							</tr>
							<!-- end space -->
						</tbody>
					</table> <!-- end container -->
				</td>
			</tr>
			<!--END LAYOUT-4 ( 2-COL IMAGE / TEXT / BUTTON )-->

		</tbody>
	</table>
</div>