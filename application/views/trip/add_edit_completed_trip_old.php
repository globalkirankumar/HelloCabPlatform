<?php
$view_mode = $mode;
?>
<!-- BEGIN CONTENT -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700,300|Roboto:400,300,700&amp;subset=latin,cyrillic,greek" rel="stylesheet" type="text/css">

<!-- BEGIN CONTENT BODY -->
<div class="page-content">

	<!---- Page Bar ends ---->
	
	<table id="mainStructure" style="border: 1px solid #dedede; box-shadow: 8px 12px 23px -6px rgba(208,210,212,1);" align="center" width="700" cellspacing="0" cellpadding="0" border="0">
  <!--START LAYOUT-1 ( View Online / Call )-->
  <tbody><tr>
    <td class="container" style="background-color: #293896;" align="center" valign="top"><!-- start container -->
      
      <table class="container" style="min-width: 600px; margin: 0px auto; padding-left: 20px; padding-right: 20px; width: 600px; background-color: #293896;" align="center" width="600" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
          <td valign="top"><table class="full-width" style="margin: 0px auto; width: 560px;" align="center" width="560" cellspacing="0" cellpadding="0" border="0">
              <!-- start space -->
              <tbody><tr>
                <td valign="top" height="20"></td>
              </tr>
              <!-- end space -->
              <tr>
                <td valign="top"><table align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                      <td valign="middle"><table class="full-width-center" dir="ltr" align="left" width="auto" cellspacing="0" cellpadding="0" border="0">
                          <tbody><tr>
                            <td dup="0" align="center" valign="top"><table align="center" width="auto" cellspacing="0" cellpadding="0" border="0">
                                <tbody><tr>
                                  <td style="font-size: 14px; line-height: 24px; color: rgb(255, 255, 255); font-weight: normal; text-align: left; font-family: 'Open Sans', Arial, Helvetica, sans-serif; word-break: break-word;" align="left"><?php echo date('d-m-Y',strtotime($transaction_model[0]->invoiceDatetime));?>&nbsp;</td>
                                </tr>
                              </tbody></table></td>
                          </tr>
                        </tbody></table>
                        <!--[if (gte mso 9)|(IE)]></td><td valign="top"><![endif]-->
                        <table class="full-width" style="height: 1px; border-spacing: 0px; width: 20px;" dir="ltr" align="left" width="20" cellspacing="0" cellpadding="0" border="0">
                          <tbody><tr>
                            <td class="h-20" height="1"></td>
                          </tr>
                        </tbody></table>
                        <!--[if (gte mso 9)|(IE)]></td><td valign="top"><![endif]-->
                        <table class="auto-center" dir="ltr" align="right" width="auto" cellspacing="0" cellpadding="0" border="0">
                          <tbody><tr>
                            <td dup="0" align="left" valign="top"><table align="left" width="auto" cellspacing="0" cellpadding="0" border="0">
                                <tbody><tr>
                                  <td style="font-size: 14px; line-height: 24px; color: rgb(255, 255, 255); font-weight: normal; text-align: left; font-family: 'Open Sans', Arial, Helvetica, sans-serif; word-break: break-word;" align="left"><span style="text-decoration: none; color: rgb(255, 255, 255); font-size: inherit; line-height: 24px;"> Invoice No. : <?php echo $transaction_model[0]->invoiceNo;?> </span></td>
                                </tr>
                              </tbody></table></td>
                          </tr>
                        </tbody></table></td>
                    </tr>
                  </tbody></table></td>
              </tr>
              <!-- start space -->
              <tr>
                <td valign="top" height="20"></td>
              </tr>
              <!-- end space -->
            </tbody></table></td>
        </tr>
      </tbody></table>
      
      <!-- end container --></td>
  </tr>
  <!--END LAYOUT-1 ( View Online / Call )--> 
  
  <!--START LAYOUT-2 ( LOGO / MENU )-->
  <tr>
    <td class="container" style="background-color: #ffffff;" align="center" valign="top"><!-- start container -->
      
      <table class="container" style="min-width: 600px; padding-left: 20px; padding-right: 20px; width: 600px; background-color: rgb(255, 255, 255);" align="center" width="600" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
          <td valign="top"><table class="full-width" style="margin:0 auto;" align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
              <!-- start space -->
              <tbody><tr>
                <td valign="top" height="15"></td>
              </tr>
              <!-- end space -->
              <tr>
                <td valign="top"><table align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
                    <!-- start image -->
                    <tbody><tr>
                      <td style="width: 125px;" align="center" width="125" valign="top"><img src="https://images.benchmarkemail.com/client668733/image4652565.png" alt="set6-iogo-footer" style="max-width: 240px; display: block !important; " width="102" vspace="0" hspace="0" height="98" border="0"></td>
                    </tr>
                    <!-- end image--> 
                    <!-- start space -->
                    <tr>
                      <td valign="top" height="15"></td>
                    </tr>
                    <!-- end space -->
                  </tbody></table></td>
              </tr>
            </tbody></table></td>
        </tr>
      </tbody></table>
      
      <!-- end container --></td>
  </tr>
  <!--END LAYOUT-2 ( LOGO / MENU )--> 
  
  <!--START LAYOUT-3 ( BG IMAGE / TEXT-CENTER / BUTTON )-->
  <tr>
    <td class="container" style="background-color: #fff; background-size: cover; background-position: 50% 100%; background-repeat: no-repeat !important;" align="center" valign="top"><!-- start container -->
      
      <table class="container" style="min-width: 600px; margin: 0px auto; width: 600px;" align="center" width="600" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
          <td align="center" valign="top"><table class="full-width" style="margin:0 auto;" align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
              <!-- start space -->
              <tbody><tr>
                <td valign="top" height="20"></td>
              </tr>
              <!-- end space --> 
              
              <!-- start border content -->
              <tr dup="0">
                <td class="clear-pad" style="padding-left:20px; padding-right:20px;" valign="top"><table class="auto-center" align="center" width="auto" cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                      <td valign="top"><table align="left" width="100%" cellspacing="0" cellpadding="0" border="0">
                          <!-- start border top  -->
                          <tbody><tr>
                            <td style="opacity:0.8; background-color:#293896; " width="100%" valign="top" height="5"><table align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tbody><tr>
                                  <td width="100%" valign="top" height="5"></td>
                                </tr>
                              </tbody></table></td>
                          </tr>
                          <!-- end border top -->
                          <tr>
                            <td valign="top"><table align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tbody><tr> 
                                  <!-- start border left  -->
                                  <td style="opacity: 0.8; width: 5px; background-color: #293896;" width="5" valign="top" height="100%"><table style="min-width: 5px !important; max-width: 5px !important; width: 5px;" align="center" width="5" cellspacing="0" cellpadding="0" border="0">
                                      <tbody><tr>
                                        <td style="font-size: 0px; line-height: 0; border-collapse: collapse; width: 5px;" width="5" valign="top" height="100%"></td>
                                      </tr>
                                    </tbody></table></td>
                                  <!-- end border left -->
                                  <td valign="top"><table align="left" width="100%" cellspacing="0" cellpadding="0" border="0">
                                      <!-- start space -->
                                      <tbody><tr>
                                        <td valign="top" height="20"></td>
                                      </tr>
                                      <!-- end space -->
                                      <tr>
                                        <td style="padding-left:20px; padding-right:20px;" valign="top"><table align="left" width="100%" cellspacing="0" cellpadding="0" border="0">
                                            <!-- start title -->
                                            <tbody><tr dup="0">
                                              <td align="center" valign="top"><table class="full-width-center" style="margin:0 auto;" align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                  <tbody><tr>
                                                    <td style="font-size: 22px; color: rgb(255, 255, 255); font-weight: bold; text-align: center; font-family: 'Open Sans', Arial, Helvetica, sans-serif; word-break: break-word;" align="center"><span style="color:#293896; font-size: 48px; font-weight: bold; line-height: 48px;"><?php echo $transaction_model[0]->totalTripCost;?></span> <span style="color: rgb(0, 0, 0); font-weight: bold; font-size: inherit; line-height: 24px;"><?php echo $transaction_model[0]->currencyCode;?>&nbsp;&nbsp;</span></td>
                                                  </tr>
                                                  <!-- start space -->
                                                  <tr>
                                                    <td valign="top" height="20"></td>
                                                  </tr>
                                                  <!-- end space -->
                                                </tbody></table></td>
                                            </tr>
                                            <!-- end title -->
                                          </tbody></table></td>
                                      </tr>
                                    </tbody></table></td>
                                  <!-- start border right  -->
                                  <td style="opacity: 0.8; width: 5px; background-color: #293896;" width="5" valign="top" height="100%"><table style="min-width: 5px !important; max-width: 5px !important; width: 5px;" align="center" width="5" cellspacing="0" cellpadding="0" border="0">
                                      <tbody><tr>
                                        <td style="font-size: 0px; line-height: 0; border-collapse: collapse; width: 5px;" width="5" valign="top" height="100%"></td>
                                      </tr>
                                    </tbody></table></td>
                                  <!-- end border right --> 
                                </tr>
                              </tbody></table></td>
                          </tr>
                          <!-- start border bottom  -->
                          <tr>
                            <td style="opacity:0.8; background-color:#293896; " width="100%" valign="top" height="5"><table align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tbody><tr>
                                  <td width="100%" valign="top" height="5"></td>
                                </tr>
                              </tbody></table></td>
                          </tr>
                          <!-- end border bottom -->
                        </tbody></table></td>
                    </tr>
                  </tbody></table></td>
              </tr>
              <!-- end border content -->
              
              <tr>
                <td class="clear-pad" style="padding-left:20px; padding-right:20px;" align="center" valign="top"><table class="full-width-center" style="margin:0 auto;" align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
                    <!-- start space -->
                    <tbody><tr>
                      <td valign="top" height="10"></td>
                    </tr>
                    <!-- end space --> 
                    <!-- start title -->
                    <tr dup="0">
                      <td align="center" valign="top"><table class="full-width-center" style="margin:0 auto;" align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
                          <tbody><tr>
                            <td style="font-size: 15px; color: rgb(51, 51, 51); font-weight: bold; text-align: center; font-family: 'Open Sans', Arial, Helvetica, sans-serif; word-break: break-word;" align="center"><span style="color: rgb(51, 51, 51); font-weight: bold; font-size: inherit; line-height: 24px;"><?php echo $transaction_model[0]->customerReferenceCode;?></span></td>
                          </tr>
                          <!-- start space -->
                          <tr>
                            <td valign="top" height="5"></td>
                          </tr>
                          <!-- end space -->
                          <tr>
                            <td style="font-size: 28px; color: rgb(51, 51, 51); font-weight: 300; text-align: center; font-family: 'Open Sans', Arial, Helvetica, sans-serif; word-break: break-word;" align="center"><span style="color: rgb(51, 51, 51); font-weight: 300; font-size: inherit; line-height: 28px;">Thanks for traveling with us, <span style="color: rgb(51, 51, 51); font-weight: bold; font-size: inherit; line-height: 24px;"><?php echo $transaction_model[0]->passengerFullName;?></span> </span></td>
                          </tr>
                        </tbody></table></td>
                    </tr>
                    <!-- end title -->
                    
                  </tbody></table></td>
              </tr>
              <!-- start space -->
              <tr>
                <td valign="top" height="20"></td>
              </tr>
              <!-- end space -->
            </tbody></table></td>
        </tr>
      </tbody></table>
      
      <!-- end container --></td>
  </tr>
  <!--END LAYOUT-3 ( BG IMAGE / TEXT-CENTER / BUTTON )--> 
  
  <!--START LAYOUT-4 ( 2-COL IMAGE / TEXT / BUTTON )-->
  
  <tr>
    <td class="container" style="background-color:#fff; border-bottom: 1px solid #ededed" align="center" valign="top"><!-- start container -->
      
      <table class="container" style="min-width: 600px; margin: 0px auto; width: 600px; background-color: #fff;" align="center" width="600" cellspacing="0" cellpadding="0" border="0">
        <!-- start space -->
        <tbody><tr>
          <td style="font-size: 0px; line-height: 0; border-collapse: collapse;" valign="top" height="20"></td>
        </tr>
        <!-- end space --><!-- start content -->
        <tr>
          <td class="clear-pad" style="padding-left:20px; padding-right:20px;" align="center" valign="top"><table class="full-width-center" style="margin:0 auto;" align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
              <!-- start title -->
              <tbody><tr dup="0">
                <td align="center" valign="top"><table style="margin:0 auto;" align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                      <td style="font-size: 24px; color: #2a2d32; font-weight: bold; text-align: center; font-family: 'Open Sans', Arial, Helvetica, sans-serif; word-break: break-word;" align="center"><span style="color: #2a2d32; font-weight: bold; font-size: inherit; line-height: 24px;">Ride Details</span></td>
                    </tr>
                    <!-- start space -->
                    <tr>
                      <td style="font-size: 0px; line-height: 0; border-collapse: collapse;" valign="top" height="10"></td>
                    </tr>
                    <!-- end space -->
                  </tbody></table></td>
              </tr>
              <!-- end title --><!-- start space -->
              <tr>
                <td style="font-size: 0px; line-height: 0; border-collapse: collapse;" valign="top" height="10"></td>
              </tr>
              <!-- end space -->
            </tbody></table></td>
            
            
            
            
            
            
            
            
            
                      <td class="clear-pad" style="padding-left:20px; padding-right:20px;" align="center" valign="top"><table class="full-width-center" style="margin:0 auto;" align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
              <!-- start title -->
              <tbody><tr dup="0">
                <td align="center" valign="top"><table style="margin:0 auto;" align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                      <td style="font-size: 24px; color: #2a2d32; font-weight: bold; text-align: center; font-family: 'Open Sans', Arial, Helvetica, sans-serif; word-break: break-word;" align="center"><span style="color: #2a2d32; font-weight: bold; font-size: inherit; line-height: 24px;">Bill Details</span></td>
                    </tr>
                    <!-- start space -->
                    <tr>
                      <td style="font-size: 0px; line-height: 0; border-collapse: collapse;" valign="top" height="10"></td>
                    </tr>
                    <!-- end space -->
                  </tbody></table></td>
              </tr>
              <!-- end title --><!-- start space -->
              <tr>
                <td style="font-size: 0px; line-height: 0; border-collapse: collapse;" valign="top" height="10"></td>
              </tr>
              <!-- end space -->
            </tbody></table></td>
        </tr>
        <!-- end content --><!-- start content container-->
        
        <tr dup="0"><!-- start duplicate all -->
        			
				 
					<td class="em_pad" width="100%" valign="top">	
					
						<table class="full" style="border-top-right-radius: 5px; border-top-left-radius: 5px; background-color: #fafafa;
border: 1px solid #dedede; box-shadow: 8px 12px 23px -6px rgba(208,210,212,1);" align="left" width="310" cellspacing="0" cellpadding="0" border="0">
							<tbody><tr>
								<td style="-webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; " align="center" width="100%" valign="middle" bgcolor="#fafafa">
								
							<div class="sortable_inner">
				
								<table object="drag-module-small" align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
									 <tbody><tr>
										 <td class="em_h30" height="20"></td>
									</tr>
								</tbody></table>
                                
                                <table object="drag-module-small" align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
									<tbody><tr>
									  <td align="center" valign="top">
									  <?php $driver_profile_image=($transaction_model[0]->driverProfileImage) ? (driver_content_url ( $transaction_model[0]->driverId . '/' . $transaction_model[0]->driverProfileImage )) : image_url ( 'app/user.png' ); ?>
										<img class="" src="<?php echo ($transaction_model[0]->driverProfileImage) ? (driver_content_url ( $transaction_model[0]->driverId . '/' . $transaction_model[0]->driverProfileImage )) : image_url ( 'app/user.png' );?>" alt="" style="font-family:Arial, sans-serif; font-size:20px; color:#000000;" width="90" border="0">
									  </td>
									</tr>
                                    <tr>
										 <td class="em_h20" height="22"></td>
									</tr>
								</tbody></table>
							
								<table class="em_wrapper" object="drag-module-small" align="center" width="300" cellspacing="0" cellpadding="0" border="0">
									<tbody><tr>
										<td align="center" width="100%" valign="middle">
										
											<table style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter" align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
												<tbody><tr>
													<td style="font-size:26px; color:#2a2d32; font-family:'Lato',Arial, sans-serif; font-weight:700;" align="center" width="100%"><?php echo $transaction_model[0]->driverFullName;?></td>
												</tr>
											</tbody></table>							
										</td>
									</tr>
								</tbody></table>
								<table object="drag-module-small" align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
									<tbody><tr>
									  <td height="10"></td>
									</tr>
								</tbody></table>
								
								<table class="full" object="drag-module-small" align="center" width="250" cellspacing="0" cellpadding="0" border="0">
									<tbody><tr>
										<td class="em_pad" align="center" width="250" valign="middle">
										
											<table style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter" align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
												<tbody><tr>
													<td style="font-size:16px; color:rgb(51, 51, 51); font-family:'Lato',Arial, sans-serif; font-weight:400; line-height:22px; text-align:center;" align="center" width="100%"><?php echo $transaction_model[0]->taxiCategoryTypeName.'-'.$transaction_model[0]->taxiModel;?></td>
												</tr>
											</tbody></table>							
										</td>
									</tr>
								</tbody></table>
								
							 <table align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
									<tbody><tr>
									  <td height="1" bgcolor="#e9ebf4"></td>
									</tr>
							  </tbody></table>
                   
                              <table align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
									<tbody><tr>
									  <td height="1" bgcolor="#e9ebf4"></td>
									</tr>
								</tbody></table>
								
                            <table class="full" object="drag-module-small" align="center" width="250" cellspacing="0" cellpadding="0" border="0">
                            
                            
                           
                            
                            <tbody><tr>
                             <td style="padding-right:20px;">
                            <img src="https://images.benchmarkemail.com/client668733/image4652564.png">
                            </td>
                              <td class="em_pad2" valign="top">
                            <table class="full" object="drag-module-small" align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody><tr>
                              <td colspan="2" class="em_h30" height="10"></td>
                            </tr>
                            
                            <tr>
                           <td style="font-size:13px; color:#aaaaaa; font-family:'Lato',Arial, sans-serif; font-weight:400;" align="left" width="63%" valign="middle">
                                          	<?php echo date('',strtotime($transaction_model[0]->actualPickupDatetime));?>
                                    <span style="font-size:15px; color:#2a2d32; font-family:'Lato',Arial, sans-serif; font-weight:700;"> <?php echo $transaction_model[0]->pickupLocation;?> </span>
                                        
                                         
                                      </td>
                            </tr>
                            <tr>
                              <td colspan="2" height="13"></td>
                            </tr>
                            <tr>
                              <td colspan="2" height="1" bgcolor="e9ebf4"></td>
                            </tr>
                            </tbody></table>		 
                            
                            <table class="full" object="drag-module-small" align="center" width="250" cellspacing="0" cellpadding="0" border="0">
                            <tbody><tr>
                              <td colspan="2" height="13"></td>
                            </tr>
                            
                            <tr>
                            <td style="font-size:13px; color:#aaaaaa; font-family:'Lato',Arial, sans-serif; font-weight:400;" align="left" width="63%" valign="middle">
                                      <?php echo date('',strtotime($transaction_model[0]->dropDatetime));?>
                                    <span style="font-size:15px; color:#2a2d32; font-family:'Lato',Arial, sans-serif; font-weight:700;"> <?php echo $transaction_model[0]->dropLocation;?></span>
                                        
                                         
                                      </td>
                            </tr>
                            <tr>
                              <td colspan="2" height="13"></td>
                            </tr>
                            <tr>
                              <td colspan="2" height="1" bgcolor="e9ebf4"></td>
                            </tr>
                            </tbody></table>
                          
                            
                            </td>
                            </tr>
                            </tbody></table>
								
								
								 
								
								 
								 
								
								<table style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter" align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
												<tbody><tr>
													<td class="em_h30" width="100%" height="30"> </td>
												</tr>
											</tbody></table>
                                            
                                            
                                            <table class="full" style="border-top-right-radius: 5px; border-top-left-radius: 5px;" align="center" width="100%" height="232" cellspacing="0" cellpadding="0" border="0">
									<tbody><tr>
										<td style="border-top-right-radius: 5px; border-top-left-radius: 5px;" valign="bottom">											 
											<iframe src="https://www.google.com/maps/embed?pb=!1m24!1m8!1m3!1d62224.426051902556!2d77.57493870466308!3d12.906009272664315!3m2!1i1024!2i768!4f13.1!4m13!3e6!4m5!1s0x3bae150de1038031%3A0xb98bf46f66302cd!2s2nd+Phase%2C+JP+Nagar%2C+Bengaluru%2C+Karnataka!3m2!1d12.9147764!2d77.5882591!4m5!1s0x3bae6c8a7750e1c3%3A0x4a5cfc0fce5af71d!2sElectronic+City%2C+Bengaluru%2C+Karnataka!3m2!1d12.8399389!2d77.6770031!5e0!3m2!1sen!2sin!4v1501668786208" style="border:0" allowfullscreen="" width="310" height="225" frameborder="0"></iframe>
										</td>  
									</tr>
									</tbody></table>
								

							</div>	
									
								</td>
							</tr>
						</tbody></table>
						
					</td>
		

















					<td class="em_pad" width="100%" valign="top">	
					
						<table class="full" style="border-top-right-radius: 5px; border-top-left-radius: 5px; background-color: #fafafa;
border: 1px solid #dedede; box-shadow: 8px 12px 23px -6px rgba(208,210,212,1);" align="right" width="280" cellspacing="0" cellpadding="0" border="0">
							<tbody><tr>
								<td style="-webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px;" align="center" width="100%" valign="middle" bgcolor="#fafafa">
								
							<div class="sortable_inner">
				
                         
                         
                         
                         
                         
                         
                         
                         
                         
                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                             
							
						
                 
                                 <table align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
									<tbody><tr>
									  <td height="1" bgcolor="#e9ebf4"></td>
									</tr>
								</tbody></table>
								 <table align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
									<tbody><tr>
									  <td class="em_h30" height="20"></td>
									</tr>
								</tbody></table> 
								 
                            <table class="full" object="drag-module-small" align="center" width="250" cellspacing="0" cellpadding="0" border="0">
                            <tbody><tr>
                              <td class="em_pad2" valign="top">
                            		 
                            
                         
                            		<table class="em_wrapper" object="drag-module-small" align="center" cellspacing="0" cellpadding="0" border="0">
									<tbody><tr>
										<td align="center" width="100%" valign="middle">
										
											<table style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter" align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
												<tbody><tr>
													<td style="font-size:16px; color:#2a2d32; font-family:'Lato',Arial, sans-serif; font-weight:700;" align="center" width="100%">FARE BREAKUP </td>
												</tr>
											</tbody></table>							
										</td>
									</tr>
								</tbody></table>
                            
                            
                            
                            
                            
                            
                            
                          <?php if ($transaction_model[0]->convenienceCharge>0):?>  
                       <table class="full" object="drag-module-small" align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody><tr>
                              <td colspan="2" class="em_h30" height="20"></td>
                            </tr>
                            
                            <tr>
                            <td style="font-size:15px; color:#aaaaaa; font-family:'Lato',Arial, sans-serif; font-weight:500; line-height:22px;" align="left"> Base Charge   </td>
                            <td align="center" width="100">
                             <table class="mcenter" width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                          <td style="font-size:15px;  font-family:'Lato',Arial, sans-serif; font-weight:500; color:#aaaaaa; border-radius:5px; padding-left:5px; padding-right:5px;" align="center" valign="middle" height="21">
                                      <span style="font-size:20px;  font-family:'Lato',Arial, sans-serif; font-weight:500; color:#aaaaaa; border-radius:5px; text-align:right;"><?php echo $transaction_model[0]->convenienceCharge;?></span></td>
                                      
                                      <td style="font-size:15px;  font-family:'Lato',Arial, sans-serif; font-weight:500; color:#aaaaaa; border-radius:5px; padding-left:5px; padding-right:5px;" align="center" valign="middle" height="21">
                                      <span style="text-decoration:none; color:#aaaaaa; line-height:18px; display:block; " object="link-editable"><?php echo $transaction_model[0]->currencyCode;?></span></td>
                                        </tr>
                                      </tbody></table>
                                 
                            </td>
                            </tr>
                            
                            <tr>
                              <td colspan="2" height="13"></td>
                            </tr>
                            <tr>
                              <td colspan="2" height="1" bgcolor="e9ebf4"></td>
                            </tr>
                            </tbody></table>
                            <?php endif;?>
                            <?php if ($transaction_model[0]->travelCharge>0):?>
                            <table class="full" object="drag-module-small" align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody><tr>
                              <td colspan="2" class="em_h30" height="20"></td>
                            </tr>
                             
                            <tr>
                            <td style="font-size:15px; color:#aaaaaa; font-family:'Lato',Arial, sans-serif; font-weight:500; line-height:22px;" align="left"> Transit Charge  </td>
                            <td align="center" width="100">
                             <table class="mcenter" width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                          <td style="font-size:15px;  font-family:'Lato',Arial, sans-serif; font-weight:500; color:#aaaaaa; border-radius:5px; padding-left:5px; padding-right:5px;" align="center" valign="middle" height="21">
                                      <span style="font-size:20px;  font-family:'Lato',Arial, sans-serif; font-weight:500; color:#aaaaaa; border-radius:5px; text-align:right;"><?php echo $transaction_model[0]->travelCharge;?></span></td>
                                      
                                      <td style="font-size:15px;  font-family:'Lato',Arial, sans-serif; font-weight:500; color:#aaaaaa; border-radius:5px; padding-left:5px; padding-right:5px;" align="center" valign="middle" height="21">
                                      <span style="text-decoration:none; color:#aaaaaa; line-height:18px; display:block; " object="link-editable"><?php echo $transaction_model[0]->currencyCode;?></span></td>
                                        </tr>
                                      </tbody></table>
                                 
                            </td>
                            </tr>
                             
                            <tr>
                              <td colspan="2" height="13"></td>
                            </tr>
                            <tr>
                              <td colspan="2" height="1" bgcolor="e9ebf4"></td>
                            </tr>
                            </tbody></table>
                            <?php endif;?>
                            <?php if ($transaction_model[0]->bookingCharge>0):?>
                            <table class="full" object="drag-module-small" align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody><tr>
                              <td colspan="2" class="em_h30" height="20"></td>
                            </tr>
                             
                            <tr>
                            <td style="font-size:15px; color:#aaaaaa; font-family:'Lato',Arial, sans-serif; font-weight:500; line-height:22px;" align="left"> Booking Charge   </td>
                            <td align="center" width="100">
                             <table class="mcenter" width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                          <td style="font-size:15px;  font-family:'Lato',Arial, sans-serif; font-weight:500; color:#aaaaaa; border-radius:5px; padding-left:5px; padding-right:5px;" align="center" valign="middle" height="21">
                                      <span style="font-size:20px;  font-family:'Lato',Arial, sans-serif; font-weight:500; color:#aaaaaa; border-radius:5px; text-align:right;"><?php echo $transaction_model[0]->bookingCharge;?></span></td>
                                      
                                      <td style="font-size:15px;  font-family:'Lato',Arial, sans-serif; font-weight:500; color:#aaaaaa; border-radius:5px; padding-left:5px; padding-right:5px;" align="center" valign="middle" height="21">
                                      <span style="text-decoration:none; color:#aaaaaa; line-height:18px; display:block; " object="link-editable"><?php echo $transaction_model[0]->currencyCode;?></span></td>
                                        </tr>
                                      </tbody></table>
                                 
                            </td>
                            </tr>
                             
                            <tr>
                              <td colspan="2" height="13"></td>
                            </tr>
                            <tr>
                              <td colspan="2" height="1" bgcolor="e9ebf4"></td>
                            </tr>
                            </tbody></table>
                            <?php endif;?>
                            <?php if ($transaction_model[0]->penaltyCharge>0):?>
                             <table class="full" object="drag-module-small" align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody><tr>
                              <td colspan="2" class="em_h30" height="20"></td>
                            </tr>
                             
                            <tr>
                            <td style="font-size:15px; color:#aaaaaa; font-family:'Lato',Arial, sans-serif; font-weight:500; line-height:22px;" align="left"> Penalty Charge   </td>
                            <td align="center" width="100">
                             <table class="mcenter" width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                          <td style="font-size:15px;  font-family:'Lato',Arial, sans-serif; font-weight:500; color:#aaaaaa; border-radius:5px; padding-left:5px; padding-right:5px;" align="center" valign="middle" height="21">
                                      <span style="font-size:20px;  font-family:'Lato',Arial, sans-serif; font-weight:500; color:#aaaaaa; border-radius:5px; text-align:right;"><?php echo $transaction_model[0]->penaltyCharge;?></span></td>
                                      
                                      <td style="font-size:15px;  font-family:'Lato',Arial, sans-serif; font-weight:500; color:#aaaaaa; border-radius:5px; padding-left:5px; padding-right:5px;" align="center" valign="middle" height="21">
                                      <span style="text-decoration:none; color:#aaaaaa; line-height:18px; display:block; " object="link-editable"><?php echo $transaction_model[0]->currencyCode;?></span></td>
                                        </tr>
                                      </tbody></table>
                                 
                            </td>
                            </tr>
                             
                            <tr>
                              <td colspan="2" height="13"></td>
                            </tr>
                            <tr>
                              <td colspan="2" height="1" bgcolor="e9ebf4"></td>
                            </tr>
                            </tbody></table>
                            <?php endif;?>
                            <?php if ($transaction_model[0]->surgeCharge>0):?>
                            <table class="full" object="drag-module-small" align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody><tr>
                              <td colspan="2" class="em_h30" height="20"></td>
                            </tr>
                             
                            <tr>
                            <td style="font-size:15px; color:#aaaaaa; font-family:'Lato',Arial, sans-serif; font-weight:500; line-height:22px;" align="left"> Surge Charge   </td>
                            <td align="center" width="100">
                             <table class="mcenter" width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                          <td style="font-size:15px;  font-family:'Lato',Arial, sans-serif; font-weight:500; color:#aaaaaa; border-radius:5px; padding-left:5px; padding-right:5px;" align="center" valign="middle" height="21">
                                      <span style="font-size:20px;  font-family:'Lato',Arial, sans-serif; font-weight:500; color:#aaaaaa; border-radius:5px; text-align:right;"><?php echo $transaction_model[0]->surgeCharge;?></span></td>
                                      
                                      <td style="font-size:15px;  font-family:'Lato',Arial, sans-serif; font-weight:500; color:#aaaaaa; border-radius:5px; padding-left:5px; padding-right:5px;" align="center" valign="middle" height="21">
                                      <span style="text-decoration:none; color:#aaaaaa; line-height:18px; display:block; " object="link-editable"><?php echo $transaction_model[0]->currencyCode;?></span></td>
                                        </tr>
                                      </tbody></table>
                                 
                            </td>
                            </tr>
                             
                            <tr>
                              <td colspan="2" height="13"></td>
                            </tr>
                            <tr>
                              <td colspan="2" height="1" bgcolor="e9ebf4"></td>
                            </tr>
                            </tbody></table>
                            <?php endif;?>
                            <?php if ($transaction_model[0]->promoDiscountAmount>0):?>
                            <table class="em_wrapper" object="drag-module-small" align="center" cellspacing="0" cellpadding="0" border="0">
									<tbody><tr>
										<td align="center" width="100%" valign="middle">
										
											<table style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter" align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
												<tbody><tr>
													<td style="font-size:16px; color:#2a2d32; font-family:'Lato',Arial, sans-serif; font-weight:700;" align="center" width="100%">Discount </td>
												</tr>
											</tbody></table>							
										</td>
									</tr>
								</tbody></table>
                            
                            
                            
                            
                            
                            <?php endif;?>
                            
                            <?php if ($transaction_model[0]->promoDiscountAmount>0):?>
                       <table class="full" object="drag-module-small" align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody><tr>
                              <td colspan="2" class="em_h30" height="20"></td>
                            </tr>
                            
                            <tr>
                            <td style="font-size:15px; color:#aaaaaa; font-family:'Lato',Arial, sans-serif; font-weight:500; line-height:22px;" align="left"> Base Charge   </td>
                            <td align="center" width="100">
                             <table class="mcenter" width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                          <td style="font-size:15px;  font-family:'Lato',Arial, sans-serif; font-weight:500; color:#aaaaaa; border-radius:5px; padding-left:5px; padding-right:5px;" align="center" valign="middle" height="21">
                                      <span style="font-size:20px;  font-family:'Lato',Arial, sans-serif; font-weight:500; color:#aaaaaa; border-radius:5px; text-align:right;"><?php echo $transaction_model[0]->promoDiscountAmount;?></span></td>
                                      
                                      <td style="font-size:15px;  font-family:'Lato',Arial, sans-serif; font-weight:500; color:#aaaaaa; border-radius:5px; padding-left:5px; padding-right:5px;" align="center" valign="middle" height="21">
                                      <span style="text-decoration:none; color:#aaaaaa; line-height:18px; display:block; " object="link-editable"><?php echo $transaction_model[0]->currencyCode;?></span></td>
                                        </tr>
                                      </tbody></table>
                                 
                            </td>
                            </tr>
                            
                            <tr>
                              <td colspan="2" height="13"></td>
                            </tr>
                            <tr>
                              <td colspan="2" height="1" bgcolor="e9ebf4"></td>
                            </tr>
                            </tbody></table>
                            <?php endif;?>
                            <table class="em_wrapper" object="drag-module-small" align="center" cellspacing="0" cellpadding="0" border="0">
									<tbody><tr>
										<td align="center" width="100%" valign="middle">
										
											<table style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter" align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
												<tbody><tr>
													<td style="font-size:16px; color:#2a2d32; font-family:'Lato',Arial, sans-serif; font-weight:700;" align="center" width="100%">Payment </td>
												</tr>
											</tbody></table>							
										</td>
									</tr>
								</tbody></table>
                            
                            
                            
                            
                            
                            
                            
                            <?php if ($transaction_model[0]->walletPaymentAmount>0):?>
                       <table class="full" object="drag-module-small" align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody><tr>
                              <td colspan="2" class="em_h30" height="20"></td>
                            </tr>
                            
                            <tr>
                            <td style="font-size:15px; color:#aaaaaa; font-family:'Lato',Arial, sans-serif; font-weight:500; line-height:22px;" align="left"> Wallet</td>
                            <td align="center" width="100">
                             <table class="mcenter" width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                          <td style="font-size:15px;  font-family:'Lato',Arial, sans-serif; font-weight:500; color:#aaaaaa; border-radius:5px; padding-left:5px; padding-right:5px;" align="center" valign="middle" height="21">
                                      <span style="font-size:20px;  font-family:'Lato',Arial, sans-serif; font-weight:500; color:#aaaaaa; border-radius:5px; text-align:right;"><?php echo $transaction_model[0]->walletPaymentAmount;?></span></td>
                                      
                                      <td style="font-size:15px;  font-family:'Lato',Arial, sans-serif; font-weight:500; color:#aaaaaa; border-radius:5px; padding-left:5px; padding-right:5px;" align="center" valign="middle" height="21">
                                      <span style="text-decoration:none; color:#aaaaaa; line-height:18px; display:block; " object="link-editable"><?php echo $transaction_model[0]->currencyCode;?></span></td>
                                        </tr>
                                      </tbody></table>
                                 
                            </td>
                            </tr>
                            
                            <tr>
                              <td colspan="2" height="13"></td>
                            </tr>
                            <tr>
                              <td colspan="2" height="1" bgcolor="e9ebf4"></td>
                            </tr>
                            </tbody></table>
                            <?php endif;?>
                            <?php if ($transaction_model[0]->totalTripCost-$transaction_model[0]->walletPaymentAmount > 0):?>
                            <table class="full" object="drag-module-small" align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody><tr>
                              <td colspan="2" class="em_h30" height="20"></td>
                            </tr>
                            
                            <tr>
                            <td style="font-size:15px; color:#aaaaaa; font-family:'Lato',Arial, sans-serif; font-weight:500; line-height:22px;" align="left"> Cash</td>
                            <td align="center" width="100">
                             <table class="mcenter" width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                          <td style="font-size:15px;  font-family:'Lato',Arial, sans-serif; font-weight:500; color:#aaaaaa; border-radius:5px; padding-left:5px; padding-right:5px;" align="center" valign="middle" height="21">
                                      <span style="font-size:20px;  font-family:'Lato',Arial, sans-serif; font-weight:500; color:#aaaaaa; border-radius:5px; text-align:right;"><?php echo ($transaction_model[0]->totalTripCost-$transaction_model[0]->walletPaymentAmount);?></span></td>
                                      
                                      <td style="font-size:15px;  font-family:'Lato',Arial, sans-serif; font-weight:500; color:#aaaaaa; border-radius:5px; padding-left:5px; padding-right:5px;" align="center" valign="middle" height="21">
                                      <span style="text-decoration:none; color:#aaaaaa; line-height:18px; display:block; " object="link-editable"><?php echo $transaction_model[0]->currencyCode;?></span></td>
                                        </tr>
                                      </tbody></table>
                                 
                            </td>
                            </tr>
                            
                            <tr>
                              <td colspan="2" height="13"></td>
                            </tr>
                            <tr>
                              <td colspan="2" height="1" bgcolor="e9ebf4"></td>
                            </tr>
                            </tbody></table>
                            <?php endif; ?>
                            
                            
                            
                             
                            
                            
                
                            
                            
                            
                            
                            
                            		
                                
                                
                                 
                            
                            
                            </td>
                            </tr>
                            </tbody></table>
								
								
								 
								
								 
								 
								
							
						
<!-- End Button -->		 
							<table style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter" align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
												<tbody><tr>
													<td class="em_h30" width="100%" height="120"> </td>
												</tr>
											</tbody></table>
							</div>	
									
								</td>
							</tr>
						</tbody></table>
						
					</td>
        </tr>
        <!--end button--><!-- start space -->
        <tr>
          <td style="font-size: 0px; line-height: 0; border-collapse: collapse;" valign="top" height="30"></td>
        </tr>
        <!-- end space -->
      </tbody></table>
      <!-- end container --></td>
  </tr>
  <!--END LAYOUT-4 ( 2-COL IMAGE / TEXT / BUTTON )-->
  
  
  


  
  
  
  
  
  
  
  
  
</tbody></table>
	
</div>