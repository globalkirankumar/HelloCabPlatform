<style>
.label.label-sm {
	font-size: 13px;
	padding: 2px 5px;
	cursor: pointer;
}
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">

		<div class="page-bar ">
			<ul class="page-breadcrumb ">
				<li><a href="<?php echo base_url('Dashboard');?>">Home</a> <i
					class="fa fa-circle"></i></li>
				<li><a href="<?php echo base_url('Trip/getTripList');?>">Trip List</a></li>

			</ul>

		</div>
		<!---- Page Bar ends ---->
		<div class="">

			<!-- BEGIN BORDERED TABLE PORTLET-->
			<div class="portlet box green gp-form">
				<div class="portlet-title">
					<div class="caption">Trip Details</div>

				</div>
				<div class="portlet-body">
					<div class="row">
						<div class="col-lg-3 input_field_sections nowarp">

                                <?php
																																echo form_label ( 'Start date:', 'startDate', array (
																																		'class' => '' 
																																) );
																																
																																echo form_input ( array (
																																		'id' => 'startDate',
																																		'name' => 'startDate',
																																		'class' => 'form-control',
																																		
																																		// 'required' => 'required',
																																		'placeholder' => 'Start Date',
																																		'value' => '' 
																																) );
																																
																																?>
                            </div>
						<div class="col-lg-3 input_field_sections nowarp">

                                <?php
																																echo form_label ( 'End Date:', 'endDate', array (
																																		'class' => '' 
																																) );
																																
																																echo form_input ( array (
																																		'id' => 'endDate',
																																		'name' => 'endDate',
																																		'class' => 'form-control',
																																		
																																		// 'required' => 'required',
																																		'placeholder' => 'End Date',
																																		'value' => '' 
																																) );
																																
																																?>
                            </div>
					</div>
					<div class="row">

						<div class="col-lg-3 input_field_sections nowarp">

                            <?php
																												echo form_label ( 'Entity:', 'entityId', array (
																														'class' => '' 
																												) );
																												
																												echo form_dropdown ( 'entityId', $entity_list, '', array (
																														'id' => 'entityId',
																														'class' => 'form-control' 
																												) );
																												
																												?>

                        </div>
						<div class="col-lg-3 input_field_sections nowarp">

                            <?php
																												echo form_label ( 'Zone:', 'zoneListId', array (
																														'class' => '' 
																												) );
																												// debug_exit($zone_list);
																												echo form_dropdown ( 'zoneListId', $zone_list, '', array (
																														'id' => 'zoneListId',
																														'class' => 'form-control' 
																												) );
																												
																												?>

                        </div>
						<div class="col-lg-3 input_field_sections nowarp">

                            <?php
																												echo form_label ( 'Trip Type:', 'tripType', array (
																														'class' => '' 
																												) );
																												
																												echo form_dropdown ( 'tripType', $trip_type_list, '', array (
																														'id' => 'tripType',
																														'class' => 'form-control' 
																												)
																												// 'required' => 'required'
																												 );
																												
																												?>

                        </div>
						<div class="col-lg-3 input_field_sections nowarp">

                            <?php
																												echo form_label ( 'TripStatus:', 'tripStatus', array (
																														'class' => '' 
																												) );
																												
																												echo form_multiselect ( 'tripStatus', $trip_status_list, '', array (
																														'id' => 'tripStatus',
																														'class' => 'form-control' 
																												)
																												// 'required' => 'required'
																												 );
																												
																												?>

                        </div>
					</div>

					<div class="form-actions" style="margin-top: 22px;">
						<div class="row">
							<div class="col-md-12">

								<div class="gp-cen">
									<button type="button" id="trip-details-btn" class="btn gpblue">Get
										Trip List</button>
								</div>
							</div>
						</div>
					</div>

				</div>
				<!-- END BORDERED TABLE PORTLET-->

			</div>

		</div>
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-comments"></i>Trip List
						</div>

					</div>

					<div class="portlet-body">
						<div class="table-toolbar">
							<div class="row">
								<div class="col-md-6">
									<div class="col-md-3 btn-group gp-pad1">
										<a href="<?php echo base_url('Trip/add');?>">
											<button id="sample_editable_1_new" class="btn sbold green1">
												Add New <i class="fa fa-plus"></i>
											</button>
										</a>
									</div>

								</div>

							</div>
						</div>

						<div id="table-columns" class="hidden">

							<label class="">
                                            <?php
																																												echo form_checkbox ( array (
																																														'class' => 'flat toggle-vis',
																																														'data-column' => '6' 
																																												) );
																																												?>
                                        Dispatch Datetime
                                    </label> <label class="">
                                        <?php
																																								echo form_checkbox ( array (
																																										'class' => 'flat toggle-vis',
																																										'data-column' => '7' 
																																								) );
																																								?>
                                        Driver Accept Datetime
                                    </label> <label class="">
                                        <?php
																																								echo form_checkbox ( array (
																																										'class' => 'flat toggle-vis',
																																										'data-column' => '8' 
																																								) );
																																								?>
                                        Driver Arrived Datetime
                                    </label> <label class="">
                                               <?php
																																															echo form_checkbox ( array (
																																																	'class' => 'flat toggle-vis',
																																																	'data-column' => '9' 
																																															) );
																																															?>
                                         Actual Pickup Datetime
                                    </label> <label class="">
                                        <?php
																																								echo form_checkbox ( array (
																																										'class' => 'flat toggle-vis',
																																										'data-column' => '17' 
																																								) );
																																								?>
                                        Taxi Type
                                    </label>
							<!-- <label class="">
                                        <?php
																																								// echo form_checkbox ( array ('class' => 'flat toggle-vis','data-column' => '17') );
																																								?>
                                        Trip Type
                                    </label>  -->
							<label class="">
                                        <?php
																																								echo form_checkbox ( array (
																																										'class' => 'flat toggle-vis',
																																										'data-column' => '19' 
																																								) );
																																								?>
                                        Booked From
                                    </label> <label class="">
                                        <?php
																																								echo form_checkbox ( array (
																																										'class' => 'flat toggle-vis',
																																										'data-column' => '20' 
																																								) );
																																								?>
                                        Payment Mode
                                    </label> <label class="">
                                        <?php
																																								echo form_checkbox ( array (
																																										'class' => 'flat toggle-vis',
																																										'data-column' => '21' 
																																								) );
																																								?>
                                       Promocode
                                    </label> <label class="">
                                        <?php
																																								echo form_checkbox ( array (
																																										'class' => 'flat toggle-vis',
																																										'data-column' => '22' 
																																								) );
																																								?>
                                        Zone
                                    </label>

						</div>

						<table
							class="trip-data-table table table-striped table-bordered table-hover table-checkable order-column"
							id="trip-data-table" style="width: 99%">
							<thead>
								<tr>
									<th>Action</th>
									<th>Trip Status</th>
									<th>Job Card Id</th>
									<th>Entity</th>
									<th>Pickup Location</th>
									<th>Pickup Datetime</th>
									<th>Dispatch Datetime</th>
									<th>Driver Accept Datetime</th>
									<th>Driver Arrived Datetime</th>
									<th>Actual Pickup Datetime</th>
									<th>Drop Location</th>
									<th>Taxi Reg No</th>
									<th>Passenger Name</th>
									<th>Passenger Mobile</th>
									<th>Driver Code</th>
									<th>Driver Name</th>
									<th>Driver Mobile</th>
									<th>Taxi Type</th>
									<th>Trip Type</th>
									<th>Booked From</th>
									<th>Payment Mode</th>
									<th>Promocode</th>
									<th>Zone</th>

								</tr>
							</thead>
							<tbody>

							</tbody>
						</table>
					</div>
				</div>

			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>



