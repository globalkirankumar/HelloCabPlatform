<style>
.label.label-sm {
	font-size: 13px;
	padding: 2px 5px;
	cursor: pointer;
}
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">

		<div class="page-bar ">
			<ul class="page-breadcrumb ">
				<li><a href="<?php echo base_url('Dashboard');?>">Home</a> <i
					class="fa fa-circle"></i></li>
				<li><a href="<?php echo base_url('Passenger/getPassengerList');?>">Passenger
						List</a> <i class="fa fa-circle"></i></li>
				<li><a href="<?php echo base_url(uri_string());?>">Transaction List</a>

				</li>

			</ul>

		</div>
		<!---- Page Bar ends ---->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-comments"></i><?php echo $passenger_fullname ;?> </div>

					</div>

					<div class="portlet-body">
						<div class="table-toolbar">
							<div class="row">
								<div class="col-md-6">
									<div class="col-md-3 btn-group gp-pad1">
										<a
											href="<?php echo base_url('Passenger/addPassengerTransaction/'.$this->uri->segment(3));?>">
											<button id="sample_editable_1_new" class="btn sbold green1">
												Add New <i class="fa fa-plus"></i>
											</button>
										</a>
									</div>

									<!--<div class="col-md-9 m-b-10">
                                                <button type="button" id="toggle-table-columns"
                                                        class="btn btn-primary">Show Columns List
                                                </button>
                                        </div>-->
								</div>
								<!--<div class="col-md-6">
                                                    <div class="btn-group pull-right">
                                                        <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Export
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a href="#">
                                                                    <i class="fa fa-print"></i> Print </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>-->
							</div>
						</div>



						<table
							class="table table-striped table-bordered table-hover table-checkable order-column"
							id="passenger-transaction-data-table" style="width: 99%">
							<thead>
								<tr>
									<th>Action</th>
									<th>TripId</th>
									<th>Transaction Amount</th>
									<th>Previous Amount</th>
									<th>Current Amount</th>
									<th>Transaction Status</th>
									 <th>Transaction From</th>
									<th>Transaction Type</th>
									<th>Transaction Mode</th>
									<th>Transaction Datetime</th>
									<th>Transaction Id</th>
									<th>Comments</th>

								</tr>
							</thead>
							<tbody>

							</tbody>
						</table>
					</div>
				</div>

			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>