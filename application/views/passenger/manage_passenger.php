<style>
    .label.label-sm {
        font-size: 13px;
        padding: 2px 5px;
        cursor:pointer;
    }
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">

        <div class="page-bar ">
            <ul class="page-breadcrumb ">
                <li>
                    <a href="<?php echo base_url('Dashboard'); ?>">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="<?php echo base_url('Passenger/getPassengerList'); ?>">Passenger List</a>

                </li>

            </ul>

        </div>
        <!---- Page Bar ends ---->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                           <i class="fa fa-list" aria-hidden="true"></i>Passenger List </div>

                    </div>

                    <div class="portlet-body">
                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="col-md-3 btn-group gp-pad1">
                                                    
                                                        <a href="<?php echo base_url();?>passenger/add">
                                                        <button id="sample_editable_1_new" class="btn sbold green1"> Add New
                                                            <i class="fa fa-plus"></i>
                                                        </button>
                                                            </a>
                                                    </div>
                                                    
                                                    
                                                </div>
                                               
                                            </div>
                                        </div>
                        
                        <div id="table-columns" class="hidden">

                            <label class="">
                                <?php
                                echo form_checkbox(array(
                                    'class' => 'flat toggle-vis',
                                    'data-column' => '10'
                                ));
                                ?>
                                Zone
                            </label>

                            <label class="">
                                <?php
                                echo form_checkbox(array(
                                    'class' => 'flat toggle-vis',
                                    'data-column' => '11',
                                ));
                                ?>
                                Last Logged In
                            </label>

                            
                        </div>

                        <table id="passenger-data-table"
                               class="table table-striped table-bordered table-hover table-checkable order-column gp-font dataTable no-footer" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Action</th>
                                    <th>Image</th>
                                    <th>Passenger Code</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Mobile</th>
                                    <th>Email</th>
                                    <th>Passenger Rating</th>
                                    <th>Wallet Amount</th>
                                    <th>Joined Datetime</th>
                                    <th>Zone</th>
                                    <th>Last Logged In</th>
                                    <th>Login Status</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <!-- <tfoot>
                                    <tr>
                                            <th>Image</th>
                                            <th>Passenger Code</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Mobile</th>
                                            <th>Email</th>
											<th>City</th>
                                            <th>Last Logged In</th>
                                            <th>Login Status</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                    </tr>
                            </tfoot> -->
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>



