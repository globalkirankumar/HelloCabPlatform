<?php
$view_mode = $mode;
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <?php
        $form_attr = array(
            'name' => 'edit_passenger_form',
            'id' => 'edit_passenger_form',
            'method' => 'POST'
        );
        echo form_open_multipart(base_url(''), $form_attr);
        // passenger id by default is -1
        echo form_input(array(
            'type' => 'hidden',
            'id' => 'passenger_id',
            'name' => 'id',
            'value' => ($passenger_model->get('id')) ? $passenger_model->get('id') : -1
        ));
        echo form_input(array(
            'type' => 'hidden',
            'id' => 'entityId',
            'name' => 'entityId',
            'value' => DEFAULT_COMPANY_ID
        ));
        
        ?>
        <div class="page-bar ">
        	<ul class="page-breadcrumb ">
			<li><a href="<?php echo base_url('Dashboard'); ?>">Home</a> <i
				class="fa fa-circle"></i></li>
			<li><a href="<?php echo base_url('Passenger/getPassengerList'); ?>">Passenger
					List</a> <i class="fa fa-circle"></i></li>
			<li><a href="<?php echo base_url(uri_string()); ?>"><?php echo ($passenger_model->get ( 'id' )) ?(($view_mode == VIEW_MODE)?'View':'Edit'): 'Create'; ?> Passenger</a></li>

		</ul>
            
        </div>
        <!---- Page Bar ends ---->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <!--                                <div class="portlet box green">
                                                <div class="portlet-title">
                                                        <div class="caption">
                                                            <i class="fa fa-comments"></i>Driver List </div>
                                                       
                                                    </div>
                                                 
                                                    
                                                </div>-->
                <div class="portlet-body">
                    <div class="form-body gp-form">
                        <div class="card-header bg-white">
                            <h1><?php echo ($passenger_model->get('id')) ? (($view_mode == VIEW_MODE) ? 'View' : 'Edit') : 'Create'; ?> Passenger</h1>
                        </div>
                        <div class="row">
                        <div class="col-lg-3 input_field_sections nowarp">
                        <img alt="no-image" src="<?php echo ($passenger_model->get('profileImage'))? passenger_content_url($passenger_model->get('firstName')."/".$passenger_model->get('profileImage')):image_url('app/user.png'); ?>" width="80" height="80">
                        </div>
                          </div>
                          <div class="row">  
                            <div class="col-lg-3 input_field_sections nowarp">
                                <?php
                                echo form_label('First Name:', 'firstName', array(
                                    'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                                ));
                                
                                // validation for passenger first name
                                if ($view_mode == EDIT_MODE) {
                                	
                                    echo form_input(array(
                                        'id' => 'firstName',
                                        'name' => 'firstName',
                                        'class' => 'form-control',
                                        'required' => 'required',
                                        'pattern' => '[a-zA-Z\s]{3,15}',
                                        'placeholder' => 'First Name',
                                        'value' => ($passenger_model->get('firstName')) ? $passenger_model->get('firstName') : ''
                                    ));
                                } else {
                                    echo text($passenger_model->get('firstName'));
                                }
                                ?>

                            </div>
                            <div class="col-lg-3 input_field_sections nowarp">

                                <?php
                                echo form_label('Last Name:', 'lastName', array(
                                    'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                                ));
                                // validation for passenger first name
                                if ($view_mode == EDIT_MODE) {
                                    echo form_input(array(
                                        'id' => 'lastName',
                                        'name' => 'lastName',
                                        'class' => 'form-control',
                                       // 'required' => 'required',
                                       // 'pattern' => '[a-zA-Z\s]{1,15}',
                                        'placeholder' => 'Last Name',
                                        'value' => ($passenger_model->get('lastName')) ? $passenger_model->get('lastName') : ''
                                    ));
                                } else {
                                    echo text($passenger_model->get('lastName'));
                                }
                                ?>

                            </div>
                            <div class="col-lg-3 input_field_sections nowarp section-container">
                                <div class="section-message hidden"></div>
                                <?php
                                echo form_label('Email:', 'email', array(
                                    'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                                ));
                                // validation for passenger first name
                                if ($view_mode == EDIT_MODE) {
                                    echo form_input(array(
                                        'id' => 'email',
                                        'name' => 'email',
                                        'class' => 'form-control',
                                       // 'required' => 'required',
                                       // 'pattern' => '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$',
                                        'placeholder' => 'Email Id',
                                        'value' => ($passenger_model->get('email')) ? $passenger_model->get('email') : ''
                                    ));
                                } else {
                                    echo text($passenger_model->get('email'));
                                }
                                ?>

                            </div>
                            <div class="col-lg-3 input_field_sections nowarp section-container">
                                <div class="section-message hidden"></div>
                                <?php
                                echo form_label('Mobile No:', 'mobile', array(
                                    'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                                ));
                                // validation for passenger first name
                                if ($view_mode == EDIT_MODE) {
                                    echo form_input(array(
                                        'id' => 'mobile',
                                        'name' => 'mobile',
                                        'class' => 'form-control',
                                    	'maxlength'=>'11',
                                        'required' => 'required',
                                        'pattern' => '^[0]?[789]\d{7,9}$',
                                        'placeholder' => 'Mobile',
                                        'value' => ($passenger_model->get('mobile')) ? $passenger_model->get('mobile') : ''
                                    ));
                                } else {
                                    echo text($passenger_model->get('mobile'));
                                }
                                ?>

                            </div>

                        </div>
                        <div class="row">
                             <div class="col-lg-3 input_field_sections nowarp">
                                <?php
                                echo form_label('address:', 'address', array(
                                    'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                                ));

                                if ($view_mode == EDIT_MODE) {
                                    echo form_textarea(array(
                                        'id' => 'address',
                                        'name' => 'address',
                                        'rows' => '2',
                                        'class' => 'form-control',
                                        //'required' => 'required',
                                        'value' => ($passenger_model->get('address')) ? $passenger_model->get('address') : ''
                                    ));
                                } else {
                                    echo form_textarea(array(
                                        'id' => 'address',
                                        'name' => 'address',
                                        'class' => 'form-control',
                                        'rows' => '2',
                                        'readonly' => 'readonly',
                                        'value' => ($passenger_model->get('address')) ? $passenger_model->get('address') : ''
                                    ));
                                }
                                ?>

                            </div>
                            
                            <div class="col-md-3 input_field_sections nowarp">
                                    <?php
                                    echo form_label('Gender:', 'gender', array(
                                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                                    ));
                                    if ($view_mode == EDIT_MODE) {
                                        if (is_array($gender_list) && count($gender_list)) {
                                            echo '<div class="mt-radio-inline">';
                                            foreach ($gender_list as $list) {
                                                echo '<label class="mt-radio" style="padding-right:10px;">';
                                                $is_checked = ($passenger_model->get ( 'gender' )==$list->description) ? TRUE : FALSE;
                                                $checked_value =$list->description;
                                              
                                                	echo form_radio ( array (
                                                			'id' => 'gender' . $list->description,
                                                			'name' => 'gender',
                                                			'class' => 'flat',
                                                			'required' => 'required'
                                                	), $checked_value, $is_checked );
                                               
                                                
                                                echo '<span></span>' . $list->description . '</label>';
                                            }
                                            echo '</div>';
                                        }
                                    } else {
                                        echo text($passenger_model->get('gender'));
                                    }
                                    ?>
                                </div>

                            <div class="col-lg-3 input_field_sections nowarp">
                                <?php
                                echo form_label('DOB:', 'dob', array(
                                    'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                                ));

                                if ($view_mode == EDIT_MODE) {
                                    echo form_input(array(
                                        'id' => 'dob',
                                        'name' => 'dob',
                                        'class' => 'form-control',
                                        'required' => 'required',
                                        'placeholder' => 'DOB',
                                        'value' => ($passenger_model->get('dob')) ? $passenger_model->get('dob') : ''
                                    ));
                                } else {
                                    echo text($passenger_model->get('dob'));
                                }
                                ?>

                            </div>
                            
                            <div class="col-lg-3 input_field_sections nowarp">
                                
                                <?php
                                echo form_label('Zone Name:', 'zoneId', array(
                                    'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                                ));
// validation for passenger first name
                                if ($view_mode == EDIT_MODE) {
                                    echo form_dropdown('zoneId', $zone_list, $passenger_model->get('zoneId'), array(
                                        'id' => 'zoneId',
                                        'class' => 'form-control',
                                        //'required' => 'required'
                                    ));
                                } else {
                                    echo text($zone_list[$passenger_model->get('zoneId')]);
                                }
                                ?>	

                            </div>

                        </div>

                        
                        <div class="row">
                        	<div class="col-lg-3 input_field_sections nowarp">
                                <?php
                                echo form_label('Entity Name:', 'entityId', array(
                                    'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                                ));
// validation for passenger first name
                                if ($view_mode == EDIT_MODE) {
                                    echo form_dropdown('entityId', $entity_list, ($passenger_model->get('entityId'))?$passenger_model->get('entityId'):DEFAULT_COMPANY_ID, array(
                                        'id' => 'entityId',
                                        'class' => 'form-control',
                                        'required' => 'required'
                                    ));
                                } else {
                                    echo text($entity_list[$passenger_model->get('entityId')]);
                                }
                                ?>

                            </div>
                            
                            

                        </div>


                        <div class="form-actions" style="margin-top:22px;">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php if ($view_mode == EDIT_MODE) { ?>
                                        <div class="gp-cen">
                                            
                                            <button type="button" id="cancel-passenger-btn" class="btn grey-salsa btn-outline">Cancel</button>
                                            <button type="button" id="save-passenger-btn" class="btn gpblue">Save</button>
                                         </div>
                                <?php } else { ?>
                                                           <div class="gp-cen">
                                                <button type="button"
                                                        id="cancel-passenger-btn" class="btn btn-danger">Back</button>
                                            </div>
                                    <?php } ?>
                                   
                                                                </div>
                                                            </div>
                                                        </div>
                                <?php
                                echo form_close();
                                ?>

                    </div>
                </div>        
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>



