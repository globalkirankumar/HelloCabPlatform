<style>
    div.dt-buttons {
    position: relative;
    float: left;
    margin-right: 29px;
}
</style>
<!-- BEGIN CONTENT BODY -->
<div class="page-content gp-dashbg">
    <!-- BEGIN PAGE HEADER-->
    <div class="portlet light bordered">
        <div class="portlet-title gp-title">
            <div class="caption">
                <i class="icon-bar-chart font-dark hide"></i>
                <span class="caption-subject font-dark bold uppercase">Trip Details</span><br>
                <span class="caption-helper">Hourwise</span>
            </div>
            <!-- <div class="actions">
                 <div class="gp-select">

<select id="basic" class="selectpicker show-tick form-control">

<option>Select Zone </option>
<option>Select Zone 1</option>
<option>Select Zone 2</option>
</select>
</div>
            </div> -->


            <div class="actions" style="width: 200px;">
                <div class="gp-select1">

                    <select id="trip_detail_select" class="selectpicker show-tick form-control">

                        <option value=''>Select option </option>
                        <option value='0'>Today</option>
                        <option value='1'>Last 24 hours</option>
                        <option value='2'>Last 2 days</option>
                    </select>
                </div>
            </div>


        </div>


        <div id="trip-details"></div>

        <div class="gp-cen">
            <h5 class="colors">
                <span class="color green not-available" data-toggle="tooltip" title="Not In store"></span><span class="gp-p">COMPLETED</span>

                <span class="color yellow"></span><span class="gp-p">ONGOING</span>
                <span class="color red"></span><span class="gp-p">CANCEL</span>
            </h5>
        </div>


    </div>



    <div class="row">
        <div class="col-lg-6 col-xs-12 col-sm-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase font-dark">CAB STATUS DETAILS</span>
                        <!--<span class="caption-helper">distance stats...</span>-->
                    </div>
                    <div class="actions" style="width: 200px;">
<!--                        <div class="gp-select1">

                            <select id="cab_details_select" class="selectpicker show-tick form-control">

                                <option value=''>Select option </option>
                                <option value='0'>Today </option>
                                <option value='1'>Last 24 hours</option> 
                                <option value='2'>Last 48 hours </option>
                                <option value='3'>0-2 Hours </option>
                              
                            </select>
                        </div>-->
                    </div>
                </div>
                <div class="portlet-body">



                    <div id="cabs-details" ></div>

                    <div class="gp-cen">
                        <h5 class="colors">
                            <span class="color green not-available" data-toggle="tooltip" title="Not In store"></span><span class="gp-p">Online cabs</span>

                            <span class="color yellow"></span><span class="gp-p">Busy cabs</span>
                            <span class="color blue"></span><span class="gp-p">On trip busy cabs</span>
                            <span class="color red"></span><span class="gp-p">Offline cabs</span>
                        </h5>
                    </div>
                    <div class="gp-cen">
                        <h5 class="colors">
                            <span class="gp-p">LogIn cabs=<?php echo $log_in_cabs;?></span>

                            <span class="gp-p">LogOut cabs=<?php echo $log_out_cabs;?></span>
                            <span class="gp-p"><?php echo ($this->uri->segment ( 4 ) != '') ? 'Cabs on other zones=' : 'InActive cabs=';?><?php echo $unknown_log_cabs;?></span>
                        </h5>
                    </div>

                </div></div></div>



        <div class="col-lg-6 col-xs-12 col-sm-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption ">
                        <span class="caption-subject font-dark bold uppercase">DRIVER CONSECUTIVE REJECTED DETAILS</span>
                        <!--<span class="caption-helper">distance stats...</span>-->
                    </div>
                    <div class="actions" style="width: 200px;">
<!--                        <div class="gp-select1">

                            <select id="basic" class="selectpicker show-tick form-control">

                                <option>Select Zone </option>
                                <option>Select Zone 1</option>
                                <option>Select Zone 2</option>
                            </select>
                        </div>-->
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="active-inactive-cabs" ></div>

                    <div class="gp-cen">
                        <h5 class="colors">
                        <?php foreach ($consecutive_count as $key=>$val)
                        {
                        	echo '<span class="color color_'.$key.'"></span><span class="gp-p">'.$key.' Consecutive</span>';
                        }
                        ?>
                          <!--  <span class="color green not-available" data-toggle="tooltip" title="Not In store"></span>
                           <span class="gp-p">2 Consecutive Rejected</span>

                           <span class="color yellow"></span><span class="gp-p">3 Consecutive Rejected</span><br>
                            <span class="color red"></span><span class="gp-p">4 Consecutive Rejected</span> -->

                        </h5>
                    </div>
                </div>
                
            </div>
        
            
            
            
        </div>
    </div>

</div>
<!-- END CONTENT BODY -->

<script src="<?php echo base_url();?>public/js/jquery.min.js"></script>
<script>
    $(function () {
         //$('#trip_detail_select').val(0); //make default for today in trip details chart
        /** chart daata for Today **/
        //$('#trip_detail_select').val(2);
        Morris.Bar({
            element: 'trip-details',
            data: [
                <?php echo $chartdata; ?>
            ],
            xkey: 'y',
            ykeys: ['a', 'b', 'c'],
            labels: ['Completed', 'Ongoing', 'Cancelled'],
            colors: ["#4DB3A4", "#FFAF2F", "#F36A5A"],
        });
        
     $('#trip_detail_select').on('change',function(){
      var trip_detail_value = $(this).val();
      var zone              = $('#header_zone').val();
      var subzone           = $('#header_subzone').val();
       if(trip_detail_value !=''){
           var url = location.href + '/Dashboard/index/';
           window.location = location.origin+'/Dashboard/index/'+trip_detail_value+'/'+zone+'/'+subzone;
       }
   });
       
     $('#header_zone').on('change',function(){
      var trip_detail_value = $('#trip_detail_select').val();
      var zone              = $('#header_zone').val();
      var subzone           = $('#header_subzone').val();
       if(zone !=''){
           var url = location.href + '/Dashboard/index/';
           window.location = location.origin+'/Dashboard/index/'+trip_detail_value+'/'+zone+'/'+subzone;
       }else{
           window.location = location.origin+'/Dashboard';
       }
     });
     // to get trip detail drop down selected
        var url = location.href;
        var segments = url.split( '/' );
        var trip_select_filter    = segments[5];
        var subzone_select_filter = segments[7];
            if(trip_select_filter==0){
              $('#trip_detail_select').val(0);  
            }else if(trip_select_filter==1){
              $('#trip_detail_select').val(1);   
            }else if(trip_select_filter==2){
                $('#trip_detail_select').val(2);
            }else{
                $('#trip_detail_select').val(0);
            }
          var url = location.href;
            var segments = url.split( '/' );
            var zone    = segments[6];
            var subzone_select_filter    = segments[7];
            getSubZones(zone,1);
             if(subzone_select_filter > 0){
                $('#header_subzone').selectpicker('val', subzone_select_filter);
             }else{
                $('#header_subzone').selectpicker('val', ''); 
             }  
         
         
    });

</script>

<script>
/*** Cab Available Details Chart ***/
$(function(){
 
   Morris.Donut({
        element: 'cabs-details',
        colors:["#4DB3A4", "#FFAF2F","#293896", "#F36A5A"],
        labelColor:[ "#4DB3A4"],
        labelColor:[ "red"],
        labelColor:[ "#293896"],
        labelColor:[ "#4DB3A4"],
        data: [

          {value: <?php echo $free_status_cabs;?> , label: 'Online cabs'},
          {value: <?php echo $busy_status_cabs;?>, label: 'Busy cabs'},
          {value: <?php echo $ontrip_busy_status_cabs;?>, label: 'On trip busy cabs'},
          {value: <?php echo $unknown_status_cabs;?>, label: 'Offline cabs'},
          ],
        formatter: function (x) { return x + ""}
      }).on('click', function(i, row){
        console.log(i, row);
}); 
});

</script>
<script>
/*** Active / InActive Cabs ***/
$(function(){
	
   Morris.Donut({
        element: 'active-inactive-cabs',
        colors:["#4DB3A4", "#FFAF2F", "#F36A5A", "#00BCD4", "#E91E63", "#607D8B", "#009688", "#745AF2", "#CDDC39", "#DD4B39"],
        data: [
               <?php
               $data='';
               foreach ($consecutive_count as $key=>$val){
               	echo "{value:".$val.",label: '".$key." Consecutive Rejected'},";
               
				}
				
			   ?>       
          ],
        formatter: function (x) { return x + ""}
      }).on('click', function(i, row){
        console.log(i, row);
    });
});

</script>
<script src="http://203.81.73.38:2002/socket.io/socket.io.js">