<?php
$view_mode = $mode;
?>
<!-- BEGIN CONTENT -->

<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <?php
    $form_attr = array(
        'name' => 'edit_taxi_form',
        'id' => 'edit_taxi_form',
        'method' => 'POST'
    );
    echo form_open_multipart(base_url(''), $form_attr);
// driver id by default is -1
    echo form_input(array(
        'type' => 'hidden',
        'id' => 'taxi_id',
        'name' => 'id',
        'value' => ($taxi_model->get('id')) ? $taxi_model->get('id') : - 1
    ));
    ?>
    <div class="page-bar ">
        <ul class="page-breadcrumb ">
            <li><a href="<?php echo base_url('Dashboard'); ?>">Home</a> <i
                    class="fa fa-circle"></i></li>
            <li><a href="<?php echo base_url('Taxi/getTaxiList'); ?>">Taxi List</a>
                <i class="fa fa-circle"></i></li>
            <li><a href="<?php echo base_url(uri_string()); ?>"><?php echo ($taxi_model->get('id')) ? (($view_mode == VIEW_MODE) ? 'View' : 'Edit') : 'Create'; ?> Taxi</a></li>

        </ul>

    </div>
    <!---- Page Bar ends ---->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->

            <div class="portlet-body">
                <div class="form-body gp-form">
                    <div class="card-header bg-white">
                        <h1><?php echo ($taxi_model->get('id')) ? (($view_mode == VIEW_MODE) ? 'View' : 'Edit') : 'Create'; ?> Taxi</h1>
                    </div>


                    <div class="row">
                        <div class="col-lg-3 input_field_sections nowarp">
                            <?php
                            echo form_label('Taxi Owner Name:', 'taxiOwnerId', array(
                                'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                            ));
                            // validation for passenger first name
                            if ($view_mode == EDIT_MODE) {
                                echo form_input(array(
                                    'type' => 'hidden',
                                    'class' => 'form-control',
                                    'required' => 'required',
                                    'id' => 'taxiOwnerId',
                                    'name' => 'taxiOwnerId',
                                    'value' => ($taxi_model->get('taxiOwnerId')) ? $taxi_model->get('taxiOwnerId') : ''
                                ));
                                echo form_input(array(
                                    'id' => 'searchTaxiOwner',
                                    'class' => 'form-control',
                                    'required' => 'required',
                                    'name' => 'searchTaxiOwner',
                                    'value' => ($taxi_model->get('taxiOwnerId')) ? $taxi_owner_list [$taxi_model->get('taxiOwnerId')] : ''
                                ));
                                echo "<div id='mobile-suggesstion-box' style='display:none;'></div>";
                            } else {
                                echo text(@$taxi_owner_list [$taxi_model->get('taxiOwnerId')]);
                            }
                            ?>
                        </div>
               
                        

                        
                        
                        
                        
                        <div class="col-lg-3 input_field_sections nowarp">
<?php
echo form_label('Group name:', 'name', array(
    'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
));

if ($view_mode == EDIT_MODE) {
    echo form_input(array(
        'id' => 'name',
        'name' => 'name',
        'class' => 'form-control',
        'required' => 'required',
        'pattern' => '[a-zA-Z\s]{3,15}',
        'placeholder' => 'Taxi Name',
        'value' => ($taxi_model->get('name')) ? $taxi_model->get('name') : ''
    ));
} else {
    echo text($taxi_model->get('name'));
}
?>

                        </div>
                        
                        <div class="col-lg-3 input_field_sections nowarp">
                            <?php
                            echo form_label('Taxi Type:', 'taxiCategoryType', array(
                                'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                            ));
                             if ($view_mode == EDIT_MODE) {
                                echo form_dropdown('taxiCategoryType', $taxi_category_type_list, $taxi_model->get('taxiCategoryType'), array(
                                    'id' => 'taxiCategoryType',
                                    'class' => 'form-control',
                                    'required' => 'required'
                                ));
                             } else {
                                echo text($taxi_category_type_list [$taxi_model->get('taxiCategoryType')]);
                            }
                            ?>
                        </div>
                        <div class="col-lg-3 input_field_sections nowarp">

                                <?php
                                echo form_label('Model:', 'model', array(
                                    'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                                ));

                                if ($view_mode == EDIT_MODE) {
                                    echo form_input(array(
                                        'id' => 'model',
                                        'name' => 'model',
                                        'class' => 'form-control',
                                        'required' => 'required',
                                        'pattern' => '[a-zA-Z0-9\s]{3,15}',
                                        'placeholder' => 'Model',
                                        'value' => ($taxi_model->get('model')) ? $taxi_model->get('model') : ''
                                    ));
                                } else {
                                    echo text($taxi_model->get('model'));
                                }
                                ?>

                        </div>
                        <div class="col-lg-3 input_field_sections nowarp">

                                <?php
                                echo form_label('Manufacturer:', 'manufacturer', array(
                                    'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                                ));

                                if ($view_mode == EDIT_MODE) {
                                    echo form_input(array(
                                        'id' => 'manufacturer',
                                        'name' => 'manufacturer',
                                        'class' => 'form-control',
                                      //  'required' => 'required',
                                      //  'pattern' => '[a-zA-Z\s]{3,15}',
                                        'placeholder' => 'Manufacturer',
                                        'value' => ($taxi_model->get('manufacturer')) ? $taxi_model->get('manufacturer') : ''
                                    ));
                                } else {
                                    echo text($taxi_model->get('manufacturer'));
                                }
                                ?>

                        </div>
                            
                        <div class="col-lg-3 input_field_sections nowarp">

                                <?php
                                echo form_label('Colour:', 'colour', array(
                                    'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                                ));

                                if ($view_mode == EDIT_MODE) {
                                    echo form_input(array(
                                        'id' => 'colour',
                                        'name' => 'colour',
                                        'class' => 'form-control',
                                      //  'required' => 'required',
                                      //  'pattern' => '[a-zA-Z\s]{1,15}',
                                        'placeholder' => 'colour',
                                        'value' => ($taxi_model->get('colour')) ? $taxi_model->get('colour') : ''
                                    ));
                                } else {
                                    echo text($taxi_model->get('colour'));
                                }
                                ?>
                        </div>
                        
                         <div class="col-lg-3 input_field_sections nowarp section-container">
                            <div class="section-message"></div>
                                    <?php
                                    echo form_label('Registration Number:', 'registrationNo', array(
                                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                                    ));

                                    if ($view_mode == EDIT_MODE) {
                                        echo form_input(array(
                                            'id' => 'registrationNo',
                                            'name' => 'registrationNo',
                                            'class' => 'form-control',
                                            'required' => 'required',
                                            'pattern' => '^[A-Z0-9]{2}[\s]{1}\d{1,4}$',
                                            'placeholder' => 'registrationNo',
                                            'value' => ($taxi_model->get('registrationNo')) ? $taxi_model->get('registrationNo') : ''
                                        ));
                                    } else {
                                        echo text($taxi_model->get('registrationNo'));
                                    }
                                    ?>

                        </div>
                        <div class="col-lg-3 input_field_sections nowarp">
                            <div class="fileinput fileinput-new" data-provides="fileinput"
                                 style="width: 100%">

                                <label>Registration Image:</label>
                                    <?php if ($view_mode == EDIT_MODE) { ?>
                                            <div>
                                                <span class="btn default btn-file" style="width: 52%;"> <span
                                                        class="fileinput-new"> Select image </span> <span
                                                        class="fileinput-exists"> Change </span>
                                        <?php
                                        echo form_upload(array(
                                            'id' => 'registrationImage',
                                            'name' => 'registrationImage[]',
                                            'class' => 'form-control',
                                            'accept' => 'image/*',
                                            'multiple' => 'multiple',
                                            // 'required' => 'required',
                                            // 'placeholder' => 'PAN Card Image',
                                            'value' => ($taxi_model->get('registrationImage')) ? taxi_content_url($taxi_model->get('id') . '/' .
                                                    $taxi_model->get('registrationImage')) : ''
                                        ));
                                        ?>
                                                                            </span>
                                                <a  href="javascript:;" class="btn red fileinput-exists"
                                                                    data-dismiss="fileinput"> <i class="fa fa-trash-o"></i>
                                                </a>

                                <div class="fileinput-new thumbnail"
                                     style="width: 60px; height: 60px;">
                                    <img
                                        src="<?php
                                    echo ($taxi_model->get('registrationImage')) ? 
                                        taxi_content_url($taxi_model->get('id') . '/' . $taxi_model->get('registrationImage')) : image_url('app/no_image.png');
                                    ?>"
                                        alt="">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail"
                                     style="max-width: 60px; max-height: 60px;"></div>

                                                                        </div>
                                    <?php } else { ?>
                                        <?php echo text($taxi_model->get('registrationImage')); ?>
                                    <div
                                        class="col-lg-3 chose-img">
                                        <img
                                            src="<?php
                                    echo ($taxi_model->get('registrationImage')) ? taxi_content_url($taxi_model->get('id') . '/' . 
                                            $taxi_model->get('registrationImage')) : image_url('app/no_image.png');
                                    ?>">
                                    </div>
                                    <?php } ?>
                            </div>

                        </div>
                        
                    </div>
                    
                    <div class="row">
                        
                        <div class="col-lg-3 input_field_sections nowarp">

                                <?php
                                echo form_label('Seat Capacity:', 'seatCapacity', array(
                                    'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                                ));

                                if ($view_mode == EDIT_MODE) {
                                    echo form_input(array(
                                        'id' => 'seatCapacity',
                                        'name' => 'seatCapacity',
                                        'class' => 'form-control',
                                        'required' => 'required',
                                        'pattern' => '\d{1,2}',
                                        'placeholder' => 'seat Capacity',
                                        'value' => ($taxi_model->get('seatCapacity')) ? $taxi_model->get('seatCapacity') : ''
                                    ));
                                } else {
                                    echo text($taxi_model->get('seatCapacity'));
                                }
                                ?>

                        </div>
                            
                        <div class="col-lg-3 input_field_sections nowarp">

                                    <?php
                                    echo form_label('Next FC Date:', 'nextFcDate', array(
                                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                                    ));

                                    if ($view_mode == EDIT_MODE) {
                                        echo form_input(array(
                                            'id' => 'nextFcDate',
                                            'name' => 'nextFcDate',
                                            'class' => 'form-control',
                                            'required' => 'required',
                                            'placeholder' => 'Next FC Date',
                                            'value' => ($taxi_model->get('nextFcDate')) ? $taxi_model->get('nextFcDate') : ''
                                        ));
                                    } else {
                                        echo text($taxi_model->get('nextFcDate'));
                                    }
                                    ?>

                        </div>
                        <div class="col-lg-3 input_field_sections nowarp">

                            <?php
                            echo form_label('Insurance Number:', 'insuranceNo', array(
                                'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                            ));

                            if ($view_mode == EDIT_MODE) {
                                echo form_input(array(
                                    'id' => 'insuranceNo',
                                    'name' => 'insuranceNo',
                                    'class' => 'form-control',
                                   // 'required' => 'required',
                                   // 'pattern' => '[a-zA-Z0-9\s\\-]{7,20}',
                                    'placeholder' => 'Insurance Number',
                                    'value' => ($taxi_model->get('insuranceNo')) ? $taxi_model->get('insuranceNo') : ''
                                ));
                            } else {
                                echo text($taxi_model->get('insuranceNo'));
                            }
                            ?>

                        </div>
                        <div class="col-lg-3 input_field_sections nowarp">

                            <?php
                            echo form_label('Insurance Expiry Date:', 'insuranceExpiryDate', array(
                                'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                            ));

                            if ($view_mode == EDIT_MODE) {
                                echo form_input(array(
                                    'id' => 'insuranceExpiryDate',
                                    'name' => 'insuranceExpiryDate',
                                    'class' => 'form-control',
                                   // 'required' => 'required',
                                    'placeholder' => 'Insurance Expiry Date',
                                    'value' => ($taxi_model->get('insuranceExpiryDate')) ? $taxi_model->get('insuranceExpiryDate') : ''
                                ));
                            } else {
                                echo text($taxi_model->get('insuranceExpiryDate'));
                            }
                            ?>

                        </div>
                        
                    </div>
                    
                    <div class="row">
                        <div class="col-lg-3 input_field_sections nowarp">

                            <?php
                            echo form_label('Minimum Speed:', 'minSpeed', array(
                                'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                            ));

                            if ($view_mode == EDIT_MODE) {
                                echo form_input(array(
                                    'id' => 'minSpeed',
                                    'name' => 'minSpeed',
                                    'class' => 'form-control',
                                   // 'required' => 'required',
                                   // 'pattern' => '\d{2,3}',
                                    'placeholder' => 'Minimum Speed',
                                    'value' => ($taxi_model->get('minSpeed')) ? $taxi_model->get('minSpeed') : ''
                                ));
                            } else {
                                echo text($taxi_model->get('minSpeed'));
                            }
                            ?>

                        </div>
                        
                        <div class="col-lg-3 input_field_sections nowarp">

                                        <?php
                                        echo form_label('Maximum Speed:', 'maxSpeed', array(
                                            'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                                        ));

                                        if ($view_mode == EDIT_MODE) {
                                            echo form_input(array(
                                                'id' => 'maxSpeed',
                                                'name' => 'maxSpeed',
                                                'class' => 'form-control',
                                              //  'required' => 'required',
                                              //  'pattern' => '\d{2,3}',
                                                'placeholder' => 'Maximum Speed',
                                                'value' => ($taxi_model->get('maxSpeed')) ? $taxi_model->get('maxSpeed') : ''
                                            ));
                                        } else {
                                            echo text($taxi_model->get('maxSpeed'));
                                        }
                                        ?>

                        </div>
                        
                        <div class="col-lg-3 input_field_sections nowarp">

                            <?php
                            echo form_label('Transmission Type:', 'transmissionType', array(
                                'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                            ));
                            if ($view_mode == EDIT_MODE) {
                                echo form_dropdown('transmissionType', $transmission_type_list, $taxi_model->get('transmissionType'), array(
                                    'id' => 'transmissionType',
                                    'class' => 'form-control',
                                    'required' => 'required'
                                ));
                            } else {
                                echo text($transmission_type_list [$taxi_model->get('transmissionType')]);
                            }
                            ?>

                        </div>

                            <div class="col-lg-3 input_field_sections nowarp">

                            <?php
                            echo form_label('Zone Name:', 'zoneId', array(
                                'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                            ));
                            // validation for passenger first name
                            if ($view_mode == EDIT_MODE) {
                                echo form_dropdown('zoneId', $zone_list, $taxi_model->get('zoneId'), array(
                                    'id' => 'zoneId',
                                    'class' => 'form-control',
                                    'required' => 'required'
                                ));
                            } else {
                                echo text($zone_list [$taxi_model->get('zoneId')]);
                            }
                            ?>
                        </div>
                    </div>
                    
                    <div class="row">
                        
                        
                        <div class="col-lg-3 input_field_sections nowarp">

                            <?php
                            echo form_label('Entity Name:', 'entityId', array(
                                'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                            ));
                            // validation for passenger first name
                            if ($view_mode == EDIT_MODE) {
                                echo form_dropdown('entityId', $entity_list, $taxi_model->get('entityId'), array(
                                    'id' => 'entityId',
                                    'class' => 'form-control',
                                    'required' => 'required'
                                ));
                            } else {
                                echo text($entity_list [$taxi_model->get('entityId')]);
                            }
                            ?>
                        </div>

                        <div class="col-lg-3 input_field_sections nowarp">
                            <div class="fileinput fileinput-new" data-provides="fileinput"
                                 style="width: 100%">

                                <label>Car Image:</label>
                            <?php if ($view_mode == EDIT_MODE) { ?>
                                    <div>
                                        <span class="btn default btn-file" style="width: 52%;"> <span
                                                class="fileinput-new"> Select image </span> <span
                                                class="fileinput-exists"> Change </span>
    <?php
    echo form_upload(array(
        'id' => 'carImage',
        'name' => 'carImage[]',
        'class' => 'form-control',
        'accept' => 'image/*',
        // 'required' => 'required',
        // 'placeholder' => 'PAN Card Image',
        'value' => ($taxi_model->get('carImage')) ? taxi_content_url($taxi_model->get('id') . '/' . $taxi_model->get('carImage')) : ''
    ));
    ?>
                                        </span> <a
                                            href="javascript:;" class="btn red fileinput-exists"
                                            data-dismiss="fileinput"> <i class="fa fa-trash-o"></i>
                                        </a>

                                        <div class="fileinput-new thumbnail"
                                             style="width: 60px; height: 60px;">
                                            <img
                                                src="<?php
                                                echo ($taxi_model->get('carImage')) ? taxi_content_url($taxi_model->get('id') . '/' . $taxi_model->get('carImage')) : image_url('app/no_image.png');
                                                ?>"
                                                alt="">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail"
                                             style="max-width: 60px; max-height: 60px;"></div>

                                    </div>
<?php } else { ?>
    <?php echo text($taxi_model->get('carImage')); ?>
                                    <div
                                        class="col-lg-3 chose-img">
                                        <img
                                            src="<?php
                                                echo ($taxi_model->get('carImage')) ? taxi_content_url($taxi_model->get('id') . '/' . $taxi_model->get('carImage')) : image_url('app/no_image.png');
                                                ?>">
                                    </div>
<?php } ?>
                            </div>

                        </div>

                        <div class="col-lg-3 input_field_sections nowarp">
                            <div class="fileinput fileinput-new" data-provides="fileinput"
                                 style="width: 100%">

                                <label>Car Driver Image:</label>
<?php if ($view_mode == EDIT_MODE) { ?>
                                    <div>
                                        <span class="btn default btn-file" style="width: 52%;"> <span
                                                class="fileinput-new"> Select image </span> <span
                                                class="fileinput-exists"> Change </span>
                                            <?php
                                            echo form_upload(array(
                                                'id' => 'carDriverImage',
                                                'name' => 'carDriverImage[]',
                                                'class' => 'form-control',
                                                'accept' => 'image/*',
                                                // 'required' => 'required',
                                                // 'placeholder' => 'PAN Card Image',
                                                'value' => ($taxi_model->get('carDriverImage')) ? taxi_content_url($taxi_model->get('id') . '/' . $taxi_model->get('carDriverImage')) : ''
                                            ));
                                            ?>
                                        </span> <a
                                            href="javascript:;" class="btn red fileinput-exists"
                                            data-dismiss="fileinput"> <i class="fa fa-trash-o"></i>
                                        </a>

                                        <div class="fileinput-new thumbnail"
                                             style="width: 60px; height: 60px;">
                                            <img
                                                src="<?php
                                                echo ($taxi_model->get('carDriverImage')) ? taxi_content_url($taxi_model->get('id') . '/' . $taxi_model->get('carDriverImage')) : image_url('app/no_image.png');
                                                ?>"
                                                alt="">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail"
                                             style="max-width: 60px; max-height: 60px;"></div>

                                    </div>
<?php } else { ?>
    <?php echo text($taxi_model->get('carDriverImage')); ?>
                                    <div
                                        class="col-lg-3 chose-img">
                                        <img
                                            src="<?php
    echo ($taxi_model->get('carDriverImage')) ? taxi_content_url($taxi_model->get('id') . '/' . $taxi_model->get('carDriverImage')) : image_url('app/no_image.png');
    ?>">
                                    </div>
                                            <?php } ?>
                            </div>

                        </div>

                        <div class="col-lg-3 input_field_sections nowarp">
                            <div class="fileinput fileinput-new" data-provides="fileinput"
                                 style="width: 100%">

                                <label>Wheel Tax Image:</label>
                                <?php if ($view_mode == EDIT_MODE) { ?>
                                    <div>
                                        <span class="btn default btn-file" style="width: 52%;"> <span
                                                class="fileinput-new"> Select image </span> <span
                                                class="fileinput-exists"> Change </span>
                                            <?php
                                            echo form_upload(array(
                                                'id' => 'wheelTaxImage',
                                                'name' => 'wheelTaxImage[]',
                                                'class' => 'form-control',
                                                'accept' => 'image/*',
                                                'multiple' => 'multiple',
                                                // 'required' => 'required',
                                                // 'placeholder' => 'PAN Card Image',
                                                'value' => ($taxi_model->get('wheelTaxImage')) ? taxi_content_url($taxi_model->get('id') . '/' . $taxi_model->get('wheelTaxImage')) : ''
                                            ));
                                            ?>
                                        </span> <a
                                            href="javascript:;" class="btn red fileinput-exists"
                                            data-dismiss="fileinput"> <i class="fa fa-trash-o"></i>
                                        </a>

                                        <div class="fileinput-new thumbnail"
                                             style="width: 60px; height: 60px;">
                                            <img
                                                src="<?php
                                                echo ($taxi_model->get('wheelTaxImage')) ? taxi_content_url($taxi_model->get('id') . '/' . $taxi_model->get('wheelTaxImage')) : image_url('app/no_image.png');
                                                ?>"
                                                alt="">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail"
                                             style="max-width: 60px; max-height: 60px;"></div>

                                    </div>
                                            <?php } else { ?>
                                                <?php echo text($taxi_model->get('wheelTaxImage')); ?>
                                    <div
                                        class="col-lg-3 chose-img">
                                        <img
                                            src="<?php
                                            echo ($taxi_model->get('wheelTaxImage')) ? taxi_content_url($taxi_model->get('id') . '/' . $taxi_model->get('wheelTaxImage')) : image_url('app/no_image.png');
                                            ?>">
                                    </div>
<?php } ?>
                            </div>

                        </div>
                        
                    </div>
                    
                    
                    <div class="row">
                        
                        
                        
                        <div class="col-md-3 input_field_sections nowarp gp-nomar">
                                            <?php
                                            echo form_label('Allocated:', 'isAllocated', array(
                                                'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                                            ));
                                            ?>
                            <div class="">
                                <label class="mt-checkbox">
                                <?php
                                $is_checked = $taxi_model->get('isAllocated') ? TRUE : FALSE;
                                $checked = $taxi_model->get('isAllocated') ? 1 : 0;
                                if ($view_mode == EDIT_MODE) {
                                    echo form_checkbox(array(
                                        'id' => 'isAllocated',
                                        'name' => 'isAllocated',
                                        'class' => 'flat'
                                            ),
                                            // 'required' => 'required',
                                            $checked, $is_checked);
                                } else {
                                    echo form_checkbox(array(
                                        'id' => 'isAllocated',
                                        'name' => 'isAllocated',
                                        'class' => 'flat',
                                        'disabled' => 'disabled'
                                            ),
                                            // 'required' => 'required',
                                            $checked, $is_checked);
                                }
                                ?> Allocated
                                    <span></span>
                                </label>
                            </div>
                        </div>

                    </div>
                    <div class="form-actions" style="margin-top: 22px;">
                        <div class="row">
                            <div class="col-md-12">
                                    <?php if ($view_mode == EDIT_MODE) { ?>
                                    <div class="gp-cen">
                                        <button type="button" id="cancel-taxi-btn"
                                                class="btn grey-salsa btn-outline">Cancel</button>
                                        <button type="button" id="save-taxi-btn" class="btn gpblue">Save</button>

                                    </div>
                                    <?php } else { ?>


                                    <div class="gp-cen">
                                        <button type="button"
                                                id="cancel-taxi-btn" class="btn btn-danger">Back</button>
                                    </div>
<?php } ?>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>
<?php
echo form_close();
?>
    </div>
</div>