<?php
$view_mode = $mode;
?>
<!-- BEGIN CONTENT -->

<!-- BEGIN CONTENT BODY -->
<div class="page-content">
<?php
	$form_attr = array (
			'name' => 'edit_emergency_form',
			'id' => 'edit_emergency_form',
			'method' => 'POST' 
	);
	echo form_open_multipart ( base_url ( '' ), $form_attr );
	// driver id by default is -1
	echo form_input ( array (
			'type' => 'hidden',
			'id' => 'emergency_id',
			'name' => 'id',
			'value' => ($emergency_model->get ( 'id' )) ? $emergency_model->get ( 'id' ) : - 1 
	) );
	echo form_input ( array (
			'type' => 'hidden',
			'id' => 'latitude',
			'name' => 'latitude',
			'value' => ($emergency_model->get ( 'latitude' ))?$emergency_model->get ( 'latitude' ):''
	) );
	echo form_input ( array (
			'type' => 'hidden',
			'id' => 'longitude',
			'name' => 'longitude',
			'value' => ($emergency_model->get ( 'longitude' ))?$emergency_model->get ( 'longitude' ):''
	) );
	echo form_input ( array (
			'type' => 'hidden',
			'id' => 'userId',
			'name' => 'userId',
			'value' => ($emergency_model->get ( 'userId' ))?$emergency_model->get ( 'userId' ):''
	) );
	?>
        <div class="page-bar ">
		<ul class="page-breadcrumb ">
			<li><a href="<?php echo base_url('Dashboard'); ?>">Home</a> <i
				class="fa fa-circle"></i></li>
			<li><a href="<?php echo base_url('Emergency/getEmergencyList'); ?>">SOS
					List</a> <i class="fa fa-circle"></i></li>
			<li><a href="<?php echo base_url(uri_string()); ?>"><?php echo ($emergency_model->get('id')) ? (($view_mode == VIEW_MODE) ? 'View' : 'Edit') : 'Create'; ?> SOS</a></li>

		</ul>

	</div>
	<!---- Page Bar ends ---->
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->

			<div class="portlet-body">
				<div class="form-body gp-form">
					<div class="card-header bg-white">
						<h1>
						<?php $user_type=($emergency_model->get('userType')==User_Type_Enum::DRIVER)?'Driver':(($emergency_model->get('userType')==User_Type_Enum::PASSENGER) ? 'Passenger': 'Other');?>
<?php echo ($emergency_model->get('id')) ? (($view_mode == VIEW_MODE) ? 'View ' : 'Edit ') : 'Create ';echo $user_type; ?> SOS</h1></div>
					
					<?php if($emergency_contact_model): ?>
					<div class="row">
					<table class="table table-hover table-bordered table-striped" style="width:100%">
									<thead>
										<tr>
											
											<th>Emergency Name</th>
											<th>Emergency Email</th>
											<th>Emergency Mobile</th>
											
										</tr>
									</thead>
									<tbody>
									<?php foreach ($emergency_contact_model as $list)
									{
										echo '<tr>';
										echo '<td>'.$list->name.'</td>';
										echo '<td>'.$list->email.'</td>';
										echo '<td>'.$list->mobile.'</td>';
										echo '</tr>';
										
									}	
									?>
									</tbody>
								</table>
					</div>
					<?php endif; ?>
						<div class="row">
							
							<div class="col-lg-4 input_field_sections">
								<?php
								
								echo form_label ( 'User Name:', 'userName', array (
										'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
								) );
								
									echo text ( $user_model->get ( 'firstName' )." ".$user_model->get ( 'lastName' ) );
								
								?>
								
							</div>
							<div class="col-lg-4 input_field_sections">
								<?php
								
								echo form_label ( 'User Email:', 'email', array (
										'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
								) );
								
								
									echo text ( $user_model->get ( 'email' ) );
								
								?>
								
							</div>
							<div class="col-lg-4 input_field_sections">
								<?php
								
								echo form_label ( 'User Mobile:', 'mobile', array (
										'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
								) );
								
								
									echo text ( $user_model->get ( 'mobile' ));
								
								?>
								
							</div>
							</div>
							<div class="row">
							<div class="col-lg-4 input_field_sections nowarp">
									<?php
									echo form_label ( 'Location:', 'location', array (
											'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
									) );
									
									if ($view_mode == EDIT_MODE) {
										echo form_textarea ( array (
												'id' => 'location',
												'name' => 'location',
												'class' => 'form-control',
												'readonly' => 'readonly',
												//'required' => 'required',
												'rows' => '2',
												'value' => ($emergency_model->get ( 'location' )) ? $emergency_model->get ( 'location' ) : '' 
										) );
									} else {
										echo form_textarea ( array (
												'id' => 'location',
												'name' => 'location',
												'class' => 'form-control',
												'readonly' => 'readonly',
												'rows' => '2',
												'value' => ($emergency_model->get ( 'location' )) ? $emergency_model->get ( 'location' ) : '' 
										) );
									}
									?>
								
							</div>
							<div class="col-lg-4 input_field_sections">
								
								<?php
								echo form_label ( 'Emergency Status:', 'emergencyStatus', array (
										'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
								) );
								// validation for passenger first name
								if ($view_mode == EDIT_MODE) {
									echo form_dropdown ( 'emergencyStatus', $emergency_list, $emergency_model->get ( 'emergencyStatus' ), array (
											'id' => 'emergencyStatus',
											'class' => 'form-control',
											'required' => 'required' 
									) );
								} else {
									echo text ( $emergency_list [$emergency_model->get ( 'emergencyStatus' )] );
								}
								
								?>
							</div>
							<div class="col-lg-4 input_field_sections nowarp">
									<?php
									echo form_label ( 'Comments:', 'comments', array (
											'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
									) );
									
									if ($view_mode == EDIT_MODE) {
										echo form_textarea ( array (
												'id' => 'comments',
												'name' => 'comments',
												'class' => 'form-control',
												'required' => 'required',
												'rows' => '3',
												'value' => ($emergency_model->get ( 'comments' )) ? $emergency_model->get ( 'comments' ) : '' 
										) );
									} else {
										echo form_textarea ( array (
												'id' => 'comments',
												'name' => 'comments',
												'class' => 'form-control',
												'readonly' => 'readonly',
												'rows' => '3',
												'value' => ($emergency_model->get ( 'comments' )) ? $emergency_model->get ( 'comments' ) : '' 
										) );
									}
									?>
								
							</div>
							
						</div>
						

							
							
						
						<div class="form-actions" style="margin-top: 22px;">
								<div class="row">
									<div class="col-md-12">
                                            <?php if ($view_mode == EDIT_MODE) { ?>
                                                <div class="gp-cen">
											
											<button type="button" id="cancel-emergency-btn"
												class="btn grey-salsa btn-outline">Cancel</button>
												<button type="button" id="save-emergency-btn" class="btn gpblue">Save</button>
										</div>
                                            <?php } else { ?>
                                            <div class="gp-cen">
                                                <button type="button"
											id="cancel-emergency-btn" class="btn btn-danger">Back</button></div>
                                            <?php } ?>
                                        </div>
								</div>
							</div>
					
				</div>
			</div>
		</div>
		<?php
		echo form_close ();
		?>
	</div>
</div>