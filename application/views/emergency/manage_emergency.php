<style>
.label.label-sm {
	font-size: 13px;
	padding: 2px 5px;
	cursor: pointer;
}
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">

		<div class="page-bar ">
			<ul class="page-breadcrumb ">
				<li><a href="<?php echo base_url('Dashboard');?>">Home</a> <i
					class="fa fa-circle"></i></li>
				<li><a href="<?php echo base_url('Emergency/getEmergencyList');?>">Emergency
						List</a></li>

			</ul>

		</div>
		<!---- Page Bar ends ---->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-list"></i>Emergency List
						</div>

					</div>

					<div class="portlet-body">
						<div class="table-toolbar">
							<div class="row">
								<div class="col-md-6">
									<div class="col-md-3 btn-group" style="visibility: hidden;">
										<a href="<?php echo base_url('Emergency/add');?>">
											<button id="sample_editable_1_new" class="btn sbold green1">
												Add New <i class="fa fa-plus"></i>
											</button>
										</a>
									</div>


								</div>

							</div>
						</div>
						<div id="table-columns" class="hidden">

							<label class="">
			                          <?php
																													echo form_checkbox ( array (
																															'class' => 'flat toggle-vis',
																															'data-column' => '6' 
																													) );
																													
																													?>
			                          Latitude
									</label> <label class="">
			                          <?php
																													echo form_checkbox ( array (
																															'class' => 'flat toggle-vis',
																															'data-column' => '7' 
																													) );
																													
																													?>
			                          Longitude
									</label> <label class="">
			                          <?php
																													echo form_checkbox ( array (
																															'class' => 'flat toggle-vis',
																															'data-column' => '8' 
																													) );
																													
																													?>
			                          Comments
									</label>

						</div>
						<table
							class="table table-striped table-bordered table-hover table-checkable order-column"
							id="emergency-data-table" style="width: 99%">

							<thead>
								<tr>
									<th>Action</th>
									<th>User Name</th>
									<th>User Mobile</th>
									<th>User Email</th>
									<th>User Type</th>
									<th>Location</th>
									<th>Latitude</th>
									<th>Longitude</th>
									<th>Comments</th>
									<th>Emergency Status</th>
									<th>Status</th>

								</tr>
							</thead>
							<tbody>
								<!-- <tbody class="fontaws"> -->
							</tbody>
							<!--  <tfoot>
										<tr>
											
											<th>Passenger First name</th>
											<th>PassengerLast name</th>
											<th>Passenger Mobile</th>
											<th>Location</th>
											<th>Latitude</th>
											<th>Longitude</th>
											<th>Comments</th>
											<th>Emeregency Status</th>
											<th>Action</th>
											
										</tr>
									</tfoot> -->
						</table>
					</div>
				</div>

			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>

<div class="modal fade" id="emergency-status" tabindex="-1"
	role="dialog" aria-labelledby="modal-login-label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
				<h3 class="modal-title" id="modal-login-label"
					style="text-align: center">Emergency Status</h3>

			</div>

			<div class="modal-body">
        				<?php
												$form_attr = array (
														'name' => 'edit_emergency_status_form',
														'id' => 'edit_emergency_status_form',
														'method' => 'POST' 
												);
												echo form_open_multipart ( base_url ( '' ), $form_attr );
												// driver id by default is -1
												
												echo form_input ( array (
														'type' => 'hidden',
														'id' => 'tempEmergencyId',
														'name' => 'tempEmergencyId',
														'value' => '' 
												) );
												
												?>
	                    
	                    	<div class="form-group mandatory-field">
	                    		 
<?php
echo form_label ( 'Emergency Status:', 'emergencyStatus', array (
		'class' => 'required' 
) );
echo form_dropdown ( 'emergencyStatus', $emergency_list, '', array (
		'id' => 'emergencyStatus',
		'class' => 'form-control',
		'required' => 'required' 
) );
?>
	                        </div>
				<div class="form-group mandatory-field">
	                        		
	                        		<label class='required'>Comments</label>
	                        		<?php
	                        		
																											
																											echo form_textarea ( array (
																													'id' => 'comments',
																													'name' => 'comments',
																													'class' => 'form-control',
																													
																													'required' => 'required',
																													'rows' => '3',
																													'value' => '' 
																											) );
																											?>
	                        </div>
				<div class="modal-footer">

					<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
					<button type="button" id="save-emergency-status-btn"
						class="btn btn-primary">Save</button>
				</div>
	                   <?php
																				echo form_close ();
																				?>
	                    
        			</div>

		</div>
	</div>
</div>