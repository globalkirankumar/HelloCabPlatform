<style>
.label.label-sm {
	font-size: 13px;
	padding: 2px 5px;
	cursor: pointer;
}
#taxi-device-data-table{
 float:left !important;}
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">

		<div class="page-bar ">
			<ul class="page-breadcrumb ">
				<li><a href="<?php echo base_url('Dashboard');?>">Home</a> <i
					class="fa fa-circle"></i></li>
				<li><a href="<?php echo base_url('Country/getCountryList');?>">Country
						List</a></li>

			</ul>

		</div>
		<!---- Page Bar ends ---->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-comments"></i>Country List
						</div>

					</div>

					<div class="portlet-body">
						<div class="table-toolbar">
							<div class="row">
								<div class="col-md-6">
									<div class="btn-group">
										<a href="<?php echo base_url('Country/add');?>">
											<button id="sample_editable_1_new" class="btn sbold green1">
												Add New <i class="fa fa-plus"></i>
											</button>
										</a>
									</div>
								</div>
								
							</div>
						</div>
								<table id="country-data-table"
									class="table table-hover table-bordered table-striped" style="width:99%">
									<thead>
										<tr>
											<th>Action</th>
											<th>Country name</th>
											<th>ISO Code</th>
											<th>Telephone Code</th>
											<th>Currency Code</th>
											<th>Currency Symbol</th>
											<th>Time Zone</th>
											<th>Status</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
									</table>
					</div>
				</div>

			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>