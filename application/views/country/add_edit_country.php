<?php
$view_mode = $mode;
?>
<!-- BEGIN CONTENT -->

<!-- BEGIN CONTENT BODY -->
<div class="page-content">
<?php
$form_attr = array (
		'name' => 'edit_country_form',
		'id' => 'edit_country_form',
		'method' => 'POST' 
);
echo form_open ( base_url ( '' ), $form_attr );
// country id by default is -1
echo form_input ( array (
		'type' => 'hidden',
		'id' => 'country_id',
		'name' => 'id',
		'value' => ($country_model->get ( 'id' )) ? $country_model->get ( 'id' ) : - 1 
) );
?>
        <div class="page-bar ">
		<ul class="page-breadcrumb ">
			<li><a href="<?php echo base_url('Dashboard'); ?>">Home</a> <i
				class="fa fa-circle"></i></li>
			<li><a href="<?php echo base_url('Country/getCountryList'); ?>">Country
					List</a> <i class="fa fa-circle"></i></li>
			<li><a href="<?php echo base_url(uri_string()); ?>"><?php echo ($country_model->get ( 'id' )) ?(($view_mode == VIEW_MODE)?'View':'Edit'): 'Create'; ?> Country</a></li>

		</ul>

	</div>
	<!---- Page Bar ends ---->
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->

			<div class="portlet-body">
				<div class="form-body gp-form">
					<div class="card-header bg-white">
						<h1><?php echo ($country_model->get ( 'id' )) ?(($view_mode == VIEW_MODE)?'View':'Edit'): 'Create'; ?> Country</h1>
					</div>


					<div class="row">
						<div class="col-lg-4 input_field_sections nowarp">
								<?php
								
								echo form_label ( 'Country Name:', 'name', array (
										'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
								) );
								
								if ($view_mode == EDIT_MODE) {
									echo form_input ( array (
											'id' => 'name',
											'name' => 'name',
											'class' => 'form-control',
											'required' => 'required',
											'pattern' => '[a-zA-Z\s]{3,30}',
											'placeholder' => 'Country Name',
											'value' => ($country_model->get ( 'name' )) ? $country_model->get ( 'name' ) : '' 
									) );
								} else {
									echo text ( $country_model->get ( 'name' ) );
								}
								?>
								
							</div>
						<div class="col-lg-4 input_field_sections nowarp">
								
									<?php
									echo form_label ( 'ISO Code:', 'isoCode', array (
											'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
									) );
									
									if ($view_mode == EDIT_MODE) {
										echo form_input ( array (
												'id' => 'isoCode',
												'name' => 'isoCode',
												'class' => 'form-control',
												'required' => 'required',
												'pattern' => '[A-Z]{3}',
												'placeholder' => 'ISC Code',
												'value' => ($country_model->get ( 'isoCode' )) ? $country_model->get ( 'isoCode' ) : '' 
										) );
									} else {
										echo text ( $country_model->get ( 'isoCode' ) );
									}
									?>
								
							</div>
						<div class="col-lg-4 input_field_sections nowarp">
								
									<?php
									echo form_label ( 'Telephone Code:', 'telephoneCode', array (
											'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
									) );
									
									if ($view_mode == EDIT_MODE) {
										echo form_input ( array (
												'id' => 'telephoneCode',
												'name' => 'telephoneCode',
												'class' => 'form-control',
												'required' => 'required',
												'pattern' => '[+]\d{2}',
												'placeholder' => 'Telephone Code',
												'value' => ($country_model->get ( 'telephoneCode' )) ? $country_model->get ( 'telephoneCode' ) : '' 
										) );
									} else {
										echo text ( $country_model->get ( 'telephoneCode' ) );
									}
									?>
								
							</div>
					</div>
					<div class="row">
						<div class="col-lg-4 input_field_sections nowarp">
								
									<?php
									echo form_label ( 'Currency Code:', 'currencyCode', array (
											'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
									) );
									
									if ($view_mode == EDIT_MODE) {
										echo form_input ( array (
												'id' => 'currencyCode',
												'name' => 'currencyCode',
												'class' => 'form-control',
												'required' => 'required',
												'pattern' => '[A-Z]{3}',
												'placeholder' => 'Currency Code',
												'value' => ($country_model->get ( 'currencyCode' )) ? $country_model->get ( 'currencyCode' ) : '' 
										) );
									} else {
										echo text ( $country_model->get ( 'currencyCode' ) );
									}
									
									?>
								
							</div>

						<div class="col-lg-4 input_field_sections nowarp">
								
								<?php
								echo form_label ( 'Currency Symbol:', 'currencySymbol', array (
										'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
								) );
								
								if ($view_mode == EDIT_MODE) {
									echo form_input ( array (
											'id' => 'currencySymbol',
											'name' => 'currencySymbol',
											'class' => 'form-control',
											'required' => 'required',
											'placeholder' => 'Currency Symbol',
											'value' => ($country_model->get ( 'currencySymbol' )) ? $country_model->get ( 'currencySymbol' ) : '' 
									) );
								} else {
									echo text ( $country_model->get ( 'currencySymbol' ) );
								}
								
								?>
							</div>
						<div class="col-lg-4 input_field_sections nowarp">
								
								<?php
								echo form_label ( 'Time Zone:', 'timeZone', array (
										'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
								) );
								
								if ($view_mode == EDIT_MODE) {
									echo form_input ( array (
											'id' => 'timeZone',
											'name' => 'timeZone',
											'class' => 'form-control',
											'required' => 'required',
											//'pattern' => '[a-zA-Z\s]{10,50}',
											'placeholder' => 'Time Zone',
											'value' => ($country_model->get ( 'timeZone' )) ? $country_model->get ( 'timeZone' ) : '' 
									) );
								} else {
									echo text ( $country_model->get ( 'timeZone' ) );
								}
								
								?>
							</div>

					</div>

					<div class="form-actions" style="margin-top: 22px;">
						<div class="row">
							<div class="col-md-12">
                                            <?php if ($view_mode == EDIT_MODE) { ?>
                                                <div class="gp-cen">
									<button type="button" id="cancel-country-btn"
										class="btn grey-salsa btn-outline">Cancel</button>
									<button type="button" id="save-country-btn" class="btn gpblue">Save</button>

								</div>
                                            <?php } else { ?>
                                            <div class="gp-cen">
                                                <button type="button"
									id="cancel-country-btn" class="btn btn-danger">Back</button></div>
                                            <?php } ?>
                                        </div>
						</div>
					</div>
				
		</div>
	</div>
</div>
</div>
<?php
echo form_close ();
?>
<!-- END EXAMPLE TABLE PORTLET-->
</div>