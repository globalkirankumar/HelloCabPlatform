<?php
$view_mode = $mode;
?>
<!-- BEGIN CONTENT -->

<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <?php
    $form_attr = array(
        'name' => 'edit_driver_form',
        'id' => 'edit_driver_form',
        'method' => 'POST'
    );
    echo form_open_multipart(base_url(''), $form_attr);
// driver id by default is -1
    echo form_input(array(
        'type' => 'hidden',
        'id' => 'driver_transaction_id',
        'name' => 'id',
        'value' => ($driver_transaction_model->get('id')) ? $driver_transaction_model->get('id') : - 1
    ));
    if ($this->uri->segment(2)=='addDriverTransaction')
    {
    echo form_input(array(
    		'type' => 'hidden',
    		'id' => 'driver_id',
    		'name' => 'driverId',
    		'value' => $this->uri->segment(3)
    ));
    }
    echo form_input(array(
    		'type' => 'hidden',
    		'id' => 'driverWallet',
    		'name' => 'driverWallet',
    		'value' => $driver_model->driverWallet
    ));
    echo form_input(array(
    		'type' => 'hidden',
    		'id' => 'driverCredit',
    		'name' => 'driverCredit',
    		'value' => $driver_model->driverCredit
    ));
    ?>
    <div class="page-bar ">
        <ul class="page-breadcrumb ">
            <li><a href="<?php echo base_url('Dashboard'); ?>">Home</a> <i
                    class="fa fa-circle"></i></li>
            <li><a href="<?php echo base_url('Driver/getDriverList'); ?>">Driver
                    List</a> <i class="fa fa-circle"></i></li>
            <li>
                <a href="<?php echo base_url(uri_string()); ?>">
                    <?php echo ($driver_transaction_model->get('id')) ? (($view_mode == VIEW_MODE) ? 'View' : 'Edit') : 'Create'; ?> Driver Transaction </a></li>

        </ul>

    </div>
    <!---- Page Bar ends ---->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->

            <div class="portlet-body">
                <div class="form-body gp-form">
                    <div class="card-header bg-white">
                        <h1>
                            <?php echo ($driver_transaction_model->get('id')) ? (($view_mode == VIEW_MODE) ? 'View' : 'Edit') : 'Create'; ?>
                            Driver( <?php echo $driver_model->firstName.' '.$driver_model->lastName; ?> ) Transaction 
                        </h1>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 input_field_sections nowarp">
                            <?php
                            echo form_label('TripId:', 'tripId', array(
                                'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                            ));

                            if ($view_mode == EDIT_MODE) {
                                echo form_input(array(
                                    'id' => 'tripId',
                                    'name' => 'tripId',
                                    'class' => 'form-control',
                                    'readonly' => 'readonly',
                                    'placeholder' => 'Trip Id',
                                    'value' => ($driver_transaction_model->get('tripId')) ? $driver_transaction_model->get('tripId') : ''
                                ));
                            } else {
                                echo text($driver_transaction_model->get('tripId'));
                            }
                            ?>

                        </div>
                        <div class="col-lg-3 input_field_sections nowarp">

                            <?php
                            echo form_label('Transaction Amount:', 'transactionAmount', array(
                                'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                            ));

                            if ($view_mode == EDIT_MODE) {
                                echo form_input(array(
                                    'id' => 'transactionAmount',
                                    'name' => 'transactionAmount',
                                    'class' => 'form-control',
                                    'required' => 'required',
                                   'pattern'=>'^[1-9]\d{0,6}([\.]?(\d{1,2}))$',
									'maxlength'=>'10',
                                    'placeholder' => 'Transaction Amount',
                                    'value' => ($driver_transaction_model->get('transactionAmount')) ? $driver_transaction_model->get('transactionAmount') : ''
                                ));
                            } else {
                                echo text($driver_transaction_model->get('transactionAmount'));
                            }
                            ?>

                        </div>
                        <div
                            class="col-lg-3 input_field_sections nowarp section-container">
                            <div class="section-message hidden"></div>
                            <?php
                            echo form_label('Previous Amount:', 'previousAmount', array(
                                'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                            ));

                            if ($view_mode == EDIT_MODE) {
                                echo form_input(array(
                                    'id' => 'previousAmount',
                                    'name' => 'previousAmount',
                                    'class' => 'form-control',
                                    'readonly' => 'readonly',
                                	'required' => 'required',
                                    'pattern'=>'^[1-9]\d{0,6}([\.]?(\d{1,2}))$',
									'maxlength'=>'10',
                                    'placeholder' => 'Previous Amount',
                                    'value' => ($driver_transaction_model->get('previousAmount')) ? $driver_transaction_model->get('previousAmount') : $driver_model->driverWallet
                                ));
                            } else {
                                echo text($driver_transaction_model->get('previousAmount'));
                            }
                            ?>

                        </div>
                        <div
                            class="col-lg-3 input_field_sections nowarp section-container">
                            <div class="section-message hidden"></div>
                            <?php
                            echo form_label('Current Amount:', 'currentAmount', array(
                                'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                            ));

                            if ($view_mode == EDIT_MODE) {
                                echo form_input(array(
                                    'id' => 'currentAmount',
                                    'name' => 'currentAmount',
                                    'class' => 'form-control',
                                    'readonly' => 'readonly',
                                	'required' => 'required',
                                	'pattern'=>'^[1-9]\d{0,6}([\.]?(\d{1,2}))$',
                                	'maxlength'=>'10',
                                    'placeholder' => 'Current Amount',
                                    'value' => ($driver_transaction_model->get('currentAmount')) ? $driver_transaction_model->get('currentAmount') : ''
                                ));
                            } else {
                                echo text($driver_transaction_model->get('currentAmount'));
                            }
                            ?>

                        </div>

                    </div>


                    <div class="row">

						<div class="col-md-3 input_field_sections nowarp">



                            <?php
                            echo form_label('Transaction From:', 'transactionFrom', array(
                                'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                            ));
                            // validation for passenger first name
                            if ($view_mode == EDIT_MODE) {
                                echo form_dropdown('transactionFrom', $transaction_from_list, ($driver_transaction_model->get('transactionFrom'))?$driver_transaction_model->get('transactionFrom'):Transaction_From_Enum::WALLET_ACCOUNT, array(
                                    'id' => 'transactionFrom',
                                    'class' => 'form-control',
                                    'required' => 'required'
                                ));
                            } else {
                                echo text($transaction_from_list [$driver_transaction_model->get('transactionFrom')]);
                            }
                            ?>



                        </div>


                        
                        <div class="col-md-3 input_field_sections nowarp">



                            <?php
                            echo form_label('Transaction Type:', 'transactionType', array(
                                'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                            ));
                            // validation for passenger first name
                            if ($view_mode == EDIT_MODE) {
                                echo form_dropdown('transactionType', $transaction_type_list, $driver_transaction_model->get('transactionType'), array(
                                    'id' => 'transactionType',
                                    'class' => 'form-control',
                                    'required' => 'required'
                                ));
                            } else {
                                echo text($transaction_type_list [$driver_transaction_model->get('transactionType')]);
                            }
                            ?>



                        </div>

                        <div class="col-md-3 input_field_sections nowarp">

                            <?php
                            echo form_label('Transaction Mode:', 'transactionMode', array(
                                'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                            ));
                            // validation for passenger first name
                            if ($view_mode == EDIT_MODE) {
                                echo form_dropdown('transactionMode', $transaction_mode_list, $driver_transaction_model->get('transactionMode'), array(
                                    'id' => 'transactionMode',
                                    'class' => 'form-control',
                                    'required' => 'required'
                                ));
                            } else {
                                echo text($transaction_mode_list[$driver_transaction_model->get('transactionMode')]);
                            }
                            ?>
                        </div>
                        <div class="col-md-3 input_field_sections nowarp">

                            <?php
                            echo form_label('Transaction Id:', 'transactionId', array(
                                'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                            ));

                            if ($view_mode == EDIT_MODE) {
                                echo form_input(array(
                                    'id' => 'transactionId',
                                    'name' => 'transactionId',
                                    'class' => 'form-control',
                                    //'required' => 'required',
                                    'placeholder' => 'Transaction Id',
                                    'value' => ($driver_transaction_model->get('transactionId')) ? $driver_transaction_model->get('transactionId') : ''
                                ));
                            } else {
                                echo text($driver_transaction_model->get('transactionId'));
                            }
                            ?>

                        </div>

                    </div>
                    <div class="row">

						<div class="col-lg-3 input_field_sections nowarp">

                            <?php
                            echo form_label('Transaction Status:', 'transactionStatus', array(
                                'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                            ));

                            if ($view_mode == EDIT_MODE) {
                                echo form_textarea(array(
                                    'id' => 'transactionStatus',
                                    'name' => 'transactionStatus',
                                    'rows' => '3',
                                    'class' => 'form-control',
                                   // 'required' => 'required',
                                    'value' => ($driver_transaction_model->get('transactionStatus')) ? $driver_transaction_model->get('transactionStatus') : ''
                                ));
                            } else {
                                echo form_textarea(array(
                                    'id' => 'transactionStatus',
                                    'name' => 'transactionStatus',
                                    'class' => 'form-control',
                                    'rows' => '3',
                                    'readonly' => 'readonly',
                                    'value' => ($driver_transaction_model->get('transactionStatus')) ? $driver_transaction_model->get('transactionStatus') : ''
                                ));
                            }
                            ?>

                        </div>
                         <div class="col-md-3 input_field_sections nowarp">

                            <?php
                            echo form_label('Comments:', 'comments', array(
                                'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                            ));

                            if ($view_mode == EDIT_MODE) {
                                echo form_textarea(array(
                                    'id' => 'comments',
                                    'name' => 'comments',
                                    'rows' => '3',
                                    'class' => 'form-control',
                                   // 'required' => 'required',
                                    'value' => ($driver_transaction_model->get('comments')) ? $driver_transaction_model->get('comments') : ''
                                ));
                            } else {
                                echo form_textarea(array(
                                    'id' => 'comments',
                                    'name' => 'comments',
                                    'class' => 'form-control',
                                    'rows' => '3',
                                    'readonly' => 'readonly',
                                    'value' => ($driver_transaction_model->get('comments')) ? $driver_transaction_model->get('comments') : ''
                                ));
                            }
                            ?>
                        </div>







                    </div>



                    <div class="row"></div>

                    <div class="form-actions" style="margin-top: 22px;">
                        <div class="row">
                            <div class="col-md-12">
                                <?php if ($view_mode == EDIT_MODE) { ?>
                                    <div class="gp-cen">

                                        <button type="button" id="cancel-driver-transaction-btn"
                                                class="btn grey-salsa btn-outline">Cancel</button>
                                        <button type="button" id="save-driver-transaction-btn" class="btn gpblue">Save</button>
                                    </div>
                                <?php } else { ?>
                                <div class="gp-cen">
                                    <button type="button"
                                            id="cancel-driver-transaction-btn" class="btn btn-danger">Back</button>
                                      </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <?php
    echo form_close();
    ?>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>




