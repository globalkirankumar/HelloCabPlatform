<script src="<?php echo base_url('public/js/jquery-3.2.1.min.js');?>"></script>
<script src="https://maps.googleapis.com/maps/api/js?key="<?php echo GOOGLE_MAP_API_KEY; ?>"&libraries=geometry&callback=app.initialize" async defer></script>
<!-- BEGIN CONTENT -->

<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <input type="hidden" id="marker_address" name="marker_address" value="">
    <style>
       #map-f {
        height: 600px;
     /*position: relative;*/
    width: 100%;
    }
 html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
   </style>
            <div id="map-f"></div>
    <!-- END EXAMPLE TABLE PORTLET--> 
</div>

<script>
            'use strict';
            var app = {
                // Utility function to make a polygon with some standard properties set
                makePolygon: function (paths, color) {
                    return (new google.maps.Polygon({
                        paths: paths,
                        strokeColor: color,
                        strokeOpacity: 0.8,
                        strokeWeight: 2,
                        fillColor: color,
                        fillOpacity: 0.35
                    }));
                },
                // Run on page load
                initialize: function () {
                    var map,
                    polygons = [];
                    var INTERVAL = 15000;
                    //Used to remember markers
                    var markerStore = {};
                    // Add useful missing method - credit to http://stackoverflow.com/a/13772082/5338708
                    google.maps.Polygon.prototype.getBoundingBox = function () {
                        var bounds = new google.maps.LatLngBounds();

                        this.getPath().forEach(function (element, index) {
                            bounds.extend(element)
                        });

                        return(bounds);
                    };
                    // Add center calculation method
                    google.maps.Polygon.prototype.getApproximateCenter = function () {
                        var boundsHeight = 0,
                                boundsWidth = 0,
                                centerPoint,
                                heightIncr = 0,
                                maxSearchLoops,
                                maxSearchSteps = 10,
                                n = 1,
                                northWest,
                                polygonBounds = this.getBoundingBox(),
                                testPos,
                                widthIncr = 0;

                        // Get polygon Centroid
                        centerPoint = polygonBounds.getCenter();

                        if (google.maps.geometry.poly.containsLocation(centerPoint, this)) {
                            // Nothing to do Centroid is in polygon use it as is
                            return centerPoint;
                        } else {
                            maxSearchLoops = maxSearchSteps / 2;

                            // Calculate NorthWest point so we can work out height of polygon NW->SE
                            northWest = new google.maps.LatLng(polygonBounds.getNorthEast().lat(), polygonBounds.getSouthWest().lng());

                            // Work out how tall and wide the bounds are and what our search increment will be
                            boundsHeight = google.maps.geometry.spherical.computeDistanceBetween(northWest, polygonBounds.getSouthWest());
                            heightIncr = boundsHeight / maxSearchSteps;
                            boundsWidth = google.maps.geometry.spherical.computeDistanceBetween(northWest, polygonBounds.getNorthEast());
                            widthIncr = boundsWidth / maxSearchSteps;

                            // Expand out from Centroid and find a point within polygon at 0, 90, 180, 270 degrees
                            for (; n <= maxSearchLoops; n++) {
                                // Test point North of Centroid
                                testPos = google.maps.geometry.spherical.computeOffset(centerPoint, (heightIncr * n), 0);
                                if (google.maps.geometry.poly.containsLocation(testPos, this)) {
                                    break;
                                }

                                // Test point East of Centroid
                                testPos = google.maps.geometry.spherical.computeOffset(centerPoint, (widthIncr * n), 90);
                                if (google.maps.geometry.poly.containsLocation(testPos, this)) {
                                    break;
                                }

                                // Test point South of Centroid
                                testPos = google.maps.geometry.spherical.computeOffset(centerPoint, (heightIncr * n), 180);
                                if (google.maps.geometry.poly.containsLocation(testPos, this)) {
                                    break;
                                }

                                // Test point West of Centroid
                                testPos = google.maps.geometry.spherical.computeOffset(centerPoint, (widthIncr * n), 270);
                                if (google.maps.geometry.poly.containsLocation(testPos, this)) {
                                    break;
                                }
                            }

                            return(testPos);
                        }
                    };
                    // Set up map around Chicago 
                    map = new google.maps.Map(document.getElementById('map-f'), {
                        center: <?php echo $center_lat_long ;?>
                        zoom: <?php echo $map_zoom ;?>
                    });
                    
                    polygons.push(this.makePolygon(
                            [
                                <?php foreach ($zone_polygan_data as $points) { echo $points ; } ;?>
                            ],
                            '#F57F17'
                            ));

                    // },2000);

                    // Put sample polygons on the map with marker at approximated center
                    polygons.forEach(function (poly) {
                        console.log(poly);
                        poly.setMap(map);
                          getMarkers();  
                    });
                  
                  function getMarkers() {
                    console.log('getMarkers');
                    var url        = location.href;
                    var segments   = url.split( '/' );
                    var zone_id    = segments[5];
                    var zoneid;
                    if(isNaN(zone_id)){
                        zoneid = 0;
                    }
                    if(zone_id > 0){
                        zoneid = zone_id;
                    }
                    var url = location.origin + '/Driver/getLatLongOfDriverByZone/';
				jQuery.ajax({
					method : "POST",
					url : url,
                                        data:{'zone_id':zoneid}
				}).done(function(response) {
                                    
					response = jQuery.parseJSON(response);
                                            $.each(response, function(i, item) { 
                                             var pzone_id   = item.zone_id;
                                             var driver_id = item.driver_id;
                                             var lat       = item.latitude;
                                             var long      = item.longitutde;
                                             var status    = item.status;
                                             var name      = item.driver_name;
                                             var mobile    = item.driver_mobile;
                                             var taxi_registration    = item.taxi_registration;
                                             var marker_icon ;
                                             if(status==40){
                                                 marker_icon = 'driver_free.png';
                                             }
                                             if(status==41){
                                                 marker_icon = 'driver_busy.png';
                                             }
                        /** show markers only if Selected zone matches ***/
                           //if(zoneid == pzone_id) {  
                                var infowindow = new google.maps.InfoWindow();
                                var geocoder = new google.maps.Geocoder();
                                /*** before Ajax Call ***/
                                if(markerStore.hasOwnProperty(driver_id)) {
                                        markerStore[driver_id].setPosition(new google.maps.LatLng(lat,long));
                                        markerStore[driver_id].setIcon('<?php echo base_url('public/images/app/');?>'+marker_icon);
                                        var coord1 = new google.maps.LatLng(lat,long);
                                        var bounds = new google.maps.LatLngBounds();
                                            google.maps.event.addListener(marker, 'click', function(event) {
                                            geocoder.geocode({
                                                'latLng': coord1
                                                 }, function(results, status) {
                                                 if (status == google.maps.GeocoderStatus.OK) {
                                                    if (results[0]) {
                                                        var address = results[0].formatted_address+'<br>'+'<b>Name</b> :'+name+'<br>'+'<b>Mobile</b> :' +mobile+'<br><b>Taxi</b> :' +taxi_registration;
                                                        infowindow.setContent(address);
                                                            infowindow.open(marker.get('map'), marker);
                                                            setTimeout(function(){
                                                                infowindow.close();
                                                            },5000); 
                                                        }
                                                    }
                                                 });
                                            });
                                            
                                    } 
                                      /*** After Ajax Call ***/
                                        else {
                                            var marker = new google.maps.Marker({
                                                                                position: new google.maps.LatLng(lat,long),
                                                                                title:'Driver',
                                                                                map:map,
                                                                                icon:'<?php echo base_url('public/images/app/');?>'+marker_icon
                                                                        });	
                                            var coord1 = new google.maps.LatLng(lat,long);
                                            var bounds = new google.maps.LatLngBounds();
                                            google.maps.event.addListener(marker, 'click', function(event) {
                                            geocoder.geocode({
                                                'latLng': coord1
                                                 }, function(results, status) {
                                                 if (status == google.maps.GeocoderStatus.OK) {
                                                    if (results[0]) {
                                                        var address = results[0].formatted_address+'<br>'+'<b>Name</b> :'+name+'<br>'+'<b>Mobile</b> :' +mobile+'<br><b>Taxi</b> :' +taxi_registration;
                                                        infowindow.setContent(address);
                                                            infowindow.open(marker.get('map'), marker);
                                                            setTimeout(function(){
                                                                infowindow.close();
                                                            },5000); 
                                                        }
                                                    }
                                                 });
                                            });
                                                          
                                                markerStore[driver_id] = marker;
                                                console.log(marker.getTitle());
                                    }
                              //  }      
//                                                 
                            });
                });
                   window.setTimeout(getMarkers,INTERVAL);
                    
                }
            }
            
            
        }
</script>

<script>
$(function(){
    

 $('#header_zone').change(function(){
    var zone_id =  $(this).val();
    if(zone_id > 0){
           window.location = location.origin+'/Driver/trackDrivers/'+zone_id;
    }
    
 });
    var url           = location.href;
    var segments       = url.split( '/' );
    var zone_select    = segments[5];
  setTimeout(function(){
     if(zone_select > 0){
                $('#header_zone').selectpicker('val', zone_select);
             }else{
                $('#header_zone').selectpicker('val', ''); 
             }  
  },1000);
     
});
</script>