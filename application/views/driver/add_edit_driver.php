<?php
$view_mode = $mode;
?>
<!-- BEGIN CONTENT -->

<!-- BEGIN CONTENT BODY -->

<div class="page-content">
    <?php
    $form_attr = array(
        'name' => 'edit_driver_form',
        'id' => 'edit_driver_form',
        'method' => 'POST'
    );
    echo form_open_multipart(base_url(''), $form_attr);
// driver id by default is -1
    echo form_input(array(
        'type' => 'hidden',
        'id' => 'driver_id',
        'name' => 'id',
        'value' => ($driver_model->get('id')) ? $driver_model->get('id') : - 1
    ));
    echo form_input(array(
        'type' => 'hidden',
        'id' => 'entityId',
        'name' => 'entityId',
        'value' => DEFAULT_COMPANY_ID
    ));
    ?>
    <div class="page-bar ">
        <ul class="page-breadcrumb ">
            <li><a href="<?php echo base_url('Dashboard'); ?>">Home</a> <i
                    class="fa fa-circle"></i></li>
            <li><a href="<?php echo base_url('Driver/getDriverList'); ?>">Driver
                    List</a> <i class="fa fa-circle"></i></li>
            <li><a href="<?php echo base_url(uri_string()); ?>"><?php echo ($driver_model->get('id')) ? (($view_mode == VIEW_MODE) ? 'View' : 'Edit') : 'Create'; ?>Driver</a></li>
        </ul>
    </div>
    <!---- Page Bar ends ---->
    <div class="row">
        <div class="col-md-12"> 
            <!-- BEGIN EXAMPLE TABLE PORTLET-->

            <div class="portlet-body">
                <div class="form-body gp-form">
                    <div class="card-header bg-white">
                        <h1><?php echo ($driver_model->get('id')) ? (($view_mode == VIEW_MODE) ? 'View' : 'Edit') : 'Create'; ?> Driver</h1>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 input_field_sections nowarp">
                            <?php
                            echo form_label('First Name:', 'firstName', array(
                                'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                            ));

                            if ($view_mode == EDIT_MODE) {
                                echo form_input(array(
                                    'id' => 'firstName',
                                    'name' => 'firstName',
                                    'class' => 'form-control',
                                    'required' => 'required',
                                    'pattern' => '[a-zA-Z\s]{3,15}',
                                    'placeholder' => 'First Name',
                                    'value' => ($driver_model->get('firstName')) ? $driver_model->get('firstName') : ''
                                ));
                            } else {
                                echo text($driver_model->get('firstName'));
                            }
                            ?>
                        </div>
                        <div class="col-lg-3 input_field_sections nowarp">
                            <?php
                            echo form_label('Last Name:', 'lastName', array(
                                'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                            ));

                            if ($view_mode == EDIT_MODE) {
                                echo form_input(array(
                                    'id' => 'lastName',
                                    'name' => 'lastName',
                                    'class' => 'form-control',
                                    // 'required' => 'required',
                                    // 'pattern' => '[a-zA-Z\s]{1,15}',
                                    'placeholder' => 'Last Name',
                                    'value' => ($driver_model->get('lastName')) ? $driver_model->get('lastName') : ''
                                ));
                            } else {
                                echo text($driver_model->get('lastName'));
                            }
                            ?>
                        </div>
                        <div
                            class="col-lg-3 input_field_sections nowarp section-container">
                            <div class="section-message"></div>
                            <?php
                            echo form_label('Email:', 'email', array(
                                'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                            ));

                            if ($view_mode == EDIT_MODE) {
                                echo form_input(array(
                                    'id' => 'email',
                                    'name' => 'email',
                                    'class' => 'form-control',
                                    // 'required' => 'required',
                                    // 'pattern' => '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$',
                                    'placeholder' => 'Email Id',
                                    'value' => ($driver_model->get('email')) ? $driver_model->get('email') : ''
                                ));
                            } else {
                                echo text($driver_model->get('email'));
                            }
                            ?>
                        </div>
                        <div
                            class="col-lg-3 input_field_sections nowarp section-container">
                            <div class="section-message"></div>
                            <?php
                            echo form_label('Mobile No:', 'mobile', array(
                                'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                            ));

                            if ($view_mode == EDIT_MODE) {
                                echo form_input(array(
                                    'id' => 'mobile',
                                    'name' => 'mobile',
                                    'class' => 'form-control',
                                    'required' => 'required',
                                    'pattern' => '^[0]?[789]\d{7,9}$',
                                    'placeholder' => 'Mobile',
                                	'maxlength'=>'11',
                                    'value' => ($driver_model->get('mobile')) ? $driver_model->get('mobile') : ''
                                ));
                            } else {
                                echo text($driver_model->get('mobile'));
                            }
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 input_field_sections nowarp">
                            <?php
                            echo form_label('License No:', 'licenseNo', array(
                                'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                            ));

                            if ($view_mode == EDIT_MODE) {
                                echo form_input(array(
                                    'id' => 'licenseNo',
                                    'name' => 'licenseNo',
                                    'class' => 'form-control',
                                    'required' => 'required',
                                    'pattern' => '[a-zA-Z0-9-/\s]{1,30}',
                                    'placeholder' => 'License No',
                                    'value' => ($driver_model->get('licenseNo')) ? $driver_model->get('licenseNo') : ''
                                ));
                            } else {
                                echo text($driver_model->get('licenseNo'));
                            }
                            ?>
                        </div>
                        <div class="col-md-3 input_field_sections nowarp">
                            <?php
                            echo form_label('License Expiry Date:', 'licenseExpiryDate', array(
                                'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                            ));

                            if ($view_mode == EDIT_MODE) {
                                echo form_input(array(
                                    'id' => 'licenseExpiryDate',
                                    'name' => 'licenseExpiryDate',
                                    'class' => 'form-control',
                                    'required' => 'required',
                                    'placeholder' => 'License Expiry Date',
                                    'value' => ($driver_model->get('licenseExpiryDate')) ? $driver_model->get('licenseExpiryDate') : ''
                                ));
                            } else {
                                echo text($driver_model->get('licenseExpiryDate'));
                            }
                            ?>
                        </div>
                        <div class="col-md-3 input_field_sections nowarp">
                            <?php
                            echo form_label('License Type:', 'licenseType', array(
                                'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                            ));
                            // validation for passenger first name
                            if ($view_mode == EDIT_MODE) {
                                echo form_dropdown('licenseType', $license_type_list, $driver_model->get('licenseType'), array(
                                    'id' => 'licenseType',
                                    'class' => 'form-control',
                                    'required' => 'required'
                                ));
                            } else {
                                echo text($license_type_list [$driver_model->get('licenseType')]);
                            }
                            ?>
                        </div>
                        <div class="col-md-3 input_field_sections nowarp">
                            <?php
                            echo form_label('Languages Known:', 'languageKnown', array(
                                'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                            ));
                            if ($view_mode == EDIT_MODE) {
                                echo form_input(array(
                                    'id' => 'languageKnown',
                                    'name' => 'languageKnown',
                                    'class' => 'form-control',
                                    'required' => 'required',
                                    'pattern' => '[a-zA-Z,]{3,50}',
                                    'placeholder' => 'Enter languages seperated by comma with out any space',
                                    'value' => ($driver_model->get('languageKnown')) ? $driver_model->get('languageKnown') : ''
                                ));
                            } else {
                                echo text($driver_model->get('languageKnown'));
                            }
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 input_field_sections nowarp">
                            <?php
                            echo form_label('Experience:', 'experience', array(
                                'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                            ));
// validation for passenger first name
                            if ($view_mode == EDIT_MODE) {
                                echo form_dropdown('experience', $experience_list, $driver_model->get('experience'), array(
                                    'id' => 'experience',
                                    'class' => 'form-control',
                                    'required' => 'required'
                                ));
                            } else {
                                echo text($experience_list [$driver_model->get('experience')]);
                            }
                            ?>
                        </div>
                        <div class="col-md-3 input_field_sections nowarp">
                            <?php
                            echo form_label('Zone Name:', 'zoneId', array(
                                'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                            ));
                            // validation for passenger first name
                            if ($view_mode == EDIT_MODE) {
                                echo form_dropdown('zoneId', $zone_list, $driver_model->get('zoneId'), array(
                                    'id' => 'zoneId',
                                    'class' => 'form-control',
                                    'required' => 'required'
                                ));
                            } else {
                                echo text($zone_list [$driver_model->get('zoneId')]);
                            }
                            ?>
                        </div>
                         <div class="col-md-3 input_field_sections nowarp">
                            <?php
                            echo form_label('Entity Name:', 'entityId', array(
                                'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                            ));
                            // validation for passenger first name
                            if ($view_mode == EDIT_MODE) {
                                echo form_dropdown('entityId', $entity_list, ($driver_model->get('entityId'))?$driver_model->get('entityId'):DEFAULT_COMPANY_ID, array(
                                    'id' => 'entityId',
                                    'class' => 'form-control',
                                    'required' => 'required'
                                ));
                            } else {
                                echo text($entity_list [$driver_model->get('entityId')]);
                            }
                            ?>
                        </div>
                        <div class="col-md-3 input_field_sections nowarp gp-nomar">
                            <?php
                            echo form_label('Driver Verified:', 'isVerified', array(
                                'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                            ));
                            ?>
                            <div class="">
                                <label class="mt-checkbox">
                                    <?php
                                    $is_checked = $driver_model->get('isVerified') ? TRUE : FALSE;
                                    $checked = $driver_model->get('isVerified') ? 1 : 0;
                                    if ($view_mode == EDIT_MODE) {
                                        echo form_checkbox(array(
                                            'id' => 'isVerified',
                                            'name' => 'isVerified',
                                            'class' => 'flat'
                                                )
                                                // 'required' => 'required',
                                                , $checked, $is_checked);
                                    } else {
                                        echo form_checkbox(array(
                                            'id' => 'isVerified',
                                            'name' => 'isVerified',
                                            'class' => 'flat',
                                            'disabled' => 'disabled'
                                                )
                                                // 'required' => 'required',
                                                , $checked, $is_checked);
                                    }
                                    ?>
                                    Is Verified <span></span> </label>
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                    	<div class="col-md-3 input_field_sections nowarp">
                            <div class="fileinput fileinput-new" data-provides="fileinput"
                                 style="width: 100%">
                                <label>Police Recommendation Image:</label>
                                <?php if ($view_mode == EDIT_MODE) { ?>
                                    <div> <span class="btn default btn-file" style="width: 52%;"> <span
                                                class="fileinput-new"> Select Document </span> <span
                                                class="fileinput-exists"> Change </span>
                                                <?php
                                                echo form_upload(array(
                                                    'id' => 'policeRecommendationImage',
                                                    'name' => 'policeRecommendationImage[]',
                                                    'class' => '',
                                                    "accept" => "image/*",
                                                    // 'required' => 'required',
                                                    'placeholder' => 'License Image',
                                                    'value' => ($driver_model->get('policeRecommendationImage')) ? $driver_model->get('policeRecommendationImage') : ''
                                                ));
                                                ?>
                                        </span> <a href="" class="btn red fileinput-exists" data-dismiss="fileinput"> <i class="fa fa-trash-o"></i> </a>
                                        <div class="fileinput-new thumbnail"
                                             style="width: 60px; height: 60px;">
                                            <a href="<?php
                                            echo ($driver_model->get('policeRecommendationImage')) ?
                                                    driver_content_url($driver_model->get('id') . '/' . $driver_model->get('policeRecommendationImage')) : 
                                                image_url('app/no_image.png');
                                            ?>" data-imagelightbox="demo" data-ilb2-caption="Police Recommendation Image">
                                                <img src="<?php
                                                echo ($driver_model->get('policeRecommendationImage')) ?
                                                        driver_content_url($driver_model->get('id') . '/' . $driver_model->get('policeRecommendationImage')) :
                                                        image_url('app/no_image.png');
                                                ?>" alt="">
                                            </a>
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail"
                                             style="max-width: 60px; max-height: 60px;"></div>
                                    </div>
                                <?php } else { ?>
                                    <?php //echo text($driver_model->get('policeRecommendationImage')); ?><br>
                                    <div class="col-lg-3 chose-img"> 
                                        <a href="<?php
                                            echo ($driver_model->get('policeRecommendationImage')) ?
                                            driver_content_url($driver_model->get('id') . '/' . $driver_model->get('policeRecommendationImage')) :
                                                image_url('app/no_image.png');
                                            ?>" data-imagelightbox="demo" data-ilb2-caption="Police Recommendation Image">
                                        <img src="<?php
                                            echo ($driver_model->get('policeRecommendationImage')) ?
                                            driver_content_url($driver_model->get('id') . '/' . $driver_model->get('policeRecommendationImage')) :
                                                image_url('app/no_image.png');
                                            ?>"> 
                                        </a>
                                        </div>
                                    <?php } ?>
                            </div>
                        </div>
                        <div class="col-md-3 input_field_sections nowarp">
                            <div class="fileinput fileinput-new" data-provides="fileinput"
                                 style="width: 100%">
                                <label>Profile Image:</label>
                                <?php if ($view_mode == EDIT_MODE) { ?>
                                    <div> <span class="btn default btn-file" style="width: 52%;"> <span
                                                class="fileinput-new"> Select image </span> <span
                                                class="fileinput-exists"> Change </span>
                                                <?php
                                                echo form_upload(array(
                                                    'id' => 'profileImage',
                                                    'name' => 'profileImage[]',
                                                    'class' => '',
                                                    'accept' => 'image/*',
                                                    // 'required' => 'required',
                                                    'placeholder' => 'Profile Image',
                                                    'value' => ($driver_model->get('profileImage')) ? driver_content_url($driver_model->get('id') . '/' .
                                                                    $driver_model->get('profileImage')) : ''
                                                ));
                                                ?>
                                        </span> <a
                                            href="javascript:void(0);" class="btn red fileinput-exists"
                                            data-dismiss="fileinput"> <i class="fa fa-trash-o"></i> </a>
                                        <div class="fileinput-new thumbnail"
                                             style="width: 60px; height: 60px;"> 



                                            <a href="<?php
                                            echo ($driver_model->get('profileImage')) ?
                                                    driver_content_url($driver_model->get('id') . '/' . $driver_model->get('profileImage')) : 
                                                image_url('app/user.png');
                                            ?>" data-imagelightbox="demo" data-ilb2-caption="profileImage">        
                                                <img src="<?php
                                                echo ($driver_model->get('profileImage')) ?
                                                        driver_content_url($driver_model->get('id') . '/' . $driver_model->get('profileImage')) :
                                                    image_url('app/user.png');
                                                ?>"  alt="">
                                            </a>
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail"
                                             style="max-width: 60px; max-height: 60px;"></div>
                                    </div>
                                <?php } else { ?>
                                    <?php //echo text($driver_model->get('profileImage')); ?>
                                <br>
                                    <div class="col-lg-3 chose-img"> 
                                       <a href="<?php  echo ($driver_model->get('profileImage')) ?
                                            driver_content_url($driver_model->get('id') . '/' . $driver_model->get('profileImage')) :
                                                image_url('app/user.png');
                                            ?>" data-imagelightbox="demo" data-ilb2-caption="Profile Image">
                                        <img src="<?php echo ($driver_model->get('profileImage')) ? 
                                        driver_content_url($driver_model->get('id') . '/' . $driver_model->get('profileImage')) : image_url('app/user.png');
                                        ?>">
                                       </a>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-md-3 input_field_sections nowarp"> 
                            <!-- <div class="col-lg-9">-->

                            <div class="fileinput fileinput-new" data-provides="fileinput"
                                 style="width: 100%">
                                <label>License Image:</label>
                                <?php if ($view_mode == EDIT_MODE) { ?>
                                    <div> <span class="btn default btn-file" style="width: 52%;"> <span
                                                class="fileinput-new"> Select image </span> <span
                                                class="fileinput-exists"> Change </span>
                                                <?php
                                                echo form_upload(array(
                                                    'id' => 'licenseImage',
                                                    'name' => 'licenseImage[]',
                                                    'class' => 'form-control-file',
                                                    // 'required' => 'required',
                                                    'accept' => 'image/*',
                                                    'placeholder' => 'License Image',
                                                    'value' => ($driver_model->get('licenseImage')) ? driver_content_url($driver_model->get('id') . '/' .
                                                            $driver_model->get('licenseImage')) : ''
                                                ));
                                                ?>
                                        </span> <a
                                            href="javascript:;" class="btn red fileinput-exists"
                                            data-dismiss="fileinput"> <i class="fa fa-trash-o"></i> </a>
                                        <div class="fileinput-new thumbnail"
                                             style="width: 60px; height: 60px;"> 
                                            <a href="<?php
                                            echo ($driver_model->get('licenseImage')) ?
                                            driver_content_url($driver_model->get('id') . '/' . $driver_model->get('licenseImage')) : 
                                                image_url('app/no_image.png');
                                            ?>" data-imagelightbox="demo" data-ilb2-caption="LicenseImage">
                                                <img src="<?php
                                                echo ($driver_model->get('licenseImage')) ?
                                                        driver_content_url($driver_model->get('id') . '/' . $driver_model->get('licenseImage')) :
                                                        image_url('app/no_image.png');
                                                ?>"
                                                     alt=""> 
                                            </a>
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail"
                                             style="max-width: 60px; max-height: 60px;"></div>
                                    </div>
                            <?php } else { ?>
                                <?php //echo text($driver_model->get('licenseImage')); ?><br>
                                <div class="col-lg-3 chose-img"> 
                                    <a href="<?php  echo ($driver_model->get('licenseImage')) ?
                                            driver_content_url($driver_model->get('id') . '/' . $driver_model->get('licenseImage')) :
                                                image_url('app/no_image.png');
                                            ?>" data-imagelightbox="demo" data-ilb2-caption="License Image">
                                    <img src="<?php echo ($driver_model->get('licenseImage')) ? driver_content_url($driver_model->get('id') . '/' .
                                            $driver_model->get('licenseImage')) : image_url('app/no_image.png'); ?>"  alt=""> 
                                    </a>
                                </div>
                            <?php } ?>
                        </div>
                       </div>


                    </div>
                    <!--                </div>    -->

                    <div class="row">
                        <div class="col-lg-12 m-t-35">
                            <div class="card-header bg-white">
                                <h1>Driver Personal Details</h1>
                            </div>

                            <div class="row">
                                
                                <div class="col-md-3 input_field_sections nowarp">
                                    <?php
                                    echo form_label('Gender:', 'gender', array(
                                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                                    ));
                                    if ($view_mode == EDIT_MODE) {
                                        if (is_array($gender_list) && count($gender_list)) {
                                            echo '<div class="mt-radio-inline">';
                                            foreach ($gender_list as $list) {
                                                echo '<label class="mt-radio" style="padding-right:10px;">';
                                                $is_checked = ($driver_personal_model->get ( 'gender' )==$list->description) ? TRUE : FALSE;
                                                $checked_value =$list->description;
                                              
                                                	echo form_radio ( array (
                                                			'id' => 'gender' . $list->description,
                                                			'name' => 'gender',
                                                			'class' => 'flat',
                                                			'required' => 'required'
                                                	), $checked_value, $is_checked );
                                               
                                                
                                                echo '<span></span>' . $list->description . '</label>';
                                            }
                                            echo '</div>';
                                        }
                                    } else {
                                        echo text($driver_personal_model->get('gender'));
                                    }
                                    ?>
                                </div>
                                <div class="col-md-3 input_field_sections nowarp">
                                    <?php
                                    echo form_label('DOB:', 'dob', array(
                                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                                    ));

                                    if ($view_mode == EDIT_MODE) {
                                        echo form_input(array(
                                            'id' => 'dob',
                                            'name' => 'dob',
                                            'class' => 'form-control',
                                            'required' => 'required',
                                            'placeholder' => 'DOB',
                                            'value' => ($driver_personal_model->get('dob')) ? $driver_personal_model->get('dob') : ''
                                        ));
                                    } else {
                                        echo text($driver_personal_model->get('dob'));
                                    }
                                    ?>
                                </div>
                                <div class="col-md-3 input_field_sections nowarp">
                                    <?php
                                    echo form_label('Address:', 'address', array(
                                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                                    ));

                                    if ($view_mode == EDIT_MODE) {
                                        echo form_textarea(array(
                                            'id' => 'address',
                                            'name' => 'address',
                                            'rows' => '2',
                                            'class' => 'form-control',
                                            'required' => 'required',
                                            'value' => ($driver_personal_model->get('address')) ? $driver_personal_model->get('address') : ''
                                        ));
                                    } else {
                                        echo form_textarea(array(
                                            'id' => 'address',
                                            'name' => 'address',
                                            'class' => 'form-control',
                                            'rows' => '2',
                                            'readonly' => 'readonly',
                                            'value' => ($driver_personal_model->get('address')) ? $driver_personal_model->get('address') : ''
                                        ));
                                    }
                                    ?>
                                </div>
                                <div class="col-md-3 input_field_sections nowarp">
                                    <?php
                                    echo form_label('Qualification:', 'qualification', array(
                                        'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                                    ));
                                    // validation for passenger first name
                                    if ($view_mode == EDIT_MODE) {
                                        echo form_input(array(
                                            'id' => 'qualification',
                                            'name' => 'qualification',
                                            'class' => 'form-control',
                                            // 'required' => 'required',
                                            // 'pattern' => '[a-zA-Z.]{2,20}',
                                            'placeholder' => 'Qualification',
                                            'value' => ($driver_personal_model->get('qualification')) ? $driver_personal_model->get('qualification') : ''
                                        ));
                                    } else {
                                        echo text($driver_personal_model->get('qualification'));
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 input_field_sections nowarp">
                                    <?php
                                    echo form_label('Nation Registration No:', 'nationRegistrationNo', array(
                                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                                    ));

                                    if ($view_mode == EDIT_MODE) {
                                        echo form_input(array(
                                            'id' => 'nationRegistrationNo',
                                            'name' => 'nationRegistrationNo',
                                            'class' => 'form-control',
                                            'accept' => 'image/*',
                                            'required' => 'required',
                                            // 'pattern' => '[A-Za-z0-9]{7,15}',
                                            'placeholder' => 'Nation Registration No',
                                            'value' => ($driver_personal_model->get('nationRegistrationNo')) ? $driver_personal_model->get('nationRegistrationNo') : ''
                                        ));
                                    } else {
                                        echo text($driver_personal_model->get('nationRegistrationNo'));
                                    }
                                    ?>
                                </div>
                                <div class="col-md-3 input_field_sections nowarp">
                                    <div class="fileinput fileinput-new" data-provides="fileinput"
                                         style="width: 100%">
                                        <label>Nation Registration Image:</label>
                                        <?php if ($view_mode == EDIT_MODE) { ?>
                                            <div> <span class="btn default btn-file" style="width: 52%;"> <span
                                                        class="fileinput-new"> Select image </span> <span
                                                        class="fileinput-exists"> Change </span>
                                                        <?php
                                                        echo form_upload(array(
                                                            'id' => 'nationRegistrationImage',
                                                            'name' => 'nationRegistrationImage[]',
                                                            'accept' => 'image/*',
                                                            // 'required' => 'required',
                                                            'placeholder' => 'Address Proof Image',
                                                            'value' => ($driver_personal_model->get('nationRegistrationImage')) ?
                                                                    driver_content_url($driver_model->get('id') . '/' .
                                                                            $driver_personal_model->get('nationRegistrationImage')) : ''
                                                        ));
                                                        ?>
                                                </span> <a
                                                    href="javascript:void(0);" class="btn red fileinput-exists"
                                                    data-dismiss="fileinput"> <i class="fa fa-trash-o"></i> </a>
                                                <div class="fileinput-new thumbnail"      style="width: 60px; height: 60px;"> 
                                                    <a href="<?php
                                                    echo ($driver_personal_model->get('nationRegistrationImage')) ?
                                                            driver_content_url($driver_model->get('id') . '/' .
                                                                    $driver_personal_model->get('nationRegistrationImage')) : image_url('app/no_image.png');
                                                    ?>" data-imagelightbox="demo" data-ilb2-caption="Nation Registration Image">
                                                        <img src="<?php
                                                        echo ($driver_personal_model->get('nationRegistrationImage')) ?
                                                                driver_content_url($driver_model->get('id') . '/' .
                                                                        $driver_personal_model->get('nationRegistrationImage')) :
                                                                image_url('app/no_image.png');
                                                        ?>"  alt=""> 
                                                    </a>

                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail"
                                                     style="max-width: 60px; max-height: 60px;"></div>
                                            </div>
                                        <?php } else { ?>
                                            <?php //echo text($driver_personal_model->get('nationRegistrationImage')); ?><br>
                                            <div class="col-lg-3 chose-img"> 
                                                <a href="<?php echo ($driver_personal_model->get('nationRegistrationImage')) ?
                                                            driver_content_url($driver_model->get('id') . '/' .
                                                                    $driver_personal_model->get('nationRegistrationImage')) : image_url('app/no_image.png');
                                                    ?>" data-imagelightbox="demo" data-ilb2-caption="Nation Registration Image">
                                                <img  src="<?php   echo ($driver_personal_model->get('nationRegistrationImage')) ?
                                                        driver_content_url($driver_model->get('id') . '/' .
                                                       $driver_personal_model->get('nationRegistrationImage')) : image_url('app/no_image.png');  ?>"> 
                                                </a>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="col-md-3 input_field_sections nowarp">
                                    <div class="fileinput fileinput-new" data-provides="fileinput"
                                         style="width: 100%">
                                        <label>Home Recommendation Image:</label>
                            <?php if ($view_mode == EDIT_MODE) { ?>
                                            <div> <span class="btn default btn-file" style="width: 52%;"> <span
                                                        class="fileinput-new"> Select image </span> <span
                                                        class="fileinput-exists"> Change </span>
                                                        <?php
                                                        echo form_upload(array(
                                                            'id' => 'homeRecommendationImage',
                                                            'name' => 'homeRecommendationImage[]',
                                                            'class' => '',
                                                            'accept' => 'image/*',
                                                            // 'required' => 'required',
                                                            'placeholder' => 'Identity Proof Image',
                                                            'value' => ($driver_personal_model->get('homeRecommendationImage')) ? 
                                                            driver_content_url($driver_model->get('id') . '/' . 
                                                                    $driver_personal_model->get('homeRecommendationImage')) : ''
                                                        ));
                                                        ?>
                                                </span> <a
                                                    href="javascript:;" class="btn red fileinput-exists"
                                                    data-dismiss="fileinput"> <i class="fa fa-trash-o"></i> </a>
                                                <div class="fileinput-new thumbnail"
                                                     style="width: 60px; height: 60px;">
                                                    <a href="<?php
                                                       echo ($driver_personal_model->get('homeRecommendationImage')) ?
                                                               driver_content_url($driver_model->get('id') . '/' . 
                                                                       $driver_personal_model->get('homeRecommendationImage')) : image_url('app/no_image.png');
                                                       ?>" data-imagelightbox="demo" data-ilb2-caption="Home Recommendation Image">
                                                        <img src="<?php
                                                        echo ($driver_personal_model->get('homeRecommendationImage')) ?
                                                                driver_content_url($driver_model->get('id') . '/' .
                                                                        $driver_personal_model->get('homeRecommendationImage')) : image_url('app/no_image.png');
                                                        ?>" alt="">
                                                    </a>
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail"
                                                     style="max-width: 60px; max-height: 60px;"></div>
                                            </div>
                                                    <?php } else { ?>
                                                        <?php //echo text($driver_personal_model->get('homeRecommendation')); ?> <br>
                                                        <div class="col-lg-3 chose-img"> 
                                                            <a href="<?php
                                                       echo ($driver_personal_model->get('homeRecommendationImage')) ?
                                                               driver_content_url($driver_model->get('id') . '/' . 
                                                                       $driver_personal_model->get('homeRecommendationImage')) : image_url('app/no_image.png');
                                                       ?>" data-imagelightbox="demo" data-ilb2-caption="Home Recommendation Image">
                                                            <img src="<?php echo ($driver_personal_model->get('homeRecommendationImage')) ?
                                                                        driver_content_url($driver_model->get('id') . '/' . 
                                                                        $driver_personal_model->get('homeRecommendationImage')) : image_url('app/no_image.png');
                                                            ?>"> 
                                                            </a>
                                                        </div>
                                                    <?php } ?>
                                    </div>
                                </div>
                                <div class="col-md-3 input_field_sections nowarp">
                                    <div class="fileinput fileinput-new" data-provides="fileinput"
                                         style="width: 100%">
                                        <label>Family Member List Image:</label>
                                    <?php if ($view_mode == EDIT_MODE) { ?>
                                            <div> <span class="btn default btn-file" style="width: 52%;"> <span
                                                        class="fileinput-new"> Select image </span> <span
                                                        class="fileinput-exists"> Change </span>
                                                        <?php
                                                        echo form_upload(array(
                                                            'id' => 'familyMemberListImage',
                                                            'name' => 'familyMemberListImage[]',
                                                            'class' => '',
                                                            'accept' => 'image/*',
                                                            // 'required' => 'required',
                                                            'placeholder' => 'Identity Proof Image',
                                                            'value' => ($driver_personal_model->get('familyMemberListImage')) ?
                                                            driver_content_url($driver_model->get('id') . '/' . 
                                                                    $driver_personal_model->get('familyMemberListImage')) : ''
                                                        ));
                                                        ?>
                                                </span> <a
                                                    href="javascript:;" class="btn red fileinput-exists"
                                                    data-dismiss="fileinput"> <i class="fa fa-trash-o"></i> </a>
                                                <div class="fileinput-new thumbnail"
                                                     style="width: 60px; height: 60px;"> 
                                                    <a href="<?php
                                                       echo ($driver_personal_model->get('familyMemberListImage')) ?
                                                               driver_content_url($driver_model->get('id') . '/' . 
                                                            $driver_personal_model->get('familyMemberListImage')) : image_url('app/no_image.png');
                                                       ?>" data-imagelightbox="demo" data-ilb2-caption="Family MemberList Image">
                                                        <img src="<?php
                                                        echo ($driver_personal_model->get('familyMemberListImage')) ?
                                                                driver_content_url($driver_model->get('id') . '/' .
                                                                        $driver_personal_model->get('familyMemberListImage')) : image_url('app/no_image.png');
                                                        ?>" alt=""> 
                                                    </a>
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail"
                                                     style="max-width: 60px; max-height: 60px;"></div>
                                            </div>
                                    <?php } else { ?>
                                        <?php //echo text($driver_personal_model->get('familyMemberListImage')); ?> <br>
                                    <div class="col-lg-3 chose-img">
                                        <a href="<?php echo ($driver_personal_model->get('familyMemberListImage')) ?
                                            driver_content_url($driver_model->get('id') . '/' . $driver_personal_model->get('familyMemberListImage')) : 
                                            image_url('app/no_image.png');
                                                       ?>" data-imagelightbox="demo" data-ilb2-caption="Family MemberList Image">
                                        <img src="<?php  echo ($driver_personal_model->get('familyMemberListImage')) ? 
                                            driver_content_url($driver_model->get('id') . '/' . $driver_personal_model->get('familyMemberListImage')) : 
                                            image_url('app/no_image.png'); ?>"> 
                                        </a>
                                    </div>
                                    <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 input_field_sections nowarp">
                                    <div class="fileinput fileinput-new" data-provides="fileinput"
                                         style="width: 100%">
                                        <label>Contract Paper Image:</label>
                                        <?php if ($view_mode == EDIT_MODE) { ?>
                                            <div> <span class="btn default btn-file" style="width: 52%;"> <span
                                                        class="fileinput-new"> Select image </span> <span
                                                        class="fileinput-exists"> Change </span>
                                                        <?php
                                                        echo form_upload(array(
                                                            'id' => 'contractPaperImage',
                                                            'name' => 'contractPaperImage[]',
                                                            'class' => 'form-control',
                                                            'accept' => 'image/*',
                                                            // 'required' => 'required',
                                                            // 'placeholder' => 'PAN Card Image',
                                                            'value' => ($driver_personal_model->get('contractPaperImage')) ? 
                                                            driver_content_url($driver_model->get('id') . '/' . 
                                                            $driver_personal_model->get('contractPaperImage')) : ''
                                                        ));
                                                        ?>
                                                </span> <a
                                                    href="javascript:;" class="btn red fileinput-exists"
                                                    data-dismiss="fileinput"> <i class="fa fa-trash-o"></i> </a>
                                                <div class="fileinput-new thumbnail" style="width: 60px; height: 60px;"> 
                                                    <a href="<?php echo ($driver_personal_model->get('contractPaperImage')) ?
                                                               driver_content_url($driver_model->get('id') . '/' . 
                                                               $driver_personal_model->get('contractPaperImage')) : image_url('app/no_image.png');
                                                       ?>" data-imagelightbox="demo" data-ilb2-caption="Contract Paper Image">
                                                        <img src="<?php
                                                             echo ($driver_personal_model->get('contractPaperImage')) ?
                                                                     driver_content_url($driver_model->get('id') . '/' . 
                                                                    $driver_personal_model->get('contractPaperImage')) : image_url('app/no_image.png');
                                                             ?>"  alt=""> 
                                                    </a>
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail"
                                                     style="max-width: 60px; max-height: 60px;"></div>
                                            </div>
                                        <?php } else { ?>
                                            <?php //echo text($driver_personal_model->get('contractPaperImage')); ?> <br>
                                                <div class="col-lg-3 chose-img">
                                                    <a href="<?php echo ($driver_personal_model->get('contractPaperImage')) ?
                                                               driver_content_url($driver_model->get('id') . '/' . 
                                                               $driver_personal_model->get('contractPaperImage')) : image_url('app/no_image.png');
                                                       ?>" data-imagelightbox="demo" data-ilb2-caption="Contract Paper Image">
                                                    <img src="<?php  echo ($driver_personal_model->get('contractPaperImage')) ? 
                                                    driver_content_url($driver_model->get('id') . '/' . $driver_personal_model->get('contractPaperImage')) :
                                                        image_url('app/no_image.png');
                                                    ?>"> 
                                                    </a>
                                                </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="col-md-3 input_field_sections nowarp">
                                    <?php
                                    echo form_label('Bank Name:', 'bankName', array(
                                        'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                                    ));

                                    if ($view_mode == EDIT_MODE) {
                                        echo form_input(array(
                                            'id' => 'bankName',
                                            'name' => 'bankName',
                                            'class' => 'form-control',
                                            // 'required' => 'required',
                                            // 'pattern' => '[a-zA-Z\s]{2,30}',
                                            'placeholder' => 'Bank Name',
                                            'value' => ($driver_personal_model->get('bankName')) ? $driver_personal_model->get('bankName') : ''
                                        ));
                                    } else {
                                        echo text($driver_personal_model->get('bankName'));
                                    }
                                    ?>
                                </div>
                                <div class="col-md-3 input_field_sections nowarp">
                                    <?php
                                    echo form_label('Bank Branch:', 'bankBranch', array(
                                        'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                                    ));

                                    if ($view_mode == EDIT_MODE) {
                                        echo form_input(array(
                                            'id' => 'bankBranch',
                                            'name' => 'bankBranch',
                                            'class' => 'form-control',
                                            // 'required' => 'required',
                                            // 'pattern' => '[a-zA-Z\s]{2,30}',
                                            'placeholder' => 'Bank Name',
                                            'value' => ($driver_personal_model->get('bankBranch')) ? $driver_personal_model->get('bankBranch') : ''
                                        ));
                                    } else {
                                        echo text($driver_personal_model->get('bankBranch'));
                                    }
                                    ?>
                                </div>
                                <div class="col-md-3 input_field_sections nowarp">
                                    <?php
                                    echo form_label('Bank Account No:', 'bankAccountNo', array(
                                        'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                                    ));

                                    if ($view_mode == EDIT_MODE) {
                                        echo form_input(array(
                                            'id' => 'bankAccountNo',
                                            'name' => 'bankAccountNo',
                                            'class' => 'form-control',
                                          //  'required' => 'required',
                                           // 'pattern' => '\d{10,16}',
                                        	//'maxlength'=>'16',
                                            'placeholder' => 'Bank Account No',
                                            'value' => ($driver_personal_model->get('bankAccountNo')) ? $driver_personal_model->get('bankAccountNo') : ''
                                        ));
                                    } else {
                                        echo text($driver_personal_model->get('bankAccountNo'));
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 input_field_sections nowarp">
                                    <?php
                                    echo form_label('Security Deposit:', 'securityDeposit', array(
                                        'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                                    ));

                                    if ($view_mode == EDIT_MODE) {
                                        echo form_input(array(
                                            'id' => 'securityDeposit',
                                            'name' => 'securityDeposit',
                                            'class' => 'form-control',
                                        	'readonly'=>'readonly',
                                            //'required' => 'required',
                                           // 'maxlength' => 7,
                                           // 'pattern' => '[0-9]+([\.][0-9]+)?',
                                            'placeholder' => 'Security Deposit a/m',
                                            'value' => ($driver_personal_model->get('securityDeposit')) ? $driver_personal_model->get('securityDeposit') : $entity_config_model->get('driverSecurityAmount')
                                        ));
                                    } else {
                                        echo text($driver_personal_model->get('securityDeposit'));
                                    }
                                    ?>
                                </div>
                                <?php if ($view_mode == EDIT_MODE && $driver_personal_model->get('groupType')): ?>
                                <div class="col-md-3 input_field_sections nowarp">
                                    <?php
                                    echo form_label('Group Name:', 'groupType', array(
                                        'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                                    ));

                                    if ($view_mode == EDIT_MODE) {
                                        echo form_input(array(
                                            'id' => 'groupType',
                                            'name' => 'groupType',
                                            'class' => 'form-control',
                                            'readonly' => 'readonly',
                                            'placeholder' => 'Group Name',
                                            'value' => ($driver_personal_model->get('groupType')) ? $driver_personal_model->get('groupType') : ''
                                        ));
                                    } else {
                                        echo text($driver_personal_model->get('groupType'));
                                    }
                                    ?>
                                </div>
                                <?php endif;?>
                                <div class="col-md-3 input_field_sections nowarp">
                                    <?php
                                    echo form_label('Behaviour Type:', 'behaviourType', array(
                                        'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                                    ));

                                    if ($view_mode == EDIT_MODE) {
                                        echo form_textarea(array(
                                            'id' => 'behaviourType',
                                            'name' => 'behaviourType',
                                            'rows' => '2',
                                            'class' => 'form-control',
                                            //'required' => 'required',
                                            'value' => ($driver_personal_model->get('behaviourType')) ? $driver_personal_model->get('behaviourType') : ''
                                        ));
                                    } else {
                                        echo form_textarea(array(
                                            'id' => 'behaviourType',
                                            'name' => 'behaviourType',
                                            'class' => 'form-control',
                                            'rows' => '2',
                                            'readonly' => 'readonly',
                                            'value' => ($driver_personal_model->get('behaviourType')) ? $driver_personal_model->get('behaviourType') : ''
                                        ));
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="form-actions" style="margin-top: 22px;">
                                <div class="row">
                                    <div class="col-md-12">
<?php if ($view_mode == EDIT_MODE) { ?>
                                            <div class="gp-cen">
                                                <button type="button" id="cancel-driver-btn"
                                                        class="btn grey-salsa btn-outline">Cancel</button>
                                                <button type="button" id="save-driver-btn" class="btn gpblue">Save</button>
                                            </div>
<?php } else { ?>
                                            <div class="gp-cen">
                                                <button type="button"
                                                        id="cancel-driver-btn" class="btn btn-danger">Back</button>
                                            </div>
<?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    echo form_close();
    ?>
    <!-- END EXAMPLE TABLE PORTLET--> 
</div>
