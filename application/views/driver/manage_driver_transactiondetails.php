<style>
    .label.label-sm {
    font-size: 13px;
    padding: 2px 5px;
    cursor:pointer;
}
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">

        <div class="page-bar ">
            <ul class="page-breadcrumb ">
                <li>
                    <a href="<?php echo base_url('Dashboard');?>">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="<?php echo base_url('Driver/getDriverList');?>">Driver List</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="<?php echo base_url(uri_string());?>">Transaction List</a>

                </li>

            </ul>

        </div>
        <!---- Page Bar ends ---->
        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-comments"></i><?php echo $driver_fullname ;?> </div>
                                       
                                    </div>
                                  
                                    <div class="portlet-body">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="col-md-3 btn-group gp-pad1">
                                                        <a href="<?php echo base_url('driver/addDriverTransaction/'.$this->uri->segment(3));?>">
                                                        <button id="sample_editable_1_new" class="btn sbold green1"> Add New
                                                            <i class="fa fa-plus"></i>
                                                        </button>
                                                            </a>
                                                    </div>
                                                    
                                                    <!--<div class="col-md-9 m-b-10">
                                                <button type="button" id="toggle-table-columns"
                                                        class="btn btn-primary">Show Columns List
                                                </button>
                                        </div>-->
                                                </div>
                                                <!--<div class="col-md-6">
                                                    <div class="btn-group pull-right">
                                                        <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Export
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a href="#">
                                                                    <i class="fa fa-print"></i> Print </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>-->
                                            </div>
                                        </div>
                                        
                        
                                     
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="driver-transaction-data-table" style="width:99%">
                                            <thead>
                                                <tr>
                                            <th>Action</th>
                                            <th>TripId</th>
                                            <th>Transaction Amount</th>
                                            <th>Previous Amount</th>
                                            <th>Current Amount</th>
                                            <th>Transaction Status</th>
                                            <th>Transaction From</th>
                                            <th>Transaction Type</th>
                                            <th>Transaction Mode</th>
                                            <th>Transaction Datetime</th>
                                            <th>Transaction Id</th>
                                            <th>Comments</th>
                                            
                                        </tr>
                                            </thead>
                                            <tbody>
                                                   
                                            </tbody>
                                        </table>
                                            </div>
                                        </div>
                                                
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>

<!------------------ Allocate Taxi Modal Starts ----------------------------->
<div class="modal fade" id="allocate-taxi-device" tabindex="-1"
     role="dialog" aria-labelledby="modal-login-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h3 class="modal-title" id="modal-login-label"
                    style="text-align: center">Allocate Taxi/Device</h3>

            </div>

            <div class="modal-body">
<?php
$form_attr = array(
    'name' => 'edit_drive_allocate_form',
    'id' => 'edit_drive_allocate_form',
    'method' => 'POST'
);
echo form_open(base_url(''), $form_attr);
// driver id by default is -1

echo form_input(array(
    'type' => 'hidden',
    'id' => 'tempDriverId',
    'name' => 'tempDriverId',
    'value' => ''
));
?>
                <div class="form-group">
                    <label><strong>Driver Name</strong></label> <span
                        class="form-control" id="show-driver-name"></span>
                </div>
                <div class="form-group">

                <?php
                echo form_label('Taxi:', 'taxiId', array(
                    'class' => ''
                ));

                echo form_dropdown('taxiId', $taxi_list, '', array(
                    'id' => 'taxiId',
                    'class' => 'form-control',
                        //'required' => 'required' 
                ));
                ?>
                </div>
                <div class="form-group">
                <?php
                echo form_label('Taxi Device:', 'taxiDeviceId', array(
                    'class' => 'required'
                ));

                echo form_dropdown('taxiDeviceId', $taxi_device_list, '', array(
                    'id' => 'taxiDeviceId',
                    'class' => 'form-control',
                    'required' => 'required'
                ));
                ?>		


                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    <button type="button" id="save-allocate-taxi-btn"
                            class="btn btn-primary">Save</button>
                </div>
                    <?php
                    echo form_close();
                    ?>

            </div>

        </div>
    </div>
</div>

<!------------------ Allocate Driver Modal Ends   ----------------------------->

