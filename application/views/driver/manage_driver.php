<style>
    .label.label-sm {
    font-size: 13px;
    padding: 2px 5px;
    cursor:pointer;
}
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">

        <div class="page-bar ">
            <ul class="page-breadcrumb ">
                <li>
                    <a href="<?php echo base_url('Dashboard');?>">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="<?php echo base_url('Driver/getDriverList');?>">Driver List</a>

                </li>

            </ul>

        </div>
        <!---- Page Bar ends ---->
        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                <div class="portlet-title">
                                        <div class="caption">
                                           <i class="fa fa-list" aria-hidden="true"></i>
Driver List </div>
                                       
                                    </div>
                                 
                                    <div class="portlet-body">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="col-md-3 btn-group gp-pad1">
                                                        <a href="<?php echo base_url('Driver/add');?>">
                                                        <button id="sample_editable_1_new" class="btn sbold green1"> Add New
                                                            <i class="fa fa-plus"></i>
                                                        </button>
                                                            </a>
                                                    </div>
                                                    
                                                    <!--<div class="col-md-9 m-b-10">
                                                <button type="button" id="toggle-table-columns"
                                                        class="btn btn-primary">Show Columns List
                                                </button>
                                        </div>-->
                                                </div>
                                                <!--<div class="col-md-6">
                                                    <div class="btn-group pull-right">
                                                        <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Export
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a href="#">
                                                                    <i class="fa fa-print"></i> Print </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>-->
                                            </div>
                                        </div>
                                        
                        <div id="table-columns" class="hidden">

                                    <label class="">
                                            <?php
                                            echo form_checkbox(array(
                                                'class' => 'flat toggle-vis',
                                                'data-column' => '11'
                                            ));
                                            ?>
                                        Experience
                                    </label> 
                                    <label class="">
                                        <?php
                                        echo form_checkbox(array(
                                            'class' => 'flat toggle-vis',
                                            'data-column' => '12',
                                        ));
                                        ?>
                                        Langauage
                                    </label> 
                                    <label class="">
                                        <?php
                                        echo form_checkbox(array(
                                            'class' => 'flat toggle-vis',
                                            'data-column' => '13',
                                        ));
                                        ?>
                                        Qualification
                                    </label>
                                    <label class="">
                                               <?php
                                               echo form_checkbox(array(
                                                   'class' => 'flat toggle-vis',
                                                   'data-column' => '14',
                                               ));
                                               ?>
                                               Email
                                    </label>
                                    <label class="">
                                        <?php
                                        echo form_checkbox(array(
                                            'class' => 'flat toggle-vis',
                                            'data-column' => '15',
                                        ));
                                        ?>
                                        License No
                                    </label>
                                    <label class="">
                                        <?php
                                        echo form_checkbox(array(
                                            'class' => 'flat toggle-vis',
                                            'data-column' => '16',
                                        ));
                                        ?>
                                        Licencse Expiry
                                    </label>
                                    <label class="">
                                        <?php
                                        echo form_checkbox(array(
                                            'class' => 'flat toggle-vis',
                                            'data-column' => '17',
                                        ));
                                        ?>
                                        Licencse Type
                                    </label> 
                                    <label class="">
                                        <?php
                                        echo form_checkbox(array(
                                            'class' => 'flat toggle-vis',
                                            'data-column' => '18',
                                        ));
                                        ?>
                                        Address
                                    </label>
                            <label class="">
                                        <?php
                                        echo form_checkbox(array(
                                            'class' => 'flat toggle-vis',
                                            'data-column' => '19'
                                        ));
                                        ?>
                                        DOB
                                    </label>
                                    <label class="">
                                        <?php
                                        echo form_checkbox(array(
                                            'class' => 'flat toggle-vis',
                                            'data-column' => '20',
                                        ));
                                        ?>
                                        Gender
                                    </label>
                                  <label class="">
                                        <?php
                                        echo form_checkbox(array(
                                            'class' => 'flat toggle-vis',
                                            'data-column' => '21',
                                        ));
                                        ?>
                                        Bank Name
                                    </label>
                                    <label class="">
                                        <?php
                                        echo form_checkbox(array(
                                            'class' => 'flat toggle-vis',
                                            'data-column' => '22',
                                        ));
                                        ?>
                                        Bank Branch
                                    </label>
                               
                                <label class="">
                                        <?php
                                        echo form_checkbox(array(
                                            'class' => 'flat toggle-vis',
                                            'data-column' => '23',
                                        ));
                                        ?>
                                        Account No
                                    </label>
                                    <label class="">
                                        <?php
                                        echo form_checkbox(array(
                                            'class' => 'flat toggle-vis',
                                            'data-column' => '24',
                                        ));
                                        ?>
                                        Group
                                    </label>
                            <label class="">
                                        <?php
                                        echo form_checkbox(array(
                                            'class' => 'flat toggle-vis',
                                            'data-column' => '25',
                                        ));
                                        ?>
                                        Behaviour
                                    </label>
                            <label class="">
                                        <?php
                                        echo form_checkbox(array(
                                            'class' => 'flat toggle-vis',
                                            'data-column' => '26',
                                        ));
                                        ?>
                                        Zone
                                    </label>
                            <label class="">
                                        <?php
                                        echo form_checkbox(array(
                                            'class' => 'flat toggle-vis',
                                            'data-column' => '27',
                                        ));
                                        ?>
                                        Last Logged In
                                    </label>
                          <!--   <label class=""> -->
                                        <?php
                                       /*  echo form_checkbox(array(
                                            'class' => 'flat toggle-vis',
                                            'data-column' => '24',
                                        )); */
                                        ?>
                                       <!--  Login Status
                                    </label> -->
                           <!--  <label class=""> -->
                                        <?php
                                       /*  echo form_checkbox(array(
                                            'class' => 'flat toggle-vis',
                                            'data-column' => '25',
                                        )); */
                                        ?>
                                      <!--   Verify Status
                                    </label>  -->
                            
                                </div>
                                     
                                        <table class="table table-striped table-bordered table-hover table-checkable order-column gp-font" id="driver-data-table" style="width:99%">
                                            <thead>
                                                <tr>
                                            <th>Action</th>
                                            <th>Image</th>
                                            <th>Driver Code</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Mobile</th>
                                            <th>Driver Wallet</th>
                                            <th>Driver Credit</th>
                                            <th>Taxi No</th>
                                            <th>Device Mobile</th>
                                            <th>Driver Rating</th>
                                            <th>Experience</th>
                                            <th>Language</th>
                                            <th>Qualification</th>
                                            <th>Email</th>
                                            <th>License No</th>
                                            <th>Licencse Expiry</th>
                                            <th>Licencse Type</th>
                                            <th>Address</th>
                                            <th>DOB</th>
                                            <th>Gender</th>
                                            <th>Bank Name</th>
                                            <th>Bank Branch</th>
                                            <th>Account No</th>
                                            <th>Group</th>
                                            <th>Behaviour</th>
                                            <th>Zone</th>
                                            <th>Last Logged In</th>
                                            <th>Login Status</th>
                                            <th>Verify Status</th>
                                            <th>App Version</th>
                                            <th>Joined Date</th>
                                            <th>Resiged Date</th>
                                            <th>Status</th>
                                            <th>Resign Status</th>
                                            <th>Allocated</th>
                                        </tr>
                                            </thead>
                                            <tbody>
                                                   
                                            </tbody>
                                        </table>
                                            </div>
                                        </div>
                                                
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>

<!------------------ Allocate Taxi Modal Starts ----------------------------->
<div class="modal fade" id="allocate-taxi-device" tabindex="-1"
     role="dialog" aria-labelledby="modal-login-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h3 class="modal-title" id="modal-login-label"
                    style="text-align: center">Allocate Taxi/Device</h3>

            </div>

            <div class="modal-body">
<?php
$form_attr = array(
    'name' => 'edit_drive_allocate_form',
    'id' => 'edit_drive_allocate_form',
    'method' => 'POST'
);
echo form_open(base_url(''), $form_attr);
// driver id by default is -1

echo form_input(array(
    'type' => 'hidden',
    'id' => 'tempDriverId',
    'name' => 'tempDriverId',
    'value' => ''
));
?>
                <div class="form-group">
                    <label><strong>Driver Name</strong></label> <span
                        class="form-control" id="show-driver-name"></span>
                </div>
                <div class="form-group mandatory-field">

                <?php
                echo form_label('Taxi:', 'taxiId', array(
                    'class' => 'required'
                ));

                echo form_dropdown('taxiId', $taxi_list, '', array(
                    'id' => 'taxiId',
                    'class' => 'form-control',
                    'required' => 'required' 
                ));
                ?>
                </div>
                <div class="form-group mandatory-field">
                <?php
                echo form_label('Taxi Device:', 'taxiDeviceId', array(
                    'class' => 'required'
					
                ));

                echo form_dropdown('taxiDeviceId', $taxi_device_list, '', array(
                    'id' => 'taxiDeviceId',
                    'class' => 'form-control',
                    'required' => 'required'
                ));
                ?>		


                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    <button type="button" id="save-allocate-taxi-btn"
                            class="btn btn-primary">Save</button>
                </div>
                    <?php
                    echo form_close();
                    ?>

            </div>

        </div>
    </div>
</div>

<!------------------ Allocate Driver Modal Ends   ----------------------------->

