<style>
    .label.label-sm {
    font-size: 13px;
    padding: 2px 5px;
    cursor:pointer;
}
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">

        <div class="page-bar ">
            <ul class="page-breadcrumb ">
                <li>
                    <a href="<?php echo base_url('Dashboard');?>">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="<?php echo base_url('Driver/getDriverList');?>">Driver List</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="<?php echo base_url(uri_string());?>">Log History List</a>

                </li>

            </ul>

        </div>
        <!---- Page Bar ends ---->
        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-comments"></i><?php echo $driver_fullname ;?> </div>
                                       
                                    </div>
                                  
                                    <div class="portlet-body">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                  
                                                </div>
                                                
                                            </div>
                                        </div>
                                        
                        
                                     
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="driver-shift-history-data-table" style="width:99%">
                                            <thead>
                                                <tr>
                              				<th>Sl no</th>
                                            <th>Current Shift Status</th>
                                            <th>Free Status Datetime</th>
                                            <th>Busy Status Datetime</th>
                                            <th>SMS Sent Count</th>
                                            <th>Last SMS Sent Datetime</th>
                                            
                                        </tr>
                                            </thead>
                                            <tbody>
                                                   
                                            </tbody>
                                        </table>
                                            </div>
                                        </div>
                                                
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>
