<?php
$view_mode = $mode;
?>
<!-- BEGIN CONTENT -->
<style>
.gp-row {
	margin-bottom: 10px;
}
</style>
<!-- BEGIN CONTENT BODY -->

<div class="page-content">
    <?php
				$form_attr = array (
						'name' => 'edit_taxiTariff_form',
						'id' => 'edit_taxiTariff_form',
						'method' => 'POST',
						'action' => 'welcome' 
				);
				
				echo form_open_multipart ( base_url ( '' ), $form_attr );
				// driver id by default is -1
				echo form_input ( array (
						'type' => 'hidden',
						'id' => 'taxiTariff_id',
						'name' => 'id',
						'value' => ($taxi_tariff_model->get ( 'id' )) ? $taxi_tariff_model->get ( 'id' ) : - 1 
				) );
				/* echo form_input ( array (
						'type' => 'hidden',
						'id' => 'entityId',
						'name' => 'entityId',
						'value' => DEFAULT_COMPANY_ID 
				) ); */
				?>
    <!--<form method="post" accept-charset="utf-8" action="http://dev.hellocabs.com/Taxitariff/save_tarif">-->
	<div class="page-bar ">
		<ul class="page-breadcrumb ">
			<li><a href="<?php echo base_url('Dashboard'); ?>">Home</a> <i
				class="fa fa-circle"></i></li>
			<li><a href="<?php echo base_url('TaxiTariff/getTaxiTariffList'); ?>">Tariff
					List</a> <i class="fa fa-circle"></i></li>
			<li><a href="<?php echo base_url(uri_string()); ?>"><?php echo ($taxi_tariff_model->get('id')) ? (($view_mode == VIEW_MODE) ? 'View' : 'Edit') : 'Create'; ?> Tariff</a></li>

		</ul>

	</div>
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN VALIDATION STATES-->


			<div class="portlet-body">
				<div class="form-body gp-form">
					<div class="card-header bg-white">
						<h1><?php echo ($taxi_tariff_model->get('id')) ? (($view_mode == VIEW_MODE) ? 'View' : 'Edit') : 'Create'; ?> Tariff</h1>
					</div>
					<div class="row">

						<div class="col-lg-3 input_field_sections nowarp">
                            <?php
																												echo form_label ( 'Name:', 'name', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																												) );
																												
																												if ($view_mode == EDIT_MODE) {
																													echo form_input ( array (
																															'id' => 'name',
																															'name' => 'name',
																															'class' => 'form-control',
																															'required' => 'required',
																															
																															// 'pattern' => '[a-zA-Z0-9\s]{3,15}',
																															'placeholder' => 'Name',
																															'value' => ($taxi_tariff_model->get ( 'name' )) ? $taxi_tariff_model->get ( 'name' ) : '' 
																													) );
																												} else {
																													echo text ( $taxi_tariff_model->get ( 'name' ) );
																												}
																												?>

                        </div>
						<div class="col-lg-3 input_field_sections nowarp">
                            <?php
																												echo form_label ( 'Day Base Charge:', 'dayConvenienceCharge', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																												) );
																												
																												if ($view_mode == EDIT_MODE) {
																													echo form_input ( array (
																															'id' => 'dayConvenienceCharge',
																															'name' => 'dayConvenienceCharge',
																															'class' => 'form-control',
																															'required' => 'required',
																															'pattern' => '^[1-9]\d{0,6}([\.]?(\d{1,2}))$',
																															'maxlength' => '10',
																															
																															// 'pattern' => '\d{1,6}',
																															'placeholder' => 'Day Base Charge',
																															'value' => ($taxi_tariff_model->get ( 'dayConvenienceCharge' )) ? $taxi_tariff_model->get ( 'dayConvenienceCharge' ) : '' 
																													) );
																												} else {
																													echo text ( $taxi_tariff_model->get ( 'dayConvenienceCharge' ) );
																												}
																												?>

                        </div>
						<div class="col-lg-3 input_field_sections nowarp">
                            <?php
																												echo form_label ( 'Night Base Charge:', 'nightConvenienceCharge', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																												) );
																												
																												if ($view_mode == EDIT_MODE) {
																													echo form_input ( array (
																															'id' => 'nightConvenienceCharge',
																															'name' => 'nightConvenienceCharge',
																															'class' => 'form-control',
																															'required' => 'required',
																															'pattern' => '^[1-9]\d{0,6}([\.]?(\d{1,2}))$',
																															'maxlength' => '10',
																															
																															// 'pattern' => '\d{1,6}',
																															'placeholder' => 'Night Base Charge',
																															'value' => ($taxi_tariff_model->get ( 'nightConvenienceCharge' )) ? $taxi_tariff_model->get ( 'nightConvenienceCharge' ) : '' 
																													) );
																												} else {
																													echo text ( $taxi_tariff_model->get ( 'nightConvenienceCharge' ) );
																												}
																												?>

                        </div>
						<div class="col-lg-3 input_field_sections nowarp">

<?php
echo form_label ( 'Taxi Category Type:', 'taxiCategoryType', array (
		'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
) );

if ($view_mode == EDIT_MODE) {
	echo form_dropdown ( 'taxiCategoryType', $taxi_list, $taxi_tariff_model->get ( 'taxiCategoryType' ), array (
			'id' => 'taxiCategoryType',
			'class' => 'form-control',
			'required' => 'required' 
	) );
} else {
	echo text ( $taxi_list [$taxi_tariff_model->get ( 'taxiCategoryType' )] );
}
?>


                        </div>


					</div>


					<div class="row">
						<div class="col-lg-3 input_field_sections nowarp">

<?php
echo form_label ( 'Zone:', 'zoneId', array (
		'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
) );

if ($view_mode == EDIT_MODE) {
	echo form_dropdown ( 'zoneId', $zone_list, $taxi_tariff_model->get ( 'zoneId' ), array (
			'id' => 'zoneId',
			'class' => 'form-control',
			//'required' => 'required' 
	) );
} else {
	echo text ( @$zone_list [$taxi_tariff_model->get ( 'zoneId' )] );
}
?>


                        </div>



						<div class="col-lg-3 input_field_sections nowarp">

<?php
echo form_label ( 'Entity:', 'entityId', array (
		'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
) );

if ($view_mode == EDIT_MODE) {
	echo form_dropdown ( 'entityId', $entity_list, ($taxi_tariff_model->get ( 'entityId' ))?$taxi_tariff_model->get ( 'entityId' ):'', array (
			'id' => 'entityId',
			'class' => 'form-control',
			//'required' => 'required' 
	) );
} else {
	echo text ( @$entity_list [$taxi_tariff_model->get ( 'entityId' )] );
}
?>


                        </div>
					</div>

				</div>
				<?php if ($taxi_tariff_distance_slab_model): ?>
				<div class="form-body gp-form">
					<div class="card-header bg-white">
						<h1>Distance Slab</h1>
					</div>
					<div class="row">



						<div id="distance-slab" class="section-container">
							<div class="section-message"></div>
							<table id="distance-slab-table" class="table-head"
								style="width: 100%;">
								<thead>
									<tr>
										<th class="col-md-3">Slab start</th>
										<th class="col-md-3">Slab end</th>
										<th class="col-md-3">Slab charge</th>
									<?php
					
if ($view_mode == EDIT_MODE) {
						
						echo '<th class="col-md-3">Action</th>';
					}
					?>
									
								</tr>
								</thead>
								<tbody>
									<?php if ($edit_distance_slab_model || ($view_mode == VIEW_MODE)){ $distance_slab_count=count ($taxi_tariff_distance_slab_model);$i=0; foreach ($taxi_tariff_distance_slab_model as $distance_slab){ ?>
									<tr class="gp-mg ">

										<td class="col-md-3 gp-row">
                                            <?php
							echo form_input ( array (
									'type' => 'hidden',
									'name' => 'result[' . $i . '][key]',
									'class' => 'slab-count' 
							) );
							if ($view_mode == EDIT_MODE) {
								echo form_input ( array (
										'type' => 'hidden',
										'id' => 'distance-slabId-' . $i,
										'name' => 'distanceSlab[' . $i . '][slabId]',
										'value' => ($distance_slab->get ( 'id' )) ? $distance_slab->get ( 'id' ) : - 1 
								) );
								echo form_input ( array (
										'type' => 'hidden',
										'id' => 'distance-slabType-' . $i,
										'name' => 'distanceSlab[' . $i . '][slabType]',
										'value' => Slab_Type_Enum::DISTANCE 
								) );
								echo form_input ( array (
										'type' => 'hidden',
										'id' => 'distance-slabChargeType-' . $i,
										'name' => 'distanceSlab[' . $i . '][slabChargeType]',
										'value' => Payment_Type_Enum::AMOUNT 
								) );
								echo form_input ( array (
										'id' => 'distance-slabStart-' . $i,
										'name' => 'distanceSlab[' . $i . '][slabStart]',
										'class' => 'form-control',
										'required' => 'required',
										'pattern' => '\d{1,2}',
										'placeholder' => 'Start Slab',
										'value' => ($distance_slab->get ( 'slabStart' )) ? $distance_slab->get ( 'slabStart' ) : 0,
										'readonly' => 'true' 
								) );
							} else {
								echo text ( $distance_slab->get ( 'slabStart' ) );
							}
							?></td>
										<td class="col-md-3"><?php
							if ($view_mode == EDIT_MODE) {
								if (($distance_slab_count - 1) == $i)
								{
								echo form_input ( array (
										'id' => 'distance-slabEnd-' . $i,
										'name' => "distanceSlab[" . $i . "][slabEnd]",
										'class' => 'form-control',
										'required' => 'required',
										'pattern' => '\d{1,2}',
										'placeholder' => 'End Slab',
										'value' => ($distance_slab->get ( 'slabEnd' )) ? $distance_slab->get ( 'slabEnd' ) : 0 
								) );
								}
								else 
								{
									echo form_input ( array (
											'id' => 'distance-slabEnd-' . $i,
											'name' => "distanceSlab[" . $i . "][slabEnd]",
											'class' => 'form-control',
											'required' => 'required',
											'readonly' => 'readonly',
											'pattern' => '\d{1,2}',
											'placeholder' => 'End Slab',
											'value' => ($distance_slab->get ( 'slabEnd' )) ? $distance_slab->get ( 'slabEnd' ) : 0
									) );
								}
							} else {
								echo text ( $distance_slab->get ( 'slabEnd' ) );
							}
							?></td>

										<td class="col-md-3"><?php
							if ($view_mode == EDIT_MODE) {
								echo form_input ( array (
										'id' => 'distance-slabCharge-' . $i,
										'name' => "distanceSlab[" . $i . "][slabCharge]",
										'class' => 'form-control',
										'required' => 'required',
										'pattern' => '^\d{0,7}([\.]?(\d{1,2}))$',
										'maxlength' => '10',
										'placeholder' => 'Slab Charge',
										'value' => ($distance_slab->get ( 'slabCharge' )) ? $distance_slab->get ( 'slabCharge' ) : 0 
								) );
							} else {
								echo text ( $distance_slab->get ( 'slabCharge' ) );
							}
							?></td>

										<td class="col-md-3">
										<?php
							
							if ($view_mode == EDIT_MODE) {
								$class_hide = (($distance_slab_count - 1) == $i) ? '' : ' hidden';
								echo form_button ( array (
										'type' => 'button',
										'id' => 'add-distance-slab-' . $i,
										'class' => 'btn btn-sm btn-success' . $class_hide,
										'title' => 'Add',
										'content' => '<i class="fa fa-plus" aria-hidden="true"></i>' 
								) );
								$class_hide = (($distance_slab_count - 1) == $i && $i != 0) ? '' : ' hidden';
								echo form_button ( array (
										'type' => 'button',
										'id' => 'del-distance-slab-' . $i,
										'class' => 'btn btn-sm btn-danger' . $class_hide,
										'title' => 'Delete',
										'content' => '<i class="fa fa-trash-o"></i></i>' 
								) );
							}
							?>
										
											
											

										</td>
									</tr>
									<?php $i++; }}else{?>
									<tr class="gp-mg ">

										<td class="col-md-3 gp-row">
                                            <?php
						
						echo form_input ( array (
								'type' => 'hidden',
								'name' => 'result[0][key]',
								'class' => 'slab-count' 
						) );
						echo form_input ( array (
								'type' => 'hidden',
								'id' => 'distance-slabId-0',
								'name' => 'distanceSlab[0][slabId]',
								'value' => ($taxi_tariff_distance_slab_model->get ( 'id' )) ? $taxi_tariff_distance_slab_model->get ( 'id' ) : - 1 
						) );
						echo form_input ( array (
								'type' => 'hidden',
								'id' => 'distance-slabType-0',
								'name' => 'distanceSlab[0][slabType]',
								'value' => Slab_Type_Enum::DISTANCE 
						) );
						echo form_input ( array (
								'type' => 'hidden',
								'id' => 'distance-slabChargeType-0',
								'name' => 'distanceSlab[0][slabChargeType]',
								'value' => Payment_Type_Enum::AMOUNT 
						) );
						echo form_input ( array (
								'id' => 'distance-slabStart-0',
								'name' => "distanceSlab[0][slabStart]",
								'class' => 'form-control',
								'required' => 'required',
								'pattern' => '\d{1,2}',
								'maxlength' => '2',
								'placeholder' => 'Start Slab',
								'value' => ($taxi_tariff_distance_slab_model->get ( 'slabStart' )) ? $taxi_tariff_distance_slab_model->get ( 'slabStart' ) : 0,
								'readonly' => 'true' 
						) );
						
						?></td>
										<td class="col-md-3"><?php
										
						echo form_input ( array (
								'id' => 'distance-slabEnd-0',
								'name' => "distanceSlab[0][slabEnd]",
								'class' => 'form-control',
								'required' => 'required',
								'pattern' => '\d{1,2}',
								'maxlength' => '2',
								'placeholder' => 'End Slab',
								'value' => ($taxi_tariff_distance_slab_model->get ( 'slabEnd' )) ? $taxi_tariff_distance_slab_model->get ( 'slabEnd' ) : 0 
						) );
						
						?></td>

										<td class="col-md-3"><?php
						
						echo form_input ( array (
								'id' => 'distance-slabCharge-0',
								'name' => "distanceSlab[0][slabCharge]",
								'class' => 'form-control',
								'required' => 'required',
								'pattern' => '^\d{0,7}([\.]?(\d{1,2}))$',
								'maxlength' => '10',
								'placeholder' => 'Slab Charge',
								'value' => ($taxi_tariff_distance_slab_model->get ( 'slabCharge' )) ? $taxi_tariff_distance_slab_model->get ( 'slabCharge' ) : '' 
						) );
						
						?></td>

										<td class="col-md-3">
											<?php
						
						if ($view_mode == EDIT_MODE) {
							
							echo form_button ( array (
									'type' => 'button',
									'id' => 'add-distance-slab-0',
									'class' => 'btn btn-sm btn-success',
									'title' => 'Add',
									'content' => '<i class="fa fa-plus" aria-hidden="true"></i>' 
							) );
							
							echo form_button ( array (
									'type' => 'button',
									'id' => 'del-distance-slab-0',
									'class' => 'btn btn-sm btn-danger hidden',
									'title' => 'Delete',
									'content' => '<i class="fa fa-trash-o"></i></i>' 
							) );
						}
						?>
										</td>
									</tr>
									<?php } ?>
								</tbody>

							</table>
						</div>

					</div>




				</div>
<?php endif;?>
				



<?php if ($taxi_tariff_time_slab_model):?>

				<div class="form-body gp-form">
					<div class="card-header bg-white">
						<h1>Time Slab</h1>
					</div>
					<div class="row">



						<div id="time-slab" class="section-container">
							<div class="section-message"></div>
							<table id="time-slab-table" class="table-head"
								style="width: 100%;">
								<thead>

									<tr>
										<th class="col-md-3">Slab start</th>
										<th class="col-md-3">Slab end</th>
										<th class="col-md-3">Slab charge</th>
           <?php
	
if ($view_mode == EDIT_MODE) {
		echo '<th class="col-md-3">Action</th>';
	}
	?>
        </tr>
								</thead>
								<tbody>
									<?php if ($edit_time_slab_model || ($view_mode == VIEW_MODE)){ $time_slab_count=count ($taxi_tariff_time_slab_model);$i=0; foreach ($taxi_tariff_time_slab_model as $time_slab){ ?>
									<tr class="gp-mg">

										<td class="col-md-3">
                                            <?php
			echo form_input ( array (
					'type' => 'hidden',
					'name' => 'result[' . $i . '][key]',
					'class' => 'slab-count' 
			) );
			if ($view_mode == EDIT_MODE) {
				echo form_input ( array (
						'type' => 'hidden',
						'id' => 'time-slabId-' . $i,
						'name' => 'timeSlab[' . $i . '][slabId]',
						'value' => ($time_slab->get ( 'id' )) ? $time_slab->get ( 'id' ) : - 1 
				) );
				echo form_input ( array (
						'type' => 'hidden',
						'id' => 'time-slabType-' . $i,
						'name' => 'timeSlab[' . $i . '][slabType]',
						'value' => Slab_Type_Enum::TIME 
				) );
				echo form_input ( array (
						'type' => 'hidden',
						'id' => 'time-slabChargeType-' . $i,
						'name' => 'timeSlab[' . $i . '][slabChargeType]',
						'value' => Payment_Type_Enum::AMOUNT 
				) );
				echo form_input ( array (
						'id' => 'time-slabStart-' . $i,
						'name' => 'timeSlab[' . $i . '][slabStart]',
						'class' => 'form-control',
						'required' => 'required',
						'pattern' => '\d{1,2}',
						'maxlength' => '2',
						'placeholder' => 'Start Slab',
						'value' => ($time_slab->get ( 'slabStart' )) ? $time_slab->get ( 'slabStart' ) : 0,
						'readonly' => 'true' 
				) );
			} else {
				echo text ( $time_slab->get ( 'slabStart' ) );
			}
			
			?></td>
										<td class="col-md-3"><?php
			if ($view_mode == EDIT_MODE) {
				if (($time_slab_count - 1) == $i)
				{
				echo form_input ( array (
						'id' => 'time-slabEnd-' . $i,
						'name' => "timeSlab[" . $i . "][slabEnd]",
						'class' => 'form-control',
						'required' => 'required',
						'pattern' => '\d{1,2}',
						'maxlength' => '2',
						'placeholder' => 'End Slab',
						'value' => ($time_slab->get ( 'slabEnd' )) ? $time_slab->get ( 'slabEnd' ) : 0 
				) );
				}
				else 
				{
					echo form_input ( array (
							'id' => 'time-slabEnd-' . $i,
							'name' => "timeSlab[" . $i . "][slabEnd]",
							'class' => 'form-control',
							'required' => 'required',
							'readonly' => 'readonly',
							'pattern' => '\d{1,2}',
							'maxlength' => '2',
							'placeholder' => 'End Slab',
							'value' => ($time_slab->get ( 'slabEnd' )) ? $time_slab->get ( 'slabEnd' ) : 0
					) );
				}
			} else {
				echo text ( $time_slab->get ( 'slabEnd' ) );
			}
			?></td>

										<td class="col-md-3"><?php
			if ($view_mode == EDIT_MODE) {
				echo form_input ( array (
						'id' => 'time-slabCharge-' . $i,
						'name' => "timeSlab[" . $i . "][slabCharge]",
						'class' => 'form-control',
						'required' => 'required',
						'pattern' => '^\d{0,7}([\.]?(\d{1,2}))$',
						'maxlength' => '10',
						'placeholder' => 'Slab Charge',
						'value' => ($time_slab->get ( 'slabCharge' )) ? $time_slab->get ( 'slabCharge' ) : 0 
				) );
			} else {
				echo text ( $time_slab->get ( 'slabCharge' ) );
			}
			?></td>
											
										
										<?php
			
			if ($view_mode == EDIT_MODE) {
				echo '<td class="col-md-3">';
				
				$class_hide = (($time_slab_count - 1) == $i) ? '' : ' hidden';
				echo form_button ( array (
						'type' => 'button',
						'id' => 'add-time-slab-' . $i,
						'class' => 'btn btn-sm btn-success' . $class_hide,
						'title' => 'Add',
						'content' => '<i class="fa fa-plus" aria-hidden="true"></i>' 
				) );
				$class_hide = (($time_slab_count - 1) == $i && $i != 0) ? '' : ' hidden';
				
				echo form_button ( array (
						'type' => 'button',
						'id' => 'del-time-slab-' . $i,
						'class' => 'btn btn-sm btn-danger' . $class_hide,
						'title' => 'Delete',
						'content' => '<i class="fa fa-trash-o"></i></i>' 
				) );
			}
			echo '</td>';
			?>
																				
									</tr>
									<?php $i++; }} else {?>
									<tr class="gp-mg">

										<td class="col-md-3">
                                            <?php
		
		echo form_input ( array (
				'type' => 'hidden',
				'name' => 'result[0][key]',
				'class' => 'slab-count' 
		) );
		
		echo form_input ( array (
				'type' => 'hidden',
				'id' => 'time-slabId-0',
				'name' => 'timeSlab[0][slabId]',
				'value' => ($taxi_tariff_time_slab_model->get ( 'id' )) ? $taxi_tariff_time_slab_model->get ( 'id' ) : - 1 
		) );
		echo form_input ( array (
				'type' => 'hidden',
				'id' => 'time-slabType-0',
				'name' => 'timeSlab[0][slabType]',
				'value' => Slab_Type_Enum::TIME 
		) );
		echo form_input ( array (
				'type' => 'hidden',
				'id' => 'timw-slabChargeType-0',
				'name' => 'timeSlab[0][slabChargeType]',
				'value' => Payment_Type_Enum::AMOUNT 
		) );
		echo form_input ( array (
				'id' => 'time-slabStart-0',
				'name' => "timeSlab[0][slabStart]",
				'class' => 'form-control',
				'required' => 'required',
				'pattern' => '\d{1,2}',
				'placeholder' => 'Start Slab',
				'value' => ($taxi_tariff_time_slab_model->get ( 'slabStart' )) ? $taxi_tariff_time_slab_model->get ( 'slabStart' ) : 0,
				'readonly' => 'true' 
		) );
		
		?></td>
										<td class="col-md-3"><?php
		if ($view_mode == EDIT_MODE) {
			echo form_input ( array (
					'id' => 'time-slabEnd-0',
					'name' => "timeSlab[0][slabEnd]",
					'class' => 'form-control',
					'required' => 'required',
					'pattern' => '\d{1,2}',
					'placeholder' => 'End Slab',
					'value' => ($taxi_tariff_time_slab_model->get ( 'slabEnd' )) ? $taxi_tariff_time_slab_model->get ( 'slabEnd' ) : '' 
			) );
		} else {
			echo text ( $taxi_tariff_time_slab_model->get ( 'slabEnd' ) );
		}
		?></td>

										<td class="col-md-3"><?php
		if ($view_mode == EDIT_MODE) {
			echo form_input ( array (
					'id' => 'time-slabCharge-0',
					'name' => "timeSlab[0][slabCharge]",
					'class' => 'form-control',
					'pattern' => '^\d{0,7}([\.]?(\d{1,2}))$',
					'maxlength' => '10',
					'required' => 'required',
					'placeholder' => 'Slab Charge',
					'value' => ($taxi_tariff_time_slab_model->get ( 'slabCharge' )) ? $taxi_tariff_time_slab_model->get ( 'slabCharge' ) : '' 
			) );
		} else {
			echo text ( $taxi_tariff_time_slab_model->get ( 'slabCharge' ) );
		}
		?></td>

										<td class="col-md-3">
											<?php
		
		if ($view_mode == EDIT_MODE) {
			
			echo form_button ( array (
					'type' => 'button',
					'id' => 'add-time-slab-0',
					'class' => 'btn btn-sm btn-success',
					'title' => 'Add',
					'content' => '<i class="fa fa-plus" aria-hidden="true"></i>' 
			) );
			
			echo form_button ( array (
					'type' => 'button',
					'id' => 'del-time-slab-0',
					'class' => 'btn btn-sm btn-danger hidden',
					'title' => 'Delete',
					'content' => '<i class="fa fa-trash-o"></i></i>' 
			) );
		}
		?>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>

					</div>




				</div>
<?php endif;?>



		<?php if ($taxi_tariff_surge_slab_model):?>	
				<div class="form-body gp-form">

					<div class="card-header bg-white">

						<h1>Surge Slab</h1>
					</div>
					<div class="row">



						<div id="surge-slab">

							<table id="surge-slab-table" style="width: 100%;">
								<thead>

									<tr>
										<th class="col-md-2">Slab start</th>
										<th class="col-md-2">Slab end</th>
										<th class="col-md-2">Slab charge type</th>
										<th class="col-md-3">Slab charge</th>
            <?php
			
if ($view_mode == EDIT_MODE) {
				echo '<th class="col-md-3">Action</th>';
			}
			?>
        </tr>
								</thead>
								<tbody>
								<?php
			
			if ($edit_surge_slab_model || ($view_mode == VIEW_MODE)) {
				$surge_slab_count = count ( $taxi_tariff_surge_slab_model );
				$i = 0;
				foreach ( $taxi_tariff_surge_slab_model as $surge_slab ) {
					?>
								
									<tr class="gp-mg">

										<td class="col-md-2">
                                            <?php
					echo form_input ( array (
							'type' => 'hidden',
							'name' => 'result[' . $i . '][key]',
							'class' => 'slab-count' 
					) );
					if ($view_mode == EDIT_MODE) {
						
						echo form_input ( array (
								'type' => 'hidden',
								'id' => 'surge-slabId-' . $i,
								'name' => 'surgeSlab[' . $i . '][slabId]',
								'value' => ($surge_slab->get ( 'id' )) ? $surge_slab->get ( 'id' ) : - 1 
						) );
						echo form_input ( array (
								'type' => 'hidden',
								'id' => 'surge-slabType-' . $i,
								'name' => 'surgeSlab[' . $i . '][slabType]',
								'value' => Slab_Type_Enum::SURGE 
						) );
						
						echo form_input ( array (
								'id' => 'surge-slabStart-' . $i,
								'name' => 'surgeSlab[' . $i . '][slabStart]',
								'class' => 'form-control',
								
								// 'required' => 'required',
								'placeholder' => 'Start Slab',
								'value' => ($surge_slab->get ( 'slabStart' )) ? $surge_slab->get ( 'slabStart' ) : '' 
						)
						 );
					} else {
						echo text ( $surge_slab->get ( 'slabStart' ) );
					}
					?>
										
										
										
										</td>
										<td class="col-md-2"><?php
					if ($view_mode == EDIT_MODE) {
						echo form_input ( array (
								'id' => 'surge-slabEnd-' . $i,
								'name' => "surgeSlab[" . $i . "][slabEnd]",
								'class' => 'form-control',
								
								// 'required' => 'required',
								'placeholder' => 'End Slab',
								'value' => ($surge_slab->get ( 'slabEnd' )) ? $surge_slab->get ( 'slabEnd' ) : 0 
						) );
					} else {
						echo text ( $surge_slab->get ( 'slabEnd' ) );
					}
					?></td>



										<td class="col-md-2">
										<?php
					if ($view_mode == EDIT_MODE) {
						echo form_dropdown ( 'surgeSlab[' . $i . '][slabChargeType]', $payment_type_list, $surge_slab->get ( 'slabChargeType' ), array (
								'id' => 'surge-slabChargeType-' . $i,
								'class' => 'form-control' 
						)
						// 'required' => 'required'
						 );
					} else {
						echo text ( @$payment_type_list [$surge_slab->get ( 'slabChargeType' )] );
					}
					?>
										</td>
										<td class="col-md-3"><?php
					if ($view_mode == EDIT_MODE) {
						echo form_input ( array (
								'id' => 'surge-slabCharge-' . $i,
								'name' => "surgeSlab[" . $i . "][slabCharge]",
								'class' => 'form-control',
								
								// 'required' => 'required',
								'placeholder' => 'Slab Charge',
								'value' => ($surge_slab->get ( 'slabCharge' )) ? $surge_slab->get ( 'slabCharge' ) : 0 
						) );
					} else {
						echo text ( $surge_slab->get ( 'slabCharge' ) );
					}
					?></td>
										

										
										<?php
					
					if ($view_mode == EDIT_MODE) {
						echo '<td class="col-md-3">';
						$class_hide = (($surge_slab_count - 1) == $i) ? '' : ' hidden';
						echo form_button ( array (
								'type' => 'button',
								'id' => 'add-surge-slab-' . $i,
								'class' => 'btn btn-sm btn-success' . $class_hide,
								'title' => 'Add',
								'content' => '<i class="fa fa-plus" aria-hidden="true"></i>' 
						) );
						$class_hide = (($surge_slab_count - 1) == $i && $i != 0) ? '' : ' hidden';
						echo form_button ( array (
								'type' => 'button',
								'id' => 'del-surge-slab-' . $i,
								'class' => 'btn btn-sm btn-danger' . $class_hide,
								'title' => 'Delete',
								'content' => '<i class="fa fa-trash-o"></i></i>' 
						) );
					}
					echo '</td>';
					?>
										
										
									</tr>
									<?php
					
					$i ++;
				}
			} else {
				?>
									<tr class="gp-mg">

										<td class="col-md-2">
                                            <?php
				echo form_input ( array (
						'type' => 'hidden',
						'name' => 'result[0][key]',
						'class' => 'slab-count' 
				) );
				
				echo form_input ( array (
						'type' => 'hidden',
						'id' => 'surge-slabId-0',
						'name' => 'surgeSlab[0][slabId]',
						'value' => ($taxi_tariff_surge_slab_model->get ( 'id' )) ? $taxi_tariff_surge_slab_model->get ( 'id' ) : - 1 
				) );
				echo form_input ( array (
						'type' => 'hidden',
						'id' => 'surge-slabType-0',
						'name' => 'surgeSlab[0][slabType]',
						'value' => Slab_Type_Enum::SURGE 
				) );
				
				echo form_input ( array (
						'id' => 'surge-slabStart-0',
						'name' => "surgeSlab[0][slabStart]",
						'class' => 'form-control',
						
						// 'required' => 'required',
						'placeholder' => 'Start Slab',
						'value' => ($taxi_tariff_surge_slab_model->get ( 'slabStart' )) ? $taxi_tariff_surge_slab_model->get ( 'slabStart' ) : '' 
				) );
				
				?>
																																												</td>
										<td class="col-md-2"><?php
				if ($view_mode == EDIT_MODE) {
					echo form_input ( array (
							'id' => 'surge-slabEnd-0',
							'name' => "surgeSlab[0][slabEnd]",
							'class' => 'form-control',
							
							// 'required' => 'required',
							'placeholder' => 'End Slab',
							'value' => ($taxi_tariff_surge_slab_model->get ( 'slabEnd' )) ? $taxi_tariff_surge_slab_model->get ( 'slabEnd' ) : '' 
					) );
				} else {
					echo text ( $taxi_tariff_surge_slab_model->get ( 'slabEnd' ) );
				}
				?></td>
										<td class="col-md-2">
										<?php
				if ($view_mode == EDIT_MODE) {
					echo form_dropdown ( 'surgeSlab[0][slabChargeType]', $payment_type_list, $taxi_tariff_surge_slab_model->get ( 'slabChargeType' ), array (
							'id' => 'surge-slabChargeType-0',
							'class' => 'form-control' 
					)
					// 'required' => 'required'
					 );
				} else {
					echo text ( $payment_type_list [$taxi_tariff_surge_slab_model->get ( 'slabChargeType' )] );
				}
				?>
										</td>
										<td class="col-md-3"><?php
				if ($view_mode == EDIT_MODE) {
					echo form_input ( array (
							'id' => 'surge-slabCharge-0',
							'name' => "surgeSlab[0][slabCharge]",
							'class' => 'form-control',
							
							// 'required' => 'required',
							'placeholder' => 'Slab Charge',
							'value' => ($taxi_tariff_surge_slab_model->get ( 'slabCharge' )) ? $taxi_tariff_surge_slab_model->get ( 'slabCharge' ) : '' 
					) );
				} else {
					echo text ( $taxi_tariff_surge_slab_model->get ( 'slabCharge' ) );
				}
				?></td>

										
											<?php
				
				if ($view_mode == EDIT_MODE) {
					echo '<td class="col-md-3">';
					echo form_button ( array (
							'type' => 'button',
							'id' => 'add-surge-slab-0',
							'class' => 'btn btn-sm btn-success',
							'title' => 'Add',
							'content' => '<i class="fa fa-plus" aria-hidden="true"></i>' 
					) );
					
					echo form_button ( array (
							'type' => 'button',
							'id' => 'del-surge-slab-0',
							'class' => 'btn btn-sm btn-danger hidden',
							'title' => 'Delete',
							'content' => '<i class="fa fa-trash-o"></i></i>' 
					) );
				}
				echo '</td>';
				?>
										
									</tr>
									
<?php }?>
								</tbody>
							</table>
						</div>

					</div>

				</div>
<?php endif;?>

			<div class="form-actions" style="margin-top: 22px;">
					<div class="row">
						<div class="col-md-12">
                    <?php if ($view_mode == EDIT_MODE) { ?>
                    <div class="gp-cen">
								<button type="button" id="cancel-tariff-btn"
									class="btn grey-salsa btn-outline">Cancel</button>
								<button type="button" id="save-tariff-btn" class="btn gpblue">Save</button>
							</div>
                    <?php } else { ?>
                    <div class="gp-cen">
								<button type="button" id="cancel-tariff-btn"
									class="btn btn-danger">Back</button>
							</div>
                    <?php } ?>
                  </div>
					</div>
				</div>
				<!-- END VALIDATION STATES-->
			</div>
		</div>
       
               
 <?php
	echo form_close ();
	?>


    </div>
	<!-- END CONTENT BODY -->
</div>
