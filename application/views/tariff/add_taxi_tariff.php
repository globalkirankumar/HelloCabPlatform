<?php
$view_mode = $mode;
?>
<!-- BEGIN CONTENT -->
<style>
.gp-row {
	margin-bottom: 10px;
}
</style>
<!-- BEGIN CONTENT BODY -->

<div class="page-content">
    <?php
				$form_attr = array (
						'name' => 'edit_taxiTariff_form',
						'id' => 'edit_taxiTariff_form',
						'method' => 'POST',
						'action' => 'welcome' 
				);
				
				echo form_open_multipart ( base_url ( '' ), $form_attr );
				// driver id by default is -1
				echo form_input ( array (
						'type' => 'hidden',
						'id' => 'taxiTariff_id',
						'name' => 'id',
						'value' => ($taxi_tariff_model->get ( 'id' )) ? $taxi_tariff_model->get ( 'id' ) : - 1 
				) );
				echo form_input ( array (
						'type' => 'hidden',
						'id' => 'entityId',
						'name' => 'entityId',
						'value' => DEFAULT_COMPANY_ID 
				) );
				?>
    <!--<form method="post" accept-charset="utf-8" action="http://dev.hellocabs.com/Taxitariff/save_tarif">-->
	<div class="page-bar ">
		<ul class="page-breadcrumb ">
			<li><a href="<?php echo base_url('Dashboard'); ?>">Home</a> <i
				class="fa fa-circle"></i></li>
			<li><a href="<?php echo base_url('TaxiTariff/getTaxiTariffList'); ?>">Tariff
					List</a> <i class="fa fa-circle"></i></li>
			<li><a href="<?php echo base_url(uri_string()); ?>"><?php echo ($taxi_tariff_model->get('id')) ? (($view_mode == VIEW_MODE) ? 'View' : 'Edit') : 'Create'; ?> Tariff</a></li>

		</ul>

	</div>
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN VALIDATION STATES-->


			<div class="portlet-body">
				<div class="form-body gp-form">
					<div class="card-header bg-white">
						<h1><?php echo ($taxi_tariff_model->get('id')) ? (($view_mode == VIEW_MODE) ? 'View' : 'Edit') : 'Create'; ?> Tariff</h1>
					</div>
					<div class="row">

						<div class="col-lg-3 input_field_sections nowarp">
                            <?php
																												echo form_label ( 'Name:', 'name', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																												) );
																												
																												if ($view_mode == EDIT_MODE) {
																													echo form_input ( array (
																															'id' => 'name',
																															'name' => 'name',
																															'class' => 'form-control',
																															'required' => 'required',
																															
																															// 'pattern' => '[a-zA-Z0-9\s]{3,15}',
																															'placeholder' => 'Name',
																															'value' => ($taxi_tariff_model->get ( 'name' )) ? $taxi_tariff_model->get ( 'name' ) : '' 
																													) );
																												} else {
																													echo text ( $taxi_tariff_model->get ( 'name' ) );
																												}
																												?>

                        </div>
						<div class="col-lg-3 input_field_sections nowarp">
                            <?php
																												echo form_label ( 'Base Charge:', 'dayConvenienceCharge', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																												) );
																												
																												if ($view_mode == EDIT_MODE) {
																													echo form_input ( array (
																															'id' => 'dayConvenienceCharge',
																															'name' => 'dayConvenienceCharge',
																															'class' => 'form-control',
																															'required' => 'required',
																															
																															// 'pattern' => '\d{1,6}',
																															'placeholder' => 'Base Charge',
																															'value' => ($taxi_tariff_model->get ( 'dayConvenienceCharge' )) ? $taxi_tariff_model->get ( 'dayConvenienceCharge' ) : '' 
																													) );
																												} else {
																													echo text ( $taxi_tariff_model->get ( 'dayConvenienceCharge' ) );
																												}
																												?>

                        </div>
						<div class="col-lg-3 input_field_sections nowarp">

<?php
echo form_label ( 'Taxi Category Type:', 'taxiCategoryType', array (
		'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
) );

if ($view_mode == EDIT_MODE) {
	echo form_dropdown ( 'taxiCategoryType', $taxi_list, $taxi_tariff_model->get ( 'taxiCategoryType' ), array (
			'id' => 'taxiCategoryType',
			'class' => 'form-control',
			'required' => 'required' 
	) );
} else {
	echo text ( $taxi_list [$taxi_tariff_model->get ( 'taxiCategoryType' )] );
}
?>


                        </div>
						<div class="col-lg-3 input_field_sections nowarp">

<?php
echo form_label ( 'Zone:', 'zoneId', array (
		'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
) );

if ($view_mode == EDIT_MODE) {
	echo form_dropdown ( 'zoneId', $zone_list, $taxi_tariff_model->get ( 'zoneId' ), array (
			'id' => 'zoneId',
			'class' => 'form-control',
			'required' => 'required' 
	) );
} else {
	echo text ( $zone_list [$taxi_tariff_model->get ( 'zoneId' )] );
}
?>


                        </div>

					</div>


					<div class="row">




						<div class="col-lg-3 input_field_sections nowarp">

<?php
echo form_label ( 'Entity:', 'entityId', array (
		'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
) );

if ($view_mode == EDIT_MODE) {
	echo form_dropdown ( 'entityId', $entity_list, $taxi_tariff_model->get ( 'entityId' ), array (
			'id' => 'entityId',
			'class' => 'form-control',
			'required' => 'required' 
	) );
} else {
	echo text ( $entity_list [$taxi_tariff_model->get ( 'entityId' )] );
}
?>


                        </div>
					</div>

				</div>
				<div class="form-body gp-form">
					<div class="card-header bg-white">
						<h1>Distance Slab</h1>
					</div>
					<div class="row">



						<div id="distance-slab" class="section-container">
							<div class="section-message"></div>
							<table id="distance-slab-table" style="width: 100%;">
								<tbody>

									<tr class="gp-mg ">

										<td class="col-md-3 gp-row">
                                            <?php
																																												if ($view_mode == EDIT_MODE) {
													echo form_input ( array (
												'type' => 'hidden',
												'name' => 'result[0][key]',
												'class' => 'slab-count' 
										) );																																echo form_input ( array (
																																															'type' => 'hidden',
																																															'id' => 'distance-slabType-0',
																																															'name' => 'distanceSlab[0][slabType]',
																																															'value' => Slab_Type_Enum::DISTANCE 
																																													) );
																																													echo form_input ( array (
																																															'type' => 'hidden',
																																															'id' => 'distance-slabChargeType-0',
																																															'name' => 'distanceSlab[0][slabChargeType]',
																																															'value' => Payment_Type_Enum::AMOUNT 
																																													) );
																																													echo form_input ( array (
																																															'id' => 'distance-slabStart-0',
																																															'name' => "distanceSlab[0][slabStart]",
																																															'class' => 'form-control',
																																															'required' => 'required',
																																															'pattern' => '\d{1,2}',
																																															'placeholder' => 'Start Slab',
																																															'value' => ($taxi_tariff_slab_model->get ( 'slabStart' )) ? $taxi_tariff_slab_model->get ( 'slabStart' ) : 0,
																																															'readonly' => 'true' 
																																													) );
																																												} else {
																																													echo text ( $taxi_tariff_slab_model->get ( 'slabStart' ) );
																																												}
																																												?></td>
										<td class="col-md-3"><?php
										if ($view_mode == EDIT_MODE) {
											echo form_input ( array (
													'id' => 'distance-slabEnd-0',
													'name' => "distanceSlab[0][slabEnd]",
													'class' => 'form-control',
													'required' => 'required',
													'pattern' => '\d{1,2}',
													'placeholder' => 'End Slab',
													'value' => ($taxi_tariff_slab_model->get ( 'slabEnd' )) ? $taxi_tariff_slab_model->get ( 'slabEnd' ) : '' 
											) );
										} else {
											echo text ( $taxi_tariff_slab_model->get ( 'slabEnd' ) );
										}
										?></td>

										<td class="col-md-3"><?php
										if ($view_mode == EDIT_MODE) {
											echo form_input ( array (
													'id' => 'distance-slabCharge-0',
													'name' => "distanceSlab[0][slabCharge]",
													'class' => 'form-control',
													'required' => 'required',
													'placeholder' => 'Slab Charge',
													'value' => ($taxi_tariff_slab_model->get ( 'slabChargeType' )) ? $taxi_tariff_slab_model->get ( 'slabChargeType' ) : '' 
											) );
										} else {
											echo text ( $taxi_tariff_slab_model->get ( 'slabCharge' ) );
										}
										?></td>

										<td class="col-md-3">
											<button  class="btn btn-sm btn-success" id="add-distance-slab-0"
												title="Add">
												<i class="fa fa-plus" aria-hidden="true"></i>
											</button>

											<button class="btn btn-sm btn-danger hidden" id="del-distance-slab-0" title="Delete">
												<i class="fa fa-trash-o"></i>
											</button>


										</td>
									</tr>
								</tbody>
							</table>
						</div>

					</div>




				</div>







				<div class="form-body gp-form">
					<div class="card-header bg-white">
						<h1>Time Slab</h1>
					</div>
					<div class="row">



						<div id="time-slab" class="section-container">
							<div class="section-message"></div>
							<table id="time-slab-table" style="width: 100%;">
								<tbody>

									<tr class="gp-mg">

										<td class="col-md-3">
                                            <?php
													
													echo form_input ( array (
												'type' => 'hidden',
												'name' => 'result[0][key]',
												'class' => 'slab-count' 
										) );																															if ($view_mode == EDIT_MODE) {
																																													echo form_input ( array (
																																															'type' => 'hidden',
																																															'id' => 'time-slabType-0',
																																															'name' => 'timeSlab[0][slabType]',
																																															'value' => Slab_Type_Enum::DISTANCE 
																																													) );
																																													echo form_input ( array (
																																															'type' => 'hidden',
																																															'id' => 'timw-slabChargeType-0',
																																															'name' => 'timeSlab[0][slabChargeType]',
																																															'value' => Payment_Type_Enum::AMOUNT 
																																													) );
																																													echo form_input ( array (
																																															'id' => 'time-slabStart-0',
																																															'name' => "timeSlab[0][slabStart]",
																																															'class' => 'form-control',
																																															'required' => 'required',
																																															'pattern' => '\d{1,2}',
																																															'placeholder' => 'Start Slab',
																																															'value' => ($taxi_tariff_slab_model->get ( 'slabStart' )) ? $taxi_tariff_slab_model->get ( 'slabStart' ) : 0,
																																															'readonly' => 'true' 
																																													) );
																																												} else {
																																													echo text ( $taxi_tariff_slab_model->get ( 'slabStart' ) );
																																												}
																																												?></td>
										<td class="col-md-3"><?php
										if ($view_mode == EDIT_MODE) {
											echo form_input ( array (
													'id' => 'time-slabEnd-0',
													'name' => "timeSlab[0][slabEnd]",
													'class' => 'form-control',
													'required' => 'required',
													'pattern' => '\d{1,2}',
													'placeholder' => 'End Slab',
													'value' => ($taxi_tariff_slab_model->get ( 'slabEnd' )) ? $taxi_tariff_slab_model->get ( 'slabEnd' ) : '' 
											) );
										} else {
											echo text ( $taxi_tariff_slab_model->get ( 'slabEnd' ) );
										}
										?></td>

										<td class="col-md-3"><?php
										if ($view_mode == EDIT_MODE) {
											echo form_input ( array (
													'id' => 'time-slabCharge-0',
													'name' => "timeSlab[0][slabCharge]",
													'class' => 'form-control',
													'required' => 'required',
													'placeholder' => 'Slab Charge',
													'value' => ($taxi_tariff_slab_model->get ( 'slabChargeType' )) ? $taxi_tariff_slab_model->get ( 'slabChargeType' ) : '' 
											) );
										} else {
											echo text ( $taxi_tariff_slab_model->get ( 'slabCharge' ) );
										}
										?></td>

										<td class="col-md-3">
											<button class="btn btn-sm btn-success"
												title="Add" id="add-time-slab-0">
												<i class="fa fa-plus" aria-hidden="true"></i>
											</button>

											<button class="btn btn-sm btn-danger hidden" id="del-time-slab-0" title="Delete">
												<i class="fa fa-trash-o"></i>
											</button>


										</td>
									</tr>
								</tbody>
							</table>
						</div>

					</div>




				</div>





				<div class="form-body gp-form">
					<div class="card-header bg-white">
						<h1>Surge Slab</h1>
					</div>
					<div class="row">



						<div id="surge-slab" class="section-container">
						<div class="section-message"></div>
							<table id="surge-slab-table" style="width: 100%;">
								<tbody>
									<tr class="gp-mg">

										<td class="col-md-2">
                                            <?php
												echo form_input ( array (
												'type' => 'hidden',
												'name' => 'result[0][key]',
												'class' => 'slab-count' 
										) );																																		if ($view_mode == EDIT_MODE) {
																																													echo form_input ( array (
																																															'type' => 'hidden',
																																															'id' => 'surge-slabType-0',
																																															'name' => 'surgeSlab[0][slabType]',
																																															'value' => Slab_Type_Enum::SURGE 
																																													) );
																																													echo form_input ( array (
																																															'type' => 'hidden',
																																															'id' => 'distance-slabChargeType-0',
																																															'name' => 'distanceSlab[0][slabChargeType]',
																																															'value' => Payment_Type_Enum::AMOUNT 
																																													) );
																																													echo form_input ( array (
																																															'id' => 'surge-slabStart-0',
																																															'name' => "surgeSlab[0][slabStart]",
																																															'class' => 'form-control',
																																															'required' => 'required',
																																															'placeholder' => 'Start Slab',
																																															'value' => ($taxi_tariff_slab_model->get ( 'slabStart' )) ? $taxi_tariff_slab_model->get ( 'slabStart' ) :'' 
																																													) );
																																												} else {
																																													echo text ( $taxi_tariff_slab_model->get ( 'slabStart' ) );
																																												}
																																												?>
																																												</td>
										<td class="col-md-2"><?php
										if ($view_mode == EDIT_MODE) {
											echo form_input ( array (
													'id' => 'surge-slabEnd-0',
													'name' => "surgeSlab[0][slabEnd]",
													'class' => 'form-control',
													'required' => 'required',
													'placeholder' => 'End Slab',
													'value' => ($taxi_tariff_slab_model->get ( 'slabEnd' )) ? $taxi_tariff_slab_model->get ( 'slabEnd' ) : '' 
											) );
										} else {
											echo text ( $taxi_tariff_slab_model->get ( 'slabEnd' ) );
										}
										?></td>
										<td class="col-md-2">
										<?php
										if ($view_mode == EDIT_MODE) {
											echo form_dropdown ( 'surgeSlab[0][slabChargeType]', $payment_type_list, $taxi_tariff_slab_model->get ( 'slabChargeType' ), array (
													'id' => 'surge-slabChargeType-0',
													'class' => 'form-control',
													'required' => 'required' 
											) );
										} else {
											echo text ( $payment_type_list [$taxi_tariff_slab_model->get ( 'slabChargeType' )] );
										}
										?>
										</td>
										<td class="col-md-3"><?php
										if ($view_mode == EDIT_MODE) {
											echo form_input ( array (
													'id' => 'surge-slabCharge-0',
													'name' => "surgeSlab[0][slabCharge]",
													'class' => 'form-control',
													'required' => 'required',
													'placeholder' => 'Slab Charge',
													'value' => ($taxi_tariff_slab_model->get ( 'slabChargeType' )) ? $taxi_tariff_slab_model->get ( 'slabChargeType' ) : '' 
											) );
										} else {
											echo text ( $taxi_tariff_slab_model->get ( 'slabCharge' ) );
										}
										?></td>

										<td class="col-md-3">
											<button  class="btn btn-sm btn-success" id="add-surge-slab-0"
												title="Add">
												<i class="fa fa-plus" aria-hidden="true"></i>
											</button>

											<button class="btn btn-sm btn-danger hidden" id="del-surge-slab-0" title="Delete">
												<i class="fa fa-trash-o"></i>
											</button>


										</td>
									</tr>

								</tbody>
							</table>
						</div>

					</div>
					<div class="form-actions" style="margin-top: 22px;">
						<div class="row">
							<div class="col-md-12">
                    <?php if ($view_mode == EDIT_MODE) { ?>
                    <div class="gp-cen">
									<button type="button" id="cancel-tariff-btn"
										class="btn grey-salsa btn-outline">Cancel</button>
									<button type="button" id="save-tariff-btn" class="btn gpblue">Save</button>
								</div>
                    <?php } else { ?>
                    <div class="gp-cen">
									<button type="button" id="cancel-tariff-btn"
										class="btn btn-danger">Back</button>
								</div>
                    <?php } ?>
                  </div>
						</div>
					</div>



				</div>



				<!-- END VALIDATION STATES-->
			</div>
		</div>
       
               
 <?php
	echo form_close ();
	?>


    </div>
	<!-- END CONTENT BODY -->
</div>
