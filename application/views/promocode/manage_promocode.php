<style>
    .label.label-sm {
    font-size: 13px;
    padding: 2px 5px;
    cursor:pointer;
}
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">

        <div class="page-bar ">
            <ul class="page-breadcrumb ">
                <li>
                    <a href="<?php echo base_url('Dashboard');?>">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="<?php echo base_url('Promocode/getPromocodeList');?>">Promocode List</a>

                </li>

            </ul>

        </div>
        <!---- Page Bar ends ---->
        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-comments"></i>Promocode List</div>
                                       
                                    </div>
                                 
                                    <div class="portlet-body">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                	<div class="btn-group">
										<a href="<?php echo base_url('Promocode/add');?>">
											<button id="sample_editable_1_new" class="btn sbold green1">
												Add New <i class="fa fa-plus"></i>
											</button>
										</a>
									</div>
                                                   
                                                </div>
                                                
                                            </div>
                                        </div>
						
							<div id="table-columns" class="hidden">
							
									<label class="">
			                          <?php
											echo form_checkbox ( array (
													'class' => 'flat toggle-vis',
													'data-column'=>'7'
											) );
																													
									?>
			                         Individual Limit
									</label>
							
									<label class="">
			                          <?php
											echo form_checkbox ( array (
													'class' => 'flat toggle-vis',
													'data-column'=>'8',
											) );
																													
									?>
			                          Freeride Promocode
									</label>
							
									<!--<label class="">
			                          <?php
											/*echo form_checkbox ( array (
													'class' => 'flat toggle-vis',
													'data-column'=>'10', 
											) );*/
																													
									?>
			                          Zone1 Name
									</label> -->
									<!-- <label class="">
			                          <?php
											/*echo form_checkbox ( array (
													'class' => 'flat toggle-vis',
													'data-column'=>'11', 
											) );*/
																													
									?>
			                          Zone2 Name
									</label> -->
									<label class="">
			                          <?php
											echo form_checkbox ( array (
													'class' => 'flat toggle-vis',
													'data-column'=>'11',
											) );
																													
									?>
			                          Entity
									</label>
							</div>
							<table class="table table-striped table-bordered table-hover table-checkable order-column" id="promocode-data-table" style="width:99%">
								
									<thead>
										<tr>
											<th>Action</th>
											<th>PromoCode</th>
											<th>Discount Percentage</th>
											<th>Discount Amount</th>
											<th>Start Datetime</th>
											<th>End Datetime</th>
											<th>Total Limit</th>
											<th>Individual Limit</th>
											<th>Freeride Promocode</th>
											<th>Zone1 Name</th>
											<th>Zone2 Name</th>
											<th>Entity</th>
											<th>Status</th>
											
										</tr>
									</thead>
									<tbody>
									<!-- <tbody class="fontaws"> -->
									</tbody>
									
								</table>
							</div>
                                        </div>
                                                
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>