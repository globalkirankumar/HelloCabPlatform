<?php
$view_mode = $mode;
?>
<!-- BEGIN CONTENT -->

<!-- BEGIN CONTENT BODY -->
<div class="page-content">
<?php
$form_attr = array (
		'name' => 'edit_promocode_form',
		'id' => 'edit_promocode_form',
		'method' => 'POST' 
);
echo form_open_multipart ( base_url ( '' ), $form_attr );
// passenger id by default is -1
echo form_input ( array (
		'type' => 'hidden',
		'id' => 'promocode_id',
		'name' => 'id',
		'value' => ($promocode_model->get ( 'id' )) ? $promocode_model->get ( 'id' ) : - 1 
) );
/* echo form_input ( array (
		'type' => 'hidden',
		'id' => 'entityId',
		'name' => 'entityId',
		'value' => DEFAULT_COMPANY_ID 
) ); */
?>
        <div class="page-bar ">
		<ul class="page-breadcrumb ">
			<li><a href="<?php echo base_url('Dashboard'); ?>">Home</a> <i
				class="fa fa-circle"></i></li>
			<li><a href="<?php echo base_url('Promocode/getPromocodeList'); ?>">Promocode
					List</a> <i class="fa fa-circle"></i></li>
			<li><a href="<?php echo base_url(uri_string()); ?>"><?php echo ($promocode_model->get('id')) ? (($view_mode == VIEW_MODE) ? 'View' : 'Edit') : 'Create'; ?> Promocode</a></li>

		</ul>

	</div>
	<!---- Page Bar ends ---->
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->

			<div class="portlet-body">
				<div class="form-body gp-form">
					<div class="card-header bg-white">
						<h1><?php echo ($promocode_model->get('id')) ? (($view_mode == VIEW_MODE) ? 'View' : 'Edit') : 'Create'; ?> Promocode</h1>
					</div>

					<div class="row">
						<div class="col-lg-3 input_field_sections nowarp section-container">
						<div class="section-message"></div>
								<?php
								echo form_label ( 'Promocode:', 'promoCode', array (
										'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
								) );
								
								// validation for passenger first name
								if ($view_mode == EDIT_MODE) {
									if ($promocode_model->get ( 'id' )>0)
									{
									echo form_input ( array (
											'id' => 'promoCode',
											'name' => 'promoCode',
											'class' => 'form-control',
											'readonly'=>'readonly',
											'required' => 'required',
											'placeholder' => 'Promocode',
											'value' => ($promocode_model->get ( 'promoCode' )) ? $promocode_model->get ( 'promoCode' ) : '' 
									) );
									}
									else 
									{
										echo form_input ( array (
												'id' => 'promoCode',
												'name' => 'promoCode',
												'class' => 'form-control',
												'required' => 'required',
												'placeholder' => 'Promocode',
												'value' => ($promocode_model->get ( 'promoCode' )) ? $promocode_model->get ( 'promoCode' ) : ''
										) );
									}
								} else {
									echo text ( $promocode_model->get ( 'promoCode' ) );
								}
								?>
							</div>
						<div class="col-md-3 input_field_sections nowarp gp-nomar">
                                <?php
																																echo form_label ( '100 % Discount:', 'isCentPercentDiscount', array (
																																		'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																																) );
																																?>
                                <div class="">
								<label class="mt-checkbox">
                                        <?php
                                        $is_checked = ($promocode_model->get ( 'isCentPercentDiscount' )==1) ? TRUE : FALSE;
                                        $checked = ($promocode_model->get ( 'isCentPercentDiscount' )==1) ? 1 : 0;
                                        if ($view_mode == EDIT_MODE) {
                                        	echo form_checkbox ( array (
                                        			'id' => 'isCentPercentDiscount',
                                        			'name' => 'isCentPercentDiscount',
                                        			'class' => 'flat'
                                        	)
                                        			// 'required' => 'required',
                                        			, $checked, $is_checked );
                                        } else {
                                        	echo form_checkbox ( array (
                                        			'id' => 'isCentPercentDiscount',
                                        			'name' => 'isCentPercentDiscount',
                                        			'class' => 'flat',
                                        			'disabled' => 'disabled'
                                        	)
                                        			// 'required' => 'required',
                                        			, $checked, $is_checked );
                                        }
																																								
																																								?> 100 % Discount
                                        <span></span>
								</label>
							</div>
						</div>

						<div class="col-lg-3 input_field_sections nowarp hidden">
									<?php
									echo form_label ( 'Promo Discount Type:', 'promoDiscountType', array (
											'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
									) );
									// validation for passenger first name
									if ($view_mode == EDIT_MODE) {
										
										echo form_dropdown ( 'promoDiscountType', $payment_type_list, Payment_Type_Enum::PERCENTAGE, array (
												'id' => 'promoDiscountType',
												'name' => 'promoDiscountType',
												'type' => 'hidden',
												'class' => 'form-control',
												'placeholder' => 'Promo Discount Type',
												'required' => 'required' 
										) );
									} else {
										echo text ( $payment_type_list[$promocode_model->get ( 'promoDiscountType' )]);
									}
									?>
							</div>
							<div class="col-lg-3 input_field_sections nowarp">
								<?php
								echo form_label ( 'Promo Discount Percentage:', 'promoDiscountAmount', array (
										'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
								) );
								
								// validation for passenger first name
								if ($view_mode == EDIT_MODE) {
									echo form_input ( array (
												'id' => 'promoDiscountAmount',
												'name' => 'promoDiscountAmount',
												'class' => 'form-control',
												'required' => 'required',
												'pattern'=>'(?!^0*$)(?!^0*\.0*$)^\d{1,2}([\.]?(\d{1,2}))?$',
												//'pattern'=>'^\d{1,2}([\.]?(\d{1,2}))$',
												'placeholder' => 'Promocode Discount Percentage',
												'value' => ($promocode_model->get ( 'promoDiscountAmount' )) ? $promocode_model->get ( 'promoDiscountAmount' ) : ''
										) );
								
								} else {
									echo text ( $promocode_model->get ( 'promoDiscountAmount' ) );
								}
								?>
							</div>
						<div class="col-lg-3 input_field_sections nowarp hidden">
									<?php
									echo form_label ( 'Promo Discount Type:', 'promoDiscountType2', array (
											'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
									) );
									// validation for passenger first name
									if ($view_mode == EDIT_MODE) {
										echo form_dropdown ( 'promoDiscountType2', $payment_type_list, Payment_Type_Enum::AMOUNT, array (
												'id' => 'promoDiscountType2',
												'name' => 'promoDiscountType2',
												'class' => 'form-control',
												'placeholder' => 'Promo Discount Type2',
												'required' => 'required' 
										) );
									} else {
										echo text ( $payment_type_list[$promocode_model->get ( 'promoDiscountType2' )]);
									}
									?>
							</div>
							<div class="col-lg-3 input_field_sections nowarp">
								<?php
								echo form_label ( 'Promo Discount Amount:', 'promoDiscountAmount2', array (
										'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
								) );
								
								// validation for passenger first name
								if ($view_mode == EDIT_MODE) {
									echo form_input ( array (
												'id' => 'promoDiscountAmount2',
												'name' => 'promoDiscountAmount2',
												'class' => 'form-control',
												'required' => 'required',
											    'pattern'=>'^\d{1,6}([\.]?(\d{1,2}))?$',
												'placeholder' => 'Promocode Discount Amount',
												'value' => ($promocode_model->get ( 'promoDiscountAmount2' )) ? $promocode_model->get ( 'promoDiscountAmount2' ) : ''
										) );
									
								} else {
									echo text ( $promocode_model->get ( 'promoDiscountAmount2' ) );
								}
								?>
							</div>
						
						</div>
						<div class="row">
						<div class="col-lg-3 input_field_sections nowarp">
									<?php
									echo form_label ( 'Promo Start Date Time:', 'promoStartDatetime', array (
											'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
									) );
									// validation for passenger first name
									if ($view_mode == EDIT_MODE) {
										echo form_input ( array (
												'id' => 'promoStartDatetime',
												'name' => 'promoStartDatetime',
												'class' => 'form-control',
												'required' => 'required',
												'placeholder' => 'Promo Start Date Time',
												'value' => ($promocode_model->get ( 'promoStartDatetime' )) ? $promocode_model->get ( 'promoStartDatetime' ) : '' 
										) );
									} else {
										echo text ( $promocode_model->get ( 'promoStartDatetime' ) );
									}
									?>
							</div>
						<div class="col-lg-3 input_field_sections nowarp">
									<?php
									echo form_label ( 'Promo End Date Time:', 'promoEndDatetime', array (
											'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
									) );
									// validation for passenger first name
									if ($view_mode == EDIT_MODE) {
										echo form_input ( array (
												'id' => 'promoEndDatetime',
												'name' => 'promoEndDatetime',
												'class' => 'form-control',
												'required' => 'required',
												'placeholder' => 'Promo End Date Time',
												'value' => ($promocode_model->get ( 'promoEndDatetime')) ? $promocode_model->get ( 'promoEndDatetime') : '' 
										) );
									} else {
										echo text ( $promocode_model->get ( 'promoEndDatetime' ) );
									}
									
									?>
							</div>
						<div class="col-lg-3 input_field_sections nowarp">
									<?php
									echo form_label ( 'Promo Code Limit	:', 'promoCodeLimit', array (
											'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
									) );
									// validation for passenger first name
									if ($view_mode == EDIT_MODE) {
										echo form_input ( array (
												'id' => 'promoCodeLimit',
												'name' => 'promoCodeLimit',
												'class' => 'form-control',
												'required' => 'required',
												'pattern'=>'^[1-9]\d{0,4}$',
												'maxlength'=>'5',
												'placeholder' => 'Promo Code Limit',
												'value' => ($promocode_model->get ( 'promoCodeLimit')) ? $promocode_model->get ('promoCodeLimit') : '' 
										) );
									} else {
										echo text ( $promocode_model->get ( 'promoCodeLimit' ) );
									}
									
									?>
							</div>

						<div class="col-lg-3 input_field_sections nowarp">
									<?php
									echo form_label ( 'Promo Code User Limit:', 'promoCodeUserLimit', array (
											'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
									) );
									// validation for passenger first name
									if ($view_mode == EDIT_MODE) {
										echo form_input ( array (
												'id' => 'promoCodeUserLimit',
												'name' => 'promoCodeUserLimit',
												'class' => 'form-control',
												'required' => 'required',
												'pattern'=>'^[1-5]\d{0,1}$',
												'maxlength'=>'2',
												'placeholder' => 'Promo Code User Limit max is 59',
												'value' => ($promocode_model->get ( 'promoCodeUserLimit' )) ? $promocode_model->get ( 'promoCodeUserLimit' ) : '' 
										) );
									} else {
										echo text ( $promocode_model->get ( 'promoCodeUserLimit' ) );
									}
									
									?>
							</div>
							
						
							</div>
							<div class="row">
							<div class="col-lg-2 input_field_sections nowarp">
								<?php
								echo form_label ( 'Entity Name:', 'entityId', array (
										'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
								) );
								// validation for passenger first name
								if ($view_mode == EDIT_MODE) {
									echo form_dropdown ( 'entityId', $entity_list, ($promocode_model->get ( 'entityId' ))?$promocode_model->get ( 'entityId' ):DEFAULT_COMPANY_ID, array (
											'id' => 'entityId',
											'class' => 'form-control',
											'required' => 'required' 
									) );
								} else {
									echo text ( ($promocode_model->get ( 'entityId' ))?$entity_list [$promocode_model->get ( 'entityId' )]:'All Entity' );
								}
								
								?>
							</div>
							<div class="col-lg-2 input_field_sections nowarp">
								<?php
								echo form_label ( 'Promcode Type:', 'promocodeType', array (
										'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
								) );
								// validation for passenger first name
								if ($view_mode == EDIT_MODE) {
									echo form_dropdown( 'promocodeType', $promocode_type_list, ($promocode_model->get ( 'promocodeType' ))?$promocode_model->get ( 'promocodeType' ):Promocode_Type_Enum::GENERIC, array (
											'id' => 'promocodeType',
											'class' => 'form-control',
											'required' => 'required' 
									) );
								} else {
									echo text (($promocode_model->get ( 'promocodeType' ))? $promocode_type_list [$promocode_model->get ( 'promocodeType' )]:'' );
								}
								
								?>
							</div>
							<div class="col-lg-2 input_field_sections nowarp" id="genric-promocode">
								<?php
								echo form_label ( 'Zone Name:', 'zoneId', array (
										'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
								) );
								// validation for passenger first name
								if ($view_mode == EDIT_MODE) {
									echo form_multiselect( 'zoneId[]', $zone_list, ($promocode_model->get ( 'zoneId' ))?$promocode_model->get ( 'zoneId' ):'', array (
											'id' => 'zoneId',
											'class' => 'form-control',
											//'required' => 'required' 
									) );
								} else {
									echo text (($promocode_model->get ( 'zoneId' ))? $zone_list [$promocode_model->get ( 'zoneId' )]:'All Zones' );
								}
								
								?>
							</div>
							<div class="col-lg-2 input_field_sections nowarp hidden"  id="combination-promocode">
								<?php
								echo form_label ( 'Zone1 Name:', 'zoneId1', array (
										'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
								) );
								// validation for passenger first name
								if ($view_mode == EDIT_MODE) {
									echo form_dropdown( 'zoneId1', $zone_list, ($promocode_model->get ( 'zoneId' ))?$promocode_model->get ( 'zoneId' ):'', array (
											'id' => 'zoneId1',
											'class' => 'form-control'
											 
									) );
								} else {
									echo text (($promocode_model->get ( 'zoneId' ))? $zone_list [$promocode_model->get ( 'zoneId' )]:'All Zones' );
								}
								
								?>
							</div>
							
							<div class="col-lg-2 input_field_sections nowarp hidden" id="zone_id2">
								<?php
								echo form_label ( 'Zone2 Name:', 'zoneId2', array (
										'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
								) );
								// validation for passenger first name
								if ($view_mode == EDIT_MODE) {
									echo form_dropdown( 'zoneId2', $zone_list, ($promocode_model->get ( 'zoneId2' ))?$promocode_model->get ( 'zoneId2' ):'', array (
											'id' => 'zoneId2',
											'class' => 'form-control'
									) );
								} else {
									echo text (($promocode_model->get ( 'zoneId2' ))? $zone_list [$promocode_model->get ( 'zoneId2' )]:'' );
								}
								
								?>
							</div>
							<div class="col-md-3 input_field_sections nowarp gp-nomar">
                                <?php
																																echo form_label ( 'First Free Ride:', 'isFirstFreeRide', array (
																																		'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																																) );
																																?>
                                <div class="">
								<label class="mt-checkbox">
                                        <?php
                                        $is_checked = $promocode_model->get ( 'isFirstFreeRide' ) ? TRUE : FALSE;
                                        $checked = $promocode_model->get ( 'isFirstFreeRide' ) ? 1 : 0;
                                        if ($view_mode == EDIT_MODE) {
                                        	echo form_checkbox ( array (
                                        			'id' => 'isFirstFreeRide',
                                        			'name' => 'isFirstFreeRide',
                                        			'class' => 'flat'
                                        	)
                                        			// 'required' => 'required',
                                        			, $checked, $is_checked );
                                        } else {
                                        	echo form_checkbox ( array (
                                        			'id' => 'isFirstFreeRide',
                                        			'name' => 'isFirstFreeRide',
                                        			'class' => 'flat',
                                        			'disabled' => 'disabled'
                                        	)
                                        			// 'required' => 'required',
                                        			, $checked, $is_checked );
                                        }
																																								
																																								?> Is First Free Ride
                                        <span></span>
								</label>
							</div>
						</div>
					</div>
					<div class="form-actions" style="margin-top: 22px;">
						<div class="row">
							<div class="col-md-12">
                                            <?php if ($view_mode == EDIT_MODE) { ?>
                                                <div class="gp-cen">

									<button type="button" id="cancel-promocode-btn"
										class="btn grey-salsa btn-outline">Cancel</button>
									<button type="button" id="save-promocode-btn"
										class="btn gpblue">Save</button>
								</div>
                                            <?php } else { ?>
                                            <div class="gp-cen">
									<button type="button" id="cancel-promocode-btn"
										class="btn btn-danger">Back</button>
								</div>
                                            <?php } ?>
                                        </div>
						</div>
					</div>



				</div>
			</div>
		</div>
		<?php
		echo form_close ();
		?>
	</div>
</div>