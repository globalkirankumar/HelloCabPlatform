<?php
$view_mode = $mode;
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">

		<div class="page-bar ">
			<ul class="page-breadcrumb ">
				<li><a href="<?php echo base_url('Dashboard'); ?>">Home</a> <i
					class="fa fa-circle"></i></li>
				<li><a href="<?php echo base_url('Entity/getEntityList'); ?>">Entity
						List</a> <i class="fa fa-circle"></i></li>
				<li><a href="<?php echo base_url(uri_string()); ?>"><?php echo ($entity_model->get('id')) ? (($view_mode == VIEW_MODE) ? 'View' : 'Edit') : 'Create'; ?> Entity Details</a></li>

			</ul>

		</div>
		<!---- Page Bar ends ---->
		<div class="row">
			<div class="col-md-12">
                <?php
																$form_attr = array (
																		'name' => 'edit_entity_form',
																		'id' => 'edit_entity_form',
																		'method' => 'POST' 
																);
																echo form_open_multipart ( base_url ( '' ), $form_attr );
																// driver id by default is -1
																echo form_input ( array (
																		'type' => 'hidden',
																		'id' => 'entity_id',
																		'name' => 'id',
																		'value' => ($entity_model->get ( 'id' )) ? $entity_model->get ( 'id' ) : - 1 
																) );
																?>
                <!-- /.inner -->
				<div class="row">
					<div class="col-lg-12 m-t-35">
						<div class="portlet-body">
							<div class="form-body gp-form">
								<div class="card-header bg-white">
									<h1><?php
									echo ($entity_model->get ( 'id' )) ? (($view_mode == VIEW_MODE) ? 'View' : 'Edit') : 'Create';
									?> Entity Details</h1>
								</div>

								<div class="row">
									<div class="col-lg-3 input_field_sections">
<?php
echo form_label ( 'Name:', 'name', array (
		'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
) );

if ($view_mode == EDIT_MODE) {
	echo form_input ( array (
			'id' => 'name',
			'name' => 'name',
			'class' => 'form-control',
			'required' => 'required',
			'placeholder' => 'name',
			'value' => ($entity_model->get ( 'name' )) ? $entity_model->get ( 'name' ) : '' 
	) );
} else {
	echo text ( $entity_model->get ( 'name' ) );
}
?>

                                    </div>
									<div class="col-lg-3 input_field_sections section-container">
									<div class="section-message"></div>
<?php
echo form_label ( 'Prefix:', 'entityPrefix', array (
		'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
) );

if ($view_mode == EDIT_MODE) {
	echo form_input ( array (
			'id' => 'entityPrefix',
			'name' => 'entityPrefix',
			'class' => 'form-control',
			'required' => 'required',
			'pattern' => '[A-Z]{2}',
			'maxlength' => '2',
			'placeholder' => 'Prefix',
			'value' => ($entity_model->get ( 'entityPrefix' )) ? $entity_model->get ( 'entityPrefix' ) : '' 
	) );
} else {
	echo text ( $entity_model->get ( 'entityPrefix' ) );
}
?>

                                    </div>
									<div class="col-lg-3 input_field_sections">
<?php
echo form_label ( 'Description:', 'description', array (
		'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
) );

if ($view_mode == EDIT_MODE) {
	echo form_textarea ( array (
			'id' => 'description',
			'name' => 'description',
			'class' => 'form-control',
			'rows' => '1',
			'required' => 'required',
			'value' => ($entity_model->get ( 'description' )) ? $entity_model->get ( 'description' ) : '' 
	) );
} else {
	echo form_textarea ( array (
			'id' => 'description',
			'name' => 'description',
			'class' => 'form-control',
			'rows' => '1',
			'readonly' => 'readonly',
			'value' => ($entity_model->get ( 'description' )) ? $entity_model->get ( 'description' ) : '' 
	) );
}
?>
                                    </div>
									<div class="col-lg-3 input_field_sections">
<?php
echo form_label ( 'Meta Keyword:', 'metaKeyword', array (
		'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
) );

if ($view_mode == EDIT_MODE) {
	echo form_textarea ( array (
			'id' => 'metaKeyword',
			'name' => 'metaKeyword',
			'class' => 'form-control',
			'rows' => '1',
			'required' => 'required',
			'value' => ($entity_model->get ( 'metaKeyword' )) ? $entity_model->get ( 'metaKeyword' ) : '' 
	) );
} else {
	echo form_textarea ( array (
			'id' => 'metaKeyword',
			'name' => 'metaKeyword',
			'class' => 'form-control',
			'rows' => '1',
			'readonly' => 'readonly',
			'value' => ($entity_model->get ( 'metaKeyword' )) ? $entity_model->get ( 'metaKeyword' ) : '' 
	) );
}
?>
                                    </div>

								</div>

								<div class="row">
									<div class="col-lg-3 input_field_sections">

<?php
echo form_label ( 'Tag Line:', 'tagLine', array (
		'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
) );
if ($view_mode == EDIT_MODE) {
	echo form_input ( array (
			'id' => 'tagLine',
			'name' => 'tagLine',
			'class' => 'form-control',
			'required' => 'required',
			'placeholder' => 'Enter tagLine',
			'value' => ($entity_model->get ( 'tagLine' )) ? $entity_model->get ( 'tagLine' ) : '' 
	) );
} else {
	echo text ( $entity_model->get ( 'tagLine' ) );
}
?>

                                    </div>
									<div class="col-lg-3 input_field_sections">
<?php
echo form_label ( 'Email:', 'email', array (
		'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
) );

if ($view_mode == EDIT_MODE) {
	echo form_input ( array (
			'id' => 'email',
			'name' => 'email',
			'class' => 'form-control',
			'required' => 'required',
			'pattern' => '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$',
			'placeholder' => 'Email Id',
			'value' => ($entity_model->get ( 'email' )) ? $entity_model->get ( 'email' ) : '' 
	) );
} else {
	echo text ( $entity_model->get ( 'email' ) );
}
?>
                                    </div>
									<div class="col-lg-3 input_field_sections">
<?php
echo form_label ( 'Phone:', 'phone', array (
		'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
) );

if ($view_mode == EDIT_MODE) {
	echo form_input ( array (
			'id' => 'phone',
			'name' => 'phone',
			'class' => 'form-control',
			'required' => 'required',
			
			// 'pattern' => '[789]\d{9}',
			'placeholder' => 'Phone No',
			'value' => ($entity_model->get ( 'phone' )) ? $entity_model->get ( 'phone' ) : '' 
	) );
} else {
	echo text ( $entity_model->get ( 'phone' ) );
}
?>
                                    </div>
									<div class="col-lg-3 input_field_sections">
                                        <?php
																																								echo form_label ( 'Website URL:', 'websiteUrl', array (
																																										'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																																								) );
																																								
																																								if ($view_mode == EDIT_MODE) {
																																									echo form_input ( array (
																																											'id' => 'websiteUrl',
																																											'name' => 'websiteUrl',
																																											'class' => 'form-control',
																																											
																																											// 'required' => 'required',
																																											// 'pattern' => '[789]\d{9}',
																																											'placeholder' => 'Website URL',
																																											'value' => ($entity_model->get ( 'websiteUrl' )) ? $entity_model->get ( 'websiteUrl' ) : '' 
																																									) );
																																								} else {
																																									echo text ( $entity_model->get ( 'websiteUrl' ) );
																																								}
																																								?>
                                    </div>


								</div>
								<div class="row">
									<div class="col-lg-3 input_field_sections">
                                        <?php
																																								echo form_label ( 'Address:', 'address', array (
																																										'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																																								) );
																																								
																																								if ($view_mode == EDIT_MODE) {
																																									echo form_textarea ( array (
																																											'id' => 'address',
																																											'name' => 'address',
																																											'class' => 'form-control',
																																											'rows' => '2',
																																											'required' => 'required',
																																											'value' => ($entity_model->get ( 'address' )) ? $entity_model->get ( 'address' ) : '' 
																																									) );
																																								} else {
																																									echo form_textarea ( array (
																																											'id' => 'address',
																																											'name' => 'address',
																																											'class' => 'form-control',
																																											'rows' => '2',
																																											'readonly' => 'readonly',
																																											'value' => ($entity_model->get ( 'address' )) ? $entity_model->get ( 'address' ) : '' 
																																									) );
																																								}
																																								?>
                                    </div>
									<div class="col-lg-3 input_field_sections">
<?php
echo form_label ( 'Emergency Email:', 'emergencyEmail', array (
		'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
) );

if ($view_mode == EDIT_MODE) {
	echo form_input ( array (
			'id' => 'emergencyEmail',
			'name' => 'emergencyEmail',
			'class' => 'form-control',
			
			// 'required' => 'required',
			// 'pattern' => '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$',
			'placeholder' => 'Email Id',
			'value' => ($entity_model->get ( 'emergencyEmail' )) ? $entity_model->get ( 'emergencyEmail' ) : '' 
	) );
} else {
	echo text ( $entity_model->get ( 'emergencyEmail' ) );
}
?>
                                    </div>
									<div class="col-lg-3 input_field_sections">
                                        <?php
																																								echo form_label ( 'Emergency Phone:', 'emergencyPhone', array (
																																										'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																																								) );
																																								
																																								if ($view_mode == EDIT_MODE) {
																																									echo form_input ( array (
																																											'id' => 'emergencyPhone',
																																											'name' => 'emergencyPhone',
																																											'class' => 'form-control',
																																											
																																											// 'required' => 'required',
																																											// 'pattern' => '[789]\d{9}',
																																											'placeholder' => 'Phone No',
																																											'value' => ($entity_model->get ( 'emergencyPhone' )) ? $entity_model->get ( 'emergencyPhone' ) : '' 
																																									) );
																																								} else {
																																									echo text ( $entity_model->get ( 'emergencyPhone' ) );
																																								}
																																								?>
                                    </div>
									<div class="col-lg-3 input_field_sections">
                                        <?php
																																								echo form_label ( 'Toll Free Number:', 'tollFreeNo', array (
																																										'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																																								) );
																																								
																																								if ($view_mode == EDIT_MODE) {
																																									echo form_input ( array (
																																											'id' => 'tollFreeNo',
																																											'name' => 'tollFreeNo',
																																											'class' => 'form-control',
																																											
																																											// 'required' => 'required',
																																											// 'pattern' => '[789]\d{9}',
																																											'placeholder' => 'Tool Free Number',
																																											'value' => ($entity_model->get ( 'tollFreeNo' )) ? $entity_model->get ( 'tollFreeNo' ) : '' 
																																									) );
																																								} else {
																																									echo text ( $entity_model->get ( 'tollFreeNo' ) );
																																								}
																																								?>
                                    </div>




								</div>
								<div class="row">
									<div class="col-lg-3 input_field_sections">
                                        <?php
																																								echo form_label ( 'Copy Rights:', 'copyRights', array (
																																										'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																																								) );
																																								
																																								if ($view_mode == EDIT_MODE) {
																																									echo form_input ( array (
																																											'id' => 'copyRights',
																																											'name' => 'copyRights',
																																											'class' => 'form-control',
																																											
																																											// 'required' => 'required',
																																											// 'pattern' => '[789]\d{9}',
																																											'placeholder' => 'Phone No',
																																											'value' => ($entity_model->get ( 'copyRights' )) ? $entity_model->get ( 'copyRights' ) : '' 
																																									) );
																																								} else {
																																									echo text ( $entity_model->get ( 'copyRights' ) );
																																								}
																																								?>
                                    </div>
									<div class="col-lg-3 input_field_sections">

<?php
echo form_label ( 'Entity Type:', 'entityType', array (
		'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
) );
// validation for passenger first name
if ($view_mode == EDIT_MODE) {
	echo form_dropdown ( 'entityType', $entity_type_list, $entity_model->get ( 'entityType' ), array (
			'id' => 'entityType',
			'class' => 'form-control',
			'required' => 'required' 
	) );
} else {
	echo text ( $entity_type_list [$entity_model->get ( 'entityType' )] );
}
?>

                                    </div>
									<div class="col-lg-3 input_field_sections">

<?php
echo form_label ( 'Zone Name:', 'zoneId', array (
		'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
) );
// validation for passenger first name
if ($view_mode == EDIT_MODE) {
	echo form_dropdown ( 'zoneId', $zone_list, $entity_model->get ( 'zoneId' ), array (
			'id' => 'zoneId',
			'class' => 'form-control',
			'required' => 'required' 
	) );
} else {
	echo text ( $zone_list [$entity_model->get ( 'zoneId' )] );
}
?>

                                    </div>

									<div class="col-lg-3 input_field_sections">

<?php
echo form_label ( 'Country Name:', 'countryId', array (
		'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
) );
// validation for passenger first name
if ($view_mode == EDIT_MODE) {
	echo form_dropdown ( 'countryId', $country_list, $entity_model->get ( 'countryId' ), array (
			'id' => 'countryId',
			'class' => 'form-control',
			'required' => 'required' 
	) );
} else {
	echo text ( $country_list [$entity_model->get ( 'countryId' )] );
}
?>

                                    </div>
								</div>

								<div class="row">
									<div class="col-md-3 input_field_sections nowarp">
										<div class="fileinput fileinput-new" data-provides="fileinput"
											style="width: 100%">

                                                 <?php
																																																	echo form_label ( 'Logo:', 'logo', array (
																																																			'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																																																	) );
																																																	echo '<br>';
																																																	if ($view_mode == EDIT_MODE) {
																																																		?>
                                                <div>
												<span class="btn default btn-file" style="width: 53%;"> <span
													class="fileinput-new"> Select image </span> <span
													class="fileinput-exists"> Change </span>
                                                        <?php
																																																		echo form_upload ( array (
																																																				'id' => 'logo',
																																																				'name' => 'logo[]',
																																																				'class' => 'form-control-file',
																																																				
																																																				// 'required' => 'required',
																																																				'accept' => 'image/*',
																																																				'placeholder' => 'License Image',
																																																				'value' => ($entity_model->get ( 'logo' )) ? entity_content_url ( $entity_model->get ( 'id' ) . '/' . $entity_model->get ( 'logo' ) ) : '' 
																																																		) );
																																																		?>  

                                                    </span> <a
													href="javascript:;" class="btn red fileinput-exists"
													data-dismiss="fileinput"> <i class="fa fa-trash-o"></i>
												</a>

												<div class="fileinput-new thumbnail"
													style="width: 60px; height: 60px;">
													<a
														href="<?php
																																																		
echo ($entity_model->get ( 'logo' )) ? entity_content_url ( $entity_model->get ( 'id' ) . '/' . $entity_model->get ( 'logo' ) ) : image_url ( 'app/user.png' );
																																																		?>"
														data-imagelightbox="demo" data-ilb2-caption="Logo"> <img
														src="<?php
																																																		
echo ($entity_model->get ( 'logo' )) ? entity_content_url ( $entity_model->get ( 'id' ) . '/' . $entity_model->get ( 'logo' ) ) : image_url ( 'app/no_image.png' );
																																																		?>"
														alt="">
													</a>
												</div>
												<div class="fileinput-preview fileinput-exists thumbnail"
													style="max-width: 60px; max-height: 60px;"></div>

											</div>
                                            <?php } else { ?>    
                                            <?php //echo text($entity_model->get('logo')); ?>
                                                <div
												class="fileinput-new thumbnail"
												style="width: 60px; height: 60px;">
												<a
													href="<?php
																																																		
echo ($entity_model->get ( 'logo' )) ? entity_content_url ( $entity_model->get ( 'id' ) . '/' . $entity_model->get ( 'logo' ) ) : image_url ( 'app/user.png' );
																																																		?>"
													data-imagelightbox="demo" data-ilb2-caption="Logo"> <img
													src="<?php
																																																		
echo ($entity_model->get ( 'logo' )) ? entity_content_url ( $entity_model->get ( 'id' ) . '/' . $entity_model->get ( 'logo' ) ) : image_url ( 'app/no_image.png' );
																																																		?>"
													alt="">
												</a>
											</div>
                                        <?php } ?>
                                            </div>
									</div>

									<div class="col-md-3 input_field_sections nowarp">
										<div class="fileinput fileinput-new" data-provides="fileinput"
											style="width: 100%">

                                                 <?php
																																																	echo form_label ( 'Email Logo:', 'emailLogo', array (
																																																			'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																																																	) );
																																																	echo '<br>';
																																																	if ($view_mode == EDIT_MODE) {
																																																		?>
                                                <div>
												<span class="btn default btn-file" style="width: 53%;"> <span
													class="fileinput-new"> Select image </span> <span
													class="fileinput-exists"> Change </span>
                                                <?php
																																																		echo form_upload ( array (
																																																				'id' => 'emailLogo',
																																																				'name' => 'emailLogo[]',
																																																				'class' => 'form-control-file',
																																																				
																																																				// 'required' => 'required',
																																																				'accept' => 'image/*',
																																																				'placeholder' => 'Email Logo Image',
																																																				'value' => ($entity_model->get ( 'emailLogo' )) ? entity_content_url ( $entity_model->get ( 'id' ) . '/' . $entity_model->get ( 'emailLogo' ) ) : '' 
																																																		) );
																																																		?>  

                                                    </span> <a
													href="javascript:;" class="btn red fileinput-exists"
													data-dismiss="fileinput"> <i class="fa fa-trash-o"></i>
												</a>

												<div class="fileinput-new thumbnail"
													style="width: 60px; height: 60px;">
													<a
														href="<?php
																																																		
echo ($entity_model->get ( 'emailLogo' )) ? entity_content_url ( $entity_model->get ( 'id' ) . '/' . $entity_model->get ( 'emailLogo' ) ) : image_url ( 'app/user.png' );
																																																		?>"
														data-imagelightbox="demo" data-ilb2-caption="Email Logo">
														<img
														src="<?php
																																																		
echo ($entity_model->get ( 'emailLogo' )) ? entity_content_url ( $entity_model->get ( 'id' ) . '/' . $entity_model->get ( 'emailLogo' ) ) : image_url ( 'app/no_image.png' );
																																																		?>"
														alt="">
													</a>
												</div>
												<div class="fileinput-preview fileinput-exists thumbnail"
													style="max-width: 60px; max-height: 60px;"></div>

											</div>
                                                        <?php } else { ?>    
                                            <?php //echo text($entity_model->get('emailLogo')); ?>
                                                    <div
												class="fileinput-new thumbnail"
												style="width: 60px; height: 60px;">
												<a
													href="<?php
																																																		
echo ($entity_model->get ( 'emailLogo' )) ? entity_content_url ( $entity_model->get ( 'id' ) . '/' . $entity_model->get ( 'emailLogo' ) ) : image_url ( 'app/user.png' );
																																																		?>"
													data-imagelightbox="demo" data-ilb2-caption="Email Logo"> <img
													src="<?php
																																																		
echo ($entity_model->get ( 'emailLogo' )) ? entity_content_url ( $entity_model->get ( 'id' ) . '/' . $entity_model->get ( 'emailLogo' ) ) : image_url ( 'app/no_image.png' );
																																																		?>"
													alt="">
												</a>
											</div>
                                        <?php } ?>
                                            </div>
									</div>


									<div class="col-md-3 input_field_sections nowarp">
										<div class="fileinput fileinput-new" data-provides="fileinput"
											style="width: 100%">

                                                 <?php
																																																	echo form_label ( 'Fav icon:', 'favicon', array (
																																																			'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																																																	) );
																																																	echo '<br>';
																																																	if ($view_mode == EDIT_MODE) {
																																																		?>
                                                <div>
												<span class="btn default btn-file" style="width: 53%;"> <span
													class="fileinput-new"> Select image </span> <span
													class="fileinput-exists"> Change </span>
                                                <?php
																																																		echo form_upload ( array (
																																																				'id' => 'favicon',
																																																				'name' => 'favicon[]',
																																																				'class' => 'form-control-file',
																																																				
																																																				// 'required' => 'required',
																																																				'accept' => 'image/*',
																																																				'placeholder' => 'Favicon Image',
																																																				'value' => ($entity_model->get ( 'favicon' )) ? entity_content_url ( $entity_model->get ( 'id' ) . '/' . $entity_model->get ( 'favicon' ) ) : '' 
																																																		) );
																																																		?>  

                                                    </span> <a
													href="javascript:;" class="btn red fileinput-exists"
													data-dismiss="fileinput"> <i class="fa fa-trash-o"></i>
												</a>

												<div class="fileinput-new thumbnail"
													style="width: 60px; height: 60px;">
													<a
														href="<?php
																																																		
echo ($entity_model->get ( 'favicon' )) ? entity_content_url ( $entity_model->get ( 'id' ) . '/' . $entity_model->get ( 'favicon' ) ) : image_url ( 'app/user.png' );
																																																		?>"
														data-imagelightbox="demo" data-ilb2-caption="Favicon"> <img
														src="<?php
																																																		
echo ($entity_model->get ( 'favicon' )) ? entity_content_url ( $entity_model->get ( 'id' ) . '/' . $entity_model->get ( 'favicon' ) ) : image_url ( 'app/no_image.png' );
																																																		?>"
														alt="">
													</a>
												</div>
												<div class="fileinput-preview fileinput-exists thumbnail"
													style="max-width: 60px; max-height: 60px;"></div>

											</div>
                                                        <?php } else { ?>    
                                                            <?php //echo text($entity_model->get('favicon')); ?>
                                            <div
												class="fileinput-new thumbnail"
												style="width: 60px; height: 60px;">
												<a
													href="<?php
																																																		
echo ($entity_model->get ( 'favicon' )) ? entity_content_url ( $entity_model->get ( 'id' ) . '/' . $entity_model->get ( 'favicon' ) ) : image_url ( 'app/user.png' );
																																																		?>"
													data-imagelightbox="demo" data-ilb2-caption="Favicon"> <img
													src="<?php
																																																		
echo ($entity_model->get ( 'favicon' )) ? entity_content_url ( $entity_model->get ( 'id' ) . '/' . $entity_model->get ( 'favicon' ) ) : image_url ( 'app/no_image.png' );
																																																		?>"
													alt="">
												</a>
											</div>
                                        <?php } ?>
                                            </div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-12 m-t-35">
						<div class="portlet-body">
							<div class="form-body gp-form">
								<div class="card-header bg-white">
									<h1>Entity Configuration Details</h1>
								</div>

								<div class="row">
									<div class="col-lg-3 input_field_sections">
<?php
echo form_label ( 'Tax in %:', 'tax', array (
		'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
) );

if ($view_mode == EDIT_MODE) {
	echo form_input ( array (
			'id' => 'tax',
			'name' => 'tax',
			'class' => 'form-control',
			
			// 'required' => 'required',
			'placeholder' => 'tax',
			'value' => ($entity_config_model->get ( 'tax' )) ? $entity_config_model->get ( 'tax' ) : '' 
	) );
} else {
	echo text ( $entity_config_model->get ( 'tax' ) );
}
?>
                                    </div>
									<div class="col-lg-3 input_field_sections">
                                        <?php
																																								echo form_label ( 'Admin Commission in %:', 'adminCommission', array (
																																										'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																																								) );
																																								
																																								if ($view_mode == EDIT_MODE) {
																																									echo form_input ( array (
																																											'id' => 'adminCommission',
																																											'name' => 'adminCommission',
																																											'class' => 'form-control',
																																											'required' => 'required',
																																											'pattern' => '^\d{1,2}([\.]?(\d{1,2}))$',
																																											'maxlength' => '5',
																																											'placeholder' => 'Admin Commission in Percentage',
																																											'value' => ($entity_config_model->get ( 'adminCommission' )) ? $entity_config_model->get ( 'adminCommission' ) : '' 
																																									) );
																																								} else {
																																									echo text ( $entity_config_model->get ( 'adminCommission' ) );
																																								}
																																								?>
                                    </div>
									<div class="col-lg-3 input_field_sections">
                                        <?php
																																								echo form_label ( 'Passenger Referral Discount:', 'passengerReferralDiscount', array (
																																										'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																																								) );
																																								
																																								if ($view_mode == EDIT_MODE) {
																																									echo form_input ( array (
																																											'id' => 'passengerReferralDiscount',
																																											'name' => 'passengerReferralDiscount',
																																											'class' => 'form-control',
																																											
																																											// 'required' => 'required',
																																											'placeholder' => 'Passenger Referral Discount',
																																											'value' => ($entity_config_model->get ( 'passengerReferralDiscount' )) ? $entity_config_model->get ( 'passengerReferralDiscount' ) : '' 
																																									) );
																																								} else {
																																									echo text ( $entity_config_model->get ( 'passengerReferralDiscount' ) );
																																								}
																																								?>
                                    </div>
									<div class="col-lg-3 input_field_sections">

                                        <?php
																																								echo form_label ( 'Driver Referral Discount:', 'driverReferralDiscount', array (
																																										'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																																								) );
																																								if ($view_mode == EDIT_MODE) {
																																									echo form_input ( array (
																																											'id' => 'driverReferralDiscount',
																																											'name' => 'driverReferralDiscount',
																																											'class' => 'form-control',
																																											
																																											// 'required' => 'required',
																																											'placeholder' => 'Driver Referral Discount',
																																											'value' => ($entity_config_model->get ( 'driverReferralDiscount' )) ? $entity_config_model->get ( 'driverReferralDiscount' ) : '' 
																																									) );
																																								} else {
																																									echo text ( $entity_config_model->get ( 'driverReferralDiscount' ) );
																																								}
																																								?>

                                    </div>
								</div>
								<div class="row">


									<div class="col-lg-3 input_field_sections">

                                        <?php
																																								echo form_label ( 'Passenger Rejection Charge:', 'passengerRejectionCharge', array (
																																										'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																																								) );
																																								if ($view_mode == EDIT_MODE) {
																																									echo form_input ( array (
																																											'id' => 'passengerRejectionCharge',
																																											'name' => 'passengerRejectionCharge',
																																											'class' => 'form-control',
																																											
																																											// 'required' => 'required',
																																											'placeholder' => 'Passenger Rejection Charge',
																																											'value' => ($entity_config_model->get ( 'passengerRejectionCharge' )) ? $entity_config_model->get ( 'passengerRejectionCharge' ) : '' 
																																									) );
																																								} else {
																																									echo text ( $entity_config_model->get ( 'passengerRejectionCharge' ) );
																																								}
																																								?>

                                    </div>
									<div class="col-lg-3 input_field_sections">
                                        <?php
																																								echo form_label ( 'Driver Min Wallet:', 'driverMinWallet', array (
																																										'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																																								) );
																																								if ($view_mode == EDIT_MODE) {
																																									echo form_input ( array (
																																											'id' => 'driverMinWallet',
																																											'name' => 'driverMinWallet',
																																											'class' => 'form-control',
																																											'required' => 'required',
																																											'pattern' => '^([+-]?([0-9]{0,7}([\.]?(\d{1,2}))))$',
																																											'maxlength' => '11',
																																											'placeholder' => 'Driver Min Wallet',
																																											'value' => ($entity_config_model->get ( 'driverMinWallet' )) ? $entity_config_model->get ( 'driverMinWallet' ) : '' 
																																									) );
																																								} else {
																																									echo text ( $entity_config_model->get ( 'driverMinWallet' ) );
																																								}
																																								?>
                                    </div>
									<div class="col-lg-3 input_field_sections">
                                        <?php
																																								echo form_label ( 'Driver Max Wallet:', 'driverMaxWallet', array (
																																										'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																																								) );
																																								
																																								if ($view_mode == EDIT_MODE) {
																																									echo form_input ( array (
																																											'id' => 'driverMaxWallet',
																																											'name' => 'driverMaxWallet',
																																											'class' => 'form-control',
																																											'required' => 'required',
																																											'pattern' => '^([+-]?([0-9]{0,7}([\.]?(\d{1,2}))))$',
																																											'maxlength' => '10',
																																											'placeholder' => 'Driver Max Wallet',
																																											'value' => ($entity_config_model->get ( 'driverMaxWallet' )) ? $entity_config_model->get ( 'driverMaxWallet' ) : '' 
																																									) );
																																								} else {
																																									echo text ( $entity_config_model->get ( 'driverMaxWallet' ) );
																																								}
																																								?>
                                    </div>
									<div class="col-lg-3 input_field_sections">
                                        <?php
																																								echo form_label ( 'Driver Security Amount:', 'driverSecurityAmount', array (
																																										'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																																								) );
																																								
																																								if ($view_mode == EDIT_MODE) {
																																									echo form_input ( array (
																																											'id' => 'driverSecurityAmount',
																																											'name' => 'driverSecurityAmount',
																																											'class' => 'form-control',
																																											'required' => 'required',
																																											'pattern' => '^[1-9]\d{0,6}([\.]?(\d{1,2}))$',
																																											'maxlength' => '10',
																																											'placeholder' => 'Driver Security Amount',
																																											'value' => ($entity_config_model->get ( 'driverSecurityAmount' )) ? $entity_config_model->get ( 'driverSecurityAmount' ) : '' 
																																									) );
																																								} else {
																																									echo text ( $entity_config_model->get ( 'driverSecurityAmount' ) );
																																								}
																																								?>
                                    </div>
								</div>
								<div class="row">


									<div class="col-lg-3 input_field_sections">
                                        <?php
																																								echo form_label ( 'Admin Booking Charge:', 'adminBookingCharge', array (
																																										'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																																								) );
																																								
																																								if ($view_mode == EDIT_MODE) {
																																									echo form_input ( array (
																																											'id' => 'adminBookingCharge',
																																											'name' => 'adminBookingCharge',
																																											'class' => 'form-control',
																																											'required' => 'required',
																																											'pattern' => '^[1-9]\d{0,6}([\.]?(\d{1,2}))$',
																																											'maxlength' => '10',
																																											'placeholder' => 'Admin Booking Charge',
																																											'value' => ($entity_config_model->get ( 'adminBookingCharge' )) ? $entity_config_model->get ( 'adminBookingCharge' ) : '' 
																																									) );
																																								} else {
																																									echo text ( $entity_config_model->get ( 'adminBookingCharge' ) );
																																								}
																																								?>
                                    </div>
									<div class="col-lg-3 input_field_sections">
                                        <?php
																																								echo form_label ( 'DRT Promocode Amount:', 'drtPromoAmount', array (
																																										'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																																								) );
																																								
																																								if ($view_mode == EDIT_MODE) {
																																									echo form_input ( array (
																																											'id' => 'drtPromoAmount',
																																											'name' => 'drtPromoAmount',
																																											'class' => 'form-control',
																																											'required' => 'required',
																																											'pattern' => '^[1-9]\d{0,6}([\.]?(\d{1,2}))$',
																																											'maxlength' => '10',
																																											'placeholder' => 'DRT Promocode Amount',
																																											'value' => ($entity_config_model->get ( 'drtPromoAmount' )) ? $entity_config_model->get ( 'drtPromoAmount' ) : '' 
																																									) );
																																								} else {
																																									echo text ( $entity_config_model->get ( 'drtPromoAmount' ) );
																																								}
																																								?>
                                    </div>
									<div class="col-lg-3 input_field_sections">
                                        <?php
																																								echo form_label ( 'DRT Promocode Days Limit:', 'drtPromoDaysLimit', array (
																																										'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																																								) );
																																								
																																								if ($view_mode == EDIT_MODE) {
																																									echo form_input ( array (
																																											'id' => 'drtPromoDaysLimit',
																																											'name' => 'drtPromoDaysLimit',
																																											'class' => 'form-control',
																																											'required' => 'required',
																																											'pattern' => '\d{1,2}',
																																											'placeholder' => 'DRT Promocode Days Limit',
																																											'value' => ($entity_config_model->get ( 'drtPromoDaysLimit' )) ? $entity_config_model->get ( 'drtPromoDaysLimit' ) : '' 
																																									) );
																																								} else {
																																									echo text ( $entity_config_model->get ( 'drtPromoDaysLimit' ) );
																																								}
																																								?>
                                    </div>
									<div class="col-lg-3 input_field_sections">
                                        <?php
																																								echo form_label ( 'DRT First Limit:', 'drtFirstLimit', array (
																																										'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																																								) );
																																								
																																								if ($view_mode == EDIT_MODE) {
																																									echo form_input ( array (
																																											'id' => 'drtFirstLimit',
																																											'name' => 'drtFirstLimit',
																																											'class' => 'form-control',
																																											'required' => 'required',
																																											'pattern' => '\d{1,2}',
																																											'placeholder' => 'DRT First Limit',
																																											'value' => ($entity_config_model->get ( 'drtFirstLimit' )) ? $entity_config_model->get ( 'drtFirstLimit' ) : '' 
																																									) );
																																								} else {
																																									echo text ( $entity_config_model->get ( 'drtFirstLimit' ) );
																																								}
																																								?>
                                    </div>
								</div>
								<div class="row">
									<div class="col-lg-3 input_field_sections">
                                        <?php
																																								echo form_label ( 'DRT First Limit Penalty:', 'drtFirstlLimitCharge', array (
																																										'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																																								) );
																																								
																																								if ($view_mode == EDIT_MODE) {
																																									echo form_input ( array (
																																											'id' => 'drtFirstlLimitCharge',
																																											'name' => 'drtFirstlLimitCharge',
																																											'class' => 'form-control',
																																											'required' => 'required',
																																											'pattern' => '^[1-9]\d{0,6}([\.]?(\d{1,2}))$',
																																											'maxlength' => '10',
																																											'placeholder' => 'DRT First Limit Penalty',
																																											'value' => ($entity_config_model->get ( 'drtFirstlLimitCharge' )) ? $entity_config_model->get ( 'drtFirstlLimitCharge' ) : '' 
																																									) );
																																								} else {
																																									echo text ( $entity_config_model->get ( 'drtFirstlLimitCharge' ) );
																																								}
																																								?>
                                    </div>
									<div class="col-lg-3 input_field_sections">
                                        <?php
																																								echo form_label ( 'DRT Second Limit:', 'drtSecondLimit', array (
																																										'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																																								) );
																																								
																																								if ($view_mode == EDIT_MODE) {
																																									echo form_input ( array (
																																											'id' => 'drtSecondLimit',
																																											'name' => 'drtSecondLimit',
																																											'class' => 'form-control',
																																											'required' => 'required',
																																											'pattern' => '\d{1,2}',
																																											'placeholder' => 'DRT Second Limit',
																																											'value' => ($entity_config_model->get ( 'drtSecondLimit' )) ? $entity_config_model->get ( 'drtSecondLimit' ) : '' 
																																									) );
																																								} else {
																																									echo text ( $entity_config_model->get ( 'drtSecondLimit' ) );
																																								}
																																								?>
                                    </div>
								</div>
								<div class="row">
									<div class="col-lg-3 input_field_sections">
                                        <?php
																																								echo form_label ( 'Passenger Android App Url:', 'passengerAndroidAppUrl', array (
																																										'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																																								) );
																																								
																																								if ($view_mode == EDIT_MODE) {
																																									echo form_input ( array (
																																											'id' => 'passengerAndroidAppUrl',
																																											'name' => 'passengerAndroidAppUrl',
																																											'class' => 'form-control',
																																											
																																											// 'required' => 'required',
																																											'placeholder' => 'Passenger Android App Url',
																																											'value' => ($entity_config_model->get ( 'passengerAndroidAppUrl' )) ? $entity_config_model->get ( 'passengerAndroidAppUrl' ) : '' 
																																									) );
																																								} else {
																																									echo text ( $entity_config_model->get ( 'passengerAndroidAppUrl' ) );
																																								}
																																								?>
                                    </div>
									<div class="col-lg-3 input_field_sections">
                                        <?php
																																								echo form_label ( 'Passenger Android App Ver:', 'passengerAndroidAppVer', array (
																																										'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																																								) );
																																								
																																								if ($view_mode == EDIT_MODE) {
																																									echo form_input ( array (
																																											'id' => 'passengerAndroidAppVer',
																																											'name' => 'passengerAndroidAppVer',
																																											'class' => 'form-control',
																																											
																																											// 'required' => 'required',
																																											'placeholder' => 'Passenger Android App Ver',
																																											'value' => ($entity_config_model->get ( 'passengerAndroidAppVer' )) ? $entity_config_model->get ( 'passengerAndroidAppVer' ) : '' 
																																									) );
																																								} else {
																																									echo text ( $entity_config_model->get ( 'passengerAndroidAppVer' ) );
																																								}
																																								?>
                                    </div>
									<div class="col-lg-3 input_field_sections">
                                        <?php
																																								echo form_label ( 'Passenger Iphone App Url:', 'passengerIphoneAppUrl', array (
																																										'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																																								) );
																																								
																																								if ($view_mode == EDIT_MODE) {
																																									echo form_input ( array (
																																											'id' => 'passengerIphoneAppUrl',
																																											'name' => 'passengerIphoneAppUrl',
																																											'class' => 'form-control',
																																											
																																											// 'required' => 'required',
																																											'placeholder' => 'Passenger Iphone App Url',
																																											'value' => ($entity_config_model->get ( 'passengerIphoneAppUrl' )) ? $entity_config_model->get ( 'passengerIphoneAppUrl' ) : '' 
																																									) );
																																								} else {
																																									echo text ( $entity_config_model->get ( 'passengerIphoneAppUrl' ) );
																																								}
																																								?>
                                    </div>
									<div class="col-lg-3 input_field_sections">
                                        <?php
																																								echo form_label ( 'Passenger Iphone App Ver:', 'passengerIphoneAppVer', array (
																																										'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																																								) );
																																								
																																								if ($view_mode == EDIT_MODE) {
																																									echo form_input ( array (
																																											'id' => 'passengerIphoneAppVer',
																																											'name' => 'passengerIphoneAppVer',
																																											'class' => 'form-control',
																																											
																																											// 'required' => 'required',
																																											'placeholder' => 'Passenger Iphone App Ver',
																																											'value' => ($entity_config_model->get ( 'passengerIphoneAppVer' )) ? $entity_config_model->get ( 'passengerIphoneAppVer' ) : '' 
																																									) );
																																								} else {
																																									echo text ( $entity_config_model->get ( 'passengerIphoneAppVer' ) );
																																								}
																																								?>
                                    </div>
								</div>
								<div class="row">
									<div class="col-lg-3 input_field_sections">
                                        <?php
																																								echo form_label ( 'Driver Android App URL:', 'driverAndroidAppUrl', array (
																																										'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																																								) );
																																								
																																								if ($view_mode == EDIT_MODE) {
																																									echo form_input ( array (
																																											'id' => 'driverAndroidAppUrl',
																																											'name' => 'driverAndroidAppUrl',
																																											'class' => 'form-control',
																																											
																																											// 'required' => 'required',
																																											'placeholder' => 'Driver Android App URL',
																																											'value' => ($entity_config_model->get ( 'driverAndroidAppUrl' )) ? $entity_config_model->get ( 'driverAndroidAppUrl' ) : '' 
																																									) );
																																								} else {
																																									echo text ( $entity_config_model->get ( 'driverAndroidAppUrl' ) );
																																								}
																																								?>
                                    </div>
									<div class="col-lg-3 input_field_sections">
                                        <?php
																																								echo form_label ( 'Driver Android App Ver:', 'driverAndroidAppVer', array (
																																										'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																																								) );
																																								
																																								if ($view_mode == EDIT_MODE) {
																																									echo form_input ( array (
																																											'id' => 'driverAndroidAppVer',
																																											'name' => 'driverAndroidAppVer',
																																											'class' => 'form-control',
																																											
																																											// 'required' => 'required',
																																											'placeholder' => 'Driver Android App Ver',
																																											'value' => ($entity_config_model->get ( 'driverAndroidAppVer' )) ? $entity_config_model->get ( 'driverAndroidAppVer' ) : '' 
																																									) );
																																								} else {
																																									echo text ( $entity_config_model->get ( 'driverAndroidAppVer' ) );
																																								}
																																								?>
                                    </div>
									<div class="col-lg-3 input_field_sections">
                                        <?php
																																								echo form_label ( 'Driver Iphone App URL:', 'driverIphoneAppUrl', array (
																																										'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																																								) );
																																								
																																								if ($view_mode == EDIT_MODE) {
																																									echo form_input ( array (
																																											'id' => 'driverIphoneAppUrl',
																																											'name' => 'driverIphoneAppUrl',
																																											'class' => 'form-control',
																																											
																																											// 'required' => 'required',
																																											'placeholder' => 'Driver Iphone App URL',
																																											'value' => ($entity_config_model->get ( 'driverIphoneAppUrl' )) ? $entity_config_model->get ( 'driverIphoneAppUrl' ) : '' 
																																									) );
																																								} else {
																																									echo text ( $entity_config_model->get ( 'driverIphoneAppUrl' ) );
																																								}
																																								?>
                                    </div>
									<div class="col-lg-3 input_field_sections">
                                        <?php
																																								echo form_label ( 'Driver Iphone App Ver:', 'driverIphoneAppVer', array (
																																										'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																																								) );
																																								
																																								if ($view_mode == EDIT_MODE) {
																																									echo form_input ( array (
																																											'id' => 'driverIphoneAppVer',
																																											'name' => 'driverIphoneAppVer',
																																											'class' => 'form-control',
																																											
																																											// 'required' => 'required',
																																											'placeholder' => 'Driver Iphone App Ver',
																																											'value' => ($entity_config_model->get ( 'driverIphoneAppVer' )) ? $entity_config_model->get ( 'driverIphoneAppVer' ) : '' 
																																									) );
																																								} else {
																																									echo text ( $entity_config_model->get ( 'driverIphoneAppVer' ) );
																																								}
																																								?>
                                    </div>
								</div>
								<div class="row">
									<div class="col-md-3 input_field_sections nowarp gp-nomar">
                                        <?php
																																								echo form_label ( 'Send SMS:', 'sendSms', array (
																																										'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																																								) );
																																								?>
                                        <div class="">
											<label class="mt-checkbox">
                                        <?php
																																								$is_checked = $entity_config_model->get ( 'sendSms' ) ? TRUE : FALSE;
																																								$checked = $entity_config_model->get ( 'sendSms' ) ? 1 : 0;
																																								if ($view_mode == EDIT_MODE) {
																																									echo form_checkbox ( array (
																																											'id' => 'sendSms',
																																											'name' => 'sendSms',
																																											'class' => 'flat' 
																																									), 
																																											// 'required' => 'required',
																																											$checked, $is_checked );
																																								} else {
																																									echo form_checkbox ( array (
																																											'id' => 'sendSms',
																																											'name' => 'sendSms',
																																											'class' => 'flat',
																																											'disabled' => 'disabled' 
																																									), 
																																											// 'required' => 'required',
																																											$checked, $is_checked );
																																								}
																																								?> Send SMS
                                                <span></span>
											</label>
										</div>
									</div>
									<div class="col-md-3 input_field_sections nowarp gp-nomar">
                                                <?php
																																																echo form_label ( 'Send Email:', 'sendEmail', array (
																																																		'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																																																) );
																																																?>
                                        <div class="">
											<label class="mt-checkbox">
                                                <?php
																																																$is_checked = $entity_config_model->get ( 'sendEmail' ) ? TRUE : FALSE;
																																																$checked = $entity_config_model->get ( 'sendEmail' ) ? 1 : 0;
																																																if ($view_mode == EDIT_MODE) {
																																																	echo form_checkbox ( array (
																																																			'id' => 'sendEmail',
																																																			'name' => 'sendEmail',
																																																			'class' => 'flat' 
																																																	), 
																																																			// 'required' => 'required',
																																																			$checked, $is_checked );
																																																} else {
																																																	echo form_checkbox ( array (
																																																			'id' => 'sendEmail',
																																																			'name' => 'sendEmail',
																																																			'class' => 'flat',
																																																			'disabled' => 'disabled' 
																																																	), 
																																																			// 'required' => 'required',
																																																			$checked, $is_checked );
																																																}
																																																?> Send Email
                                                <span></span>
											</label>
										</div>
									</div>
								</div>

							</div>
							<div class="form-actions" style="margin-top: 22px;">
								<div class="row">
									<div class="col-md-12">
                                                <?php if ($view_mode == EDIT_MODE) { ?>
                                            <div class="gp-cen">

											<button type="button" id="cancel-entity-btn"
												class="btn grey-salsa btn-outline">Cancel</button>
											<button type="button" id="save-entity-btn" class="btn gpblue">Save</button>
										</div>
                                                <?php } else { ?>
                                        <div class="gp-cen">
											<button type="button" id="cancel-entity-btn"
												class="btn btn-danger">Back</button>
										</div>
                                                <?php } ?>
                                    </div>
								</div>
							</div>

<?php
echo form_close ();
?>      
                        </div>
					</div>


				</div>



			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>



