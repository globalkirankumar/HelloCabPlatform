<style>
    .label.label-sm {
        font-size: 13px;
        padding: 2px 5px;
        cursor: pointer;
    }
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">

        <div class="page-bar ">
            <ul class="page-breadcrumb ">
                <li><a href="<?php echo base_url('Dashboard'); ?>">Home</a> <i
                        class="fa fa-circle"></i></li>
                <li><a href="<?php echo base_url('Entity/getEntityList'); ?>">Entity List</a></li>

            </ul>

        </div>
        <!---- Page Bar ends ---->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-comments"></i>Entity List
                        </div>

                    </div>

                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col-md-3 btn-group gp-pad1">
                                        <a href="<?php echo base_url('Entity/add'); ?>">
                                            <button id="sample_editable_1_new" class="btn sbold green1">
                                                Add New <i class="fa fa-plus"></i>
                                            </button>
                                        </a>
                                    </div>
                                    
                                    
                                </div>
                                
                            </div>
                        </div>
                        <div id="table-columns" class="hidden">

                            <label class="">
                                <?php
                                echo form_checkbox(array(
                                    'class' => 'flat toggle-vis',
                                    'data-column' => '3'
                                ));
                                ?>
                                Description
                            </label> 
                            <label class="">
                                <?php
                                echo form_checkbox(array(
                                    'class' => 'flat toggle-vis',
                                    'data-column' => '4'
                                ));
                                ?>
                                Meta Keyword
                            </label> 
                            <label class="">
                                <?php
                                echo form_checkbox(array(
                                    'class' => 'flat toggle-vis',
                                    'data-column' => '5'
                                ));
                                ?>
                                Tag Line
                            </label> 
                            <label class="">
                                <?php
                                echo form_checkbox(array(
                                    'class' => 'flat toggle-vis',
                                    'data-column' => '8'
                                ));
                                ?>
                               Website
                            </label> 
                            <label class="">
                                <?php
                                echo form_checkbox(array(
                                    'class' => 'flat toggle-vis',
                                    'data-column' => '10'
                                ));
                                ?>
                               Emergency Email
                            </label> 
                            <label class="">
                                <?php
                                echo form_checkbox(array(
                                    'class' => 'flat toggle-vis',
                                    'data-column' => '11'
                                ));
                                ?>
                               Emergency Phone
                            </label> 
                            <label class="">
                                <?php
                                echo form_checkbox(array(
                                    'class' => 'flat toggle-vis',
                                    'data-column' => '12'
                                ));
                                ?>
                               Entity Type
                            </label> 
                            <label class="">
                                <?php
                                echo form_checkbox(array(
                                    'class' => 'flat toggle-vis',
                                    'data-column' => '13'
                                ));
                                ?>
                               CopyRights
                            </label> 
                            <label class="">
                                <?php
                                echo form_checkbox(array(
                                    'class' => 'flat toggle-vis',
                                    'data-column' => '14'
                                ));
                                ?>
                               Toll Free No
                            </label> 
                            <label class="">
                                <?php
                                echo form_checkbox(array(
                                    'class' => 'flat toggle-vis',
                                    'data-column' => '17'
                                ));
                                ?>
                               Passenger Referal Discount
                            </label> 
                            <label class="">
                                <?php
                                echo form_checkbox(array(
                                    'class' => 'flat toggle-vis',
                                    'data-column' => '18'
                                ));
                                ?>
                               Driver Referal Discount
                            </label> 
                            <label class="">
                                <?php
                                echo form_checkbox(array(
                                    'class' => 'flat toggle-vis',
                                    'data-column' => '19'
                                ));
                                ?>
                               Passenger Rejection Charge
                            </label> 
                            <label class="">
                                <?php
                                echo form_checkbox(array(
                                    'class' => 'flat toggle-vis',
                                    'data-column' => '20'
                                ));
                                ?>
                               Driver Min Wallet
                            </label> 
                            <label class="">
                                <?php
                                echo form_checkbox(array(
                                    'class' => 'flat toggle-vis',
                                    'data-column' => '21'
                                ));
                                ?>
                               Driver Max Wallet
                            </label> 
                            <label class="">
                                <?php
                                echo form_checkbox(array(
                                    'class' => 'flat toggle-vis',
                                    'data-column' => '22'
                                ));
                                ?>
                               Driver Security Amount
                            </label> 
                            <label class="">
                                <?php
                                echo form_checkbox(array(
                                    'class' => 'flat toggle-vis',
                                    'data-column' => '23'
                                ));
                                ?>
                               DRT Promocode Amount
                            </label> 
                            <label class="">
                                <?php
                                echo form_checkbox(array(
                                    'class' => 'flat toggle-vis',
                                    'data-column' => '24'
                                ));
                                ?>
                               DRT Promocode Days Limit
                            </label> 
                            <label class="">
                                <?php
                                echo form_checkbox(array(
                                    'class' => 'flat toggle-vis',
                                    'data-column' => '25'
                                ));
                                ?>
                               DRT First Limit
                            </label>
                            <label class="">
                                <?php
                                echo form_checkbox(array(
                                    'class' => 'flat toggle-vis',
                                    'data-column' => '26'
                                ));
                                ?>
                               DRT First Limit Penalty
                            </label>
                            <label class="">
                                <?php
                                echo form_checkbox(array(
                                    'class' => 'flat toggle-vis',
                                    'data-column' => '27'
                                ));
                                ?>
                               DRT Second Limit
                            </label>
                            <label class="">
                                <?php
                                echo form_checkbox(array(
                                    'class' => 'flat toggle-vis',
                                    'data-column' => '28'
                                ));
                                ?>
                              Admin Booking Charge
                            </label> 
                            <label class="">
                                <?php
                                echo form_checkbox(array(
                                    'class' => 'flat toggle-vis',
                                    'data-column' => '29'
                                ));
                                ?>
                              Send Sms
                            </label> 
                            <label class="">
                                <?php
                                echo form_checkbox(array(
                                    'class' => 'flat toggle-vis',
                                    'data-column' => '30'
                                ));
                                ?>
                              Send Email
                            </label> 
                            <label class="">
                                <?php
                                echo form_checkbox(array(
                                    'class' => 'flat toggle-vis',
                                    'data-column' => '31'
                                ));
                                ?>
                              pasenger Andriod AppUrl
                            </label> 
                            <label class="">
                                <?php
                                echo form_checkbox(array(
                                    'class' => 'flat toggle-vis',
                                    'data-column' => '32'
                                ));
                                ?>
                              pasenger Iphone AppUrl
                            </label> 
                            <label class="">
                                <?php
                                echo form_checkbox(array(
                                    'class' => 'flat toggle-vis',
                                    'data-column' => '33'
                                ));
                                ?>
                              pasenger Andriod AppVersion
                            </label> 
                            <label class="">
                                <?php
                                echo form_checkbox(array(
                                    'class' => 'flat toggle-vis',
                                    'data-column' => '34'
                                ));
                                ?>
                              pasenger Iphone AppVersion
                            </label> 
                            
                            <label class="">
                                <?php
                                echo form_checkbox(array(
                                    'class' => 'flat toggle-vis',
                                    'data-column' => '35'
                                ));
                                ?>
                             Driver Andriod AppUrl
                            </label> 
                            <label class="">
                                <?php
                                echo form_checkbox(array(
                                    'class' => 'flat toggle-vis',
                                    'data-column' => '36'
                                ));
                                ?>
                             Driver Iphone AppUrl
                            </label> 
                            <label class="">
                                <?php
                                echo form_checkbox(array(
                                    'class' => 'flat toggle-vis',
                                    'data-column' => '37'
                                ));
                                ?>
                             Driver Andriod AppVersion
                            </label> 
                            <label class="">
                                <?php
                                echo form_checkbox(array(
                                    'class' => 'flat toggle-vis',
                                    'data-column' => '38'
                                ));
                                ?>
                             Driver Iphone AppVersion
                            </label> 
                            

                        </div>
                        <table
                            class="table table-striped table-bordered table-hover table-checkable order-column gp-font"
                            id="entity-data-table" style="width: 99%">

                            <thead>
                                <tr>

                                    <th>Action</th>
                                    <th>Entity Code</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Meta Keyword</th>
                                    <th>Tag Line</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Website</th>
                                    <th>Address</th>
                                    <th>Emergency Email</th>
                                    <th>Emergency Phone</th>
                                    <th>Entity Type</th>
                                    <th>CopyRights</th>
                                    <th>Toll FreeNo</th>
                                    <th>Tax</th>
                                    <th>Admin Commission</th>
                                    <th>Passenger Referral Discount</th>
                                    <th>Driver ReferralDiscount</th>
                                    <th>Passenger Rejection Charge</th>

                                    <th>Driver Min Wallet</th>
                                    <th>Driver Max Wallet</th>
                                    <th>Driver Security Amount</th>
                                    <th>DRT Promocode Amount</th>
                                    <th>DRT Promocode Days Limit</th>
                                    <th>DRT First Limit</th>
                                    <th>DRT First Limit Penalty</th>
                                    <th>DRT Second Limit</th>
                                   
                                    <th>Admin Booking Charge</th>
                                    <th>Send SMS</th>
                                    <th>Send Email</th>
                                    <th>Passenger Android App Url</th>
                                    <th>Passenger Iphone App Url</th>
                                    <th>Passenger Android App Ver</th>
                                    <th>Passenger Iphone App Ver</th>
                                    <th>Driver Android App Url</th>
                                    <th>Driver Iphone App Url</th>
                                    <th>Driver Android App Ver</th>
                                    <th>Driver Iphone App Ver</th>

                                    <th>Zone Name</th>
                                    <th>Country Name</th>
                                    <th>Status</th>

                                </tr>
                            </thead>
                            <tbody>
                            </tbody>

                        </table>
                    </div>
                </div>

            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>