<?php
class Login extends My_Controller {
	function __construct() {
		parent::__construct ();
		
		$this->load->helper ( 'form' );
		$this->load->model ( 'User_Login_Details_Model' );
		$this->load->model ( 'Sms_Template_Model' );
		$this->load->model ( 'Common_Api_Webservice_Model' );
	}
	public function index() {
		$this->addJs('app/login.js');
		if($this->session->userdata('user_id'))
		{
			redirect('dashboard');
		}
		else 
		{
		$this->load->view( 'login/login' );
		}
	}
	public function validationLogin() {
		$this->load->library ( 'form_validation' );
		$config = array (
				array (
						'field' => 'loginEmail',
						'label' => 'Email',
						'rules' => 'trim|required|valid_email' 
				),
				array (
						'field' => 'password',
						'label' => 'Password',
						'rules' => 'trim|required' 
				) 
		);
		
		$this->form_validation->set_rules ( $config );
		if ($this->form_validation->run () == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	public function userLogin() {
		// get API messages from message.php
		$msg_data = $this->config->item ( 'msg' );
		$result=NULL;
		$data = array ();
		$data = $this->input->post ();
		$internal_email = $data ['loginEmail'];
		$password = $data ['password'];
		if ($internal_email && $password)
		{
		$encpassword = hash ( "sha256", $password );
		$result = $this->User_Login_Details_Model->getOneByKeyValueArray ( array (
				'loginEmail' => $internal_email,
				'password' => $encpassword,
				'status' => Status_Type_Enum::ACTIVE,
				'isDeleted' => Status_Type_Enum::INACTIVE
		), 'id' );
		/* if(!$result)
		{
			$result = $this->User_Login_Details_Model->getOneByKeyValueArray ( array (
					'mobile' => $internal_email,
					'password' => $encpassword,
					'status' => Status_Type_Enum::ACTIVE,
					'isDeleted' => Status_Type_Enum::INACTIVE
			), 'id' );
		} */
		if ($result) {
			$id = $result->id;
			$internal_email = $result->loginEmail;
			$role_type = $result->roleType;
			//$role_user_id = $result->roleType;
			$user_name = $result->firstName . ' ' . $result->lastName;
			$user_city = $result->zoneId;
			$user_entity = $result->entityId;
			$user_profile = ($result->profileImage)?user_content_url ( $id . '/' . $result->profileImage ):image_url('app/user.png');
			$this->session->set_userdata ( 'user_id', $id );
			$this->session->set_userdata ( 'user_email', $internal_email );
			$this->session->set_userdata ( 'role_type', $role_type );
			//$this->session->set_userdata ( 'role_user_id', $role_user_id );
			$this->session->set_userdata ( 'user_name', $user_name );
			$this->session->set_userdata ( 'user_profile', $user_profile );
			$this->session->set_userdata ( 'user_city', $user_city );
			$this->session->set_userdata ( 'user_entity', $user_entity );
			$this->session->set_userdata ( 'table_prefix','');
			$message = array (
					"message" => $msg_data ['login_success'],
					"status" => 1 
			);
		} else {
			$message = array (
					"message" => $msg_data ['login_failed'],
					"status" => - 1 
			);
		}
		}
		else 
		{
			$message = array (
					"message" => $msg_data ['mandatory_field'],
					"status" => - 1
			);
		}
		echo json_encode ( $message );
	}
	public function resetPassword() {
		// get API messages from message.php
		$msg_data = $this->config->item ( 'msg' );
		$data = array ();
		$data = $this->input->post ();
		$internal_email = $data ['loginEmail'];
		if ($internal_email)
		{
		$user_exists = $this->User_Login_Details_Model->getOneByKeyValueArray ( array (
				'loginEmail' => $internal_email 
		) );
		if(!$user_exists)
		{
			$user_exists = $this->User_Login_Details_Model->getOneByKeyValueArray ( array (
				'mobile' => $internal_email
			) );
		}
		
		if ($user_exists) {
			// echo"hii";exit;
			$password = random_string ( 'alnum', 8 );
			$data ['password'] = hash ( "sha256", $password );
			// SMS & email service start
			$rest_password = $this->User_Login_Details_Model->update ( $data, array (
					'id' => $user_exists->id 
			) );
			
			if ($rest_password) {
				// SMS & email service start
				  if (SMS) {
					$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
							'title' => 'forgot_password_sms' 
					) );
					
					$message_data = $message_details->content;
					$message_data = str_replace ( "##PASSWORD##", $password, $message_data );
					if ($user_exists->mobile != "") {
						
						$this->Common_Api_Webservice_Model->sendSMS ( $user_exists->mobile, $message_data );
					}
				}
				/*
				$from = SMTP_EMAIL_ID;
				$to = $internal_email;
				$subject = 'Reset Password';
				$data = array (
						'password' => $password 
				);
				$message_data = $this->load->view ( 'emailtemplate/forgotpassword', $data, TRUE );
				
				if (SMTP) {
					
					if ($to) {
						$this->User_Login_Details_Model->sendEmail ( $to, $subject, $message_data );
					}
				} else {
					// To send HTML mail, the Content-type header must be set
					$headers = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					// Additional headers
					$headers .= 'From: Zuver<' . $from . '>' . "\r\n";
					$headers .= 'To: <' . $to . '>' . "\r\n";
					if (! filter_var ( $to, FILTER_VALIDATE_EMAIL ) === false) {
						mail ( $to, $subject, $message_data, $headers );
					}
				}*/
				
				// SMS & email service end
				$message = array (
						"message" => $msg_data ['reset_success'],
						"status" => 1 
				);
			} else {
				$message = array (
						"message" => $msg_data ['reset_failed'],
						"status" => - 1 
				);
			}
		}
		else {
			$message = array (
					"message" => $msg_data ['invalid_email'],
					"status" => - 1
			);
		}
		}
		else 
		{
			$message = array (
					"message" => $msg_data ['mandatory_field'],
					"status" => - 1
			);
		}
		echo json_encode ( $message );
	}
	public function checkUserIsLogged() {
		if ($this->session->userdata ( 'user_email' ) != '') {
			redirect ( 'Dashboard' );
		}
	}
	public function changeSessionCityId() {
		$city_id = $this->input->post ( 'city_id' );
		if ($city_id) {
			$this->session->set_userdata ( 'user_city', $city_id );
			$response = array (
					'msg' => 'Success' 
			);
		} else {
			$response = array (
					'msg' => 'Failed' 
			);
		}
		
		echo json_encode ( $response );
	}
}