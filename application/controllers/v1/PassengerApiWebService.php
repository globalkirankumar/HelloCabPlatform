<?php
require_once (APPPATH . 'libraries/Rest_Controller.php');
class PassengerApiWebService extends Rest_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->model ( 'Auth_Key_Details_Model' );
		$this->load->model ( 'v1/Passenger_Api_Webservice_Model' );
		$this->load->model ( 'v1/Driver_Api_Webservice_Model' );
		$this->load->model ( 'v1/Common_Api_Webservice_Model' );
		$this->load->model ( 'Passenger_Model' );
		$this->load->model ( 'Driver_Model' );
		$this->load->model ( 'Driver_Personal_Details_Model' );
		$this->load->model ( 'Driver_Taxi_Mapping_Model' );
		$this->load->model ( 'Country_Model' );
		$this->load->model ( 'Zone_Details_Model' );
		$this->load->model ( 'Sms_Template_Model' );
		$this->load->model ( 'Trip_Details_Model' );
		$this->load->model ( 'Trip_Transaction_Details_Model' );
		$this->load->model ( 'Driver_Dispatch_Details_Model' );
		$this->load->model ( 'Trip_Reject_Reason_List_Model' );
		$this->load->model ( 'Notification_Model' );
		$this->load->model ( 'Rate_Card_Details_Model' );
		$this->load->model ( 'Rate_Card_Slab_Details_Model' );
		$this->load->model ( 'Emergency_Request_Details_Model' );
		$this->load->model ( 'Taxi_Details_Model' );
		$this->load->model ( 'Taxi_Rejected_Trip_Details_Model' );
		$this->load->model ( 'Driver_Shift_History_Model' );
		$this->load->model ( 'Taxi_Request_Details_Model' );
		$this->load->model ( 'Promocode_Details_Model' );
		$this->load->model ( 'User_Emergency_Contacts_Model' );
		$this->load->model ( 'Data_Attributes_Model' );
		
		$this->load->model ( 'Entity_Model' );
		$this->load->model ( 'Entity_Config_Model' );
		$this->load->model ( 'Passenger_Transaction_Details_Model' );
		
		$this->load->model ( 'Referral_Details_Model' );
		$this->load->model ( 'User_Login_Details_Model' );
		$this->load->model ( 'User_Personal_Details_Model' );

	}
	public function get_post_data($validate = FALSE) {
		$data = file_get_contents ( 'php://input' );
		$post = json_decode ( $data );
		if (! $post) {
			$post = json_decode ( json_encode ( $_POST ), FALSE );
		}
		return $post;
	}
	public function getbase64ImageUrl($passenger_id, $image) {
		$file_image = '';
		$img = $image;
		$img = str_replace ( 'data:image/png;base64,', '', $img );
		$img = str_replace ( ' ', '+', $img );
		$data = base64_decode ( $img );
		$file_name = "PROFILE-" . $passenger_id . ".jpg";
		$upload_path = $this->config->item ( 'passenger_content_path' ) . "/" . $passenger_id;
		if (! file_exists ( $upload_path )) {
			mkdir ( $upload_path, 0777 );
		}
		$file = $upload_path . "/" . $file_name;
		$success = file_put_contents ( $file, $data );
		if ($success) {
			$file_image = $file_name;
		} else {
			$file_image = '';
		}
		return $file_image;
	}
	public function login_post() {
		// we need create API authKey for login user by adding keys &
		// intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = array ();
		$data = array ();
		$passenger_exists = NULL;
		$passenger_update = NULL;
		$passenger_otp_update = NULL;
		$response_data = array ();
		
		$passenger_data_exists = NULL;
		
		// authentication generated authKey
		$auth_key = NULL; // $post_data ['auth_key'];
		$auth_user_id = NULL; // $post_data ['user_id'];
		$auth_user_type = NULL; // $post_data ['userType'];
		$auth_details = array ();
		
		$message = '';
		
		$mobile = NULL;
		$email = NULL;
		$password = NULL;
		$device_id = NULL;
		$device_type = NULL;
		$gcm_id = NULL;
		$app_version = 0.0;
		// Posted json data
		$post_data = $this->get_post_data ();
		
		if (array_key_exists ( 'mobile', $post_data )) {
			$mobile = $post_data->mobile;
		}
		if (array_key_exists ( 'email', $post_data )) {
			$email = $post_data->email;
		}
		if (array_key_exists ( 'gcm_id', $post_data )) {
			$gcm_id = $post_data->gcm_id;
		}
		$password = $post_data->password;
		$device_id = $post_data->device_id;
		$device_type = $post_data->device_type;
		if (array_key_exists ( 'app_version', $post_data )) {
			$app_version = $post_data->app_version;
		}
		
		if (trim ( $gcm_id )) {
			if ($mobile || $email) {
				
				$passenger_data_exists = $this->Passenger_Model->getOneByKeyValueArray ( array (
						'mobile' => $mobile,
						'password' => '',
						'status' => Status_Type_Enum::ACTIVE 
				) );
				
				if ($passenger_data_exists) {
					$password_insert = hash ( "sha256", $password );
					$update_password = $this->Passenger_Model->update ( array (
							'password' => $password_insert 
					), array (
							'mobile' => $passenger_data_exists->mobile 
					) );
				}
				
				// To get tax & admin commission entity details
				
				$entity_details = $this->Entity_Model->getOneByKeyValueArray ( array (
						'id' => DEFAULT_COMPANY_ID 
				) );
				$entity_config_details = $this->Entity_Config_Model->getOneByKeyValueArray ( array (
						'id' => $entity_details->id 
				) );
				if ($email && $password) {
					$passenger_exists = $this->Passenger_Model->getOneByKeyValueArray ( array (
							'email' => $email,
							'password' => hash ( "sha256", $password )
							 
					) );
				}
				if (! $passenger_exists && $mobile && $password) {
					$passenger_exists = $this->Passenger_Model->getOneByKeyValueArray ( array (
							'mobile' => $mobile,
							'password' => hash ( "sha256", $password )
					) );
				}
				if ($passenger_exists) {
					if ($passenger_exists->status==Status_Type_Enum::ACTIVE)
					{
					$auth_data = array ();
					// update the paggenger deviceId & device type by default is 'A'=Android, 'I'=Iphone, 'W'=Windows,'N'=No device
					// @todo change the device based on device
					$passenger_update = $this->Passenger_Model->update ( array (
							'deviceId' => $device_id,
							'deviceType' => $device_type,
							'signupFrom' => Signup_Type_Enum::MOBILE,
							'loginStatus' => Status_Type_Enum::ACTIVE,
							'gcmId' => $gcm_id,
							'appVersion' => $app_version,
							'lastLoggedIn' => getCurrentDateTime () 
					), array (
							'mobile' => $passenger_exists->mobile 
					) );
					
					// Send OTP if passenger activation staus is failed
					if ($passenger_exists->activationStatus == Status_Type_Enum::INACTIVE) {
						
						// generate new 6 digit otp send SMS
						$otp = str_pad ( rand ( 0, 999999 ), 6, '0', STR_PAD_LEFT );
						$passenger_otp_update = $this->Passenger_Model->update ( array (
								'otp' => $otp 
						), array (
								'mobile' => $passenger_exists->mobile 
						) );
						if ($passenger_otp_update) {
							$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
									'title' => 'otp' 
							) );
							
							$message_data = $message_details->content;
							// $message = str_replace("##USERNAME##",$name,$message);
							$message_data = str_replace ( "##OTP##", $otp, $message_data );
							// print_r($message);exit;
							if ($passenger_exists->mobile != "") {
								
								$this->Common_Api_Webservice_Model->sendSMS ( $passenger_exists->mobile, $message_data );
							}
						}
						$message = array (
								"message" => $msg_data ['otp_verify_failed'],
								"passenger_id" => $passenger_exists->id,
								"otp" => $otp,
								"status" => 2 
						);
					}  // Send response data with Auth details & passenger details if passenger activation staus is success
else {
						
						$key = random_string ( 'alnum', 16 );
						$auth_key = hash ( "sha256", $key );
						$auth_key_exists = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
								'userId' => $passenger_exists->id,
								'userType' => User_Type_Enum::PASSENGER 
						) );
						
						// If user already exists update auth authKey
						if ($auth_key_exists) {
							$this->Auth_Key_Details_Model->update ( array (
									'authKey' => $auth_key 
							), array (
									'userId' => $passenger_exists->id,
									'userType' => User_Type_Enum::PASSENGER 
							) );
						}  // create new auth authKey for passenger
else {
							$this->Auth_Key_Details_Model->insert ( array (
									'authKey' => $auth_key,
									'userId' => $passenger_exists->id,
									'userType' => User_Type_Enum::PASSENGER 
							) );
						}
						
						// Authentication details
						$auth_details = array (
								'auth_key' => $auth_key,
								'user_id' => $passenger_exists->id,
								'user_type' => User_Type_Enum::PASSENGER 
						);
						
						// response_data
						
						// to get the referral message template
						$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
								'title' => 'referral_sms' 
						) );
						
						$referral_message_data = $message_details->content;
						
						$referral_message_data = str_replace ( "##REFERRALCODE##", $passenger_exists->passengerCode, $referral_message_data );
						$referral_message_data = str_replace ( "##REFERRALDISCOUNT##", $entity_config_details->passengerReferralDiscount, $referral_message_data );
						
						// to get the share message template
						$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
								'title' => 'share_sms' 
						) );
						$share_message_data = $message_details->content;
						$share_message_data = str_replace ( "##USERNAME##", $passenger_exists->firstName . ' ' . $passenger_exists->lastName, $share_message_data );
						
						$response_data = array (
								'auth_details' => $auth_details,
								'passenger_details' => array (
										'passenger_id' => $passenger_exists->id,
										'full_name' => $passenger_exists->firstName . ' ' . $passenger_exists->lastName,
										'email' => $passenger_exists->email,
										'gender' => $passenger_exists->gender,
										'dob' => $passenger_exists->dob,
										'mobile' => $passenger_exists->mobile,
										'wallet_amount' => $passenger_exists->walletAmount,
										'share_message' => $share_message_data,
										'referral_message' => $referral_message_data,
										'profile_image' => ($passenger_exists->profileImage) ? passenger_content_url ( '/' . $passenger_exists->id . '/' . $passenger_exists->profileImage ) : image_url ( 'app/user.png' ),
										'passenger_referral_code' => $passenger_exists->passengerCode 
								) 
						);
						
						$message = array (
								"message" => $msg_data ['login_success'],
								"detail" => array (
										$response_data 
								),
								"status" => 1 
						);
					}
					}else{
						$message = array (
								"message" => $msg_data ['inactive_account'],
								"status" => - 1
						);
					}
				} else {
					
					$message = array (
							"message" => $msg_data ['account_exists'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['mandatory_data'],
						"status" => - 1 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['gcm_mandatory_data'],
					"status" => - 1 
			);
		}
		echo json_encode ( $message );
	}
	public function logout_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$update_passenger_logout = NULL;
		$response_data = array ();
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$passenger_id = NULL;
		
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$passenger_id = $post_data->passenger_id;
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual functionality
				if ($passenger_id) {
					// update the passenger logout data
					$passenger_exists = $this->Passenger_Model->getById ( $passenger_id );
					$update_data = array (
							'signupFrom' => Signup_Type_Enum::NONE,
							'loginStatus' => Status_Type_Enum::INACTIVE,
							'deviceId' => '',
							'gcmId' => '',
							'deviceType' => Device_Type_Enum::NO_DEVICE 
					);
					$update_passenger_logout = $this->Passenger_Model->update ( $update_data, array (
							'mobile' => $passenger_exists->mobile 
					) );
					// update the passenger logout auth authKey=''
					$update_auth_key = $this->Auth_Key_Details_Model->update ( array (
							'authKey' => '' 
					), array (
							'userId' => $auth_user_id,
							'userType' => $auth_user_type 
					) );
					// @todo Delete rejected trip once passenger is logged out from logout date time
					if ($update_auth_key) {
						$message = array (
								"message" => $msg_data ['logout_success'],
								"status" => 1 
						);
					} else {
						$message = array (
								"message" => $msg_data ['logout_failed'],
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['invalid_user'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	public function getCoreConfig_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		
		$post_data = array ();
		$response_data = array ();
		$data_auth_key = FALSE;
		
		$message = '';
		// Posted json data
		$post_data = $this->get_post_data ();
		
		// To get tax & admin commission entity details
		
		$entity_details = $this->Entity_Model->getOneByKeyValueArray ( array (
				'id' => DEFAULT_COMPANY_ID 
		) );
		$entity_config_details = $this->Entity_Config_Model->getOneByKeyValueArray ( array (
				'entityId' => $entity_details->id 
		) );
		// to get currency code/currency symbol currencySymbol
		$currency_details = $this->Country_Model->getColumnByKeyValueArray ( 'currencySymbol', array (
				'id' => $entity_details->countryId 
		) );
		// to get trip_type list from data attributes
		$trip_type_list = $this->Data_Attributes_Model->getColumnByKeyValueArray ( 'id,description', array (
				'tableName' => strtoupper ( 'TRIP_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		) );
		
		// to get payment_mode list from data attributes
		$payment_mode_list = $this->Data_Attributes_Model->getColumnByKeyValueArray ( 'id,description', array (
				'tableName' => strtoupper ( 'PAYMENT_MODE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		) );
		
		// to get taxi_category_type list from data attributes
		$taxi_category_type_list = $this->Data_Attributes_Model->getColumnByKeyValueArray ( 'id,description', array (
				'tableName' => strtoupper ( 'TAXI_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		) );
		// to get taxi_category_type list from data attributes
		$trip_status_list = $this->Data_Attributes_Model->getColumnByKeyValueArray ( 'id,description', array (
				'tableName' => strtoupper ( 'TRIP_STATUS' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		) );
		// to get taxi_category_type list from data attributes
		$segment_type_list = $this->Data_Attributes_Model->getColumnByKeyValueArray ( 'id,description', array (
				'tableName' => strtoupper ( 'SEGMENT_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		) );
		// to get trip reject reason list
		
		$trip_reject_reason_list = $this->Trip_Reject_Reason_List_Model->getColumnByKeyValueArray ( 'description', array (
				'status' => Status_Type_Enum::ACTIVE 
		) );
		$reject_reason_list = array ();
		if ($trip_reject_reason_list) {
			foreach ( $trip_reject_reason_list as $list ) {
				$reject_reason_list [] = $list->description;
			}
		}
		
		if ($entity_details) {
			
			$response_data = array (
					'api_base_url' => base_url (),
					'logo_image_url' => image_url ( 'app/' . $entity_details->logo ),
					'aboutpage_description' => $entity_details->description,
					'contact_email' => $entity_details->email,
					"currency_symbol" => $currency_details [0]->currencySymbol,
					'android_passenger_app_ver' => number_format ( $entity_config_details->passengerAndroidAppVer, 1 ),
					'iphone_passenger_app_ver' => number_format ( $entity_config_details->passengerIphoneAppVer, 1 ),
					'referral_amount' => $entity_config_details->passengerReferralDiscount,
					'trip_type_list' => $trip_type_list,
					'payment_mode_list' => $payment_mode_list,
					'taxi_category_type_list' => $taxi_category_type_list,
					'trip_reject_reason' => $reject_reason_list,
					'trip_status_list' => $trip_status_list,
					'segment_type_list' => $segment_type_list,
					'booking_period_limit' => 120,
					'is_force_update' => Status_Type_Enum::ACTIVE 
			);
			
			$message = array (
					"message" => $msg_data ['success'],
					"detail" => $response_data,
					"status" => 1 
			);
		} else {
			$message = array (
					"message" => $msg_data ['failed'],
					"status" => - 1 
			);
		}
		
		echo json_encode ( $message );
	}
	public function getTripDetail_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$response_data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$trip_details = NULL;
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$trip_id = NULL;
		
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$trip_id = $post_data->trip_id;
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual funtionality
				if ($trip_id != null) {
					$trip_details = $this->Common_Api_Webservice_Model->getTripDetails ( $trip_id );
					
					if ($trip_details) {
						// To get tax & admin commission entity details
						$entity_details = $this->Entity_Model->getOneByKeyValueArray ( array (
								'id' => $trip_details [0]->entityId 
						) );
						$entity_config_details = $this->Entity_Config_Model->getOneByKeyValueArray ( array (
								'entityId' => $entity_details->id 
						) );
						
						// to get currency code/currency symbol currencySymbol
						$currency_details = $this->Country_Model->getColumnByKeyValueArray ( 'currencySymbol', array (
								'id' => $entity_details->countryId 
						) );
						$response_data = array (
								'trip_details' => array (
										'trip_id' => $trip_id,
										'job_card_id' => $trip_details [0]->jobCardId,
										'pickup_location' => $trip_details [0]->pickupLocation,
										'pickup_latitude' => $trip_details [0]->pickupLatitude,
										'pickup_longitude' => $trip_details [0]->pickupLongitude,
										'pickup_time' => $trip_details [0]->actualPickupDatetime,
										'drop_location' => $trip_details [0]->dropLocation,
										'drop_latitude' => $trip_details [0]->dropLatitude,
										'drop_longitude' => $trip_details [0]->dropLongitude,
										'drop_time' => $trip_details [0]->dropDatetime,
										'land_mark' => $trip_details [0]->landmark,
										'taxi_rating' => $trip_details [0]->passengerRating,
										'taxi_comments' => $trip_details [0]->passengerComments,
										'trip_type' => $trip_details [0]->tripType,
										'trip_type_name' => $trip_details [0]->tripTypeName,
										'payment_mode' => $trip_details [0]->paymentMode,
										'payment_mode_name' => $trip_details [0]->paymentModeName,
										'trip_status' => $trip_details [0]->tripStatus,
										'trip_status_name' => $trip_details [0]->tripStatusName 
								),
								'passenger_details' => array (
										'passenger_id' => $trip_details [0]->passengerId,
										'passenger_firstname' => $trip_details [0]->passengerFirstName,
										'passenger_lastname' => $trip_details [0]->passengerLastName,
										'passenger_mobile' => $trip_details [0]->passengerMobile,
										'passenger_profile_image' => ($trip_details [0]->passengerProfileImage) ? (passenger_content_url ( $trip_details [0]->passengerId . '/' . $trip_details [0]->passengerProfileImage )) : image_url ( 'app/user.png' ) 
								),
								'driver_details' => array (
										'driver_id' => $trip_details [0]->driverId,
										'driver_firstname' => $trip_details [0]->driverFirstName,
										'driver_lastname' => $trip_details [0]->driverLastName,
										'driver_mobile' => $trip_details [0]->driverMobile,
										'driver_profile_image' => ($trip_details [0]->driverProfileImage) ? (driver_content_url ( $trip_details [0]->driverId . '/' . $trip_details [0]->driverProfileImage )) : image_url ( 'app/user.png' ),
										'driver_language' => $trip_details [0]->driverLanguagesKnown,
										'driver_experience' => $trip_details [0]->driverExperience,
										'driver_age' => $trip_details [0]->driverDob,
										'driver_latitute' => $trip_details [0]->currentLatitude,
										'driver_longtitute' => $trip_details [0]->currentLongitude,
										'driver_status' => $trip_details [0]->availabilityStatus 
								),
								'taxi_details' => array (
										'taxi_id' => $trip_details [0]->taxiId,
										'taxi_name' => $trip_details [0]->taxiName,
										'taxi_model' => $trip_details [0]->taxiModel,
										'taxi_manufacturer' => $trip_details [0]->taxiManufacturer,
										'taxi_registration_no' => $trip_details [0]->taxiRegistrationNo,
										'taxi_colour' => $trip_details [0]->taxiColour 
								),
								
								'transaction_details' => array (
										'base_charge' => $trip_details [0]->baseCharge,
										'travel_charge' => $trip_details [0]->travelCharge,
										'parking_charge' => $trip_details [0]->parkingCharge,
										'tax_charge' => $trip_details [0]->taxCharge,
										'tax_percentage' => $trip_details [0]->taxPercentage,
										'toll_charge' => $trip_details [0]->tollCharge,
										'penalty_charge' => $trip_details [0]->penaltyCharge,
										'booking_charge' => $trip_details [0]->bookingCharge,
										'promo_discount' => $trip_details [0]->promoDiscountAmount,
										'wallet_payment' => $trip_details [0]->walletPaymentAmount,
										'travelled_period' => $trip_details [0]->travelledPeriod,
										'distance' => number_format ( $trip_details [0]->travelledDistance, 1 ),
										'total_trip_cost' => $trip_details [0]->totalTripCharge 
								) 
						);
						
						$message = array (
								"message" => $msg_data ['success'],
								"detail" => array (
										$response_data 
								),
								"status" => 1,
								"currency_symbol" => $currency_details [0]->currencySymbol 
						);
					} else {
						$message = array (
								"message" => $msg_data ['invalid_trip'],
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['invalid_trip'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	public function changePassword_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$passenger_exists = NULL;
		
		$password_status = NULL;
		$update_password = NULL;
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$passenger_id = NULL;
		$old_password = NULL;
		$new_password = NULL;
		$confirm_password = NULL;
		
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$passenger_id = $post_data->passenger_id;
		$old_password = $post_data->old_password;
		$new_password = $post_data->new_password;
		$confirm_password = $post_data->confirm_password;
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual funtionality
				if ($passenger_id != null) {
					// validation for checking $old_password, $new_password, $confirm_password data avilablity
					if ($old_password && $new_password && $confirm_password) {
						
						$passenger_exists = $this->Passenger_Model->getById ( $passenger_id );
						if ($passenger_exists) {
							$password = $passenger_exists->password;
							$raw_confirm_password = $confirm_password;
							$confirm_password = hash ( "sha256", $confirm_password );
							
							if ($password != hash ( "sha256", $old_password )) {
								$password_status = - 1;
							} else if ($new_password != $raw_confirm_password) {
								$password_status = - 2;
							} else if ($password == $confirm_password) {
								$password_status = - 3;
							} else {
								$update_password = $this->Passenger_Model->update ( array (
										'password' => $confirm_password 
								), array (
										'mobile' => $passenger_exists->mobile 
								) );
								if ($update_password) {
									$password_status = 1;
								}
							}
						} else {
							$password_status = - 4;
						}
					} else {
						$message = array (
								"message" => $msg_data ['mandatory_data'],
								"status" => - 1 
						);
					}
					switch ($password_status) {
						case - 1 :
							$message = array (
									"message" => $msg_data ['incorrect_old_password'],
									"status" => - 1 
							);
							break;
						case - 2 :
							$message = array (
									"message" => $msg_data ['confirm_password'],
									"status" => - 1 
							);
							break;
						case - 3 :
							$message = array (
									"message" => $msg_data ['old_new_password_same'],
									"status" => - 1 
							);
							break;
						case 1 :
							$message = array (
									"message" => $msg_data ['password_success'],
									"status" => 1 
							);
							break;
						case - 4 :
							$message = array (
									"message" => $msg_data ['invalid_user'],
									"status" => - 1 
							);
							break;
					}
				} else {
					$message = array (
							"message" => $msg_data ['invalid_user'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	public function resetPassword_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$message = '';
		$passenger_exists = NULL;
		$password = NULL;
		$update_password = NULL;
		
		$email = NULL;
		$mobile = NULL;
		// Posted json data
		$post_data = $this->get_post_data ();
		
		if (array_key_exists ( 'email', $post_data ) && $post_data->email) {
			$email = $post_data->email;
			$passenger_exists = $this->Passenger_Model->getOneByKeyValueArray ( array (
					'email' => $email 
			) );
		}
		
		if (! $passenger_exists && array_key_exists ( 'mobile', $post_data ) && $post_data->mobile) {
			$mobile = $post_data->mobile;
			$passenger_exists = $this->Passenger_Model->getOneByKeyValueArray ( array (
					'mobile' => $mobile 
			) );
		}
		
		if ($passenger_exists) {
			$entity_config_details = $this->Entity_Config_Model->getOneByKeyValueArray ( array (
					'entityId' => $passenger_exists->entityId 
			) );
			$password = random_string ( 'alnum', 8 );
			// encrypted password
			$password_key = hash ( "sha256", $password );
			$update_password = $this->Passenger_Model->update ( array (
					'password' => $password_key 
			), array (
					'mobile' => $passenger_exists->mobile 
			) );
			
			if ($entity_config_details->sendSms && $update_password) {
				$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
						'title' => 'forgot_password_sms' 
				) );
				
				$message_data = $message_details->content;
				// $message = str_replace("##USERNAME##",$name,$message);
				$message_data = str_replace ( "##PASSWORD##", $password, $message_data );
				// print_r($message);exit;
				if ($passenger_exists->mobile != "") {
					
					$this->Common_Api_Webservice_Model->sendSMS ( $passenger_exists->mobile, $message_data );
				}
			}
			/*
			 * $from = SMTP_EMAIL_ID;
			 * $to = $email;
			 * $subject = 'Take A Right Forgot Password';
			 * $data = array (
			 * 'password' => $password
			 * );
			 * $message_data = $this->load->view ( 'emailtemplate/forgotpassword', $data, TRUE );
			 *
			 * if (SMTP && $update_password) {
			 *
			 * if ($to) {
			 * $this->Common_Api_Webservice_Model->sendEmail ( $to, $subject, $message_data );
			 * }
			 * } else {
			 *
			 * // To send HTML mail, the Content-type header must be set
			 * $headers = 'MIME-Version: 1.0' . "\r\n";
			 * $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			 * // Additional headers
			 * $headers .= 'From: Zuver<' . $from . '>' . "\r\n";
			 * $headers .= 'To: <' . $to . '>' . "\r\n";
			 * if (! filter_var ( $to, FILTER_VALIDATE_EMAIL ) === false) {
			 * mail ( $to, $subject, $message_data, $headers );
			 * }
			 * }
			 */
			$message = array (
					"message" => $msg_data ['reset_password'],
					'status' => 1 
			);
		} else {
			$message = array (
					"message" => $msg_data ['invalid_user'],
					'status' => - 1 
			);
		}
		
		echo json_encode ( $message );
	}
	public function updateDriverTaxiRating_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$user_exist = NULL;
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$trip_id = NULL;
		$ratings = NULL;
		$comments = NULL;
		$taxi_ratings = NULL;
		$taxi_comments = NULL;
		
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$trip_id = $post_data->trip_id;
		$ratings = $post_data->ratings;
		$comments = $post_data->comments;
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual funtionality
				if ($trip_id != NULL) {
					if ($ratings) {
						
						$update_data = array (
								'passengerRating' => $ratings,
								'passengerComments' => $comments 
						);
						
						$update_result = $this->Trip_Details_Model->update ( $update_data, array (
								'id' => $trip_id 
						) );
						$trip_details = $this->Trip_Details_Model->getById ( $trip_id );
						// @todo update driiver rejected count $ consecutive reject count of driver to driver dispath details table
						$driver_dispatch_exists = NULL;
						$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
								'driverId' => $trip_details->driverId 
						) );
						if ($driver_dispatch_exists) {
							$avg_rating = round ( (($driver_dispatch_exists->avgRating + $ratings) / $driver_dispatch_exists->totalCompletedTrip), 1 );
							$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
									'avgRating' => $avg_rating 
							), array (
									'driverId' => $trip_details->driverId 
							) );
						} else {
							$insert_dispatch_data = array (
									'driverId' => $trip_details->driverId,
									'avgRating' => $ratings 
							);
							$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
						}
						// end driiver rejected count $ consecutive reject count of driver to driver dispath details table
						if ($update_result) {
							$message = array (
									"message" => $msg_data ['rating_success'],
									"status" => 1 
							);
						} else {
							$message = array (
									"message" => $msg_data ['rating_failed'],
									"status" => - 1 
							);
						}
					} else {
						$message = array (
								"message" => $msg_data ['mandatory_data'],
								"detail" => '',
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['invalid_user'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => - 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => - 401 
			);
		}
		echo json_encode ( $message );
	}
	public function signup_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		
		$referrer_id = Null;
		$referral_insert_id = NULL;
		$referrer_type = User_Type_Enum::NONE;
		$passenger_id = NULL;
		$user_exists = array ();
		$referrer_exists = array ();
		$update_passenger = NULL;
		$response_data = array ();
		
		$email = NULL;
		$first_name = NULL;
		$last_name = NULL;
		$mobile = NULL;
		$password = NULL;
		$zone_id = DEFAULT_ZONE_ID;
		
		$device_id = NULL;
		$device_type = NULL;
		$refered_code = NULL;
		$profile_image = NULL;
		$gender = NULL;
		$dob = NULL;
		$latitude = NULL;
		$longitude = NULL;
		$gcm_id = NULL;
		
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$email = $post_data->email;
		$first_name = $post_data->first_name;
		if (array_key_exists ( 'last_name', $post_data )) {
			$last_name = $post_data->last_name;
		}
		$mobile = $post_data->mobile;
		$password = $post_data->password;
		$device_id = $post_data->device_id;
		$device_type = $post_data->device_type;
		$dob = $post_data->dob;
		$gender = $post_data->gender;
		if (array_key_exists ( 'refered_code', $post_data )) {
			$refered_code = $post_data->refered_code;
		}
		if (array_key_exists ( 'profile_image', $post_data )) {
			$profile_image = $post_data->profile_image;
		}
		if (array_key_exists ( 'zone_id', $post_data )) {
			$zone_id = $post_data->zone_id;
		}
		if (array_key_exists ( 'latitude', $post_data )) {
			$latitude = $post_data->latitude;
		}
		if (array_key_exists ( 'longitude', $post_data )) {
			$longitude = $post_data->longitude;
		}
		if (array_key_exists ( 'gcm_id', $post_data )) {
			$gcm_id = $post_data->gcm_id;
		}
		if ($latitude && $longitude) {
			$zone_id = getZoneByLatLong ( $latitude, $longitude );
			if (! $zone_id) {
				$zone_id = DEFAULT_ZONE_ID;
			}
		}
		// actual functionality
		// to get site information details
		$entity_details = $this->Entity_Model->getById ( DEFAULT_COMPANY_ID );
		$entity_config_details = $this->Entity_Config_Model->getOneByKeyValueArray ( array (
				'entityId' => $entity_details->id 
		) );
		
		if ($email && $mobile && $password) {
			$user_exists = $this->Passenger_Model->getOneByKeyValueArray ( array (
					'email' => $email 
			) );
			if ($user_exists) {
				$message = array (
						"message" => $msg_data ['email_exists'],
						"status" => - 1 
				);
			} else if (! $user_exists) {
				$user_exists = $this->Passenger_Model->getOneByKeyValueArray ( array (
						'mobile' => $mobile 
				) );
				if ($user_exists) {
					$message = array (
							"message" => $msg_data ['mobile_exists'],
							"status" => - 1 
					);
				} else if (! $user_exists) {
					if ($refered_code) {
						$referrer_exists = $this->Passenger_Model->getOneByKeyValueArray ( array (
								'passengerCode' => $refered_code 
						) );
						$referrer_type = User_Type_Enum::PASSENGER;
						
						if ($referrer_exists) {
							$referrer_id = $referrer_exists->id;
						} else {
							
							$message = array (
									"message" => $msg_data ['valid_referral_code'],
									"status" => - 1 
							);
						}
					}
					
					$password = hash ( "sha256", $password );
					// @todo generate 6 digit otp
					$otp = str_pad ( rand ( 0, 999999 ), 6, '0', STR_PAD_LEFT );
					
					$insert_data = array (
							'firstName' => $first_name,
							'lastName' => $last_name,
							'email' => $email,
							'mobile' => $mobile,
							'password' => $password,
							'otp' => $otp,
							'gender' => $gender,
							'dob' => $dob,
							'gcmId' => $gcm_id,
							'deviceId' => $device_id,
							'deviceType' => $device_type,
							'zoneId' => $zone_id,
							'entityId' => DEFAULT_COMPANY_ID,
							'signupFrom' => Signup_Type_Enum::MOBILE,
							'registerType' => Register_Type_Enum::MANUAL 
					);
					$passenger_id = $this->Passenger_Model->insert ( $insert_data );
					// update the Unique Passenger Code
					$passenger_code_id = ($passenger_id <= 999999) ? str_pad ( ( string ) $passenger_id, 6, '0', STR_PAD_LEFT ) : $passenger_id;
					
					$passenger_code = 'PAS' . $passenger_code_id;
					$update_passenger_code = $this->Passenger_Model->update ( array (
							'passengerCode' => $passenger_code 
					), array (
							'mobile' => $mobile 
					) );
					
					if ($referrer_id) {
						$insert_data = array (
								'referrerId' => $referrer_id,
								'registeredId' => $passenger_id,
								'isReferrerEarned' => Status_Type_Enum::INACTIVE,
								'isRegisteredEarned' => Status_Type_Enum::INACTIVE,
								'earnedAmount' => $entity_config_details->passengerReferralDiscount,
								'userType' => User_Type_Enum::PASSENGER,
								'referrerType' => $referrer_type,
								'status' => Status_Type_Enum::ACTIVE 
						);
						$referral_insert_id = $this->Referral_Details_Model->insert ( $insert_data );
						/*
						 * $wallet_amount = $referrer_exists->walletAmount + $entity_config_details->passengerReferralDiscount;
						 *
						 * if ($referrer_type == User_Type_Enum::PASSENGER) {
						 * $referrer_wallet_update = $this->Passenger_Model->update ( array (
						 * 'walletAmount' => $wallet_amount
						 * ), array (
						 * 'id' => $referrer_id
						 * ) );
						 * $transaction_insert_data = array (
						 * 'passengerId' => $referrer_id,
						 * 'referralId' => $referral_insert_id,
						 * 'transactionAmount' => $entity_config_details->passengerReferralDiscount,
						 * 'transactionStatus' => 'Success',
						 * 'previousAmount' => $referrer_exists->walletAmount,
						 * 'currentAmount' => $wallet_amount,
						 * 'transactionMode' => Transaction_Mode_Enum::CREDIT,
						 * 'transactionType' => Transaction_Type_Enum::REFERRAL,
						 * 'transactionFrom' => Transaction_From_Enum::WALLET_ACCOUNT
						 * );
						 * $passenger_transaction_id = $this->Passenger_Transaction_Details_Model->insert ( $transaction_insert_data );
						 * }
						 */
					}
					
					if ($passenger_id) {
						if (array_key_exists ( 'profile_image', $post_data ) && $profile_image) {
							$data ['file_name'] = $this->getbase64ImageUrl ( $passenger_id, $profile_image );
							
							if ($data) {
								
								$update_passenger = $this->Passenger_Model->update ( array (
										'profileImage' => $data ['file_name'] 
								), array (
										'mobile' => $mobile 
								) );
							}
						}
						
						// response data
						$response_data = array (
								"passenger_id" => $passenger_id,
								'otp' => $otp 
						);
						
						if ($passenger_id) {
							
							if ($entity_config_details->sendSms && $passenger_id) {
								$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
										'title' => 'otp' 
								) );
								
								$message_data = $message_details->content;
								// $message = str_replace("##USERNAME##",$name,$message);
								$message_data = str_replace ( "##OTP##", $otp, $message_data );
								// print_r($message);exit;
								if ($mobile != "") {
									
									$this->Common_Api_Webservice_Model->sendSMS ( $mobile, $message_data );
								}
							}
							
							if ($update_passenger && $passenger_id) {
								$message = array (
										"message" => $msg_data ['signup_success'],
										"detail" => array (
												$response_data 
										),
										"status" => 1 
								);
							} else if (! $update_passenger && $passenger_id) {
								$message = array (
										"message" => $msg_data ['signup_success_image_failed'],
										"detail" => $response_data,
										"status" => 1 
								);
							}
						}
					} 

					else {
						$message = array (
								"message" => $msg_data ['signup_failed'],
								"status" => - 1 
						);
					}
				}
			}
		} else {
			$message = array (
					"message" => $msg_data ['mandatory_data'],
					"status" => - 1 
			);
		}
		
		echo json_encode ( $message );
	}
	public function otpVerification_post() {
		
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		
		$message = '';
		$signup_form = NULL;
		$check_otp = NULL;
		$passenger_exists = array ();
		$response_data = array ();
		
		$passenger_id = NULL;
		$otp = NULL;
		
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$passenger_id = $post_data->passenger_id;
		$otp = $post_data->otp;
		
		// actual functionality
		
		if ($passenger_id && $otp) {
			
			// To get tax & admin commission entity details
			$passenger_exists = $this->Passenger_Model->getOneByKeyValueArray ( array (
					'id' => $passenger_id,
					'otp' => $otp 
			) );
			
			if ($passenger_exists) {
				$entity_config_details = $this->Entity_Config_Model->getOneByKeyValueArray ( array (
						'entityId' => $passenger_exists->entityId 
				) );
				$check_otp = $this->Passenger_Model->update ( array (
						'activationStatus' => Status_Type_Enum::ACTIVE,
						'loginStatus' => Status_Type_Enum::ACTIVE,
						'lastLoggedIn' => getCurrentDateTime () 
				), array (
						'mobile' => $passenger_exists->mobile 
				) );
			}
			
			if ($passenger_exists && $check_otp) {
				
				// Auth details
				$key = random_string ( 'alnum', 16 );
				$auth_key = hash ( "sha256", $key );
				$auth_key_exists = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
						'userId' => $passenger_exists->id,
						'userType' => User_Type_Enum::PASSENGER 
				) );
				
				// If user already exists update auth authKey
				if ($auth_key_exists) {
					$this->Auth_Key_Details_Model->update ( array (
							'authKey' => $auth_key 
					), array (
							'userId' => $passenger_exists->id,
							'userType' => User_Type_Enum::PASSENGER 
					) );
				}  // create new auth authKey for passenger
else {
					$this->Auth_Key_Details_Model->insert ( array (
							'authKey' => $auth_key,
							'userId' => $passenger_exists->id,
							'userType' => User_Type_Enum::PASSENGER 
					) );
				}
				// Authentication details
				$auth_details = array (
						'auth_key' => $auth_key,
						'user_id' => $passenger_exists->id,
						'user_type' => User_Type_Enum::PASSENGER 
				);
				
				if ($entity_config_details->sendSms && $check_otp) {
					$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
							'title' => 'welcome' 
					) );
					
					$message_data = $message_details->content;
					$message_data = str_replace ( "##USERNAME##", $passenger_exists->email . '/' . $passenger_exists->mobile, $message_data );
					// print_r($message);exit;
					if ($passenger_exists->mobile != "") {
						
						$this->Common_Api_Webservice_Model->sendSMS ( $passenger_exists->mobile, $message_data );
					}
				}
				
				// response_data
				
				if ($entity_config_details->sendSms) {
					// to get the referral message template
					$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
							'title' => 'referral_sms' 
					) );
					
					$referral_message_data = $message_details->content;
					
					$referral_message_data = str_replace ( "##REFERRALCODE##", $passenger_exists->passengerCode, $referral_message_data );
					$referral_message_data = str_replace ( "##REFERRALDISCOUNT##", $entity_config_details->passengerReferralDiscount, $referral_message_data );
				}
				if ($entity_config_details->sendSms) {
					// to get the share message template
					$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
							'title' => 'share_sms' 
					) );
					$share_message_data = $message_details->content;
					$share_message_data = str_replace ( "##USERNAME##", $passenger_exists->firstName . ' ' . $passenger_exists->lastName, $share_message_data );
				}
				$response_data = array (
						"auth_details" => $auth_details,
						"passenger_details" => array (
								"passenger_id" => $passenger_exists->id,
								"full_name" => $passenger_exists->firstName . ' ' . $passenger_exists->lastName,
								"mobile" => $passenger_exists->mobile,
								"email" => $passenger_exists->email,
								'wallet_amount' => $passenger_exists->walletAmount,
								'share_message' => $share_message_data,
								'referral_message' => $referral_message_data,
								"profile_image" => ($passenger_exists->profileImage) ? passenger_content_url ( '/' . $passenger_exists->id . '/' . $passenger_exists->profileImage ) : '',
								"passenger_referral_code" => ($passenger_exists->passengerCode) ? $passenger_exists->passengerCode : '' 
						) 
				);
				// response data
				$message = array (
						"message" => $msg_data ['activation_success'],
						"detail" => array (
								$response_data 
						),
						"status" => 1 
				);
			} else {
				$message = array (
						"message" => $msg_data ['invaild_otp'],
						"status" => - 1 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['mandatory_data'],
					"status" => - 1 
			);
		}
		
		echo json_encode ( $message );
	}
	public function otpResend_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$otp = NULL;
		$passenger_exists = NULL;
		$update_otp = NULL;
		
		$passenger_id = NULL;
		
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$passenger_id = $post_data->passenger_id;
		
		// actual functionality
		$otp = str_pad ( rand ( 0, 999999 ), 6, '0', STR_PAD_LEFT );
		
		if ($passenger_id) {
			
			$passenger_exists = $this->Passenger_Model->getById ( $passenger_id );
			if ($passenger_exists) {
				$update_otp = $this->Passenger_Model->update ( array (
						'otp' => $otp 
				), array (
						'mobile' => $passenger_exists->mobile 
				) );
			}
			
			if ($passenger_exists && $update_otp) {
				$entity_config_details = $this->Entity_Config_Model->getOneByKeyValueArray ( array (
						'entityId' => $passenger_exists->entityId 
				) );
				if (SMS && $update_otp) {
					$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
							'title' => 'otp' 
					) );
					
					$message_data = $message_details->content;
					
					$message_data = str_replace ( "##OTP##", $otp, $message_data );
					
					// print_r($message);exit;
					if ($passenger_exists->mobile != "") {
						
						$this->Common_Api_Webservice_Model->sendSMS ( $passenger_exists->mobile, $message_data );
					}
				}
				
				$message = array (
						"message" => $msg_data ['otp_success'],
						"otp" => $otp,
						"status" => 1 
				);
			} else {
				$message = array (
						"message" => $msg_data ['failed'],
						"status" => - 1 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['invalid_user'],
					"status" => - 1 
			);
		}
		
		echo json_encode ( $message );
	}
	public function tripHistoryList_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		
		$passenger_pending_trip_list = NULL;
		$passenger_past_trip_list = NULL;
		$passenger_cancelled_trip_list = NULL;
		
		$trip_history_list = array ();
		
		$passenger_exists = NULL;
		$response_data = array ();
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$passenger_id = NULL;
		$start_offset = 0;
		$end_limit = NULL;
		$category = NULL;
		$start_date = NULL;
		$end_date = NULL;
		
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$passenger_id = $post_data->passenger_id;
		
		if (array_key_exists ( 'start_offset', $post_data )) {
			$start_offset = $post_data->start_offset;
		}
		if (array_key_exists ( 'end_limit', $post_data )) {
			$end_limit = $post_data->end_limit;
		}
		if (array_key_exists ( 'start_date', $post_data )) {
			$start_date = $post_data->start_date;
		}
		if (array_key_exists ( 'end_date', $post_data )) {
			$end_date = $post_data->end_date;
		}
		if (array_key_exists ( 'category', $post_data )) {
			$category = $post_data->category;
		}
		// $category = $post_data->category;
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual funtionality
				if ($passenger_id || $start_offset || $end_limit) {
					$passenger_exists = $this->Passenger_Model->getById ( $passenger_id );
					if ($passenger_exists) {
						
						/* if ($category == 1) {
							$passenger_pending_trip_list = $this->Passenger_Api_Webservice_Model->getPendingTripList ( $passenger_id, $start_offset, $end_limit, $start_date, $end_date );
							if ($passenger_pending_trip_list) {
								
								foreach ( $passenger_pending_trip_list as $list ) {
									
									$trip_history_list [] = array (
											'trip_id' => $list->tripId,
											'pickup_location' => $list->pickupLocation,
											'pickup_latitude' => $list->pickupLatitude,
											'pickup_longitude' => $list->pickupLongitude,
											'drop_location' => $list->dropLocation,
											'drop_latitude' => $list->dropLatitude,
											'drop_longitude' => $list->dropLongitude,
											'pickup_time' => $list->pickupDatetime,
											'crn' => $list->customerReferenceCode,
											'taxi_category' => $list->taxiCategoryTypeName,
											'travel_status' => $list->tripStatus,
											'travel_msg' => $list->tripStatusName,
											'trip_type' => $list->tripType,
											'trip_type_name' => $list->tripTypeName,
											'payment_mode' => $list->paymentMode,
											'payment_mode_name' => $list->paymentModeName 
									);
								}
							}
						}
						if ($category == - 1) {
							$passenger_past_trip_list = $this->Passenger_Api_Webservice_Model->getPastTripList ( $passenger_id, $start_offset, $end_limit, $start_date, $end_date );
							if ($passenger_past_trip_list) {
								foreach ( $passenger_past_trip_list as $list ) {
									
									$trip_history_list [] = array (
											'trip_id' => $list->tripId,
											'pickup_location' => $list->pickupLocation,
											'pickup_latitude' => $list->pickupLatitude,
											'pickup_longitude' => $list->pickupLongitude,
											'drop_location' => $list->dropLocation,
											'drop_latitude' => $list->dropLatitude,
											'drop_longitude' => $list->dropLongitude,
											'pickup_time' => $list->actualPickupDatetime,
											'crn' => $list->customerReferenceCode,
											'taxi_category' => $list->taxiCategoryTypeName,
											'travel_status' => $list->tripStatus,
											'total_trip_charge' => $list->totalTripCharge,
											'travel_msg' => $list->tripStatusName,
											'trip_type' => $list->tripType,
											'trip_type_name' => $list->tripTypeName,
											'payment_mode' => $list->paymentMode,
											'payment_mode_name' => $list->paymentModeName 
									);
								}
							}
						} */
						if ($category === NULL) {
							$passenger_trip_history_list = $this->Passenger_Api_Webservice_Model->getTripHistoryList ( $passenger_id, $start_offset, $end_limit, $start_date, $end_date );
							if ($passenger_trip_history_list) {
								foreach ( $passenger_trip_history_list as $list ) {
									$pickup_time = $list->pickupDatetime;
									$trip_history_list [] = array (
											'trip_id' => $list->tripId,
											'pickup_location' => $list->pickupLocation,
											'pickup_latitude' => $list->pickupLatitude,
											'pickup_longitude' => $list->pickupLongitude,
											'drop_location' => $list->dropLocation,
											'drop_latitude' => $list->dropLatitude,
											'drop_longitude' => $list->dropLongitude,
											'pickup_time' => $pickup_time,
											'crn' => $list->customerReferenceCode,
											'taxi_category' => $list->taxiCategoryTypeName,
											'travel_status' => $list->tripStatus,
											'total_trip_charge' => ($list->totalTripCharge) ? $list->totalTripCharge : 00.00,
											'travel_msg' => $list->tripStatusName,
											'trip_type' => $list->tripType,
											'trip_type_name' => ($list->tripTypeName == 'Ride Later' && ($list->tripStatus == Trip_Status_Enum::BOOKED || $list->tripStatus == Trip_Status_Enum::DRIVER_DISPATCHED || $list->tripStatus == Trip_Status_Enum::DRIVER_ACCEPTED || $list->tripStatus == Trip_Status_Enum::DRIVER_ARRIVED)) ? 'DriveLater' : $list->tripTypeName,
											'payment_mode' => $list->paymentMode,
											'payment_mode_name' => $list->paymentModeName
									);
								}
							}
						}
						
						/* if ($category == 0) {
							$passenger_cancelled_trip_list = $this->Passenger_Api_Webservice_Model->getCancelledTripList ( $passenger_id, $start_offset, $end_limit, $start_date, $end_date );
							if ($passenger_cancelled_trip_list) {
								foreach ( $passenger_cancelled_trip_list as $list ) {
									
									$trip_history_list [] = array (
											'trip_id' => $list->tripId,
											'pickup_location' => $list->pickupLocation,
											'pickup_latitude' => $list->pickupLatitude,
											'pickup_longitude' => $list->pickupLongitude,
											'drop_location' => $list->dropLocation,
											'drop_latitude' => $list->dropLatitude,
											'drop_longitude' => $list->dropLongitude,
											'pickup_time' => $list->pickupDatetime,
											'crn' => $list->customerReferenceCode,
											'taxi_category' => $list->taxiCategoryTypeName,
											'travel_status' => $list->tripStatus,
											'travel_msg' => $list->tripStatusName,
											'trip_type' => $list->tripType,
											'trip_type_name' => $list->tripTypeName,
											'payment_mode' => $list->paymentMode,
											'payment_mode_name' => $list->paymentModeName 
									);
								}
							}
						} */
						
						
						$response_data ['trip_history_list'] = $trip_history_list;
						
						if (count ( $response_data ) > 0) {
							// $message = $passengers_current;
							$message = array (
									"message" => $msg_data ['success'],
									"trip_details" => $response_data,
									"status" => 1 
							);
						} else {
							$message = array (
									"message" => $msg_data ['data_not_found'],
									"status" => - 1 
							);
						}
					} else {
						$message = array (
								"message" => $msg_data ['invalid_user'],
								"status" => - 1 
						);
					}
				} else {
					
					$message = array (
							"message" => $msg_data ['mandatory_data'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	public function getEmergencyContacts_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = array ();
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$emergency_contact_list = NULL;
		
		$response_data = array ();
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$passenger_id = NULL;
		
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$passenger_id = $post_data->passenger_id;
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual functonality
				if ($passenger_id) {
					$emergency_contact_list = $this->User_Emergency_Contacts_Model->getByKeyValueArray ( array (
							'userId' => $passenger_id,
							'userType' => User_Type_Enum::PASSENGER,
							'status' => Status_Type_Enum::ACTIVE 
					) );
					if ($emergency_contact_list) {
						foreach ( $emergency_contact_list as $list ) {
							$response_data [] = array (
									'emergency_id' => $list->id,
									'name' => $list->name,
									'mobile' => $list->mobile,
									'email' => $list->email 
							);
						}
						$message = array (
								"message" => $msg_data ['success'],
								"emergency_contact_list" => $response_data,
								"status" => 1 
						);
					} else {
						$message = array (
								"message" => $msg_data ['failed'],
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['mandatory_data'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		
		echo json_encode ( $message );
	}
	public function deleteEmergencyContact_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = array ();
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$delete_user_emergency = NULL;
		$passenger_id = NULL;
		$emergency_id = NULL;
		$response_data = array ();
		
		// Posted json data
		$post_data = $this->get_post_data ();
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$passenger_id = $post_data->passenger_id;
		$emergency_id = $post_data->emergency_id;
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual functonality
				if ($passenger_id && $emergency_id) {
					$delete_user_emergency = $this->User_Emergency_Contacts_Model->delete ( array (
							'id' => $emergency_id,
							'userId' => $passenger_id,
							'userType' => User_Type_Enum::PASSENGER 
					) );
					if ($delete_user_emergency) {
						$message = array (
								"message" => $msg_data ['sos_delete_success'],
								"status" => 1 
						);
					} else {
						$message = array (
								"message" => $msg_data ['sos_delete_failed'],
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['mandatory_data'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		
		echo json_encode ( $message );
	}
	
	/**
	 * used to add/edit the emergency contacts
	 */
	public function saveEmergencyContact_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$check_user_emergency = NULL;
		$user_emergency_exists = NULL;
		$insert_data = array ();
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$passenger_id = NULL;
		$emergency_id = NULL;
		$emergency_name = NULL;
		$emergency_mobile = NULL;
		$emergency_email = '';
		
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$passenger_id = $post_data->passenger_id;
		$emergency_name = $post_data->emergency_name;
		$emergency_mobile = $post_data->emergency_mobile;
		if (array_key_exists ( 'emergency_email', $post_data )) {
			$emergency_email = $post_data->emergency_email;
		}
		if (array_key_exists ( 'emergency_id', $post_data )) {
			$emergency_id = $post_data->emergency_id;
		}
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual functonality
				if ($passenger_id && $emergency_name && $emergency_mobile) {
					$check_user_emergency = $this->User_Emergency_Contacts_Model->getByKeyValueArray ( array (
							'userId' => $passenger_id 
					) );
					
					if (! $emergency_id > 0) {
						$user_emergency_exists = $this->User_Emergency_Contacts_Model->getByKeyValueArray ( array (
								'userId' => $passenger_id,
								'userType' => User_Type_Enum::PASSENGER,
								'mobile' => $emergency_mobile,
								'email' => $emergency_email,
								'status' => Status_Type_Enum::ACTIVE 
						) );
						if (! $user_emergency_exists) {
							// debug_exit(EMERGENCY_COUNT_LIMIT);
							if (count ( $check_user_emergency ) < EMERGENCY_COUNT_LIMIT) {
								$insert_data = array (
										'userId' => $passenger_id,
										'userType' => User_Type_Enum::PASSENGER,
										'name' => $emergency_name,
										'email' => $emergency_email,
										'mobile' => $emergency_mobile,
										'status' => Status_Type_Enum::ACTIVE 
								);
								$emergency_id = $this->User_Emergency_Contacts_Model->insert ( $insert_data );
								if ($emergency_id) {
									$message = array (
											"message" => $msg_data ['sos_add_success'],
											"emergency_id" => $emergency_id,
											"status" => 1 
									);
								} else {
									$message = array (
											"message" => $msg_data ['sos_add_failed'],
											"status" => - 1 
									);
								}
							} else {
								$message = array (
										"message" => $msg_data ['sos_limit_exceeds'],
										"status" => - 1 
								);
							}
						} else {
							$message = array (
									"message" => $msg_data ['email_mobile_exists'],
									"status" => - 1 
							);
						}
					} else {
						$update_data = array (
								'userId' => $passenger_id,
								'userType' => User_Type_Enum::PASSENGER,
								'name' => $emergency_name,
								'email' => $emergency_email,
								'mobile' => $emergency_mobile,
								'status' => Status_Type_Enum::ACTIVE 
						);
						$emergency_update = $this->User_Emergency_Contacts_Model->update ( $update_data, array (
								'id' => $emergency_id 
						) );
						if ($emergency_update) {
							$message = array (
									"message" => $msg_data ['sos_updated_success'],
									"emergency_id" => $emergency_id,
									"status" => 1 
							);
						} else {
							$message = array (
									"message" => $msg_data ['sos_add_failed'],
									"status" => - 1 
							);
						}
					}
				} else {
					$message = array (
							"message" => $msg_data ['mandatory_data'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		
		echo json_encode ( $message );
	}
	public function emergencyReport_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$user_exists = array ();
		$user_id = NULL;
		$latitude = NULL;
		$longitude = NULL;
		$current_location = NULL;
		$emergency_list = array ();
		$address = array ();
		
		// Posted json data
		$post_data = $this->get_post_data ();
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$passenger_id = $post_data->passenger_id;
		$latitude = $post_data->latitude;
		$longitude = $post_data->longitude;
		$current_location = $post_data->current_location;
		
		$insert_data = array (
				'userId' => $passenger_id,
				'userType' => User_Type_Enum::PASSENGER,
				'location' => $current_location,
				'latitude' => $latitude,
				'longitude' => $longitude,
				'emergencyStatus' => Emergency_Status_Enum::OPEN 
		);
		$emergency_id = $this->Emergency_Request_Details_Model->insert ( $insert_data );
		// Google map url link
		$google_map_url = "http://maps.google.com/maps?z=11&t=m&q=loc:" . $latitude . "+" . $longitude;
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual functionality
				if ($passenger_id) {
					$user_exists = $this->Passenger_Model->getById ( $passenger_id );
					$entity_config_details = $this->Entity_Config_Model->getOneByKeyValueArray ( array (
							'entityId' => $user_exists->entityId 
					) );
					if (count ( $user_exists ) > 0) {
						$emergency_list = $this->User_Emergency_Contacts_Model->getByKeyValueArray ( array (
								'userId' => $passenger_id,
								'userType' => User_Type_Enum::PASSENGER,
								'status' => Status_Type_Enum::ACTIVE 
						) );
						
						if (count ( $emergency_list ) > 0) {
							
							foreach ( $emergency_list as $list ) {
								
								if ($entity_config_details->sendSms && $list->mobile) {
									$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
											'title' => 'emergency_contact' 
									) );
									
									$message_data = $message_details->content;
									
									$message_data = str_replace ( "##PASSENGERNAME##", $user_exists->firstName . ' ' . $user_exists->lastName, $message_data );
									$message_data = str_replace ( "##LOCATION##", $current_location, $message_data );
									$message_data = str_replace ( "##GOOGLELINK##", $google_map_url, $message_data );
									
									if ($list->mobile != "") {
										
										$this->Common_Api_Webservice_Model->sendSMS ( $list->mobile, $message_data );
									}
								}
							}
							
							$message = array (
									"message" => $msg_data ['sos_notice'],
									"status" => 1 
							);
						} else {
							$message = array (
									"message" => $msg_data ['data_not_found'],
									"status" => - 1 
							);
						}
					} else {
						$message = array (
								"message" => $msg_data ['invalid_user'],
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['invalid_user'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	public function promocodeVerification_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$promocode_exists = NULL;
		$promocode_limit_exists = NULL;
		$promo_code_validate = array ();
		$response_data = array ();
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$passenger_id = NULL;
		$promo_code = NULL;
		$zone_id = 0;
		$drop_zone_id = 0;
		$pickup_latitude = NULL;
		$pickup_longitude = NULL;
		$drop_latitude = NULL;
		$drop_longitude = NULL;
		
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$promo_code = ($post_data->promo_code) ? $post_data->promo_code : '';
		$passenger_id = $post_data->passenger_id;
		
		if (array_key_exists ( 'pickup_latitude', $post_data )) {
			$pickup_latitude = $post_data->pickup_latitude;
		}
		if (array_key_exists ( 'pickup_longitude', $post_data )) {
			$pickup_longitude = $post_data->pickup_longitude;
		}
		if (array_key_exists ( 'drop_latitude', $post_data )) {
			$drop_latitude = $post_data->drop_latitude;
		}
		if (array_key_exists ( 'drop_longitude', $post_data )) {
			$drop_longitude = $post_data->drop_longitude;
		}
		// PickUp Zone ID
		if ($pickup_latitude && $pickup_longitude) {
			$zone_id = getZoneByLatLong ( $pickup_latitude, $pickup_longitude );
		}
		if (! $zone_id) {
			$zone_id = DEFAULT_ZONE_ID;
		}
		// Drop Zone ID
		if ($drop_latitude && $drop_longitude) {
			$drop_zone_id = getZoneByLatLong ( $drop_latitude, $drop_longitude );
		}
		if (! $drop_zone_id) {
			$drop_zone_id = DEFAULT_ZONE_ID;
		}
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual functionality
				// $zone_id=18; $drop_zone_id=19;
				// Check promocode exists or not
				if ($promo_code) {
					// $zone_id = 12;
					// $drop_zone_id = 0;
					$promo_code_validate = $this->validatePromoCode ( $promo_code, $passenger_id, $zone_id, $drop_zone_id, DEFAULT_COMPANY_ID );
					if ($promo_code_validate) {
						$message = $promo_code_validate;
					}
				} else {
					$message = array (
							"message" => $msg_data ['invalid_promocode'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		
		echo json_encode ( $message );
	}
	public function updateProfile_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		
		$update_passenger_profile = NULL;
		$passenger_data = array ();
		$passenger_exists = array ();
		$user_mobile_exists = array ();
		$response_data = array ();
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$email = NULL;
		$first_name = NULL;
		$last_name = NULL;
		$mobile = NULL;
		$passenger_id = NULL;
		$profile_image = NULL;
		
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$email = $post_data->email;
		$first_name = $post_data->first_name;
		$last_name = $post_data->last_name;
		$mobile = $post_data->mobile;
		$passenger_id = $post_data->passenger_id;
		$profile_image = $post_data->profile_image;
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual functionality
				if ($email) {
					$passenger_exists = $this->Passenger_Model->getOneByKeyValueArray ( array (
							'id' => $passenger_id,
							'status' => Status_Type_Enum::ACTIVE,
							'activationStatus' => Status_Type_Enum::ACTIVE 
					) );
					if ($passenger_exists) {
						
						$user_mobile_exists = $this->Passenger_Model->getOneByKeyValueArray ( array (
								'mobile' => $mobile 
						) );
						
						if ($user_mobile_exists && $passenger_exists->mobile != $mobile) {
							$message = array (
									"message" => $msg_data ['mobile_exists'],
									"status" => - 1 
							);
						} else if (! $user_mobile_exists || $passenger_exists->mobile == $mobile) {
							
							$update_passenger = $this->Passenger_Model->update ( array (
									'firstName' => $first_name,
									'lastName' => $last_name,
									'mobile' => $mobile 
							), array (
									'mobile' => $passenger_exists->mobile 
							) );
							if ($update_passenger) {
								if ($profile_image) {
									if (array_key_exists ( 'profile_image', $post_data ) && $profile_image) {
										
										$data ['file_name'] = $this->getbase64ImageUrl ( $passenger_id, $profile_image );
										
										if ($data) {
											
											$update_passenger_profile = $this->Passenger_Model->update ( array (
													'profileImage' => $data ['file_name'] 
											), array (
													'mobile' => $passenger_exists->mobile 
											) );
										}
									}
									$passenger_data = $this->Passenger_Model->getById ( $passenger_id );
									
									$response_data = array (
											'passenger_details' => array (
													'profile_image' => ($passenger_data->profileImage) ? passenger_content_url ( $passenger_id . '/' . $passenger_data->profileImage ) : image_url ( 'app/user.png' ),
													'first_name' => $passenger_data->firstName,
													'last_name' => $passenger_data->lastName,
													'email' => $passenger_data->email,
													'referral_code' => $passenger_data->passengerCode,
													'wallet_amount' => $passenger_data->walletAmount,
													'mobile' => $passenger_data->mobile 
											) 
									);
									if ($update_passenger_profile) {
										
										$message = array (
												"message" => $msg_data ['signup_success'],
												"detail" => array (
														$response_data 
												),
												"status" => 1 
										);
									} else {
										$message = array (
												"message" => $msg_data ['signup_success_image_failed'],
												"status" => - 1 
										);
									}
								} else {
									$message = array (
											"message" => $msg_data ['signup_success'],
											"status" => 1 
									);
								}
							} else {
								$message = array (
										"message" => $msg_data ['signup_failed'],
										"status" => - 1 
								);
							}
						}
					} else {
						$message = array (
								"message" => $msg_data ['invalid_user'],
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => 'Invalid Email',
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	
	/**
	 * get the passenger profile info
	 */
	public function getProfileDetail_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$passenger_data = NULL;
		
		$response_data = array ();
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$passenger_id = NULL;
		
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$passenger_id = $post_data->passenger_id;
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual functionality
				if ($passenger_id) {
					$passenger_data = $this->Passenger_Model->getById ( $passenger_id );
					if ($passenger_data) {
						$response_data = array (
								'passenger_details' => array (
										'profile_image' => ($passenger_data->profileImage) ? passenger_content_url ( $passenger_id . '/' . $passenger_data->profileImage ) : image_url ( 'app/user.png' ),
										'first_name' => $passenger_data->firstName,
										'last_name' => $passenger_data->lastName,
										'email' => $passenger_data->email,
										'referral_code' => $passenger_data->passengerCode,
										'wallet_amount' => $passenger_data->walletAmount,
										'mobile' => $passenger_data->mobile 
								) 
						);
						
						$message = array (
								"message" => $msg_data ['success'],
								"detail" => array (
										$response_data 
								),
								"status" => 1 
						);
					} else {
						$message = array (
								"message" => $msg_data ['invalid_user'],
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['invalid_user'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	public function getNearByTaxiList_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$passenger_exists = NULL;
		$available_taxis = NULL;
		$available_taxi_list = array ();
		$response_data = array ();
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$passenger_id = NULL;
		$latitude = NULL;
		$longitude = NULL;
		$app_version = 0.0;
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$passenger_id = $post_data->passenger_id;
		$latitude = $post_data->latitude;
		$longitude = $post_data->longitude;
		if (array_key_exists ( 'app_version', $post_data )) {
			$app_version = $post_data->app_version;
		}
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual functionality
				
				if ($latitude & $longitude) {
					$passenger_exists = $this->Passenger_Model->getById ( $passenger_id );
					// get passenger details
					$update_app_version = $this->Passenger_Model->update ( array (
							'appVersion' => $app_version 
					), array (
							'mobile' => $passenger_exists->mobile 
					) );
					$passenger_details = $this->Passenger_Model->getById ( $passenger_id );
					
					$distance = 0;
					if (TimeIsBetweenTwoTimes ( DAY_START_TIME, DAY_END_TIME, getCurrentDateTime () )) {
						$distance = RIDE_NOW_LIMIT_DAY_ONE;
					} else if (TimeIsBetweenTwoTimes ( NIGHT_START_TIME, NIGHT_END_TIME, getCurrentDateTime () )) {
						$distance = RIDE_NOW_LIMIT_NIGHT_ONE;
					}
					$available_taxis = $this->Passenger_Api_Webservice_Model->getNearestTaxiList ( $latitude, $longitude, $passenger_details->entityId, $distance = 2 );
					if (! $available_taxis) {
						$distance = 0;
						if (TimeIsBetweenTwoTimes ( DAY_START_TIME, DAY_END_TIME, getCurrentDateTime () )) {
							$distance = RIDE_NOW_LIMIT_DAY_TWO;
						} else if (TimeIsBetweenTwoTimes ( NIGHT_START_TIME, NIGHT_END_TIME, getCurrentDateTime () )) {
							$distance = RIDE_NOW_LIMIT_NIGHT_TWO;
						}
						$available_taxis = $this->Passenger_Api_Webservice_Model->getNearestTaxiList ( $latitude, $longitude, $passenger_details->entityId, $distance = 2 );
					}
					
					if ($available_taxis > 0) {
						foreach ( $available_taxis as $list ) {
							
							$available_taxi_list [] = array (
									'latitude' => $list->currentLatitude,
									'longitude' => $list->currentLongitude,
									'bearing' => $list->currentBearing,
									'availablity_status' => ($list->availabilityStatus == Taxi_Available_Status_Enum::BUSY) ? Taxi_Available_Status_Enum::BUSYSTATUS : Taxi_Available_Status_Enum::FREESTATUS,
									'driver_id' => $list->driverId,
									'driver_name' => $list->firstName . " " . $list->lastName,
									'driver_mobile' => $list->mobile,
									'distance_away' => round ( $list->distance, 3 ) 
							);
						}
						$message = array (
								"message" => $msg_data ['hellocab_notice'],
								"taxi_list" => $available_taxi_list,
								"status" => 1 
						);
					} else {
						$message = array (
								"message" => $msg_data ['no_driver'],
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['lat_long_mandatory'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	public function taxiBooking_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$promocode_exists = NULL;
		$promocode_limit_exists = NULL;
		$job_card_id = NULL;
		$trip_id = NULL;
		$taxi_request_details = NULL;
		$customer_reference_code = NULL;
		$passenger_data = array ();
		$insert_data = array ();
		$data = array ();
		$zone_details = NULL;
		$is_fifo_zone_id = 0;
		$promo_code_validated = array (
				"message" => '',
				'status' => - 1 
		);
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$passenger_id = NULL;
		$promo_code = NULL;
		$zone_id = 0;
		$payment_mode = NULL;
		$trip_type = NULL;
		$taxi_category_type = Null;
		$pickup_location = NULL;
		$pickup_latitude = NULL;
		$pickup_longitude = NULL;
		$pickup_time = NULL;
		$drop_location = NULL;
		$drop_latitude = NULL;
		$drop_longitude = NULL;
		$landmark = NULL;
		$estimated_distance = 0;
		$estimated_time = 0;
		
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$promo_code = ($post_data->promo_code) ? $post_data->promo_code : '';
		
		if (array_key_exists ( 'passenger_id', $post_data ) && $post_data->passenger_id > 0) {
			$passenger_id = $post_data->passenger_id;
		}
		if (array_key_exists ( 'zone_id', $post_data ) && $post_data->zone_id > 0) {
			$zone_id = $post_data->zone_id;
		}
		$payment_mode = $post_data->payment_mode;
		$trip_type = $post_data->trip_type;
		$taxi_category_type = $post_data->taxi_category_type;
		$pickup_location = $post_data->pickup_location;
		$pickup_latitude = $post_data->pickup_latitude;
		$pickup_longitude = $post_data->pickup_longitude;
		$pickup_time = $post_data->pickup_time;
		$drop_location = $post_data->drop_location;
		$drop_latitude = $post_data->drop_latitude;
		$drop_longitude = $post_data->drop_longitude;
		$landmark = $post_data->landmark;
		if (array_key_exists ( 'estimated_distance', $post_data )) {
			$estimated_distance = $post_data->estimated_distance;
		}
		if (array_key_exists ( 'estimated_time', $post_data )) {
			$estimated_time = $post_data->estimated_time;
		}
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// PickUp Zone ID
				if ($pickup_latitude && $pickup_longitude) {
					$zone_id = getZoneByLatLong ( $pickup_latitude, $pickup_longitude );
				}
				if (! $zone_id) {
					$zone_id = DEFAULT_ZONE_ID;
				}
				// Drop Zone ID
				if ($drop_latitude && $drop_longitude) {
					$drop_zone_id = getZoneByLatLong ( $drop_latitude, $drop_longitude );
				}
				if (! $drop_zone_id) {
					$drop_zone_id = DEFAULT_ZONE_ID;
				}
				// To get trip estimated distance and time from google API
				if (($estimated_distance<=0 || $estimated_time<=0) || ($estimated_distance == '' || $estimated_time == '')) {
					$URL = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=" . $pickup_latitude . "," . $pickup_longitude . "&destinations=" . $drop_latitude . "," . $drop_longitude . "&key=" . GOOGLE_MAP_API_KEY;
					
					$curl = curl_init ();
					curl_setopt ( $curl, CURLOPT_URL, $URL );
					curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, true );
					curl_setopt ( $curl, CURLOPT_HEADER, false );
					$result = curl_exec ( $curl );
					curl_close ( $curl );
					// return $result; ;
					
					$response = json_decode ( $result );
					if ($response) {
						if ($response->status) {
							if ($response->rows [0]->elements [0]->distance->text) {
								$distance = explode ( ' ', $response->rows [0]->elements [0]->distance->text );
								if ($distance [1] == 'm') {
									$estimated_distance = round ( ($distance [0] / 1000), 1 );
								} else if ($distance [1] == 'km') {
									$estimated_distance = round ( ($distance [0]), 1 );
								}
							}
							if ($response->rows [0]->elements [0]->duration->text) {
								$time = explode ( ' ', $response->rows [0]->elements [0]->duration->text );
								
								if ($time [1] == 'hour' || $time [1] == 'hours') {
									$estimated_time += ($time [0] * 60) + $time [2];
								} else if ($time [1] == 'mins') {
									$estimated_time = $time [0];
								}
							}
						}
					}
				}
				// To get trip estimated distance and time from google API
				
				if ($zone_id) {
					$zone_details = $this->Zone_Details_Model->getById ( $zone_id );
					if ($zone_details) {
						if ($zone_details->dispatchType == Dispatch_Type_Enum::FIFO) {
							$is_fifo_zone_id = 1;
						}
					}
					// actual functionality
					$current_timestamp = getCurrentUnixDateTime ();
					if ($trip_type == Trip_Type_Enum::NOW) {
						$pickup_time = getCurrentUnixDateTime ();
						$pickup_time = $pickup_time + (60 * 2);
						$pickup_time = date ( "Y-m-d H:i:s", $pickup_time );
					}
					
					if (strtotime ( $pickup_time ) >= $current_timestamp) {
						if ($passenger_id && $pickup_latitude && $pickup_longitude && $trip_type && $pickup_time) {
							// Check promocode exists or not
							
							// Set zone Id by pickup latitude and longitude else set 1
							
							$passenger_details = $this->Passenger_Model->getById ( $passenger_id );
							$entity_details = $this->Entity_Model->getById ( $passenger_details->entityId );
							$customer_reference_code = str_pad ( ( string ) rand ( 0, 9999 ), 4, '0', STR_PAD_LEFT );
							// insert trip data to trip details table 'promoCode'=>$promocode
							if ($pickup_latitude != '0' || empty ( $pickup_latitude ) && $pickup_longitude != '0' || empty ( $pickup_longitude )) {
								if ($promo_code) {
									$promo_code_validated = $this->validatePromoCode ( $promo_code, $passenger_id, $zone_id, $drop_zone_id, DEFAULT_COMPANY_ID );
								}
								
								if ($promo_code_validated ['status'] > 0 || $promo_code === NULL || $promo_code == '') {
									$insert_data = array (
											'passengerId' => $passenger_id,
											'entityId' => $passenger_details->entityId,
											'zoneId' => $zone_id,
											'dropZoneId' => $drop_zone_id,
											'pickupLocation' => $pickup_location,
											'pickupLatitude' => $pickup_latitude,
											'pickupLongitude' => $pickup_longitude,
											'dropLocation' => $drop_location,
											'dropLatitude' => $drop_latitude,
											'dropLongitude' => $drop_longitude,
											'pickupDatetime' => $pickup_time,
											'tripStatus' => Trip_Status_Enum::BOOKED,
											'notificationStatus' => Trip_Status_Enum::BOOKED,
											'driverAcceptedStatus' => Driver_Accepted_Status_Enum::NONE,
											'tripType' => $trip_type,
											'bookedFrom' => Signup_Type_Enum::MOBILE,
											'customerReferenceCode' => $customer_reference_code,
											'paymentMode' => $payment_mode,
											'taxiCategoryType' => $taxi_category_type,
											'promoCode' => $promo_code,
											'landmark' => $landmark,
											'estimatedDistance' => $estimated_distance,
											'estimatedTime' => $estimated_time 
									);
									$trip_id = $this->Trip_Details_Model->insert ( $insert_data );
									
									
									// update the Job card id
									$trip_job_id = (($trip_id <= 999999)) ? str_pad ( ( string ) $trip_id, 6, '0', STR_PAD_LEFT ) : $trip_id;
									
									$job_card_id = $entity_details->entityPrefix . '' . $trip_job_id;
									$update_job_card_id = $this->Trip_Details_Model->update ( array (
											'jobCardId' => $job_card_id 
									), array (
											'id' => $trip_id 
									) );
									
									
									
									if ($trip_id) {
										// To get the passenger mobile & email
										$passenger_data = $this->Passenger_Model->getById ( $passenger_id );
										$entity_config_details = $this->Entity_Config_Model->getOneByKeyValueArray ( array (
												'entityId' => $passenger_data->entityId 
										) );
										if ($trip_type == Trip_Type_Enum::LATER) {
											if ($entity_config_details->sendSms && count ( $passenger_data ) > 0) {
												$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
														'title' => 'drive_later_booking_sms' 
												) );
												$message_data = $message_details->content;
												
												$message_data = str_replace ( "##PASSENGERNAME##", $passenger_data->firstName . ' ' . $passenger_data->lastName, $message_data );
												$message_data = str_replace ( "##JOBCARDID##", $job_card_id, $message_data );
												$message_data = str_replace ( "##PICKUP_TIME##", $pickup_time, $message_data );
												// $message_data = str_replace ( "##CRN##", $customer_reference_code, $message_data );
												
												if ($passenger_data->mobile != "") {
													// sending SMS is Stopped on request by Anirban by Email on 19/02/108
													// $this->Common_Api_Webservice_Model->sendSMS ( $passenger_data->mobile, $message_data );
												}
											}
										}
										if ($trip_type == Trip_Type_Enum::NOW) {
											
											if ($entity_config_details->sendSms && count ( $passenger_data ) > 0) {
												$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
														'title' => 'drive_now_booking_sms' 
												) );
												$message_data = $message_details->content;
												
												$message_data = str_replace ( "##PASSENGERNAME##", $passenger_data->firstName . ' ' . $passenger_data->lastName, $message_data );
												$message_data = str_replace ( "##JOBCARDID##", $job_card_id, $message_data );
												// $message_data = str_replace ( "##CRN##", $customer_reference_code, $message_data );
												
												if ($passenger_data->mobile != "") {
													// sending SMS is Stopped on request by Anirban by Email on 19/02/108
													// $this->Common_Api_Webservice_Model->sendSMS ( $passenger_data->mobile, $message_data );
												}
											}
										}
										$response_data = array (
												'trip_detail' => array (
														"trip_id" => $trip_id,
														"trip_type" => $trip_type,
														"booking_date_time" => getCurrentDateTime (),
														"pickup_location" => $pickup_location,
														"drop_location" => $drop_location,
														"pickup_time" => $pickup_time,
														"taxi_category_type" => $taxi_category_type,
														"crn_no" => $customer_reference_code,
														"job_card_id" => $job_card_id 
												) 
										);
										
										/*
										 * $message = array (
										 * "message" => $msg_data ['booking_success'],
										 * "status" => 1,
										 * "detail"=>array($response_data)
										 * );
										 */
										
										// to get Taxi for trip booked on immediately it does not matter of pickup time
										if ($trip_type == Trip_Type_Enum::NOW) {
											$taxi_request_details = $this->getTaxiForTrip ( $trip_id, $is_fifo_zone_id );
											
											/*
											 * if ($taxi_request_details)
											 * {
											 * $driver_dispatched=$this->tripDispatchDriverNotification($taxi_request_details[0]->tripId,$taxi_request_details[0]->driverId,$taxi_request_details[0]->distance);
											 *
											 * }
											 * if (!$taxi_request_details)
											 * {
											 */
											$taxi_request_details = $trip_id;
											// }
											if ($taxi_request_details) {
												
												/*
												 * if ($driver_dispatched)
												 * {
												 * $message = array (
												 * "message" => $msg_data ['driver_dispatched'],
												 * "status" => 1,
												 * "detail"=>array($response_data)
												 * );
												 * }
												 * else
												 * {
												 */
												$message = array (
														"message" => $msg_data ['booking_success'],
														"status" => 1,
														"detail" => array (
																$response_data 
														) 
												);
												// }
											} else {
												$message = array (
														"message" => $msg_data ['driver_not_found'],
														"status" => - 1 
												);
												/*
												 * $message = array (
												 * "message" => $msg_data ['booking_success'],
												 * "status" => 1,
												 * "detail"=>array($response_data)
												 * );
												 */
											}
										} else {
											$message = array (
													"message" => $msg_data ['booking_success'],
													"status" => 1,
													"detail" => array (
															$response_data 
													) 
											);
										}
									} else {
										$message = array (
												"message" => $msg_data ['booking_failed'],
												"status" => - 1 
										);
									}
								} else {
									$message = $promo_code_validated;
								}
							} else {
								$message = array (
										"message" => $msg_data ['lat_long_mandatory'],
										"status" => - 1 
								);
							}
						} else {
							$message = array (
									"message" => $msg_data ['mandatory_data'],
									"status" => - 1 
							);
						}
					} else {
						$message = array (
								"message" => $msg_data ['invalid_pickup_time'],
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['invalid_zone'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	public function tripReject_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$check_trip_avilablity = NULL;
		
		$trip_details = NULL;
		$response_data = array ();
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$reject_reason = NULL;
		$trip_id = NULL;
		
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$trip_id = $post_data->trip_id;
		$reject_reason = $post_data->reject_reason;
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual functionality
				if ($trip_id) {
					// get trip details
					$trip_details = $this->Trip_Details_Model->getById ( $trip_id );
					$trip_details = $this->Common_Api_Webservice_Model->getTripDetails ( $trip_id, $trip_details->passengerId );
					// check trip avilable for driver/any in progress trip before logout
					$check_trip_avilablity = $this->Taxi_Request_Details_Model->getOneByKeyValueArray ( array (
							'tripId' => $trip_id 
					), 'id DESC' );
					if ($check_trip_avilablity) {
						if (($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::DRIVER_ACCEPTED) && ($trip_details [0]->tripStatus == Trip_Status_Enum::IN_PROGRESS || $trip_details [0]->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYMENT)) {
							$message = array (
									"message" => $msg_data ['trip_progress'],
									"status" => - 1 
							);
						} else if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::PASSENGER_CANCELLED) {
							$message = array (
									"message" => $msg_data ['trip_reject_passenger'],
									"status" => - 1 
							);
						} else if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::COMPLETED_TRIP) {
							$message = array (
									"message" => $msg_data ['trip_completed'],
									"status" => - 1 
							);
						} else if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::DRIVER_REJECTED) {
							$message = array (
									"message" => $msg_data ['trip_reject_driver'],
									"status" => - 1 
							);
						} else if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::DRIVER_NOT_FOUND) {
							$message = array (
									"message" => $msg_data ['driver_not_found'],
									"status" => - 1 
							);
						} else if (($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::DRIVER_ACCEPTED || $check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::AVAILABLE_TRIP) && ($trip_details [0]->tripStatus == Trip_Status_Enum::DRIVER_ACCEPTED || $trip_details [0]->tripStatus == Trip_Status_Enum::DRIVER_DISPATCHED || $trip_details [0]->tripStatus == Trip_Status_Enum::DRIVER_ARRIVED)) {
							
							$trip_status_update = $this->Trip_Details_Model->update ( array (
									'tripStatus' => Trip_Status_Enum::CANCELLED_BY_PASSENGER,
									'notificationStatus' => Trip_Status_Enum::CANCELLED_BY_PASSENGER,
									'passengerRejectComments' => $reject_reason 
							), array (
									'id' => $trip_id 
							) );
							
							$trip_request_status_update = $this->Taxi_Request_Details_Model->update ( array (
									'taxiRequestStatus' => Taxi_Request_Status_Enum::PASSENGER_CANCELLED 
							), array (
									'tripId' => $trip_id 
							) );
							// @todo update driiver rejected count $ consecutive reject count of driver to driver dispath details table
							$driver_dispatch_exists = NULL;
							$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
									'tripId' => $trip_id 
							) );
							if ($driver_dispatch_exists) {
								$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
										'tripId' => Status_Type_Enum::INACTIVE,
										'availabilityStatus' => Taxi_Available_Status_Enum::FREE,
										'isEligible' => Status_Type_Enum::ACTIVE 
								), array (
										'tripId' => $trip_id 
								) );
							}
							
							// get last inserted shift id for particular driver
							$get_last_shift_id = $this->Driver_Shift_History_Model->getOneByKeyValueArray ( array (
									'driverId' => $trip_details [0]->driverId 
							), 'id DESC' );
							$driver_shift_status_update = $this->Driver_Shift_History_Model->update ( array (
									'availabilityStatus' => Taxi_Available_Status_Enum::FREE 
							), array (
									'id' => $get_last_shift_id->id 
							) );
							if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::DRIVER_ACCEPTED && $trip_details [0]->tripStatus == Trip_Status_Enum::DRIVER_ACCEPTED || $trip_details [0]->tripStatus == Trip_Status_Enum::DRIVER_ARRIVED) {
								// update the penalty trip reject count by one
								$passenger_details = $this->Passenger_Model->getById ( $trip_details [0]->passengerId );
								$penalty_trip_count = ++ $passenger_details->penaltyTripCount;
								$update_penalty_count = $this->Passenger_Model->update ( array (
										'penaltyTripCount' => $penalty_trip_count 
								), array (
										'mobile' => $passenger_details->mobile 
								) );
							}
							$entity_config_details = $this->Entity_Config_Model->getOneByKeyValueArray ( array (
									'entityId' => $trip_details [0]->entityId 
							) );
							// sms & email start
							if ($entity_config_details->sendSms && $trip_request_status_update) {
								// SMS To driver
								
								$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
										'title' => 'passenger_cancelled_driver_sms' 
								) );
								
								$message_data = $message_details->content;
								$message_data = str_replace ( "##JOBCARDID##", $trip_details [0]->jobCardId, $message_data );
								if ($trip_details [0]->driverMobile) {
									
									$this->Common_Api_Webservice_Model->sendSMS ( $trip_details [0]->driverMobile, $message_data );
								}
							}
							
							// sms & email end
							$message = array (
									"message" => $msg_data ['passenger_cancel_success'],
									"status" => 1 
							);
						} else {
							echo "=========";
							debug_exit ( $trip_id );
							$message = array (
									"message" => $msg_data ['invalid_trip'],
									"status" => - 1 
							);
						}
					} else {
						if ($trip_details) {
							$trip_status_update = $this->Trip_Details_Model->update ( array (
									'tripStatus' => Trip_Status_Enum::CANCELLED_BY_PASSENGER,
									'notificationStatus' => Trip_Status_Enum::CANCELLED_BY_PASSENGER,
									'passengerRejectComments' => $reject_reason 
							), array (
									'id' => $trip_id 
							) );
							
							$entity_config_details = $this->Entity_Config_Model->getOneByKeyValueArray ( array (
									'entityId' => $trip_details [0]->entityId 
							) );
							// sms & email start
							if ($entity_config_details->sendSms && $trip_status_update) {
								// SMS To driver
								
								$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
										'title' => 'passenger_cancelled_driver_sms' 
								) );
								
								$message_data = $message_details->content;
								$message_data = str_replace ( "##JOBCARDID##", $trip_details [0]->jobCardId, $message_data );
								if ($trip_details [0]->driverMobile) {
									
									$this->Common_Api_Webservice_Model->sendSMS ( $trip_details [0]->driverMobile, $message_data );
								}
							}
							
							// sms & email end
							$message = array (
									"message" => $msg_data ['passenger_cancel_success'],
									"status" => 1 
							);
						} else {
							
							$message = array (
									"message" => $msg_data ['invalid_trip'],
									"status" => - 1 
							);
						}
					}
				} else {
					
					$message = array (
							"message" => $msg_data ['invalid_trip'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	public function getTariffDetails_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$entity_id = NULL;
		$passenger_details = NULL;
		$response_data = array ();
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$city_id = NULL;
		$passenger_id = NULL;
		
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$city_id = $post_data->city_id;
		$passenger_id = $post_data->passenger_id;
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				
				$passenger_details = $this->Passenger_Model->getById ( $passenger_id );
				if (array_key_exists ( 'city_id', $post_data ) && $post_data->city_id) {
					$city_id = $post_data->city_id;
				}
				if ($passenger_details) {
					$entity_id = $passenger_details->entityId;
				}
				// To get tax & admin commission entity details
				$partner_entity_details = $this->Partner_Entity_Model->getOneByKeyValueArray ( array (
						'id' => DEFAULT_COMPANY_ID 
				) );
				$entity_details = $this->Entity_Model->getOneByKeyValueArray ( array (
						'id' => $partner_entity_details->entityId 
				) );
				
				// to get rate card details based on companyId passenger/driver
				$rate_card_details = $this->Passenger_Api_Webservice_Model->getRateCardDetails ( $entity_id, $city_id );
				if ($rate_card_details) {
					
					foreach ( $rate_card_details as $fare ) {
						$response_data [] = array (
								'taxi_name' => $fare->rateCardName,
								'taxi_category_type' => $fare->taxiCategoryType,
								'taxi_category_type_name' => $fare->taxiCategoryTypeName,
								'day_base_charge' => $fare->dayBaseCharge,
								'night_base_charge' => $fare->nightBaseCharge,
								'per_km_charge' => $fare->perKmCharge,
								'waiting_charge' => $fare->waitingCharge,
								'service_tax' => $entity_details->tax 
						);
					}
					
					$message = array (
							"message" => $msg_data ['success'],
							"taxi_fare_details" => $response_data,
							"status" => 1 
					);
				} else {
					
					$message = array (
							"message" => $msg_data ['tariff_failed'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	public function tripUpdateCron_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		
		$passenger_id = NULL;
		$trip_id = NULL;
		$latitude = NULL;
		$longitude = NULL;
		$request_type = NULL;
		$passenger_exists = NULL;
		$trip_details = NULL;
		$passenger_exists = NULL;
		$arrived_display = NULL;
		$tripstart_display = NULL;
		$trip_complete_display = NULL;
		$tripfare_update_display = NULL;
		$driver_cancel_display = NULL;
		$trip_complete_paytm_display = NULL;
		$payment_confirmed_display = NULL;
		
		$response_data = array ();
		
		// Posted json data
		$post_data = $this->get_post_data ();
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$passenger_id = $post_data->passenger_id;
		
		if (array_key_exists ( 'latitude', $post_data )) {
			$latitude = $post_data->latitude;
		}
		if (array_key_exists ( 'longitude', $post_data )) {
			$longitude = $post_data->longitude;
		}
		
		$trip_id = $post_data->trip_id;
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual funtioanality
				$passenger_exists = $this->Passenger_Model->getById ( $passenger_id );
				if ($passenger_id) {
					if ($latitude && $longitude) {
						$update_passenger_location = $this->Passenger_Model->update ( array (
								'currentLatitude' => $latitude,
								'currentLongitude' => $longitude 
						), array (
								'mobile' => $passenger_exists->mobile 
						) );
					}
					$trip_details = $this->Common_Api_Webservice_Model->getTripDetails ( $trip_id, $passenger_id );
					if (count ( $trip_details ) > 0) {
						if ($trip_details [0]->tripStatus == Trip_Status_Enum::CANCELLED_BY_PASSENGER) {
							$passenger_cancel_display = ($trip_details [0]->tripStatus == Trip_Status_Enum::CANCELLED_BY_PASSENGER && $trip_details [0]->notificationStatus == Trip_Status_Enum::CANCELLED_BY_PASSENGER) ? 1 : 0;
							$message = array (
									"message" => $msg_data ['trip_reject_passenger'],
									"driver_id" => $trip_details [0]->driverId,
									"driver_name" => $trip_details [0]->driverFirstName . ' ' . $trip_details [0]->driverLastName,
									'driver_mobile' => $trip_details [0]->driverMobile,
									"driver_profile_image" => ($trip_details [0]->driverProfileImage) ? (driver_content_url ( $trip_details [0]->driverId . '/' . $trip_details [0]->driverProfileImage )) : image_url ( 'app/user.png' ),
									"driver_latitude" => $trip_details [0]->currentLatitude,
									"driver_longitude" => $trip_details [0]->currentLongitude,
									"driver_bearing" => $trip_details [0]->currentBearing,
									"driver_last_updated" => $trip_details [0]->driverLastUpdated,
									"status" => 9,
									"display" => $passenger_cancel_display 
							);
							$trip_status_changed = $this->Trip_Details_Model->update ( array (
									'notificationStatus' => Trip_Status_Enum::NO_NOTIFICATION 
							), array (
									'id' => $trip_id 
							) );
						} else if ($trip_details [0]->tripStatus == Trip_Status_Enum::BOOKED && $trip_details [0]->driverAcceptedStatus == Driver_Accepted_Status_Enum::NONE) {
							$message = array (
									"message" => $msg_data ['trip_not_started'],
									"driver_id" => $trip_details [0]->driverId,
									"driver_name" => $trip_details [0]->driverFirstName . ' ' . $trip_details [0]->driverLastName,
									'driver_mobile' => $trip_details [0]->driverMobile,
									"driver_profile_image" => ($trip_details [0]->driverProfileImage) ? (driver_content_url ( $trip_details [0]->driverId . '/' . $trip_details [0]->driverProfileImage )) : image_url ( 'app/user.png' ),
									"driver_latitude" => $trip_details [0]->currentLatitude,
									"driver_longitude" => $trip_details [0]->currentLongitude,
									"driver_bearing" => $trip_details [0]->currentBearing,
									"driver_last_updated" => $trip_details [0]->driverLastUpdated,
									"crn_no" => $trip_details [0]->customerReferenceCode,
									"status" => 6 
							);
						} else if ($trip_details [0]->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYMENT) {
							$trip_complete_display = ($trip_details [0]->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYMENT && $trip_details [0]->notificationStatus == Trip_Status_Enum::WAITING_FOR_PAYMENT) ? 1 : 0;
							$message = array (
									"message" => $msg_data ['trip_waiting_payment'],
									"driver_id" => $trip_details [0]->driverId,
									"driver_name" => $trip_details [0]->driverFirstName . ' ' . $trip_details [0]->driverLastName,
									'driver_mobile' => $trip_details [0]->driverMobile,
									"driver_profile_image" => ($trip_details [0]->driverProfileImage) ? (driver_content_url ( $trip_details [0]->driverId . '/' . $trip_details [0]->driverProfileImage )) : image_url ( 'app/user.png' ),
									"driver_latitude" => $trip_details [0]->currentLatitude,
									"driver_longitude" => $trip_details [0]->currentLongitude,
									"driver_bearing" => $trip_details [0]->currentBearing,
									"driver_last_updated" => $trip_details [0]->driverLastUpdated,
									"taxi_registration_no" => $trip_details [0]->taxiRegistrationNo,
									"crn_no" => $trip_details [0]->customerReferenceCode,
									"pickup_location" => $trip_details [0]->pickupLocation,
									"pickup_latitude" => $trip_details [0]->pickupLatitude,
									"pickup_longitude" => $trip_details [0]->pickupLongitude,
									"pickup_time" => $trip_details [0]->actualPickupDatetime,
									"drop_location" => $trip_details [0]->dropLocation,
									"drop_latitude" => $trip_details [0]->dropLatitude,
									"drop_longitude" => $trip_details [0]->dropLongitude,
									"drop_time" => $trip_details [0]->dropDatetime,
									"trip_type" => $trip_details [0]->tripType,
									"trip_type_name" => $trip_details [0]->tripTypeName,
									"payment_mode" => $trip_details [0]->paymentMode,
									"payment_mode_name" => $trip_details [0]->paymentModeName,
									"base_charge" => $trip_details [0]->baseCharge,
									"travel_charge" => $trip_details [0]->travelCharge,
									"parking_charge" => $trip_details [0]->parkingCharge,
									"tax_charge" => $trip_details [0]->taxCharge,
									"tax_percentage" => $trip_details [0]->taxPercentage,
									"toll_charge" => $trip_details [0]->tollCharge,
									"penalty_charge" => $trip_details [0]->penaltyCharge,
									"booking_charge" => $trip_details [0]->bookingCharge,
									"promo_discount" => $trip_details [0]->promoDiscountAmount,
									"wallet_payment" => $trip_details [0]->walletPaymentAmount,
									"travelled_period" => $trip_details [0]->travelledPeriod,
									"travelled_distance" => number_format ( $trip_details [0]->travelledDistance, 1 ),
									"total_trip_cost" => $trip_details [0]->totalTripCharge,
									"status" => 4,
									"display" => $trip_complete_display 
							);
							$trip_status_changed = $this->Trip_Details_Model->update ( array (
									'notificationStatus' => Trip_Status_Enum::NO_NOTIFICATION 
							), array (
									'id' => $trip_id 
							) );
						} else if ($trip_details [0]->tripStatus == Trip_Status_Enum::TRIP_COMPLETED) {
							$payment_confirmed_display = ($trip_details [0]->tripStatus == Trip_Status_Enum::TRIP_COMPLETED && $trip_details [0]->notificationStatus == Trip_Status_Enum::NO_NOTIFICATION) ? 1 : 0;
							$message = array (
									"message" => $msg_data ['trip_completed'],
									"driver_id" => $trip_details [0]->driverId,
									"driver_name" => $trip_details [0]->driverFirstName . ' ' . $trip_details [0]->driverLastName,
									'driver_mobile' => $trip_details [0]->driverMobile,
									"driver_profile_image" => ($trip_details [0]->driverProfileImage) ? (driver_content_url ( $trip_details [0]->driverId . '/' . $trip_details [0]->driverProfileImage )) : image_url ( 'app/user.png' ),
									"driver_latitude" => $trip_details [0]->currentLatitude,
									"driver_longitude" => $trip_details [0]->currentLongitude,
									"driver_bearing" => $trip_details [0]->currentBearing,
									"driver_last_updated" => $trip_details [0]->driverLastUpdated,
									"taxi_registration_no" => $trip_details [0]->taxiRegistrationNo,
									"crn_no" => $trip_details [0]->customerReferenceCode,
									"pickup_location" => $trip_details [0]->pickupLocation,
									"pickup_latitude" => $trip_details [0]->pickupLatitude,
									"pickup_longitude" => $trip_details [0]->pickupLongitude,
									"pickup_time" => $trip_details [0]->actualPickupDatetime,
									"drop_location" => $trip_details [0]->dropLocation,
									"drop_latitude" => $trip_details [0]->dropLatitude,
									"drop_longitude" => $trip_details [0]->dropLongitude,
									"drop_time" => $trip_details [0]->dropDatetime,
									"trip_type" => $trip_details [0]->tripType,
									"trip_type_name" => $trip_details [0]->tripTypeName,
									"payment_mode" => $trip_details [0]->paymentMode,
									"payment_mode_name" => $trip_details [0]->paymentModeName,
									"base_charge" => $trip_details [0]->baseCharge,
									"travel_charge" => $trip_details [0]->travelCharge,
									"parking_charge" => $trip_details [0]->parkingCharge,
									"tax_charge" => $trip_details [0]->taxCharge,
									"tax_percentage" => $trip_details [0]->taxPercentage,
									"toll_charge" => $trip_details [0]->tollCharge,
									"penalty_charge" => $trip_details [0]->penaltyCharge,
									"booking_charge" => $trip_details [0]->bookingCharge,
									"promo_discount" => $trip_details [0]->promoDiscountAmount,
									"wallet_payment" => $trip_details [0]->walletPaymentAmount,
									"travelled_period" => $trip_details [0]->travelledPeriod,
									"travelled_distance" => number_format ( $trip_details [0]->travelledDistance, 1 ),
									"total_trip_cost" => $trip_details [0]->totalTripCharge,
									"status" => 5,
									"display" => $payment_confirmed_display 
							);
							$trip_status_changed = $this->Trip_Details_Model->update ( array (
									'notificationStatus' => Trip_Status_Enum::TRIP_COMPLETED 
							), array (
									'id' => $trip_id 
							) );
						} else if ($trip_details [0]->tripStatus == Trip_Status_Enum::IN_PROGRESS && $trip_details [0]->driverAcceptedStatus == Driver_Accepted_Status_Enum::ACCEPTED) {
							$driver_avg_rating = $this->Driver_Api_Webservice_Model->getDriverAverageRating ( $trip_details [0]->driverId );
							$tripstart_display = ($trip_details [0]->tripStatus == Trip_Status_Enum::IN_PROGRESS && $trip_details [0]->notificationStatus == Trip_Status_Enum::IN_PROGRESS) ? 1 : 0;
							$message = array (
									"message" => $msg_data ['trip_progress'],
									"driver_id" => $trip_details [0]->driverId,
									"driver_name" => $trip_details [0]->driverFirstName . ' ' . $trip_details [0]->driverLastName,
									'driver_mobile' => $trip_details [0]->driverMobile,
									"driver_profile_image" => ($trip_details [0]->driverProfileImage) ? (driver_content_url ( $trip_details [0]->driverId . '/' . $trip_details [0]->driverProfileImage )) : image_url ( 'app/user.png' ),
									"driver_latitude" => $trip_details [0]->currentLatitude,
									"driver_longitude" => $trip_details [0]->currentLongitude,
									"driver_bearing" => $trip_details [0]->currentBearing,
									"driver_last_updated" => $trip_details [0]->driverLastUpdated,
									"driver_avg_rating" => ($driver_avg_rating [0]->driverRating) ? $driver_avg_rating [0]->driverRating : '0.0',
									"taxi_registration_no" => $trip_details [0]->taxiRegistrationNo,
									"crn_no" => $trip_details [0]->customerReferenceCode,
									"pickup_location" => $trip_details [0]->pickupLocation,
									"pickup_latitude" => $trip_details [0]->pickupLatitude,
									"pickup_longitude" => $trip_details [0]->pickupLongitude,
									"pickup_time" => $trip_details [0]->actualPickupDatetime,
									"drop_location" => $trip_details [0]->dropLocation,
									"drop_latitude" => $trip_details [0]->dropLatitude,
									"drop_longitude" => $trip_details [0]->dropLongitude,
									"drop_time" => $trip_details [0]->dropDatetime,
									"status" => 3,
									"display" => $tripstart_display 
							);
							$trip_status_changed = $this->Trip_Details_Model->update ( array (
									'notificationStatus' => Trip_Status_Enum::WAITING_FOR_PAYMENT 
							), array (
									'id' => $trip_id 
							) );
						} else if ($trip_details [0]->tripStatus == Trip_Status_Enum::DRIVER_ARRIVED && $trip_details [0]->driverAcceptedStatus == Driver_Accepted_Status_Enum::ACCEPTED) {
							$driver_avg_rating = $this->Driver_Api_Webservice_Model->getDriverAverageRating ( $trip_details [0]->driverId );
							$arrived_display = ($trip_details [0]->tripStatus == Trip_Status_Enum::DRIVER_ARRIVED && $trip_details [0]->notificationStatus == Trip_Status_Enum::DRIVER_ARRIVED) ? 1 : 0;
							$message = array (
									"message" => $msg_data ['trip_driver_arrived'],
									"driver_id" => $trip_details [0]->driverId,
									"driver_name" => $trip_details [0]->driverFirstName . ' ' . $trip_details [0]->driverLastName,
									'driver_mobile' => $trip_details [0]->driverMobile,
									"driver_profile_image" => ($trip_details [0]->driverProfileImage) ? (driver_content_url ( $trip_details [0]->driverId . '/' . $trip_details [0]->driverProfileImage )) : image_url ( 'app/user.png' ),
									"driver_latitude" => $trip_details [0]->currentLatitude,
									"driver_longitude" => $trip_details [0]->currentLongitude,
									"driver_bearing" => $trip_details [0]->currentBearing,
									"driver_last_updated" => $trip_details [0]->driverLastUpdated,
									"driver_avg_rating" => ($driver_avg_rating [0]->driverRating) ? $driver_avg_rating [0]->driverRating : '0.0',
									"taxi_registration_no" => $trip_details [0]->taxiRegistrationNo,
									"crn_no" => $trip_details [0]->customerReferenceCode,
									"pickup_location" => $trip_details [0]->pickupLocation,
									"pickup_latitude" => $trip_details [0]->pickupLatitude,
									"pickup_longitude" => $trip_details [0]->pickupLongitude,
									"pickup_time" => $trip_details [0]->actualPickupDatetime,
									"drop_location" => $trip_details [0]->dropLocation,
									"drop_latitude" => $trip_details [0]->dropLatitude,
									"drop_longitude" => $trip_details [0]->dropLongitude,
									"drop_time" => $trip_details [0]->dropDatetime,
									"status" => 2,
									"display" => $arrived_display 
							);
							$trip_status_changed = $this->Trip_Details_Model->update ( array (
									'notificationStatus' => Trip_Status_Enum::IN_PROGRESS 
							), array (
									'id' => $trip_id 
							) );
						} else if ($trip_details [0]->tripStatus == Trip_Status_Enum::CANCELLED_BY_DRIVER) {
							$driver_cancel_display = ($trip_details [0]->tripStatus == Trip_Status_Enum::CANCELLED_BY_DRIVER && $trip_details [0]->notificationStatus == Trip_Status_Enum::CANCELLED_BY_DRIVER) ? 1 : 0;
							$message = array (
									"message" => $msg_data ['trip_reject_driver'],
									"driver_id" => $trip_details [0]->driverId,
									"driver_name" => $trip_details [0]->driverFirstName . ' ' . $trip_details [0]->driverLastName,
									'driver_mobile' => $trip_details [0]->driverMobile,
									"driver_profile_image" => ($trip_details [0]->driverProfileImage) ? (driver_content_url ( $trip_details [0]->driverId . '/' . $trip_details [0]->driverProfileImage )) : image_url ( 'app/user.png' ),
									"driver_latitude" => $trip_details [0]->currentLatitude,
									"driver_longitude" => $trip_details [0]->currentLongitude,
									"driver_bearing" => $trip_details [0]->currentBearing,
									"driver_last_updated" => $trip_details [0]->driverLastUpdated,
									"status" => 8,
									"display" => $driver_cancel_display 
							);
							$trip_status_changed = $this->Trip_Details_Model->update ( array (
									'notificationStatus' => Trip_Status_Enum::NO_NOTIFICATION 
							), array (
									'id' => $trip_id 
							) );
						} else if ($trip_details [0]->tripStatus == Trip_Status_Enum::DRIVER_REJECTED_TRIP) {
							$driver_cancel_display = ($trip_details [0]->tripStatus == Trip_Status_Enum::DRIVER_REJECTED_TRIP && $trip_details [0]->notificationStatus == Trip_Status_Enum::DRIVER_REJECTED_TRIP) ? 1 : 0;
							$message = array (
									"message" => $msg_data ['trip_reject_driver'],
									/* "driver_id"=>$trip_details [0]->driverId,
									"driver_name"=>$trip_details [0]->driverFirstName.' '.$trip_details [0]->driverLastName,
									'driver_mobile'=>$trip_details [0]->driverMobile,
									"driver_profile_image"=>($trip_details [0]->driverProfileImage) ? (driver_content_url ( $trip_details [0]->driverId . '/' . $trip_details [0]->driverProfileImage )) : image_url ( 'app/user.png' ),
									"driver_latitude" => $trip_details [0]->currentLatitude,
									"driver_longitude" => $trip_details [0]->currentLongitude,
									"driver_bearing"=>$trip_details [0]->currentBearing,
									"driver_last_updated"=>$trip_details[0]->driverLastUpdated, */
									"status" => 8,
									"display" => $driver_cancel_display 
							);
							$trip_status_changed = $this->Trip_Details_Model->update ( array (
									'notificationStatus' => Trip_Status_Enum::NO_NOTIFICATION 
							), array (
									'id' => $trip_id 
							) );
						} else if ($trip_details [0]->tripStatus == Trip_Status_Enum::DRIVER_ACCEPTED && $trip_details [0]->driverAcceptedStatus == Driver_Accepted_Status_Enum::ACCEPTED) {
							$driver_avg_rating = $this->Driver_Api_Webservice_Model->getDriverAverageRating ( $trip_details [0]->driverId );
							$driver_confirm_display = ($trip_details [0]->tripStatus == Trip_Status_Enum::DRIVER_ACCEPTED && $trip_details [0]->notificationStatus == Trip_Status_Enum::DRIVER_ACCEPTED) ? 1 : 0;
							$message = array (
									"message" => $msg_data ['trip_confirmed'],
									"driver_id" => $trip_details [0]->driverId,
									"driver_name" => $trip_details [0]->driverFirstName . ' ' . $trip_details [0]->driverLastName,
									'driver_mobile' => $trip_details [0]->driverMobile,
									"driver_profile_image" => ($trip_details [0]->driverProfileImage) ? (driver_content_url ( $trip_details [0]->driverId . '/' . $trip_details [0]->driverProfileImage )) : image_url ( 'app/user.png' ),
									"driver_latitude" => $trip_details [0]->currentLatitude,
									"driver_longitude" => $trip_details [0]->currentLongitude,
									"driver_bearing" => $trip_details [0]->currentBearing,
									"driver_last_updated" => $trip_details [0]->driverLastUpdated,
									"driver_avg_rating" => ($driver_avg_rating [0]->driverRating) ? $driver_avg_rating [0]->driverRating : '0.0',
									"taxi_registration_no" => $trip_details [0]->taxiRegistrationNo,
									"crn_no" => $trip_details [0]->customerReferenceCode,
									"pickup_location" => $trip_details [0]->pickupLocation,
									"pickup_latitude" => $trip_details [0]->pickupLatitude,
									"pickup_longitude" => $trip_details [0]->pickupLongitude,
									"pickup_time" => $trip_details [0]->actualPickupDatetime,
									"drop_location" => $trip_details [0]->dropLocation,
									"drop_latitude" => $trip_details [0]->dropLatitude,
									"drop_longitude" => $trip_details [0]->dropLongitude,
									"drop_time" => $trip_details [0]->dropDatetime,
									"status" => 1,
									"display" => $driver_confirm_display 
							);
							$trip_status_changed = $this->Trip_Details_Model->update ( array (
									'notificationStatus' => Trip_Status_Enum::DRIVER_ARRIVED 
							), array (
									'id' => $trip_id 
							) );
						} else if ($trip_details [0]->tripStatus == Trip_Status_Enum::DRIVER_NOT_FOUND) {
							$driver_not_found_display = ($trip_details [0]->tripStatus == Trip_Status_Enum::DRIVER_NOT_FOUND && $trip_details [0]->notificationStatus == Trip_Status_Enum::NO_NOTIFICATION) ? 1 : 0;
							$message = array (
									"message" => $msg_data ['driver_not_found'],
									"driver_id" => $trip_details [0]->driverId,
									"driver_name" => $trip_details [0]->driverFirstName . ' ' . $trip_details [0]->driverLastName,
									'driver_mobile' => $trip_details [0]->driverMobile,
									"driver_profile_image" => ($trip_details [0]->driverProfileImage) ? (driver_content_url ( $trip_details [0]->driverId . '/' . $trip_details [0]->driverProfileImage )) : image_url ( 'app/user.png' ),
									"driver_latitude" => $trip_details [0]->currentLatitude,
									"driver_longitude" => $trip_details [0]->currentLongitude,
									"driver_bearing" => $trip_details [0]->currentBearing,
									"driver_last_updated" => $trip_details [0]->driverLastUpdated,
									"crn_no" => $trip_details [0]->customerReferenceCode,
									"status" => 7,
									"display" => $driver_not_found_display 
							);
							$trip_status_changed = $this->Trip_Details_Model->update ( array (
									'notificationStatus' => Trip_Status_Enum::DRIVER_NOT_FOUND 
							), array (
									'id' => $trip_id 
							) );
						} else if ($trip_details [0]->tripStatus == Trip_Status_Enum::DRIVER_DISPATCHED) {
							
							$driver_dispatch_display = ($trip_details [0]->tripStatus == Trip_Status_Enum::DRIVER_DISPATCHED && $trip_details [0]->notificationStatus == Trip_Status_Enum::DRIVER_DISPATCHED) ? 1 : 0;
							$message = array (
									"message" => $msg_data ['driver_dispatch'],
									"driver_id" => $trip_details [0]->driverId,
									"driver_name" => $trip_details [0]->driverFirstName . ' ' . $trip_details [0]->driverLastName,
									'driver_mobile' => $trip_details [0]->driverMobile,
									"driver_profile_image" => ($trip_details [0]->driverProfileImage) ? (driver_content_url ( $trip_details [0]->driverId . '/' . $trip_details [0]->driverProfileImage )) : image_url ( 'app/user.png' ),
									"driver_latitude" => $trip_details [0]->currentLatitude,
									"driver_longitude" => $trip_details [0]->currentLongitude,
									"driver_bearing" => $trip_details [0]->currentBearing,
									"driver_last_updated" => $trip_details [0]->driverLastUpdated,
									"taxi_registration_no" => $trip_details [0]->taxiRegistrationNo,
									"crn_no" => $trip_details [0]->customerReferenceCode,
									"status" => 10,
									"display" => $driver_dispatch_display 
							);
							$trip_status_changed = $this->Trip_Details_Model->update ( array (
									'notificationStatus' => Trip_Status_Enum::DRIVER_ACCEPTED 
							), array (
									'id' => $trip_id 
							) );
						}
					} else {
						$message = array (
								"message" => $msg_data ['invalid_trip'],
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['mandatory_data'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	public function tripEstimate_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		
		$rate_card_details = NULL;
		$rate_card_distance_slab_details = NULL;
		$rate_card_time_slab_details = NULL;
		$rate_card_surge_slab_details = NULL;
		
		// postdata intialized
		$trip_tracking_update = NULL;
		
		$response_data = array ();
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$total_travelled_minute = NULL;
		$travelled_distance = NULL;
		$taxi_category_type = NULL;
		$pickup_latitude = NULL;
		$pickup_longitude = NULL;
		$drop_latitude = NULL;
		$drop_longitude = NULL;
		$zone_id = 0;
		// Posted json data
		$post_data = $this->get_post_data ();
		
		if (array_key_exists ( 'pickup_latitude', $post_data )) {
			$pickup_latitude = $post_data->pickup_latitude;
		}
		if (array_key_exists ( 'pickup_longitude', $post_data )) {
			$pickup_longitude = $post_data->pickup_longitude;
		}
		if (array_key_exists ( 'drop_latitude', $post_data )) {
			$drop_latitude = $post_data->drop_latitude;
		}
		if (array_key_exists ( 'drop_longitude', $post_data )) {
			$drop_longitude = $post_data->drop_longitude;
		}
		if ($pickup_latitude && $pickup_longitude) {
			$zone_id = getZoneByLatLong ( $pickup_latitude, $pickup_longitude );
			// debug($zone_id);echo "===";
		}
		
		if (! $zone_id) {
			$zone_id = DEFAULT_ZONE_ID;
		}
		$taxi_category_type = ($post_data->taxi_category_type) ? $post_data->taxi_category_type : Taxi_Type_Enum::STANDARD;
		$total_travelled_minute = ($post_data->total_travelled_minute || $post_data->total_travelled_minute == '') ? $post_data->total_travelled_minute : 0;
		$travelled_distance = ($post_data->travelled_distance || $post_data->travelled_distance == '') ? $post_data->travelled_distance + 0.5 : 0;
		
		// To get trip estimated distance and time from google API
		/*
		 * if (($travelled_distance || $total_travelled_minute || $travelled_distance == '' || $total_travelled_minute == '') || ($pickup_latitude && $pickup_longitude && $drop_latitude && $drop_longitude)) {
		 * $URL = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=" . $pickup_latitude . "," . $pickup_longitude . "&destinations=" . $drop_latitude . "," . $drop_longitude . "&key=" . GOOGLE_MAP_API_KEY;
		 *
		 * $curl = curl_init ();
		 * curl_setopt ( $curl, CURLOPT_URL, $URL );
		 * curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, true );
		 * curl_setopt ( $curl, CURLOPT_HEADER, false );
		 * $result = curl_exec ( $curl );
		 * curl_close ( $curl );
		 * // return $result; ;
		 *
		 * $response = json_decode ( $result );
		 *
		 * if ($response->status) {
		 * if ($response->rows [0]->elements [0]->distance->text) {
		 * $distance = explode ( ' ', $response->rows [0]->elements [0]->distance->text );
		 * if ($distance [1] == 'm') {
		 * $travelled_distance = round ( ($distance [0] / 1000), 1 ) + 0.5;
		 * } else if ($distance [1] == 'km') {
		 * $travelled_distance = round ( ($distance [0]), 1 ) + 0.5;
		 * }
		 * }
		 * if ($response->rows [0]->elements [0]->duration->text) {
		 * $time = explode ( ' ', $response->rows [0]->elements [0]->duration->text );
		 *
		 * if ($time [1] == 'hour' || $time [1] == 'hours') {
		 * $total_travelled_minute += ($time [0] * 60) + $time [2];
		 * } else if ($time [1] == 'mins') {
		 * $total_travelled_minute = $time [0];
		 * }
		 * }
		 * }
		 * }
		 */
		// To get trip estimated distance and time from google API
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		// debug($travelled_distance);echo "===";debug($total_travelled_minute);echo "===";
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual functionality
				// consider actual pickup time as current datatime
				$actual_pickup_time = getCurrentDateTime ();
				$convenience_charge = 0;
				$travel_charge = 0;
				$total_trip_cost = 0;
				$travelled_distance_charge = 0;
				$travelled_time_charge = 0;
				
				if ($zone_id && $taxi_category_type) {
					
					$passenger_details = $this->Passenger_Model->getById ( $auth_user_id );
					// get rate card details based on the cityId & trip type
					// debug($zone_id);
					$rate_card_details = $this->Rate_Card_Details_Model->getOneByKeyValueArray ( array (
							'zoneId' => $zone_id,
							'entityId' => $passenger_details->entityId,
							'taxiCategoryType' => $taxi_category_type,
							'status' => Status_Type_Enum::ACTIVE,
							'isDeleted' => Status_Type_Enum::INACTIVE 
					) );
					if (! $rate_card_details) {
						
						$rate_card_details = $this->Rate_Card_Details_Model->getOneByKeyValueArray ( array (
								'zoneId' => Status_Type_Enum::INACTIVE,
								'entityId' => $passenger_details->entityId,
								'taxiCategoryType' => $taxi_category_type,
								'status' => Status_Type_Enum::ACTIVE,
								'isDeleted' => Status_Type_Enum::INACTIVE 
						) );
					}
					if (! $rate_card_details) {
						
						$rate_card_details = $this->Rate_Card_Details_Model->getOneByKeyValueArray ( array (
								'zoneId' => $zone_id,
								'entityId' => Status_Type_Enum::INACTIVE,
								'taxiCategoryType' => $taxi_category_type,
								'status' => Status_Type_Enum::ACTIVE,
								'isDeleted' => Status_Type_Enum::INACTIVE 
						) );
					}
					if (! $rate_card_details) {
						
						$rate_card_details = $this->Rate_Card_Details_Model->getOneByKeyValueArray ( array (
								'zoneId' => Status_Type_Enum::INACTIVE,
								'entityId' => Status_Type_Enum::INACTIVE,
								'taxiCategoryType' => $taxi_category_type,
								'status' => Status_Type_Enum::ACTIVE,
								'isDeleted' => Status_Type_Enum::INACTIVE 
						) );
					}
					
					// to get convenience charge based pickup time day/night
					
					if (TimeIsBetweenTwoTimes ( DAY_START_TIME, DAY_END_TIME, $actual_pickup_time )) {
						
						$convenience_charge += $rate_card_details->dayConvenienceCharge;
					} else if (TimeIsBetweenTwoTimes ( NIGHT_START_TIME, NIGHT_END_TIME, $actual_pickup_time )) {
						$convenience_charge += $rate_card_details->nightConvenienceCharge;
					}
					// to calculate the distance slab trip journey amount
					$rate_card_distance_slab_details = $this->Rate_Card_Slab_Details_Model->getByKeyValueArray ( array (
							'rateCardId' => $rate_card_details->id,
							'slabType' => Slab_Type_Enum::DISTANCE 
					) );
					if ($travelled_distance) {
						$available_travelled_distance = $travelled_distance;
						$calculated_travelled_distance = 0;
						$calculate_to_distance = 0;
						foreach ( $rate_card_distance_slab_details as $rate_distance ) {
							if ($available_travelled_distance) {
								if ($rate_distance->slabEnd) {
									
									if (! $rate_distance->slabStart) {
										$calculate_to_distance = $rate_distance->slabEnd;
									} else {
										$calculate_to_distance = ($rate_distance->slabEnd - $rate_distance->slabStart);
									}
									$calculated_travelled_distance += $calculate_to_distance;
									// $available_travelled_distance -= $calculate_to_distance;
									if ($available_travelled_distance <= $calculate_to_distance) {
										$calculate_to_distance = $available_travelled_distance;
										$available_travelled_distance = 0;
									} else {
										$available_travelled_distance -= $calculate_to_distance;
									}
								} else if ($rate_distance->slabStart && ! $rate_distance->slabEnd) {
									$calculate_to_distance = $available_travelled_distance;
									
									$calculated_travelled_distance += $calculate_to_distance;
									$available_travelled_distance -= $calculate_to_distance;
								}
								
								if ($calculate_to_distance) {
									if ($rate_distance->slabCharge) {
										
										$travelled_distance_charge += $calculate_to_distance * $rate_distance->slabCharge;
										$calculate_to_distance = 0;
									}
								}
							}
						}
					}
					// /add to travel charge
					$travel_charge += $travelled_distance_charge;
					
					// to calculate the time slab trip journey amount
					$rate_card_time_slab_details = $this->Rate_Card_Slab_Details_Model->getByKeyValueArray ( array (
							'rateCardId' => $rate_card_details->id,
							'slabType' => Slab_Type_Enum::TIME 
					) );
					if ($total_travelled_minute) {
						
						$available_travelled_minute = $total_travelled_minute;
						$calculated_travelled_minute = 0;
						$calculate_to_minute = 0;
						foreach ( $rate_card_time_slab_details as $rate_time ) {
							if ($available_travelled_minute) {
								if ($rate_time->slabEnd) {
									
									if (! $rate_time->slabStart) {
										$calculate_to_minute = $rate_time->slabEnd;
									} else {
										$calculate_to_minute = ($rate_time->slabEnd - $rate_time->slabStart);
									}
									$calculated_travelled_minute += $calculate_to_minute;
									// $available_travelled_distance -= $calculate_to_distance;
									if ($available_travelled_minute <= $calculate_to_minute) {
										$calculate_to_minute = $available_travelled_minute;
										$available_travelled_minute = 0;
									} else {
										$available_travelled_minute -= $calculate_to_minute;
									}
								} else if ($rate_time->slabStart && ! $rate_time->slabEnd) {
									$calculate_to_minute = $available_travelled_minute;
									
									$calculated_travelled_minute += $calculate_to_minute;
									$available_travelled_minute -= $calculate_to_minute;
								}
								
								if ($calculate_to_minute) {
									if ($rate_time->slabCharge) {
										
										$travelled_time_charge += $calculate_to_minute * $rate_time->slabCharge;
										$calculate_to_minute = 0;
									}
								}
							}
						}
					}
					
					// /add to travel charge
					$travel_charge += $travelled_time_charge;
					
					// to calculate the surge slab trip journey amount
					$rate_card_time_slab_details = $this->Rate_Card_Slab_Details_Model->getByKeyValueArray ( array (
							'rateCardId' => $rate_card_details->id,
							'slabType' => Slab_Type_Enum::SURGE 
					) );
					if ($rate_card_surge_slab_details) {
						foreach ( $rate_card_surge_slab_details as $rate_surge ) {
							$slab_start_time = $rate_surge->slabStart;
							$slab_end_time = $rate_surge->slabEnd;
							// $user_st=$user_et="2017-08-02 09:29:12";
							if (TimeIsBetweenTwoTimes ( $slab_start_time, $slab_end_time, $actual_pickup_time )) {
								// if (((strtotime ( $actual_pickup_time ) >= strtotime ( $slab_start_time )) && (strtotime ( $actual_pickup_time ) <= strtotime ( $slab_end_time ))) || (strtotime ( $actual_pickup_time ) >= strtotime ( $slab_start_time )) && (strtotime ( $actual_pickup_time ) <= strtotime ( $slab_end_time ))) {
								if ($rate_surge->slabChargeType == Payment_Type_Enum::AMOUNT) {
									$surge_charge = $rate_surge->slabCharge;
									$total_trip_cost += $surge_charge;
									break;
								} else if ($rate_surge->slabChargeType == Payment_Type_Enum::PERCENTAGE) {
									$surge_charge = ($rate_surge->slabCharge / 100) * ($convenience_charge + $travel_charge);
									$surge_charge = round ( $surge_charge, 2 );
									$total_trip_cost += $surge_charge;
									break;
								}
							}
						}
					}
					
					$total_trip_cost = $convenience_charge + $travel_charge;
					$total_trip_cost = round ( $total_trip_cost, 2 );
					$response_data = array (
							"travel_charge" => $travel_charge,
							"total_trip_cost" => round ( $total_trip_cost / 100 ) * 100,
							"base_charge" => $convenience_charge,
							"distance" => $travelled_distance 
					);
					$message = array (
							"message" => 'Success',
							"trip_estimate_detail" => $response_data,
							"status" => 1 
					);
				} else {
					
					$message = array (
							"message" => $msg_data ['mandatory_data'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	private function getTaxiForTrip($trip_id, $is_fifo_zone_id) {
		sleep ( 1 );
		$taxi_request_id = FALSE;
		$driver_taxi_details = NULL;
		$distance = 0;
		$estimated_pickup_distance = 0;
		$estimated_pickup_minute = 0;
		
		$trip_details = $this->Trip_Details_Model->getById ( $trip_id );
		if (TimeIsBetweenTwoTimes ( DAY_START_TIME, DAY_END_TIME, $trip_details->pickupDatetime )) {
			$distance = RIDE_NOW_LIMIT_DAY_ONE;
		} else if (TimeIsBetweenTwoTimes ( NIGHT_START_TIME, NIGHT_END_TIME, $trip_details->pickupDatetime )) {
			$distance = RIDE_NOW_LIMIT_NIGHT_ONE;
		}
		if ($is_fifo_zone_id) {
			$driver_taxi_details = $this->Driver_Api_Webservice_Model->getDriverListForFifoTrip ( $trip_id, $distance );
		} else {
			$driver_taxi_details = $this->Driver_Api_Webservice_Model->getDriverListForTrip ( $trip_id, $distance );
		}
		if (! $driver_taxi_details) {
			$distance = 0;
			if (TimeIsBetweenTwoTimes ( DAY_START_TIME, DAY_END_TIME, $trip_details->pickupDatetime )) {
				$distance = RIDE_NOW_LIMIT_DAY_TWO;
			} else if (TimeIsBetweenTwoTimes ( NIGHT_START_TIME, NIGHT_END_TIME, $trip_details->pickupDatetime )) {
				$distance = RIDE_NOW_LIMIT_NIGHT_TWO;
			}
			if ($is_fifo_zone_id) {
				$driver_taxi_details = $this->Driver_Api_Webservice_Model->getDriverListForFifoTrip ( $trip_id, $distance );
			} else {
				$driver_taxi_details = $this->Driver_Api_Webservice_Model->getDriverListForTrip ( $trip_id, $distance );
			}
		}
		// check taxi request for particular trip already exists or not
		$taxi_request_exists = $this->Taxi_Request_Details_Model->getByKeyValueArray ( array (
				'tripId' => $trip_id 
		) );
		
		if ($driver_taxi_details) {
			if ($taxi_request_exists) {
				$update_taxi_request = array (
						'totalAvailableDriver' => count ( $driver_taxi_details ),
						'selectedDriverId' => $driver_taxi_details [0]->driverId,
						'taxiRequestStatus' => Taxi_Request_Status_Enum::AVAILABLE_TRIP 
				);
				$taxi_request_id = $this->Taxi_Request_Details_Model->update ( $update_taxi_request, array (
						'tripId' => $trip_id 
				) );
			} else {
				$insert_taxi_request = array (
						'tripId' => $trip_id,
						'totalAvailableDriver' => count ( $driver_taxi_details ),
						'selectedDriverId' => $driver_taxi_details [0]->driverId,
						'taxiRequestStatus' => Taxi_Request_Status_Enum::AVAILABLE_TRIP 
				);
				$taxi_request_id = $this->Taxi_Request_Details_Model->insert ( $insert_taxi_request );
			}
			// @todo update driiver rejected count $ consecutive reject count of driver to driver dispath details table
			$driver_dispatch_exists = NULL;
			$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
					'driverId' => $trip_details->driverId 
			) );
			if ($driver_dispatch_exists) {
				
				$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
						'isEligible' => Status_Type_Enum::INACTIVE 
				), array (
						'driverId' => $trip_details->driverId 
				) );
			} else {
				$insert_dispatch_data = array (
						'driverId' => $trip_details->driverId,
						'isEligible' => Status_Type_Enum::INACTIVE 
				);
				$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
			}
			// end driiver rejected count $ consecutive reject count of driver to driver dispath details table
			
			// To get estimated distance & time to reach driver for pickup location from google API
			if ($driver_taxi_details) {
				if ((! $estimated_pickup_distance || $estimated_pickup_minute || $estimated_pickup_distance == '' || $estimated_pickup_minute == '') || ($driver_taxi_details [0]->driverLatitude && $driver_taxi_details [0]->driverLongitude && $driver_taxi_details [0]->pickupLatitude && $driver_taxi_details [0]->pickupLongitude)) {
					$URL = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=" . $driver_taxi_details [0]->driverLatitude . "," . $driver_taxi_details [0]->driverLongitude . "&destinations=" . $driver_taxi_details [0]->pickupLatitude . "," . $driver_taxi_details [0]->pickupLongitude . "&key=" . GOOGLE_MAP_API_KEY;
					$curl = curl_init ();
					curl_setopt ( $curl, CURLOPT_URL, $URL );
					curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, true );
					curl_setopt ( $curl, CURLOPT_HEADER, false );
					$result = curl_exec ( $curl );
					curl_close ( $curl );
					// return $result; ;
					
					$response = json_decode ( $result );
					if ($response)
					{
					if ($response->status) {
						if ($response->rows [0]->elements [0]->distance->text) {
							$distance = explode ( ' ', $response->rows [0]->elements [0]->distance->text );
							if ($distance [1] == 'm') {
								$estimated_pickup_distance = round ( ($distance [0] / 1000), 1 ) + 0.5;
							} else if ($distance [1] == 'km') {
								$estimated_pickup_distance = round ( ($distance [0]), 1 ) + 0.5;
							}
						}
						if ($response->rows [0]->elements [0]->duration->text) {
							$time = explode ( ' ', $response->rows [0]->elements [0]->duration->text );
							
							if ($time [1] == 'hour' || $time [1] == 'hours') {
								$estimated_pickup_minute += ($time [0] * 60) + $time [2];
							} else if ($time [1] == 'mins') {
								$estimated_pickup_minute = $time [0];
							}
						}
					}
					}
				}
			}
			// To get estimated distance & time to reach driver for pickup location from google API
			$update_trip_details = $this->Trip_Details_Model->update ( array (
					'driverId' => $driver_taxi_details [0]->driverId,
					'taxiId' => $driver_taxi_details [0]->taxiId,
					'tripStatus' => Trip_Status_Enum::DRIVER_DISPATCHED,
					'dispatchedDatetime' => getCurrentDateTime (),
					'estimatedPickupDistance' => $estimated_pickup_distance,
					'estimatedPickupMinute' => $estimated_pickup_minute,
					'notificationStatus' => Trip_Status_Enum::DRIVER_DISPATCHED,
					'driverAcceptedStatus' => Driver_Accepted_Status_Enum::NONE 
			), array (
					'id' => $trip_id 
			) );
		} else {
			$update_trip_details = $this->Trip_Details_Model->update ( array (
					'tripStatus' => Trip_Status_Enum::DRIVER_NOT_FOUND,
					'notificationStatus' => Trip_Status_Enum::DRIVER_NOT_FOUND 
			), array (
					'id' => $trip_id 
			) );
		}
		return $driver_taxi_details;
	}
	
	/**
	 * used to get the Hash key from server
	 */
	public function getPayUHashes_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$response_data = array ();
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$key = '';
		$salt = '';
		$txnid = '';
		$amount = '';
		$productinfo = '';
		$firstname = '';
		$email = '';
		$udf1 = '';
		$udf2 = '';
		$udf3 = '';
		$udf4 = '';
		$udf5 = '';
		$user_credentials = '';
		$offerKey = '';
		$cardBin = '';
		
		// Posted json data
		$post_data = $this->get_post_data ();
		
		// $firstname, $email can be "", i.e empty string if needed. Same should be sent to PayU server (in request params) also.
		$key = ($post_data->key != NULL && $post_data->key != '') ? $post_data->key : "";
		$salt = ($post_data->salt != NULL && $post_data->salt != '') ? $post_data->salt : "";
		$txnid = $post_data->txnid;
		$amount = $post_data->amount;
		$productinfo = $post_data->productinfo;
		$firstname = $post_data->firstname;
		$email = $post_data->email;
		$udf1 = $post_data->udf1;
		$udf2 = $post_data->udf2;
		$udf3 = $post_data->udf3;
		$udf4 = $post_data->udf4;
		$udf5 = $post_data->udf5;
		$user_credentials = $post_data->user_credentials;
		$offerKey = $post_data->offerKey;
		$cardBin = $post_data->cardBin;
		
		// $passenger_id = $post_data->passenger_id;
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual functionality
				$response_data = array ();
				
				$payhash_str = $key . '|' . checkNull ( $txnid ) . '|' . checkNull ( $amount ) . '|' . checkNull ( $productinfo ) . '|' . checkNull ( $firstname ) . '|' . checkNull ( $email ) . '|' . checkNull ( $udf1 ) . '|' . checkNull ( $udf2 ) . '|' . checkNull ( $udf3 ) . '|' . checkNull ( $udf4 ) . '|' . checkNull ( $udf5 ) . '||||||' . $salt;
				$paymentHash = strtolower ( hash ( 'sha512', $payhash_str ) );
				$response_data ['payment_hash'] = $paymentHash;
				
				$cmnNameMerchantCodes = 'get_merchant_ibibo_codes';
				$merchantCodesHash_str = $key . '|' . $cmnNameMerchantCodes . '|default|' . $salt;
				$merchantCodesHash = strtolower ( hash ( 'sha512', $merchantCodesHash_str ) );
				$response_data ['get_merchant_ibibo_codes_hash'] = $merchantCodesHash;
				
				$cmnMobileSdk = 'vas_for_mobile_sdk';
				$mobileSdk_str = $key . '|' . $cmnMobileSdk . '|default|' . $salt;
				$mobileSdk = strtolower ( hash ( 'sha512', $mobileSdk_str ) );
				$response_data ['vas_for_mobile_sdk_hash'] = $mobileSdk;
				
				$cmnPaymentRelatedDetailsForMobileSdk1 = 'payment_related_details_for_mobile_sdk';
				$detailsForMobileSdk_str1 = $key . '|' . $cmnPaymentRelatedDetailsForMobileSdk1 . '|default|' . $salt;
				$detailsForMobileSdk1 = strtolower ( hash ( 'sha512', $detailsForMobileSdk_str1 ) );
				$response_data ['payment_related_details_for_mobile_sdk_hash'] = $detailsForMobileSdk1;
				
				// used for verifying payment(optional)
				$cmnVerifyPayment = 'verify_payment';
				$verifyPayment_str = $key . '|' . $cmnVerifyPayment . '|' . $txnid . '|' . $salt;
				$verifyPayment = strtolower ( hash ( 'sha512', $verifyPayment_str ) );
				$response_data ['verify_payment_hash'] = $verifyPayment;
				
				if ($user_credentials != NULL && $user_credentials != '') {
					$cmnNameDeleteCard = 'delete_user_card';
					$deleteHash_str = $key . '|' . $cmnNameDeleteCard . '|' . $user_credentials . '|' . $salt;
					$deleteHash = strtolower ( hash ( 'sha512', $deleteHash_str ) );
					$response_data ['delete_user_card_hash'] = $deleteHash;
					
					$cmnNameGetUserCard = 'get_user_cards';
					$getUserCardHash_str = $key . '|' . $cmnNameGetUserCard . '|' . $user_credentials . '|' . $salt;
					$getUserCardHash = strtolower ( hash ( 'sha512', $getUserCardHash_str ) );
					$response_data ['get_user_cards_hash'] = $getUserCardHash;
					
					$cmnNameEditUserCard = 'edit_user_card';
					$editUserCardHash_str = $key . '|' . $cmnNameEditUserCard . '|' . $user_credentials . '|' . $salt;
					$editUserCardHash = strtolower ( hash ( 'sha512', $editUserCardHash_str ) );
					$response_data ['edit_user_card_hash'] = $editUserCardHash;
					
					$cmnNameSaveUserCard = 'save_user_card';
					$saveUserCardHash_str = $key . '|' . $cmnNameSaveUserCard . '|' . $user_credentials . '|' . $salt;
					$saveUserCardHash = strtolower ( hash ( 'sha512', $saveUserCardHash_str ) );
					$response_data ['save_user_card_hash'] = $saveUserCardHash;
					
					$cmnPaymentRelatedDetailsForMobileSdk = 'payment_related_details_for_mobile_sdk';
					$detailsForMobileSdk_str = $key . '|' . $cmnPaymentRelatedDetailsForMobileSdk . '|' . $user_credentials . '|' . $salt;
					$detailsForMobileSdk = strtolower ( hash ( 'sha512', $detailsForMobileSdk_str ) );
					$response_data ['payment_related_details_for_mobile_sdk_hash'] = $detailsForMobileSdk;
				}
				
				if ($offerKey != NULL && ! empty ( $offerKey )) {
					$cmnCheckOfferStatus = 'check_offer_status';
					$checkOfferStatus_str = $key . '|' . $cmnCheckOfferStatus . '|' . $offerKey . '|' . $salt;
					$checkOfferStatus = strtolower ( hash ( 'sha512', $checkOfferStatus_str ) );
					$response_data ['check_offer_status_hash'] = $checkOfferStatus;
				}
				
				if ($cardBin != NULL && ! empty ( $cardBin )) {
					$cmnCheckIsDomestic = 'check_isDomestic';
					$checkIsDomestic_str = $key . '|' . $cmnCheckIsDomestic . '|' . $cardBin . '|' . $salt;
					$checkIsDomestic = strtolower ( hash ( 'sha512', $checkIsDomestic_str ) );
					$response_data ['check_isDomestic_hash'] = $checkIsDomestic;
				}
				$message = (array (
						'result' => $response_data,
						"status" => 1 
				));
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	
	/**
	 * update the passenger wallet amount
	 */
	public function walletUpdate_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$wallet_amount = NULL;
		$passenger_id = NULL;
		$transaction_status = NULL;
		$transaction_id = NULL;
		$wallet_balance_amount = NULL;
		$passenger_exists = NULL;
		// Posted json data
		$post_data = $this->get_post_data ();
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$passenger_id = $post_data->passenger_id;
		$wallet_amount = $post_data->amount;
		$transaction_amount = $wallet_amount;
		// @todo update the transaction history of the passenger wallet debit with status & transaction id
		$transaction_status = $post_data->transaction_status;
		$transaction_id = $post_data->transaction_id;
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				$passenger_exists = $this->Passenger_Model->getById ( $passenger_id );
				// actual functionality
				if ($passenger_id && $wallet_amount && is_numeric ( $wallet_amount )) {
					if ($wallet_amount > 0) {
						$current_wallet_balance = $this->Passenger_Model->getById ( $passenger_id );
						$wallet_amount += $current_wallet_balance->walletAmount;
						$update_wallet = $this->Passenger_Model->update ( array (
								'walletAmount' => $wallet_amount 
						), array (
								'mobile' => $passenger_exists->mobile 
						) );
						
						if ($update_wallet) {
							$insert_data = array (
									'passengerId' => $passenger_id,
									'transactionAmount' => $transaction_amount,
									'transactionId' => $transaction_id,
									'transactionStatus' => $transaction_status,
									'previousAmount' => $current_wallet_balance->walletAmount,
									'currentAmount' => $wallet_amount,
									'transactionMode' => Transaction_Mode_Enum::DEBIT,
									'transactionType' => Transaction_Type_Enum::TWOC_TWOP,
									'transactionFrom' => Transaction_From_Enum::WALLET_ACCOUNT 
							);
							$passenger_wallet_details_update = $this->Passenger_Transaction_Details_Model->insert ( $insert_data );
							
							$wallet_balance_amount = $this->Passenger_Model->getColumnByKeyValueArray ( 'walletAmount', array (
									'id' => $passenger_id 
							) );
							$message = array (
									"message" => $msg_data ['wallet_success'],
									"amount" => $wallet_balance_amount [0]->walletAmount,
									"status" => 1 
							);
						} else {
							$message = array (
									"message" => $msg_data ['wallet_failed'],
									"status" => - 1 
							);
						}
					} else {
						$message = array (
								"message" => $msg_data ['wallet_empty'],
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['mandatory_data'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	public function dataRefresh_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$status = - 1;
		$passenger_wallet_amount = NULL;
		$passenger_id = NULL;
		
		// Posted json data
		$post_data = $this->get_post_data ();
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$passenger_id = $post_data->passenger_id;
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				if ($passenger_id) {
					// actual functionality
					$passenger_wallet_amount = $this->Passenger_Model->getColumnByKeyValueArray ( 'walletAmount', array (
							'id' => $passenger_id 
					) );
					if ($passenger_wallet_amount) {
						$message = $msg_data ['success'];
						$status = 1;
					} else {
						$message = $msg_data ['failed'];
					}
				} else {
					$message = $msg_data ['invalid_user'];
				}
				$message = array (
						'wallet_amount' => $passenger_wallet_amount [0]->walletAmount,
						'message' => $message,
						"status" => $status 
				);
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	public function tripDispatchDriverNotification($trip_id, $driver_id, $distance = 0) {
		// $gcm_id,$trip_id //Posted json data
		$gcm_id = '';
		// $trip_id = $post_data->trip_id;
		// $driver_id = 2;
		$response = NULL;
		$msg_data = $this->config->item ( 'api' );
		$msg = '';
		$status = 0;
		$firebase_api_key = DRIVER_FIREBASE_API_KEY;
		
		$post_data = array ();
		$response_data = array ();
		
		$driver_details = $this->Driver_Model->getById ( $driver_id );
		
		if ($driver_details) {
			if ($driver_details->gcmId) {
				$gcm_id = $driver_details->gcmId;
				$post_data ['registration_ids'] = ( array ) $gcm_id;
				if ($gcm_id) {
					// $trip_details = $this->Trip_Details_Model->getById($trip_id);
					$trip_details = $this->Common_Api_Webservice_Model->getTripDetails ( $trip_id );
					// $passenger_details = $this->Passenger_Model->getById ( $trip_details->passengerId );
					
					$response_data = array (
							"message" => 'driver_dispatch',
							"status" => 1,
							'trip_details' => array (
									'trip_id' => $trip_details [0]->tripId,
									'job_card_id' => $trip_details [0]->jobCardId,
									'pickup_location' => $trip_details [0]->pickupLocation,
									'pickup_latitude' => $trip_details [0]->pickupLatitude,
									'pickup_longitude' => $trip_details [0]->pickupLongitude,
									'pickup_time' => $trip_details [0]->pickupDatetime,
									'drop_location' => $trip_details [0]->dropLocation,
									'drop_latitude' => $trip_details [0]->dropLatitude,
									'drop_longitude' => $trip_details [0]->dropLongitude,
									'drop_time' => $trip_details [0]->dropDatetime,
									'land_mark' => $trip_details [0]->landmark,
									'taxi_rating' => $trip_details [0]->passengerRating,
									'taxi_comments' => $trip_details [0]->passengerComments,
									'trip_type' => $trip_details [0]->tripType,
									'trip_type_name' => $trip_details [0]->tripTypeName,
									'payment_mode' => $trip_details [0]->paymentMode,
									'payment_mode_name' => $trip_details [0]->paymentModeName,
									'trip_status' => $trip_details [0]->tripStatus,
									'trip_status_name' => $trip_details [0]->tripStatusName,
									'promocode' => $trip_details [0]->promoCode,
									'is_driver_rated' => ($trip_details [0]->driverRating) ? TRUE : FALSE 
							),
							'passenger_details' => array (
									'passenger_id' => $trip_details [0]->passengerId,
									'passenger_firstname' => $trip_details [0]->passengerFirstName,
									'passenger_lastname' => $trip_details [0]->passengerLastName,
									'passenger_mobile' => $trip_details [0]->passengerMobile,
									'passenger_latitude' => $trip_details [0]->passengerCurrentLatitude,
									'passenger_longitude' => $trip_details [0]->passengerCurrentLongitude,
									'passenger_profile_image' => ($trip_details [0]->passengerProfileImage) ? (passenger_content_url ( $trip_details [0]->passengerId . '/' . $trip_details [0]->passengerProfileImage )) : image_url ( 'app/user.png' ) 
							),
							'driver_details' => array (
									'driver_id' => $trip_details [0]->driverId,
									'driver_firstname' => $trip_details [0]->driverFirstName,
									'driver_lastname' => $trip_details [0]->driverLastName,
									'driver_mobile' => $trip_details [0]->driverMobile,
									'driver_profile_image' => ($trip_details [0]->driverProfileImage) ? (driver_content_url ( $trip_details [0]->driverId . '/' . $trip_details [0]->driverProfileImage )) : image_url ( 'app/user.png' ),
									'driver_language' => $trip_details [0]->driverLanguagesKnown,
									'driver_experience' => $trip_details [0]->driverExperience,
									'driver_age' => $trip_details [0]->driverDob,
									'driver_latitute' => $trip_details [0]->currentLatitude,
									'driver_longtitute' => $trip_details [0]->currentLongitude,
									'driver_status' => $trip_details [0]->availabilityStatus 
							),
							'taxi_details' => array (
									'taxi_id' => $trip_details [0]->taxiId,
									'taxi_name' => $trip_details [0]->taxiName,
									'taxi_model' => $trip_details [0]->taxiModel,
									'taxi_manufacturer' => $trip_details [0]->taxiManufacturer,
									'taxi_registration_no' => $trip_details [0]->taxiRegistrationNo,
									'taxi_colour' => $trip_details [0]->taxiColour 
							),
							
							"estimated_time" => $trip_details [0]->estimatedTime,
							"distance_away" => number_format ( $trip_details [0]->estimatedDistance, 1 ),
							"calculate_distance_away" => number_format ( $trip_details [0]->estimatedDistance + 0.5, 1 ),
							"notification_time" => 20 
					);
					$post_data ['data'] = array (
							
							'details' => $response_data 
					);
				} else {
					$msg = $msg_data ['failed'];
					
					$post_data ['data'] = array (
							'message' => $msg,
							'status' => - 1 
					);
				}
				
				$post_data = json_encode ( $post_data );
				$url = "https://fcm.googleapis.com/fcm/send";
				
				$ch = curl_init ();
				curl_setopt ( $ch, CURLOPT_URL, $url );
				curl_setopt ( $ch, CURLOPT_HTTPHEADER, array (
						'Content-Type:application/json',
						'Authorization: key=' . $firebase_api_key 
				) );
				curl_setopt ( $ch, CURLOPT_POST, 1 );
				curl_setopt ( $ch, CURLOPT_POSTFIELDS, $post_data );
				curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
				$response = curl_exec ( $ch );
				curl_close ( $ch );
			}
		}
		return $response;
		// echo json_encode ( array('response'=>$response,'msg'=>$msg,'status'=>$status));
	}
	
	/**
	 * update the passenger wallet amount
	 */
	public function passengerWalletUpdate_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$wallet_amount = NULL;
		$passenger_id = NULL;
		$transaction_status = NULL;
		$transaction_id = NULL;
		$wallet_balance_amount = NULL;
		$passenger_exists = NULL;
		// Posted json data
		$post_data = $this->get_post_data ();
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$passenger_id = $post_data->passenger_id;
		$wallet_amount = $post_data->transaction_amt;
		$transaction_amount = $wallet_amount;
		// @todo update the transaction history of the passenger wallet debit with status & transaction id
		$transaction_status = $post_data->transaction_status;
		$transaction_id = $post_data->transaction_id;
		
		// Checked wheather authentication key availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual functionality
				$passenger_exists = $this->Passenger_Model->getById ( $passenger_id );
				if ($passenger_id && $wallet_amount && is_numeric ( $wallet_amount )) {
					if ($wallet_amount > 0) {
						$current_wallet_balance = $this->Passenger_Model->getById ( $passenger_id );
						$wallet_amount += $current_wallet_balance->walletAmount;
						$update_wallet = $this->Passenger_Model->update ( array (
								'walletAmount' => $wallet_amount 
						), array (
								'mobile' => $passenger_exists->mobile 
						) );
						
						if ($update_wallet) {
							$insert_data = array (
									'passengerId' => $passenger_id,
									'transactionAmount' => $transaction_amount,
									'transactionId' => $transaction_id,
									'transactionStatus' => $transaction_status,
									'previousAmount' => $current_wallet_balance->walletAmount,
									'currentAmount' => $wallet_amount,
									'transactionMode' => Transaction_Mode_Enum::CREDIT,
									'transactionType' => Transaction_Type_Enum::TWOC_TWOP,
									'transactionFrom' => Transaction_From_Enum::WALLET_ACCOUNT 
							);
							$passenger_wallet_details_update = $this->Passenger_Transaction_Details_Model->insert ( $insert_data );
							
							$wallet_balance_amount = $this->Passenger_Model->getColumnByKeyValueArray ( 'walletAmount', array (
									'id' => $passenger_id 
							) );
							$message = array (
									"message" => $msg_data ['wallet_success'],
									"amount" => $wallet_balance_amount [0]->walletAmount,
									"status" => 1 
							);
						} else {
							$message = array (
									"message" => $msg_data ['wallet_failed'],
									"status" => - 1 
							);
						}
					} else {
						$message = array (
								"message" => $msg_data ['wallet_empty'],
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['mandatory_data'],
							"status" => - 1,
							"detail" => $msg_data ['mandatory_data'] 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	public function getNotificationList_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = array ();
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$notification_list = NULL;
		
		$response_data = array ();
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual functonality
				
				$notification_list = $this->Notification_Model->getByKeyValueArray ( array (
						'startDatetime<=' => getCurrentDateTime (),
						'endDatetime>=' => getCurrentDateTime (),
						'isDeleted' => Status_Type_Enum::INACTIVE,
						'notificationTo' => User_Type_Enum::PASSENGER,
						'status' => Status_Type_Enum::ACTIVE 
				) );
				if ($notification_list) {
					foreach ( $notification_list as $list ) {
						$response_data [] = array (
								'id' => $list->id,
								'title' => $list->title,
								'description' => $list->description,
								'start_date_time' => $list->startDatetime,
								'end_date_time' => $list->endDatetime,
								'noticed_date_time' => $list->createdDatetime 
						);
					}
					$message = array (
							"message" => $msg_data ['success'],
							"details" => $response_data,
							"status" => 1 
					);
				} else {
					$message = array (
							"message" => $msg_data ['data_not_found'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	private function validatePromoCode($promo_code, $passenger_id, $zone_id, $drop_zone_id, $entity_id) {
		$msg_data = $this->config->item ( 'api' );
		$message = array ();
		$promocode_details = NULL;
		$promocode_type = NULL;
		$promocode_details = $this->Promocode_Details_Model->getOneByKeyValueArray ( array (
				'promoCode' => $promo_code 
		) );
		if ($promocode_details) {
			$promocode_type = $promocode_details->promocodeType;
			
			// Check promocode exists or not for particular passenger at particular city
			$promocode_exists = $this->Common_Api_Webservice_Model->checkPromocodeAvailablity ( $promo_code, $promocode_type, $passenger_id, $zone_id, $drop_zone_id, $entity_id );
			/*
			 * if (! $promocode_exists) {
			 * $promocode_exists = $this->Common_Api_Webservice_Model->checkPromocodeAvailablity ( $promo_code, $passenger_id, $drop_zone_id, $entity_id );
			 * }
			 */
			// debug($promocode_exists [0]->promoStartDatetime);echo "====";debug(strtotime ($promocode_exists [0]->promoStartDatetime));echo "====";debug_exit(getCurrentUnixDateTime ());
			if ($promocode_exists && count ( $promocode_exists ) > 0) {
				/*
				 * $promocode_exists = $this->Promocode_Details_Model->getByKeyValueArray ( array (
				 * 'passengerId' => 0,
				 * 'promoCode' => $promocode,
				 * 'cityId' => $city_id
				 * ) );
				 */
				
				// check promocode limit exceeds or not for specific passenger (note: only completed trip)
				$promocode_user_limit_exists = $this->Trip_Details_Model->getByKeyValueArray ( array (
						'passengerId' => $passenger_id,
						'promoCode' => $promo_code,
						'tripStatus' => Trip_Status_Enum::TRIP_COMPLETED 
				) );
				// check promocode limit exceeds or not total limit (note: only completed trip)
				$promocode_limit_exists = $this->Trip_Details_Model->getUsedPromocodeLimitCount ( $promo_code );
				if (count ( $promocode_limit_exists ) >= $promocode_exists [0]->promoCodeLimit) {
					$message = array (
							"message" => $msg_data ['promocode_total_limit'],
							"status" => - 1 
					);
				} else if (count ( $promocode_user_limit_exists ) >= $promocode_exists [0]->promoCodeUserLimit) {
					$message = array (
							"message" => $msg_data ['promocode_user_limit'],
							"status" => - 1 
					);
				} else if (strtotime ( $promocode_exists [0]->promoStartDatetime ) > getCurrentUnixDateTime ()) {
					$message = array (
							"message" => $msg_data ['promocode_start'],
							"status" => - 1 
					);
				} else if (strtotime ( $promocode_exists [0]->promoEndDatetime ) < getCurrentUnixDateTime ()) {
					$message = array (
							"message" => $msg_data ['promocode_expiry'],
							"status" => - 1 
					);
				} else {
					$message = array (
							"message" => $msg_data ['promocode_success'],
							"status" => 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['invalid_promocode'],
						"status" => - 1 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['invalid_promocode'],
					"status" => - 1 
			);
		}
		return $message;
	}
}