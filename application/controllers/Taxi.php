<?php
class Taxi extends My_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->helper ( 'form' );
		$this->load->model ( 'Taxi_Query_Model' );
		$this->load->model ( 'Taxi_Details_Model' );
		$this->load->model ( 'Taxi_Owner_Details_Model' );
		$this->load->model ( 'Entity_Model' );
		$this->load->model ( 'Zone_Details_Model' );
		$this->load->model ( 'Data_Attributes_Model' );
	}
	public function index() {
	}
	public function add() {
		if ($this->User_Access_Model->createModule ($this->session->userdata('role_type'),Module_Name_Enum::TAXI) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$this->addJs ( 'app/taxi.js' );
		
		$data = array (
				'mode' => EDIT_MODE 
		);
		$data ['activetab'] = "taxi";
		$data ['active_menu'] = 'taxi';
		$data ['zone_list'] = $this->Zone_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE 
		), 'name' );
		$data ['entity_list'] = $this->Entity_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE
		), 'name' );
		$data ['transmission_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRANSMISSION_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		$data ['taxi_category_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TAXI_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		$data ['taxi_model'] = $this->Taxi_Details_Model->getBlankModel ();
		$data['taxi_owner_model']=$this->Taxi_Owner_Details_Model->getBlankModel();
		$this->render ( 'taxi/add_edit_taxi', $data );
	}
		
	public function getDetailsById($taxi_id, $view_mode) {
	if ($view_mode==EDIT_MODE)
	{
	if ($this->User_Access_Model->writeModule ($this->session->userdata('role_type'),Module_Name_Enum::TAXI) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
	}
	else
	{
		if ($this->User_Access_Model->readModule ($this->session->userdata('role_type'),Module_Name_Enum::TAXI) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
	} 
		$this->addJs ( 'app/taxi.js' );
		// get API messages from message.php
		$msg_data = $this->config->item ( 'msg' );
		$message='';
		$data = array (
				'mode' => $view_mode 
		);
		$data ['activetab'] = "taxi";
		$data ['active_menu'] = 'taxi';
		$data ['taxi_owner_list'] = $this->Taxi_Owner_Details_Model->getSelectDropdownOptions ( array ('status'=>Status_Type_Enum::ACTIVE,'isDeleted'=>Status_Type_Enum::INACTIVE), 'firstName' );
		$data ['zone_list'] = $this->Zone_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE 
		), 'name' );
		$data ['entity_list'] = $this->Entity_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE
		), 'name' );
		$data ['transmission_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRANSMISSION_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		$data ['taxi_category_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TAXI_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		if ($this->input->post ( 'id' )) {
			$taxi_id = $this->input->post ( 'id' );
		}
		
		if ($taxi_id > 0) {
			if ($view_mode==EDIT_MODE)
			{
				$locked=$this->Taxi_Details_Model->isLocked($taxi_id);
				if ($locked)
				{
					$locked_user=$this->Taxi_Details_Model->getLockedUser($taxi_id);
					$locked_date=$this->Taxi_Details_Model->getLockedDate($taxi_id);
					$locked_date=date('jS, F Y G:ia',strtotime($locked_date));
					$message=$msg_data ['locked'];
					$message=str_replace('##USERNAME##', $locked_user, $message);
					$message.=$locked_date;
			
					$data['mode']=VIEW_MODE;
				}
				else
				{
					$is_locked=$this->Taxi_Details_Model->lock($taxi_id);
				}
			}
			$data ['taxi_model'] = $this->Taxi_Details_Model->getById ( $taxi_id );
			$data['taxi_owner_model']=$this->Taxi_Owner_Details_Model->getById($data ['taxi_model']->taxiOwnerId);
			if (!$data['taxi_owner_model'])
			{
				$data['taxi_owner_model']=$this->Taxi_Owner_Details_Model->getBlankModel();
			}
		} else {
			$data ['taxi_model'] = $this->Taxi_Details_Model->getBlankModel ();
			$data['taxi_owner_model']=$this->Taxi_Owner_Details_Model->getBlankModel();
		}
		if ($this->input->post ( 'id' )) {
			
			$html = $this->load->view ( 'taxi/add_edit_taxi', $data );
			echo json_encode ( array (
					'html' => $html 
			) );
		} else {
			$this->render ( 'taxi/add_edit_taxi', $data );
		}
	}
	
	public function getTaxilist()
	{
		if ($this->User_Access_Model->showTaxi () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		
		$this->addJs ( 'app/taxi.js' );
		/* $this->addJs ( 'vendors/datatables/js/jquery.dataTables.min.js' );
		$this->addJs ( 'vendors/datatables/js/dataTables.bootstrap.min.js' );
		
		$this->addCss( 'app/pages/tables.css');
		$this->addCss( 'app/pages/dataTables.bootstrap.css');
		 */
		$data ['activetab'] = "taxi";
		$data ['active_menu'] = 'taxi';
		
		$this->render ( 'taxi/manage_taxi', $data );
	}
	public function ajax_list()
	{
		//echo"ajaxlist";exit;
		$sql_query=$this->Taxi_Details_Model->getTaxiListQuery();
		$taxi_list = $this->Taxi_Details_Model->getDataTable($sql_query);
		//print_r($list);exit;
		$data = array();
		$no = $_POST['start'];
		foreach ($taxi_list as $list) {
			$no++;
			$row = array();
			if ($list->isAllocated)
			{
			$row[] = '
			<button class="btn btn-sm btn-success" id="viewtaxi-'.$list->taxiId.'" title="View" ><i class="fa fa-eye" aria-hidden="true"></i></button>
				  <button class="btn btn-sm btn-blue" id="edittaxi-'.$list->taxiId.'" title="Edit" )"><i class="fa fa-pencil-square-o"></i></button>';
                  
			}
			else 
			{
				$row[] = '
			<button class="btn btn-sm btn-success" id="viewtaxi-'.$list->taxiId.'" title="View" ><i class="fa fa-eye" aria-hidden="true"></i></button>
				  <button class="btn btn-sm btn-blue" id="edittaxi-'.$list->taxiId.'" title="Edit" )"><i class="fa fa-pencil-square-o"></i></button>
                  <button class="btn btn-sm btn-danger" id="deletetaxi-'.$list->taxiId.'"  title="Delete"><i class="fa fa-trash-o"></i></button>';
			}
			$row[] = $list->registrationNo;
			$row[] = ucfirst($list->name);
			$row[] = ucfirst($list->model);
			$row[] = ucfirst($list->manufacturer);
			$row[] = ucfirst($list->colour);
			$row[] = $list->insuranceNo;
			$row[] = $list->insuranceExpiryDate;
			$row[] = $list->nextFcdate;
			$row[] = $list->taxiCategoryType;
			$row[] = $list->minSpeed;
			$row[] = $list->maxSpeed;
			$row[] = $list->transmissionType;
			$row[] = $list->serviceStartDate;
			$row[] = ucfirst($list->zoneName);
			$allocated_class=($list->isAllocated)?'class="btn btn-success"':'class="btn btn-danger"';
			$allocated_name=($list->isAllocated)?'Yes':'No';
			$row[] = '<button '.$allocated_class.'><span>'.$allocated_name.'</span></button>';
			$status_class=($list->status)?'class="btn btn-success"':'class="btn btn-danger"';
			$status_name=($list->status)?'Active':'Inactive';
			$status_id=(!$list->isAllocated)?'id="status-'.$list->taxiId.'"':'';
			$row[] = '<button '.$status_id.' status="'.$list->status.'" '.$status_class.'><span>'.$status_name.'</span></button>';
			
			//add html for action
			
			$data[] = $row;
		}
	
		$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->Taxi_Details_Model->getDataTableAllCount($sql_query),
				"recordsFiltered" => $this->Taxi_Details_Model->getDataTableFilterCount($sql_query),
				"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	
	public function saveTaxi() {
		$this->addJs ( 'app/taxi.js' );
		$data = array ();
		$data = $this->input->post ();
		//print_r($data);exit;
		$taxi_id = $data ['id'];
		$data['activetab']="admin";
		$data['active_menu'] = 'admin';
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['failed'];
		if ($taxi_id > 0) {
			$this->Taxi_Details_Model->update ( $data, array (
					'id' => $taxi_id
			) );
			
			$file_names = array_keys ( $_FILES );
				
			$registration_img_selected = $_FILES ['registrationImage'] ['name'];
			$car_img_selected = $_FILES ['carImage'] ['name'];
			$car_driver_img_selected = $_FILES ['carDriverImage'] ['name'];
			$wheel_tax_img_selected = $_FILES ['wheelTaxImage'] ['name'];
			
			
			// Registration Image Upload
			if ($registration_img_selected [0] != '') {
				$prefix = "REG-" . $taxi_id;
				$upload_path =$this->config->item ( 'taxi_content_path' );
				$file_upload_path = $upload_path . $taxi_id;
				file_upload ( $file_names [0], $taxi_id, $registration_img_selected, $prefix, $file_upload_path,'Taxi_Details_Model','id' );
			}
			//Car Image Upload
			if ($car_img_selected [0] != '') {
				$prefix = "CAR-" . $taxi_id;
				$upload_path =$this->config->item ( 'taxi_content_path' );
				$file_upload_path = $upload_path . $taxi_id;
				file_upload ( $file_names [1], $taxi_id, $car_img_selected, $prefix, $file_upload_path,'Taxi_Details_Model','id' );
			}
			// Car & Driver Image Upload
			if ($car_driver_img_selected [0] != '') {
				$prefix = "CARDRIVER-" . $taxi_id;
				$upload_path =$this->config->item ( 'taxi_content_path' );
				$file_upload_path = $upload_path . $taxi_id;
				file_upload ( $file_names [2], $taxi_id, $car_driver_img_selected, $prefix, $file_upload_path,'Taxi_Details_Model','id' );
			}
			//Wheel Tax image upload
			if ($wheel_tax_img_selected [0] != '') {
				// in which name img should be uploaded
				$prefix = "WHEELTAX-" . $taxi_id;
				$upload_path =$this->config->item ( 'taxi_content_path' );
				$file_upload_path = $upload_path . $taxi_id;
				file_upload ( $file_names [3], $taxi_id, $wheel_tax_img_selected, $prefix, $file_upload_path,'Taxi_Details_Model','id' );
			}
			
			$msg = $msg_data ['updated'];
	
		} else {
			
			$taxi_id = $this->Taxi_Details_Model->insert ( $data );
			
			$file_names = array_keys ( $_FILES );
			
			$registration_img_selected = $_FILES ['registrationImage'] ['name'];
			$car_img_selected = $_FILES ['carImage'] ['name'];
			$car_driver_img_selected = $_FILES ['carDriverImage'] ['name'];
			$wheel_tax_img_selected = $_FILES ['wheelTaxImage'] ['name'];
				
				
			// Registration Image Upload
			if ($registration_img_selected [0] != '') {
				$prefix = "REG-" . $taxi_id;
				$upload_path =$this->config->item ( 'taxi_content_path' );
				$file_upload_path = $upload_path . $taxi_id;
				file_upload ( $file_names [0], $taxi_id, $registration_img_selected, $prefix, $file_upload_path,'Taxi_Details_Model','id' );
			}
			//Car Image Upload
			if ($car_img_selected [0] != '') {
				$prefix = "CAR-" . $taxi_id;
				$upload_path =$this->config->item ( 'taxi_content_path' );
				$file_upload_path = $upload_path . $taxi_id;
				file_upload ( $file_names [1], $taxi_id, $car_img_selected, $prefix, $file_upload_path,'Taxi_Details_Model','id' );
			}
			// Car & Driver Image Upload
			if ($car_driver_img_selected [0] != '') {
				$prefix = "CARDRIVER-" . $taxi_id;
				$upload_path =$this->config->item ( 'taxi_content_path' );
				$file_upload_path = $upload_path . $taxi_id;
				file_upload ( $file_names [2], $taxi_id, $car_driver_img_selected, $prefix, $file_upload_path,'Taxi_Details_Model','id' );
			}
			//Wheel Tax image upload
			if ($wheel_tax_img_selected [0] != '') {
				// in which name img should be uploaded
				$prefix = "WHEELTAX-" . $taxi_id;
				$upload_path =$this->config->item ( 'taxi_content_path' );
				$file_upload_path = $upload_path . $taxi_id;
				file_upload ( $file_names [3], $taxi_id, $wheel_tax_img_selected, $prefix, $file_upload_path,'Taxi_Details_Model','id' );
			}
			$msg = $msg_data ['success'];
		}
		$response = array (
				'msg' => $msg,
				'taxi_id' => $taxi_id
		);
		echo json_encode ( $response );
		// $this->render("passenger/add_edit_passenger", $data);
	
	}
	
	public function deleteTaxi() {
		if ($this->User_Access_Model->deleteModule ($this->session->userdata('role_type'),Module_Name_Enum::TAXI) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to delete", '404' );
			return;
		}
		$data = array ();
	
		$this->addJs ( 'app/taxi.js' );
		$taxi_updated=-1;
		$data = $this->input->post ();
		$taxi_id = $data ['id'];
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['delete_failed'];
		if ($taxi_id) {
			$taxi_updated = $this->Taxi_Details_Model->update ( array (
					'isDeleted' => Status_Type_Enum::ACTIVE
			), array (
					'id' => $taxi_id
			) );
			$msg = $msg_data ['delete_success'];
		}
		$response = array (
				'msg' => $msg,
				'status' => $taxi_updated
		);
		echo json_encode ( $response );
	}
	
	public function checkTaxiRegistrationNo() {
		$data = array ();
		$data = $this->input->post ();
		$registration_no = $data ['registrationNo'];
		$taxi_id = $data ['taxi_id'];
		$registration_exists = NULL;
		$status = 0;
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['registration_not_exists'];
		if ($registration_no) {
			if ($taxi_id>0)
			{
			$registration_exists = $this->Taxi_Details_Model->getOneByKeyValueArray ( array (
					'registrationNo' => $registration_no,
					'id!='=>$taxi_id,
					'isDeleted'=>Status_Type_Enum::INACTIVE
			) );
			}
			else 
			{
				$registration_exists = $this->Taxi_Details_Model->getOneByKeyValueArray ( array (
						'registrationNo' => $registration_no,
						'isDeleted'=>Status_Type_Enum::INACTIVE
				) );
			}
			if ($registration_exists) {
				$status = 1;
				$msg = $msg_data ['registration_exists'];
			}
		}
		$response = array (
				'msg' => $msg,
				'registration_no' => $registration_no,
				'status' => $status
		);
		echo json_encode ( $response );
	}
	public function getTaxiOwnerDetails()
	{
		$data=array();
	
		$key = $this->input->post('key');
		$taxi_owner_lists= $this->Taxi_Owner_Details_Model->getTaxiOwnerDetails($key);
		if ($taxi_owner_lists) {
			//debug_exit($merchant_lists);
			echo "<ul id='passenger-autocompletelist' style=''>";
			foreach ($taxi_owner_lists as $list) {
				$taxi_owner_id = $list->taxiOwnerId;
				$taxi_owner_name = $list->taxiOwnerFullName;
				$taxi_owner_mobile = $list->taxiOwnerMobile;
				$taxi_owner_code = $list->taxiOwnerCode;
				$taxi_owner_data=(trim($taxi_owner_mobile)!='')?$taxi_owner_name."(".$taxi_owner_mobile.")": $taxi_owner_name;
				echo "<li onClick=\"selectTaxiOwner('$taxi_owner_id','$taxi_owner_mobile','$taxi_owner_name');\">$taxi_owner_data </li> ";
			}
			echo "</ul>";
		}
	
	}
	
	
}