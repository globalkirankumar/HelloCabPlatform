<?php
class Cronjob extends My_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->helper ( 'form' );
		$this->load->model ( 'Cronjob_Query_Model' );
		$this->load->model ( 'v1/Driver_Api_Webservice_Model' );
		$this->load->model ( 'Taxi_Request_Details_Model' );
		$this->load->model ( 'Trip_Details_Model' );
		$this->load->model ( 'Taxi_Details_Model' );
		$this->load->model ( 'Emergency_Request_Details_Model' );
		$this->load->model ( 'Driver_Model' );
		$this->load->model ( 'Driver_Personal_Details_Model' );
		$this->load->model ( 'Passenger_Model' );
		$this->load->model ( 'v1/Common_Api_Webservice_Model' );
		$this->load->model ( 'Emergency_Request_Details_Model' );
		$this->load->model ( 'Driver_Dispatch_Details_Model' );
		$this->load->model ( 'Sms_Template_Model' );
		$this->load->model ( 'Notification_Model' );
		$this->load->model ( 'Data_Analytics_Model' );
		$this->load->model ( 'Zone_Details_Model' );
		$this->load->model ( 'Data_Attributes_Model' );
		$this->load->model ( 'Entity_Model' );
		$this->load->model ( 'User_Login_Details_Model' );
		$this->load->model ( 'Taxi_Rejected_Trip_Details_Model' );
	}
	public function index() {
	}
	public function dispatchDriveLaterTrips() {
		$zone_details = NULL;
		$is_fifo_zone_id = 0;
		$trip_later_list = $this->Cronjob_Query_Model->getDriverLaterTripList ();
		if ($trip_later_list) {
			foreach ( $trip_later_list as $list ) {
				log_message ( 'info', getCurrentDateTime () . "  " . $list->tripId );
				if ($list->zoneId) {
					$zone_details = NULL;
					$zone_details = $this->Zone_Details_Model->getById ( $list->zoneId );
					if ($zone_details) {
						if ($zone_details->dispatchType == Dispatch_Type_Enum::FIFO) {
							$is_fifo_zone_id = 1;
						}
					}
				}
				$taxi_request_details = $this->getTaxiForTrip ( $list->tripId, $is_fifo_zone_id );
				sleep ( 1 );
			}
		}
		debug_exit ( 'completed' );
		log_message ( 'info', getCurrentDateTime () . "  Driver later trip dispatch cron job" );
	}
	public function checkTripDispatchStatusUpdate() {
		$zone_details = NULL;
		$is_fifo_zone_id = 0;
		$trip_dispatch_failed_list = $this->Cronjob_Query_Model->checkTripDispatchStatusUpdate ();
		if ($trip_dispatch_failed_list) {
			foreach ( $trip_dispatch_failed_list as $list ) {
				log_message ( 'info', getCurrentDateTime () . "  " . $list->tripId );
				if ($list->zoneId) {
					$zone_details = NULL;
					$zone_details = $this->Zone_Details_Model->getById ( $list->zoneId );
					if ($zone_details) {
						if ($zone_details->dispatchType == Dispatch_Type_Enum::FIFO) {
							$is_fifo_zone_id = 1;
						}
					}
				}
				$taxi_request_details = $this->getTaxiForTrip ( $list->tripId, $is_fifo_zone_id );
				sleep ( 1 );
			}
		}
		log_message ( 'info', getCurrentDateTime () . "  Check Driver status update cron job" );
	}
	private function getTaxiForTrip($trip_id, $is_fifo_zone_id) {
		$taxi_request_id = FALSE;
		$driver_taxi_details = NULL;
		$distance = 0;
		$estimated_pickup_distance = 0;
		$estimated_pickup_minute = 0;
		
		$trip_details = $this->Trip_Details_Model->getById ( $trip_id );
		$passenger_details = $this->Passenger_Model->getById ( $trip_details->passengerId );
		
		if (TimeIsBetweenTwoTimes ( DAY_START_TIME, DAY_END_TIME, $trip_details->pickupDatetime )) {
			$distance = RIDE_LATER_LIMIT_DAY_ONE;
		} else if (TimeIsBetweenTwoTimes ( NIGHT_START_TIME, NIGHT_END_TIME, $trip_details->pickupDatetime )) {
			$distance = RIDE_LATER_LIMIT_NIGHT_ONE;
		}
		if ($is_fifo_zone_id) {
			$driver_taxi_details = $this->Driver_Api_Webservice_Model->getDriverListForFifoTrip ( $trip_id, $distance );
		} else {
			$driver_taxi_details = $this->Driver_Api_Webservice_Model->getDriverListForTrip ( $trip_id, $distance );
		}
		if (! $driver_taxi_details) {
			$distance = 0;
			if (TimeIsBetweenTwoTimes ( DAY_START_TIME, DAY_END_TIME, $trip_details->pickupDatetime )) {
				$distance = RIDE_LATER_LIMIT_DAY_TWO;
			} else if (TimeIsBetweenTwoTimes ( NIGHT_START_TIME, NIGHT_END_TIME, $trip_details->pickupDatetime )) {
				$distance = RIDE_LATER_LIMIT_NIGHT_TWO;
			}
			if ($is_fifo_zone_id) {
				$driver_taxi_details = $this->Driver_Api_Webservice_Model->getDriverListForFifoTrip ( $trip_id, $distance );
			} else {
				$driver_taxi_details = $this->Driver_Api_Webservice_Model->getDriverListForTrip ( $trip_id, $distance );
			}
		}
		$minutes = 0;
		$interval = abs ( strtotime ( $trip_details->pickupDatetime ) - getCurrentUnixDateTime () );
		$minutes = round ( $interval / 60 );
		// check taxi request for particular trip already exists or not
		$taxi_request_exists = $this->Taxi_Request_Details_Model->getByKeyValueArray ( array (
				'tripId' => $trip_id 
		) );
		if ($driver_taxi_details) {
			if ($taxi_request_exists) {
				$update_taxi_request = array (
						'totalAvailableDriver' => count ( $driver_taxi_details ),
						'selectedDriverId' => $driver_taxi_details [0]->driverId,
						'taxiRequestStatus' => Taxi_Request_Status_Enum::AVAILABLE_TRIP 
				);
				$taxi_request_id = $this->Taxi_Request_Details_Model->update ( $update_taxi_request, array (
						'tripId' => $trip_id 
				) );
			} else {
				$insert_taxi_request = array (
						'tripId' => $trip_id,
						'totalAvailableDriver' => count ( $driver_taxi_details ),
						'selectedDriverId' => $driver_taxi_details [0]->driverId,
						'taxiRequestStatus' => Taxi_Request_Status_Enum::AVAILABLE_TRIP 
				);
				$taxi_request_id = $this->Taxi_Request_Details_Model->insert ( $insert_taxi_request );
			}
			// @todo update driiver rejected count $ consecutive reject count of driver to driver dispath details table
			$driver_dispatch_exists = NULL;
			$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
					'driverId' => $trip_details->driverId 
			) );
			if ($driver_dispatch_exists) {
				
				$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
						'isEligible' => Status_Type_Enum::INACTIVE 
				), array (
						'driverId' => $trip_details->driverId 
				) );
			} else {
				$insert_dispatch_data = array (
						'driverId' => $trip_details->driverId,
						'isEligible' => Status_Type_Enum::INACTIVE 
				);
				$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
			}
			// end driiver rejected count $ consecutive reject count of driver to driver dispath details table
			// To get estimated distance & time to reach driver for pickup location from google API
			if ((! $estimated_pickup_distance || $estimated_pickup_minute || $estimated_pickup_distance == '' || $estimated_pickup_minute == '') || ($driver_taxi_details [0]->driverLatitude && $driver_taxi_details [0]->driverLongitude && $driver_taxi_details [0]->pickupLatitude && $driver_taxi_details [0]->pickupLongitude)) {
				$URL = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=" . $driver_taxi_details [0]->driverLatitude . "," . $driver_taxi_details [0]->driverLongitude . "&destinations=" . $driver_taxi_details [0]->pickupLatitude . "," . $driver_taxi_details [0]->pickupLongitude . "&key=" . GOOGLE_MAP_API_KEY;
				
				$curl = curl_init ();
				curl_setopt ( $curl, CURLOPT_URL, $URL );
				curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, true );
				curl_setopt ( $curl, CURLOPT_HEADER, false );
				$result = curl_exec ( $curl );
				curl_close ( $curl );
				// return $result; ;
				
				$response = json_decode ( $result );
				if ($response) {
					if ($response->status) {
						if ($response->rows [0]->elements [0]->distance->text) {
							$distance = explode ( ' ', $response->rows [0]->elements [0]->distance->text );
							if ($distance [1] == 'm') {
								$estimated_pickup_distance = round ( ($distance [0] / 1000), 1 ) + 0.5;
							} else if ($distance [1] == 'km') {
								$estimated_pickup_distance = round ( ($distance [0]), 1 ) + 0.5;
							}
						}
						if ($response->rows [0]->elements [0]->duration->text) {
							$time = explode ( ' ', $response->rows [0]->elements [0]->duration->text );
							
							if ($time [1] == 'hour' || $time [1] == 'hours') {
								$estimated_pickup_minute += ($time [0] * 60) + $time [2];
							} else if ($time [1] == 'mins') {
								$estimated_pickup_minute = $time [0];
							}
						}
					}
				}
			}
			// To get estimated distance & time to reach driver for pickup location from google API
			$update_trip_details = $this->Trip_Details_Model->update ( array (
					'driverId' => $driver_taxi_details [0]->driverId,
					'taxiId' => $driver_taxi_details [0]->taxiId,
					'tripStatus' => Trip_Status_Enum::DRIVER_DISPATCHED,
					'dispatchedDatetime' => getCurrentDateTime (),
					'estimatedPickupDistance' => $estimated_pickup_distance,
					'estimatedPickupMinute' => $estimated_pickup_minute,
					'notificationStatus' => Trip_Status_Enum::DRIVER_DISPATCHED,
					'driverAcceptedStatus' => Driver_Accepted_Status_Enum::NONE 
			), array (
					'id' => $trip_id 
			) );
		} else if ($minutes <= 10) {
			
			$update_trip_details = $this->Trip_Details_Model->update ( array (
					'tripStatus' => Trip_Status_Enum::DRIVER_NOT_FOUND,
					'notificationStatus' => Trip_Status_Enum::DRIVER_NOT_FOUND 
			), array (
					'id' => $trip_id 
			) );
			
			$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
					'title' => 'driver_later_cancelled_sms' 
			) );
			
			$message_data = $message_details->content;
			// $message = str_replace("##USERNAME##",$name,$message);
			$message_data = str_replace ( "##PASSENGERNAME##", $passenger_details->firstName . ' ' . $passenger_details->lastName, $message_data );
			$message_data = str_replace ( "##JOBCARDID##", $trip_details->jobCardId, $message_data );
			// print_r($message);exit;
			if ($passenger_details->mobile != "") {
				
				$this->Common_Api_Webservice_Model->sendSMS ( $passenger_details->mobile, $message_data );
			}
		}
		
		return $driver_taxi_details;
	}
	public function getDriverOnTripOffineStatus() {
		$driver_list = $this->Cronjob_Query_Model->getDriverOnTripOffineStatus ();
		if ($driver_list) {
			foreach ( $driver_list as $list ) {
				$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
						'title' => 'driver_ontrip_offline_sms' 
				) );
				$message_data = $message_details->content;
				// $message = str_replace("##USERNAME##",$name,$message);
				// $message_data = str_replace ( "##DRIVERNAME##", $list->driverName, $message_data );
				// $message_data = str_replace ( "##MINUTE##", 3, $message_data );
				// print_r($message);exit;
				/*
				 * if ($list->driverMobile != "") {
				 *
				 * $this->Common_Api_Webservice_Model->sendSMS ( $list->driverMobile, $message_data );
				 * }
				 */
				$update_driver = $this->Driver_Model->update ( array (
						'isNotified' => Status_Type_Enum::ACTIVE 
				), array (
						'id' => $list->driverId 
				) );
				/*
				 * $trip_details=$this->Trip_Details_Model->getById($list->tripId);
				 * $insert_data = array (
				 * 'userId' => $list->driverId,
				 * 'userType' => User_Type_Enum::DRIVER,
				 * 'location' => 'Driver On Trip No Heartbeat SMS',
				 * 'comments'=>'Driver on trip no heartbeat for jobCardId='.$trip_details->jobCardId,
				 * 'emergencyStatus' => Emergency_Status_Enum::OPEN
				 * );
				 * $emergency_id = $this->Emergency_Request_Details_Model->insert ( $insert_data );
				 */
			}
		}
		log_message ( 'info', "Driver on trip message cron job" );
	}
	public function getDriverOnFreeOffineStatus() {
		$driver_list = $this->Cronjob_Query_Model->getDriverOnFreeOffineStatus ();
		if ($driver_list) {
			foreach ( $driver_list as $list ) {
				$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
						'title' => 'driver_ontrip_offline_sms' 
				) );
				$message_data = $message_details->content;
				// $message = str_replace("##USERNAME##",$name,$message);
				// $message_data = str_replace ( "##DRIVERNAME##", $list->driverName, $message_data );
				// $message_data = str_replace ( "##MINUTE##", 2, $message_data );
				// print_r($message);exit;
				
				/*
				 * if ($list->driverMobile != "") {
				 *
				 * $this->Common_Api_Webservice_Model->sendSMS ( $list->driverMobile, $message_data );
				 * }
				 */
				
				$update_driver = $this->Driver_Model->update ( array (
						'isNotified' => Status_Type_Enum::ACTIVE 
				), array (
						'id' => $list->driverId 
				) );
				
				$insert_data = array (
						'userId' => $list->driverId,
						'userType' => User_Type_Enum::DRIVER,
						'location' => 'Driver On Free No Heartbeat SMS sent on ' . getCurrentDateTime (),
						'comments' => 'Driver on free no heartbeat',
						'emergencyStatus' => Emergency_Status_Enum::CLOSED 
				);
				$emergency_id = $this->Emergency_Request_Details_Model->insert ( $insert_data );
			}
		}
		log_message ( 'info', "Driver on free message cron job" );
	}
	public function getDriverOnFreeStatus() {
		$driver_list = $this->Cronjob_Query_Model->getDriverOnFreeStatus ();
		
		if ($driver_list) {
			foreach ( $driver_list as $list ) {
				$this->driverAlertNotification ( $list->driverId, $list->gcmId );
			}
		}
		// $driver_details=$this->Driver_Model->getById(723);
		// $this->driverAlertNotification($driver_details->id,$driver_details->gcmId);
		log_message ( 'info', "Driver on free alert notification cron job" );
	}
	private function driverAlertNotification($driver_id, $gcm_id = NULL) {
		
		// $trip_id = $post_data->trip_id;
		// $driver_id = 2;
		$response = NULL;
		$msg_data = $this->config->item ( 'api' );
		$msg = '';
		$status = 0;
		$firebase_api_key = DRIVER_FIREBASE_API_KEY;
		
		$post_data = array ();
		$response_data = array ();
		
		if ($driver_id) {
			$post_data ['registration_ids'] = ( array ) $gcm_id;
			if ($gcm_id) {
				
				$response_data = array (
						"message" => 'alert_notification',
						"status" => 1,
						"description" => 'Alert notification' 
				);
				$post_data ['data'] = array (
						
						'details' => $response_data 
				);
			} else {
				$msg = $msg_data ['failed'];
				
				$post_data ['data'] = array (
						'message' => $msg,
						'status' => - 1 
				);
			}
			
			$post_data = json_encode ( $post_data );
			$url = "https://fcm.googleapis.com/fcm/send";
			
			$ch = curl_init ();
			curl_setopt ( $ch, CURLOPT_URL, $url );
			curl_setopt ( $ch, CURLOPT_HTTPHEADER, array (
					'Content-Type:application/json',
					'Authorization: key=' . $firebase_api_key 
			) );
			curl_setopt ( $ch, CURLOPT_POST, 1 );
			curl_setopt ( $ch, CURLOPT_POSTFIELDS, $post_data );
			curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
			$response = curl_exec ( $ch );
			curl_close ( $ch );
		}
		return $response;
		// echo json_encode ( array('response'=>$response,'msg'=>$msg,'status'=>$status));
	}
	public function getDriverDailyWorkReport() {
		$report_data = $this->Cronjob_Query_Model->getDriverDailyWorkReport ();
		$file_name = "driver_report_" . getCurrentDate () . ".csv";
		$this->load->helper ( 'csv' );
		$export_data = array ();
		
		$title = array (
				"Driver Code",
				"Driver Name",
				"Driver Mobile",
				"Taxi No",
				"Hours",
				"Minutes" 
		);
		array_push ( $export_data, $title );
		if (! empty ( $report_data )) {
			foreach ( $report_data as $list ) {
				array_push ( $export_data, array (
						$list->driverCode,
						$list->driverName,
						$list->driverMobile,
						$list->taxiRegistrationNo,
						$list->hour,
						$list->minute 
				) );
			}
		}
		
		$response = convert_to_csv ( $export_data, "public/reports", $file_name );
		
		/*
		 * $upload_path = "public/reports";
		 * $attach_file_path="public/reports/".$file_name;
		 * if (file_exists ($attach_file_path)) {
		 * if ($report_data)
		 * {
		 * $this->Common_Api_Webservice_Model->sendEmail('globalkirankumar@gmail.com','Driver Report','PFA of Driver Report On'.getCurrentDate(),'kiran.k@invenzolabs.com',$attach_file_path);
		 * }
		 * }
		 * unlink($attach_file_path);
		 */
	}
	public function getDataAnalytics() {
		$this->load->helper ( 'csv' );
		$data = array ();
		$insert_data = array ();
		
		$data_analytics = NULL;
		$data_analytics = $this->Cronjob_Query_Model->getDataAnalytics ();
		if ($data_analytics) {
			foreach ( $data_analytics as $list ) {
				$insert_data = array ();
				// Intialize keys
				
				$insert_data ['tripId'] = $list->tripId;
				$insert_data ['passengerId'] = ($list->passengerId === NULL) ? 0 : $list->passengerId;
				$insert_data ['passengerName'] = ($list->passengerName === NULL) ? '' : $list->passengerName;
				$insert_data ['passengerGender'] = ($list->passengerGender === NULL) ? '' : $list->passengerGender;
				$insert_data ['passengerDob'] = ($list->passengerDob === NULL) ? '' : $list->passengerDob;
				$insert_data ['passengerMobile'] = ($list->passengerMobile === NULL) ? '' : $list->passengerMobile;
				$insert_data ['passengerEmail'] = ($list->passengerEmail === NULL) ? '' : $list->passengerEmail;
				
				$insert_data ['driverId'] = ($list->driverId === NULL) ? 0 : $list->driverId;
				$insert_data ['driverName'] = ($list->driverName === NULL) ? '' : $list->driverName;
				$insert_data ['driverMobile'] = ($list->driverMobile === NULL) ? '' : $list->driverMobile;
				$insert_data ['driverEmail'] = ($list->driverEmail === NULL) ? '' : $list->driverEmail;
				$insert_data ['driverGender'] = ($list->driverGender === NULL) ? '' : $list->driverGender;
				$insert_data ['driverDob'] = ($list->driverDob === NULL) ? '' : $list->driverDob;
				$insert_data ['driverAddress'] = ($list->driverAddress === NULL) ? '' : $list->driverAddress;
				$insert_data ['driverWallet'] = ($list->driverWallet === NULL) ? 0 : $list->driverWallet;
				$insert_data ['driverCredit'] = ($list->driverCredit === NULL) ? 0 : $list->driverCredit;
				$insert_data ['driverExperience'] = ($list->driverExperience === NULL) ? 0 : $list->driverExperience;
				$insert_data ['driverExperienceName'] = '';
				if ($list->driverExperience) {
					$driver_experience_name = $this->Data_Attributes_Model->getColumnByKeyValue ( 'description', 'id', $list->driverExperience );
					$insert_data ['driverExperienceName'] = $driver_experience_name [0]->description;
				}
				$insert_data ['taxiId'] = ($list->taxiId === NULL) ? 0 : $list->taxiId;
				$insert_data ['taxiRegistrationNo'] = '';
				if ($list->taxiId) {
					$taxi_registration_no = $this->Taxi_Details_Model->getColumnByKeyValue ( 'registrationNo', 'id', $list->taxiId );
					$insert_data ['taxiRegistrationNo'] = $taxi_registration_no [0]->registrationNo;
				}
				
				$insert_data ['jobCardId'] = ($list->jobCardId === NULL) ? '' : $list->jobCardId;
				$insert_data ['zoneId'] = ($list->zoneId === NULL) ? 0 : $list->zoneId;
				$insert_data ['zoneName'] = '';
				if ($list->zoneId) {
					$zone_name = $this->Zone_Details_Model->getColumnByKeyValue ( 'name', 'id', $list->zoneId );
					$insert_data ['zoneName'] = $zone_name [0]->name;
				}
				$insert_data ['dropZoneId'] = ($list->dropZoneId === NULL) ? 0 : $list->dropZoneId;
				$insert_data ['dropZoneName'] = '';
				if ($list->dropZoneId) {
					$drop_zone_name = $this->Zone_Details_Model->getColumnByKeyValue ( 'name', 'id', $list->dropZoneId );
					$insert_data ['dropZoneName'] = $drop_zone_name [0]->name;
				}
				$insert_data ['entityId'] = ($list->entityId === NULL) ? 0 : $list->entityId;
				$insert_data ['entityName'] = '';
				if ($list->entityId) {
					$entity_id = $this->Entity_Model->getColumnByKeyValue ( 'name', 'id', $list->entityId );
					$insert_data ['entityName'] = $entity_id [0]->name;
				}
				$insert_data ['estimatedDistance'] = ($list->estimatedDistance === NULL) ? 0 : $list->estimatedDistance;
				$insert_data ['estimatedTime'] = ($list->estimatedTime === NULL) ? 0 : $list->estimatedTime;
				$insert_data ['pickupLocation'] = ($list->pickupLocation === NULL) ? '' : $list->pickupLocation;
				$insert_data ['pickupLatitude'] = ($list->pickupLatitude === NULL) ? 0 : $list->pickupLatitude;
				$insert_data ['pickupLongitude'] = ($list->pickupLongitude === NULL) ? 0 : $list->pickupLongitude;
				$insert_data ['pickupDatetime'] = ($list->pickupDatetime === NULL) ? 0 : $list->pickupDatetime;
				$insert_data ['dispatchedDatetime'] = ($list->dispatchedDatetime === NULL) ? 0 : $list->dispatchedDatetime;
				$insert_data ['driverAcceptedDatetime'] = ($list->driverAcceptedDatetime === NULL) ? 0 : $list->driverAcceptedDatetime;
				$insert_data ['taxiArrivedDatetime'] = ($list->taxiArrivedDatetime === NULL) ? 0 : $list->taxiArrivedDatetime;
				$insert_data ['actualPickupDatetime'] = ($list->actualPickupDatetime === NULL) ? 0 : $list->actualPickupDatetime;
				$insert_data ['estimatedPickupDistance'] = ($list->estimatedPickupDistance === NULL) ? 0 : $list->estimatedPickupDistance;
				$insert_data ['estimatedPickupMinute'] = ($list->estimatedPickupMinute === NULL) ? 0 : $list->estimatedPickupMinute;
				$insert_data ['dropLocation'] = ($list->dropLocation === NULL) ? '' : $list->dropLocation;
				$insert_data ['dropLatitude'] = ($list->dropLatitude === NULL) ? 0 : $list->dropLatitude;
				$insert_data ['dropLongitude'] = ($list->dropLongitude === NULL) ? 0 : $list->dropLongitude;
				$insert_data ['actualDropLocation'] = ($list->actualDropLocation === NULL) ? '' : $list->actualDropLocation;
				$insert_data ['actualDropLatitude'] = ($list->actualDropLatitude === NULL) ? 0 : $list->actualDropLatitude;
				$insert_data ['actualDropLongitude'] = ($list->actualDropLongitude === NULL) ? 0 : $list->actualDropLongitude;
				$insert_data ['dropDatetime'] = ($list->dropDatetime === NULL) ? 0 : $list->dropDatetime;
				$insert_data ['tripStatus'] = ($list->tripStatus === NULL) ? 0 : $list->tripStatus;
				$insert_data ['tripStatusName'] = '';
				if ($list->tripStatus) {
					$trip_status_name = $this->Data_Attributes_Model->getColumnByKeyValue ( 'description', 'id', $list->tripStatus );
					$insert_data ['tripStatusName'] = $trip_status_name [0]->description;
				}
				
				$insert_data ['totalDriversFoundOnDispatch'] = ($list->totalDriversFoundOnDispatch === NULL) ? 0 : $list->totalDriversFoundOnDispatch;
				
				$insert_data ['taxiCategoryType'] = ($list->taxiCategoryType === NULL) ? 0 : $list->taxiCategoryType;
				$insert_data ['taxiCategoryTypeName'] = '';
				if ($list->taxiCategoryType) {
					$taxi_category_name = $this->Data_Attributes_Model->getColumnByKeyValue ( 'description', 'id', $list->taxiCategoryType );
					$insert_data ['taxiCategoryTypeName'] = $taxi_category_name [0]->description;
				}
				
				$insert_data ['tripType'] = ($list->tripType === NULL) ? 0 : $list->tripType;
				$insert_data ['tripTypeName'] = '';
				if ($list->tripType) {
					$trip_type_name = $this->Data_Attributes_Model->getColumnByKeyValue ( 'description', 'id', $list->tripType );
					$insert_data ['tripTypeName'] = $trip_type_name [0]->description;
				}
				$insert_data ['paymentMode'] = ($list->paymentMode === NULL) ? 0 : $list->paymentMode;
				$insert_data ['paymentModeName'] = '';
				
				if ($list->paymentMode) {
					$payment_mode_name = $this->Data_Attributes_Model->getColumnByKeyValue ( 'description', 'id', $list->paymentMode );
					$insert_data ['paymentModeName'] = $payment_mode_name [0]->description;
				}
				
				$insert_data ['bookedFrom'] = ($list->bookedFrom === NULL) ? 0 : $list->bookedFrom;
				$insert_data ['bookedFromName'] = '';
				if ($list->bookedFrom) {
					$booked_from_name = $this->Data_Attributes_Model->getColumnByKeyValue ( 'description', 'id', $list->bookedFrom );
					$insert_data ['bookedFromName'] = $booked_from_name [0]->description;
				}
				$insert_data ['driverRating'] = ($list->driverRating === NULL) ? 0 : $list->driverRating;
				$insert_data ['driverComments'] = ($list->driverComments === NULL) ? '' : $list->driverComments;
				$insert_data ['passengerRating'] = ($list->passengerRating === NULL) ? 0 : $list->passengerRating;
				$insert_data ['passengerComments'] = ($list->passengerComments === NULL) ? '' : $list->passengerComments;
				$insert_data ['passengerRejectComments'] = ($list->passengerRejectComments === NULL) ? '' : $list->passengerRejectComments;
				$insert_data ['promoCode'] = ($list->promoCode === NULL) ? '' : $list->promoCode;
				$insert_data ['promoCodeLimit'] = ($list->promoCodeLimit === NULL) ? 0 : $list->promoCodeLimit;
				$insert_data ['promoCodeUserLimit'] = ($list->promoCodeUserLimit === NULL) ? 0 : $list->promoCodeUserLimit;
				$insert_data ['promoStartDatetime'] = ($list->promoStartDatetime === NULL) ? 0 : $list->promoStartDatetime;
				$insert_data ['promoEndDatetime'] = ($list->promoEndDatetime === NULL) ? 0 : $list->promoEndDatetime;
				$insert_data ['promoDiscountPercentage'] = ($list->promoDiscountPercentage === NULL) ? 0 : $list->promoDiscountPercentage;
				$insert_data ['promoDiscountValue'] = ($list->promoDiscountValue === NULL) ? 0 : $list->promoDiscountValue;
				$insert_data ['promoZoneId'] = ($list->promoZoneId === NULL) ? 0 : $list->promoZoneId;
				$insert_data ['promoZoneIdName'] = '';
				if ($list->promoZoneId) {
					$prome_zone_id_name = $this->Zone_Details_Model->getColumnByKeyValue ( 'name', 'id', $list->promoZoneId );
					$insert_data ['promoZoneIdName'] = $prome_zone_id_name [0]->name;
				}
				$insert_data ['promoZoneId2'] = ($list->promoZoneId2 === NULL) ? 0 : $list->promoZoneId2;
				$insert_data ['promoZoneId2Name'] = '';
				if ($list->promoZoneId2) {
					$prome_zone_id2_name = $this->Zone_Details_Model->getColumnByKeyValue ( 'name', 'id', $list->promoZoneId2 );
					$insert_data ['promoZoneId2Name'] = $prome_zone_id2_name [0]->name;
				}
				$insert_data ['promoEntityId'] = ($list->promoEntityId === NULL) ? 0 : $list->promoEntityId;
				$insert_data ['promoEntityIdName'] = '';
				if ($list->promoEntityId) {
					$promo_entity_name = $this->Entity_Model->getColumnByKeyValue ( 'name', 'id', $list->promoEntityId );
					$insert_data ['promoEntityIdName'] = $promo_entity_name [0]->name;
				}
				$insert_data ['travelledDistance'] = ($list->travelledDistance === NULL) ? 0 : $list->travelledDistance;
				$insert_data ['actualTravelledDistance'] = ($list->actualTravelledDistance === NULL) ? 0 : $list->actualTravelledDistance;
				$insert_data ['travelledPeriod'] = ($list->travelledPeriod === NULL) ? 0 : $list->travelledPeriod;
				$insert_data ['convenienceCharge'] = ($list->convenienceCharge === NULL) ? 0 : $list->convenienceCharge;
				$insert_data ['travelCharge'] = ($list->travelCharge === NULL) ? 0 : $list->travelCharge;
				$insert_data ['parkingCharge'] = ($list->parkingCharge === NULL) ? 0 : $list->parkingCharge;
				$insert_data ['tollCharge'] = ($list->tollCharge === NULL) ? 0 : $list->tollCharge;
				$insert_data ['surgeCharge'] = ($list->surgeCharge === NULL) ? 0 : $list->surgeCharge;
				$insert_data ['penaltyCharge'] = ($list->penaltyCharge === NULL) ? 0 : $list->penaltyCharge;
				$insert_data ['bookingCharge'] = ($list->bookingCharge === NULL) ? 0 : $list->bookingCharge;
				$insert_data ['taxCharge'] = ($list->taxCharge === NULL) ? 0 : $list->taxCharge;
				$insert_data ['totalTripCharge'] = ($list->totalTripCharge === NULL) ? 0 : $list->totalTripCharge;
				$insert_data ['promoDiscountAmount'] = ($list->promoDiscountAmount === NULL) ? 0 : $list->promoDiscountAmount;
				$insert_data ['walletPaymentAmount'] = ($list->walletPaymentAmount === NULL) ? 0 : $list->walletPaymentAmount;
				$insert_data ['adminAmount'] = ($list->adminAmount === NULL) ? 0 : $list->adminAmount;
				$insert_data ['taxPercentage'] = ($list->taxPercentage === NULL) ? 0 : $list->taxPercentage;
				$insert_data ['paymentStatus'] = ($list->paymentStatus === NULL) ? 0 : $list->paymentStatus;
				$insert_data ['transactionId'] = ($list->transactionId === NULL) ? 0 : $list->transactionId;
				$insert_data ['invoiceNo'] = ($list->invoiceNo === NULL) ? 0 : $list->invoiceNo;
				$insert_data ['driverEarning'] = ($list->driverEarning === NULL) ? 0 : $list->driverEarning;
				
				$insert_data ['createdBy'] = ($list->createdBy === NULL) ? 0 : $list->createdBy;
				$insert_data ['createdByName'] = '';
				$created_first_name = '';
				$created_last_name = '';
				if ($list->createdBy) {
					$created_first_name = $this->User_Login_Details_Model->getColumnByKeyValue ( 'firstName', 'id', $list->createdBy );
					$created_last_name = $this->User_Login_Details_Model->getColumnByKeyValue ( 'lastName', 'id', $list->createdBy );
					
					$insert_data ['createdByName'] = $created_first_name [0]->firstName . " " . $created_last_name [0]->lastName;
				}
				
				$insert_data ['updatedBy'] = ($list->createdBy === NULL) ? 0 : $list->updatedBy;
				$insert_data ['updatedByName'] = '';
				$updated_first_name = '';
				$updated_last_name = '';
				if ($list->updatedBy) {
					$updated_first_name = $this->User_Login_Details_Model->getColumnByKeyValue ( 'firstName', 'id', $list->updatedBy );
					$updated_last_name = $this->User_Login_Details_Model->getColumnByKeyValue ( 'lastName', 'id', $list->updatedBy );
					
					$insert_data ['updatedByName'] = $updated_first_name [0]->firstName . " " . $updated_last_name [0]->lastName;
				}
				
				$insert_data ['createdDatetime'] = $list->createdDatetime;
				$insert_data ['updatedDatetime'] = $list->updatedDatetime;
				
				$insert_data ['rejectionType'] = 0;
				$insert_data ['rejectionTypeName'] = '';
				
				if ($list->rejectedDriverList != '' || ! empty ( $list->rejectedDriverList ) || $list->rejectedDriverList === NULL) {
					$rejected_driver_list = explode ( ',', $list->rejectedDriverList );
					if (count ( $rejected_driver_list ) > 0) {
						foreach ( $rejected_driver_list as $val ) {
							$driver_frist_name = '';
							$driver_last_name = '';
							$insert_data ['driverId'] = $val;
							if ($val) {
								$driver_frist_name = $this->Driver_Model->getColumnByKeyValue ( 'firstName', 'id', $val );
								$driver_last_name = $this->Driver_Model->getColumnByKeyValue ( 'lastName', 'id', $val );
								$insert_data ['driverName'] = $driver_frist_name [0]->firstName . " " . $driver_last_name [0]->lastName;
								
								$driver_mobile = $this->Driver_Model->getColumnByKeyValue ( 'mobile', 'id', $val );
								$insert_data ['driverMobile'] = $driver_mobile [0]->mobile;
								
								$driver_email = $this->Driver_Model->getColumnByKeyValue ( 'email', 'id', $val );
								$insert_data ['driverEmail'] = $driver_email [0]->email;
								
								$driver_gender = $this->Driver_Personal_Details_Model->getColumnByKeyValue ( 'gender', 'driverId', $val );
								$insert_data ['driverGender'] = $driver_gender [0]->gender;
								
								$driver_dob = $this->Driver_Personal_Details_Model->getColumnByKeyValue ( 'dob', 'driverId', $val );
								$insert_data ['driverDob'] = $driver_dob [0]->dob;
								
								$driver_address = $this->Driver_Personal_Details_Model->getColumnByKeyValue ( 'address', 'driverId', $val );
								$insert_data ['driverAddress'] = $driver_address [0]->address;
								
								$driver_wallet = $this->Driver_Model->getColumnByKeyValue ( 'driverWallet', 'id', $val );
								$insert_data ['driverWallet'] = $driver_wallet [0]->driverWallet;
								
								$driver_credit = $this->Driver_Model->getColumnByKeyValue ( 'driverCredit', 'id', $val );
								$insert_data ['driverCredit'] = $driver_credit [0]->driverCredit;
								
								$driver_experience = $this->Driver_Model->getColumnByKeyValue ( 'experience', 'id', $val );
								$insert_data ['driverExperience'] = $driver_experience [0]->experience;
								
								$driver_experience_name = $this->Data_Attributes_Model->getColumnByKeyValue ( 'description', 'id', $insert_data ['driverExperience'] );
								$insert_data ['driverExperienceName'] = $driver_experience_name [0]->description;
								
								$taxi_id = $this->Taxi_Rejected_Trip_Details_Model->getColumnByKeyValue ( 'taxiId', 'tripId', $insert_data ['tripId'] );
								$insert_data ['taxiId'] = $taxi_id [0]->taxiId;
								
								$insert_data ['taxiRegistrationNo'] = '';
								$taxi_registration_no = $this->Taxi_Details_Model->getColumnByKeyValue ( 'registrationNo', 'id', $insert_data ['taxiId'] );
								$insert_data ['taxiRegistrationNo'] = $taxi_registration_no [0]->registrationNo;
								
								$insert_data ['tripStatus'] = Trip_Status_Enum::CANCELLED_BY_DRIVER;
								$trip_status_name = $this->Data_Attributes_Model->getColumnByKeyValue ( 'description', 'id', $insert_data ['tripStatus'] );
								$insert_data ['tripStatusName'] = $trip_status_name [0]->description;
								
								$rejection_type = $this->Taxi_Rejected_Trip_Details_Model->getColumnByKeyValue ( 'rejectionType', 'tripId', $insert_data ['tripId'] );
								$insert_data ['rejectionType'] = $rejection_type [0]->rejectionType;
								
								$rejection_type_name = $this->Data_Attributes_Model->getColumnByKeyValue ( 'description', 'id', $insert_data ['rejectionType'] );
								$insert_data ['rejectionTypeName'] = $rejection_type_name [0]->description;
							}
							
							$insert_data ['travelledDistance'] = 0;
							$insert_data ['actualTravelledDistance'] = 0;
							$insert_data ['travelledPeriod'] = 0;
							$insert_data ['convenienceCharge'] = 0;
							$insert_data ['travelCharge'] = 0;
							$insert_data ['parkingCharge'] = 0;
							$insert_data ['tollCharge'] = 0;
							$insert_data ['surgeCharge'] = 0;
							$insert_data ['penaltyCharge'] = 0;
							$insert_data ['bookingCharge'] = 0;
							$insert_data ['taxCharge'] = 0;
							$insert_data ['totalTripCharge'] = 0;
							$insert_data ['promoDiscountAmount'] = 0;
							$insert_data ['walletPaymentAmount'] = 0;
							$insert_data ['adminAmount'] = 0;
							$insert_data ['taxPercentage'] = 0;
							$insert_data ['paymentStatus'] = '';
							$insert_data ['transactionId'] = '';
							$insert_data ['invoiceNo'] = '';
							$insert_data ['driverEarning'] = 0;
							
							$this->db = $this->load->database ( 'analytics', TRUE );
							
							$data_analytics_id = $this->Data_Analytics_Model->insert ( $insert_data );
							$this->db = $this->load->database ( 'default', TRUE );
						}
					}
				}
				$this->db = $this->load->database ( 'analytics', TRUE );
				
				$data_analytics_id = $this->Data_Analytics_Model->insert ( $insert_data );
				$this->db = $this->load->database ( 'default', TRUE );
				array_push ( $data, $insert_data );
			}
			debug ( $data );
			$response = $this->getCsvDataAnalytics ( $data );
		}
	}
	public function getCsvDataAnalytics($insert_data) {
		debug ( $insert_data );
		$this->load->helper ( 'csv' );
		$export_data = array ();
		
		$title = array (
				"tripId",
				"passengerId",
				"passengerName",
				"passengerGender",
				"passengerDob",
				"passengerMobile",
				"passengerEmail",
				"driverId",
				"driverName",
				"driverMobile",
				"driverEmail",
				"driverGender",
				"driverDob",
				"driverAddress",
				"driverWallet",
				"driverCredit",
				"driverExperience",
				"driverExperienceName",
				"taxiId",
				"taxiRegistrationNo",
				"jobCardId",
				"zoneId",
				"zoneName",
				"dropZoneId",
				"dropZoneName",
				"entityId",
				"entityName",
				"estimatedDistance",
				"estimatedTime",
				"pickupLocation",
				"pickupLatitude",
				"pickupLongitude",
				"pickupDatetime",
				"dispatchedDatetime",
				"driverAcceptedDatetime",
				"taxiArrivedDatetime",
				"actualPickupDatetime",
				"estimatedPickupDistance",
				"estimatedPickupMinute",
				"dropLocation",
				"dropLatitude",
				"dropLongitude",
				"actualDropLocation",
				"actualDropLatitude",
				"actualDropLongitude",
				"dropDatetime",
				"tripStatus",
				"tripStatusName",
				"rejectionType",
				"rejectionTypeName",
				"totalDriversFoundOnDispatch",
				"taxiCategoryType",
				"taxiCategoryTypeName",
				"tripType",
				"tripTypeName",
				"paymentMode",
				"paymentModeName",
				"bookedFrom",
				"bookedFromName",
				"driverRating",
				"driverComments",
				"passengerRating",
				"passengerComments",
				"passengerRejectComments",
				"promoCode",
				"promoCodeLimit",
				"promoCodeUserLimit",
				"promoStartDatetime",
				"promoEndDatetime",
				"promoZoneId",
				"promoZoneId2",
				"promoEntityId",
				"promoZoneIdName",
				"promoZoneId2Name",
				"promoEntityIdName",
				"promoDiscountPercentage",
				"promoDiscountValue",
				"travelledDistance",
				"actualTravelledDistance",
				"travelledPeriod",
				"convenienceCharge",
				"travelCharge",
				"parkingCharge",
				"tollCharge",
				"surgeCharge",
				"penaltyCharge",
				"bookingCharge",
				"taxCharge",
				"totalTripCharge",
				"promoDiscountAmount",
				"walletPaymentAmount",
				"adminAmount",
				"taxPercentage",
				"paymentStatus",
				"transactionId",
				"invoiceNo",
				"driverEarning",
				"createdBy",
				"createdByName",
				"updatedBy",
				"updatedByName",
				"createdDatetime",
				"updatedDatetime" 
		);
		// array_push ( $export_data,$title);
		if (! empty ( $insert_data )) {
			foreach ( $insert_data as $list ) {
				
				array_push ( $export_data, array (
						$list ['tripId'],
						$list ['passengerId'],
						$list ['passengerName'],
						$list ['passengerGender'],
						$list ['passengerDob'],
						$list ['passengerMobile'],
						$list ['passengerEmail'],
						$list ['driverId'],
						$list ['driverName'],
						$list ['driverMobile'],
						$list ['driverEmail'],
						$list ['driverGender'],
						$list ['driverDob'],
						$list ['driverAddress'],
						$list ['driverWallet'],
						$list ['driverCredit'],
						$list ['driverExperience'],
						$list ['driverExperienceName'],
						$list ['taxiId'],
						$list ['taxiRegistrationNo'],
						$list ['jobCardId'],
						$list ['zoneId'],
						$list ['zoneName'],
						$list ['dropZoneId'],
						$list ['dropZoneName'],
						$list ['entityId'],
						$list ['entityName'],
						$list ['estimatedDistance'],
						$list ['estimatedTime'],
						$list ['pickupLocation'],
						$list ['pickupLatitude'],
						$list ['pickupLongitude'],
						$list ['pickupDatetime'],
						$list ['dispatchedDatetime'],
						$list ['driverAcceptedDatetime'],
						$list ['taxiArrivedDatetime'],
						$list ['actualPickupDatetime'],
						$list ['estimatedPickupDistance'],
						$list ['estimatedPickupMinute'],
						$list ['dropLocation'],
						$list ['dropLatitude'],
						$list ['dropLongitude'],
						$list ['actualDropLocation'],
						$list ['actualDropLatitude'],
						$list ['actualDropLongitude'],
						$list ['dropDatetime'],
						$list ['tripStatus'],
						$list ['tripStatusName'],
						$list ['rejectionType'],
						$list ['rejectionTypeName'],
						$list ['totalDriversFoundOnDispatch'],
						$list ['taxiCategoryType'],
						$list ['taxiCategoryTypeName'],
						$list ['tripType'],
						$list ['tripTypeName'],
						$list ['paymentMode'],
						$list ['paymentModeName'],
						$list ['bookedFrom'],
						$list ['bookedFromName'],
						$list ['driverRating'],
						$list ['driverComments'],
						$list ['passengerRating'],
						$list ['passengerComments'],
						$list ['passengerRejectComments'],
						$list ['promoCode'],
						$list ['promoCodeLimit'],
						$list ['promoCodeUserLimit'],
						$list ['promoStartDatetime'],
						$list ['promoEndDatetime'],
						$list ['promoZoneId'],
						$list ['promoZoneId2'],
						$list ['promoEntityId'],
						$list ['promoZoneIdName'],
						$list ['promoZoneId2Name'],
						$list ['promoEntityIdName'],
						$list ['promoDiscountPercentage'],
						$list ['promoDiscountValue'],
						$list ['travelledDistance'],
						$list ['actualTravelledDistance'],
						$list ['travelledPeriod'],
						$list ['convenienceCharge'],
						$list ['travelCharge'],
						$list ['parkingCharge'],
						$list ['tollCharge'],
						$list ['surgeCharge'],
						$list ['penaltyCharge'],
						$list ['bookingCharge'],
						$list ['taxCharge'],
						$list ['totalTripCharge'],
						$list ['promoDiscountAmount'],
						$list ['walletPaymentAmount'],
						$list ['adminAmount'],
						$list ['taxPercentage'],
						$list ['paymentStatus'],
						$list ['transactionId'],
						$list ['invoiceNo'],
						$list ['driverEarning'],
						$list ['createdBy'],
						$list ['createdByName'],
						$list ['updatedBy'],
						$list ['updatedByName'],
						$list ['createdDatetime'],
						$list ['updatedDatetime'] 
				) );
			}
		}
		$chunk_export_data = array_chunk ( $export_data, 1000, true );
		$i = 1;
		foreach ( $chunk_export_data as $data ) {
			$file_name = "dataAnalytics_" . getCurrentDate () . "_" . $i . ".csv";
			$upload_path = "/home/ubuntu/reports/";
			$csv_data = array ();
			array_push ( $csv_data, $title );
			foreach ( $data as $list ) {
				array_push ( $csv_data, $list );
			}
			$response = convert_to_csv ( $csv_data, $upload_path, $file_name );
			$i ++;
		}
	}
}