<?php
class TripTransaction extends My_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->helper ( 'form' );
		$this->load->model ( 'Taxi_Query_Model' );
		$this->load->model ( 'Driver_Model' );
		$this->load->model ( 'Taxi_Device_Details_Model' );
		$this->load->model ( 'Taxi_Device_Install_History_Model' );
		$this->load->model ( 'Taxi_Details_Model' );
		$this->load->model ( 'Trip_Details_Model' );
		$this->load->model ( 'Driver_Taxi_Mapping_Model' );
		$this->load->model ( 'Zone_Details_Model' );
		$this->load->model ( 'Entity_Model' );
		$this->load->model ( 'Driver_Personal_Details_Model' );
		$this->load->model ( 'Data_Attributes_Model' );
		$this->load->model ( 'Sms_Template_Model' );
		$this->load->model ( 'Common_Api_Webservice_Model' );
		$this->load->model ( 'Driver_Api_Webservice_Model' );
	}
	public function index() {
	}
	
	public function getDetailsById($transaction_id, $view_mode) {
	if ($view_mode==EDIT_MODE)
	{
	if ($this->User_Access_Model->writeModule ($this->session->userdata('role_type'),Module_Name_Enum::TRIP) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
	}
	else
	{
		if ($this->User_Access_Model->readModule ($this->session->userdata('role_type'),Module_Name_Enum::TRIP) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
	}
		$data = array ();
		$msg_data = $this->config->item ( 'msg' );
		$this->addJs ( 'app/trip-transaction.js' );
		
		$data = array (
				'mode' => $view_mode 
		);
		//print_r($data);exit;
		$data ['activetab'] = "trip";
		$data ['active_menu'] = 'trip';
		 
		if ($this->input->post ( 'id' )) {
			$transaction_id = $this->input->post ( 'id' );
		}
		$data ['transaction_model'] = $this->Driver_Api_Webservice_Model->getBillDetails ($transaction_id);
		
		if ($this->input->post ( 'id' )) {
			
			$html = $this->load->view ( 'trip/add_edit_completed_trip', $data );
			echo json_encode ( array (
					'html' => $html 
			) );
		} else {
			$this->render ( 'trip/add_edit_completed_trip', $data );
		}
	}
	
	public function getTripTransactionList()
	{
		if ($this->User_Access_Model->showDriver() === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		//$this->addJs ( 'app/trip/trip-table.js' );
		$this->addJs('app/trip-transaction.js');
		$data ['activetab'] = "trip";
		$data ['active_menu'] = 'trip';
		$data ['entity_list'] = $this->Entity_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE,
				'isDeleted' => Status_Type_Enum::INACTIVE
		), 'name' );
		$data ['zone_list'] = $this->Zone_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE,
				'isDeleted' => Status_Type_Enum::INACTIVE
		), 'name' );
		$this->render('trip/manage_completed_trip', $data);
	}
	public function ajax_list()
	{
		//echo"ajaxlist";exit;
		$post_data = $this->input->post ();
		$start_date=getCurrentDateTime();
		if (array_key_exists('start_date', $post_data) && $post_data['start_date']!='' )
		{
			$start_date = $post_data['start_date'];
		}
		$end_date = $post_data['end_date'];
		$entity_id = $post_data['entity_id'];
		$zone_id = $post_data['zone_id'];
		
		$sql_query      = $this->Trip_Transaction_Details_Model->getTripTransactionListQuery($start_date,$end_date,$zone_id,$entity_id);
		$driver_list    = $this->Trip_Transaction_Details_Model->getDataTable($sql_query);
		$data = array();
		$no = $_POST['start'];
		foreach ($driver_list as $list) {
			$no++;
			
			$row = array();
			$row[] = '
				  <button class="btn btn-sm btn-success" id="viewtriptransaction-'.$list->transactionId.'" title="View" ><i class="fa fa-eye" aria-hidden="true"></i></button>';
				 // <button class="btn btn-sm btn-blue"  id="edittriptransaction-'.$list->transactionId.'" title="Edit" ><i class="fa fa-pencil-square-o"></i></button>
                //  <button class="btn btn-sm btn-danger"  id="deletetriptransaction-'.$list->transactionId.'" title="Delete"><i class="fa fa-trash-o"></i></button>';
			$row[] = $list->jobCardId;
			$row[] = $list->entityName;
			$row[] = $list->invoiceNo;
			$row[] = $list->pickupLocation;
			$row[] = $list->pickupDatetime;
			$row[] = $list->dispatchedDatetime;
			$row[] = $list->driverAcceptedDatetime;
			$row[] = $list->taxiArrivedDatetime;
			$row[] = $list->actualPickupDatetime;
			$row[] = $list->dropLocation;
			$row[] = $list->dropDatetime;
			$row[] = $list->taxiRegistrationNo;
			$row[] = $list->taxiCategoryTypeName;
			$row[] = $list->tripTypeName;
			$row [] = ($list->bookedFrom==Signup_Type_Enum::BACKEND)?$list->bookedFromName.' ('.$list->userFirstName.' '.$list->userLastName.')':$list->bookedFromName;
			$row[] = $list->paymentModeName;
			$row[] = $list->passengerInfo;
			$row[] = $list->driverCode;
			$row[] = $list->driverInfo;
			$row[] = $list->promoCode;
			$row[] = $list->totalTripCharge;
			//$row[] = number_format($list->estimatedDistance,1);
			$row[] = number_format($list->travelledDistance,1);
			$row[] = $list->travelledPeriod;
			$row[] = $list->estimatedPeriod;
			$row[] = $list->convenienceCharge;
			$row[] = $list->travelCharge;
			$row[] = $list->promoDiscountAmount;
			$row[] = $list->walletPaymentAmount;
			$row[] = $list->bookingCharge;
			$row[] = $list->penaltyCharge;
			$row[] = $list->adminAmount;
			$row[] = $list->driverEarning;
			$row[] = $list->bookingZone;
			
			//add html for action
	
        
                       /*  $row[] = '<div class="btn-group">
                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a href="javascript:void(0);" id="viewdriver-'.$list->driverId.'">
                                                <i class="icon-docs"></i> View </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);" id="editdriver-'.$list->driverId.'">
                                                <i class="icon-tag"></i> Edit</a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);" id="deletedriver-'.$list->driverId.'">
                                                <i class="icon-user"></i> Delete</a>
                                        </li>

                                    </ul>
                                </div>'; */
			
                        $data[] = $row;
		}
	
		$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->Trip_Transaction_Details_Model->getDataTableAllCount($sql_query),
				"recordsFiltered" => $this->Trip_Transaction_Details_Model->getDataTableFilterCount($sql_query),
				"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
}