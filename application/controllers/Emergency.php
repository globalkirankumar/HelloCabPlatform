<?php
class Emergency extends My_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->helper ( 'form' );
		$this->load->model ( 'Taxi_Query_Model' );
		$this->load->model ( 'Passenger_Model' );
		$this->load->model ( 'Driver_Model' );
		$this->load->model ( 'User_Emergency_Contacts_Model' );
		$this->load->model ( 'Emergency_Request_Details_Model' );
		
	}
	public function index() {
	}
	
	public function getDetailsById($emergency_id, $view_mode) {
	if ($view_mode==EDIT_MODE)
	{
	if ($this->User_Access_Model->writeModule ($this->session->userdata('role_type'),Module_Name_Enum::SOS) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
	}
	else
	{
		if ($this->User_Access_Model->readModule ($this->session->userdata('role_type'),Module_Name_Enum::SOS) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
	}
		$this->addJs ( 'app/emergency.js' );
		// get API messages from message.php
		$msg_data = $this->config->item ( 'msg' );
		$message='';
		$data = array (
				'mode' => $view_mode 
		);
		$data ['activetab'] = "emergency";
		$data ['active_menu'] = 'emergency';
		$data['emergency_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'EMERGENCY_STATUS' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		if ($this->input->post ( 'id' )) {
			$emergency_id = $this->input->post ( 'id' );
		}
		
		if ($emergency_id > 0) {
			if ($view_mode==EDIT_MODE)
			{
				$locked=$this->Emergency_Request_Details_Model->isLocked($emergency_id);
				if ($locked)
				{
					$locked_user=$this->Emergency_Request_Details_Model->getLockedUser($emergency_id);
					$locked_date=$this->Emergency_Request_Details_Model->getLockedDate($emergency_id);
					$locked_date=date('jS, F Y G:ia',strtotime($locked_date));
					$message=$msg_data ['locked'];
					$message=str_replace('##USERNAME##', $locked_user, $message);
					$message.=$locked_date;
			
					$data['mode']=VIEW_MODE;
				}
				else
				{
					$is_locked=$this->Emergency_Request_Details_Model->lock($emergency_id);
				}
			}
			$data ['emergency_model'] = $this->Emergency_Request_Details_Model->getById ( $emergency_id );
			$data['emergency_contact_model']=$this->User_Emergency_Contacts_Model->getByKeyValueArray(array('userId'=>$data ['emergency_model']->userId,'userType'=>$data ['emergency_model']->userType,'status'=>Status_Type_Enum::ACTIVE));
			if ($data ['emergency_model']->userType==User_Type_Enum::PASSENGER)
			{
			$data ['user_model'] = $this->Passenger_Model->getById ( $data ['emergency_model']->userId );
			}
			else 
			{
				$data ['user_model'] = $this->Driver_Model->getById ( $data ['emergency_model']->userId );
			}
			
		} else {
			$data ['emergency_model'] = $this->Emergency_Request_Details_Model->getBlankModel ();
			$data['emergency_contact_model']=$this->User_Emergency_Contacts_Model->getBlankModel();
			$data ['user_model'] = $this->Passenger_Model->getBlankModel ();
		}
		if ($this->input->post ( 'id' )) {
			
			$html = $this->load->view ( 'emergency/add_edit_emergency', $data );
			echo json_encode ( array (
					'html' => $html 
			) );
		} else {
			$this->render ( 'emergency/add_edit_emergency', $data );
		}
	}
	
	public function getEmergencyList()
	{
		/*if ($this->User_Access_Model->showEmergency() === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}*/
		$data = array ();
		
		$this->addJs ( 'app/emergency.js' );
		
		$this->addJs ( 'vendors/datatables/js/jquery.dataTables.min.js' );
		$this->addJs ( 'vendors/datatables/js/dataTables.bootstrap.min.js' );
		
		$this->addCss( 'app/pages/tables.css');
		$this->addCss( 'app/pages/dataTables.bootstrap.css');
		
		$data ['activetab'] = "emergency";
		$data ['active_menu'] = 'emergency';
		$data['emergency_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'EMERGENCY_STATUS' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		$this->render ( 'emergency/manage_emergency', $data );
	}
	public function ajax_list()
	{
		//echo"ajaxlist";exit;
		$sql_query=$this->Emergency_Request_Details_Model->getEmergencyListQuery();
		$emergency_list = $this->Emergency_Request_Details_Model->getDataTable($sql_query);
		$data = array();
		$no = $_POST['start'];
		foreach ($emergency_list as $list) {
			$no++;
			$row = array();
			$row[] = '
		
				  <button class="btn btn-sm btn-success" id="viewemergency-'.$list->EmergencyId.'" title="View" ><i class="fa fa-eye" aria-hidden="true"></i></button>
				  <button class="btn btn-sm btn-blue"  id="editemergency-'.$list->EmergencyId.'" title="Edit" ><i class="fa fa-pencil-square-o"></i></button>';
			// <button class="btn btn-sm btn-danger" id="deleteemergency-'.$list->EmergencyId.'" title="Delete" ><i class="fa fa-trash-o"></i></button>';
					
			$row[] = ($list->userType==User_Type_Enum::PASSENGER)?ucfirst($list->passengerName):ucfirst($list->driverName);
			$row[] = ($list->userType==User_Type_Enum::PASSENGER)?$list->passengerMobile:$list->driverMobile;
			$row[] = ($list->userType==User_Type_Enum::PASSENGER)?$list->passengerEmail:$list->driverEmail;
			$row[] = ($list->userType==User_Type_Enum::PASSENGER)?'Passenger':(($list->userType==User_Type_Enum::DRIVER)?'Driver':'None');
			$row[] = $list->location;
			$row[] = $list->latitude;
			$row[] = $list->longitude;
			$row[] = $list->comments;
			//$row[] = $list->emergencyStatusName;
			
			$emergency_status_class = ($list->emergencyStatus==Emergency_Status_Enum::OPEN || $list->emergencyStatus==Emergency_Status_Enum::PENDING || $list->emergencyStatus==Emergency_Status_Enum::INACTION) ? 'class="btn btn btn-success"' : (($list->emergencyStatus==Emergency_Status_Enum::WRONGREQUEST)?'class="btn btn-danger"':'class="btn btn-blue"');
			$emergency_status_name  = ($list->emergencyStatusName) ? $list->emergencyStatusName : '';
				
			if($list->emergencyStatus==Emergency_Status_Enum::CLOSED || $list->emergencyStatus==Emergency_Status_Enum::WRONGREQUEST)
			{
				$emergency_status_id  = '';
			}else{
				$emergency_status_id =  'id="emergencystatus-' . $list->EmergencyId.'" ';
			}
			$row [] = '<button ' . $emergency_status_id . ' emergencyStatus="'.$list->emergencyStatus.'" ' . $emergency_status_class . '><span>' . $emergency_status_name . '</span></button>';
			
			
			$status_class=($list->status)?(($list->emergencyStatus==Emergency_Status_Enum::CLOSED || $list->emergencyStatus==Emergency_Status_Enum::WRONGREQUEST)?'class="btn btn btn-success"':'class="btn btn-success"'):'class="btn btn-danger"';
			$status_name=($list->status)?'Active':'Inactive';
			if($list->emergencyStatus==Emergency_Status_Enum::CLOSED || $list->emergencyStatus==Emergency_Status_Enum::WRONGREQUEST)
			{
				$status_id =  'id="status-' . $list->EmergencyId.'" ';
			}else{
				$status_id  = '';
			}
			$row[] = '<button '.$status_id.' status="'.$list->status.'" '.$status_class.'><span>'.$status_name.'</span></button>';
			
	
			//add html for action
			
			$data[] = $row;
		}
	
		$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->Emergency_Request_Details_Model->getDataTableAllCount($sql_query),
				"recordsFiltered" => $this->Emergency_Request_Details_Model->getDataTableFilterCount($sql_query),
				"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	
	public function saveEmergency() {
		$this->addJs ( 'app/emergency.js' );
		$data = array ();
		$data = $this->input->post ();
		//print_r($data);exit;
		$emergency_id = $data ['id'];
		$data['activetab']="emergency";
		$data['active_menu'] = 'emergency';
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['failed'];
		if ($emergency_id > 0) {
			$this->Emergency_Request_Details_Model->update ( $data, array (
					'id' => $emergency_id
			) );
			$msg = $msg_data ['updated'];
	
		} else {
			
			$emergency_id = $this->Emergency_Request_Details_Model->insert ( $data );
			$msg = $msg_data ['success'];
		}
		$response = array (
				'msg' => $msg,
				'emergency_id' => $emergency_id
		);
		echo json_encode ( $response );
		// $this->render("passenger/add_edit_passenger", $data);
	
	}
	
	public function changeStatus() {
		if ($this->User_Access_Model->fullAccessModule ($this->session->userdata('role_type'),Module_Name_Enum::SOS) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to change staus", '404' );
			return;
		}
		$data = array ();
	
		$this->addJs ( 'app/emergency.js' );
		$emergency_updated=-1;
		$data = $this->input->post ();
		$emergency_id = $data ['id'];
		$status = $data ['status'];
		
	
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['status_change_failed'];
		if ($emergency_id) {
			$emergency_updated = $this->Emergency_Request_Details_Model->update ( array (
						'status' => $status
				), array (
						'id' => $emergency_id
				) );
			
			$msg = $msg_data ['status_change_success'];
		}
		$response = array (
				'msg' => $msg,
				'status' => $emergency_updated
				
		);
		echo json_encode ( $response );
	}
	
	public function changeEmergencyStatus() {
		if ($this->User_Access_Model->fullAccessModule ($this->session->userdata('role_type'),Module_Name_Enum::SOS) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to change staus", '404' );
			return;
		}
		$data = array ();
	
		$this->addJs ( 'app/emergency.js' );
		$emergency_updated=-1;
		$data = $this->input->post ();
		$emergency_id = $data ['tempEmergencyId'];
		$comments = $data ['comments'];
		$emergency_status = $data ['emergencyStatus'];
	
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['status_change_failed'];
		if ($emergency_id) {
			$emergency_updated = $this->Emergency_Request_Details_Model->update ( array (
					'comments' => $comments,'emergencyStatus'=>$emergency_status
			), array (
					'id' => $emergency_id
			) );
				
			$msg = $msg_data ['status_change_success'];
		}
		$response = array (
				'msg' => $msg,
				'status' => $emergency_updated
			
		);
		echo json_encode ( $response );
	}
}