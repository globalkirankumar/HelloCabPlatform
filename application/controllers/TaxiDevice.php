<?php
class TaxiDevice extends My_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->helper ( 'form' );
		$this->load->model ( 'Taxi_Query_Model' );
		$this->load->model ( 'Taxi_Device_Details_Model' );
		// $this->load->model ( 'Advertise_Served_Details_Model' );
		$this->load->model ( 'Entity_Model' );
		// $this->load->model ( 'City_Model' );
		$this->load->model ( 'Data_Attributes_Model' );
	}
	public function index() {
	}
	public function add() {
	if ($this->User_Access_Model->createModule ($this->session->userdata('role_type'),Module_Name_Enum::TAXI_DEVICE) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$this->addJs ( 'app/taxi-device.js' );
		
		$data = array (
				'mode' => EDIT_MODE 
		);
		$data ['activetab'] = "taxidevice";
		$data ['active_menu'] = 'taxidevice';
		$data ['device_status_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'DEVICE_STATUS' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		
		$data ['taxidevice_model'] = $this->Taxi_Device_Details_Model->getBlankModel ();
		$this->render ( 'taxidevice/add_edit_taxi_device', $data );
	}
	public function saveTaxiDevice() {
		$this->addJs ( 'app/taxi-device.js' );
		$data = array ();
		$data = $this->input->post ();
		//print_r($data);exit;
		$taxi_device_id = $data ['id'];
		$data ['activetab'] = "admin";
		$data ['active_menu'] = 'admin';
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['failed'];
		if (!array_key_exists('isWarranty', $data))
		{
			$data['isWarranty']=0;
		}
		else
		{
			$data['isWarranty']=1;
		}
		if ($taxi_device_id > 0) {
			$this->Taxi_Device_Details_Model->update ( $data, array (
					'id' => $taxi_device_id 
			) );
			
			$msg = $msg_data ['updated'];
		} else {
			
			// $data ['userIdentity'] = trim ( substr ( strtoupper ( str_replace ( ' ', '', $data ['firstName'] . '' . $data ['lastName'] ) ), 0, 5 ) );
			$data ['deviceCode'] = 'DEV' . str_pad ( $this->Taxi_Device_Details_Model->getAutoIncrementValue (), 5, '0', STR_PAD_LEFT );
			$taxi_device_id = $this->Taxi_Device_Details_Model->insert ( $data );
			//update the taxi device Code
			$taxi_device_code_id=($taxi_device_id<=999999)?str_pad ( (string)$taxi_device_id, 6, '0', STR_PAD_LEFT ):$taxi_device_id;
				
			$taxi_device_code = 'DEV' . $taxi_device_code_id;
			$update_device_code=$this->Taxi_Device_Details_Model->update(array('deviceCode' => $taxi_device_code,),array('id'=>$taxi_device_id));
				
			$msg = $msg_data ['success'];
		}
		$response = array (
				'msg' => $msg,
				'taxi_device_id' => $taxi_device_id 
		);
		echo json_encode ( $response );
		// $this->render("passenger/add_edit_passenger", $data);
	}
	public function getDetailsById($taxi_device_id, $view_mode) {
	if ($view_mode==EDIT_MODE)
	{
	if ($this->User_Access_Model->writeModule ($this->session->userdata('role_type'),Module_Name_Enum::TAXI_DEVICE) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
	}
	else
	{
		if ($this->User_Access_Model->readModule ($this->session->userdata('role_type'),Module_Name_Enum::TAXI_DEVICE) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
	} 
		$this->addJs ( 'app/taxi-device.js' );
		// get API messages from message.php
		$msg_data = $this->config->item ( 'msg' );
		$message = '';
		$data = array (
				'mode' => $view_mode 
		);
		$data ['activetab'] = "taxidevice";
		$data ['active_menu'] = 'taxidevice';
		
		$data ['device_status_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'DEVICE_STATUS' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		
		if ($this->input->post ( 'id' )) {
			$taxi_device_id = $this->input->post ( 'id' );
		}
		
		if ($taxi_device_id > 0) {
			if ($view_mode == EDIT_MODE) {
				$locked = $this->Taxi_Device_Details_Model->isLocked ( $taxi_device_id );
				if ($locked) {
					$locked_user = $this->Taxi_Device_Details_Model->getLockedUser ( $taxi_device_id );
					$locked_date = $this->Taxi_Device_Details_Model->getLockedDate ( $taxi_device_id );
					$locked_date = date ( 'jS, F Y G:ia', strtotime ( $locked_date ) );
					$message = $msg_data ['locked'];
					$message = str_replace ( '##USERNAME##', $locked_user, $message );
					$message .= $locked_date;
					
					$data ['mode'] = VIEW_MODE;
				} else {
					$is_locked = $this->Taxi_Device_Details_Model->lock ( $taxi_device_id );
				}
			}
			$data ['taxidevice_model'] = $this->Taxi_Device_Details_Model->getById ( $taxi_device_id );
		} else {
			$data ['taxidevice_model'] = $this->Taxi_Device_Details_Model->getBlankModel ();
		}
		if ($this->input->post ( 'id' )) {
			
			$html = $this->load->view ( 'taxidevice/add_edit_taxi_device', $data );
			echo json_encode ( array (
					'html' => $html 
			) );
		} else {
			$this->render ( 'taxidevice/add_edit_taxi_device', $data );
		}
	}
	public function getTaxiDeviceList() {
		if ($this->User_Access_Model->showTaxiDevice () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$this->addJs ( 'app/taxi-device.js' );
		$data = array ();
		$data ['activetab'] = "taxiowner";
		$data ['active_menu'] = 'taxiowner';
		$this->render ( 'taxidevice/manage_taxi_device', $data );
	}
	public function ajax_list() {
		// echo"ajaxlist";exit;
		$sql_query = $this->Taxi_Device_Details_Model->getTaxiDeviceListQuery ();
		$taxi_device_list = $this->Taxi_Device_Details_Model->getDataTable ( $sql_query );
		// print_r($list);exit;
		$data = array ();
		$no = $_POST ['start'];
		foreach ( $taxi_device_list as $list ) {
			$no ++;
			$row = array ();
			if ($list->isAllocated)
			{
				$row[] = '
			<button class="btn btn-sm btn-success" id="viewtaxidevice-'.$list->taxiDeviceId.'" title="View" ><i class="fa fa-eye" aria-hidden="true"></i></button>
				  <button class="btn btn-sm btn-blue" id="edittaxidevice-'.$list->taxiDeviceId.'" title="Edit" )"><i class="fa fa-pencil-square-o"></i></button>';
			
			}
			else
			{
				$row[] = '
			<button class="btn btn-sm btn-success" id="viewtaxidevice-'.$list->taxiDeviceId.'" title="View" ><i class="fa fa-eye" aria-hidden="true"></i></button>
				  <button class="btn btn-sm btn-blue" id="edittaxidevice-'.$list->taxiDeviceId.'" title="Edit" )"><i class="fa fa-pencil-square-o"></i></button>
                  <button class="btn btn-sm btn-danger" id="deletetaxidevice-'.$list->taxiDeviceId.'"  title="Delete"><i class="fa fa-trash-o"></i></button>';
			}
			
			$row [] = $list->taxiDeviceCode;
			$row [] = $list->imeiNo;
			$row [] = $list->mobile;
			$row [] = $list->deviceStatus;
			$row [] = $list->purchasedDate;
			
			$warranty_class = ($list->isWarranty) ? 'class="btn btn-success"' : 'class="btn btn-danger"';
			$warranty_name = ($list->isWarranty) ? 'Yes' : 'No';
			$row [] = '<button ' . $warranty_class . '><span>' . $warranty_name . '</span></button>';
			
			$allocated_class = ($list->isAllocated) ? 'class="btn btn-success"' : 'class="btn btn-danger"';
			$allocated_name = ($list->isAllocated) ? 'Yes' : 'No';
			$row [] = '<button ' . $allocated_class . '><span>' . $allocated_name . '</span></button>';
			
			$status_class = ($list->status) ? 'class="btn btn-success"' : 'class="btn btn-danger"';
			$status_name = ($list->status) ? 'Active' : 'Inactive';
			$status_id=(!$list->isAllocated)?'id="status-'.$list->taxiDeviceId.'"':'';
			$row [] = '<button '.$status_id.' status="' . $list->status . '" ' . $status_class . '><span>' . $status_name . '</span></button>';
			
			// add html for action
			
			$data [] = $row;
		}
		
		$output = array (
				"draw" => $_POST ['draw'],
				"recordsTotal" => $this->Taxi_Device_Details_Model->getDataTableAllCount ( $sql_query ),
				"recordsFiltered" => $this->Taxi_Device_Details_Model->getDataTableFilterCount ( $sql_query ),
				"data" => $data 
		);
		// output to json format
		echo json_encode ( $output );
	}
	public function getDeviceImpressionList($advt_id, $advt_type) {
		if ($this->User_Access_Model->showTaxiDevice () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		$data ['advt_id'] = $advt_id;
		$data ['advt_type'] = $advt_type;
		$this->addJs ( 'app/taxi-device.js' );
		$data ['activetab'] = "taxiowner";
		$data ['active_menu'] = 'taxiowner';
		
		$html = $this->load->view ( "taxidevice/manage_device_impression", $data );
		echo json_encode ( array (
				'html' => $html 
		) );
		$this->render ( 'taxidevice/manage_device_impression', $data );
	}
	public function device_impression_ajax_list() {
		// echo"ajaxlist";exit;
		$advt_id = $_POST ['advt_id'];
		$advt_type = $_POST ['advt_type'];
		$sql_query = $this->Advertise_Served_Details_Model->getDeviceImpressionListQuery ( $advt_id, $advt_type );
		$device_impression_list = $this->Advertise_Served_Details_Model->getDataTable ( $sql_query );
		// print_r($list);exit;
		$data = array ();
		$no = $_POST ['start'];
		foreach ( $device_impression_list as $list ) {
			$no ++;
			$row = array ();
			$row [] = $list->taxiDeviceCode;
			$row [] = $list->driverName;
			$row [] = $list->driverMobile;
			$row [] = $list->deviceImpressionCount;
			
			$data [] = $row;
		}
		
		$output = array (
				"draw" => $_POST ['draw'],
				"recordsTotal" => $this->Advertise_Served_Details_Model->getDataTableAllCount ( $sql_query ),
				"recordsFiltered" => $this->Advertise_Served_Details_Model->getDataTableFilterCount ( $sql_query ),
				"data" => $data 
		);
		// output to json format
		echo json_encode ( $output );
	}
	public function changeTaxiDeviceStatus() {
		$data = array ();
		
		$this->addJs ( 'app/taxi-device.js' );
		$taxi_device_updated = - 1;
		$data = $this->input->post ();
		$taxi_device_id = $data ['id'];
		$taxi_device_status = $data ['status'];
		
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['status_change_failed'];
		if ($taxi_device_id) {
			$taxi_device_updated = $this->Taxi_Device_Details_Model->update ( array (
					'status' => $taxi_device_status 
			), array (
					'id' => $taxi_device_id 
			) );
			$msg = $msg_data ['status_change_success'];
		}
		$response = array (
				'msg' => $msg,
				'status' => $taxi_device_updated 
		);
		echo json_encode ( $response );
	}
	public function deleteTaxiDevice() {
		if ($this->User_Access_Model->deleteModule ($this->session->userdata('role_type'),Module_Name_Enum::TAXI_DEVICE) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to delete", '404' );
			return;
		}
		$data = array ();
		
		$this->addJs ( 'app/taxi-device.js' );
		$taxi_device_updated = - 1;
		$data = $this->input->post ();
		$taxi_device_id = $data ['id'];
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['delete_failed'];
		if ($taxi_device_id) {
			$taxi_device_updated = $this->Taxi_Device_Details_Model->update ( array (
					'isDeleted' => Status_Type_Enum::ACTIVE 
			), array (
					'id' => $taxi_device_id 
			) );
			$msg = $msg_data ['delete_success'];
		}
		$response = array (
				'msg' => $msg,
				'status' => $taxi_device_updated 
		);
		echo json_encode ( $response );
	}
	
	public function locateCurrentDevice() {
		if ($this->User_Access_Model->showTaxiDevice () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		$this->addJs ( 'app/taxi-device.js' );
		$data ['activetab'] = "taxiowner";
		$data ['active_menu'] = 'taxiowner';
		$this->load->library ( 'googlemap' );
		$this->googlemap->initialize ();
		$config ['center'] = '12.9716, 77.5946';
		$config ['zoom'] = 'auto';
		$this->googlemap->initialize ( $config );
		$device_list = $this->Taxi_Device_Details_Model->getCurrentDeviceDetails ();
		if (count ( $device_list ) > 0) {
			$marker = array ();
			foreach ( $device_list as $list ) {
				$deviceId = $list->taxiDeviceId;
				$device_code = $list->deviceCode;
				$current_latitude = $list->deviceCurrentLatitude;
				$current_longitude = $list->deviceCurrentLongitude;
				$driver_code = $list->driverCode;
				$driver_name = $list->driverFullName;
				$driver_mobile = $list->driverMobile;
				
				$marker ['position'] = "$current_latitude, $current_longitude";
				$marker ['infowindow_content'] = 'DeviceCode:<b>' . $device_code . '</b><br>Name : ' . '<b>' . $driver_name . '</b><br>' . 'Mobile : ' . '<b>' . $driver_mobile . '</b>';
				$marker ['draggable'] = TRUE;
				$marker ['animation'] = 'DROP';
				if ($current_latitude > 0 && $current_longitude > 0) {
					$this->googlemap->add_marker ( $marker );
				}
			}
		}
		$data ['map'] = $this->googlemap->create_map ();
		$this->render ( 'taxidevice/locate_currentdevice', $data );
	}
	public function getDeviceList() {
		$q = $this->input->get_post ( 'term' );
		// $searchResult = $this->Taxi_Device_Details_Model->searchBy( 'id, deviceCode', 'deviceCode', $q );
		$searchResult = $this->Taxi_Device_Details_Model->getDeviceList ( $q );
		echo json_encode ( $searchResult );
	}
	public function checkTaxiDeviceMobile() {
		$data = array ();
	
		$data = $this->input->post ();
		$mobile = $data ['mobile'];
		$taxi_device_id = $data ['taxi_device_id'];
		$mobile_exists = NULL;
		$status = 0;
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['mobile_not_exists'];
		if ($mobile) {
			if($taxi_device_id>0)
			{
			$mobile_exists = $this->Taxi_Device_Details_Model->getOneByKeyValueArray ( array (
					'mobile' => $mobile,
					'id!='=>$taxi_device_id,
					'isDeleted'=>Status_Type_Enum::INACTIVE
			) );
			}
			else 
			{
				$mobile_exists = $this->Taxi_Device_Details_Model->getOneByKeyValueArray ( array (
						'mobile' => $mobile,
						'isDeleted'=>Status_Type_Enum::INACTIVE
				) );
			}
			if ($mobile_exists) {
				$status = 1;
				$msg = $msg_data ['mobile_exists'];
			}
		}
		$response = array (
				'msg' => $msg,
				'email' => $mobile,
				'status' => $status
		);
		echo json_encode ( $response );
	}
}