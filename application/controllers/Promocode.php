<?php
class Promocode extends My_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->helper ( 'form' );
		$this->load->model ( 'Taxi_Query_Model' );
		$this->load->model ( 'Promocode_Details_Model' );
		$this->load->model ( 'Zone_Details_Model' );
		$this->load->model ( 'Entity_Model' );
	}
	public function index() {
	}
	public function add() {
	if ($this->User_Access_Model->createModule ($this->session->userdata('role_type'),Module_Name_Enum::PROMOCODE) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		
        
		$this->addJs ( 'app/promocode.js' );
		
		$data = array (
				'mode' => EDIT_MODE 
		);
		$data ['activetab'] = "promocode";
		$data ['active_menu'] = 'promocode';
		$data ['zone_list'] = $this->Zone_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE 
		), 'name' );
		$data ['entity_list'] = $this->Entity_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE 
		), 'name' );
		$data ['payment_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'PAYMENT_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		$data ['promocode_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'PROMOCODE_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		$data ['promocode_model'] = $this->Promocode_Details_Model->getBlankModel ();
		
		$this->render ( 'promocode/add_edit_promocode', $data );
	}
	public function getDetailsById($promocode_id, $view_mode) {
	if ($view_mode==EDIT_MODE)
	{
	if ($this->User_Access_Model->writeModule ($this->session->userdata('role_type'),Module_Name_Enum::PROMOCODE) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
	}
	else
	{
		if ($this->User_Access_Model->readModule ($this->session->userdata('role_type'),Module_Name_Enum::PROMOCODE) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
	}  
		$this->addJs ( 'app/promocode.js' );
		// get API messages from message.php
		$msg_data = $this->config->item ( 'msg' );
		$message = '';
		$data = array (
				'mode' => $view_mode 
		);
		$data ['activetab'] = "promocode";
		$data ['active_menu'] = 'promocode';
		// to get country list
		$data ['zone_list'] = $this->Zone_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE 
		), 'name' );
		$data ['entity_list'] = $this->Entity_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE 
		), 'name' );
		$data ['payment_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'PAYMENT_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		$data ['promocode_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'PROMOCODE_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		
		if ($this->input->post ( 'id' )) {
			$promocode_id = $this->input->post ( 'id' );
		}
		
		if ($promocode_id > 0) {
			if ($view_mode == EDIT_MODE) {
				$locked = $this->Promocode_Details_Model->isLocked ( $promocode_id );
				if ($locked) {
					$locked_user = $this->Promocode_Details_Model->getLockedUser ( $promocode_id );
					$locked_date = $this->Promocode_Details_Model->getLockedDate ( $promocode_id );
					$locked_date = date ( 'jS, F Y G:ia', strtotime ( $locked_date ) );
					$message = $msg_data ['locked'];
					$message = str_replace ( '##USERNAME##', $locked_user, $message );
					$message .= $locked_date;
					
					$data ['mode'] = VIEW_MODE;
				} else {
					$is_locked = $this->Promocode_Details_Model->lock ( $promocode_id );
				}
			}
			$data ['promocode_model'] = $this->Promocode_Details_Model->getById ( $promocode_id );
			
		} else {
			$data ['promocode_model'] = $this->Promocode_Details_Model->getBlankModel ();
		}
		if ($this->input->post ( 'id' )) {
			
			$html = $this->load->view ( 'promocode/add_edit_promocode', $data );
			echo json_encode ( array (
					'html' => $html 
			) );
		} else {
			
			$this->render ( 'promocode/add_edit_promocode', $data );
		}
	}
	public function getPromocodelist() {
		if ($this->User_Access_Model->showPromoCode () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		
		$this->addJs ( 'app/promocode.js' );
		
		$data ['activetab'] = "promocode";
		$data ['active_menu'] = 'promocode';
		
		$this->render ( 'promocode/manage_promocode', $data );
	}
	public function ajax_list() {
		// echo"ajaxlist";exit;
		$sql_query = $this->Promocode_Details_Model->getPromocodeListQuery ();
		$promocode_list = $this->Promocode_Details_Model->getDataTable ( $sql_query );
		$data = array ();
		
		$no = $_POST ['start'];
		foreach ( $promocode_list as $list ) {
			$no ++;
			$row = array ();
			$row [] = '
		
				  <button class="btn btn-sm btn-success" id="viewpromocode-' . $list->promoCodeId . '" title="View" ><i class="fa fa-eye" aria-hidden="true"></i></button>
				  <button class="btn btn-sm btn-blue" id="editpromocode-' . $list->promoCodeId . '" title="Edit" ><i class="fa fa-pencil-square-o"></i></button>';
			// <button class="btn btn-sm btn-danger" id="viewpromocode-'.$list->promoCodeId.'" title="Delete" ><i class="fa fa-trash-o"></i></button>';
			
			$row [] = $list->promoCode;
			$row [] = $list->promoDiscountAmount;
			$row [] = $list->promoDiscountAmount2;
			$row [] = $list->promoStartDatetime;
			$row [] = $list->promoEndDatetime;
			$row [] = $list->promoCodeLimit;
			$row [] = $list->promoCodeUserLimit;
			
			$is_free_ride_class = ($list->isFirstFreeRide) ? 'class="btn btn-success"' : 'class="btn btn-danger"';
			$is_free_ride_name = ($list->isFirstFreeRide) ? 'Yes' : 'No';
			$row [] = '<button id="status-' . $list->promoCodeId . '" status="' . $list->isFirstFreeRide . '" ' . $is_free_ride_class . '><span>' . $is_free_ride_name . '</span></button>';
			$row [] = ($list->promoZoneId)?ucfirst ( $list->zone1Name ):'All Zones';
			$row [] = ($list->promoZoneId2)?ucfirst ( $list->zone2Name ):'NA';
			$row [] = ucfirst ( $list->entityName );
			$status_class = ($list->status) ? 'class="btn btn-success"' : 'class="btn btn-danger"';
			$status_name = ($list->status) ? 'Active' : 'Inactive';
			$row [] = '<button id="status-' . $list->promoCodeId . '" status="' . $list->status . '" ' . $status_class . '><span>' . $status_name . '</span></button>';
			// add html for action
			
			$data [] = $row;
		}
		
		$output = array (
				"draw" => $_POST ['draw'],
				"recordsTotal" => $this->Promocode_Details_Model->getDataTableAllCount ( $sql_query ),
				"recordsFiltered" => $this->Promocode_Details_Model->getDataTableFilterCount ( $sql_query ),
				"data" => $data 
		);
		// output to json format
		echo json_encode ( $output );
	}
 
 public function savePromocode()
    {
        $this->addJs('app/promocode.js');
        $data = array();
        $data = $this->input->post();

       // debug($data);
        $promocode_id = $data ['id'];
        $data ['activetab'] = "promocode";
        $data ['active_menu'] = 'promocode';
        $data ['promoCode'] = trim($data ['promoCode']);
        
        if (array_key_exists('zoneId', $data)){
           $data ['zoneId']=array_map(function($value) {return $value === "" ? 0: $value;}, $data ['zoneId']);
        }
        if (array_key_exists('isFirstFreeRide', $data)) {
            $data ['isFirstFreeRide'] = Status_Type_Enum::ACTIVE;
        } else {
            $data ['isFirstFreeRide'] = Status_Type_Enum::INACTIVE;
        }
        if (array_key_exists('isCentPercentDiscount', $data)) {
        	$data ['isCentPercentDiscount'] = Status_Type_Enum::ACTIVE;
        } else {
        	$data ['isCentPercentDiscount'] = Status_Type_Enum::INACTIVE;
        }
        if (array_key_exists('promocodeType', $data) && $data['promocodeType']==Promocode_Type_Enum::GENERIC) {
        	unset($data['zoneId1']);
        	unset($data['zoneId2']);
        }
        else if (array_key_exists('promocodeType', $data) && $data['promocodeType']==Promocode_Type_Enum::COMBINATION) {
        	$data ['zoneId'] = $data['zoneId1'];
        	unset($data['zoneId1']);
        }
        $msg_data = $this->config->item('msg');
        
        $msg = $msg_data ['failed'];
       
        if ($promocode_id > 0) {
        	if (is_array($data['zoneId']))
        	{
        		$db_results = array();
        	    $results = $this->Promocode_Details_Model->getByKeyValueArray(array('promoCode' => $data ['promoCode'], 'isDeleted' => Status_Type_Enum::INACTIVE, 'status' => Status_Type_Enum::ACTIVE));
	        	if (is_array($results) && count($results) > 0) {
	                // collecting only specfic fileds value for merging
	                foreach ($results as $result) {
	                    array_push($db_results, $result->zoneId);
	                }
	                
	            }
	            
	            if (is_array($db_results)) {
	            	 // Contents that needs to delete from DB for existing zone_id
	            	$delete_contents = array_diff($db_results, $data['zoneId']);
	            	
	            	if (is_array($delete_contents) && count($delete_contents) > 0) {
	            		foreach ($delete_contents as $value) {
	            				//$delete_data=$data;
	            				//$delete_data['zoneId']=$value;
	            				$delete_data['status']=Status_Type_Enum::INACTIVE;
	            				$delete_exists=NULL;
	            				$delete_exists=$this->Promocode_Details_Model->getOneByKeyValueArray(array('zoneId'=>$value,'promoCode'=>$data ['promoCode']));
	            				if ($delete_exists && $delete_exists->id==$promocode_id)
	            				{
	            					$promocode_update=$this->Promocode_Details_Model->update($delete_data,array('id'=>$delete_exists->id));
	            				}
	            		}
	            	}  
	            	
	            	// Contents that needs to add to DB for existing release_id
	            	$insert_contents = array_diff($data['zoneId'], $db_results);
	            	
	            	if (is_array($insert_contents) && count($insert_contents) > 0) {
	            		foreach ($insert_contents as $value) {
	            			
	            				$insert_data=$data;
	            				$insert_data['zoneId']=$value;
	            				$insert_exists=NULL;
	            				$insert_exists=$this->Promocode_Details_Model->getOneByKeyValueArray(array('zoneId'=>$value,'promoCode'=>$data ['promoCode']));
	            				if ($insert_exists)
	            				{
	            					$promocode_id=$insert_exists->id;
	            					$insert_data['status']=Status_Type_Enum::ACTIVE;
	            					$insert_data['isDeleted']=Status_Type_Enum::INACTIVE;
	            					$promocode_update=$this->Promocode_Details_Model->update($insert_data,array('id'=>$promocode_id));
	            				}
	            				else 
	            				{
	            					$promocode_insert_id=$this->Promocode_Details_Model->insert($insert_data);
	            				}
	            			
	            		}
	            	}
	            	
	            	// Contents that needs to update to DB for existing release_id
	            	
	            	$update_contents =array_intersect($db_results,$data['zoneId']);
	            	
	            	if (is_array($update_contents) && count($update_contents) > 0) {
	            		foreach ($update_contents as $value) {
	            			
	            				$update_data=$data;
	            				$update_data['zoneId']=$value;
	            				$update_data['status']=Status_Type_Enum::ACTIVE;
	            				$update_data['isDeleted']=Status_Type_Enum::INACTIVE;
	            				$promocode_update=$this->Promocode_Details_Model->update($update_data,array('zoneId'=>$value,'promoCode'=>$data ['promoCode']));
	            			
	            		}
	            	} 
	            }
        		
        	}
        	else 
        	{
            	$promocode_id= $this->Promocode_Details_Model->update($data,array('id'=>$promocode_id));
        	}
        	
            $msg = $msg_data ['updated'];
        } else {
        	if (is_array($data['zoneId']) && count($data['zoneId'])>0)
        	{
            foreach ($data['zoneId'] as $value) {
            	$insert_data = $data;
                $insert_data['zoneId'] = $value;
                $promocode_id = $this->Promocode_Details_Model->insert($insert_data);
            }
        	}
        	else 
        	{
        		$promocode_id = $this->Promocode_Details_Model->insert($data);
        	}
            $msg = $msg_data ['success'];
        }
        $response = array(
            'msg' => $msg,
            'promocode_id' => $promocode_id
        );

        echo json_encode($response);

    }
	public function changePromocodeStatus() {
		if ($this->User_Access_Model->fullAccessModule ($this->session->userdata('role_type'),Module_Name_Enum::PROMOCODE) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to change staus", '404' );
			return;
		}
		$data = array ();
		
		$this->addJs ( 'app/promocode.js' );
		$user_updated = - 1;
		$data = $this->input->post ();
		$user_id = $data ['id'];
		$user_status = $data ['status'];
		
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['status_change_failed'];
		if ($user_id) {
			$user_updated = $this->Promocode_Details_Model->update ( array (
					'status' => $user_status 
			), array (
					'id' => $user_id 
			) );
			$msg = $msg_data ['status_change_success'];
		}
		$response = array (
				'msg' => $msg,
				'status' => $user_updated 
		);
		echo json_encode ( $response );
	}
	public function checkPromocode() {
		$data = array ();
		
		$data = $this->input->post ();
		$promocode = trim($data ['promocode']);
		$promocode_exists = NULL;
		$status = 0;
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['promocode_not_exists'];
		if ($promocode) {
			$promocode_exists = $this->Promocode_Details_Model->getOneByKeyValueArray ( array (
					'promoCode' => $promocode 
			) );
			if ($promocode_exists) {
				$status = 1;
				$msg = $msg_data ['promocode_exists'];
			}
		}
		$response = array (
				'msg' => $msg,
				'promocode' => $promocode,
				'status' => $status 
		);
		echo json_encode ( $response );
	}
}