<?php
class TaxiOwner extends My_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->helper ( 'form' );
		$this->load->model ( 'Taxi_Query_Model' );
		$this->load->model ( 'Taxi_Owner_Details_Model' );
		$this->load->model ( 'Entity_Model' );
		$this->load->model ( 'Zone_Details_Model' );
		$this->load->model ( 'Data_Attributes_Model' );
	}
	public function index() {
	}
	public function add() {
		if ($this->User_Access_Model->createModule ($this->session->userdata('role_type'),Module_Name_Enum::TAXI_OWNER) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$this->addJs ( 'app/taxi-owner.js' );
		
		$data = array (
				'mode' => EDIT_MODE 
		);
		$data ['activetab'] = "taxiowner";
		$data ['active_menu'] = 'taxiowner';
		$data ['zone_list'] = $this->Zone_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE 
		), 'name' );
		$data ['entity_list'] = $this->Entity_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE
		), 'name' );
		
		$data ['taxiowner_model'] = $this->Taxi_Owner_Details_Model->getBlankModel ();
		$this->render ( 'taxiowner/add_edit_taxi_owner', $data );
	}
	
		
	public function getDetailsById($taxiowner_id, $view_mode) {
	if ($view_mode==EDIT_MODE)
	{
	if ($this->User_Access_Model->writeModule ($this->session->userdata('role_type'),Module_Name_Enum::TAXI_OWNER) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
	}
	else
	{
		if ($this->User_Access_Model->readModule ($this->session->userdata('role_type'),Module_Name_Enum::TAXI_OWNER) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
	} 
		$this->addJs ( 'app/taxi-owner.js' );
		// get API messages from message.php
		$msg_data = $this->config->item ( 'msg' );
		$message='';
		$data = array (
				'mode' => $view_mode 
		);
		$data ['activetab'] = "taxiowner";
		$data ['active_menu'] = 'taxiowner';
		
		$data ['zone_list'] = $this->Zone_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE 
		), 'name' );
		$data ['entity_list'] = $this->Entity_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE
		), 'name' );
		if ($this->input->post ( 'id' )) {
			$taxiowner_id = $this->input->post ( 'id' );
		}
		
		if ($taxiowner_id > 0) {
			if ($view_mode==EDIT_MODE)
			{
				$locked=$this->Taxi_Owner_Details_Model->isLocked($taxiowner_id);
				if ($locked)
				{
					$locked_user=$this->Taxi_Owner_Details_Model->getLockedUser($taxiowner_id);
					$locked_date=$this->Taxi_Owner_Details_Model->getLockedDate($taxiowner_id);
					$locked_date=date('jS, F Y G:ia',strtotime($locked_date));
					$message=$msg_data ['locked'];
					$message=str_replace('##USERNAME##', $locked_user, $message);
					$message.=$locked_date;
			
					$data['mode']=VIEW_MODE;
				}
				else
				{
					$is_locked=$this->Taxi_Owner_Details_Model->lock($taxiowner_id);
				}
			}
			$data ['taxiowner_model'] = $this->Taxi_Owner_Details_Model->getById ( $taxiowner_id );
		} else {
			$data ['taxiowner_model'] = $this->Taxi_Owner_Details_Model->getBlankModel ();
		}
		if ($this->input->post ( 'id' )) {
			
			$html = $this->load->view ( 'taxiowner/add_edit_taxi_owner', $data );
			echo json_encode ( array (
					'html' => $html 
			) );
		} else {
			$this->render ( 'taxiowner/add_edit_taxi_owner', $data );
		}
	}
	
	public function getTaxiOwnerList()
	{
		if ($this->User_Access_Model->showTaxiOwner () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
	
		$this->addJs ( 'app/taxi-owner.js' );
		$data ['activetab'] = "taxiowner";
		$data ['active_menu'] = 'taxiowner';
	
		$this->render ( 'taxiowner/manage_taxi_owner', $data );
	}
	
	public function ajax_list()
	{
		//echo"ajaxlist";exit;
		$sql_query=$this->Taxi_Owner_Details_Model->getTaxiOwnerListQuery();
		$taxi_owner_list = $this->Taxi_Owner_Details_Model->getDataTable($sql_query);
		//print_r($list);exit;
		$data = array();
		$no = $_POST['start'];
		foreach ($taxi_owner_list as $list) {
			$no++;
			$row = array();
			$row[] = '
		
				  <button class="btn btn-sm btn-success" id="viewtaxiowner-'.$list->taxiOwnerId.'" title="View" ><i class="fa fa-eye" aria-hidden="true"></i></button>
				  <button class="btn btn-sm btn-blue" id="edittaxiowner-'.$list->taxiOwnerId.'" title="Edit"><i class="fa fa-pencil-square-o"></i></button>';
			// <button class="btn btn-sm btn-danger" id="deletetaxiowner-'.$list->taxiId.'"  title="Delete"><i class="fa fa-trash-o"></i></button>
					
			$row[] = $list->taxiOwnerCode;
			$row[] = ucfirst($list->firstName);
			$row[] = ucfirst($list->lastName);
			$row[] = $list->email;
			$row[] = $list->mobile;
			$row[] = $list->address;
			
			$isdriver_class=($list->isDriver)?'class="btn btn-success"':'class="btn btn-danger"';
			$isdriver_name=($list->isDriver)?'Yes':'No';
			$row[] = '<button '.$isdriver_class.'><span>'.$isdriver_name.'</span></button>';
			
			$row[] = ucfirst($list->zoneName);
			$status_class=($list->status)?'class="btn btn-success"':'class="btn btn-danger"';
			$status_name=($list->status)?'Active':'Inactive';
			
			$row[] = '<button id="status-'.$list->taxiOwnerId.'" status="'.$list->status.'" '.$status_class.'><span>'.$status_name.'</span></button>';
			//add html for action
			
			$data[] = $row;
		}
	
		$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->Taxi_Owner_Details_Model->getDataTableAllCount($sql_query),
				"recordsFiltered" => $this->Taxi_Owner_Details_Model->getDataTableFilterCount($sql_query),
				"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	
	public function saveTaxiOwner() {
		$this->addJs ( 'app/taxi-owner.js' );
		$data = array ();
		$data = $this->input->post ();
		//print_r($data);exit;
		$taxi_owner_id = $data ['id'];
		$data['activetab']="admin";
		$data['active_menu'] = 'admin';
	
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['failed'];
		if ($taxi_owner_id > 0) {
			$this->Taxi_Owner_Details_Model->update ( $data, array (
					'id' => $taxi_owner_id
			) );
			$msg = $msg_data ['updated'];
	
		} else {
			
			$taxi_owner_id = $this->Taxi_Owner_Details_Model->insert ( $data );
			
			//update the taxi device Code
			$taxi_owner_code_id=($taxi_owner_id<=999999)?str_pad ( (string)$taxi_owner_id, 6, '0', STR_PAD_LEFT ):$taxi_owner_id;
			
			$taxi_owner_code = 'TAXIOWN' . $taxi_owner_code_id;
			$update_device_code=$this->Taxi_Owner_Details_Model->update(array('taxiOwnerCode' => $taxi_owner_code,),array('id'=>$taxi_owner_id));
				
			$msg = $msg_data ['success'];
		}
		$response = array (
				'msg' => $msg,
				'taxi_owner_id' => $taxi_owner_id
		);
		echo json_encode ( $response );
		// $this->render("passenger/add_edit_passenger", $data);
	
	}
	public function checkTaxiOwnerEmail() {
		$data = array ();
	
		$data = $this->input->post ();
		$email = $data ['email'];
		$taxi_owner_id = $data ['taxi_owner_id'];
		$email_exists = NULL;
		$status = 0;
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['email_not_exists'];
		if ($email) {
			if ($taxi_owner_id>0)
			{
			$email_exists = $this->Taxi_Owner_Details_Model->getOneByKeyValueArray ( array (
					'email' => $email,
					'id!='=>$taxi_owner_id,
					'isDeleted'=>Status_Type_Enum::INACTIVE
			) );
			}
			else 
			{
				$email_exists = $this->Taxi_Owner_Details_Model->getOneByKeyValueArray ( array (
						'email' => $email,
						'isDeleted'=>Status_Type_Enum::INACTIVE
				) );
			}
			if ($email_exists) {
				$status = 1;
				$msg = $msg_data ['email_exists'];
			}
		}
		$response = array (
				'msg' => $msg,
				'email' => $email,
				'status' => $status
		);
		echo json_encode ( $response );
	}
	public function checkTaxiOwnerMobile() {
		$data = array ();
	
		$data = $this->input->post ();
		$mobile = $data ['mobile'];
		$taxi_owner_id = $data ['taxi_owner_id'];
		$mobile_exists = NULL;
		$status = 0;
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['mobile_not_exists'];
		if ($mobile) {
			if ($taxi_owner_id>0)
			{
			$mobile_exists = $this->Taxi_Owner_Details_Model->getOneByKeyValueArray ( array (
					'mobile' => $mobile,
					'id!='=>$taxi_owner_id,
					'isDeleted'=>Status_Type_Enum::INACTIVE
			) );
			}
			else 
			{
				$mobile_exists = $this->Taxi_Owner_Details_Model->getOneByKeyValueArray ( array (
						'mobile' => $mobile,
						'isDeleted'=>Status_Type_Enum::INACTIVE
				) );
			}
			if ($mobile_exists) {
				$status = 1;
				$msg = $msg_data ['mobile_exists'];
			}
		}
		$response = array (
				'msg' => $msg,
				'mobile' => $mobile,
				'status' => $status
		);
		echo json_encode ( $response );
	}
	public function deleteTaxiOwner() {
		if ($this->User_Access_Model->deleteModule ($this->session->userdata('role_type'),Module_Name_Enum::TAXI_OWNER) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to delete", '404' );
			return;
		}
		$data = array ();
	
		$this->addJs ( 'app/taxi-owner.js' );
		$taxi_owner_updated=-1;
		$data = $this->input->post ();
		$taxi_owner_id = $data ['id'];
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['delete_failed'];
		if ($taxi_owner_id) {
			$taxi_updated = $this->Taxi_Owner_Details_Model->update ( array (
					'isDeleted' => Status_Type_Enum::ACTIVE
			), array (
					'id' => $taxi_owner_id
			) );
			$msg = $msg_data ['delete_success'];
		}
		$response = array (
				'msg' => $msg,
				'status' => $taxi_owner_updated
		);
		echo json_encode ( $response );
	}
	
}