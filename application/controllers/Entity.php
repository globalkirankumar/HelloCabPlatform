<?php
class Entity extends My_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->helper ( 'form' );
		$this->load->helper ( 'file_upload' );
		$this->load->model ( 'Taxi_Query_Model' );
		$this->load->model ( 'Entity_Model' );
		$this->load->model ( 'Zone_Details_Model' );
		//$this->load->model ( 'State_Model' );
		$this->load->model ( 'Country_Model' );
		$this->load->model ( 'Entity_Config_Model' );
	}
	public function index() {
		
	}
	
	public function add() {
		
		if ($this->User_Access_Model->createModule ($this->session->userdata('role_type'),Module_Name_Enum::ENTITY) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$this->addJs ( 'app/entity.js' );
		
		$data = array (
				'mode' => EDIT_MODE 
		);
		$data ['activetab'] = "entity";
		$data ['active_menu'] = 'entity';
		$data ['zone_list'] = $this->Zone_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE 
		), 'name' );
		$data ['country_list'] = $this->Country_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE 
		), 'name' );
		$data ['entity_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'ENTITY_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		$data ['payment_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'PAYMENT_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		
		$data ['entity_model'] = $this->Entity_Model->getBlankModel ();
		$data ['entity_config_model'] = $this->Entity_Config_Model->getBlankModel ();
		$this->render ( 'entity/add_edit_entity', $data );
	}
	
	public function saveEntity() {
		$this->addJs ( 'app/entity.js' );
		$data = array ();
		$data = $this->input->post ();
		//print_r($data);exit;
		$entity_id = $data ['id'];
		$data['activetab']="entity";
		$data['active_menu'] = 'entity';
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['failed'];
                $send_sms = isset($data ['sendSms']) ? $data ['sendSms'] : 0;
                $data['sendSms'] = $send_sms;
                $send_email     = isset($data ['sentEmail']) ? $data ['sentEmail'] : 0;
                $data['sentEmail'] = $send_email;
		if ($entity_id > 0) {
			$this->Entity_Model->update ( $data, array ( 'id' => $entity_id ) );
                        $this->Entity_Config_Model->update ( $data, array (
					'entityId' => $entity_id
			) );
                       
                        /** Logo Upload Section ***/
                        $file_names                  = array_keys ( $_FILES );
			
			$logo_img_selected           = $_FILES ['logo'] ['name'];
			$emaillogo_img_selected      = $_FILES ['emailLogo'] ['name'];
			$favicon_img_selected        = $_FILES ['favicon'] ['name'];
                        if (isset($_FILES ['logo'] ['name'])) {
				// in which name img should be uploaded
				$prefix = "LOGO-" . $entity_id;
				$upload_path =$this->config->item ( 'entity_content_path' );
				$file_upload_path = $upload_path . $entity_id;
				file_upload ( $file_names [0], $entity_id, $logo_img_selected, $prefix, $file_upload_path,'Entity_Model','id' );
			}
                        if (isset($_FILES ['emailLogo'] ['name'])) {
				// in which name img should be uploaded
				$prefix = "EMAIL-LOGO-" . $entity_id;
				$upload_path =$this->config->item ( 'entity_content_path' );
				$file_upload_path = $upload_path . $entity_id;
				file_upload ( $file_names [1], $entity_id, $logo_img_selected, $prefix, $file_upload_path,'Entity_Model','id' );
			}
                        if (isset($_FILES ['favicon'] ['name'])) {
				// in which name img should be uploaded
				$prefix = "Favicon-" . $entity_id;
				$upload_path =$this->config->item ( 'entity_content_path' );
				$file_upload_path = $upload_path . $entity_id;
				file_upload ( $file_names [2], $entity_id, $logo_img_selected, $prefix, $file_upload_path,'Entity_Model','id' );
			}
			$msg = $msg_data ['updated'];
	
		} else {
                    /** Logo Upload Section ***/
                        $file_names                  = array_keys ( $_FILES );
			
			$logo_img_selected           = $_FILES ['logo'] ['name'];
			$emaillogo_img_selected      = $_FILES ['emailLogo'] ['name'];
			$favicon_img_selected        = $_FILES ['favicon'] ['name'];
                        if (isset($_FILES ['logo'] ['name'])) {
				// in which name img should be uploaded
				$prefix = "LOGO-" . $entity_id;
				$upload_path =$this->config->item ( 'entity_content_path' );
				$file_upload_path = $upload_path . $entity_id;
				file_upload ( $file_names [0], $entity_id, $logo_img_selected, $prefix, $file_upload_path,'Entity_Model','id' );
			}
                        if (isset($_FILES ['emailLogo'] ['name'])) {
				// in which name img should be uploaded
				$prefix = "EMAIL-LOGO-" . $entity_id;
				$upload_path =$this->config->item ( 'entity_content_path' );
				$file_upload_path = $upload_path . $entity_id;
				file_upload ( $file_names [1], $entity_id, $logo_img_selected, $prefix, $file_upload_path,'Entity_Model','id' );
			}
                        if (isset($_FILES ['favicon'] ['name'])) {
				// in which name img should be uploaded
				$prefix = "Favicon-" . $entity_id;
				$upload_path =$this->config->item ( 'entity_content_path' );
				$file_upload_path = $upload_path . $entity_id;
				file_upload ( $file_names [2], $entity_id, $logo_img_selected, $prefix, $file_upload_path,'Entity_Model','id' );
			}
			$entity_id = $this->Entity_Model->insert ( $data );
			
			//update the Unique User Code
			$entity_code_id=($entity_id<=999999)?str_pad ( (string)$entity_id, 6, '0', STR_PAD_LEFT ):$entity_id;
				
			$entity_code = 'ENT' . $entity_code_id;
			$update_entity_code=$this->Entity_Model->update(array('entityCode' => $entity_code,),array('id'=>$entity_id));
				
			$data['entityId'] = $entity_id; 
			$entity_id = $this->Entity_Config_Model->insert ( $data );
			$msg = $msg_data ['success'];
				
		}
		$response = array (
				'msg' => $msg,
				'entity_id' => $entity_id
		);
		echo json_encode ( $response );
		// $this->render("passenger/add_edit_passenger", $data);
	
	}
	
	public function getDetailsById($entity_id, $view_mode) {
		if ($view_mode==EDIT_MODE)
		{
			if ($this->User_Access_Model->writeModule ($this->session->userdata('role_type'),Module_Name_Enum::ENTITY) === FALSE) {
				// redirect 404
				show_error ( "You do not have permission to view this page", '404' );
				return;
			}
		}
		else
		{
			if ($this->User_Access_Model->readModule ($this->session->userdata('role_type'),Module_Name_Enum::ENTITY) === FALSE) {
				// redirect 404
				show_error ( "You do not have permission to view this page", '404' );
				return;
			}
		}
		$this->addCss ( 'vendors/lightbox/css/imagelightbox.css' );
                $this->addJs ( 'vendors/lightbox/js/imagelightbox.js' );
		$this->addJs ( 'app/entity.js' );
		$this->addJs('app/entity/entity_pickuplocation.js');
		$this->addJs('app/entity/entity_droplocation.js');
		// get API messages from message.php
		$msg_data = $this->config->item ( 'msg' );
		$message='';
		$data = array (
				'mode' => $view_mode 
		);
		
		$data ['activetab'] = "entity";
		$data ['active_menu'] = 'entity';
		$data ['zone_list'] = $this->Zone_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE 
		), 'name' );
		$data ['country_list'] = $this->Country_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE 
		), 'name' );

		$data ['entity_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'ENTITY_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		$data ['payment_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'PAYMENT_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		/*$data ['state_list'] = $this->State_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE 
		), 'name' );*/
		
		if ($this->input->post ( 'id' )) {
			$entity_id = $this->input->post ( 'id' );
		}
		
		if ($entity_id > 0) {
			
			if ($view_mode==EDIT_MODE)
			{
				$locked=$this->Entity_Model->isLocked($entity_id);
				if ($locked)
				{
					$locked_user=$this->Entity_Model->getLockedUser($entity_id);
					$locked_date=$this->Entity_Model->getLockedDate($entity_id);
					$locked_date=date('jS, F Y G:ia',strtotime($locked_date));
					$message=$msg_data ['locked'];
					$message=str_replace('##USERNAME##', $locked_user, $message);
					$message.=$locked_date;
			
					$data['mode']=VIEW_MODE;
				}
				else
				{
					$is_locked=$this->Entity_Model->lock($entity_id);
				}
			}
			
			$data ['entity_model'] = $this->Entity_Model->getById ( $entity_id );
                        $data ['entity_config_model'] = $this->Entity_Config_Model->getOneByKeyValueArray ( array (
					'entityId' => $data ['entity_model']->id 
			) );
                        
		} else {
			$data ['entity_model'] = $this->Entity_Model->getBlankModel ();
		}
		if ($this->input->post ( 'id' )) {
			
			$html = $this->load->view ( 'entity/add_edit_entity', $data );
			echo json_encode ( array (
					'html' => $html 
			) );
		} else {
			$this->render ( 'entity/add_edit_entity', $data );
		}
	}
	
	
	public function getEntityList()
	{
		$data = array ();
		$this->addCss ( 'vendors/lightbox/css/imagelightbox.css' );
                $this->addJs ( 'vendors/lightbox/js/imagelightbox.js' );
		$this->addJs ( 'app/entity.js' );
		
		$data ['activetab'] = "entity";
		$data ['active_menu'] = 'entity';
		
		$this->render ( 'entity/manage_entity', $data );
	}
	public function ajax_list()
	{
		//echo"ajaxlist";exit;
		$sql_query=$this->Entity_Model->getEntityListQuery();
		$entity_list = $this->Entity_Model->getDataTable($sql_query);
		//print_r($list);exit;
		$data = array();
		$no = $_POST['start'];
		foreach ($entity_list as $list) {
			$no++;
			$row = array();
			$row[] = '
		
				  <button class="btn btn-sm btn-success" id="viewEntity-'.$list->entityId.'" title="View" ><i class="fa fa-eye" aria-hidden="true"></i></button>
				  <button class="btn btn-sm btn-blue" id="editEntity-'.$list->entityId.'"  title="Edit" ><i class="fa fa-pencil-square-o"></i></button>
                  <button class="btn btn-sm btn-danger" id="deletEntity-'.$list->entityId.'" title="Delete" ><i class="fa fa-trash-o"></i></button>';
				
			$row[] = $list->entityCode;
			$row[] = $list->name;
			$row[] = $list->description;
			$row[] = $list->metaKeyword;
			$row[] = $list->tagLine;
			$row[] = $list->email;
			$row[] = $list->phone;
			$row[] = $list->websiteUrl;
			$row[] = $list->address;
			$row[] = $list->emergencyEmail;
			$row[] = $list->emergencyPhone;
			$row[] = $list->entityTypeName;
			$row[] = $list->copyRights;
			$row[] = $list->tollFreeNo;
			$row[] = $list->tax;
			$row[] = $list->adminCommission;
			$row[] = $list->passengerReferralDiscount;
			$row[] = $list->driverReferralDiscount;
			$row[] = $list->passengerRejectionCharge;
			$row[] = $list->driverMinWallet;
			$row[] = $list->driverMaxWallet;
			$row[] = $list->driverSecurityAmount;
			$row[] = $list->drtPromoAmount;
			$row[] = $list->drtPromoDaysLimit;
			$row[] = $list->drtFirstLimit;
			$row[] = $list->drtFirstlLimitCharge;
			$row[] = $list->drtSecondLimit;
			
			$row[] = $list->adminBookingCharge;
			$row[] = $list->sendSms;
			$row[] = $list->sendEmail;
			$row[] = $list->passengerAndroidAppUrl;
			$row[] = $list->passengerIphoneAppUrl;
			$row[] = $list->passengerAndroidAppVer;
			$row[] = $list->passengerIphoneAppVer;
			$row[] = $list->driverAndroidAppUrl;
			$row[] = $list->driverIphoneAppUrl;
			$row[] = $list->driverAndroidAppVer;
			$row[] = $list->driverIphoneAppVer;
			$row[] = $list->zoneName;
			$row[] = $list->countryName;
			//$row[] = $list->status;
                        $status_class=($list->status)?'class="rbutton btn-success"':'class="rbutton btn-danger"';
			$status_name=($list->status)?'Active':'Inactive';
			$row[] = '<button id="status-'.$list->entityId.'" status="'.$list->status.'" '.$status_class.'><span>'.$status_name.'</span></button>';
			

			//add html for action
			
			$data[] = $row;
		}
	
		$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->Entity_Model->getDataTableAllCount($sql_query),
				"recordsFiltered" => $this->Entity_Model->getDataTableFilterCount($sql_query),
				"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
        
        
        
      public function changeEntityStatus() {
      	if ($this->User_Access_Model->fullAccessModule ($this->session->userdata('role_type'),Module_Name_Enum::ENTITY) === FALSE) {
      		// redirect 404
      		show_error ( "You do not have permission to change staus", '404' );
      		return;
      	}
		$data = array ();
	
		$this->addJs ( 'app/entity.js' );
		$user_updated=-1;
		$data = $this->input->post ();
		$user_id = $data ['id'];
		$user_status = $data ['status'];
	
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['status_change_failed'];
		if ($user_id) {
			$user_updated = $this->Entity_Model->update ( array (
					'status' => $user_status
			), array (
					'id' => $user_id
			) );
			$msg = $msg_data ['status_change_success'];
		}
		$response = array (
				'msg' => $msg,
				'status' => $user_updated
		);
		echo json_encode ( $response );
	} 

	public function checkEntityPrefix() {
		$data = array ();
	
		$data = $this->input->post ();
		$prefix = $data ['entityPrefix'];
		$prefix_exists = NULL;
		$status = 0;
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['prefix_not_exists'];
		if ($prefix) {
			
				$prefix_exists = $this->Entity_Model->getOneByKeyValueArray ( array ( 'entityPrefix' => $prefix) );
			
			if ($prefix_exists) {
				$status = 1;
				$msg = $msg_data ['prefix_exists'];
			}
		}
		
		$response = array (
				'msg' => $msg,
				'prefix' => $prefix,
				'status' => $status
		);
		echo json_encode ( $response );
	}
        
}// end of class