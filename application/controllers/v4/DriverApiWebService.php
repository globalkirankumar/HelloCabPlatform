<?php
require_once (APPPATH . 'libraries/Rest_Controller.php');
class DriverApiWebService extends Rest_Controller {
	function __construct() {
		parent::__construct ();
		
		$this->load->model ( 'Auth_Key_Details_Model' );
		$this->load->model ( 'v1/Driver_Api_Webservice_Model' );
		$this->load->model ( 'v1/Common_Api_Webservice_Model' );
		$this->load->model ( 'Menu_Content_Details_Model' );
		$this->load->model ( 'Passenger_Model' );
		$this->load->model ( 'Driver_Model' );
		$this->load->model ( 'Driver_Personal_Details_Model' );
		$this->load->model ( 'Driver_Transaction_Details_Model' );
		$this->load->model ( 'Driver_Dispatch_Details_Model' );
		$this->load->model ( 'Driver_Taxi_Mapping_Model' );
		$this->load->model ( 'Country_Model' );
		$this->load->model ( 'Data_Attributes_Model' );
		$this->load->model ( 'Sms_Template_Model' );
		$this->load->model ( 'Trip_Details_Model' );
		$this->load->model ( 'Trip_Transaction_Details_Model' );
		$this->load->model ( 'Notification_Model' );
		$this->load->model ( 'Rate_Card_Details_Model' );
		$this->load->model ( 'Rate_Card_Slab_Details_Model' );
		$this->load->model ( 'Trip_Temp_Tracking_Details_Model' );
		$this->load->model ( 'Trip_Tracked_Details_Model' );
		$this->load->model ( 'Emergency_Request_Details_Model' );
		$this->load->model ( 'User_Emergency_Contacts_Model' );
		$this->load->model ( 'Taxi_Device_Details_Model' );
		$this->load->model ( 'Taxi_Details_Model' );
		$this->load->model ( 'Taxi_Rejected_Trip_Details_Model' );
		$this->load->model ( 'Driver_Shift_History_Model' );
		$this->load->model ( 'Taxi_Request_Details_Model' );
		$this->load->model ( 'Promocode_Details_Model' );
		
		$this->load->model ( 'Referral_Details_Model' );
		
		$this->load->model ( 'Entity_Model' );
		$this->load->model ( 'Entity_Config_Model' );
		$this->load->model ( 'Passenger_Transaction_Details_Model' );
		
		$this->load->model ( 'User_Login_Details_Model' );
		$this->load->model ( 'User_Personal_Details_Model' );
	}
	public function get_post_data($validate = FALSE) {
		$data = file_get_contents ( 'php://input' );
		$post = json_decode ( $data );
		if (! $post) {
			$post = json_decode ( json_encode ( $_POST ), FALSE );
		}
		
		return $post;
	}
	public function login_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$driver_exists = NULL;
		$driver_exists_personal_details = NULL;
		$driver_update = NULL;
		$driver_id = NULL;
		$driver_shift_insert_id = NULL;
		$trip_details = array ();
		$response_data = array ();
		$trip_status = 0;
		
		$mobile = NULL;
		$password = NULL;
		$device_id = NULL;
		$device_type = NULL;
		$latitude = NULL;
		$longitude = NULL;
		$gcm_id = '';
		$app_version = 0;
		
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$mobile = $post_data->mobile;
		$password = $post_data->password;
		$device_type = $post_data->device_type;
		$device_id = $post_data->device_id;
		$latitude = $post_data->latitude;
		$longitude = $post_data->longitude;
		if (array_key_exists ( 'gcm_id', $post_data )) {
			$gcm_id = $post_data->gcm_id;
		}
		if (array_key_exists ( 'app_version', $post_data )) {
			$app_version = $post_data->app_version;
		}
		// actual functionality
		
		if ($mobile && $password) {
			if ($mobile) {
				if ($mobile && $password) {
					$driver_exists = $this->Driver_Model->getOneByKeyValueArray ( array (
							'mobile' => $mobile,
							'password' => hash ( "sha256", $password ),
							'status' => Status_Type_Enum::ACTIVE,
							'isVerified' => Status_Type_Enum::ACTIVE 
					) );
				}
				
				if ($driver_exists) {
					$driver_exists_personal_details = $this->Driver_Personal_Details_Model->getOneByKeyValueArray ( array (
							'driverId' => $driver_exists->id 
					) );
					$auth_data = array ();
					if ($driver_exists->loginStatus == Status_Type_Enum::INACTIVE) {
						
						// update the paggenger deviceId & device type by default is 'A'=Android, 'I'=Iphone, 'W'=Windows,'N'=No device
						$driver_update = $this->Driver_Model->update ( array (
								'deviceId' => $device_id,
								'lastLoggedIn' => getCurrentDateTime (),
								'loginStatus' => Status_Type_Enum::ACTIVE,
								'gcmId' => $gcm_id,
								'appVersion' => $app_version,
								'deviceType' => $device_type 
						), array (
								'id' => $driver_exists->id 
						) );
						if ($driver_update) {
							// insert driver shift history IN data
							$insert_data = array (
									'driverId' => $driver_exists->id,
									'inLatitude' => $latitude,
									'inLongitude' => $longitude,
									'availabilityStatus' => Taxi_Available_Status_Enum::BUSY,
									'shiftStatus' => Driver_Shift_Status_Enum::SHIFTIN 
							);
							$driver_shift_insert_id = $this->Driver_Shift_History_Model->insert ( $insert_data );
						}
						
						// Send response data with Auth details & passenger details if passenger activation staus is success
						if ($driver_shift_insert_id) {
							
							$key = random_string ( 'alnum', 16 );
							$auth_key = hash ( "sha256", $key );
							$auth_key_exists = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
									'userId' => $driver_exists->id,
									'userType' => User_Type_Enum::DRIVER 
							) );
							// If user already exists update auth authKey
							if ($auth_key_exists) {
								$this->Auth_Key_Details_Model->update ( array (
										'authKey' => $auth_key 
								), array (
										'userId' => $driver_exists->id,
										'userType' => User_Type_Enum::DRIVER 
								) );
							} else {
								$this->Auth_Key_Details_Model->insert ( array (
										'authKey' => $auth_key,
										'userId' => $driver_exists->id,
										'userType' => User_Type_Enum::DRIVER 
								) );
							}
							// Authentication details
							$auth_details = array (
									'auth_key' => $auth_key,
									'user_id' => $driver_exists->id,
									'user_type' => User_Type_Enum::DRIVER 
							);
							
							// @todo update driiver rejected count $ consecutive reject count of driver to driver dispath details table
							$driver_dispatch_exists = NULL;
							$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
									'driverId' => $driver_exists->id 
							) );
							if ($driver_dispatch_exists) {
								$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
										'driverLatitude' => $latitude,
										'driverLongitude' => $longitude,
										'availabilityStatus' => Taxi_Available_Status_Enum::BUSY 
								), array (
										'driverId' => $driver_exists->id 
								) );
							} else {
								$insert_dispatch_data = array (
										'driverId' => $driver_exists->id,
										'driverLatitude' => $latitude,
										'driverLongitude' => $longitude,
										'availabilityStatus' => Taxi_Available_Status_Enum::BUSY 
								);
								$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
							}
							// end driiver rejected count $ consecutive reject count of driver to driver dispath details table
							
							// To get driver taxi id
							$driver_taxi_id = $this->Driver_Taxi_Mapping_Model->getColumnByKeyValueArray ( 'taxiId', array (
									'driverId' => $driver_exists->id 
							) );
							// debug($driver_taxi_id);
							if ($driver_taxi_id) {
								// To get taxi details
								$taxi_details = $this->Taxi_Details_Model->getById ( $driver_taxi_id [0]->taxiId );
								// $taxi_status=($taxi_details->status)?"Active":"Inactive";
								$response_data ['driver_details'] = array (
										'taxi_id' => $taxi_details->id,
										'taxi_name' => $taxi_details->name,
										'taxi_model' => $taxi_details->model,
										'taxi_manufacturer' => $taxi_details->manufacturer,
										'taxi_colour' => $taxi_details->colour,
										'taxi_status' => ($taxi_details->status) ? "Active" : "Inactive",
										'taxi_registration_no' => $taxi_details->registrationNo 
								);
								$response_data ['driver_details'] ['driver_id'] = $driver_exists->id;
								// $response_data ['taxi_details'] = $taxi_response_data;
								$response_data ['driver_details'] ['full_name'] = $driver_exists->firstName . ' ' . $driver_exists->lastName;
								$response_data ['driver_details'] ['email'] = $driver_exists->email;
								$response_data ['driver_details'] ['mobile'] = $driver_exists->mobile;
								$response_data ['driver_details'] ['driver_referral_code'] = $driver_exists->driverCode;
								$response_data ['driver_details'] ['driver_license_no'] = $driver_exists->licenseNo;
								$response_data ['driver_details'] ['driver_latitude'] = $driver_exists->currentLatitude;
								$response_data ['driver_details'] ['driver_longitude'] = $driver_exists->currentLongitude;
								$response_data ['driver_details'] ['driver_doj'] = $driver_exists->createdDatetime;
								$response_data ['driver_details'] ['driver_dob'] = $driver_exists_personal_details->dob;
								$response_data ['driver_details'] ['driver_address'] = $driver_exists_personal_details->address;
								
								$response_data ['driver_details'] ['profile_image'] = ($driver_exists->profileImage) ? driver_content_url ( $driver_exists->id . '/' . $driver_exists->profileImage ) : image_url ( 'app/user.png' );
								$response_data ['auth_details'] = $auth_details;
								$message = array (
										"message" => $msg_data ['login_success'],
										"details" => array (
												$response_data 
										),
										"status" => 1 
								);
							} else {
								$message = array (
										"message" => $msg_data ['taxi_allocate_failed'],
										"status" => - 1 
								);
							}
						}
					} else {
						$message = array (
								"message" => $msg_data ['driver_login_status'],
								"status" => - 1 
						);
					}
				} else {
					
					$message = array (
							"message" => $msg_data ['driver_account_exists'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['login_mandatory'],
						"status" => - 1 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['mandatory_data'],
					"status" => - 1 
			);
		}
		
		echo json_encode ( $message );
	}
	public function logout_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$update_driver_logout = NULL;
		$check_trip_avilablity = NULL;
		
		$response_data = array ();
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$driver_id = NULL;
		$longitude = NULL;
		$latitude = NULL;
		$app_version = 0;
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$driver_id = $post_data->driver_id;
		$longitude = $post_data->longitude;
		$latitude = $post_data->latitude;
		if (array_key_exists ( 'app_version', $post_data )) {
			$app_version = $post_data->app_version;
		}
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				
				// actual functionality
				if ($driver_id) {
					// check trip avilable for driver/any in progress trip before logout
					$check_trip_avilablity = $this->Taxi_Request_Details_Model->getOneByKeyValueArray ( array (
							'selectedDriverId' => $driver_id,
							'taxiRequestStatus!=' => Taxi_Request_Status_Enum::DRIVER_REJECTED,
							'taxiRequestStatus!=' => Taxi_Request_Status_Enum::PASSENGER_CANCELLED 
					) );
					if (! $check_trip_avilablity) {
						// update the driver logout data
						$update_data = array (
								'signupFrom' => Signup_Type_Enum::NONE,
								'loginStatus' => Status_Type_Enum::INACTIVE,
								'deviceId' => '',
								'gcmId' => '',
								'appVersion' => $app_version,
								'deviceType' => Device_Type_Enum::NO_DEVICE 
						);
						$update_driver_logout = $this->Driver_Model->update ( $update_data, array (
								'id' => $driver_id 
						) );
						
						if ($update_driver_logout) {
							// to get last shift in status of driver before updating the shift out status
							$last_driver_shift_id = $this->Driver_Shift_History_Model->getOneByKeyValueArray ( array (
									'driverId' => $driver_id 
							), 'id DESC' );
							// insert driver shift history IN data
							// debug_exit($last_driver_shift_id);
							$update_data = array (
									'outLatitude' => $latitude,
									'outLongitude' => $longitude,
									'availabilityStatus' => Taxi_Available_Status_Enum::BUSY,
									'shiftStatus' => Driver_Shift_Status_Enum::SHIFTOUT 
							);
							$driver_shift_insert_id = $this->Driver_Shift_History_Model->update ( $update_data, array (
									'id' => $last_driver_shift_id->id,
									'driverId' => $driver_id 
							) );
						}
						// update the passenger logout auth authKey=''
						$update_auth_key = $this->Auth_Key_Details_Model->update ( array (
								'authKey' => '' 
						), array (
								'userId' => $auth_user_id,
								'userType' => $auth_user_type 
						) );
						if ($update_auth_key) {
							
							// @todo update driiver rejected count $ consecutive reject count of driver to driver dispath details table
							$driver_dispatch_exists = NULL;
							$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
									'driverId' => $driver_id 
							) );
							if ($driver_dispatch_exists) {
								$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
										'driverLatitude' => $latitude,
										'driverLongitude' => $longitude,
										'availabilityStatus' => Taxi_Available_Status_Enum::BUSY 
								), array (
										'driverId' => $driver_id 
								) );
							} else {
								$insert_dispatch_data = array (
										'driverId' => $driver_id,
										'driverLatitude' => $latitude,
										'driverLongitude' => $longitude,
										'availabilityStatus' => Taxi_Available_Status_Enum::BUSY 
								);
								$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
							}
							// end driiver rejected count $ consecutive reject count of driver to driver dispath details table
							
							$message = array (
									"message" => $msg_data ['logout_success'],
									"status" => 1 
							);
						} else {
							$message = array (
									"message" => $msg_data ['logout_failed'],
									"status" => - 1 
							);
						}
					} else {
						if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::AVAILABLE_TRIP) {
							$message = array (
									"message" => $msg_data ['trip_assigned'],
									"status" => - 1 
							);
						} else if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::DRIVER_ACCEPTED) {
							$message = array (
									"message" => $msg_data ['trip_progress'],
									"status" => - 1 
							);
						}
					}
				} else {
					$message = array (
							"message" => $msg_data ['invalid_user'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		
		echo json_encode ( $message );
	}
	public function shiftUpdate_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$driver_exists = NULL;
		$check_trip_avilablity = NULL;
		
		$driver_shift_insert_id = NULL;
		$response_data = array ();
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$driver_id = NULL;
		$longitude = NULL;
		$latitude = NULL;
		$shift_status = NULL;
		$app_version = 0;
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$driver_id = $post_data->driver_id;
		$shift_status = $post_data->shift_status;
		$longitude = $post_data->longitude;
		$latitude = $post_data->latitude;
		if (array_key_exists ( 'app_version', $post_data )) {
			$app_version = $post_data->app_version;
		}
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual functionality
				if ($driver_id && $shift_status) {
					// to driver exists with activation ,login & veriication status before changing the shift status
					$driver_exists = $this->Driver_Model->getById ( $driver_id );
					if ($driver_exists) {
						// update the driver app version
						$update_driver_app_version = $this->Driver_Model->update ( array (
								'appVersion' => $app_version 
						), array (
								'id' => $driver_id 
						) );
						
						$entity_config_details = $this->Entity_Config_Model->getOneByKeyValueArray ( array (
								'entityId' => DEFAULT_COMPANY_ID 
						) );
						$driver_consective_reject = NULL;
						$driver_consective_reject = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
								'driverId' => $driver_id 
						) );
						if ($driver_consective_reject->consecutiveRejectCount != $entity_config_details->drtSecondLimit) {
							if ($driver_consective_reject->consecutiveRejectCount != $entity_config_details->drtFirstLimit) {
								if ($driver_exists->status == Status_Type_Enum::ACTIVE) {
									if ($driver_exists->loginStatus == Status_Type_Enum::ACTIVE) {
										// check trip avilable for driver/any in progress trip before logout
										$check_trip_avilablity = $this->Taxi_Request_Details_Model->getOneByKeyValueArray ( array (
												'selectedDriverId' => $driver_id,
												'taxiRequestStatus!=' => Taxi_Request_Status_Enum::DRIVER_REJECTED,
												'taxiRequestStatus!=' => Taxi_Request_Status_Enum::PASSENGER_CANCELLED 
										) );
										if (! $check_trip_avilablity) {
											if ($shift_status == Driver_Shift_Status_Enum::IN) {
												// to get last shift in status of driver before updating the shift out status
												$last_driver_shift = $this->Driver_Shift_History_Model->getOneByKeyValueArray ( array (
														'driverId' => $driver_id 
												), 'id DESC' );
												// insert driver shift history IN data
												// if ($last_driver_shift->shiftStatus == Driver_Shift_Status_Enum::SHIFTOUT) {
												$insert_data = array (
														'inLatitude' => $latitude,
														'inLongitude' => $longitude,
														'driverId' => $driver_id,
														'availabilityStatus' => Taxi_Available_Status_Enum::FREE,
														'shiftStatus' => Driver_Shift_Status_Enum::SHIFTIN 
												);
												$driver_shift_insert_id = $this->Driver_Shift_History_Model->insert ( $insert_data );
												if ($driver_shift_insert_id) {
													
													// @todo update driiver rejected count $ consecutive reject count of driver to driver dispath details table
													$driver_dispatch_exists = NULL;
													$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
															'driverId' => $driver_id 
													) );
													if ($driver_dispatch_exists) {
														$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
																'driverLatitude' => $latitude,
																'driverLongitude' => $longitude,
																'availabilityStatus' => Taxi_Available_Status_Enum::FREE 
														), array (
																'driverId' => $driver_id 
														) );
													} else {
														$insert_dispatch_data = array (
																'driverId' => $driver_id,
																'driverLatitude' => $latitude,
																'driverLongitude' => $longitude,
																'availabilityStatus' => Taxi_Available_Status_Enum::FREE 
														);
														$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
													}
													// end driiver rejected count $ consecutive reject count of driver to driver dispath details table
													
													$message = array (
															"message" => $msg_data ['driver_shift_in'],
															"status" => 1 
													);
												} else {
													$message = array (
															"message" => $msg_data ['failed'],
															"status" => - 1 
													);
												}
												/*
												 * } else {
												 * $message = array (
												 * "message" => $msg_data ['driver_shift_already_in'],
												 * "status" => - 1
												 * );
												 * }
												 */
											} else if ($shift_status == Driver_Shift_Status_Enum::OUT) {
												// to get last shift in status of driver before updating the shift out status
												$last_driver_shift = $this->Driver_Shift_History_Model->getOneByKeyValueArray ( array (
														'driverId' => $driver_id 
												), 'id DESC' );
												// insert driver shift history IN data
												// if ($last_driver_shift->shiftStatus == Driver_Shift_Status_Enum::SHIFTIN) {
												$update_data = array (
														'outLatitude' => $latitude,
														'outLongitude' => $longitude,
														'availabilityStatus' => Taxi_Available_Status_Enum::BUSY,
														'shiftStatus' => Driver_Shift_Status_Enum::SHIFTIN 
												);
												$driver_shift_insert_id = $this->Driver_Shift_History_Model->update ( $update_data, array (
														'id' => $last_driver_shift->id,
														'driverId' => $driver_id 
												) );
												if ($driver_shift_insert_id) {
													// @todo update driiver rejected count $ consecutive reject count of driver to driver dispath details table
													$driver_dispatch_exists = NULL;
													$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
															'driverId' => $driver_id 
													) );
													if ($driver_dispatch_exists) {
														$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
																'availabilityStatus' => Taxi_Available_Status_Enum::BUSY 
														), array (
																'driverId' => $driver_id 
														) );
													} else {
														$insert_dispatch_data = array (
																'driverId' => $driver_id,
																'availabilityStatus' => Taxi_Available_Status_Enum::BUSY 
														);
														$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
													}
													// end driiver rejected count $ consecutive reject count of driver to driver dispath details table
													$message = array (
															"message" => $msg_data ['driver_shift_out'],
															"driver_id" => $driver_id,
															"status" => 1 
													);
												} else {
													$message = array (
															"message" => $msg_data ['failed'],
															"status" => - 1 
													);
												}
												/*
												 * } else {
												 * $message = array (
												 * "message" => $msg_data ['driver_shift_already_out'],
												 * "status" => - 1
												 * );
												 * }
												 */
											}
										} else {
											if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::AVAILABLE_TRIP) {
												if ($check_trip_avilablity->tripId) {
													$trip_details = $this->Common_Api_Webservice_Model->getTripDetails ( $check_trip_avilablity->tripId );
													$response_data = array (
															
															// "message" => $msg_data ['driver_booking_request'],
															// "status" => 1,
															'trip_details' => array (
																	'trip_id' => $trip_details [0]->tripId,
																	'job_card_id' => $trip_details [0]->jobCardId,
																	'pickup_location' => $trip_details [0]->pickupLocation,
																	'pickup_latitude' => $trip_details [0]->pickupLatitude,
																	'pickup_longitude' => $trip_details [0]->pickupLongitude,
																	'pickup_time' => $trip_details [0]->actualPickupDatetime,
																	'drop_location' => $trip_details [0]->dropLocation,
																	'drop_latitude' => $trip_details [0]->dropLatitude,
																	'drop_longitude' => $trip_details [0]->dropLongitude,
																	'drop_time' => $trip_details [0]->dropDatetime,
																	'land_mark' => $trip_details [0]->landmark,
																	'taxi_rating' => $trip_details [0]->passengerRating,
																	'taxi_comments' => $trip_details [0]->passengerComments,
																	'trip_type' => $trip_details [0]->tripType,
																	'trip_type_name' => $trip_details [0]->tripTypeName,
																	'payment_mode' => $trip_details [0]->paymentMode,
																	'payment_mode_name' => $trip_details [0]->paymentModeName,
																	'trip_status' => $trip_details [0]->tripStatus,
																	'trip_status_name' => $trip_details [0]->tripStatusName,
																	'promocode' => $trip_details [0]->promoCode,
																	'is_driver_rated' => ($trip_details [0]->driverRating) ? 1 : 0,
																	'is_admin_booked' => ($trip_details [0]->bookedFrom == Signup_Type_Enum::BACKEND) ? 1 : 0 
															),
															'passenger_details' => array (
																	'passenger_id' => $trip_details [0]->passengerId,
																	'passenger_firstname' => $trip_details [0]->passengerFirstName,
																	'passenger_lastname' => $trip_details [0]->passengerLastName,
																	'passenger_mobile' => $trip_details [0]->passengerMobile,
																	'passenger_latitude' => ($trip_details [0]->bookedFrom == Signup_Type_Enum::BACKEND) ? $trip_details [0]->pickupLatitude : $trip_details [0]->passengerCurrentLatitude,
																	'passenger_longitude' => ($trip_details [0]->bookedFrom == Signup_Type_Enum::BACKEND) ? $trip_details [0]->pickupLongitude : $trip_details [0]->passengerCurrentLongitude,
																	'passenger_profile_image' => ($trip_details [0]->passengerProfileImage) ? (passenger_content_url ( $trip_details [0]->passengerId . '/' . $trip_details [0]->passengerProfileImage )) : image_url ( 'app/user.png' ) 
															),
															'driver_details' => array (
																	'driver_id' => $trip_details [0]->driverId,
																	'driver_firstname' => $trip_details [0]->driverFirstName,
																	'driver_lastname' => $trip_details [0]->driverLastName,
																	'driver_mobile' => $trip_details [0]->driverMobile,
																	'driver_profile_image' => ($trip_details [0]->driverProfileImage) ? (driver_content_url ( $trip_details [0]->driverId . '/' . $trip_details [0]->driverProfileImage )) : image_url ( 'app/user.png' ),
																	'driver_language' => $trip_details [0]->driverLanguagesKnown,
																	'driver_experience' => $trip_details [0]->driverExperience,
																	'driver_age' => $trip_details [0]->driverDob,
																	'driver_latitute' => $trip_details [0]->currentLatitude,
																	'driver_longtitute' => $trip_details [0]->currentLongitude,
																	'driver_status' => $trip_details [0]->availabilityStatus 
															),
															'taxi_details' => array (
																	'taxi_id' => $trip_details [0]->taxiId,
																	'taxi_name' => $trip_details [0]->taxiName,
																	'taxi_model' => $trip_details [0]->taxiModel,
																	'taxi_manufacturer' => $trip_details [0]->taxiManufacturer,
																	'taxi_registration_no' => $trip_details [0]->taxiRegistrationNo,
																	'taxi_colour' => $trip_details [0]->taxiColour 
															),
															
															"estimated_time" => $trip_details [0]->estimatedTime,
															"distance_away" => number_format ( $trip_details [0]->estimatedDistance, 1 ),
															"calculate_distance_away" => number_format ( $trip_details [0]->estimatedDistance + 0.5, 1 ),
															"notification_time" => 15 
													);
													$message = array (
															"message" => $msg_data ['driver_booking_request'],
															"trip_details" => $response_data,
															"status" => 5 
													);
													/*
													 * $message = array (
													 * "message" => $msg_data ['trip_assigned'],
													 * "status" => - 1
													 * );
													 */
												} else {
													$message = array (
															"message" => $msg_data ['invalid_trip'],
															"status" => - 1 
													);
												}
											} else if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::DRIVER_ACCEPTED) {
												if ($check_trip_avilablity->tripId) {
													$trip_details = $this->Common_Api_Webservice_Model->getTripDetails ( $check_trip_avilablity->tripId );
													
													$message = array (
															"message" => $msg_data ['trip_progress'],
															"status" => 4,
															'trip_details' => array (
																	'trip_id' => $trip_details [0]->tripId,
																	'job_card_id' => $trip_details [0]->jobCardId,
																	'customer_reference_code' => $trip_details [0]->customerReferenceCode,
																	'pickup_location' => $trip_details [0]->pickupLocation,
																	'pickup_latitude' => $trip_details [0]->pickupLatitude,
																	'pickup_longitude' => $trip_details [0]->pickupLongitude,
																	'pickup_time' => $trip_details [0]->actualPickupDatetime,
																	'drop_location' => $trip_details [0]->dropLocation,
																	'drop_latitude' => $trip_details [0]->dropLatitude,
																	'drop_longitude' => $trip_details [0]->dropLongitude,
																	'drop_time' => $trip_details [0]->dropDatetime,
																	'land_mark' => $trip_details [0]->landmark,
																	'taxi_rating' => $trip_details [0]->passengerRating,
																	'taxi_comments' => $trip_details [0]->passengerComments,
																	'trip_type' => $trip_details [0]->tripType,
																	'trip_type_name' => $trip_details [0]->tripTypeName,
																	'payment_mode' => $trip_details [0]->paymentMode,
																	'payment_mode_name' => $trip_details [0]->paymentModeName,
																	'trip_status' => $trip_details [0]->tripStatus,
																	'trip_status_name' => $trip_details [0]->tripStatusName,
																	'promocode' => $trip_details [0]->promoCode,
																	'is_driver_rated' => ($trip_details [0]->driverRating) ? 1 : 0,
																	'is_admin_booked' => ($trip_details [0]->bookedFrom == Signup_Type_Enum::BACKEND) ? 1 : 0 
															),
															'passenger_details' => array (
																	'passenger_id' => $trip_details [0]->passengerId,
																	'passenger_firstname' => $trip_details [0]->passengerFirstName,
																	'passenger_lastname' => $trip_details [0]->passengerLastName,
																	'passenger_mobile' => $trip_details [0]->passengerMobile,
																	'passenger_latitude' => ($trip_details [0]->bookedFrom == Signup_Type_Enum::BACKEND) ? $trip_details [0]->pickupLatitude : $trip_details [0]->passengerCurrentLatitude,
																	'passenger_longitude' => ($trip_details [0]->bookedFrom == Signup_Type_Enum::BACKEND) ? $trip_details [0]->pickupLongitude : $trip_details [0]->passengerCurrentLongitude,
																	'passenger_profile_image' => ($trip_details [0]->passengerProfileImage) ? (passenger_content_url ( $trip_details [0]->passengerId . '/' . $trip_details [0]->passengerProfileImage )) : image_url ( 'app/user.png' ) 
															),
															'driver_details' => array (
																	'driver_id' => $trip_details [0]->driverId,
																	'driver_firstname' => $trip_details [0]->driverFirstName,
																	'driver_lastname' => $trip_details [0]->driverLastName,
																	'driver_mobile' => $trip_details [0]->driverMobile,
																	'driver_profile_image' => ($trip_details [0]->driverProfileImage) ? (driver_content_url ( $trip_details [0]->driverId . '/' . $trip_details [0]->driverProfileImage )) : image_url ( 'app/user.png' ),
																	'driver_language' => $trip_details [0]->driverLanguagesKnown,
																	'driver_experience' => $trip_details [0]->driverExperience,
																	'driver_age' => $trip_details [0]->driverDob,
																	'driver_latitute' => $trip_details [0]->currentLatitude,
																	'driver_longtitute' => $trip_details [0]->currentLongitude,
																	'driver_status' => $trip_details [0]->availabilityStatus 
															),
															'taxi_details' => array (
																	'taxi_id' => $trip_details [0]->taxiId,
																	'taxi_name' => $trip_details [0]->taxiName,
																	'taxi_model' => $trip_details [0]->taxiModel,
																	'taxi_manufacturer' => $trip_details [0]->taxiManufacturer,
																	'taxi_registration_no' => $trip_details [0]->taxiRegistrationNo,
																	'taxi_colour' => $trip_details [0]->taxiColour 
															) 
													);
													
													/*
													 * $message = array (
													 * "message" => $msg_data ['trip_progress'],
													 * "status" => - 1
													 * );
													 */
												} else {
													$message = array (
															"message" => $msg_data ['invalid_trip'],
															"status" => - 1 
													);
												}
											}
										}
									} else {
										$message = array (
												"message" => $msg_data ['driver_login_failed'],
												"status" => - 1 
										);
									}
								} else {
									$message = array (
											"message" => $msg_data ['driver_account_exists'],
											"status" => - 1 
									);
								}
							} else {
								// to get last shift in status of driver before updating the shift out status
								$last_driver_shift = $this->Driver_Shift_History_Model->getOneByKeyValueArray ( array (
										'driverId' => $driver_id,
										'availabilityStatus' => Taxi_Available_Status_Enum::BUSY,
										'shiftStatus' => Driver_Shift_Status_Enum::SHIFTIN 
								), 'id DESC' );
								
								if ($last_driver_shift) {
									// insert driver shift history IN data
									$insert_data = array (
											'inLatitude' => $latitude,
											'inLongitude' => $longitude,
											'driverId' => $driver_id,
											'availabilityStatus' => Taxi_Available_Status_Enum::FREE,
											'shiftStatus' => Driver_Shift_Status_Enum::SHIFTIN 
									);
									$driver_shift_insert_id = $this->Driver_Shift_History_Model->insert ( $insert_data );
								}
								/*
								 * else
								 * {
								 * $update_data = array (
								 * 'inLatitude' => $latitude,
								 * 'inLongitude' => $longitude,
								 * 'driverId' => $driver_id,
								 * 'availabilityStatus' => Taxi_Available_Status_Enum::FREE,
								 * 'shiftStatus' => Driver_Shift_Status_Enum::SHIFTIN
								 * );
								 * $driver_shift_insert_id = $this->Driver_Shift_History_Model->update ( $update_data,array('id'=>$last_driver_shift->id));
								 * }
								 */
								// @todo update driiver rejected count $ consecutive reject count of driver to driver dispath details table
								$driver_dispatch_exists = NULL;
								$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
										'driverId' => $driver_id 
								) );
								if ($driver_dispatch_exists) {
									$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
											'driverLatitude' => $latitude,
											'driverLongitude' => $longitude,
											'availabilityStatus' => Taxi_Available_Status_Enum::FREE 
									), array (
											'driverId' => $driver_id 
									) );
								} else {
									$insert_dispatch_data = array (
											'driverId' => $driver_id,
											'driverLatitude' => $latitude,
											'driverLongitude' => $longitude,
											'availabilityStatus' => Taxi_Available_Status_Enum::FREE 
									);
									$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
								}
								// end driiver rejected count $ consecutive reject count of driver to driver dispath details table
								
								$message = array (
										"message" => $msg_data ['drt_first_limit_warning'],
										"status" => 2 
								);
							}
						} else {
							$message = array (
									"message" => $msg_data ['drt_second_limit_warning'],
									"status" => 3 
							);
						}
					} else {
						$message = array (
								"message" => $msg_data ['invalid_user'],
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['mandatory_data'],
							"status" => - 1,
							"details" => $msg_data ['mandatory_data'] 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		
		echo json_encode ( $message );
	}
	public function tripReject_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$driver_logged_status = NULL;
		$company_id = NULL;
		$reject_trip_id = NULL;
		$trip_status_update = NULL;
		$trip_request_status_update = NULL;
		$response_data = array ();
		$taxi_id = NULL;
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$driver_id = NULL;
		$reject_type = NULL;
		$trip_id = NULL;
		
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$driver_id = $post_data->driver_id;
		$reject_type = $post_data->reject_type;
		$trip_id = $post_data->trip_id;
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual functionality
				if ($trip_id && $driver_id) {
					// To get the passenger id from trip id to update the the trip rejected by driver
					$trip_details = $this->Trip_Details_Model->getById ( $trip_id );
					
					$driver_logged_status = $this->Driver_Model->getOneByKeyValueArray ( array (
							'id' => $driver_id,
							'status' => Status_Type_Enum::ACTIVE,
							'loginStatus' => Status_Type_Enum::ACTIVE 
					) );
					if ($driver_logged_status) {
						// To update trip status on tripdetails
						$trip_status_update = $this->Trip_Details_Model->update ( array (
								'tripStatus' => Trip_Status_Enum::CANCELLED_BY_DRIVER,
								'notificationStatus' => Trip_Status_Enum::CANCELLED_BY_DRIVER,
								'driverId' => 0,
								'taxiId' => 0,
								'driverAcceptedDatetime' => 0,
								'driverAcceptedStatus' => Driver_Accepted_Status_Enum::REJECTED 
						), array (
								'id' => $trip_id,
								'driverId' => $driver_id 
						) );
						
						if ($trip_status_update) {
							// To update trip status on driverrequesttripdetails
							// to get rejected drivers for particular trip_id
							$rejected_driver_list = NULL;
							$rejected_drivers = $this->Taxi_Request_Details_Model->getOneByKeyValueArray ( array (
									'tripId' => $trip_id 
							) );
							
							if ($rejected_drivers) {
								if ($rejected_drivers->rejectedDriverList) {
									$rejected_driver_list = $rejected_drivers->rejectedDriverList;
									$rejected_driver_list .= ',' . $driver_id;
								} else {
									$rejected_driver_list = $driver_id;
								}
							}
							
							$trip_request_status_update = $this->Taxi_Request_Details_Model->update ( array (
									'taxiRequestStatus' => Taxi_Request_Status_Enum::DRIVER_REJECTED,
									'selectedDriverId' => 0,
									'rejectedDriverList' => $rejected_driver_list 
							), array (
									'tripId' => $trip_id 
							) );
						}
						if ($trip_request_status_update) {
							// to get last shift in status of driver before updating the shift out status
							$last_driver_shift_id = $this->Driver_Shift_History_Model->getOneByKeyValueArray ( array (
									'driverId' => $driver_id 
							), 'id DESC' );
							$driver_shift_insert_id = $this->Driver_Shift_History_Model->update ( array (
									'availabilityStatus' => Taxi_Available_Status_Enum::FREE 
							), array (
									'id' => $last_driver_shift_id->id,
									'driverId' => $driver_id 
							) );
							
							// Insert/update data to driverrejectedtripdetails table
							
							$insert_data = array (
									'tripId' => $trip_id,
									'driverId' => $driver_id,
									'taxiId' => $trip_details->taxiId,
									'passengerId' => $trip_details->passengerId,
									'rejectionType' => ($reject_type == Status_Type_Enum::ACTIVE) ? Driver_Accepted_Status_Enum::REJECTED : Driver_Accepted_Status_Enum::TIMEOUT 
							);
							$exist_rejected_trip = $this->Taxi_Rejected_Trip_Details_Model->getOneByKeyValueArray ( array (
									'tripId' => $trip_id,
									'driverId' => $driver_id 
							) );
							
							if (! $exist_rejected_trip) {
								$reject_trip_id = $this->Taxi_Rejected_Trip_Details_Model->insert ( $insert_data );
							} else {
								$update_data = array (
										'rejectionType' => ($reject_type == Status_Type_Enum::ACTIVE) ? Driver_Accepted_Status_Enum::REJECTED : Driver_Accepted_Status_Enum::TIMEOUT 
								);
								$reject_trip_id = $this->Taxi_Rejected_Trip_Details_Model->update ( $update_data, array (
										'tripId' => $trip_id,
										'driverId' => $driver_id 
								) );
							}
							// @todo update driiver rejected count $ consecutive reject count of driver to driver dispath details table
							$driver_dispatch_exists = NULL;
							$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
									'driverId' => $driver_id 
							) );
							if ($driver_dispatch_exists) {
								$week_reject_count = NULL;
								$week_reject_count = $this->Driver_Api_Webservice_Model->getWeekRejectedTripCount ( $driver_id );
								if ($week_reject_count) {
									$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
											'weekRejectedTrip' => $week_reject_count [0]->weekRejectedTripCount,
											'totalRejectedTrip' => ($driver_dispatch_exists->totalRejectedTrip + 1),
											'consecutiveRejectCount' => ($driver_dispatch_exists->consecutiveRejectCount + 1),
											'availabilityStatus' => Taxi_Available_Status_Enum::FREE,
											'tripId' => Status_Type_Enum::INACTIVE 
									), array (
											'driverId' => $driver_id 
									) );
								} else {
									$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
											'weekRejectedTrip' => 1,
											'totalRejectedTrip' => ($driver_dispatch_exists->totalRejectedTrip + 1),
											'consecutiveRejectCount' => ($driver_dispatch_exists->consecutiveRejectCount + 1),
											'availabilityStatus' => Taxi_Available_Status_Enum::FREE,
											'tripId' => Status_Type_Enum::INACTIVE 
									), array (
											'driverId' => $driver_id 
									) );
								}
							} else {
								$insert_dispatch_data = array (
										'driverId' => $driver_id,
										'totalRejectedTrip' => 1,
										'weekRejectedTrip' => 1,
										'consecutiveRejectCount' => 1,
										'availabilityStatus' => Taxi_Available_Status_Enum::FREE,
										'tripId' => Status_Type_Enum::INACTIVE 
								);
								$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
							}
							// end driiver rejected count $ consecutive reject count of driver to driver dispath details table
							$driver_details = $this->Driver_Model->getById ( $driver_id );
							$response_data = array (
									'driver_details' => array (
											'driver_id' => $driver_details->id,
											'driver_referral_code' => $driver_details->driverCode,
											'driver_firstname' => $driver_details->firstName,
											'driver_lastname' => $driver_details->lastName,
											'driver_mobile' => $driver_details->mobile,
											'driver_profile_image' => ($driver_details->profileImage) ? (driver_content_url ( $driver_details->id . '/' . $driver_details->profileImage )) : image_url ( 'app/user.png' ) 
									) 
							);
							// to get new Taxi for trip when driver rejected
							$taxi_request_details = $this->getTaxiForTrip ( $trip_id );
							
							/*
							 * if ($taxi_request_details) {
							 * $driver_dispatched = $this->tripDispatchDriverNotification ( $taxi_request_details [0]->tripId, $taxi_request_details [0]->driverId, $taxi_request_details [0]->distance );
							 * }
							 */
							
							// check driver consecutive Reject Count
							$entity_config_details = $this->Entity_Config_Model->getOneByKeyValueArray ( array (
									'entityId' => $trip_details->entityId 
							) );
							$driver_consective_reject = NULL;
							$driver_consective_reject = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
									'driverId' => $driver_id 
							) );
							if ($driver_consective_reject->consecutiveRejectCount == $entity_config_details->drtFirstLimit) {
								$current_driver_wallet_amount = $this->Driver_Model->getById ( $driver_id );
								// update the driver transaction by debiting the DRT penalty
								$insert_driver_transaction = array (
										'driverId' => $driver_id,
										'tripId' => $trip_id,
										'transactionAmount' => $entity_config_details->drtFirstlLimitCharge,
										'previousAmount' => $current_driver_wallet_amount->driverWallet,
										'currentAmount' => ($current_driver_wallet_amount->driverWallet - $entity_config_details->drtFirstlLimitCharge),
										'transactionStatus' => 'Success',
										'transactionType' => Transaction_Type_Enum::TRIP_REJECT_PENALTY,
										'transactionFrom' => Transaction_From_Enum::WALLET_ACCOUNT,
										'transactionMode' => Transaction_Mode_Enum::DEBIT 
								);
								$update_driver_transaction = $this->Driver_Transaction_Details_Model->insert ( $insert_driver_transaction );
								$remaining_driver_wallet = $current_driver_wallet_amount->driverWallet - $entity_config_details->drtFirstlLimitCharge;
								$update_driver_wallet = $this->Driver_Model->update ( array (
										'driverWallet' => $remaining_driver_wallet 
								), array (
										'id' => $driver_id 
								) );
								// @todo update driver dispath details table
								$driver_dispatch_exists = NULL;
								$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
										'driverId' => $driver_id 
								) );
								
								if ($driver_dispatch_exists) {
									$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
											'walletBalance' => $remaining_driver_wallet 
									), array (
											'driverId' => $driver_id 
									) );
								} else {
									$insert_dispatch_data = array (
											'driverId' => $driver_id,
											'walletBalance' => $remaining_driver_wallet 
									);
									$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
								}
								
								// end driver dispath details table
							}
							if ($driver_consective_reject->consecutiveRejectCount >= $entity_config_details->drtSecondLimit) {
								$change_driver_status = $this->Driver_Model->update ( array (
										'status' => Status_Type_Enum::INACTIVE 
								), array (
										'id' => $driver_id 
								) );
								// @todo update driver dispath details table
								$driver_dispatch_exists = NULL;
								$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
										'driverId' => $driver_id 
								) );
								
								if ($driver_dispatch_exists) {
									$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
											'status' => Status_Type_Enum::INACTIVE 
									), array (
											'driverId' => $driver_id 
									) );
								} else {
									$insert_dispatch_data = array (
											'driverId' => $driver_id,
											'availabilityStatus' => Taxi_Available_Status_Enum::BUSY,
											'status' => Status_Type_Enum::INACTIVE 
									);
									$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
								}
								
								// end driver dispath details table
							}
							// end driiver rejected count $ consecutive reject count of driver to driver dispath details table
							
							$message = array (
									"message" => $msg_data ['driver_rejected'],
									"details" => array (
											$response_data 
									),
									
									'first_reject_limit' => $entity_config_details->drtFirstLimit,
									'second_reject_limit' => $entity_config_details->drtSecondLimit,
									'first_reject_penalty' => $entity_config_details->drtFirstlLimitCharge,
									'consecutive_reject_count' => $driver_consective_reject->consecutiveRejectCount,
									
									// "reject_reason_list"=>$reject_reason_list,
									"status" => 0 
							);
						} else {
							$message = array (
									"message" => $msg_data ['trip_status_failed'] . '' . $trip_id,
									"status" => - 1 
							);
							// log_message ( 'debug', 'Failed to update trip status & trip request status for trip_id=' . $trip_id );
						}
					} else {
						$message = array (
								"message" => $msg_data ['driver_login_failed'],
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['invalid_trip'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	public function rejectTripReason_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$driver_logged_status = NULL;
		
		$reject_trip_id = NULL;
		$response_data = array ();
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$driver_id = NULL;
		$reject_type = NULL;
		$trip_id = NULL;
		$reject_reason = NULL;
		
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$driver_id = $post_data->driver_id;
		$reject_type = $post_data->reject_type;
		$trip_id = $post_data->trip_id;
		$reject_reason = $post_data->reject_reason;
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual functionality
				if ($trip_id && $driver_id) {
					
					// To get the passenger id from trip id to update the the trip rejected by driver
					$trip_details = $this->Trip_Details_Model->getById ( $trip_id );
					
					$driver_logged_status = $this->Driver_Model->getOneByKeyValueArray ( array (
							'id' => $driver_id,
							'status' => Status_Type_Enum::ACTIVE,
							'loginStatus' => Status_Type_Enum::ACTIVE 
					) );
					if ($driver_logged_status) {
						
						// to get the driver rejected/cancelled trip
						/*
						 * $driver_rejected_count = $this->Taxi_Rejected_Trip_Details_Model->getColumnByKeyValueArray ( 'driverId', array (
						 * 'driverId' => $driver_logged_status->id,
						 * 'createdDatetime >=' => getCurrentDate (),
						 * 'createdDatetime <=' => getCurrentDate ()
						 * ) );
						 *
						 * $driver_earning_details = $this->Driver_Api_Webservice_Model->getDriverEarningDetails ( $driver_logged_status->id );
						 * $driver_today_earning_details = $this->Driver_Api_Webservice_Model->getDriverEarningDetails ( $driver_logged_status->id, 1 );
						 *
						 * // To calculate time driven on current date/today
						 * $total_amount = 0;
						 * $total_today_amount = 0;
						 * $time_result = '00:00';
						 * $actual_pickup_time = '';
						 * $drop_time = '';
						 * $hours = '';
						 * $minutes = '';
						 * $seconds = '';
						 * $date_difference = "";
						 * $total_differnce = "";
						 * foreach ( $driver_earning_details as $get_details ) {
						 * $actual_pickup_time = strtotime ( $get_details->actualPickupDatetime );
						 * $drop_time = strtotime ( $get_details->dropDatetime );
						 * // echo $actual_pickup_time;
						 * // echo '-';
						 * // echo $drop_time;
						 * // echo '<br>';
						 * $date_difference = abs ( $drop_time - $actual_pickup_time );
						 * $total_differnce += $date_difference;
						 * // to get total earned amount by the driver
						 * $total_amount += $get_details->driverEarning;
						 * }
						 * foreach ( $driver_today_earning_details as $get_details ) {
						 * // to get total earned amount by the driver
						 * $total_today_amount += $get_details->driverEarning;
						 * }
						 * // $date_difference = $drop_time - $actual_pickup_time;
						 * $hours += floor ( (($total_differnce % 604800) % 86400) / 3600 );
						 * $minutes += floor ( ((($total_differnce % 604800) % 86400) % 3600) / 60 );
						 * $seconds += floor ( (((($total_differnce % 604800) % 86400) % 3600) % 60) );
						 * $time_result = $minutes . ':' . $seconds;
						 * // To calculate time driven on current date/today
						 *
						 * $statistics = array (
						 * "drivername" => $driver_logged_status->firstName . ' ' . $driver_logged_status->lastName,
						 * "total_trip" => count ( $driver_earning_details ),
						 * "cancel_trips" => count ( $driver_rejected_count ),
						 * "total_earnings" => round ( $total_amount, 2 ),
						 * "overall_rejected_trips" => count ( $driver_rejected_count ),
						 * "today_earnings" => round ( $total_today_amount, 2 ),
						 * "shift_status" => Driver_Shift_Status_Enum::SHIFTIN,
						 * "time_driven" => $time_result,
						 * "status" => 1
						 * );
						 */
						
						$insert_data = array (
								'tripId' => $trip_id,
								'driverId' => $driver_id,
								'taxiId' => $trip_details->taxiId,
								'passengerId' => $trip_details->passengerId,
								'rejectionType' => ($reject_type == Status_Type_Enum::ACTIVE) ? Driver_Accepted_Status_Enum::REJECTED : Driver_Accepted_Status_Enum::TIMEOUT,
								'rejectionReason' => $reject_reason 
						);
						$exist_rejected_trip = $this->Taxi_Rejected_Trip_Details_Model->getOneByKeyValueArray ( array (
								'tripId' => $trip_id,
								'driverId' => $driver_id,
								'taxiId' => $trip_details->taxiId 
						) );
						if (! $exist_rejected_trip) {
							$statistics ['overall_rejected_trips'] += 1;
							$reject_trip_id = $this->Taxi_Rejected_Trip_Details_Model->insert ( $insert_data );
							if ($reject_trip_id) {
								
								$message = array (
										"message" => $msg_data ['driver_rejected_reason_success'],
										
										// "driver_statistics" => $statistics,
										"status" => 1 
								);
							} else {
								$message = array (
										"message" => $msg_data ['driver_rejected_reason_failed'],
										"status" => - 1 
								);
							}
						} else {
							$update_data = array (
									'rejectionType' => ($reject_type == Status_Type_Enum::ACTIVE) ? Driver_Accepted_Status_Enum::REJECTED : Driver_Accepted_Status_Enum::TIMEOUT,
									'rejectionReason' => $reject_reason 
							);
							$reject_trip_id = $this->Taxi_Rejected_Trip_Details_Model->update ( $update_data, array (
									'tripId' => $trip_id,
									'driverId' => $driver_id,
									'taxiId' => $trip_details->taxiId 
							) );
							$message = array (
									"message" => $msg_data ['driver_rejected_reason_success'],
									
									// "driver_statistics" => $statistics,
									"status" => 1 
							);
						}
					} else {
						$message = array (
								"message" => $msg_data ['driver_login_failed'],
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['invalid_trip'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	public function taxiArrived_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$trip_details = NULL;
		$check_trip_avilablity = NULL;
		
		$response_data = array ();
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$trip_id = NULL;
		
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$trip_id = $post_data->trip_id;
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual functionality
				if ($trip_id) {
					// To get trip details i.e., trip status,driver accepted status
					$trip_details = $this->Trip_Details_Model->getById ( $trip_id );
					if ($trip_details) {
						
						// check trip avilable for driver/any in progress trip before logout
						$check_trip_avilablity = $this->Taxi_Request_Details_Model->getOneByKeyValueArray ( array (
								'tripId' => $trip_id 
						) );
						
						if ($check_trip_avilablity) {
							if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::AVAILABLE_TRIP || $check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::DRIVER_ACCEPTED) {
								if ($trip_details->tripStatus == Trip_Status_Enum::IN_PROGRESS || $trip_details->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYMENT) {
									$message = array (
											"message" => $msg_data ['trip_progress'],
											"status" => - 1 
									);
								} else {
									$update_trip_status = $this->Trip_Details_Model->update ( array (
											'tripStatus' => Trip_Status_Enum::DRIVER_ARRIVED,
											'notificationStatus' => Trip_Status_Enum::DRIVER_ARRIVED,
											'driverAcceptedStatus' => Driver_Accepted_Status_Enum::ACCEPTED,
											'taxiArrivedDatetime' => getCurrentDateTime () 
									), array (
											'id' => $trip_id 
									) );
									
									if ($update_trip_status) {
										$update_request_status = $this->Taxi_Request_Details_Model->update ( array (
												'taxiRequestStatus' => Taxi_Request_Status_Enum::DRIVER_ACCEPTED 
										), array (
												'tripId' => $trip_id,
												'selectedDriverId' => $trip_details->driverId 
										) );
										if ($update_request_status) {
											
											if ($update_request_status) {
												
												// get passenger mobile number
												$passenger_details = $this->Passenger_Model->getById ( $trip_details->passengerId );
												// get driver full name & mobile number
												$driver_details = $this->Driver_Model->getById ( $trip_details->driverId );
												/*
												 * if (SMS && $update_request_status) {
												 * $message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
												 * 'title' => 'driver_arrived'
												 * ) );
												 *
												 * $message_data = $message_details->content;
												 * $message_data = str_replace ( "##DRIVERNAME##", $driver_details->firstName . ' ' . $driver_details->lastName, $message_data );
												 * $message_data = str_replace ( "##DRIVERMOBILE##", $driver_details->mobile, $message_data );
												 *
												 * // print_r($message);exit;
												 * if ($passenger_details) {
												 * if ($passenger_details->mobile) {
												 *
												 * $this->Common_Api_Webservice_Model->sendSMS ( $passenger_details->mobile, $message_data );
												 * }
												 * }
												 * }
												 *
												 *
												 * $from = SMTP_EMAIL_ID;
												 * $to = $passenger_details->email;
												 * $subject = 'Get Ready to ride with Zuver!';
												 * $data = array (
												 * 'user_name' => $user_exists->firstName . ' ' . $user_exists->lastName,
												 * 'mobile'=>$user_exists->mobile,
												 * 'email'=>$user_exists->email
												 * );
												 * $message_data = $this->load->view ( 'emailtemplate/passenger_register', $data, TRUE );
												 *
												 * if (SMTP && $check_otp) {
												 *
												 * if ($to) {
												 * $this->Common_Api_Webservice_Model->sendEmail ( $to, $subject, $message_data );
												 * }
												 * } else {
												 *
												 * // To send HTML mail, the Content-type header must be set
												 * $headers = 'MIME-Version: 1.0' . "\r\n";
												 * $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
												 * // Additional headers
												 * $headers .= 'From: Zuver<' . $from . '>' . "\r\n";
												 * $headers .= 'To: <' . $to . '>' . "\r\n";
												 * if (!filter_var($to, FILTER_VALIDATE_EMAIL) === false) {
												 * mail ( $to, $subject, $message_data, $headers );
												 * }
												 * }
												 */
												// @todo Send notification to Device through using GCM/FCM i.e., trip is started
												// To get driver taxi device gcm_id
												/*
												 * $driver_taxi_device_details = $this->Driver_Taxi_Mapping_Model->getByKeyValueArray ( array (
												 * 'driverId' => $trip_details->driverId
												 * ) );
												 * if ($driver_taxi_device_details) {
												 * // To get taxi device details
												 * $taxi_device_details = $this->Taxi_Device_Details_Model->getById ( $driver_taxi_device_details [0]->taxiDeviceId );
												 * if ($taxi_device_details && $taxi_device_details->gcmId) {
												 * $trip_start_notification = $this->Device_Api_Webservice_Model->tripStartNotification ( $taxi_device_details->gcmId, $trip_id );
												 * }
												 * }
												 */
												$trip_details = $this->Common_Api_Webservice_Model->getTripDetails ( $trip_id );
												// To get tax & admin commission entity details
												
												if ($trip_details > 0) {
													
													$response_data = array (
															'trip_details' => array (
																	'trip_id' => $trip_id,
																	'job_card_id' => $trip_details [0]->jobCardId,
																	'customer_reference_code' => $trip_details [0]->customerReferenceCode,
																	'pickup_location' => $trip_details [0]->pickupLocation,
																	'pickup_latitude' => $trip_details [0]->pickupLatitude,
																	'pickup_longitude' => $trip_details [0]->pickupLongitude,
																	'pickup_time' => $trip_details [0]->pickupDatetime,
																	'drop_location' => $trip_details [0]->dropLocation,
																	'drop_latitude' => $trip_details [0]->dropLatitude,
																	'drop_longitude' => $trip_details [0]->dropLongitude,
																	'drop_time' => $trip_details [0]->dropDatetime,
																	'land_mark' => $trip_details [0]->landmark,
																	'taxi_rating' => $trip_details [0]->passengerRating,
																	'taxi_comments' => $trip_details [0]->passengerComments,
																	'trip_type' => $trip_details [0]->tripType,
																	'trip_type_name' => $trip_details [0]->tripTypeName,
																	'payment_mode' => $trip_details [0]->paymentMode,
																	'payment_mode_name' => $trip_details [0]->paymentModeName,
																	'trip_status' => $trip_details [0]->tripStatus,
																	'trip_status_name' => $trip_details [0]->tripStatusName,
																	'promocode' => $trip_details [0]->promoCode,
																	'is_driver_rated' => ($trip_details [0]->driverRating) ? 1 : 0,
																	'is_admin_booked' => ($trip_details [0]->bookedFrom == Signup_Type_Enum::BACKEND) ? 1 : 0 
															),
															'passenger_details' => array (
																	'passenger_id' => $trip_details [0]->passengerId,
																	'passenger_firstname' => $trip_details [0]->passengerFirstName,
																	'passenger_lastname' => $trip_details [0]->passengerLastName,
																	'passenger_mobile' => $trip_details [0]->passengerMobile,
																	'passenger_latitude' => ($trip_details [0]->bookedFrom == Signup_Type_Enum::BACKEND) ? $trip_details [0]->pickupLatitude : $trip_details [0]->passengerCurrentLatitude,
																	'passenger_longitude' => ($trip_details [0]->bookedFrom == Signup_Type_Enum::BACKEND) ? $trip_details [0]->pickupLongitude : $trip_details [0]->passengerCurrentLongitude,
																	'passenger_profile_image' => ($trip_details [0]->passengerProfileImage) ? (passenger_content_url ( $trip_details [0]->passengerId . '/' . $trip_details [0]->passengerProfileImage )) : image_url ( 'app/user.png' ) 
															),
															'driver_details' => array (
																	'driver_id' => $trip_details [0]->driverId,
																	'driver_firstname' => $trip_details [0]->driverFirstName,
																	'driver_lastname' => $trip_details [0]->driverLastName,
																	'driver_mobile' => $trip_details [0]->driverMobile,
																	'driver_profile_image' => ($trip_details [0]->driverProfileImage) ? (driver_content_url ( $trip_details [0]->driverId . '/' . $trip_details [0]->driverProfileImage )) : image_url ( 'app/user.png' ) 
															),
															'taxi_details' => array (
																	'taxi_id' => $trip_details [0]->taxiId,
																	'taxi_name' => $trip_details [0]->taxiName,
																	'taxi_model' => $trip_details [0]->taxiModel,
																	'taxi_manufacturer' => $trip_details [0]->taxiManufacturer,
																	'taxi_registration_no' => $trip_details [0]->taxiRegistrationNo,
																	'taxi_colour' => $trip_details [0]->taxiColour 
															) 
													);
												}
												// send FCM/GCM notification to passenger taxi arrived
												$this->tripNotification ( $trip_id, $trip_details [0]->passengerId, User_Type_Enum::PASSENGER, $msg_data ['trip_driver_arrived'], 2 );
												
												$message = array (
														"message" => $msg_data ['driver_arrival'],
														"details" => array (
																$response_data 
														),
														"status" => 1 
												);
											} else {
												$message = array (
														"message" => $msg_data ['failed'],
														"status" => - 1 
												);
												log_message ( 'debug', $msg_data ['trip_status_failed'] . '' . $trip_details->driverId );
											}
										} else {
											$message = array (
													"message" => $msg_data ['trip_status_failed'] . '' . $trip_id,
													"status" => - 1 
											);
											log_message ( 'debug', $msg_data ['trip_status_failed'] . '' . $trip_id . ' driver_id=' . $trip_details->driverId );
										}
									} else {
										$message = array (
												"message" => $msg_data ['trip_status_failed'] . '' . $trip_id,
												"status" => - 1 
										);
										log_message ( 'debug', $msg_data ['trip_status_failed'] . '' . $trip_id );
									}
								}
							} else {
								
								if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::PASSENGER_CANCELLED) {
									$message = array (
											"message" => $msg_data ['trip_reject_passenger'],
											"status" => - 1 
									);
								} else if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::COMPLETED_TRIP) {
									$message = array (
											"message" => $msg_data ['trip_completed'],
											"status" => - 1 
									);
								}
							}
						} else {
							$message = array (
									"message" => $msg_data ['invalid_trip_request'],
									"status" => - 1 
							);
						}
					} else {
						$message = array (
								"message" => $msg_data ['invalid_trip'],
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['invalid_trip'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else 

		{
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	public function tripStart_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$driver_logged_status = NULL;
		$check_trip_avilablity = NULL;
		$trip_status_update = NULL;
		$trip_request_status_update = NULL;
		$trip_tracking_update = NULL;
		
		$response_data = array ();
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$driver_id = NULL;
		$trip_id = NULL;
		$latitude = NULL;
		$longitude = NULL;
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$driver_id = $post_data->driver_id;
		$trip_id = $post_data->trip_id;
		$latitude = $post_data->latitude;
		$longitude = $post_data->longitude;
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual functionality
				if ($driver_id) {
					$driver_logged_status = $this->Driver_Model->getOneByKeyValueArray ( array (
							'id' => $driver_id,
							'status' => Status_Type_Enum::ACTIVE,
							'loginStatus' => Status_Type_Enum::ACTIVE 
					) );
					if ($driver_logged_status) {
						// get the trip details
						$trip_details = $this->Trip_Details_Model->getById ( $trip_id );
						if ($trip_details) {
							// check trip avilable for driver/any in progress trip before logout
							$check_trip_avilablity = $this->Taxi_Request_Details_Model->getOneByKeyValueArray ( array (
									'tripId' => $trip_id 
							) );
							if ($check_trip_avilablity) {
								if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::DRIVER_ACCEPTED && $trip_details->tripStatus == Trip_Status_Enum::DRIVER_ARRIVED) {
									// getting valid & avilable promocode if trip is first ride for passenger & if no promocode applied by passenger
									$promocode = '';
									if (! $trip_details->promoCode) {
										$promocode = $this->Promocode_Details_Model->getFreeRidePromoCode ( $trip_details->passengerId );
									}
									if ($promocode) {
										// To update trip status on tripdetails with free ride promocode
										$trip_status_update = $this->Trip_Details_Model->update ( array (
												'tripStatus' => Trip_Status_Enum::IN_PROGRESS,
												'notificationStatus' => Trip_Status_Enum::IN_PROGRESS,
												'actualPickupDatetime' => getCurrentDateTime (),
												'promoCode' => ($promocode) ? $promocode : '' 
										), array (
												'id' => $trip_id,
												'driverId' => $driver_id 
										) );
									} else {
										// To update trip status on tripdetails without promocode
										$trip_status_update = $this->Trip_Details_Model->update ( array (
												'tripStatus' => Trip_Status_Enum::IN_PROGRESS,
												'notificationStatus' => Trip_Status_Enum::IN_PROGRESS,
												'actualPickupDatetime' => getCurrentDateTime () 
										), array (
												'id' => $trip_id,
												'driverId' => $driver_id 
										) );
									}
									
									if ($trip_status_update) {
										// To update trip status on taxirequesttripdetails
										$trip_request_status_update = $this->Taxi_Request_Details_Model->update ( array (
												'taxiRequestStatus' => Taxi_Request_Status_Enum::DRIVER_ACCEPTED 
										), array (
												'tripId' => $trip_id,
												'selectedDriverId' => $driver_id 
										) );
									}
									if ($trip_request_status_update) {
										// inserting the start trip location of driver into temp tracking table for particular trip
										
										$insert_data = array (
												'tripId' => $trip_id,
												'latitude' => $latitude,
												'longitude' => $longitude 
										);
										$trip_tracking_update = $this->Trip_Temp_Tracking_Details_Model->insert ( $insert_data );
										
										$trip_details = $this->Common_Api_Webservice_Model->getTripDetails ( $trip_id );
										$response_data = array (
												'trip_details' => array (
														'trip_id' => $trip_id,
														'job_card_id' => $trip_details [0]->jobCardId,
														'customer_reference_code' => $trip_details [0]->customerReferenceCode,
														'pickup_location' => $trip_details [0]->pickupLocation,
														'pickup_latitude' => $trip_details [0]->pickupLatitude,
														'pickup_longitude' => $trip_details [0]->pickupLongitude,
														'pickup_time' => $trip_details [0]->pickupDatetime,
														'drop_location' => $trip_details [0]->dropLocation,
														'drop_latitude' => $trip_details [0]->dropLatitude,
														'drop_longitude' => $trip_details [0]->dropLongitude,
														'drop_time' => $trip_details [0]->dropDatetime,
														'land_mark' => $trip_details [0]->landmark,
														'taxi_rating' => $trip_details [0]->passengerRating,
														'taxi_comments' => $trip_details [0]->passengerComments,
														'trip_type' => $trip_details [0]->tripType,
														'trip_type_name' => $trip_details [0]->tripTypeName,
														'payment_mode' => $trip_details [0]->paymentMode,
														'payment_mode_name' => $trip_details [0]->paymentModeName,
														'trip_status' => $trip_details [0]->tripStatus,
														'trip_status_name' => $trip_details [0]->tripStatusName,
														'promocode' => $trip_details [0]->promoCode,
														'is_driver_rated' => ($trip_details [0]->driverRating) ? 1 : 0,
														'is_admin_booked' => ($trip_details [0]->bookedFrom == Signup_Type_Enum::BACKEND) ? 1 : 0 
												),
												'passenger_details' => array (
														'passenger_id' => $trip_details [0]->passengerId,
														'passenger_firstname' => $trip_details [0]->passengerFirstName,
														'passenger_lastname' => $trip_details [0]->passengerLastName,
														'passenger_mobile' => $trip_details [0]->passengerMobile,
														'passenger_latitude' => ($trip_details [0]->bookedFrom == Signup_Type_Enum::BACKEND) ? $trip_details [0]->pickupLatitude : $trip_details [0]->passengerCurrentLatitude,
														'passenger_longitude' => ($trip_details [0]->bookedFrom == Signup_Type_Enum::BACKEND) ? $trip_details [0]->pickupLongitude : $trip_details [0]->passengerCurrentLongitude,
														'passenger_profile_image' => ($trip_details [0]->passengerProfileImage) ? (passenger_content_url ( $trip_details [0]->passengerId . '/' . $trip_details [0]->passengerProfileImage )) : image_url ( 'app/user.png' ) 
												),
												'taxi_details' => array (
														'taxi_id' => $trip_details [0]->taxiId,
														'taxi_name' => $trip_details [0]->taxiName,
														'taxi_model' => $trip_details [0]->taxiModel,
														'taxi_manufacturer' => $trip_details [0]->taxiManufacturer,
														'taxi_registration_no' => $trip_details [0]->taxiRegistrationNo,
														'taxi_colour' => $trip_details [0]->taxiColour 
												),
												'driver_details' => array (
														'driver_id' => $trip_details [0]->driverId,
														'driver_firstname' => $trip_details [0]->driverFirstName,
														'driver_lastname' => $trip_details [0]->driverLastName,
														'driver_mobile' => $trip_details [0]->driverMobile,
														'driver_profile_image' => ($trip_details [0]->driverProfileImage) ? (driver_content_url ( $trip_details [0]->driverId . '/' . $trip_details [0]->driverProfileImage )) : image_url ( 'app/user.png' ) 
												) 
										);
										// send FCM/GCM notification to passenger trip started
										$this->tripNotification ( $trip_id, $trip_details [0]->passengerId, User_Type_Enum::PASSENGER, $msg_data ['trip_progress'], 3 );
										$message = array (
												"message" => $msg_data ['driver_start_trip'],
												"status" => 1,
												"details" => array (
														$response_data 
												) 
										);
									} else {
										$message = array (
												"message" => $msg_data ['trip_status_failed'] . '' . $trip_id,
												"status" => - 1 
										);
										// log_message ( 'debug', 'Failed to update trip status & trip request status for trip_id=' . $trip_id );
									}
								} else if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::DRIVER_ACCEPTED && $trip_details->tripStatus == Trip_Status_Enum::IN_PROGRESS) {
									$message = array (
											"message" => $msg_data ['trip_started'],
											"status" => 2 
									);
								} 

								else {
									
									if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::PASSENGER_CANCELLED) {
										$message = array (
												"message" => $msg_data ['trip_reject_passenger'],
												"status" => - 1 
										);
									} else if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::COMPLETED_TRIP) {
										$message = array (
												"message" => $msg_data ['trip_completed'],
												"status" => - 1 
										);
									}
								}
							} else {
								$message = array (
										"message" => $msg_data ['invalid_trip_request'],
										"status" => - 1 
								);
							}
						} else {
							$message = array (
									"message" => $msg_data ['invalid_trip'],
									"status" => - 1 
							);
						}
					} else {
						$message = array (
								"message" => $msg_data ['driver_login_failed'],
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['invalid_user'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	public function tripAccept_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$driver_logged_status = NULL;
		$check_trip_avilablity = NULL;
		$trip_status_update = NULL;
		$trip_request_status_update = NULL;
		$trip_tracking_update = NULL;
		
		$rate_card_details = array ();
		$rate_card_distance_slab_details = array ();
		$rate_card_time_slab_details = array ();
		$rate_card_surge_slab_details = array ();
		$convenience_charge = 0;
		$surge_charge = 0;
		$surge_percentage = 0;
		$promo_discount_charge = 0;
		$promo_discount_percentage = 0;
		
		$response_data = array ();
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$driver_id = NULL;
		$trip_id = NULL;
		
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$driver_id = $post_data->driver_id;
		$trip_id = $post_data->trip_id;
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual functionality
				if ($driver_id) {
					$driver_logged_status = $this->Driver_Model->getOneByKeyValueArray ( array (
							'id' => $driver_id,
							'status' => Status_Type_Enum::ACTIVE,
							'loginStatus' => Status_Type_Enum::ACTIVE 
					) );
					if ($driver_logged_status) {
						// get the trip details
						$trip_details = $this->Trip_Details_Model->getById ( $trip_id );
						if ($trip_details) {
							// check trip avilable for driver/any in progress trip before logout
							$check_trip_avilablity = $this->Taxi_Request_Details_Model->getOneByKeyValueArray ( array (
									'tripId' => $trip_id 
							) );
							if ($check_trip_avilablity) {
								$trip_details = $this->Common_Api_Webservice_Model->getTripDetails ( $trip_id );
								
								$entity_config_details = $this->Entity_Config_Model->getOneByKeyValueArray ( array (
										'entityId' => $trip_details [0]->entityId 
								) );
								$promocode_details = $this->Common_Api_Webservice_Model->checkPromocodeAvailablity ( $trip_details [0]->promoCode, $trip_details [0]->passengerId, $trip_details [0]->bookingZoneId, $trip_details [0]->bookingDropZoneId, $trip_details [0]->entityId );
								if ($promocode_details && $trip_details [0]->promoCode) {
									if ($promocode_details [0]->promoDiscountType == Payment_Type_Enum::AMOUNT) {
										$promo_discount_charge = $promocode_details [0]->promoDiscountAmount;
									} else if ($promocode_details [0]->promoDiscountType == Payment_Type_Enum::PERCENTAGE) {
										$promo_discount_percentage = $promocode_details [0]->promoDiscountAmount;
									}
									if ($promocode_details [0]->promoDiscountType2 == Payment_Type_Enum::AMOUNT) {
										$promo_discount_charge = $promocode_details [0]->promoDiscountAmount2;
									} else if ($promocode_details [0]->promoDiscountType2 == Payment_Type_Enum::PERCENTAGE) {
										$promo_discount_percentage = $promocode_details [0]->promoDiscountAmount2;
									}
								}
								
								$rate_card_details = $this->Rate_Card_Details_Model->getOneByKeyValueArray ( array (
										'zoneId' => $trip_details [0]->bookingZoneId,
										'entityId' => $trip_details [0]->entityId,
										'taxiCategoryType' => $trip_details [0]->taxiCategoryType,
										'status' => Status_Type_Enum::ACTIVE,
										'isDeleted' => Status_Type_Enum::INACTIVE 
								) );
								if (! $rate_card_details) {
									$rate_card_details = $this->Rate_Card_Details_Model->getOneByKeyValueArray ( array (
											'zoneId' => Status_Type_Enum::INACTIVE,
											'entityId' => $trip_details [0]->entityId,
											'taxiCategoryType' => $trip_details [0]->taxiCategoryType,
											'status' => Status_Type_Enum::ACTIVE,
											'isDeleted' => Status_Type_Enum::INACTIVE 
									) );
								}
								if (! $rate_card_details) {
									$rate_card_details = $this->Rate_Card_Details_Model->getOneByKeyValueArray ( array (
											'zoneId' => $trip_details [0]->bookingZoneId,
											'entityId' => Status_Type_Enum::INACTIVE,
											'taxiCategoryType' => $trip_details [0]->taxiCategoryType,
											'status' => Status_Type_Enum::ACTIVE,
											'isDeleted' => Status_Type_Enum::INACTIVE 
									) );
								}
								if (! $rate_card_details) {
									$rate_card_details = $this->Rate_Card_Details_Model->getOneByKeyValueArray ( array (
											'zoneId' => Status_Type_Enum::INACTIVE,
											'entityId' => Status_Type_Enum::INACTIVE,
											'taxiCategoryType' => $trip_details [0]->taxiCategoryType,
											'status' => Status_Type_Enum::ACTIVE,
											'isDeleted' => Status_Type_Enum::INACTIVE 
									) );
								}
								if ($rate_card_details) {
									$actual_pickup_time = $trip_details [0]->pickupDatetime;
									if (TimeIsBetweenTwoTimes ( DAY_START_TIME, DAY_END_TIME, $actual_pickup_time )) {
										
										$convenience_charge += $rate_card_details->dayConvenienceCharge;
									} else if (TimeIsBetweenTwoTimes ( NIGHT_START_TIME, NIGHT_END_TIME, $actual_pickup_time )) {
										$convenience_charge += $rate_card_details->nightConvenienceCharge;
									}
									
									$rate_card_distance_slab_details = $this->Rate_Card_Slab_Details_Model->getByKeyValueArray ( array (
											'rateCardId' => $rate_card_details->id,
											'slabType' => Slab_Type_Enum::DISTANCE 
									) );
									$rate_card_time_slab_details = $this->Rate_Card_Slab_Details_Model->getByKeyValueArray ( array (
											'rateCardId' => $rate_card_details->id,
											'slabType' => Slab_Type_Enum::TIME 
									) );
									$rate_card_surge_slab_details = $this->Rate_Card_Slab_Details_Model->getByKeyValueArray ( array (
											'rateCardId' => $rate_card_details->id,
											'slabType' => Slab_Type_Enum::SURGE 
									) );
									if ($rate_card_surge_slab_details) {
										foreach ( $rate_card_surge_slab_details as $rate_surge ) {
											$slab_start_time = $rate_surge->slabStart;
											$slab_end_time = $rate_surge->slabEnd;
											// $user_st=$user_et="2017-08-02 09:29:12";
											if (((strtotime ( $actual_pickup_time ) >= strtotime ( $slab_start_time )) && (strtotime ( $actual_pickup_time ) <= strtotime ( $slab_end_time ))) || (strtotime ( $actual_pickup_time ) >= strtotime ( $slab_start_time )) && (strtotime ( $actual_pickup_time ) <= strtotime ( $slab_end_time ))) {
												if ($rate_surge->slabChargeType == Payment_Type_Enum::AMOUNT) {
													$surge_charge = $rate_surge->slabCharge;
													break;
												} else if ($rate_surge->slabChargeType == Payment_Type_Enum::PERCENTAGE) {
													$surge_percentage = $rate_surge->slabCharge;
													break;
												}
											}
										}
									}
								}
								
								$response_data = array (
										'trip_details' => array (
												'trip_id' => $trip_id,
												'job_card_id' => $trip_details [0]->jobCardId,
												'customer_reference_code' => $trip_details [0]->customerReferenceCode,
												'pickup_location' => $trip_details [0]->pickupLocation,
												'pickup_latitude' => $trip_details [0]->pickupLatitude,
												'pickup_longitude' => $trip_details [0]->pickupLongitude,
												'pickup_time' => $trip_details [0]->actualPickupDatetime,
												'drop_location' => $trip_details [0]->dropLocation,
												'drop_latitude' => $trip_details [0]->dropLatitude,
												'drop_longitude' => $trip_details [0]->dropLongitude,
												'trip_type' => $trip_details [0]->tripType,
												'trip_type_name' => $trip_details [0]->tripTypeName,
												'payment_mode' => $trip_details [0]->paymentMode,
												'payment_mode_name' => $trip_details [0]->paymentModeName,
												'trip_status' => $trip_details [0]->tripStatus,
												'trip_status_name' => $trip_details [0]->tripStatusName,
												'promocode' => $trip_details [0]->promoCode,
												'is_admin_booked' => ($trip_details [0]->bookedFrom == Signup_Type_Enum::BACKEND) ? 1 : 0 
										),
										'passenger_details' => array (
												'passenger_id' => $trip_details [0]->passengerId,
												'passenger_firstname' => $trip_details [0]->passengerFirstName,
												'passenger_lastname' => $trip_details [0]->passengerLastName,
												'passenger_mobile' => $trip_details [0]->passengerMobile,
												'passenger_latitude' => ($trip_details [0]->bookedFrom == Signup_Type_Enum::BACKEND) ? $trip_details [0]->pickupLatitude : $trip_details [0]->passengerCurrentLatitude,
												'passenger_longitude' => ($trip_details [0]->bookedFrom == Signup_Type_Enum::BACKEND) ? $trip_details [0]->pickupLongitude : $trip_details [0]->passengerCurrentLongitude,
												'passenger_profile_image' => ($trip_details [0]->passengerProfileImage) ? (passenger_content_url ( $trip_details [0]->passengerId . '/' . $trip_details [0]->passengerProfileImage )) : image_url ( 'app/user.png' ) 
										),
										'taxi_details' => array (
												'taxi_id' => $trip_details [0]->taxiId,
												'taxi_name' => $trip_details [0]->taxiName,
												'taxi_model' => $trip_details [0]->taxiModel,
												'taxi_manufacturer' => $trip_details [0]->taxiManufacturer,
												'taxi_registration_no' => $trip_details [0]->taxiRegistrationNo,
												'taxi_colour' => $trip_details [0]->taxiColour 
										),
										'driver_details' => array (
												'driver_id' => $trip_details [0]->driverId,
												'driver_firstname' => $trip_details [0]->driverFirstName,
												'driver_lastname' => $trip_details [0]->driverLastName,
												'driver_mobile' => $trip_details [0]->driverMobile,
												'driver_profile_image' => ($trip_details [0]->driverProfileImage) ? (driver_content_url ( $trip_details [0]->driverId . '/' . $trip_details [0]->driverProfileImage )) : image_url ( 'app/user.png' ) 
										),
										'fare_utility' => array (
												'zone_id' => $trip_details [0]->bookingZoneId,
												'entity_id' => $trip_details [0]->entityId,
												'booking_charge' => ($trip_details [0]->bookedFrom == Signup_Type_Enum::BACKEND) ? $entity_config_details->adminBookingCharge : 00.00,
												'penalty_charge' => ($trip_details [0]->penaltyTripCount) ? ($trip_details [0]->penaltyTripCount * $entity_config_details->passengerRejectionCharge) : 00.00,
												'admin_commission' => $entity_config_details->adminCommission,
												'tax_percentage' => $entity_config_details->tax,
												'convenience_charge' => $convenience_charge,
												'surge_charge' => $surge_charge,
												'surge_percentage' => $surge_percentage,
												'promo_discount_charge' => $promo_discount_charge,
												'promo_discount_percentage' => $promo_discount_percentage,
												'passenger_wallet' => ($trip_details [0]->paymentMode == Payment_Mode_Enum::WALLET) ? $trip_details [0]->walletAmount : 0.00,
												'current_date_time' => getCurrentDateTime () 
										),
										'rate_card_details' => $rate_card_details,
										'rate_card_distance_slab_details' => $rate_card_distance_slab_details,
										'rate_card_time_slab_details' => $rate_card_time_slab_details,
										'rate_card_surge_slab_details' => $rate_card_surge_slab_details 
								);
								if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::AVAILABLE_TRIP) {
									
									// To update trip status on tripdetails
									$trip_status_update = $this->Trip_Details_Model->update ( array (
											'tripStatus' => Trip_Status_Enum::DRIVER_ACCEPTED,
											'notificationStatus' => Trip_Status_Enum::DRIVER_ACCEPTED,
											'driverAcceptedStatus' => Driver_Accepted_Status_Enum::ACCEPTED,
											'driverAcceptedDatetime' => getCurrentDateTime () 
									), array (
											'id' => $trip_id,
											'driverId' => $driver_id 
									) );
									
									if ($trip_status_update) {
										// To update trip status on driverrequesttripdetails
										$trip_request_status_update = $this->Taxi_Request_Details_Model->update ( array (
												'taxiRequestStatus' => Taxi_Request_Status_Enum::DRIVER_ACCEPTED 
										), array (
												'tripId' => $trip_id,
												'selectedDriverId' => $driver_id 
										) );
										
										// to get last shift in status of driver before updating the shift out status
										$last_driver_shift_id = $this->Driver_Shift_History_Model->getOneByKeyValueArray ( array (
												'driverId' => $driver_id 
										), 'id DESC' );
										$driver_shift_insert_id = $this->Driver_Shift_History_Model->update ( array (
												'availabilityStatus' => Taxi_Available_Status_Enum::BUSY 
										), array (
												'id' => $last_driver_shift_id->id,
												'driverId' => $driver_id 
										) );
										
										// send FCM/GCM notification to passenger trip accepted by driver
										$this->tripNotification ( $trip_id, $trip_details [0]->passengerId, User_Type_Enum::PASSENGER, $msg_data ['trip_confirmed'], 1 );
										// @todo update driiver rejected count $ consecutive reject count of driver to driver dispath details table
										$driver_dispatch_exists = NULL;
										$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
												'driverId' => $driver_id 
										) );
										if ($driver_dispatch_exists) {
											$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
													'tripId' => $trip_id,
													'availabilityStatus' => Taxi_Available_Status_Enum::BUSY 
											), array (
													'driverId' => $driver_id 
											) );
										} else {
											$insert_dispatch_data = array (
													'driverId' => $driver_id,
													'tripId' => $trip_id,
													'availabilityStatus' => Taxi_Available_Status_Enum::BUSY 
											);
											$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
										}
										
										// end driiver rejected count $ consecutive reject count of driver to driver dispath details table
										$message = array (
												"message" => $msg_data ['driver_confirmed'],
												"status" => 1,
												"details" => array (
														$response_data 
												) 
										);
										$entity_config_details = $this->Entity_Config_Model->getOneByKeyValueArray ( array (
												'entityId' => $trip_details [0]->entityId 
										) );
										if ($entity_config_details) {
											if ($entity_config_details->sendSms) {
												// Trip accept SMS to passenger with driver details
												$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
														'title' => 'passenger_taxi_confirmed_sms' 
												) );
												
												$message_data = $message_details->content;
												
												$message_data = str_replace ( "##PASSENGERNAME##", $trip_details [0]->passengerFirstName . ' ' . $trip_details [0]->passengerLastName, $message_data );
												$message_data = str_replace ( "##JOBCARDID##", $trip_details [0]->jobCardId, $message_data );
												$message_data = str_replace ( "##DRIVERNAME##", $trip_details [0]->driverFirstName . ' ' . $trip_details [0]->driverLastName, $message_data );
												$message_data = str_replace ( "##DRIVERMOBILE##", $trip_details [0]->driverMobile, $message_data );
												$message_data = str_replace ( "##TAXINO##", $trip_details [0]->taxiRegistrationNo, $message_data );
												$message_data = str_replace ( "##CRN##", $trip_details [0]->customerReferenceCode, $message_data );
												
												if ($trip_details [0]->passengerMobile != "") {
													
													$this->Common_Api_Webservice_Model->sendSMS ( $trip_details [0]->passengerMobile, $message_data );
												}
												// Trip accept SMS to driver with passenger details
												$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
														'title' => 'driver_job_confirmed_sms' 
												) );
												
												$message_data = $message_details->content;
												
												$message_data = str_replace ( "##DRIVERNAME##", $trip_details [0]->driverFirstName . ' ' . $trip_details [0]->driverLastName, $message_data );
												$message_data = str_replace ( "##JOBCARDID##", $trip_details [0]->jobCardId, $message_data );
												$message_data = str_replace ( "##PASSENGERNAME##", $trip_details [0]->passengerFirstName . ' ' . $trip_details [0]->passengerLastName, $message_data );
												$message_data = str_replace ( "##PASSENGERMOBILE##", $trip_details [0]->passengerFirstName, $message_data );
												
												if ($trip_details [0]->driverMobile != "") {
													
													$this->Common_Api_Webservice_Model->sendSMS ( $trip_details [0]->driverMobile, $message_data );
												}
											}
										}
									} else {
										$message = array (
												"message" => $msg_data ['trip_status_failed'] . '' . $trip_id,
												"status" => - 1 
										);
										// log_message ( 'debug', 'Failed to update trip status & trip request status for trip_id=' . $trip_id );
									}
								} else {
									if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::PASSENGER_CANCELLED) {
										$message = array (
												"message" => $msg_data ['trip_reject_passenger'],
												"status" => - 1 
										);
									} else if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::COMPLETED_TRIP) {
										$message = array (
												"message" => $msg_data ['trip_completed'],
												"status" => - 1 
										);
									} else if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::DRIVER_ACCEPTED) {
										$message = array (
												"message" => $msg_data ['trip_accepted'],
												"status" => 1,
												"details" => array (
														$response_data 
												) 
										);
									}
								}
							} else {
								$message = array (
										"message" => $msg_data ['invalid_trip_request'],
										"status" => - 1 
								);
							}
						} else {
							$message = array (
									"message" => $msg_data ['invalid_trip'],
									"status" => - 1 
							);
						}
					} else {
						$message = array (
								"message" => $msg_data ['driver_login_failed'],
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['invalid_user'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	public function tripEnd_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$driver_logged_status = NULL;
		$check_trip_avilablity = NULL;
		$trip_status_update = NULL;
		$trip_request_status_update = NULL;
		$rate_card_details = NULL;
		$rate_card_distance_slab_details = NULL;
		$rate_card_time_slab_details = NULL;
		$rate_card_surge_slab_details = NULL;
		$response_data = array ();
		$trip_tracking_update = NULL;
		$wallet_transaction_id = NULL;
		$driver_id = NULL;
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$trip_id = NULL;
		$drop_latitude = NULL;
		$drop_longitude = NULL;
		$drop_location = NULL;
		$travelled_distance = NULL;
		$total_travelled_minute = NULL;
		
		// Posted json data
		$post_data = $this->get_post_data ();
		$trip_id = $post_data->trip_id;
		$drop_latitude = $post_data->drop_latitude;
		$drop_longitude = $post_data->drop_longitude;
		$drop_location = $post_data->drop_location;
		$travelled_distance = $post_data->travelled_distance;
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				
				// actual functionality
				if ($trip_id > 0) {
					// get the trip details
					$trip_details = $this->Trip_Details_Model->getById ( $trip_id );
					
					if ($trip_details) {
						$taxi_details = $this->Taxi_Details_Model->getById ( $trip_details->taxiId );
						// To get tax & admin commission entity details
						$entity_details = $this->Entity_Model->getOneByKeyValueArray ( array (
								'id' => $trip_details->entityId 
						) );
						
						$entity_config_details = $this->Entity_Config_Model->getOneByKeyValueArray ( array (
								'entityId' => $trip_details->entityId 
						) );
						
						if ($trip_details->driverId) {
							$driver_logged_status = $this->Driver_Model->getOneByKeyValueArray ( array (
									'id' => $trip_details->driverId,
									'status' => Status_Type_Enum::ACTIVE,
									'loginStatus' => Status_Type_Enum::ACTIVE 
							) );
							if ($driver_logged_status) {
								
								// get check wheather trip is allredy ended but waiting for payment
								if ($trip_details->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYMENT) {
									$message = array (
											"message" => $msg_data ['trip_waiting_payment'],
											"status" => 2 
									);
									echo json_encode ( $message );
									exit ();
								}
								// check trip avilable for driver/any in progress trip before logout
								$check_trip_avilablity = $this->Taxi_Request_Details_Model->getOneByKeyValueArray ( array (
										'tripId' => $trip_id 
								) );
								if ($check_trip_avilablity) {
									if ($check_trip_avilablity->taxiRequestStatus != Taxi_Request_Status_Enum::DRIVER_REJECTED && $check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::DRIVER_ACCEPTED) {
										// intialized for calculation purpose
										
										$actual_pickup_time = $trip_details->actualPickupDatetime;
										$actual_drop_time = getCurrentDateTime ();
										$travelled_hours = 0;
										$travelled_minutes = 0;
										$travelled_seconds = 0;
										$travel_hours_minutes = 0;
										$convenience_charge = 0;
										$travel_charge = 0;
										$total_trip_cost = 0;
										$promo_discount_amount = 0;
										$tax_amount = 0;
										$wallet_payment_amount = 0;
										$travelled_distance_charge = 0;
										$travelled_time_charge = 0;
										$admin_commission_amount = 0;
										$booking_charge = 0;
										$penalty_charge = 0;
										$surge_charge = 0;
										
										// get rate card details based on the cityId & trip type
										$rate_card_details = $this->Rate_Card_Details_Model->getOneByKeyValueArray ( array (
												'zoneId' => $trip_details->zoneId,
												'entityId' => $trip_details->entityId,
												'taxiCategoryType' => $taxi_details->taxiCategoryType,
												'status' => Status_Type_Enum::ACTIVE,
												'isDeleted' => Status_Type_Enum::INACTIVE 
										) );
										if (! $rate_card_details) {
											$rate_card_details = $this->Rate_Card_Details_Model->getOneByKeyValueArray ( array (
													'zoneId' => Status_Type_Enum::INACTIVE,
													'entityId' => $trip_details->entityId,
													'taxiCategoryType' => $trip_details->taxiCategoryType,
													'status' => Status_Type_Enum::ACTIVE,
													'isDeleted' => Status_Type_Enum::INACTIVE 
											) );
										}
										if (! $rate_card_details) {
											$rate_card_details = $this->Rate_Card_Details_Model->getOneByKeyValueArray ( array (
													'zoneId' => $trip_details->zoneId,
													'entityId' => Status_Type_Enum::INACTIVE,
													'taxiCategoryType' => $trip_details->taxiCategoryType,
													'status' => Status_Type_Enum::ACTIVE,
													'isDeleted' => Status_Type_Enum::INACTIVE 
											) );
										}
										if (! $rate_card_details) {
											$rate_card_details = $this->Rate_Card_Details_Model->getOneByKeyValueArray ( array (
													'zoneId' => Status_Type_Enum::INACTIVE,
													'entityId' => Status_Type_Enum::INACTIVE,
													'taxiCategoryType' => $trip_details->taxiCategoryType,
													'status' => Status_Type_Enum::ACTIVE,
													'isDeleted' => Status_Type_Enum::INACTIVE 
											) );
										}
										
										if (count ( $rate_card_details ) > 0) {
											if (strtotime ( $actual_pickup_time ) < strtotime ( $actual_drop_time )) {
												
												$dateDiff = intval ( (strtotime ( $actual_pickup_time ) - strtotime ( $actual_drop_time )) / 60 );
												$total_calculated_hours = round ( abs ( strtotime ( $actual_pickup_time ) - strtotime ( $actual_drop_time ) ) / 3600, 2 );
												$travelled_hours = intval ( abs ( $dateDiff / 60 ) );
												$travelled_minutes = intval ( abs ( $dateDiff % 60 ) );
												$travelled_seconds = intval ( abs ( (strtotime ( $actual_pickup_time ) - strtotime ( $actual_drop_time )) % 3600 ) % 60 );
												
												$travel_hours_minutes = $travelled_hours . '.' . $travelled_minutes;
												$total_travelled_minute = ($travelled_hours * 60) + $travelled_minutes;
												// to get convenience charge based pickup time day/night
												
												if (TimeIsBetweenTwoTimes ( DAY_START_TIME, DAY_END_TIME, $actual_pickup_time )) {
													
													$convenience_charge += $rate_card_details->dayConvenienceCharge;
												} else if (TimeIsBetweenTwoTimes ( NIGHT_START_TIME, NIGHT_END_TIME, $actual_pickup_time )) {
													$convenience_charge += $rate_card_details->nightConvenienceCharge;
												}
												
												// to calculate the distance slab trip journey amount
												$rate_card_distance_slab_details = $this->Rate_Card_Slab_Details_Model->getByKeyValueArray ( array (
														'rateCardId' => $rate_card_details->id,
														'slabType' => Slab_Type_Enum::DISTANCE 
												) );
												if ($travelled_distance) {
													$available_travelled_distance = $travelled_distance;
													$calculated_travelled_distance = 0;
													$calculate_to_distance = 0;
													foreach ( $rate_card_distance_slab_details as $rate_distance ) {
														if ($available_travelled_distance) {
															if ($rate_distance->slabEnd) {
																
																if (! $rate_distance->slabStart) {
																	$calculate_to_distance = $rate_distance->slabEnd;
																} else {
																	$calculate_to_distance = ($rate_distance->slabEnd - $rate_distance->slabStart);
																}
																$calculated_travelled_distance += $calculate_to_distance;
																// $available_travelled_distance -= $calculate_to_distance;
																if ($available_travelled_distance <= $calculate_to_distance) {
																	$calculate_to_distance = $available_travelled_distance;
																	$available_travelled_distance = 0;
																} else {
																	$available_travelled_distance -= $calculate_to_distance;
																}
															} else if ($rate_distance->slabStart && ! $rate_distance->slabEnd) {
																$calculate_to_distance = $available_travelled_distance;
																
																$calculated_travelled_distance += $calculate_to_distance;
																$available_travelled_distance -= $calculate_to_distance;
															}
															
															if ($calculate_to_distance) {
																if ($rate_distance->slabCharge) {
																	
																	$travelled_distance_charge += $calculate_to_distance * $rate_distance->slabCharge;
																	$calculate_to_distance = 0;
																}
															}
														}
													}
												}
												// /add to travel charge
												$travel_charge += $travelled_distance_charge;
												
												// to calculate the time slab trip journey amount
												$rate_card_time_slab_details = $this->Rate_Card_Slab_Details_Model->getByKeyValueArray ( array (
														'rateCardId' => $rate_card_details->id,
														'slabType' => Slab_Type_Enum::TIME 
												) );
												if ($total_travelled_minute) {
													
													$available_travelled_minute = $total_travelled_minute;
													$calculated_travelled_minute = 0;
													$calculate_to_minute = 0;
													foreach ( $rate_card_time_slab_details as $rate_time ) {
														if ($available_travelled_minute) {
															if ($rate_time->slabEnd) {
																
																if (! $rate_time->slabStart) {
																	$calculate_to_minute = $rate_time->slabEnd;
																} else {
																	$calculate_to_minute = ($rate_time->slabEnd - $rate_time->slabStart);
																}
																$calculated_travelled_minute += $calculate_to_minute;
																// $available_travelled_distance -= $calculate_to_distance;
																if ($available_travelled_minute <= $calculate_to_minute) {
																	$calculate_to_minute = $available_travelled_minute;
																	$available_travelled_minute = 0;
																} else {
																	$available_travelled_minute -= $calculate_to_minute;
																}
															} else if ($rate_time->slabStart && ! $rate_time->slabEnd) {
																$calculate_to_minute = $available_travelled_minute;
																
																$calculated_travelled_minute += $calculate_to_minute;
																$available_travelled_minute -= $calculate_to_minute;
															}
															
															if ($calculate_to_minute) {
																if ($rate_time->slabCharge) {
																	
																	$travelled_time_charge += $calculate_to_minute * $rate_time->slabCharge;
																	$calculate_to_minute = 0;
																}
															}
														}
													}
												}
												
												// /add to travel charge
												$travel_charge += $travelled_time_charge;
												
												// to calculate the surge slab trip journey amount
												$rate_card_surge_slab_details = $this->Rate_Card_Slab_Details_Model->getByKeyValueArray ( array (
														'rateCardId' => $rate_card_details->id,
														'slabType' => Slab_Type_Enum::SURGE 
												) );
												if ($rate_card_surge_slab_details) {
													foreach ( $rate_card_surge_slab_details as $rate_surge ) {
														$slab_start_time = $rate_surge->slabStart;
														$slab_end_time = $rate_surge->slabEnd;
														// $user_st=$user_et="2017-08-02 09:29:12";
														if (((strtotime ( $actual_pickup_time ) >= strtotime ( $slab_start_time )) && (strtotime ( $actual_pickup_time ) <= strtotime ( $slab_end_time ))) || (strtotime ( $actual_pickup_time ) >= strtotime ( $slab_start_time )) && (strtotime ( $actual_pickup_time ) <= strtotime ( $slab_end_time ))) {
															if ($rate_surge->slabChargeType == Payment_Type_Enum::AMOUNT) {
																$surge_charge = $rate_surge->slabCharge;
																$total_trip_cost += $surge_charge;
																break;
															} else if ($rate_surge->slabChargeType == Payment_Type_Enum::PERCENTAGE) {
																$surge_charge = ($rate_surge->slabCharge / 100) * ($convenience_charge + $travel_charge);
																$surge_charge = round ( $surge_charge, 2 );
																$total_trip_cost += $surge_charge;
																break;
															}
														}
													}
												}
												$travel_charge += $surge_charge;
												
												// tax calculation based on data of entity config details info table
												if ($total_trip_cost > 0) {
													$tax = $entity_config_details->tax;
													if ($tax > 0) {
														$tax_amount = ($tax / 100) * $total_trip_cost;
														$tax_amount = round ( $tax_amount, 2 );
														$total_trip_cost += $tax_amount;
													}
												}
												// calculation booking charge if booking from admib panel based on entity config details info table
												if ($trip_details->bookedFrom == Signup_Type_Enum::BACKEND) {
													$booking_charge = $entity_config_details->adminBookingCharge;
													$total_trip_cost += $booking_charge;
												}
												// get passenger existing trip reject count to calucalate penalty and reset the penalty reject trip count
												$passenger_details = $this->Passenger_Model->getById ( $trip_details->passengerId );
												if ($passenger_details->penaltyTripCount) {
													$penalty_charge = $passenger_details->penaltyTripCount * $entity_config_details->passengerRejectionCharge;
													$passenger_updated = $this->Passenger_Model->update ( array (
															'penaltyTripCount' => 0 
													), array (
															'id' => $trip_details->passengerId 
													) );
													$total_trip_cost += $penalty_charge;
													// set variable to NULL because it reused below
													$passenger_details = NULL;
													$passenger_updated = NULL;
												}
												// $total_trip_cost = $convenience_charge + $travel_charge;
												// calculate promocode discount if applicable
												if (! empty ( $trip_details->promoCode ) && $trip_details->promoCode != '') {
													
													// Check promocode exists or not for particular passenger at particular city
													$promocode_exists = $this->Common_Api_Webservice_Model->checkPromocodeAvailablity ( $trip_details->promoCode, $trip_details->passengerId, $trip_details->zoneId );
													
													if ($promocode_exists && count ( $promocode_exists ) > 0) {
														
														if (strtotime ( $promocode_exists [0]->promoStartDatetime ) < getCurrentUnixDateTime () && strtotime ( $promocode_exists [0]->promoEndDatetime ) > getCurrentUnixDateTime ()) {
															
															// check promocode limit exceeds or not for specific passenger (note: only completed trip)
															$promocode_user_limit_exists = $this->Trip_Details_Model->getByKeyValueArray ( array (
																	'passengerId' => $trip_details->passengerId,
																	'promoCode' => $trip_details->promoCode,
																	'tripStatus' => Trip_Status_Enum::TRIP_COMPLETED 
															) );
															// check promocode limit exceeds or not total limit (note: only completed trip)
															$promocode_limit_exists = $this->Trip_Details_Model->getByKeyValueArray ( array (
																	'promoCode' => $trip_details->promoCode,
																	'tripStatus' => Trip_Status_Enum::TRIP_COMPLETED 
															) );
															
															if ((count ( $promocode_limit_exists ) < $promocode_exists [0]->promoCodeLimit) && (count ( $promocode_user_limit_exists ) < $promocode_exists [0]->promoCodeUserLimit)) {
																// get promo code details
																$promocode_details = $this->Promocode_Details_Model->getOneByKeyValueArray ( array (
																		'promoCode' => $trip_details->promoCode 
																) );
																
																$promo_discount = $promocode_details->promoDiscountAmount;
																$promo_type = $promocode_details->promoDiscountType;
																if ($promo_type == Payment_Type_Enum::PERCENTAGE) {
																	// discount in %
																	$calculate_amt = ($promo_discount / 100) * $total_trip_cost;
																	$promo_discount_amount = round ( $calculate_amt, 2 );
																	$total_trip_cost = $total_trip_cost - $promo_discount_amount;
																	$promo_discount = $promo_discount_amount;
																} else if ($promo_type == Payment_Type_Enum::AMOUNT) {
																	// discount in Rs
																	if ($promo_discount > $total_trip_cost) {
																		
																		$promo_discount_amount = $total_trip_cost;
																		$promo_discount = $promo_discount_amount;
																	} else {
																		
																		$promo_discount_amount = $promo_discount;
																	}
																	$total_trip_cost = $total_trip_cost - $promo_discount;
																}
																if ($total_trip_cost < 0) {
																	$total_trip_cost = 0;
																}
															}
														}
													}
												}
												// deduction from wallet if payment mode is wallet
												if ($trip_details->paymentMode == Payment_Mode_Enum::WALLET) {
													
													if ($total_trip_cost > 0) {
														// get current wallet amount of passenger
														$current_wallet_amount = $this->Passenger_Model->getById ( $trip_details->passengerId );
														
														if ($total_trip_cost >= $current_wallet_amount->walletAmount) {
															$wallet_payment_amount = $current_wallet_amount->walletAmount;
															// $total_trip_cost = $total_trip_cost - $current_wallet_amount->walletAmount;
															$remaing_wallet_amount ['walletAmount'] = 0;
														} else {
															$actual_wallet_amount = $current_wallet_amount->walletAmount;
															
															$current_wallet_amount->walletAmount = $current_wallet_amount->walletAmount - $total_trip_cost;
															$wallet_payment_amount = $total_trip_cost;
															$remaing_wallet_amount ['walletAmount'] = $current_wallet_amount->walletAmount;
															// $total_trip_cost = 0;
														}
														$wallet_payment_update = $this->Passenger_Model->update ( $remaing_wallet_amount, array (
																'id' => $trip_details->passengerId 
														) );
													}
												}
												// get current wallet amount of driver to debit admin commisiion charge
												$current_driver_wallet_amount = $this->Driver_Model->getById ( $trip_details->driverId );
												
												// calculate admin commision
												$total_bill_amount = $convenience_charge + $travel_charge;
												$admin_commission_amount = ($total_bill_amount / 100) * $entity_config_details->adminCommission;
												$admin_commission_amount = round ( $admin_commission_amount, 2 );
												
												// update the driver transaction by debiting the admin commisiion charge
												$insert_driver_transaction = array (
														'driverId' => $trip_details->driverId,
														'tripId' => $trip_id,
														'transactionAmount' => $admin_commission_amount,
														'previousAmount' => $current_driver_wallet_amount->driverWallet,
														'currentAmount' => ($current_driver_wallet_amount->driverWallet - $admin_commission_amount),
														'transactionStatus' => 'Success',
														'transactionType' => Transaction_Type_Enum::TRIP_COMMISSION,
														'transactionFrom' => Transaction_From_Enum::WALLET_ACCOUNT,
														'transactionMode' => Transaction_Mode_Enum::DEBIT 
												);
												$update_driver_transaction = $this->Driver_Transaction_Details_Model->insert ( $insert_driver_transaction );
												$remaining_driver_wallet = $current_driver_wallet_amount->driverWallet - $admin_commission_amount;
												$update_driver_wallet = $this->Driver_Model->update ( array (
														'driverWallet' => $remaining_driver_wallet 
												), array (
														'id' => $trip_details->driverId 
												) );
												// @todo update driver dispath details table
												$driver_dispatch_exists = NULL;
												$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
														'driverId' => $trip_details->driverId 
												) );
												
												if ($driver_dispatch_exists) {
													$avg_distance = round ( ($driver_dispatch_exists->avgTripDistance + $travelled_distance) / ($driver_dispatch_exists->totalCompletedTrip + 1), 2 );
													$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
															'walletBalance' => $remaining_driver_wallet,
															'avgTripDistance' => $avg_distance 
													), array (
															'driverId' => $trip_details->driverId 
													) );
												} else {
													$insert_dispatch_data = array (
															'driverId' => $trip_details->driverId,
															'walletBalance' => $remaining_driver_wallet,
															'avgTripDistance' => $travelled_distance 
													);
													$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
												}
												
												// end driver dispath details table
												
												if ($promo_discount_amount) {
													sleep ( 1 );
													// get current wallet amount of driver to credit promocode ammount
													$current_driver_credit_amount = $this->Driver_Model->getById ( $trip_details->driverId );
													
													// update the driver transaction by credit promocode ammount
													$insert_driver_transaction = array (
															'driverId' => $trip_details->driverId,
															'tripId' => $trip_id,
															'transactionAmount' => $promo_discount_amount,
															'previousAmount' => $current_driver_credit_amount->driverCredit,
															'currentAmount' => ($current_driver_credit_amount->driverCredit + $promo_discount_amount),
															'transactionStatus' => 'Success',
															'transactionType' => Transaction_Type_Enum::PROMOCODE,
															'transactionFrom' => Transaction_From_Enum::CREDIT_ACCOUNT,
															'transactionMode' => Transaction_Mode_Enum::CREDIT 
													);
													$update_driver_transaction = $this->Driver_Transaction_Details_Model->insert ( $insert_driver_transaction );
													$remaining_driver_credit = $current_driver_credit_amount->driverCredit + $promo_discount_amount;
													$update_driver_wallet = $this->Driver_Model->update ( array (
															'driverCredit' => $remaining_driver_credit 
													), array (
															'id' => $trip_details->driverId 
													) );
												}
												if ($wallet_payment_amount) {
													sleep ( 1 );
													// get current wallet amount of driver to credit wallet payment ammount
													$current_driver_credit_amount = $this->Driver_Model->getById ( $trip_details->driverId );
													
													// update the driver transaction by credit promocode ammount
													$insert_driver_transaction = array (
															'driverId' => $trip_details->driverId,
															'tripId' => $trip_id,
															'transactionAmount' => $wallet_payment_amount,
															'previousAmount' => $current_driver_credit_amount->driverCredit,
															'currentAmount' => ($current_driver_credit_amount->driverCredit + $wallet_payment_amount),
															'transactionStatus' => 'Success',
															'transactionType' => Transaction_Type_Enum::TRIP_WALLET,
															'transactionFrom' => Transaction_From_Enum::CREDIT_ACCOUNT,
															'transactionMode' => Transaction_Mode_Enum::CREDIT 
													);
													$update_driver_transaction = $this->Driver_Transaction_Details_Model->insert ( $insert_driver_transaction );
													$remaining_driver_credit = $current_driver_credit_amount->driverCredit + $wallet_payment_amount;
													$update_driver_wallet = $this->Driver_Model->update ( array (
															'driverCredit' => $remaining_driver_credit 
													), array (
															'id' => $trip_details->driverId 
													) );
												}
												// update the trip details trip status to waiting for payment ,driver shift status to free also trip request details trip status to completed
												
												$trip_status = NULL;
												$notification_status = NULL;
												if ($trip_details->paymentMode == Payment_Mode_Enum::WALLET || $trip_details->paymentMode == Payment_Mode_Enum::CASH) {
													$trip_status = Trip_Status_Enum::WAITING_FOR_PAYMENT;
													$notification_status = Trip_Status_Enum::WAITING_FOR_PAYMENT;
												} else if ($trip_details->paymentMode == Payment_Mode_Enum::PAYTM) {
													$trip_status = Trip_Status_Enum::NO_NOTIFICATION;
													$notification_status = Trip_Status_Enum::NO_NOTIFICATION;
												} else if ($trip_details->paymentMode == Payment_Mode_Enum::INVOICE) {
													$trip_status = Trip_Status_Enum::WAITING_FOR_PAYMENT;
													$notification_status = Trip_Status_Enum::WAITING_FOR_PAYMENT;
												}
												$trip_status_update = $this->Trip_Details_Model->update ( array (
														'tripStatus' => $trip_status,
														'notificationStatus' => $notification_status,
														'dropLocation' => $drop_location,
														'dropLatitude' => $drop_latitude,
														'dropLongitude' => $drop_longitude,
														'dropDatetime' => $actual_drop_time 
												), array (
														'id' => $trip_id 
												) );
												
												$is_first_ride = $this->Trip_Details_Model->getByKeyValueArray ( array (
														'passengerId' => $trip_details->passengerId,
														'tripStatus' => Trip_Status_Enum::TRIP_COMPLETED 
												), 'id' );
												if (count ( $is_first_ride ) <= 0) {
													$passenger_referral_details = $this->Referral_Details_Model->getOneByKeyValueArray ( array (
															'registeredId' => $trip_details->passengerId 
													), 'id DESC' );
													if ($passenger_referral_details) {
														$passenger_referral_update = $this->Referral_Details_Model->update ( array (
																'registeredTripId' => $trip_id,
																'isReferrerEarned' => Status_Type_Enum::ACTIVE 
														), array (
																'registeredId' => $trip_details->passengerId 
														) );
														$passenger_details = $this->Passenger_Model->getById ( $passenger_referral_details->passengerId );
														$current_balance = $passenger_details->walletAmount + $passenger_referral_details->earnedAmount;
														$insert_data = array (
																'passengerId' => $passenger_referral_details->passengerId,
																'passengerReferralId' => $trip_details->passengerId,
																'tripId' => $trip_id,
																'transactionAmount' => $passenger_referral_details->earnedAmount,
																'previousAmount' => $passenger_details->walletAmount,
																'currentAmount' => $current_balance,
																'transactionStatus' => 'Success',
																'transactionType' => Transaction_Type_Enum::REFERRAL,
																'transactionMode' => Transaction_Mode_Enum::CREDIT,
																'transactionFrom' => Transaction_From_Enum::WALLET_ACCOUNT 
														);
														$passenger_wallet_updated = $this->Passenger_Transaction_Details_Model->insert ( $insert_data );
														$passenger_updated = $this->Passenger_Model->update ( array (
																'walletAmount' => $current_balance 
														), array (
																'id' => $passenger_referral_details->passengerId 
														) );
													}
												}
												// Referral_Details_Model $trip_request_status_update=$this->Taxi_Request_Details_Model->update(array('taxiRequestStatus'=>Taxi_Request_Status_Enum::COMPLETED_TRIP),array('tripId'=>$trip_id));
												// $driver_shift_status_update=$this->Driver_Shift_History_Model->update(array('availabilityStatus'=>Taxi_Available_Status_Enum::FREE),array('driverId'=>$trip_details->driverId));
												
												// get the temp tracking lat & log to update the tracked trip
												$temp_tracking_info = $this->Trip_Temp_Tracking_Details_Model->getByKeyValueArray ( array (
														'tripId' => $trip_id 
												) );
												if (count ( $temp_tracking_info ) > 0) {
													$tracking_details = '';
													foreach ( $temp_tracking_info as $list ) {
														if (empty ( $tracking_details )) {
															$tracking_details = '[' . $list->latitude . ',' . $list->longitude . ']';
														} else {
															$tracking_details .= ',[' . $list->latitude . ',' . $list->longitude . ']';
														}
													}
													$tracking_details .= ',[' . $drop_latitude . ',' . $drop_longitude . ']';
													
													$insert_data = array (
															'tripId' => $trip_id,
															'latitudeLongitude' => $tracking_details,
															'passengerId' => $trip_details->passengerId,
															'driverId' => $trip_details->driverId,
															'taxiId' => $trip_details->taxiId 
													);
													
													$trip_tracking_update = $this->Trip_Tracked_Details_Model->insert ( $insert_data );
													
													// delete the temp tracking lat & log
													$delete_temp_tracking_info = $this->Trip_Temp_Tracking_Details_Model->delete ( array (
															'tripId' => $trip_id 
													) );
												}
												// response data
												
												$trip_details = $this->Common_Api_Webservice_Model->getTripDetails ( $trip_id );
												
												// genereate invoice No
												$invoice_no = $trip_details [0]->tripType . '-' . $trip_details [0]->paymentMode . '-' . $trip_id;
												$parking_charge = 0;
												$toll_charge = 0;
												$driver_earning = round ( (($convenience_charge + $travel_charge) - $admin_commission_amount), 2 );
												
												$insert_data = array (
														'tripId' => $trip_id,
														'travelledDistance' => $travelled_distance,
														'travelledPeriod' => $total_travelled_minute,
														'convenienceCharge' => $convenience_charge,
														'travelCharge' => $travel_charge,
														'parkingCharge' => $parking_charge,
														'tollCharge' => $toll_charge,
														'taxCharge' => $tax_amount,
														'bookingCharge' => $booking_charge,
														'penaltyCharge' => $penalty_charge,
														'surgeCharge' => $surge_charge,
														'totalTripCharge' => round ( $total_trip_cost / 100 ) * 100,
														'adminAmount' => $admin_commission_amount,
														'promoDiscountAmount' => $promo_discount_amount,
														'walletPaymentAmount' => round ( $wallet_payment_amount, 2 ),
														'driverEarning' => $driver_earning,
														'taxPercentage' => $entity_config_details->tax,
														'tripType' => $trip_details [0]->tripType,
														'paymentMode' => $trip_details [0]->paymentMode,
														'paymentStatus' => ($trip_details [0]->paymentMode == Payment_Mode_Enum::INVOICE) ? '' : (($trip_details [0]->paymentMode != Payment_Mode_Enum::AGD) ? 'Success' : 'Waiting for PAYTM payment'),
														'transactionId' => 0,
														'invoiceNo' => $invoice_no 
												);
												// insert data to transaction table
												$transaction_insert_id = $this->Trip_Transaction_Details_Model->insert ( $insert_data );
												if ($transaction_insert_id) {
													$response_data = array (
															'trip_details' => array (
																	'trip_id' => $trip_id,
																	'job_card_id' => $trip_details [0]->jobCardId,
																	'pickup_location' => $trip_details [0]->pickupLocation,
																	'pickup_latitude' => $trip_details [0]->pickupLatitude,
																	'pickup_longitude' => $trip_details [0]->pickupLongitude,
																	'pickup_time' => $trip_details [0]->actualPickupDatetime,
																	'drop_location' => $trip_details [0]->dropLocation,
																	'drop_latitude' => $trip_details [0]->dropLatitude,
																	'drop_longitude' => $trip_details [0]->dropLongitude,
																	'drop_time' => $trip_details [0]->dropDatetime,
																	'land_mark' => $trip_details [0]->landmark,
																	'trip_type' => $trip_details [0]->tripType,
																	'trip_type_name' => $trip_details [0]->tripTypeName,
																	'payment_mode' => $trip_details [0]->paymentMode,
																	'payment_mode_name' => $trip_details [0]->paymentModeName,
																	'trip_status' => $trip_details [0]->tripStatus,
																	'trip_status_name' => $trip_details [0]->tripStatusName,
																	'is_driver_rated' => ($trip_details [0]->driverRating) ? TRUE : FALSE 
															),
															'passenger_details' => array (
																	'passenger_id' => $trip_details [0]->passengerId,
																	'passenger_firstname' => $trip_details [0]->passengerFirstName,
																	'passenger_lastname' => $trip_details [0]->passengerLastName,
																	'passenger_mobile' => $trip_details [0]->passengerMobile,
																	'passenger_profile_image' => ($trip_details [0]->passengerProfileImage) ? (passenger_content_url ( $trip_details [0]->passengerId . '/' . $trip_details [0]->passengerProfileImage )) : image_url ( 'app/user.png' ) 
															),
															'taxi_details' => array (
																	'taxi_id' => $trip_details [0]->taxiId,
																	'taxi_name' => $trip_details [0]->taxiName,
																	'taxi_model' => $trip_details [0]->taxiModel,
																	'taxi_manufacturer' => $trip_details [0]->taxiManufacturer,
																	'taxi_registration_no' => $trip_details [0]->taxiRegistrationNo,
																	'taxi_colour' => $trip_details [0]->taxiColour 
															),
															'driver_details' => array (
																	'driver_id' => $trip_details [0]->driverId,
																	'driver_firstname' => $trip_details [0]->driverFirstName,
																	'driver_lastname' => $trip_details [0]->driverLastName,
																	'driver_mobile' => $trip_details [0]->driverMobile,
																	'driver_profile_image' => ($trip_details [0]->driverProfileImage) ? (driver_content_url ( $trip_details [0]->driverId . '/' . $trip_details [0]->driverProfileImage )) : image_url ( 'app/user.png' ) 
															),
															'transaction_details' => array (
																	'convenience_charge' => $convenience_charge,
																	'travel_charge' => $travel_charge,
																	'promo_discount' => $promo_discount_amount,
																	'travelled_distance' => $travelled_distance,
																	'total_trip_cost' => round ( $total_trip_cost / 100 ) * 100,
																	'tax_amount' => $tax_amount,
																	'admin_commission_amount' => $admin_commission_amount,
																	'tax_percentage' => $entity_config_details->tax,
																	'travelled_period' => $total_travelled_minute,
																	'wallet_payment' => round ( $wallet_payment_amount, 2 ),
																	'distance_payment' => round ( $travelled_distance_charge, 2 ),
																	'time_payment' => $travelled_time_charge,
																	'booking_charge' => $booking_charge,
																	'penalty_charge' => $penalty_charge,
																	'surge_charge' => $surge_charge,
																	'promocode' => $trip_details [0]->promoCode 
															) 
													);
													// send FCM/GCM notification to passenger trip ended waiting for payment
													$this->tripNotification ( $trip_id, $trip_details [0]->passengerId, User_Type_Enum::PASSENGER, $msg_data ['trip_waiting_payment'], 4, $response_data );
													$message = array (
															"message" => $msg_data ['driver_start_complete'],
															"details" => array (
																	$response_data 
															),
															"status" => 1 
													);
												} else {
													$message = array (
															"message" => $msg_data ['trip_ended_transaction_failed'],
															"status" => - 1 
													);
												}
											} else {
												$message = array (
														"message" => $msg_data ['invalid_drop_time'],
														"status" => - 1 
												);
											}
										} else {
											
											$message = array (
													"message" => $msg_data ['invalid_rate_card'],
													"status" => - 1 
											);
										}
									} else {
										if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::PASSENGER_CANCELLED) {
											$message = array (
													"message" => $msg_data ['trip_reject_passenger'],
													"status" => - 1 
											);
										} else if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::COMPLETED_TRIP) {
											$message = array (
													"message" => $msg_data ['trip_completed'],
													"status" => - 1 
											);
										} else if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::AVAILABLE_TRIP) {
											$message = array (
													"message" => $msg_data ['trip_not_started'],
													"status" => - 1 
											);
										}
									}
								} else {
									$message = array (
											"message" => $msg_data ['invalid_trip_request'],
											"status" => - 1 
									);
								}
							} else {
								$message = array (
										"message" => $msg_data ['driver_login_failed'],
										"status" => - 1 
								);
							}
						} else {
							$message = array (
									"message" => $msg_data ['invalid_user'],
									"status" => - 1 
							);
						}
					} else {
						$message = array (
								"message" => $msg_data ['invalid_trip'],
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['invalid_trip'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	public function tripFareUpdate_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$driver_logged_status = NULL;
		$check_trip_avilablity = NULL;
		$transaction_exists = NULL;
		$trip_status_update = NULL;
		$trip_request_status_update = NULL;
		$driver_shift_status_update = NULL;
		$transaction_insert_id = NULL;
		$trip_details = NULL;
		
		$admin_amount = NULL;
		$company_amount = NULL;
		$invoice_no = NULL;
		$driver_earning = 0;
		$response_data = array ();
		$statistics = array ();
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$trip_id = NULL;
		$travelled_distance = NULL;
		$travel_charge = NULL;
		$total_trip_charge = NULL;
		$tax_charge = NULL;
		$payment_mode = NULL;
		$tax_percentage = NULL;
		$wallet_payment = 0;
		$promo_discount = 0;
		$travelled_period = NULL;
		$toll_charge = 0;
		$parking_charge = 0;
		$convenience_charge = 0;
		$penalty_charge = 0;
		$surge_charge = 0;
		$booking_charge = 0;
		$admin_amount = 0;
		$offline_transaction = 1;
		$arrived_date_time = NULL;
		$start_date_time = NULL;
		$end_date_time = NULL;
		$drop_latitude = NULL;
		$drop_longitude = NULL;
		$drop_location = NULL;
		$actual_travelled_distance = NULL;
		$actual_travelled_time = NULL;
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$trip_id = $post_data->trip_id;
		$travelled_distance = $post_data->travelled_distance;
		
		$travel_charge = $post_data->travel_charge;
		$total_trip_charge = $post_data->total_trip_charge;
		$tax_charge = $post_data->tax_charge;
		$wallet_payment = $post_data->wallet_payment;
		$promo_discount = $post_data->promo_discount;
		$travelled_period = $post_data->travelled_period;
		$toll_charge = $post_data->toll_charge;
		$parking_charge = $post_data->parking_charge;
		$convenience_charge = $post_data->convenience_charge;
		$admin_amount = $post_data->admin_amount;
		$booking_charge = $post_data->booking_charge;
		if (array_key_exists ( 'start_date_time', $post_data )) {
			$start_date_time = date ( 'Y-m-d H:i:s', strtotime ( $post_data->start_date_time ) );
		}
		if (array_key_exists ( 'arrived_date_time', $post_data )) {
			$arrived_date_time = date ( 'Y-m-d H:i:s', strtotime ( $post_data->arrived_date_time ) );
		}
		if (array_key_exists ( 'end_date_time', $post_data )) {
			$end_date_time = date ( 'Y-m-d H:i:s', strtotime ( $post_data->end_date_time ) );
		}
		
		if (array_key_exists ( 'offline_transaction', $post_data )) {
			$offline_transaction = $post_data->offline_transaction;
		}
		if (array_key_exists ( 'drop_latitude', $post_data )) {
			$drop_latitude = $post_data->drop_latitude;
		}
		if (array_key_exists ( 'drop_longitude', $post_data )) {
			$drop_longitude = $post_data->drop_longitude;
		}
		if (array_key_exists ( 'drop_location', $post_data )) {
			$drop_location = $post_data->drop_location;
		}
		/*
		 * if (array_key_exists ( 'payment_mode', $post_data )) {
		 * $payment_mode = $post_data->payment_mode;
		 * }
		 */
		if (array_key_exists ( 'tax_percentage', $post_data )) {
			$tax_percentage = $post_data->tax_percentage;
		}
		
		if (array_key_exists ( 'penalty_charge', $post_data )) {
			$penalty_charge = $post_data->penalty_charge;
		}
		if (array_key_exists ( 'surge_charge', $post_data )) {
			$surge_charge = $post_data->surge_charge;
		}
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual functionality
				if ($arrived_date_time && $start_date_time && $end_date_time) {
					if ($trip_id) {
						// get the trip details
						$trip_details = $this->Trip_Details_Model->getById ( $trip_id );
						
						// To Retain Travelled distance same as estimated distance
						$travelled_distance = $trip_details->estimatedDistance;
						
						$entity_details = $this->Entity_Model->getOneByKeyValueArray ( array (
								'id' => $trip_details->entityId 
						) );
						
						$entity_config_details = $this->Entity_Config_Model->getOneByKeyValueArray ( array (
								'entityId' => $trip_details->entityId 
						) );
						
						$driver_logged_status = $this->Driver_Model->getOneByKeyValueArray ( array (
								'id' => $trip_details->driverId,
								'status' => Status_Type_Enum::ACTIVE,
								'loginStatus' => Status_Type_Enum::ACTIVE 
						) );
						if ($driver_logged_status) {
							
							if ($trip_details) {
								// check trip avilable for driver/any in progress trip before logout
								$check_trip_avilablity = $this->Taxi_Request_Details_Model->getOneByKeyValueArray ( array (
										'tripId' => $trip_id 
								) );
								
								// end get driver statistics
								if ($check_trip_avilablity) {
									if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::DRIVER_ACCEPTED) {
										// genereate invoice No
										$total_trip_cost_insert = $total_trip_charge + $parking_charge + $toll_charge;
										
										$driver_earning = round ( (($convenience_charge + $travel_charge) - $admin_amount), 2 );
										
										$passenger_details = $this->Passenger_Model->getById ( $trip_details->passengerId );
										// @todo temprarory changes made to djust driver app calculation mistake
										$transaction_wallet_amount = 0;
										if ($total_trip_charge == $wallet_payment) {
											$transaction_wallet_amount = $total_trip_cost_insert;
										} else {
											$transaction_wallet_amount = $total_trip_cost_insert - ($total_trip_charge - $wallet_payment);
										}
										// To get actual travelled distance and time from google API
										if (($travelled_distance || $total_travelled_minute || $travelled_distance == '' || $total_travelled_minute == '') || ($pickup_latitude && $pickup_longitude && $drop_latitude && $drop_longitude)) {
											$URL = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=" . $trip_details->pickupLatitude . "," . $trip_details->pickupLongitude . "&destinations=" . $drop_latitude . "," . $drop_longitude . "&key=" . GOOGLE_MAP_API_KEY;
											
											$curl = curl_init ();
											curl_setopt ( $curl, CURLOPT_URL, $URL );
											curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, true );
											curl_setopt ( $curl, CURLOPT_HEADER, false );
											$result = curl_exec ( $curl );
											curl_close ( $curl );
											// return $result; ;
											
											$response = json_decode ( $result );
											
											if ($response->status) {
												if ($response->rows [0]->elements [0]->distance->text) {
													$distance = explode ( ' ', $response->rows [0]->elements [0]->distance->text );
													if ($distance [1] == 'm') {
														$actual_travelled_distance = round ( ($distance [0] / 1000), 1 );
													} else if ($distance [1] == 'km') {
														$actual_travelled_distance = round ( ($distance [0]), 1 );
													}
												}
												if ($response->rows [0]->elements [0]->duration->text) {
													$time = explode ( ' ', $response->rows [0]->elements [0]->duration->text );
													
													if ($time [1] == 'hour' || $time [1] == 'hours') {
														$actual_travelled_time += ($time [0] * 60) + $time [2];
													} else if ($time [1] == 'mins') {
														$actual_travelled_time = $time [0];
													}
												}
											}
										}
										// To get actual travelled distance and time from google API
										
										$insert_data = array (
												'tripId' => $trip_id,
												'travelledDistance' => $travelled_distance,
												'actualTravelledDistance' => $actual_travelled_distance,
												'travelledDistance' => $travelled_distance,
												'travelledPeriod' => $travelled_period,
												'convenienceCharge' => $convenience_charge,
												'travelCharge' => $travel_charge,
												'parkingCharge' => $parking_charge,
												'tollCharge' => $toll_charge,
												'taxCharge' => $tax_charge,
												'bookingCharge' => $booking_charge,
												'penaltyCharge' => $penalty_charge,
												'surgeCharge' => $surge_charge,
												'totalTripCharge' => round ( $total_trip_cost_insert / 100 ) * 100,
												'adminAmount' => $admin_amount,
												'promoDiscountAmount' => $promo_discount,
												'walletPaymentAmount' => $transaction_wallet_amount,
												'taxPercentage' => ($tax_percentage) ? $tax_percentage : $entity_config_details->tax,
												'driverEarning' => $driver_earning,
												'tripType' => $trip_details->tripType,
												'paymentMode' => $trip_details->paymentMode,
												'paymentStatus' => ($trip_details->paymentMode == Payment_Mode_Enum::INVOICE) ? '' : (($trip_details->paymentMode != Payment_Mode_Enum::AGD) ? 'Success' : 'Waiting for PAYTM payment'),
												'transactionId' => 0,
												'offlineTransaction' => ($offline_transaction) ? 1 : 0 
										);
										
										// to check transcation table exits trip record
										$transaction_exists = $this->Trip_Transaction_Details_Model->getOneByKeyValueArray ( array (
												'tripId' => $trip_id 
										) );
										if ($transaction_exists) {
											$total_trip_cost_insert = $total_trip_charge + $parking_charge + $toll_charge;
											
											$driver_earning = round ( ($total_trip_charge - $admin_amount), 2 );
											
											$passenger_details = $this->Passenger_Model->getById ( $trip_details->passengerId );
											
											$update_data = array (
													'travelledDistance' => $travelled_distance,
													'actualTravelledDistance' => $actual_travelled_distance,
													'travelledPeriod' => $travelled_period,
													'convenienceCharge' => $convenience_charge,
													'travelCharge' => $travel_charge,
													'parkingCharge' => $parking_charge,
													'tollCharge' => $toll_charge,
													'taxCharge' => $tax_charge,
													'bookingCharge' => $booking_charge,
													'penaltyCharge' => $penalty_charge,
													'surgeCharge' => $surge_charge,
													'totalTripCharge' => $total_trip_cost_insert,
													'adminAmount' => $admin_amount,
													'promoDiscountAmount' => $promo_discount,
													'walletPaymentAmount' => $transaction_wallet_amount,
													'taxPercentage' => ($tax_percentage) ? $tax_percentage : $entity_config_details->tax,
													'tripType' => $trip_details->tripType,
													'paymentMode' => $trip_details->paymentMode,
													'paymentStatus' => ($trip_details->paymentMode == Payment_Mode_Enum::INVOICE) ? '' : (($trip_details->paymentMode != Payment_Mode_Enum::AGD) ? 'Success' : 'Waiting for PAYTM payment'),
													'transactionId' => 0,
													'offlineTransaction' => ($offline_transaction) ? 1 : 0 
											);
											// update the transaction table
											
											$transaction_insert_id = $this->Trip_Transaction_Details_Model->update ( $update_data, array (
													'id' => $transaction_exists->id 
											) );
											$transaction_insert_id = $transaction_exists->id;
										} else {
											
											// insert data to transaction table
											$transaction_insert_id = $this->Trip_Transaction_Details_Model->insert ( $insert_data );
											if ($transaction_insert_id) {
												$invoice_id = ($transaction_insert_id <= 999999) ? str_pad ( ( string ) $transaction_insert_id, 6, '0', STR_PAD_LEFT ) : $transaction_insert_id;
												$invoice_no = 'INV' . $invoice_id;
												$update_invoice_no = $this->Trip_Transaction_Details_Model->update ( array (
														'invoiceNo' => $invoice_no 
												), array (
														'id' => $transaction_insert_id 
												) );
											}
										}
										if ($transaction_insert_id) {
											
											// passenger transaction history
											// @todo temprarory changes made to djust driver app calculation mistake
											// $transaction_amount = $total_trip_cost_insert - $wallet_payment;
											if ($total_trip_charge == $wallet_payment) {
												$transaction_amount = $total_trip_cost_insert - $wallet_payment;
											} else {
												$transaction_amount = $total_trip_cost_insert - ($total_trip_charge - $wallet_payment);
											}
											if ($transaction_amount > 0 && $passenger_details) {
												$insert_wallet_transaction = array (
														'passengerId' => $passenger_details->id,
														'tripId' => $trip_id,
														'transactionAmount' => $transaction_amount,
														'transactionStatus' => 'Success',
														'transactionType' => Transaction_Type_Enum::TRIP,
														'transactionFrom' => Transaction_From_Enum::CASH,
														'transactionMode' => Transaction_Mode_Enum::DEBIT 
												);
												$wallet_transaction_id = $this->Passenger_Transaction_Details_Model->insert ( $insert_wallet_transaction );
											}
											$transaction_payment_amount = 0;
											$current_payment_amount = 0;
											if ($wallet_payment > 0 && $passenger_details) {
												// get current wallet amount of passenger
												$current_wallet_amount = $this->Passenger_Model->getById ( $trip_details->passengerId );
												
												if ($offline_transaction) {
													
													if ($total_trip_charge >= $current_wallet_amount->walletAmount) {
														
														$wallet_payment_amount = $current_wallet_amount->walletAmount;
														// $total_trip_charge = $total_trip_charge - $current_wallet_amount->walletAmount;
														$remaing_wallet_amount ['walletAmount'] = 0;
														// @todo temprarory changes made to djust driver app calculation mistake
														$transaction_payment_amount = $wallet_payment_amount;
														$current_payment_amount = 0;
													} else {
														$actual_wallet_amount = $current_wallet_amount->walletAmount;
														
														$current_wallet_amount->walletAmount = $current_wallet_amount->walletAmount - $total_trip_charge;
														$wallet_payment_amount = $total_trip_charge;
														$remaing_wallet_amount ['walletAmount'] = $current_wallet_amount->walletAmount;
														// $total_trip_charge = 0;
														// @todo temprarory changes made to djust driver app calculation mistake
														$transaction_payment_amount = $wallet_payment_amount;
														$current_payment_amount = $current_wallet_amount->walletAmount;
													}
													$wallet_payment_update = $this->Passenger_Model->update ( $remaing_wallet_amount, array (
															'id' => $trip_details->passengerId 
													) );
												}
												
												// $current_wallet_amount = $this->Passenger_Model->getById ( $trip_details->passengerId );
												// $current_amount = $current_wallet_amount->walletAmount - $wallet_payment;
												$insert_wallet_transaction = array (
														'passengerId' => $current_wallet_amount->id,
														'tripId' => $trip_id,
														'transactionAmount' => $transaction_payment_amount,
														'previousAmount' => $current_wallet_amount->walletAmount,
														'currentAmount' => $current_payment_amount,
														'transactionStatus' => 'Success',
														'transactionType' => Transaction_Type_Enum::TRIP,
														'transactionFrom' => Transaction_From_Enum::WALLET_ACCOUNT,
														'transactionMode' => Transaction_Mode_Enum::DEBIT 
												);
												$wallet_transaction_id = $this->Passenger_Transaction_Details_Model->insert ( $insert_wallet_transaction );
											}
											// get passenger existing trip reject count to calucalate penalty and reset the penalty reject trip count
											$passenger_details = $this->Passenger_Model->getById ( $trip_details->passengerId );
											if ($passenger_details->penaltyTripCount) {
												$passenger_updated = $this->Passenger_Model->update ( array (
														'penaltyTripCount' => 0 
												), array (
														'id' => $trip_details->passengerId 
												) );
											}
											if ($offline_transaction) {
												if ($admin_amount) {
													sleep ( 1 );
													// update the driver transaction by debiting the admin commisiion charge
													$current_driver_wallet_amount = $this->Driver_Model->getById ( $trip_details->driverId );
													$insert_driver_transaction = array (
															'driverId' => $trip_details->driverId,
															'tripId' => $trip_id,
															'transactionAmount' => $admin_amount,
															'previousAmount' => $current_driver_wallet_amount->driverWallet,
															'currentAmount' => ($current_driver_wallet_amount->driverWallet - $admin_amount),
															'transactionStatus' => 'Success',
															'transactionType' => Transaction_Type_Enum::TRIP_COMMISSION,
															'transactionFrom' => Transaction_From_Enum::WALLET_ACCOUNT,
															'transactionMode' => Transaction_Mode_Enum::DEBIT 
													);
													$update_driver_transaction = $this->Driver_Transaction_Details_Model->insert ( $insert_driver_transaction );
													$remaining_driver_wallet = $current_driver_wallet_amount->driverWallet - $admin_amount;
													$update_driver_wallet = $this->Driver_Model->update ( array (
															'driverWallet' => $remaining_driver_wallet 
													), array (
															'id' => $trip_details->driverId 
													) );
													
													// @todo update driver dispath details table
													$driver_dispatch_exists = NULL;
													$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
															'driverId' => $trip_details->driverId 
													) );
													
													if ($driver_dispatch_exists) {
														
														$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
																'walletBalance' => $remaining_driver_wallet 
														), array (
																'driverId' => $trip_details->driverId 
														) );
													} else {
														$insert_dispatch_data = array (
																'driverId' => $trip_details->driverId,
																'walletBalance' => $remaining_driver_wallet 
														);
														$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
													}
													
													// end driver dispath details table
												}
												if ($promo_discount) {
													sleep ( 1 );
													// get current wallet amount of driver to credit promocode ammount
													$current_driver_wallet_amount = $this->Driver_Model->getById ( $trip_details->driverId );
													
													// update the driver transaction by credit promocode ammount
													$insert_driver_transaction = array (
															'driverId' => $trip_details->driverId,
															'tripId' => $trip_id,
															'transactionAmount' => $promo_discount,
															'previousAmount' => $current_driver_wallet_amount->driverCredit,
															'currentAmount' => ($current_driver_wallet_amount->driverCredit + $promo_discount),
															'transactionStatus' => 'Success',
															'transactionType' => Transaction_Type_Enum::PROMOCODE,
															'transactionFrom' => Transaction_From_Enum::CREDIT_ACCOUNT,
															'transactionMode' => Transaction_Mode_Enum::CREDIT 
													);
													$update_driver_transaction = $this->Driver_Transaction_Details_Model->insert ( $insert_driver_transaction );
													$remaining_driver_wallet = $current_driver_wallet_amount->driverCredit + $promo_discount;
													$update_driver_wallet = $this->Driver_Model->update ( array (
															'driverCredit' => $remaining_driver_wallet 
													), array (
															'id' => $trip_details->driverId 
													) );
												}
												if ($tax_charge) {
													sleep ( 1 );
													// get current wallet amount of driver to credit promocode ammount
													$current_driver_wallet_amount = $this->Driver_Model->getById ( $trip_details->driverId );
													
													// update the driver transaction by credit promocode ammount
													$insert_driver_transaction = array (
															'driverId' => $trip_details->driverId,
															'tripId' => $trip_id,
															'transactionAmount' => $tax_charge,
															'previousAmount' => $current_driver_wallet_amount->driverWallet,
															'currentAmount' => ($current_driver_wallet_amount->driverWallet - $tax_charge),
															'transactionStatus' => 'Success',
															'transactionType' => Transaction_Type_Enum::TAX_AMOUNT,
															'transactionFrom' => Transaction_From_Enum::WALLET_ACCOUNT,
															'transactionMode' => Transaction_Mode_Enum::DEBIT 
													);
													$update_driver_transaction = $this->Driver_Transaction_Details_Model->insert ( $insert_driver_transaction );
													$remaining_driver_wallet = $current_driver_wallet_amount->driverWallet - $tax_charge;
													$update_driver_wallet = $this->Driver_Model->update ( array (
															'driverWallet' => $remaining_driver_wallet 
													), array (
															'id' => $trip_details->driverId 
													) );
													
													// @todo update driver dispath details table
													$driver_dispatch_exists = NULL;
													$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
															'driverId' => $trip_details->driverId 
													) );
													
													if ($driver_dispatch_exists) {
														
														$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
																'walletBalance' => $remaining_driver_wallet 
														), array (
																'driverId' => $trip_details->driverId 
														) );
													} else {
														$insert_dispatch_data = array (
																'driverId' => $trip_details->driverId,
																'walletBalance' => $remaining_driver_wallet 
														);
														$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
													}
													
													// end driver dispath details table
												}
												if ($booking_charge) {
													sleep ( 1 );
													// get current wallet amount of driver to credit promocode ammount
													$current_driver_wallet_amount = $this->Driver_Model->getById ( $trip_details->driverId );
													
													// update the driver transaction by credit promocode ammount
													$insert_driver_transaction = array (
															'driverId' => $trip_details->driverId,
															'tripId' => $trip_id,
															'transactionAmount' => $booking_charge,
															'previousAmount' => $current_driver_wallet_amount->driverWallet,
															'currentAmount' => ($current_driver_wallet_amount->driverWallet - $booking_charge),
															'transactionStatus' => 'Success',
															'transactionType' => Transaction_Type_Enum::BOOKING_CHARGE,
															'transactionFrom' => Transaction_From_Enum::WALLET_ACCOUNT,
															'transactionMode' => Transaction_Mode_Enum::DEBIT 
													);
													$update_driver_transaction = $this->Driver_Transaction_Details_Model->insert ( $insert_driver_transaction );
													$remaining_driver_wallet = $current_driver_wallet_amount->driverWallet - $booking_charge;
													$update_driver_wallet = $this->Driver_Model->update ( array (
															'driverWallet' => $remaining_driver_wallet 
													), array (
															'id' => $trip_details->driverId 
													) );
													// @todo update driver dispath details table
													$driver_dispatch_exists = NULL;
													$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
															'driverId' => $trip_details->driverId 
													) );
													
													if ($driver_dispatch_exists) {
														
														$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
																'walletBalance' => $remaining_driver_wallet 
														), array (
																'driverId' => $trip_details->driverId 
														) );
													} else {
														$insert_dispatch_data = array (
																'driverId' => $trip_details->driverId,
																'walletBalance' => $remaining_driver_wallet 
														);
														$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
													}
													
													// end driver dispath details table
												}
												if ($penalty_charge) {
													sleep ( 1 );
													// get current wallet amount of driver to credit promocode ammount
													$current_driver_wallet_amount = $this->Driver_Model->getById ( $trip_details->driverId );
													
													// update the driver transaction by credit promocode ammount
													$insert_driver_transaction = array (
															'driverId' => $trip_details->driverId,
															'tripId' => $trip_id,
															'transactionAmount' => $penalty_charge,
															'previousAmount' => $current_driver_wallet_amount->driverWallet,
															'currentAmount' => ($current_driver_wallet_amount->driverWallet - $penalty_charge),
															'transactionStatus' => 'Success',
															'transactionType' => Transaction_Type_Enum::PASSENGER_PENALTY_CHARGE,
															'transactionFrom' => Transaction_From_Enum::WALLET_ACCOUNT,
															'transactionMode' => Transaction_Mode_Enum::DEBIT 
													);
													$update_driver_transaction = $this->Driver_Transaction_Details_Model->insert ( $insert_driver_transaction );
													$remaining_driver_wallet = $current_driver_wallet_amount->driverWallet - $penalty_charge;
													$update_driver_wallet = $this->Driver_Model->update ( array (
															'driverWallet' => $remaining_driver_wallet 
													), array (
															'id' => $trip_details->driverId 
													) );
													
													// @todo update driver dispath details table
													$driver_dispatch_exists = NULL;
													$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
															'driverId' => $trip_details->driverId 
													) );
													
													if ($driver_dispatch_exists) {
														
														$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
																'walletBalance' => $remaining_driver_wallet 
														), array (
																'driverId' => $trip_details->driverId 
														) );
													} else {
														$insert_dispatch_data = array (
																'driverId' => $trip_details->driverId,
																'walletBalance' => $remaining_driver_wallet 
														);
														$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
													}
													
													// end driver dispath details table
												}
												
												if ($wallet_payment) {
													sleep ( 1 );
													// get current wallet amount of driver to credit wallet payment ammount
													$current_driver_wallet_amount = $this->Driver_Model->getById ( $trip_details->driverId );
													
													// update the driver transaction by credit promocode ammount
													$insert_driver_transaction = array (
															'driverId' => $trip_details->driverId,
															'tripId' => $trip_id,
															'transactionAmount' => $wallet_payment,
															'previousAmount' => $current_driver_wallet_amount->driverCredit,
															'currentAmount' => ($current_driver_wallet_amount->driverCredit + $wallet_payment),
															'transactionStatus' => 'Success',
															'transactionType' => Transaction_Type_Enum::TRIP_WALLET,
															'transactionFrom' => Transaction_From_Enum::CREDIT_ACCOUNT,
															'transactionMode' => Transaction_Mode_Enum::CREDIT 
													);
													$update_driver_transaction = $this->Driver_Transaction_Details_Model->insert ( $insert_driver_transaction );
													$remaining_driver_wallet = $current_driver_wallet_amount->driverCredit + $wallet_payment;
													$update_driver_wallet = $this->Driver_Model->update ( array (
															'driverCredit' => $remaining_driver_wallet 
													), array (
															'id' => $trip_details->driverId 
													) );
												}
											}
											$trip_status = NULL;
											$notification_status = NULL;
											if ($trip_details->paymentMode == Payment_Mode_Enum::WALLET || $trip_details->paymentMode == Payment_Mode_Enum::CASH) {
												$trip_status = Trip_Status_Enum::TRIP_COMPLETED;
												$notification_status = Trip_Status_Enum::NO_NOTIFICATION;
											} else if ($trip_details->paymentMode == Payment_Mode_Enum::AGD) {
												$trip_status = Trip_Status_Enum::WAITING_FOR_PAYMENT;
												$notification_status = Trip_Status_Enum::WAITING_FOR_PAYMENT;
											} else if ($trip_details->paymentMode == Payment_Mode_Enum::INVOICE) {
												$trip_status = Trip_Status_Enum::TRIP_COMPLETED;
												$notification_status = Trip_Status_Enum::TRIP_COMPLETED;
											}
											
											if ($trip_details->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYMENT || $trip_details->tripStatus == Trip_Status_Enum::DRIVER_ACCEPTED || $trip_details->tripStatus == Trip_Status_Enum::IN_PROGRESS || $trip_details->tripStatus == Trip_Status_Enum::DRIVER_ARRIVED || $trip_details->tripStatus == Trip_Status_Enum::TRIP_COMPLETED || $trip_details->tripStatus == Trip_Status_Enum::NO_NOTIFICATION) {
												if ($drop_latitude && $drop_longitude) {
													//$drop_location = getAddressFromLatLong ( $drop_latitude, $drop_longitude );
													$trip_status_update = $this->Trip_Details_Model->update ( array (
															'tripStatus' => $trip_status,
															'notificationStatus' => $notification_status,
															'actualDropLatitude' => $drop_latitude,
															'actualDropLongitude' => $drop_longitude,
															'actualDropLocation' => ($drop_location) ? $drop_location : '',
															'taxiArrivedDatetime' => $arrived_date_time,
															'actualPickupDatetime' => $start_date_time,
															'dropDatetime' => $end_date_time 
													), array (
															'id' => $trip_id 
													) );
												} else {
													$trip_status_update = $this->Trip_Details_Model->update ( array (
															'tripStatus' => $trip_status,
															'notificationStatus' => $notification_status,
															'taxiArrivedDatetime' => $arrived_date_time,
															'actualPickupDatetime' => $start_date_time,
															'dropDatetime' => $end_date_time 
													), array (
															'id' => $trip_id 
													) );
												}
												
												$trip_request_status_update = $this->Taxi_Request_Details_Model->update ( array (
														'taxiRequestStatus' => Taxi_Request_Status_Enum::COMPLETED_TRIP,
														'selectedDriverId' => 0 
												), array (
														'tripId' => $trip_id 
												) );
												// get last inserted shift id for particular driver
												$get_last_shift_id = $this->Driver_Shift_History_Model->getOneByKeyValueArray ( array (
														'driverId' => $trip_details->driverId 
												), 'id DESC' );
												$driver_shift_status_update = $this->Driver_Shift_History_Model->update ( array (
														'availabilityStatus' => Taxi_Available_Status_Enum::FREE 
												), array (
														'id' => $get_last_shift_id->id 
												) );
											}
											// response data
											$response_data = array (
													"total_trip_cost" => $total_trip_charge,
													"pickup_location" => $trip_details->pickupLocation,
													"invoice_no" => $invoice_no,
													"trip_id" => $trip_id,
													"is_driver_rated" => ($trip_details->driverRating) ? TRUE : FALSE 
											);
											
											//
											// send sms & email to passenger
											
											// send sms & email to passenger
											
											// @todo update driver dispath details table
											$driver_dispatch_exists = NULL;
											$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
													'driverId' => $trip_details->driverId 
											) );
											if ($driver_dispatch_exists) {
												$week_completed_count = NULL;
												$week_completed_count = $this->Driver_Api_Webservice_Model->getWeekCompletedTripCount ( $trip_details->driverId );
												if ($week_completed_count) {
													$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
															'weekCompletedTrip' => $week_completed_count [0]->weekCompletedTripCount,
															'totalCompletedTrip' => ($driver_dispatch_exists->totalCompletedTrip + 1),
															'consecutiveRejectCount' => 0,
															'availabilityStatus' => Taxi_Available_Status_Enum::FREE,
															'tripId' => Status_Type_Enum::INACTIVE 
													), array (
															'driverId' => $trip_details->driverId 
													) );
												} else {
													$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
															'weekCompletedTrip' => 1,
															'totalCompletedTrip' => ($driver_dispatch_exists->totalCompletedTrip + 1),
															'consecutiveRejectCount' => 0,
															'availabilityStatus' => Taxi_Available_Status_Enum::FREE,
															'tripId' => Status_Type_Enum::INACTIVE 
													), array (
															'driverId' => $trip_details->driverId 
													) );
												}
											} else {
												$insert_dispatch_data = array (
														'driverId' => $trip_details->driverId,
														'totalCompletedTrip' => 1,
														'weekCompletedTrip' => 1,
														'availabilityStatus' => Taxi_Available_Status_Enum::FREE,
														'tripId' => Status_Type_Enum::INACTIVE 
												);
												$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
											}
											// @update the week completed trip count
											// end driver dispath details table
											// send FCM/GCM notification to passenger trip ended waiting for payment
											$this->tripNotification ( $trip_id, $trip_details->passengerId, User_Type_Enum::PASSENGER, $msg_data ['trip_completed'], 5 );
											// get the temp tracking lat & log to update the tracked trip
											$temp_tracking_info = NULL;
											$temp_tracking_info = $this->Trip_Temp_Tracking_Details_Model->getByKeyValueArray ( array (
													'tripId' => $trip_id 
											) );
											if (count ( $temp_tracking_info ) > 0) {
												$tracking_details = '';
												foreach ( $temp_tracking_info as $list ) {
													if (empty ( $tracking_details )) {
														$tracking_details = '[' . $list->latitude . ',' . $list->longitude . ']';
													} else {
														$tracking_details .= ',[' . $list->latitude . ',' . $list->longitude . ']';
													}
												}
												
												$insert_data = array (
														'tripId' => $trip_id,
														'latitudeLongitude' => $tracking_details,
														'passengerId' => $trip_details->passengerId,
														'driverId' => $trip_details->driverId,
														'taxiId' => $trip_details->taxiId 
												);
												
												$trip_tracking_update = $this->Trip_Tracked_Details_Model->insert ( $insert_data );
												
												// delete the temp tracking lat & log
												$delete_temp_tracking_info = $this->Trip_Temp_Tracking_Details_Model->delete ( array (
														'tripId' => $trip_id 
												) );
											}
											// get the temp tracking lat & log to update the tracked trip
											
											if ($trip_details->paymentMode == Payment_Mode_Enum::AGD) {
												$message = array (
														"message" => $msg_data ['trip_finalize_waiting_paytm_payment'],
														"details" => $response_data,
														"status" => 1 
												);
											} else {
												
												$message = array (
														"message" => $msg_data ['trip_finalize_success'],
														"details" => $response_data,
														"status" => 1 
												);
											}
										} else {
											$message = array (
													"message" => $msg_data ['trip_finalize_failed'],
													"status" => - 1 
											);
										}
									} else {
										if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::PASSENGER_CANCELLED) {
											$message = array (
													"message" => $msg_data ['trip_reject_passenger'],
													"status" => - 1 
											);
										} else if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::COMPLETED_TRIP) {
											$trip_details = $this->Trip_Details_Model->getById ( $trip_id );
											$transaction_exists = $this->Trip_Transaction_Details_Model->getOneByKeyValueArray ( array (
													'tripId' => $trip_id 
											) );
											// response data
											$response_data = array (
													"total_trip_cost" => $total_trip_charge,
													"pickup_location" => $trip_details->pickupLocation,
													"invoice_no" => $transaction_exists->invoiceNo,
													"trip_id" => $trip_id,
													"is_driver_rated" => ($trip_details->driverRating) ? TRUE : FALSE 
											);
											$message = array (
													"message" => $msg_data ['trip_completed'],
													"details" => $response_data,
													"status" => 1 
											);
										} else if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::AVAILABLE_TRIP) {
											$message = array (
													
													"message" => $msg_data ['trip_not_started'],
													"status" => - 1 
											);
										} else if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::DRIVER_REJECTED) {
											$message = array (
													"message" => $msg_data ['trip_reject_driver'],
													"status" => - 1 
											);
										}
									}
									
									// get the temp tracking lat & log to update the tracked trip
									$temp_tracking_info = $this->Trip_Temp_Tracking_Details_Model->getByKeyValueArray ( array (
											'tripId' => $trip_id 
									) );
									if (count ( $temp_tracking_info ) > 0) {
										$tracking_details = '';
										foreach ( $temp_tracking_info as $list ) {
											if (empty ( $tracking_details )) {
												$tracking_details = '[' . $list->latitude . ',' . $list->longitude . ']';
											} else {
												$tracking_details .= ',[' . $list->latitude . ',' . $list->longitude . ']';
											}
										}
										// $tracking_details .= ',[' . $drop_latitude . ',' . $drop_longitude . ']';
										
										$insert_data = array (
												'tripId' => $trip_id,
												'latitudeLongitude' => $tracking_details,
												'passengerId' => $trip_details->passengerId,
												'driverId' => $trip_details->driverId,
												'taxiId' => $trip_details->taxiId 
										);
										
										$trip_tracking_update = $this->Trip_Tracked_Details_Model->insert ( $insert_data );
										
										// delete the temp tracking lat & log
										$delete_temp_tracking_info = $this->Trip_Temp_Tracking_Details_Model->delete ( array (
												'tripId' => $trip_id 
										) );
									}
								} else {
									$message = array (
											"message" => $msg_data ['invalid_trip_request'],
											"status" => - 1 
									);
								}
							} else {
								$message = array (
										"message" => $msg_data ['invalid_trip'],
										"status" => - 1 
								);
							}
						} else {
							$message = array (
									"message" => $msg_data ['driver_login_failed'],
									"status" => - 1 
							);
						}
					} else {
						$message = array (
								"message" => $msg_data ['invalid_trip_finalize_data'],
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['mandatory_data'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	public function tripFareUpdate_old_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$driver_logged_status = NULL;
		$check_trip_avilablity = NULL;
		$transaction_exists = NULL;
		$trip_status_update = NULL;
		$trip_request_status_update = NULL;
		$driver_shift_status_update = NULL;
		$transaction_insert_id = NULL;
		$trip_details = NULL;
		
		$admin_amount = NULL;
		$company_amount = NULL;
		$invoice_no = NULL;
		$driver_earning = 0;
		$response_data = array ();
		$statistics = array ();
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$trip_id = NULL;
		$travelled_distance = NULL;
		$travel_charge = NULL;
		$total_trip_charge = NULL;
		$tax_charge = NULL;
		$payment_mode = NULL;
		$wallet_payment = NULL;
		$promo_discount = NULL;
		$travelled_period = NULL;
		$toll_charge = NULL;
		$parking_charge = NULL;
		$convenience_charge = NULL;
		$penalty_charge = 0;
		$surge_charge = 0;
		$booking_charge = 0;
		$admin_amount = 0;
		
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$trip_id = $post_data->trip_id;
		$travelled_distance = $post_data->travelled_distance;
		$travel_charge = $post_data->travel_charge;
		$total_trip_charge = $post_data->total_trip_charge;
		$tax_charge = $post_data->tax_charge;
		$payment_mode = $post_data->payment_mode;
		$wallet_payment = $post_data->wallet_payment;
		$promo_discount = $post_data->promo_discount;
		$travelled_period = $post_data->travelled_period;
		$toll_charge = $post_data->toll_charge;
		$parking_charge = $post_data->parking_charge;
		$convenience_charge = $post_data->convenience_charge;
		$admin_amount = $post_data->admin_amount;
		$booking_charge = $post_data->booking_charge;
		if (array_key_exists ( 'penalty_charge', $post_data )) {
			$penalty_charge = $post_data->penalty_charge;
		}
		if (array_key_exists ( 'surge_charge', $post_data )) {
			$surge_charge = $post_data->surge_charge;
		}
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual functionality
				
				if ($trip_id) {
					// get the trip details
					$trip_details = $this->Trip_Details_Model->getById ( $trip_id );
					
					$entity_details = $this->Entity_Model->getOneByKeyValueArray ( array (
							'id' => $trip_details->entityId 
					) );
					
					$entity_config_details = $this->Entity_Config_Model->getOneByKeyValueArray ( array (
							'entityId' => $trip_details->entityId 
					) );
					
					$driver_logged_status = $this->Driver_Model->getOneByKeyValueArray ( array (
							'id' => $trip_details->driverId,
							'status' => Status_Type_Enum::ACTIVE,
							'loginStatus' => Status_Type_Enum::ACTIVE 
					) );
					if ($driver_logged_status) {
						
						if ($trip_details) {
							// check trip avilable for driver/any in progress trip before logout
							$check_trip_avilablity = $this->Taxi_Request_Details_Model->getOneByKeyValueArray ( array (
									'tripId' => $trip_id 
							) );
							
							// end get driver statistics
							if ($check_trip_avilablity) {
								if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::DRIVER_ACCEPTED) {
									// genereate invoice No
									$invoice_no = $trip_details->jobCardId;
									$total_trip_cost_insert = $total_trip_charge + $parking_charge + $toll_charge;
									
									$driver_earning = round ( ($total_trip_charge - $admin_amount), 2 );
									
									$passenger_details = $this->Passenger_Model->getById ( $trip_details->passengerId );
									
									$insert_data = array (
											'tripId' => $trip_id,
											'travelDistance' => $travelled_distance,
											'travelledPeriod' => $travelled_period,
											'convenienceCharge' => $convenience_charge,
											'travelCharge' => $travel_charge,
											'parkingCharge' => $parking_charge,
											'tollCharge' => $toll_charge,
											'taxCharge' => $tax_charge,
											'bookingCharge' => $booking_charge,
											'penaltyCharge' => $penalty_charge,
											'surgeCharge' => $surge_charge,
											'totalTripCharge' => round ( $total_trip_cost_insert / 100 ) * 100,
											'adminAmount' => $admin_amount,
											'promoDiscountAmount' => $promo_discount,
											'walletPaymentAmount' => $wallet_payment,
											'taxPercentage' => $entity_config_details->tax,
											'driverEarning' => $driver_earning,
											'tripType' => $trip_details->tripType,
											'paymentMode' => $trip_details->paymentMode,
											'paymentStatus' => ($trip_details->paymentMode == Payment_Mode_Enum::INVOICE) ? '' : (($trip_details->paymentMode != Payment_Mode_Enum::AGD) ? 'Success' : 'Waiting for PAYTM payment'),
											'transactionId' => 0,
											'invoiceNo' => $invoice_no 
									);
									
									// to check transcation table exits trip record
									$transaction_exists = $this->Trip_Transaction_Details_Model->getOneByKeyValueArray ( array (
											'tripId' => $trip_id 
									) );
									if ($transaction_exists) {
										$total_trip_cost_insert = $total_trip_charge + $parking_charge + $toll_charge;
										
										$driver_earning = round ( ($total_trip_charge - $admin_amount), 2 );
										
										$passenger_details = $this->Passenger_Model->getById ( $trip_details->passengerId );
										
										$update_data = array (
												'travelledDistance' => $travelled_distance,
												'travelledPeriod' => $travelled_period,
												'convenienceCharge' => $convenience_charge,
												'travelCharge' => $travel_charge,
												'parkingCharge' => $parking_charge,
												'tollCharge' => $toll_charge,
												'taxCharge' => $tax_charge,
												'bookingCharge' => $booking_charge,
												'penaltyCharge' => $penalty_charge,
												'surgeCharge' => $surge_charge,
												'totalTripCharge' => $total_trip_cost_insert,
												'adminAmount' => $admin_amount,
												'promoDiscountAmount' => $promo_discount,
												'walletPaymentAmount' => $wallet_payment,
												'taxPercentage' => $entity_config_details->tax,
												'tripType' => $trip_details->tripType,
												'paymentMode' => $trip_details->paymentMode,
												'paymentStatus' => ($trip_details->paymentMode == Payment_Mode_Enum::INVOICE) ? '' : (($trip_details->paymentMode != Payment_Mode_Enum::AGD) ? 'Success' : 'Waiting for PAYTM payment'),
												'transactionId' => 0 
										);
										// update the transaction table
										
										$transaction_insert_id = $this->Trip_Transaction_Details_Model->update ( $update_data, array (
												'id' => $transaction_exists->id 
										) );
										$transaction_insert_id = $transaction_exists->id;
									} else {
										// insert data to transaction table
										$transaction_insert_id = $this->Trip_Transaction_Details_Model->insert ( $insert_data );
									}
									if ($transaction_insert_id) {
										
										// passenger transaction history
										$transaction_amount = $total_trip_cost_insert - $wallet_payment;
										if ($transaction_amount > 0 && $passenger_details) {
											$insert_wallet_transaction = array (
													'passengerId' => $passenger_details->id,
													'tripId' => $trip_id,
													'transactionAmount' => $transaction_amount,
													'transactionStatus' => 'Success',
													'transactionType' => Transaction_Type_Enum::TRIP,
													'transactionFrom' => Transaction_From_Enum::CASH,
													'transactionMode' => Transaction_Mode_Enum::DEBIT 
											);
											$wallet_transaction_id = $this->Passenger_Transaction_Details_Model->insert ( $insert_wallet_transaction );
										}
										if ($wallet_payment > 0 && $passenger_details) {
											$current_amount = $passenger_details->walletAmount - $wallet_payment;
											$insert_wallet_transaction = array (
													'passengerId' => $passenger_details->id,
													'tripId' => $trip_id,
													'transactionAmount' => $wallet_payment,
													'previousAmount' => $passenger_details->walletAmount,
													'currentAmount' => $current_amount,
													'transactionStatus' => 'Success',
													'transactionType' => Transaction_Type_Enum::TRIP,
													'transactionFrom' => Transaction_From_Enum::WALLET_ACCOUNT,
													'transactionMode' => Transaction_Mode_Enum::DEBIT 
											);
											$wallet_transaction_id = $this->Passenger_Transaction_Details_Model->insert ( $insert_wallet_transaction );
										}
										
										$trip_status = NULL;
										$notification_status = NULL;
										if ($trip_details->paymentMode == Payment_Mode_Enum::WALLET || $trip_details->paymentMode == Payment_Mode_Enum::CASH) {
											$trip_status = Trip_Status_Enum::TRIP_COMPLETED;
											$notification_status = Trip_Status_Enum::NO_NOTIFICATION;
										} else if ($trip_details->paymentMode == Payment_Mode_Enum::AGD) {
											$trip_status = Trip_Status_Enum::WAITING_FOR_PAYMENT;
											$notification_status = Trip_Status_Enum::WAITING_FOR_PAYMENT;
										} else if ($trip_details->paymentMode == Payment_Mode_Enum::INVOICE) {
											$trip_status = Trip_Status_Enum::TRIP_COMPLETED;
											$notification_status = Trip_Status_Enum::TRIP_COMPLETED;
										}
										
										if ($trip_details->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYMENT || $trip_details->tripStatus == Trip_Status_Enum::NO_NOTIFICATION) {
											$trip_status_update = $this->Trip_Details_Model->update ( array (
													'tripStatus' => $trip_status,
													'notificationStatus' => $notification_status 
											), array (
													'id' => $trip_id 
											) );
											
											$trip_request_status_update = $this->Taxi_Request_Details_Model->update ( array (
													'taxiRequestStatus' => Taxi_Request_Status_Enum::COMPLETED_TRIP,
													'selectedDriverId' => 0 
											), array (
													'tripId' => $trip_id 
											) );
											// get last inserted shift id for particular driver
											$get_last_shift_id = $this->Driver_Shift_History_Model->getOneByKeyValueArray ( array (
													'driverId' => $trip_details->driverId 
											), 'id DESC' );
											$driver_shift_status_update = $this->Driver_Shift_History_Model->update ( array (
													'availabilityStatus' => Taxi_Available_Status_Enum::FREE 
											), array (
													'id' => $get_last_shift_id->id 
											) );
										}
										// response data
										$response_data = array (
												"total_trip_cost" => $total_trip_charge,
												"pickup_location" => $trip_details->pickupLocation,
												"invoice_no" => $invoice_no,
												"trip_id" => $trip_id,
												"is_driver_rated" => ($trip_details->driverRating) ? TRUE : FALSE 
										);
										
										//
										// send sms & email to passenger
										
										/*
										 * if (SMS && $transaction_insert_id) {
										 * // SMS To passenger
										 *
										 * if ($trip_details->paymentMode == Payment_Mode_Enum::PAYTM) {
										 * $message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
										 * 'title' => 'complete_trip_wallet'
										 * ) );
										 * } else {
										 * $message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
										 * 'title' => 'complete_trip'
										 * ) );
										 * }
										 * $message_data = $message_details->content;
										 * $message_data = str_replace ( "##USERNAME##", $driver_logged_status->firstName . ' ' . $driver_logged_status->lastName, $message_data );
										 * $message_data = str_replace ( "##FARE##", $total_trip_cost, $message_data );
										 *
										 * if ($trip_details->paymentMode == Payment_Mode_Enum::PAYTM) {
										 * $message_data = str_replace ( "##PAID##", $total_trip_cost + $parking_charge + $toll_charge, $message_data );
										 * }
										 *
										 *
										 * $message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
										 * 'title' => 'new_complete_trip_sms'
										 * ) );
										 * $message_data = $message_details->content;
										 * $message_data = str_replace ( "##BOOKINGID##", $trip_id, $message_data );
										 * $message_data = str_replace ( "##STARTDATETIME##", date ( 'Y-m-d H:i:s', $actual_pickup_time ), $message_data );
										 * $message_data = str_replace ( "##ENDDATETIME##", date ( 'Y-m-d H:i:s', $drop_time ), $message_data );
										 * // print_r($message);exit;
										 * if ($passenger_details) {
										 * if ($passenger_details->mobile) {
										 *
										 * $this->Common_Api_Webservice_Model->sendSMS ( $passenger_details->mobile, $message_data );
										 * }
										 * }
										 * // print_r($message);exit;
										 * if ($passenger_details) {
										 * if ($passenger_details->mobile) {
										 *
										 * $this->Common_Api_Webservice_Model->sendSMS ( $passenger_details->mobile, $message_data );
										 * }
										 * }
										 * // SMS TO driver
										 * /*
										 * $message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
										 * 'title' => 'complete_trip_driver_sms'
										 * ) );
										 * $customer_care_no = ($trip_details->bookingCityId == 1) ? CUSTOMERCARE_MUMBAI : ($trip_details->bookingCityId == 2) ? CUSTOMERCARE_BANGALORE : ($trip_details->bookingCityId == 3) ? CUSTOMERCARE_PUNE : CUSTOMERCARE_DEFAULT;
										 * $message_data = $message_details->content;
										 * $message_data = str_replace ( "##CUSTOMERCARE##", $customer_care_no, $message_data );
										 * $message_data = str_replace ( "##FARE##", $total_trip_cost, $message_data );
										 * $message_data = str_replace ( "##COLLECT##", $total_trip_cost + $parking_charge + $toll_charge, $message_data );
										 *
										 * // print_r($message);exit;
										 * if ($driver_logged_status->mobile) {
										 *
										 * $this->Common_Api_Webservice_Model->sendSMS ( $driver_logged_status->mobile, $message_data );
										 * // this SMS is sending to below numbers on request through WhatsApp from Sidhanth on 10/01/2017
										 * $this->Common_Api_Webservice_Model->sendSMS ( '8451976667', $message_data );
										 * // this SMS is sending to below numbers on request through WhatsApp from Sidhanth on 10/01/2017
										 * }
										 *
										 * }
										 */
										/*
										 * $bill_details = $this->Driver_Api_Webservice_Model->getBillDetails ( $transaction_insert_id );
										 * $from = SMTP_EMAIL_ID;
										 * $to = ($passenger_details) ? $passenger_details->email : NULL;
										 * $subject = "Your " . date ( 'l', strtotime ( $bill_details [0]->actualPickupDatetime ) ) . " trip with Zuver Rs." . $bill_details [0]->totalTripCost;
										 *
										 * $data = array (
										 * 'passengerFullName' => ($bill_details [0]->passengerFullName) ? $bill_details [0]->passengerFullName : '',
										 * 'passengerEmail' => ($bill_details [0]->passengerEmail) ? $bill_details [0]->passengerEmail : '',
										 * 'driverFullName' => ($bill_details [0]->driverFullName) ? $bill_details [0]->driverFullName : '',
										 * 'invoiceNo' => ($bill_details [0]->invoiceNo) ? $bill_details [0]->invoiceNo : '',
										 * 'invoiceDatetime' => ($bill_details [0]->invoiceDatetime) ? date ( 'd M, Y, h:i a', strtotime ( $bill_details [0]->invoiceDatetime ) ) : '',
										 * 'actualPickupDatetime' => ($bill_details [0]->actualPickupDatetime) ? date ( 'd M, Y, h:i a', strtotime ( $bill_details [0]->actualPickupDatetime ) ) : '',
										 * 'dropDatetime' => ($bill_details [0]->dropDatetime) ? date ( 'd M, Y, h:i a', strtotime ( $bill_details [0]->dropDatetime ) ) : '',
										 * 'bookedDatetime' => ($bill_details [0]->bookedDatetime) ? date ( 'd M, Y, h:i a', strtotime ( $bill_details [0]->bookedDatetime ) ) : '',
										 * 'tripType' => ($bill_details [0]->tripType) ? $bill_details [0]->tripType : '',
										 * 'paymentMode' => ($bill_details [0]->paymentMode) ? $bill_details [0]->paymentMode : '',
										 * 'totalTripCost' => ($bill_details [0]->totalTripCost) ? round ( $bill_details [0]->totalTripCost, 2 ) : '00.00',
										 * 'totalCash' => ($bill_details [0]->totalTripCost - $bill_details [0]->walletPaymentAmount) ? round ( $bill_details [0]->totalTripCost - $bill_details [0]->walletPaymentAmount, 2 ) : '00.00',
										 * 'companyTax' => ($bill_details [0]->companyTax) ? round ( $bill_details [0]->companyTax, 2 ) : '00.00',
										 * 'taxPercentage' => $tax,
										 * 'convenienceCharge' => ($bill_details [0]->convenienceCharge) ? round ( $bill_details [0]->convenienceCharge, 2 ) : '00.00',
										 * 'travelCharge' => ($bill_details [0]->travelCharge) ? round ( $bill_details [0]->travelCharge, 2 ) : '00.00',
										 * 'parkingCharge' => ($bill_details [0]->parkingCharge) ? round ( $bill_details [0]->parkingCharge, 2 ) : '00.00',
										 * 'tollCharge' => ($bill_details [0]->tollCharge) ? round ( $bill_details [0]->tollCharge, 2 ) : '00.00',
										 * 'promoDiscountAmount' => ($bill_details [0]->promoDiscountAmount) ? round ( $bill_details [0]->promoDiscountAmount, 2 ) : '00.00',
										 * 'otherDiscountAmount' => ($bill_details [0]->otherDiscountAmount) ? round ( $bill_details [0]->otherDiscountAmount, 2 ) : '00.00',
										 * 'walletPaymentAmount' => ($bill_details [0]->walletPaymentAmount) ? round ( $bill_details [0]->walletPaymentAmount, 2 ) : '00.00',
										 * 'travelHoursMinutes' => ($bill_details [0]->travelledPeriod) ? $bill_details [0]->travelledPeriod : '',
										 * 'otherCharge' => ($bill_details [0]->otherCharge) ? round ( $bill_details [0]->otherCharge, 2 ) : '00.00'
										 * );
										 * $message_data = $this->load->view ( 'emailtemplate/tripcomplete-mail', $data, TRUE );
										 *
										 * if (SMTP && $transaction_insert_id) {
										 *
										 * if ($to) {
										 * $this->Common_Api_Webservice_Model->sendEmail ( $to, $subject, $message_data );
										 * } else {
										 * // B2B completed trip invoice to be send to 'info@zuver.in' requested by Ayush through email on 10/01/2017
										 * $this->Common_Api_Webservice_Model->sendEmail ( 'info@zuver.in', $subject, $message_data );
										 * // B2B completed trip invoice to be send to 'info@zuver.in' requested by Ayush through email on 10/01/2017
										 * }
										 * } else {
										 *
										 * // To send HTML mail, the Content-type header must be set
										 * $headers = 'MIME-Version: 1.0' . "\r\n";
										 * $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
										 * // Additional headers
										 * $headers .= 'From: Zuver<' . $from . '>' . "\r\n";
										 * $headers .= 'To: <' . $to . '>' . "\r\n";
										 * if (! filter_var ( $to, FILTER_VALIDATE_EMAIL ) === false) {
										 * mail ( $to, $subject, $message_data, $headers );
										 * }
										 * }
										 */
										// send sms & email to passenger
										
										// @todo update driver dispath details table
										$driver_dispatch_exists = NULL;
										$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
												'driverId' => $trip_details->driverId 
										) );
										if ($driver_dispatch_exists) {
											$week_completed_count = NULL;
											$week_completed_count = $this->Driver_Api_Webservice_Model->getWeekCompletedTripCount ( $trip_details->driverId );
											if ($week_completed_count) {
												$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
														'weekCompletedTrip' => $week_completed_count [0]->weekCompletedTripCount,
														'totalCompletedTrip' => ($driver_dispatch_exists->totalCompletedTrip + 1),
														'consecutiveRejectCount' => 0,
														'availabilityStatus' => Taxi_Available_Status_Enum::FREE 
												), array (
														'driverId' => $trip_details->driverId 
												) );
											} else {
												$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
														'weekCompletedTrip' => 1,
														'totalCompletedTrip' => ($driver_dispatch_exists->totalCompletedTrip + 1),
														'consecutiveRejectCount' => 0,
														'availabilityStatus' => Taxi_Available_Status_Enum::FREE 
												), array (
														'driverId' => $trip_details->driverId 
												) );
											}
										} else {
											$insert_dispatch_data = array (
													'driverId' => $trip_details->driverId,
													'totalCompletedTrip' => 1,
													'weekCompletedTrip' => 1,
													'availabilityStatus' => Taxi_Available_Status_Enum::FREE 
											);
											$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
										}
										// @update the week completed trip count
										// end driver dispath details table
										// send FCM/GCM notification to passenger trip ended waiting for payment
										$this->tripNotification ( $trip_id, $trip_details->passengerId, User_Type_Enum::PASSENGER, $msg_data ['trip_completed'], 5 );
										
										if ($trip_details->paymentMode == Payment_Mode_Enum::AGD) {
											$message = array (
													"message" => $msg_data ['trip_finalize_waiting_paytm_payment'],
													"details" => $response_data,
													"status" => 1 
											);
										} else {
											
											$message = array (
													"message" => $msg_data ['trip_finalize_success'],
													"details" => $response_data,
													"status" => 1 
											);
										}
									} else {
										$message = array (
												"message" => $msg_data ['trip_finalize_failed'],
												"details" => $response_data,
												"status" => - 1 
										);
									}
								} else {
									if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::PASSENGER_CANCELLED) {
										$message = array (
												"message" => $msg_data ['trip_reject_passenger'],
												"status" => - 1 
										);
									} else if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::COMPLETED_TRIP) {
										$message = array (
												"message" => $msg_data ['trip_completed'],
												"details" => $response_data,
												"status" => - 1 
										);
									} else if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::AVAILABLE_TRIP) {
										$message = array (
												"message" => $msg_data ['trip_not_started'],
												"status" => - 1 
										);
									} else if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::DRIVER_REJECTED) {
										$message = array (
												"message" => $msg_data ['trip_reject_driver'],
												"status" => - 1 
										);
									}
								}
							} else {
								$message = array (
										"message" => $msg_data ['invalid_trip_request'],
										"status" => - 1 
								);
							}
						} else {
							$message = array (
									"message" => $msg_data ['invalid_trip'],
									"status" => - 1 
							);
						}
					} else {
						$message = array (
								"message" => $msg_data ['driver_login_failed'],
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['invalid_trip_finalize_data'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	public function offlineTripFareUpdate_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$driver_logged_status = NULL;
		$check_trip_avilablity = NULL;
		$transaction_exists = NULL;
		$trip_status_update = NULL;
		$trip_request_status_update = NULL;
		$driver_shift_status_update = NULL;
		$transaction_insert_id = NULL;
		$trip_details = NULL;
		
		$admin_amount = NULL;
		$company_amount = NULL;
		$invoice_no = NULL;
		$driver_earning = 0;
		$response_data = array ();
		$statistics = array ();
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$trip_id = NULL;
		$travelled_distance = NULL;
		$travel_charge = NULL;
		$total_trip_charge = NULL;
		$tax_charge = NULL;
		$tax_percentage = NULL;
		$wallet_payment = NULL;
		$promo_discount = NULL;
		$travelled_period = NULL;
		$toll_charge = NULL;
		$parking_charge = NULL;
		$convenience_charge = NULL;
		$penalty_charge = 0;
		$surge_charge = 0;
		$booking_charge = 0;
		$admin_amount = 0;
		
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$trip_id = $post_data->trip_id;
		$travelled_distance = $post_data->travelled_distance;
		$travel_charge = $post_data->travel_charge;
		$total_trip_charge = $post_data->total_trip_charge;
		$tax_charge = $post_data->tax_charge;
		$tax_percentage = $post_data->tax_percentage;
		$wallet_payment = $post_data->wallet_payment;
		$promo_discount = $post_data->promo_discount;
		$travelled_period = $post_data->travelled_period;
		$toll_charge = $post_data->toll_charge;
		$parking_charge = $post_data->parking_charge;
		$convenience_charge = $post_data->convenience_charge;
		$admin_amount = $post_data->admin_amount;
		$booking_charge = $post_data->booking_charge;
		if (array_key_exists ( 'penalty_charge', $post_data )) {
			$penalty_charge = $post_data->penalty_charge;
		}
		if (array_key_exists ( 'surge_charge', $post_data )) {
			$surge_charge = $post_data->surge_charge;
		}
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				
				// actual functionality
				
				if ($trip_id) {
					// get the trip details
					$trip_details = $this->Trip_Details_Model->getById ( $trip_id );
					
					$entity_details = $this->Entity_Model->getOneByKeyValueArray ( array (
							'id' => $trip_details->entityId 
					) );
					
					$entity_config_details = $this->Entity_Config_Model->getOneByKeyValueArray ( array (
							'entityId' => $trip_details->entityId 
					) );
					
					$driver_logged_status = $this->Driver_Model->getOneByKeyValueArray ( array (
							'id' => $trip_details->driverId,
							'status' => Status_Type_Enum::ACTIVE,
							'loginStatus' => Status_Type_Enum::ACTIVE 
					) );
					if ($driver_logged_status) {
						
						if ($trip_details) {
							// check trip avilable for driver/any in progress trip before logout
							$check_trip_avilablity = $this->Taxi_Request_Details_Model->getOneByKeyValueArray ( array (
									'tripId' => $trip_id 
							) );
							
							// end get driver statistics
							if ($check_trip_avilablity) {
								if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::DRIVER_ACCEPTED) {
									// genereate invoice No
									$invoice_no = $trip_details->jobCardId;
									$total_trip_cost_insert = $total_trip_charge + $parking_charge + $toll_charge;
									
									$driver_earning = round ( ($total_trip_charge - $admin_amount), 2 );
									
									$passenger_details = $this->Passenger_Model->getById ( $trip_details->passengerId );
									
									$insert_data = array (
											'tripId' => $trip_id,
											'travelDistance' => $travelled_distance,
											'travelledPeriod' => $travelled_period,
											'convenienceCharge' => $convenience_charge,
											'travelCharge' => $travel_charge,
											'parkingCharge' => $parking_charge,
											'tollCharge' => $toll_charge,
											'taxCharge' => $tax_charge,
											'bookingCharge' => $booking_charge,
											'penaltyCharge' => $penalty_charge,
											'surgeCharge' => $surge_charge,
											'totalTripCharge' => $total_trip_cost_insert,
											'adminAmount' => $admin_amount,
											'promoDiscountAmount' => $promo_discount,
											'walletPaymentAmount' => $wallet_payment,
											'taxPercentage' => $tax_percentage,
											'driverEarning' => $driver_earning,
											'tripType' => $trip_details->tripType,
											'paymentMode' => $trip_details->paymentMode,
											'paymentStatus' => ($trip_details->paymentMode == Payment_Mode_Enum::INVOICE) ? '' : (($trip_details->paymentMode != Payment_Mode_Enum::AGD) ? 'Success' : 'Waiting for PAYTM payment'),
											'transactionId' => 0,
											'offlineTransaction' => 1,
											'invoiceNo' => $invoice_no 
									);
									
									// to check transcation table exits trip record
									$transaction_exists = $this->Trip_Transaction_Details_Model->getOneByKeyValueArray ( array (
											'tripId' => $trip_id 
									) );
									if ($transaction_exists) {
										$total_trip_cost_insert = $total_trip_charge + $parking_charge + $toll_charge;
										
										$driver_earning = round ( ($total_trip_charge - $admin_amount), 2 );
										
										$passenger_details = $this->Passenger_Model->getById ( $trip_details->passengerId );
										
										$update_data = array (
												'travelledDistance' => $travelled_distance,
												'travelledPeriod' => $travelled_period,
												'convenienceCharge' => $convenience_charge,
												'travelCharge' => $travel_charge,
												'parkingCharge' => $parking_charge,
												'tollCharge' => $toll_charge,
												'taxCharge' => $tax_charge,
												'bookingCharge' => $booking_charge,
												'penaltyCharge' => $penalty_charge,
												'surgeCharge' => $surge_charge,
												'totalTripCharge' => $total_trip_cost_insert,
												'adminAmount' => $admin_amount,
												'promoDiscountAmount' => $promo_discount,
												'walletPaymentAmount' => $wallet_payment,
												'taxPercentage' => $tax_percentage,
												'tripType' => $trip_details->tripType,
												'paymentMode' => $trip_details->paymentMode,
												'paymentStatus' => ($trip_details->paymentMode == Payment_Mode_Enum::INVOICE) ? '' : (($trip_details->paymentMode != Payment_Mode_Enum::AGD) ? 'Success' : 'Waiting for PAYTM payment'),
												'transactionId' => 0,
												'offlineTransaction' => 1 
										);
										// update the transaction table
										
										$transaction_insert_id = $this->Trip_Transaction_Details_Model->update ( $update_data, array (
												'id' => $transaction_exists->id 
										) );
										$transaction_insert_id = $transaction_exists->id;
									} else {
										// insert data to transaction table
										$transaction_insert_id = $this->Trip_Transaction_Details_Model->insert ( $insert_data );
									}
									if ($transaction_insert_id) {
										
										// passenger transaction history
										$transaction_amount = $total_trip_cost_insert - $wallet_payment;
										if ($transaction_amount > 0 && $passenger_details) {
											$insert_wallet_transaction = array (
													'passengerId' => $passenger_details->id,
													'tripId' => $trip_id,
													'transactionAmount' => $transaction_amount,
													'transactionStatus' => 'Success',
													'transactionType' => Transaction_Type_Enum::TRIP,
													'transactionFrom' => Transaction_From_Enum::CASH,
													'transactionMode' => Transaction_Mode_Enum::DEBIT 
											);
											$wallet_transaction_id = $this->Passenger_Transaction_Details_Model->insert ( $insert_wallet_transaction );
										}
										if ($wallet_payment > 0 && $passenger_details) {
											
											// get current wallet amount of passenger
											$current_wallet_amount = $this->Passenger_Model->getById ( $trip_details->passengerId );
											
											if ($total_trip_cost >= $current_wallet_amount->walletAmount) {
												$wallet_payment_amount = $current_wallet_amount->walletAmount;
												// $total_trip_cost = $total_trip_cost - $current_wallet_amount->walletAmount;
												$remaing_wallet_amount ['walletAmount'] = 0;
											} else {
												$actual_wallet_amount = $current_wallet_amount->walletAmount;
												
												$current_wallet_amount->walletAmount = $current_wallet_amount->walletAmount - $total_trip_cost;
												$wallet_payment_amount = $actual_wallet_amount - $current_wallet_amount->walletAmount;
												$remaing_wallet_amount ['walletAmount'] = $current_wallet_amount->walletAmount;
												// $total_trip_cost = 0;
											}
											$wallet_payment_update = $this->Passenger_Model->update ( $remaing_wallet_amount, array (
													'id' => $trip_details->passengerId 
											) );
											
											$current_amount = $passenger_details->walletAmount - $wallet_payment;
											$insert_wallet_transaction = array (
													'passengerId' => $passenger_details->id,
													'tripId' => $trip_id,
													'transactionAmount' => $wallet_payment,
													'previousAmount' => $passenger_details->walletAmount,
													'currentAmount' => $current_amount,
													'transactionStatus' => 'Success',
													'transactionType' => Transaction_Type_Enum::TRIP,
													'transactionFrom' => Transaction_From_Enum::WALLET_ACCOUNT,
													'transactionMode' => Transaction_Mode_Enum::DEBIT 
											);
											$wallet_transaction_id = $this->Passenger_Transaction_Details_Model->insert ( $insert_wallet_transaction );
										}
										if ($admin_amount) {
											// update the driver transaction by debiting the admin commisiion charge
											$current_driver_wallet_amount = $this->Driver_Model->getById ( $trip_details->driverId );
											$insert_driver_transaction = array (
													'driverId' => $trip_details->driverId,
													'tripId' => $trip_id,
													'transactionAmount' => $admin_amount,
													'previousAmount' => $current_driver_wallet_amount->driverWallet,
													'currentAmount' => ($current_driver_wallet_amount->driverWallet - $admin_amount),
													'transactionStatus' => 'Success',
													'transactionType' => Transaction_Type_Enum::TRIP_COMMISSION,
													'transactionFrom' => Transaction_From_Enum::WALLET_ACCOUNT,
													'transactionMode' => Transaction_Mode_Enum::DEBIT 
											);
											$update_driver_transaction = $this->Driver_Transaction_Details_Model->insert ( $insert_driver_transaction );
											$remaining_driver_wallet = $current_driver_wallet_amount->driverWallet - $admin_amount;
											$update_driver_wallet = $this->Driver_Model->update ( array (
													'driverWallet' => $remaining_driver_wallet 
											), array (
													'id' => $trip_details->driverId 
											) );
											// @todo update driver dispath details table
											$driver_dispatch_exists = NULL;
											$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
													'driverId' => $trip_details->driverId 
											) );
											
											if ($driver_dispatch_exists) {
												
												$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
														'walletBalance' => $remaining_driver_wallet 
												), array (
														'driverId' => $trip_details->driverId 
												) );
											} else {
												$insert_dispatch_data = array (
														'driverId' => $trip_details->driverId,
														'walletBalance' => $remaining_driver_wallet 
												);
												$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
											}
											
											// end driver dispath details table
										}
										if ($promo_discount) {
											// get current wallet amount of driver to credit promocode ammount
											$current_driver_wallet_amount = $this->Driver_Model->getById ( $trip_details->driverId );
											
											// update the driver transaction by credit promocode ammount
											$insert_driver_transaction = array (
													'driverId' => $trip_details->driverId,
													'tripId' => $trip_id,
													'transactionAmount' => $promo_discount,
													'previousAmount' => $current_driver_wallet_amount->driverCredit,
													'currentAmount' => ($current_driver_wallet_amount->driverCredit + $promo_discount),
													'transactionStatus' => 'Success',
													'transactionType' => Transaction_Type_Enum::PROMOCODE,
													'transactionFrom' => Transaction_From_Enum::CREDIT_ACCOUNT,
													'transactionMode' => Transaction_Mode_Enum::CREDIT 
											);
											$update_driver_transaction = $this->Driver_Transaction_Details_Model->insert ( $insert_driver_transaction );
											$remaining_driver_wallet = $current_driver_wallet_amount->driverCredit + $promo_discount;
											$update_driver_wallet = $this->Driver_Model->update ( array (
													'driverCredit' => $remaining_driver_wallet 
											), array (
													'id' => $trip_details->driverId 
											) );
										}
										if ($wallet_payment) {
											// get current wallet amount of driver to credit wallet payment ammount
											$current_driver_wallet_amount = $this->Driver_Model->getById ( $trip_details->driverId );
											
											// update the driver transaction by credit promocode ammount
											$insert_driver_transaction = array (
													'driverId' => $trip_details->driverId,
													'tripId' => $trip_id,
													'transactionAmount' => $wallet_payment,
													'previousAmount' => $current_driver_wallet_amount->driverCredit,
													'currentAmount' => ($current_driver_wallet_amount->driverCredit + $wallet_payment),
													'transactionStatus' => 'Success',
													'transactionType' => Transaction_Type_Enum::TRIP_WALLET,
													'transactionFrom' => Transaction_From_Enum::CREDIT_ACCOUNT,
													'transactionMode' => Transaction_Mode_Enum::CREDIT 
											);
											$update_driver_transaction = $this->Driver_Transaction_Details_Model->insert ( $insert_driver_transaction );
											$remaining_driver_wallet = $current_driver_wallet_amount->driverCredit + $wallet_payment;
											$update_driver_wallet = $this->Driver_Model->update ( array (
													'driverCredit' => $remaining_driver_wallet 
											), array (
													'id' => $trip_details->driverId 
											) );
										}
										
										$trip_status = NULL;
										$notification_status = NULL;
										if ($trip_details->paymentMode == Payment_Mode_Enum::WALLET || $trip_details->paymentMode == Payment_Mode_Enum::CASH) {
											$trip_status = Trip_Status_Enum::TRIP_COMPLETED;
											$notification_status = Trip_Status_Enum::NO_NOTIFICATION;
										} else if ($trip_details->paymentMode == Payment_Mode_Enum::AGD) {
											$trip_status = Trip_Status_Enum::WAITING_FOR_PAYMENT;
											$notification_status = Trip_Status_Enum::WAITING_FOR_PAYMENT;
										} else if ($trip_details->paymentMode == Payment_Mode_Enum::INVOICE) {
											$trip_status = Trip_Status_Enum::TRIP_COMPLETED;
											$notification_status = Trip_Status_Enum::TRIP_COMPLETED;
										}
										
										if ($trip_details->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYMENT || $trip_details->tripStatus == Trip_Status_Enum::NO_NOTIFICATION) {
											$trip_status_update = $this->Trip_Details_Model->update ( array (
													'tripStatus' => $trip_status,
													'notificationStatus' => $notification_status 
											), array (
													'id' => $trip_id 
											) );
											
											$trip_request_status_update = $this->Taxi_Request_Details_Model->update ( array (
													'taxiRequestStatus' => Taxi_Request_Status_Enum::COMPLETED_TRIP,
													'selectedDriverId' => 0 
											), array (
													'tripId' => $trip_id 
											) );
											// get last inserted shift id for particular driver
											$get_last_shift_id = $this->Driver_Shift_History_Model->getOneByKeyValueArray ( array (
													'driverId' => $trip_details->driverId 
											), 'id DESC' );
											$driver_shift_status_update = $this->Driver_Shift_History_Model->update ( array (
													'availabilityStatus' => Taxi_Available_Status_Enum::FREE 
											), array (
													'id' => $get_last_shift_id->id 
											) );
										}
										// response data
										$response_data = array (
												"total_trip_cost" => $total_trip_charge,
												"pickup_location" => $trip_details->pickupLocation,
												"invoice_no" => $invoice_no,
												"trip_id" => $trip_id,
												"is_driver_rated" => ($trip_details->driverRating) ? TRUE : FALSE 
										);
										
										//
										// send sms & email to passenger
										
										/*
										 * if (SMS && $transaction_insert_id) {
										 * // SMS To passenger
										 *
										 * if ($trip_details->paymentMode == Payment_Mode_Enum::PAYTM) {
										 * $message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
										 * 'title' => 'complete_trip_wallet'
										 * ) );
										 * } else {
										 * $message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
										 * 'title' => 'complete_trip'
										 * ) );
										 * }
										 * $message_data = $message_details->content;
										 * $message_data = str_replace ( "##USERNAME##", $driver_logged_status->firstName . ' ' . $driver_logged_status->lastName, $message_data );
										 * $message_data = str_replace ( "##FARE##", $total_trip_cost, $message_data );
										 *
										 * if ($trip_details->paymentMode == Payment_Mode_Enum::PAYTM) {
										 * $message_data = str_replace ( "##PAID##", $total_trip_cost + $parking_charge + $toll_charge, $message_data );
										 * }
										 *
										 *
										 * $message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
										 * 'title' => 'new_complete_trip_sms'
										 * ) );
										 * $message_data = $message_details->content;
										 * $message_data = str_replace ( "##BOOKINGID##", $trip_id, $message_data );
										 * $message_data = str_replace ( "##STARTDATETIME##", date ( 'Y-m-d H:i:s', $actual_pickup_time ), $message_data );
										 * $message_data = str_replace ( "##ENDDATETIME##", date ( 'Y-m-d H:i:s', $drop_time ), $message_data );
										 * // print_r($message);exit;
										 * if ($passenger_details) {
										 * if ($passenger_details->mobile) {
										 *
										 * $this->Common_Api_Webservice_Model->sendSMS ( $passenger_details->mobile, $message_data );
										 * }
										 * }
										 * // print_r($message);exit;
										 * if ($passenger_details) {
										 * if ($passenger_details->mobile) {
										 *
										 * $this->Common_Api_Webservice_Model->sendSMS ( $passenger_details->mobile, $message_data );
										 * }
										 * }
										 * // SMS TO driver
										 * /*
										 * $message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
										 * 'title' => 'complete_trip_driver_sms'
										 * ) );
										 * $customer_care_no = ($trip_details->bookingCityId == 1) ? CUSTOMERCARE_MUMBAI : ($trip_details->bookingCityId == 2) ? CUSTOMERCARE_BANGALORE : ($trip_details->bookingCityId == 3) ? CUSTOMERCARE_PUNE : CUSTOMERCARE_DEFAULT;
										 * $message_data = $message_details->content;
										 * $message_data = str_replace ( "##CUSTOMERCARE##", $customer_care_no, $message_data );
										 * $message_data = str_replace ( "##FARE##", $total_trip_cost, $message_data );
										 * $message_data = str_replace ( "##COLLECT##", $total_trip_cost + $parking_charge + $toll_charge, $message_data );
										 *
										 * // print_r($message);exit;
										 * if ($driver_logged_status->mobile) {
										 *
										 * $this->Common_Api_Webservice_Model->sendSMS ( $driver_logged_status->mobile, $message_data );
										 * // this SMS is sending to below numbers on request through WhatsApp from Sidhanth on 10/01/2017
										 * $this->Common_Api_Webservice_Model->sendSMS ( '8451976667', $message_data );
										 * // this SMS is sending to below numbers on request through WhatsApp from Sidhanth on 10/01/2017
										 * }
										 *
										 * }
										 */
										/*
										 * $bill_details = $this->Driver_Api_Webservice_Model->getBillDetails ( $transaction_insert_id );
										 * $from = SMTP_EMAIL_ID;
										 * $to = ($passenger_details) ? $passenger_details->email : NULL;
										 * $subject = "Your " . date ( 'l', strtotime ( $bill_details [0]->actualPickupDatetime ) ) . " trip with Zuver Rs." . $bill_details [0]->totalTripCost;
										 *
										 * $data = array (
										 * 'passengerFullName' => ($bill_details [0]->passengerFullName) ? $bill_details [0]->passengerFullName : '',
										 * 'passengerEmail' => ($bill_details [0]->passengerEmail) ? $bill_details [0]->passengerEmail : '',
										 * 'driverFullName' => ($bill_details [0]->driverFullName) ? $bill_details [0]->driverFullName : '',
										 * 'invoiceNo' => ($bill_details [0]->invoiceNo) ? $bill_details [0]->invoiceNo : '',
										 * 'invoiceDatetime' => ($bill_details [0]->invoiceDatetime) ? date ( 'd M, Y, h:i a', strtotime ( $bill_details [0]->invoiceDatetime ) ) : '',
										 * 'actualPickupDatetime' => ($bill_details [0]->actualPickupDatetime) ? date ( 'd M, Y, h:i a', strtotime ( $bill_details [0]->actualPickupDatetime ) ) : '',
										 * 'dropDatetime' => ($bill_details [0]->dropDatetime) ? date ( 'd M, Y, h:i a', strtotime ( $bill_details [0]->dropDatetime ) ) : '',
										 * 'bookedDatetime' => ($bill_details [0]->bookedDatetime) ? date ( 'd M, Y, h:i a', strtotime ( $bill_details [0]->bookedDatetime ) ) : '',
										 * 'tripType' => ($bill_details [0]->tripType) ? $bill_details [0]->tripType : '',
										 * 'paymentMode' => ($bill_details [0]->paymentMode) ? $bill_details [0]->paymentMode : '',
										 * 'totalTripCost' => ($bill_details [0]->totalTripCost) ? round ( $bill_details [0]->totalTripCost, 2 ) : '00.00',
										 * 'totalCash' => ($bill_details [0]->totalTripCost - $bill_details [0]->walletPaymentAmount) ? round ( $bill_details [0]->totalTripCost - $bill_details [0]->walletPaymentAmount, 2 ) : '00.00',
										 * 'companyTax' => ($bill_details [0]->companyTax) ? round ( $bill_details [0]->companyTax, 2 ) : '00.00',
										 * 'taxPercentage' => $tax,
										 * 'convenienceCharge' => ($bill_details [0]->convenienceCharge) ? round ( $bill_details [0]->convenienceCharge, 2 ) : '00.00',
										 * 'travelCharge' => ($bill_details [0]->travelCharge) ? round ( $bill_details [0]->travelCharge, 2 ) : '00.00',
										 * 'parkingCharge' => ($bill_details [0]->parkingCharge) ? round ( $bill_details [0]->parkingCharge, 2 ) : '00.00',
										 * 'tollCharge' => ($bill_details [0]->tollCharge) ? round ( $bill_details [0]->tollCharge, 2 ) : '00.00',
										 * 'promoDiscountAmount' => ($bill_details [0]->promoDiscountAmount) ? round ( $bill_details [0]->promoDiscountAmount, 2 ) : '00.00',
										 * 'otherDiscountAmount' => ($bill_details [0]->otherDiscountAmount) ? round ( $bill_details [0]->otherDiscountAmount, 2 ) : '00.00',
										 * 'walletPaymentAmount' => ($bill_details [0]->walletPaymentAmount) ? round ( $bill_details [0]->walletPaymentAmount, 2 ) : '00.00',
										 * 'travelHoursMinutes' => ($bill_details [0]->travelledPeriod) ? $bill_details [0]->travelledPeriod : '',
										 * 'otherCharge' => ($bill_details [0]->otherCharge) ? round ( $bill_details [0]->otherCharge, 2 ) : '00.00'
										 * );
										 * $message_data = $this->load->view ( 'emailtemplate/tripcomplete-mail', $data, TRUE );
										 *
										 * if (SMTP && $transaction_insert_id) {
										 *
										 * if ($to) {
										 * $this->Common_Api_Webservice_Model->sendEmail ( $to, $subject, $message_data );
										 * } else {
										 * // B2B completed trip invoice to be send to 'info@zuver.in' requested by Ayush through email on 10/01/2017
										 * $this->Common_Api_Webservice_Model->sendEmail ( 'info@zuver.in', $subject, $message_data );
										 * // B2B completed trip invoice to be send to 'info@zuver.in' requested by Ayush through email on 10/01/2017
										 * }
										 * } else {
										 *
										 * // To send HTML mail, the Content-type header must be set
										 * $headers = 'MIME-Version: 1.0' . "\r\n";
										 * $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
										 * // Additional headers
										 * $headers .= 'From: Zuver<' . $from . '>' . "\r\n";
										 * $headers .= 'To: <' . $to . '>' . "\r\n";
										 * if (! filter_var ( $to, FILTER_VALIDATE_EMAIL ) === false) {
										 * mail ( $to, $subject, $message_data, $headers );
										 * }
										 *
										 * }
										 */
										// send sms & email to passenger
										
										// @todo update driver dispath details table
										$driver_dispatch_exists = NULL;
										$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
												'driverId' => $trip_details->driverId 
										) );
										if ($driver_dispatch_exists) {
											
											$week_completed_count = NULL;
											$week_completed_count = $this->Driver_Api_Webservice_Model->getWeekCompletedTripCount ( $trip_details->driverId );
											if ($week_completed_count) {
												$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
														'weekCompletedTrip' => $week_completed_count [0]->weekCompletedTripCount,
														'totalCompletedTrip' => ($driver_dispatch_exists->totalCompletedTrip + 1),
														'consecutiveRejectCount' => 0,
														'availabilityStatus' => Taxi_Available_Status_Enum::FREE 
												), array (
														'driverId' => $trip_details->driverId 
												) );
											} else {
												$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
														'weekCompletedTrip' => 1,
														'totalCompletedTrip' => ($driver_dispatch_exists->totalCompletedTrip + 1),
														'consecutiveRejectCount' => 0,
														'availabilityStatus' => Taxi_Available_Status_Enum::FREE 
												), array (
														'driverId' => $trip_details->driverId 
												) );
											}
										}
										// @update the week completed trip count
										// end driver dispath details table
										// send FCM/GCM notification to passenger trip ended waiting for payment
										$this->tripNotification ( $trip_id, $trip_details->passengerId, User_Type_Enum::PASSENGER, $msg_data ['trip_completed'], 5 );
										
										if ($trip_details->paymentMode == Payment_Mode_Enum::AGD) {
											$message = array (
													"message" => $msg_data ['trip_finalize_waiting_paytm_payment'],
													"details" => $response_data,
													"status" => 1 
											);
										} else {
											
											$message = array (
													"message" => $msg_data ['trip_finalize_success'],
													"details" => $response_data,
													"status" => 1 
											);
										}
									} else {
										$message = array (
												"message" => $msg_data ['trip_finalize_failed'],
												"details" => $response_data,
												"status" => - 1 
										);
									}
								} else {
									if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::PASSENGER_CANCELLED) {
										$message = array (
												"message" => $msg_data ['trip_reject_passenger'],
												"status" => - 1 
										);
									} else if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::COMPLETED_TRIP) {
										$message = array (
												"message" => $msg_data ['trip_completed'],
												"details" => $response_data,
												"status" => - 1 
										);
									} else if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::AVAILABLE_TRIP) {
										$message = array (
												"message" => $msg_data ['trip_not_started'],
												"status" => - 1 
										);
									} else if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::DRIVER_REJECTED) {
										$message = array (
												"message" => $msg_data ['trip_reject_driver'],
												"status" => - 1 
										);
									}
								}
							} else {
								$message = array (
										"message" => $msg_data ['invalid_trip_request'],
										"status" => - 1 
								);
							}
						} else {
							$message = array (
									"message" => $msg_data ['invalid_trip'],
									"status" => - 1 
							);
						}
					} else {
						$message = array (
								"message" => $msg_data ['driver_login_failed'],
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['invalid_trip_finalize_data'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	public function locationUpdateCron_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$driver_logged_status = NULL;
		$check_trip_avilablity = NULL;
		$trip_details = NULL;
		$rate_card_details = array ();
		$rate_card_distance_slab_details = array ();
		$rate_card_time_slab_details = array ();
		$rate_card_surge_slab_details = array ();
		$convenience_charge = 0;
		$surge_charge = 0;
		$surge_percentage = 0;
		$promo_discount_charge = 0;
		$promo_discount_percentage = 0;
		
		$response_data = array ();
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$driver_id = NULL;
		$trip_id = Null;
		$latitude = NULL;
		$longitude = NULL;
		$status = NULL;
		$app_version = 0;
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$driver_id = $post_data->driver_id;
		if (array_key_exists ( 'trip_id', $post_data )) {
			$trip_id = $post_data->trip_id;
		}
		if (array_key_exists ( 'app_version', $post_data )) {
			$app_version = $post_data->app_version;
		}
		$status = $post_data->status;
		$latitude = $post_data->latitude;
		$longitude = $post_data->longitude;
		$bearing = $post_data->bearing;
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual functionality
				
				if ($driver_id) {
					// get the trip details
					if ($trip_id) {
						$trip_details = $this->Common_Api_Webservice_Model->getTripDetails ( $trip_id );
					}
					
					$driver_logged_status = $this->Driver_Model->getOneByKeyValueArray ( array (
							'id' => $driver_id,
							'status' => Status_Type_Enum::ACTIVE,
							'loginStatus' => Status_Type_Enum::ACTIVE 
					) );
					if ($driver_logged_status) {
						
						// update the driver current latitude & longitude in driver details table
						$driver_update = $this->Driver_Model->update ( array (
								'currentLatitude' => $latitude,
								'currentLongitude' => $longitude,
								'currentBearing' => $bearing,
								'appVersion' => $app_version,
								'isNotified' => Status_Type_Enum::INACTIVE 
						), array (
								'id' => $driver_id 
						) );
						// @todo update driver dispath details table
						$driver_dispatch_exists = NULL;
						$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
								'driverId' => $driver_id 
						) );
						if ($driver_dispatch_exists) {
							$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
									'driverLatitude' => $latitude,
									'driverLongitude' => $longitude 
							), array (
									'driverId' => $driver_id 
							) );
						} else {
							$insert_dispatch_data = array (
									'driverId' => $driver_id,
									'driverLatitude' => $latitude,
									'driverLongitude' => $longitude 
							);
							$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
						}
						
						// end driver dispath details table
						// check trip avilable for driver/any in progress trip before logout
						$check_trip_avilablity = $this->Taxi_Request_Details_Model->getOneByKeyValueArray ( array (
								'selectedDriverId' => $driver_id 
						), 'id DESC' );
						
						if ($check_trip_avilablity) {
							
							// get the trip details
							if ($check_trip_avilablity->tripId) {
								$trip_details = $this->Common_Api_Webservice_Model->getTripDetails ( $check_trip_avilablity->tripId );
							}
							
							if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::DRIVER_ACCEPTED && $check_trip_avilablity->taxiRequestStatus != Taxi_Request_Status_Enum::COMPLETED_TRIP && $check_trip_avilablity->taxiRequestStatus != Taxi_Request_Status_Enum::DRIVER_REJECTED) {
								
								if ($trip_details) {
									
									// update the temp trip tracking info if driver in the trip
									$temp_trip_tracking_insert = $this->Trip_Temp_Tracking_Details_Model->insert ( array (
											'tripId' => $trip_details [0]->tripId,
											'latitude' => $latitude,
											'longitude' => $longitude 
									) );
									
									$entity_config_details = $this->Entity_Config_Model->getOneByKeyValueArray ( array (
											'entityId' => $trip_details [0]->entityId 
									) );
									$promocode_details = $this->Common_Api_Webservice_Model->checkPromocodeAvailablity ( $trip_details [0]->promoCode, $trip_details [0]->passengerId, $trip_details [0]->bookingZoneId, $trip_details [0]->bookingDropZoneId, $trip_details [0]->entityId );
									if ($promocode_details && $trip_details [0]->promoCode) {
										$promo_discount_charge = $promocode_details [0]->promoDiscountAmount2;
										$promo_discount_percentage = $promocode_details [0]->promoDiscountAmount;
										/*
										 * if ($promocode_details [0]->promoDiscountType == Payment_Type_Enum::AMOUNT) {
										 * $promo_discount_charge = $promocode_details [0]->promoDiscountAmount;
										 * } else if ($promocode_details [0]->promoDiscountType == Payment_Type_Enum::PERCENTAGE) {
										 * $promo_discount_percentage = $promocode_details [0]->promoDiscountAmount;
										 * }
										 * if ($promocode_details [0]->promoDiscountType2 == Payment_Type_Enum::AMOUNT) {
										 * $promo_discount_charge = $promocode_details [0]->promoDiscountAmount2;
										 * } else if ($promocode_details [0]->promoDiscountType2 == Payment_Type_Enum::PERCENTAGE) {
										 * $promo_discount_percentage = $promocode_details [0]->promoDiscountAmount2;
										 * }
										 */
									}
									
									$rate_card_details = $this->Rate_Card_Details_Model->getOneByKeyValueArray ( array (
											'zoneId' => $trip_details [0]->bookingZoneId,
											'entityId' => $trip_details [0]->entityId,
											'taxiCategoryType' => $trip_details [0]->taxiCategoryType,
											'status' => Status_Type_Enum::ACTIVE,
											'isDeleted' => Status_Type_Enum::INACTIVE 
									) );
									if (! $rate_card_details) {
										$rate_card_details = $this->Rate_Card_Details_Model->getOneByKeyValueArray ( array (
												'zoneId' => Status_Type_Enum::INACTIVE,
												'entityId' => $trip_details [0]->entityId,
												'taxiCategoryType' => $trip_details [0]->taxiCategoryType,
												'status' => Status_Type_Enum::ACTIVE,
												'isDeleted' => Status_Type_Enum::INACTIVE 
										) );
									}
									if (! $rate_card_details) {
										$rate_card_details = $this->Rate_Card_Details_Model->getOneByKeyValueArray ( array (
												'zoneId' => $trip_details [0]->bookingZoneId,
												'entityId' => Status_Type_Enum::INACTIVE,
												'taxiCategoryType' => $trip_details [0]->taxiCategoryType,
												'status' => Status_Type_Enum::ACTIVE,
												'isDeleted' => Status_Type_Enum::INACTIVE 
										) );
									}
									if (! $rate_card_details) {
										$rate_card_details = $this->Rate_Card_Details_Model->getOneByKeyValueArray ( array (
												'zoneId' => Status_Type_Enum::INACTIVE,
												'entityId' => Status_Type_Enum::INACTIVE,
												'taxiCategoryType' => $trip_details [0]->taxiCategoryType,
												'status' => Status_Type_Enum::ACTIVE,
												'isDeleted' => Status_Type_Enum::INACTIVE 
										) );
									}
									
									if ($rate_card_details) {
										$actual_pickup_time = $trip_details [0]->pickupDatetime;
										if (TimeIsBetweenTwoTimes ( DAY_START_TIME, DAY_END_TIME, $actual_pickup_time )) {
											
											$convenience_charge += $rate_card_details->dayConvenienceCharge;
										} else if (TimeIsBetweenTwoTimes ( NIGHT_START_TIME, NIGHT_END_TIME, $actual_pickup_time )) {
											$convenience_charge += $rate_card_details->nightConvenienceCharge;
										}
										
										$rate_card_distance_slab_details = $this->Rate_Card_Slab_Details_Model->getByKeyValueArray ( array (
												'rateCardId' => $rate_card_details->id,
												'slabType' => Slab_Type_Enum::DISTANCE 
										) );
										$rate_card_time_slab_details = $this->Rate_Card_Slab_Details_Model->getByKeyValueArray ( array (
												'rateCardId' => $rate_card_details->id,
												'slabType' => Slab_Type_Enum::TIME 
										) );
										$rate_card_surge_slab_details = $this->Rate_Card_Slab_Details_Model->getByKeyValueArray ( array (
												'rateCardId' => $rate_card_details->id,
												'slabType' => Slab_Type_Enum::SURGE 
										) );
										if ($rate_card_surge_slab_details) {
											foreach ( $rate_card_surge_slab_details as $rate_surge ) {
												$slab_start_time = $rate_surge->slabStart;
												$slab_end_time = $rate_surge->slabEnd;
												// $user_st=$user_et="2017-08-02 09:29:12";
												if (((strtotime ( $actual_pickup_time ) >= strtotime ( $slab_start_time )) && (strtotime ( $actual_pickup_time ) <= strtotime ( $slab_end_time ))) || (strtotime ( $actual_pickup_time ) >= strtotime ( $slab_start_time )) && (strtotime ( $actual_pickup_time ) <= strtotime ( $slab_end_time ))) {
													if ($rate_surge->slabChargeType == Payment_Type_Enum::AMOUNT) {
														$surge_charge = $rate_surge->slabCharge;
														break;
													} else if ($rate_surge->slabChargeType == Payment_Type_Enum::PERCENTAGE) {
														$surge_percentage = $rate_surge->slabCharge;
														break;
													}
												}
											}
										}
									}
									$message = array (
											"message" => $msg_data ['driver_location_update'],
											"status" => 1,
											'trip_details' => array (
													'trip_id' => $trip_id,
													'job_card_id' => $trip_details [0]->jobCardId,
													'customer_reference_code' => $trip_details [0]->customerReferenceCode,
													'pickup_location' => $trip_details [0]->pickupLocation,
													'pickup_latitude' => $trip_details [0]->pickupLatitude,
													'pickup_longitude' => $trip_details [0]->pickupLongitude,
													'pickup_time' => $trip_details [0]->pickupDatetime,
													'drop_location' => $trip_details [0]->dropLocation,
													'drop_latitude' => $trip_details [0]->dropLatitude,
													'drop_longitude' => $trip_details [0]->dropLongitude,
													'drop_time' => $trip_details [0]->dropDatetime,
													'land_mark' => $trip_details [0]->landmark,
													'taxi_rating' => $trip_details [0]->passengerRating,
													'taxi_comments' => $trip_details [0]->passengerComments,
													'trip_type' => $trip_details [0]->tripType,
													'trip_type_name' => $trip_details [0]->tripTypeName,
													'payment_mode' => $trip_details [0]->paymentMode,
													'payment_mode_name' => $trip_details [0]->paymentModeName,
													'trip_status' => $trip_details [0]->tripStatus,
													'trip_status_name' => $trip_details [0]->tripStatusName,
													'promocode' => $trip_details [0]->promoCode,
													'is_driver_rated' => ($trip_details [0]->driverRating) ? 1 : 0,
													'is_admin_booked' => ($trip_details [0]->bookedFrom == Signup_Type_Enum::BACKEND) ? 1 : 0 
											),
											'passenger_details' => array (
													'passenger_id' => $trip_details [0]->passengerId,
													'passenger_firstname' => $trip_details [0]->passengerFirstName,
													'passenger_lastname' => $trip_details [0]->passengerLastName,
													'passenger_mobile' => $trip_details [0]->passengerMobile,
													'passenger_latitude' => ($trip_details [0]->bookedFrom == Signup_Type_Enum::BACKEND) ? $trip_details [0]->pickupLatitude : $trip_details [0]->passengerCurrentLatitude,
													'passenger_longitude' => ($trip_details [0]->bookedFrom == Signup_Type_Enum::BACKEND) ? $trip_details [0]->pickupLongitude : $trip_details [0]->passengerCurrentLongitude,
													'passenger_profile_image' => ($trip_details [0]->passengerProfileImage) ? (passenger_content_url ( $trip_details [0]->passengerId . '/' . $trip_details [0]->passengerProfileImage )) : image_url ( 'app/user.png' ) 
											),
											'driver_details' => array (
													'driver_id' => $trip_details [0]->driverId,
													'driver_firstname' => $trip_details [0]->driverFirstName,
													'driver_lastname' => $trip_details [0]->driverLastName,
													'driver_mobile' => $trip_details [0]->driverMobile,
													'driver_profile_image' => ($trip_details [0]->driverProfileImage) ? (driver_content_url ( $trip_details [0]->driverId . '/' . $trip_details [0]->driverProfileImage )) : image_url ( 'app/user.png' ),
													'driver_language' => $trip_details [0]->driverLanguagesKnown,
													'driver_experience' => $trip_details [0]->driverExperience,
													'driver_age' => $trip_details [0]->driverDob,
													'driver_latitute' => $trip_details [0]->currentLatitude,
													'driver_longtitute' => $trip_details [0]->currentLongitude,
													'driver_status' => $trip_details [0]->availabilityStatus 
											),
											'taxi_details' => array (
													'taxi_id' => $trip_details [0]->taxiId,
													'taxi_name' => $trip_details [0]->taxiName,
													'taxi_model' => $trip_details [0]->taxiModel,
													'taxi_manufacturer' => $trip_details [0]->taxiManufacturer,
													'taxi_registration_no' => $trip_details [0]->taxiRegistrationNo,
													'taxi_colour' => $trip_details [0]->taxiColour 
											),
											'fare_utility' => array (
													'zone_id' => $trip_details [0]->bookingZoneId,
													'entity_id' => $trip_details [0]->entityId,
													'booking_charge' => ($trip_details [0]->bookedFrom == Signup_Type_Enum::BACKEND) ? $entity_config_details->adminBookingCharge : 00.00,
													'penalty_charge' => ($trip_details [0]->penaltyTripCount) ? ($trip_details [0]->penaltyTripCount * $entity_config_details->passengerRejectionCharge) : 00.00,
													'admin_commission' => $entity_config_details->adminCommission,
													'tax_percentage' => $entity_config_details->tax,
													'convenience_charge' => $convenience_charge,
													'surge_charge' => $surge_charge,
													'surge_percentage' => $surge_percentage,
													'promo_discount_charge' => $promo_discount_charge,
													'promo_discount_percentage' => $promo_discount_percentage,
													'passenger_wallet' => ($trip_details [0]->paymentMode == Payment_Mode_Enum::WALLET) ? $trip_details [0]->walletAmount : 0.00,
													'current_date_time' => getCurrentDateTime () 
											),
											'rate_card_details' => $rate_card_details,
											'rate_card_distance_slab_details' => $rate_card_distance_slab_details,
											'rate_card_time_slab_details' => $rate_card_time_slab_details,
											'rate_card_surge_slab_details' => $rate_card_surge_slab_details 
									);
								}
							} else {
								if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::PASSENGER_CANCELLED) {
									
									// if ($status == Taxi_Available_Status_Enum::BUSYSTATUS) {
									// to get last shift in status of driver before updating the shift out status
									$last_driver_shift_id = $this->Driver_Shift_History_Model->getOneByKeyValueArray ( array (
											'driverId' => $trip_details [0]->driverId 
									), 'id DESC' );
									
									$driver_shift_insert_id = $this->Driver_Shift_History_Model->update ( array (
											'availabilityStatus' => Taxi_Available_Status_Enum::FREE 
									), array (
											'id' => $last_driver_shift_id->id,
											'driverId' => $trip_details [0]->driverId 
									) );
									$trip_request_status_update = $this->Taxi_Request_Details_Model->update ( array (
											'taxiRequestStatus' => Taxi_Request_Status_Enum::PASSENGER_CANCELLED,
											'selectedDriverId' => 0 
									), array (
											'tripId' => $check_trip_avilablity->tripId 
									) );
									// @todo update driver dispath details table
									$driver_dispatch_exists = NULL;
									$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
											'driverId' => $trip_details [0]->driverId 
									) );
									if ($driver_dispatch_exists) {
										$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
												'availabilityStatus' => Taxi_Available_Status_Enum::FREE 
										), array (
												'driverId' => $trip_details [0]->driverId 
										) );
									} else {
										$insert_dispatch_data = array (
												'driverId' => $trip_details [0]->driverId,
												'availabilityStatus' => Taxi_Available_Status_Enum::FREE 
										);
										$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
									}
									
									// end driver dispath details table
									// }
									$trip_request_status_update = $this->Taxi_Request_Details_Model->update ( array (
											'selectedDriverId' => 0 
									), array (
											'tripId' => $check_trip_avilablity->tripId 
									) );
									$message = array (
											"message" => $msg_data ['trip_reject_passenger'],
											"status" => 7 
									);
								} else if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::COMPLETED_TRIP) {
									$message = array (
											"message" => $msg_data ['trip_completed'],
											"status" => 10,
											'trip_details' => array (
													'trip_id' => $trip_id,
													'job_card_id' => $trip_details [0]->jobCardId,
													'pickup_location' => $trip_details [0]->pickupLocation,
													'pickup_latitude' => $trip_details [0]->pickupLatitude,
													'pickup_longitude' => $trip_details [0]->pickupLongitude,
													'pickup_time' => $trip_details [0]->actualPickupDatetime,
													'drop_location' => $trip_details [0]->dropLocation,
													'drop_latitude' => $trip_details [0]->dropLatitude,
													'drop_longitude' => $trip_details [0]->dropLongitude,
													'drop_time' => $trip_details [0]->dropDatetime,
													'land_mark' => $trip_details [0]->landmark,
													'taxi_rating' => $trip_details [0]->passengerRating,
													'taxi_comments' => $trip_details [0]->passengerComments,
													'trip_type' => $trip_details [0]->tripType,
													'trip_type_name' => $trip_details [0]->tripTypeName,
													'payment_mode' => $trip_details [0]->paymentMode,
													'payment_mode_name' => $trip_details [0]->paymentModeName,
													'trip_status' => $trip_details [0]->tripStatus,
													'trip_status_name' => $trip_details [0]->tripStatusName,
													'promocode' => $trip_details [0]->promoCode,
													'is_driver_rated' => ($trip_details [0]->driverRating) ? 1 : 0,
													'is_admin_booked' => ($trip_details [0]->bookedFrom == Signup_Type_Enum::BACKEND) ? 1 : 0 
											),
											'passenger_details' => array (
													'passenger_id' => $trip_details [0]->passengerId,
													'passenger_firstname' => $trip_details [0]->passengerFirstName,
													'passenger_lastname' => $trip_details [0]->passengerLastName,
													'passenger_mobile' => $trip_details [0]->passengerMobile,
													'passenger_latitude' => ($trip_details [0]->bookedFrom == Signup_Type_Enum::BACKEND) ? $trip_details [0]->pickupLatitude : $trip_details [0]->passengerCurrentLatitude,
													'passenger_longitude' => ($trip_details [0]->bookedFrom == Signup_Type_Enum::BACKEND) ? $trip_details [0]->pickupLongitude : $trip_details [0]->passengerCurrentLongitude,
													'passenger_profile_image' => ($trip_details [0]->passengerProfileImage) ? (passenger_content_url ( $trip_details [0]->passengerId . '/' . $trip_details [0]->passengerProfileImage )) : image_url ( 'app/user.png' ) 
											),
											'driver_details' => array (
													'driver_id' => $trip_details [0]->driverId,
													'driver_firstname' => $trip_details [0]->driverFirstName,
													'driver_lastname' => $trip_details [0]->driverLastName,
													'driver_mobile' => $trip_details [0]->driverMobile,
													'driver_profile_image' => ($trip_details [0]->driverProfileImage) ? (driver_content_url ( $trip_details [0]->driverId . '/' . $trip_details [0]->driverProfileImage )) : image_url ( 'app/user.png' ),
													'driver_language' => $trip_details [0]->driverLanguagesKnown,
													'driver_experience' => $trip_details [0]->driverExperience,
													'driver_age' => $trip_details [0]->driverDob,
													'driver_latitute' => $trip_details [0]->currentLatitude,
													'driver_longtitute' => $trip_details [0]->currentLongitude,
													'driver_status' => $trip_details [0]->availabilityStatus 
											),
											'taxi_details' => array (
													'taxi_id' => $trip_details [0]->taxiId,
													'taxi_name' => $trip_details [0]->taxiName,
													'taxi_model' => $trip_details [0]->taxiModel,
													'taxi_manufacturer' => $trip_details [0]->taxiManufacturer,
													'taxi_registration_no' => $trip_details [0]->taxiRegistrationNo,
													'taxi_colour' => $trip_details [0]->taxiColour 
											),
											
											'transaction_details' => array (
													'base_charge' => $trip_details [0]->baseCharge,
													'travel_charge' => $trip_details [0]->travelCharge,
													'parking_charge' => $trip_details [0]->parkingCharge,
													'tax_charge' => $trip_details [0]->taxCharge,
													'tax_percentage' => $trip_details [0]->taxPercentage,
													'toll_charge' => $trip_details [0]->tollCharge,
													'promo_discount' => $trip_details [0]->promoDiscountAmount,
													'wallet_payment' => $trip_details [0]->walletPaymentAmount,
													'travelled_period' => $trip_details [0]->travelledPeriod,
													'distance' => $trip_details [0]->travelledDistance,
													'total_trip_cost' => $trip_details [0]->totalTripCharge 
											) 
									);
								} else if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::AVAILABLE_TRIP) {
									if ($status == Taxi_Available_Status_Enum::FREESTATUS) {
										
										$trip_details = $this->Common_Api_Webservice_Model->getTripDetails ( $check_trip_avilablity->tripId );
										$mobile = NULL;
										$full_name = NULL;
										$passenger_details = $this->Passenger_Model->getById ( $trip_details [0]->passengerId );
										$driver_details = $this->Driver_Model->getById ( $trip_details [0]->driverId );
										$response_data = array (
												"message" => $msg_data ['driver_booking_request'],
												"status" => 1,
												'trip_details' => array (
														'trip_id' => $trip_details [0]->tripId,
														'job_card_id' => $trip_details [0]->jobCardId,
														'pickup_location' => $trip_details [0]->pickupLocation,
														'pickup_latitude' => $trip_details [0]->pickupLatitude,
														'pickup_longitude' => $trip_details [0]->pickupLongitude,
														'pickup_time' => $trip_details [0]->pickupDatetime,
														'drop_location' => $trip_details [0]->dropLocation,
														'drop_latitude' => $trip_details [0]->dropLatitude,
														'drop_longitude' => $trip_details [0]->dropLongitude,
														'drop_time' => $trip_details [0]->dropDatetime,
														'land_mark' => $trip_details [0]->landmark,
														'taxi_rating' => $trip_details [0]->passengerRating,
														'taxi_comments' => $trip_details [0]->passengerComments,
														'trip_type' => $trip_details [0]->tripType,
														'trip_type_name' => $trip_details [0]->tripTypeName,
														'payment_mode' => $trip_details [0]->paymentMode,
														'payment_mode_name' => $trip_details [0]->paymentModeName,
														'trip_status' => $trip_details [0]->tripStatus,
														'trip_status_name' => $trip_details [0]->tripStatusName,
														'promocode' => $trip_details [0]->promoCode,
														'is_driver_rated' => ($trip_details [0]->driverRating) ? TRUE : FALSE 
												),
												'passenger_details' => array (
														'passenger_id' => $trip_details [0]->passengerId,
														'passenger_firstname' => $trip_details [0]->passengerFirstName,
														'passenger_lastname' => $trip_details [0]->passengerLastName,
														'passenger_mobile' => $trip_details [0]->passengerMobile,
														'passenger_latitude' => $trip_details [0]->passengerCurrentLatitude,
														'passenger_longitude' => $trip_details [0]->passengerCurrentLongitude,
														'passenger_profile_image' => ($trip_details [0]->passengerProfileImage) ? (passenger_content_url ( $trip_details [0]->passengerId . '/' . $trip_details [0]->passengerProfileImage )) : image_url ( 'app/user.png' ) 
												),
												'driver_details' => array (
														'driver_id' => $trip_details [0]->driverId,
														'driver_firstname' => $trip_details [0]->driverFirstName,
														'driver_lastname' => $trip_details [0]->driverLastName,
														'driver_mobile' => $trip_details [0]->driverMobile,
														'driver_profile_image' => ($trip_details [0]->driverProfileImage) ? (driver_content_url ( $trip_details [0]->driverId . '/' . $trip_details [0]->driverProfileImage )) : image_url ( 'app/user.png' ),
														'driver_language' => $trip_details [0]->driverLanguagesKnown,
														'driver_experience' => $trip_details [0]->driverExperience,
														'driver_age' => $trip_details [0]->driverDob,
														'driver_latitute' => $trip_details [0]->currentLatitude,
														'driver_longtitute' => $trip_details [0]->currentLongitude,
														'driver_status' => $trip_details [0]->availabilityStatus 
												),
												'taxi_details' => array (
														'taxi_id' => $trip_details [0]->taxiId,
														'taxi_name' => $trip_details [0]->taxiName,
														'taxi_model' => $trip_details [0]->taxiModel,
														'taxi_manufacturer' => $trip_details [0]->taxiManufacturer,
														'taxi_registration_no' => $trip_details [0]->taxiRegistrationNo,
														'taxi_colour' => $trip_details [0]->taxiColour 
												),
												
												"estimated_time" => $trip_details [0]->estimatedTime,
												"distance_away" => number_format ( $trip_details [0]->estimatedDistance, 1 ),
												"calculate_distance_away" => number_format ( $trip_details [0]->estimatedDistance + 0.5, 1 ),
												"notification_time" => 15 
										);
										$message = array (
												"message" => $msg_data ['driver_location_update'],
												"trip_details" => $response_data,
												"status" => 5 
										);
									} else {
										$message = array (
												"message" => $msg_data ['driver_location_update'],
												"status" => 1 
										);
									}
								} else if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::DRIVER_REJECTED) {
									$message = array (
											"message" => $msg_data ['trip_dispatcher_cancel'],
											"status" => 10 
									);
									// check trip avilable for driver/any in progress trip before logout
									$trip_request_status_update = $this->Taxi_Request_Details_Model->update ( array (
											'selectedDriverId' => 0 
									), array (
											'tripId' => $trip_id 
									) );
								}
							}
						} else {
							if ($trip_details) {
								if ($trip_details [0]->tripStatus == Trip_Status_Enum::DRIVER_REJECTED_TRIP || $trip_details [0]->tripStatus == Trip_Status_Enum::CANCELLED_BY_DRIVER) {
									$message = array (
											"message" => $msg_data ['trip_dispatcher_cancel'],
											"status" => 10 
									);
								} else if ($trip_details [0]->tripStatus == Trip_Status_Enum::TRIP_COMPLETED) {
									$message = array (
											"message" => $msg_data ['trip_completed'],
											"status" => 7 
									);
								} else {
									$message = array (
											"message" => $msg_data ['driver_location_update'],
											"status" => 1 
									);
								}
							} else {
								$message = array (
										"message" => $msg_data ['driver_location_update'],
										"status" => 1 
								);
							}
						}
					} else {
						$message = array (
								"message" => $msg_data ['driver_login_failed'],
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['invalid_trip'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	public function tripHistoryList_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$driver_logged_status = NULL;
		$check_trip_avilablity = NULL;
		$driver_pending_trip_list = NULL;
		$driver_cancelled_trip_list = NULL;
		$driver_past_trip_list = NULL;
		
		$trip_history_list = array ();
		
		$response_data = array ();
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$driver_id = NULL;
		$start_offset = NULL;
		$end_limit = NULL;
		$category = NULL;
		$start_date = NULL;
		$end_date = NULL;
		
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$driver_id = $post_data->driver_id;
		$start_offset = $post_data->start_offset;
		if (array_key_exists ( 'end_limit', $post_data )) {
			$end_limit = $post_data->end_limit;
		}
		if (array_key_exists ( 'start_date', $post_data )) {
			$start_date = $post_data->start_date;
		}
		if (array_key_exists ( 'end_date', $post_data )) {
			$end_date = $post_data->end_date;
		}
		
		$category = $post_data->category;
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual functionality
				
				if ($driver_id) {
					
					$driver_logged_status = $this->Driver_Model->getOneByKeyValueArray ( array (
							'id' => $driver_id,
							'status' => Status_Type_Enum::ACTIVE,
							'loginStatus' => Status_Type_Enum::ACTIVE 
					) );
					if ($driver_logged_status) {
						if ($category == 1) {
							$driver_pending_trip_list = $this->Driver_Api_Webservice_Model->getDriverPendingTripList ( $driver_id, $start_offset, $end_limit, $start_date, $end_date );
							if ($driver_pending_trip_list) {
								
								foreach ( $driver_pending_trip_list as $list ) {
									
									$trip_history_list [] = array (
											'trip_id' => $list->tripId,
											'job_card_id' => $list->jobCardId,
											'pickup_location' => $list->pickupLocation,
											'pickup_time' => date ( 'd-m-Y h:i A', strtotime ( $list->pickupDatetime ) ),
											'drop_location' => $list->dropLocation,
											'crn' => $list->customerReferenceCode,
											'taxi_category' => $list->taxiCategoryTypeName,
											'travel_status' => $list->tripStatus,
											'travel_msg' => $list->tripStatusName 
									);
								}
							}
						}
						if ($category == 0) {
							$driver_cancelled_trip_list = $this->Driver_Api_Webservice_Model->getDriverCancelledTripList ( $driver_id, $start_offset, $end_limit, $start_date, $end_date );
							if ($driver_cancelled_trip_list) {
								
								foreach ( $driver_cancelled_trip_list as $list ) {
									
									$trip_history_list [] = array (
											'trip_id' => $list->tripId,
											'job_card_id' => $list->jobCardId,
											'pickup_location' => $list->pickupLocation,
											'pickup_time' => date ( 'd-m-Y h:i A', strtotime ( $list->pickupDatetime ) ),
											'drop_location' => $list->dropLocation,
											'crn' => $list->customerReferenceCode,
											'taxi_category' => $list->taxiCategoryTypeName,
											'total_trip_charge' => '',
											'travel_status' => $list->tripStatus,
											'travel_msg' => $list->tripStatusName 
									);
								}
							}
						}
						if ($category == - 1) {
							$driver_past_trip_list = $this->Driver_Api_Webservice_Model->getDriverPastTripList ( $driver_id, $start_offset, $end_limit, $start_date, $end_date );
							if ($driver_past_trip_list) {
								foreach ( $driver_past_trip_list as $list ) {
									
									$trip_history_list [] = array (
											'trip_id' => $list->tripId,
											'job_card_id' => $list->jobCardId,
											'pickup_location' => $list->pickupLocation,
											'pickup_time' => date ( 'd-m-Y h:i A', strtotime ( $list->actualPickupDatetime ) ),
											'drop_location' => $list->dropLocation,
											'drop_time' => date ( 'd-m-Y h:i A', strtotime ( $list->dropDatetime ) ),
											'invoice_no' => $list->invoiceNo,
											'crn' => $list->customerReferenceCode,
											'taxi_category' => $list->taxiCategoryTypeName,
											'total_trip_charge' => $list->totalTripCharge,
											'promo_discount' => $list->promoDiscountAmount,
											'admin_amount' => $list->adminAmount,
											'travelled_distance' => $list->travelledDistance,
											'travelled_period' => $list->travelledPeriod,
											'travel_msg' => $list->tripStatusName,
											'travel_status' => $list->tripStatus,
											'travel_msg' => $list->tripStatusName 
									);
								}
							}
						}
						// start Driver satistics data
						/*
						 * $driver_rejected_count = $this->Taxi_Rejected_Trip_Details_Model->getColumnByKeyValueArray ( 'driverId', array (
						 * 'driverId' => $driver_logged_status->id,
						 * 'createdDatetime >=' => getCurrentDate (),
						 * 'createdDatetime <=' => getCurrentDate ()
						 * ) );
						 *
						 * $driver_earning_details = $this->Driver_Api_Webservice_Model->getDriverEarningDetails ( $driver_logged_status->id, 0, $start_date, $end_date );
						 * $driver_today_earning_details = $this->Driver_Api_Webservice_Model->getDriverEarningDetails ( $driver_logged_status->id, 1 );
						 * // To calculate time driven on current date/today
						 * $total_amount = 0;
						 * $total_trip_amount = 0;
						 * $total_today_amount = 0;
						 * $time_result = '00:00';
						 * $actual_pickup_time = '';
						 * $drop_time = '';
						 * $hours = '';
						 * $minutes = '';
						 * $seconds = '';
						 * $date_difference = "";
						 * $total_differnce = "";
						 * foreach ( $driver_earning_details as $get_details ) {
						 * $actual_pickup_time = strtotime ( $get_details->actualPickupDatetime );
						 * $drop_time = strtotime ( $get_details->dropDatetime );
						 * // echo $actual_pickup_time;
						 * // echo '-';
						 * // echo $drop_time;
						 * // echo '<br>';
						 * $date_difference = abs ( $drop_time - $actual_pickup_time );
						 * $total_differnce += $date_difference;
						 * // to get total earned amount by the driver
						 * $total_amount += $get_details->driverEarning;
						 * $total_trip_amount += $get_details->totalTripCost;
						 * }
						 * foreach ( $driver_today_earning_details as $get_details ) {
						 * // to get total earned amount by the driver
						 * $total_today_amount += $get_details->driverEarning;
						 * }
						 * // $date_difference = $drop_time - $actual_pickup_time;
						 * $hours += floor ( (($total_differnce % 604800) % 86400) / 3600 );
						 * $minutes += floor ( ((($total_differnce % 604800) % 86400) % 3600) / 60 );
						 * $seconds += floor ( (((($total_differnce % 604800) % 86400) % 3600) % 60) );
						 * $time_result = $hours . ':' . $minutes;
						 * // To calculate time driven on current date/today
						 *
						 * $statistics = array (
						 * "drivername" => $driver_logged_status->firstName . ' ' . $driver_logged_status->lastName,
						 * "total_trips" => count ( $driver_earning_details ),
						 * "cancel_trips" => count ( $driver_rejected_count ),
						 * "total_amount" => round ( $total_trip_amount, 2 ),
						 * "overall_rejected_trips" => count ( $driver_rejected_count ),
						 * "today_earnings" => round ( $total_today_amount, 2 ),
						 * "shift_status" => Driver_Shift_Status_Enum::SHIFTIN,
						 * "total_hours" => $time_result,
						 * "status" => 1
						 * );
						 * $response_data ['driver_statistics'] = $statistics;
						 */
						// end Driver satistics data
						$response_data = $trip_history_list;
						if (count ( $response_data ) > 0) {
							// $message = $passengers_current;
							$message = array (
									"message" => $msg_data ['success'],
									"trip_history_list" => $response_data,
									"status" => 1 
							);
						} else {
							$message = array (
									"message" => $msg_data ['data_not_found'],
									"status" => - 1 
							);
						}
					} else {
						$message = array (
								"message" => $msg_data ['driver_login_failed'],
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['invalid_user'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	public function changePassword_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$user_exist = NULL;
		
		$password_status = NULL;
		$update_password = NULL;
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$driver_id = NULL;
		$old_password = NULL;
		$new_password = NULL;
		$confirm_password = NULL;
		
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$driver_id = $post_data->driver_id;
		$old_password = $post_data->old_password;
		$new_password = $post_data->new_password;
		$confirm_password = $post_data->confirm_password;
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual funtionality
				if ($driver_id != null) {
					// validation for checking $old_password, $new_password, $confirm_password data avilablity
					if ($old_password && $new_password && $confirm_password) {
						$user_exist = $this->Driver_Model->getById ( $driver_id );
						if ($user_exist) {
							$password = $user_exist->password;
							$raw_confirm_password = $confirm_password;
							$confirm_password = hash ( "sha256", $confirm_password );
							
							if ($password != hash ( "sha256", $old_password )) {
								$password_status = - 1;
							} else if ($new_password != $raw_confirm_password) {
								$password_status = - 2;
							} else if ($password == $confirm_password) {
								$password_status = - 3;
							} else {
								$update_password = $this->Driver_Model->update ( array (
										'password' => $confirm_password 
								), array (
										'id' => $user_exist->id 
								) );
								if ($update_password) {
									$password_status = 1;
								}
							}
						} else {
							$password_status = - 4;
						}
					}
					switch ($password_status) {
						case - 1 :
							$message = array (
									"message" => $msg_data ['incorrect_old_password'],
									"status" => - 1 
							);
							break;
						case - 2 :
							$message = array (
									"message" => $msg_data ['confirm_password'],
									"status" => - 1 
							);
							break;
						case - 3 :
							$message = array (
									"message" => $msg_data ['old_new_password_same'],
									"status" => - 1 
							);
							break;
						case 1 :
							$message = array (
									"message" => $msg_data ['password_success'],
									"status" => 1 
							);
							break;
						case - 4 :
							$message = array (
									"message" => $msg_data ['invalid_user'],
									"status" => - 1 
							);
							break;
					}
				} else {
					$message = array (
							"message" => $msg_data ['invalid_user'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	public function updatePassengerRating_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$user_exist = NULL;
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$driver_id = NULL;
		$trip_id = NULL;
		$ratings = NULL;
		$comments = NULL;
		
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$driver_id = $post_data->driver_id;
		$trip_id = $post_data->trip_id;
		$ratings = $post_data->ratings;
		$comments = $post_data->comments;
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual funtionality
				if ($trip_id != NULL) {
					if ($ratings) {
						
						$update_data = array (
								'driverRating' => $ratings,
								'driverComments' => $comments 
						);
						
						$update_result = $this->Trip_Details_Model->update ( $update_data, array (
								'id' => $trip_id 
						) );
						// To get driver taxi id
						$driver_taxi_id = $this->Driver_Taxi_Mapping_Model->getColumnByKeyValueArray ( 'taxiId', array (
								'driverId' => $driver_id 
						) );
						// debug($driver_taxi_id);
						if ($driver_taxi_id) {
							// To get taxi details
							$taxi_details = $this->Taxi_Details_Model->getById ( $driver_taxi_id [0]->taxiId );
							// $taxi_status=($taxi_details->status)?"Active":"Inactive";
							$response_data ['driver_details'] = array (
									'taxi_id' => $taxi_details->id,
									'taxi_name' => $taxi_details->name,
									'taxi_model' => $taxi_details->model,
									'taxi_manufacturer' => $taxi_details->manufacturer,
									'taxi_colour' => $taxi_details->colour,
									'taxi_status' => ($taxi_details->status) ? "Active" : "Inactive",
									'taxi_registration_no' => $taxi_details->registrationNo 
							);
							$driver_details = $this->Driver_Model->getById ( $driver_id );
							$driver_personal_details = $this->Driver_Personal_Details_Model->getOneByKeyValueArray ( array (
									'driverId' => $driver_id 
							) );
							$response_data ['driver_details'] ['driver_id'] = $driver_details->id;
							// $response_data ['taxi_details'] = $taxi_response_data;
							$response_data ['driver_details'] ['full_name'] = $driver_details->firstName . ' ' . $driver_details->lastName;
							$response_data ['driver_details'] ['email'] = $driver_details->email;
							$response_data ['driver_details'] ['mobile'] = $driver_details->mobile;
							$response_data ['driver_details'] ['driver_referral_code'] = $driver_details->driverCode;
							$response_data ['driver_details'] ['driver_license_no'] = $driver_details->licenseNo;
							$response_data ['driver_details'] ['driver_latitude'] = $driver_details->currentLatitude;
							$response_data ['driver_details'] ['driver_longitude'] = $driver_details->currentLongitude;
							$response_data ['driver_details'] ['driver_doj'] = $driver_details->createdDatetime;
							$response_data ['driver_details'] ['driver_dob'] = $driver_personal_details->dob;
							$response_data ['driver_details'] ['driver_address'] = $driver_personal_details->address;
							$response_data ['driver_details'] ['profile_image'] = ($driver_details->profileImage) ? driver_content_url ( $driver_details->id . '/' . $driver_details->profileImage ) : image_url ( 'app/user.png' );
						}
						if ($update_result) {
							$message = array (
									"message" => $msg_data ['rating_success'],
									"details" => array (
											$response_data 
									),
									"status" => 1 
							);
						} else {
							$message = array (
									"message" => $msg_data ['rating_failed'],
									"status" => - 1 
							);
						}
					} else {
						$message = array (
								"message" => $msg_data ['mandatory_data'],
								"details" => '',
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['invalid_user'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => - 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => - 401 
			);
		}
		echo json_encode ( $message );
	}
	public function getTripDetail_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$response_data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$trip_details = NULL;
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$trip_id = NULL;
		
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$trip_id = $post_data->trip_id;
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual funtionality
				if ($trip_id != null) {
					
					if ($trip_details) {
						$trip_details = $this->Common_Api_Webservice_Model->getTripDetails ( $trip_id );
						// To get tax & admin commission entity details
						
						$entity_details = $this->Entity_Model->getOneByKeyValueArray ( array (
								'id' => $trip_details [0]->entityId 
						) );
						
						// to get currency code/currency symbol currencySymbol
						$currency_details = $this->Country_Model->getColumnByKeyValueArray ( 'currencySymbol', array (
								'id' => $entity_details->countryId 
						) );
						
						$response_data = array (
								'trip_details' => array (
										'trip_id' => $trip_id,
										'job_card_id' => $trip_details [0]->jobCardId,
										'pickup_location' => $trip_details [0]->pickupLocation,
										'pickup_latitude' => $trip_details [0]->pickupLatitude,
										'pickup_longitude' => $trip_details [0]->pickupLongitude,
										'pickup_time' => $trip_details [0]->actualPickupDatetime,
										'drop_location' => $trip_details [0]->dropLocation,
										'drop_latitude' => $trip_details [0]->dropLatitude,
										'drop_longitude' => $trip_details [0]->dropLongitude,
										'drop_time' => $trip_details [0]->dropDatetime,
										'land_mark' => $trip_details [0]->landmark,
										'taxi_rating' => $trip_details [0]->passengerRating,
										'taxi_comments' => $trip_details [0]->passengerComments,
										'trip_type' => $trip_details [0]->tripType,
										'trip_type_name' => $trip_details [0]->tripTypeName,
										'payment_mode' => $trip_details [0]->paymentMode,
										'payment_mode_name' => $trip_details [0]->paymentModeName,
										'trip_status' => $trip_details [0]->tripStatus,
										'trip_status_name' => $trip_details [0]->tripStatusName 
								),
								'passenger_details' => array (
										'passenger_id' => $trip_details [0]->passengerId,
										'passenger_firstname' => $trip_details [0]->passengerFirstName,
										'passenger_lastname' => $trip_details [0]->passengerLastName,
										'passenger_mobile' => $trip_details [0]->passengerMobile,
										'passenger_profile_image' => ($trip_details [0]->passengerProfileImage) ? (passenger_content_url ( $trip_details [0]->passengerId . '/' . $trip_details [0]->passengerProfileImage )) : image_url ( 'app/user.png' ) 
								),
								
								// 'driver_id' => $trip_details [0]->driverId,
								// 'driver_firstname' => $trip_details [0]->driverFirstName,
								// 'driver_lastname' => $trip_details [0]->driverLastName,
								// 'driver_mobile' => $trip_details [0]->driverMobile,
								// 'driver_profile_image' => ($trip_details [0]->driverProfileImage) ? (driver_content_url ( $trip_details [0]->driverId . '/' . $trip_details [0]->driverProfileImage )) : image_url ( 'app/user.png' ),
								// 'driver_language' => $trip_details [0]->driverLanguagesKnown,
								// 'driver_experience' => $trip_details [0]->driverExperience,
								// 'driver_age' => $trip_details [0]->driverDob,
								// 'driver_latitute' => $trip_details [0]->currentLatitude,
								// 'driver_longtitute' => $trip_details [0]->currentLongitude,
								// 'driver_status' => $trip_details [0]->availabilityStatus,
								'taxi_details' => array (
										'taxi_id' => $trip_details [0]->taxiId,
										'taxi_name' => $trip_details [0]->taxiName,
										'taxi_model' => $trip_details [0]->taxiModel,
										'taxi_manufacturer' => $trip_details [0]->taxiManufacturer,
										'taxi_registration_no' => $trip_details [0]->taxiRegistrationNo,
										'taxi_colour' => $trip_details [0]->taxiColour 
								),
								
								// 'driver_rating' => $trip_details [0]->passengerRating,
								// 'driver_comments' => $trip_details [0]->passengerComments,
								'transaction_details' => array (
										'base_charge' => $trip_details [0]->baseCharge,
										'travel_charge' => $trip_details [0]->travelCharge,
										'parking_charge' => $trip_details [0]->parkingCharge,
										'tax_charge' => $trip_details [0]->taxCharge,
										'tax_percentage' => $trip_details [0]->taxPercentage,
										'toll_charge' => $trip_details [0]->tollCharge,
										'promo_discount' => $trip_details [0]->promoDiscountAmount,
										'wallet_payment' => $trip_details [0]->walletPaymentAmount,
										'travelled_period' => $trip_details [0]->travelledPeriod,
										'distance' => $trip_details [0]->travelledDistance,
										'total_trip_cost' => $trip_details [0]->totalTripCharge 
								) 
						);
						// driver average rating
						$driver_avg_rating = $this->Driver_Api_Webservice_Model->getDriverAverageRating ( $trip_details [0]->driverId );
						if ($driver_avg_rating > 0) {
							$response_data ['driver_rating'] = round ( $driver_avg_rating [0]->driverRating, 2 );
						}
						
						$message = array (
								"message" => $msg_data ['success'],
								"details" => array (
										$response_data 
								),
								"status" => 1,
								"currency_symbol" => $currency_details [0]->currencySymbol 
						);
					} else {
						$message = array (
								"message" => $msg_data ['invalid_trip'],
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['invalid_trip'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	public function resetPassword_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		
		$message = '';
		$user_exist = NULL;
		$email = NULL;
		$password = NULL;
		$update_password = NULL;
		
		$mobile = NULL;
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$mobile = $post_data->mobile;
		
		$user_exist = $this->Driver_Model->getOneByKeyValueArray ( array (
				'mobile' => $mobile 
		) );
		if ($user_exist) {
			$email = $user_exist->email;
			$password = random_string ( 'alnum', 8 );
			// encrypted password
			$password_key = hash ( "sha256", $password );
			$update_password = $this->Driver_Model->update ( array (
					'password' => $password_key 
			), array (
					'id' => $user_exist->id 
			) );
			$entity_config_details = $this->Entity_Config_Model->getOneByKeyValueArray ( array (
					'entityId' => $user_exist->entityId 
			) );
			if ($entity_config_details->sendSms && $update_password) {
				$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
						'title' => 'forgot_password_sms' 
				) );
				
				$message_data = $message_details->content;
				// $message = str_replace("##USERNAME##",$name,$message);
				$message_data = str_replace ( "##PASSWORD##", $password, $message_data );
				// print_r($message);exit;
				if ($mobile != "") {
					
					$this->Common_Api_Webservice_Model->sendSMS ( $mobile, $message_data );
				}
			}
			/*
			 * $from = SMTP_EMAIL_ID;
			 * $to = $user_exist->email;
			 * $subject = 'Zuver Forgot Password';
			 * $data = array (
			 * 'password' => $password
			 * );
			 * $message_data = $this->load->view ( 'emailtemplate/forgotpassword', $data, TRUE );
			 *
			 * if (SMTP && $update_password) {
			 *
			 * if ($to) {
			 * $this->Common_Api_Webservice_Model->sendEmail ( $to, $subject, $message_data );
			 * }
			 * } else {
			 *
			 * // To send HTML mail, the Content-type header must be set
			 * $headers = 'MIME-Version: 1.0' . "\r\n";
			 * $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			 * // Additional headers
			 * $headers .= 'From: Zuver<' . $from . '>' . "\r\n";
			 * $headers .= 'To: <' . $to . '>' . "\r\n";
			 * if (! filter_var ( $to, FILTER_VALIDATE_EMAIL ) === false) {
			 * mail ( $to, $subject, $message_data, $headers );
			 * }
			 * }
			 */
			$message = array (
					"message" => $msg_data ['reset_password'],
					'status' => 1 
			);
		} else {
			$message = array (
					"message" => $msg_data ['invalid_user'],
					'status' => - 1 
			);
		}
		
		echo json_encode ( $message );
	}
	public function getProfileDetail_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$driver_logged_status = NULL;
		$check_trip_avilablity = NULL;
		$trip_status_update = NULL;
		$trip_request_status_update = NULL;
		$trip_tracking_update = NULL;
		$driver_id = NULL;
		
		$response_data = array ();
		
		// Posted json data
		$post_data = $this->get_post_data ();
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$driver_id = $post_data->driver_id;
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual functionality
				if ($driver_id) {
					$driver_logged_status = $this->Driver_Model->getOneByKeyValueArray ( array (
							'id' => $driver_id,
							'status' => Status_Type_Enum::ACTIVE,
							'loginStatus' => Status_Type_Enum::ACTIVE 
					) );
					if ($driver_logged_status) {
						// get the driver detail
						$driver_details = $this->Driver_Model->getById ( $driver_id );
						// get driver personal details
						$driver_personal_details = $this->Driver_Personal_Details_Model->getOneByKeyValueArray ( array (
								'driverId' => $driver_id 
						) );
						// get driver average rating rated by passenger for completed trip
						$driver_rating = $this->Driver_Api_Webservice_Model->getDriverAverageRating ( $driver_id );
						
						/*
						 * // to get the driver rejected/cancelled trip
						 * $driver_rejected_count = $this->Taxi_Rejected_Trip_Details_Model->getColumnByKeyValueArray ( 'driverId', array (
						 * 'driverId' => $driver_id,
						 * 'createdDatetime >=' => getCurrentDate (),
						 * 'createdDatetime <=' => getCurrentDate ()
						 * ) );
						 *
						 * $driver_earning_details = $this->Driver_Api_Webservice_Model->getDriverEarningDetails ( $driver_id );
						 * $driver_today_earning_details = $this->Driver_Api_Webservice_Model->getDriverEarningDetails ( $driver_id, 1 );
						 *
						 * // To calculate time driven on current date/today
						 * $total_amount = 0;
						 * $total_today_amount = 0;
						 * $time_result = '00:00';
						 * $actual_pickup_time = '';
						 * $drop_time = '';
						 * $hours = '';
						 * $minutes = '';
						 * $seconds = '';
						 * $date_difference = "";
						 * $total_differnce = "";
						 * foreach ( $driver_earning_details as $get_details ) {
						 * $actual_pickup_time = strtotime ( $get_details->actualPickupDatetime );
						 * $drop_time = strtotime ( $get_details->dropDatetime );
						 * // echo $actual_pickup_time;
						 * // echo '-';
						 * // echo $drop_time;
						 * // echo '<br>';
						 * $date_difference = abs ( $drop_time - $actual_pickup_time );
						 * $total_differnce += $date_difference;
						 * // to get total earned amount by the driver
						 * $total_amount += $get_details->driverEarning;
						 * }
						 * foreach ( $driver_today_earning_details as $get_details ) {
						 * // to get total earned amount by the driver
						 * $total_today_amount += $get_details->driverEarning;
						 * }
						 * // $date_difference = $drop_time - $actual_pickup_time;
						 * $hours += floor ( (($total_differnce % 604800) % 86400) / 3600 );
						 * $minutes += floor ( ((($total_differnce % 604800) % 86400) % 3600) / 60 );
						 * $seconds += floor ( (((($total_differnce % 604800) % 86400) % 3600) % 60) );
						 * $time_result = $minutes . ':' . $seconds;
						 * // To calculate time driven on current date/today
						 *
						 * $statistics = array (
						 * "total_trips" => count ( $driver_earning_details ),
						 * "cancel_trips" => count ( $driver_rejected_count ),
						 * "total_earnings" => round ( $total_amount, 2 ),
						 * "overall_rejected_trips" => count ( $driver_rejected_count ),
						 * "today_earnings" => round ( $total_today_amount, 2 ),
						 * "shift_status" => Driver_Shift_Status_Enum::SHIFTIN,
						 * "time_driven" => $time_result,
						 * "status" => 1
						 * );
						 * $response_data ['driver_statistics'] = $statistics;
						 */
						// To get driver taxi id
						$driver_taxi_id = $this->Driver_Taxi_Mapping_Model->getColumnByKeyValueArray ( 'taxiId', array (
								'driverId' => $driver_details->id 
						) );
						
						$response_data = array (
								"driver_details" => array (
										"driver_id" => $driver_details->id,
										"driver_referral_code" => $driver_details->driverCode,
										"firstname" => $driver_details->firstName,
										"lastname" => $driver_details->lastName,
										"email" => $driver_details->email,
										"mobile" => $driver_details->mobile,
										"driver_latitude" => $driver_details->currentLatitude,
										"driver_longitude" => $driver_details->currentLongitude,
										"driver_doj" => $driver_details->createdDatetime,
										"driver_dob" => $driver_personal_details->dob,
										"driver_address" => $driver_personal_details->address,
										"profile_image" => ($driver_details->profileImage) ? driver_content_url ( $driver_details->id . '/' . $driver_details->profileImage ) : image_url ( 'app/user.png' ),
										
										"age" => intval ( substr ( date ( 'Ymd' ) - date ( 'Ymd', strtotime ( $driver_personal_details->dob ) ), 0, - 4 ) ),
										"language" => $driver_details->languageKnown,
										"experience" => $driver_details->experience,
										"license_no" => $driver_details->licenseNo,
										"taxi_id" => $taxi_details->id,
										"taxi_name" => $taxi_details->name,
										"taxi_model" => $taxi_details->model,
										"taxi_manufacturer" => $taxi_details->manufacturer,
										"taxi_colour" => $taxi_details->colour,
										"taxi_status" => ($taxi_details->status) ? "Active" : "Inactive",
										"taxi_registration_no" => $taxi_details->registrationNo,
										"driver_wallet" => $driver_details->driverWallet,
										"driver_wallet" => $driver_details->driverCredit,
										"driver_rating" => round ( $driver_rating [0]->driverRating, 2 ) 
								) 
						);
						$message = array (
								"message" => $msg_data ['success'],
								"details" => array (
										$response_data 
								),
								"status" => 1 
						);
					} else {
						$message = array (
								"message" => $msg_data ['driver_login_failed'],
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['invalid_user'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	public function getCoreConfig_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		
		$post_data = array ();
		$response_data = array ();
		$message = '';
		// Posted json data
		$post_data = $this->get_post_data ();
		
		// To get tax & admin commission entity details
		
		$entity_details = $this->Entity_Model->getOneByKeyValueArray ( array (
				'id' => DEFAULT_COMPANY_ID 
		) );
		$entity_config_details = $this->Entity_Config_Model->getOneByKeyValueArray ( array (
				'entityId' => $entity_details->id 
		) );
		// to get currency code/currency symbol currencySymbol
		$currency_details = $this->Country_Model->getColumnByKeyValueArray ( 'currencySymbol', array (
				'id' => $entity_details->countryId 
		) );
		
		// to get trip_type list from data attributes
		$trip_type_list = $this->Data_Attributes_Model->getColumnByKeyValueArray ( 'id,description', array (
				'tableName' => strtoupper ( 'TRIP_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		) );
		
		// to get payment_mode list from data attributes
		$payment_mode_list = $this->Data_Attributes_Model->getColumnByKeyValueArray ( 'id,description', array (
				'tableName' => strtoupper ( 'PAYMENT_MODE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		) );
		
		// to get taxi_category_type list from data attributes
		$taxi_category_type_list = $this->Data_Attributes_Model->getColumnByKeyValueArray ( 'id,description', array (
				'tableName' => strtoupper ( 'TAXI_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		) );
		// to get taxi_category_type list from data attributes
		$trip_status_list = $this->Data_Attributes_Model->getColumnByKeyValueArray ( 'id,description', array (
				'tableName' => strtoupper ( 'TRIP_STATUS' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		) );
		if ($entity_details) {
			
			$response_data = array (
					'api_base_url' => base_url (),
					'logo_image_url' => image_url ( 'app/' . $entity_details->logo ),
					'aboutpage_description' => $entity_details->description,
					'contact_email' => $entity_details->email,
					'trip_type_list' => $trip_type_list,
					'payment_mode_list' => $payment_mode_list,
					'taxi_category_type_list' => $taxi_category_type_list,
					'trip_status_list' => $trip_status_list,
					'currency_symbol' => $currency_details [0]->currencySymbol,
					'driver_app_ver' => number_format ( $entity_config_details->driverAndroidAppVer, 1 ),
					'is_force_update' => 0 
			);
			
			$message = array (
					"message" => $msg_data ['success'],
					"details" => $response_data,
					"status" => 1 
			);
		} else {
			$message = array (
					"message" => $msg_data ['failed'],
					"status" => - 1 
			);
		}
		
		echo json_encode ( $message );
	}
	public function checkCustomerReferenceNo_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$user_exist = NULL;
		$response_data = array ();
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$trip_id = NULL;
		$customer_reference_no = NULL;
		
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$trip_id = $post_data->trip_id;
		$customer_reference_no = $post_data->customer_reference_no;
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual funtionality
				if ($trip_id != NULL) {
					if ($customer_reference_no) {
						$crn_exists = $this->Trip_Details_Model->getOneByKeyValueArray ( array (
								'customerReferenceCode' => $customer_reference_no,
								'id' => $trip_id 
						) );
						if ($crn_exists) {
							$update_result = $this->Trip_Details_Model->update ( array (
									'isCodeVerified' => Status_Type_Enum::ACTIVE 
							), array (
									'id' => $trip_id 
							) );
							$trip_details = $this->Common_Api_Webservice_Model->getTripDetails ( $trip_id );
							
							$response_data = array (
									'trip_details' => array (
											'job_card_id' => $trip_details [0]->jobCardId,
											'pickup_location' => $trip_details [0]->pickupLocation,
											'pickup_latitude' => $trip_details [0]->pickupLatitude,
											'pickup_longitude' => $trip_details [0]->pickupLongitude,
											'pickup_time' => $trip_details [0]->actualPickupDatetime,
											'drop_location' => $trip_details [0]->dropLocation,
											'drop_latitude' => $trip_details [0]->dropLatitude,
											'drop_longitude' => $trip_details [0]->dropLongitude 
									),
									'passenger_details' => array (
											'passenger_id' => $trip_details [0]->passengerId,
											'passenger_firstname' => $trip_details [0]->passengerFirstName,
											'passenger_lastname' => $trip_details [0]->passengerLastName,
											'passenger_mobile' => $trip_details [0]->passengerMobile,
											'passenger_profile_image' => ($trip_details [0]->passengerProfileImage) ? (passenger_content_url ( $trip_details [0]->passengerId . '/' . $trip_details [0]->passengerProfileImage )) : image_url ( 'app/user.png' ) 
									),
									'driver_details' => array (
											'driver_id' => $trip_details [0]->driverId,
											'driver_firstname' => $trip_details [0]->driverFirstName,
											'driver_lastname' => $trip_details [0]->driverLastName,
											'driver_mobile' => $trip_details [0]->driverMobile,
											'driver_profile_image' => ($trip_details [0]->driverProfileImage) ? (driver_content_url ( $trip_details [0]->driverId . '/' . $trip_details [0]->driverProfileImage )) : image_url ( 'app/user.png' ) 
									) 
							);
							if ($update_result) {
								$message = array (
										"message" => $msg_data ['crn_success'],
										"details" => array (
												$response_data 
										),
										"status" => 1 
								);
							} else {
								$message = array (
										"message" => $msg_data ['crn_failed'],
										"status" => - 1 
								);
							}
						} else {
							$message = array (
									"message" => $msg_data ['inavlid_crn'],
									"status" => - 1 
							);
						}
					} else {
						$message = array (
								"message" => $msg_data ['mandatory_data'],
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['invalid_user'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => - 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => - 401 
			);
		}
		echo json_encode ( $message );
	}
	private function getTaxiForTrip($trip_id) {
		sleep ( 1 );
		$taxi_request_id = FALSE;
		$driver_taxi_details = NULL;
		$distance = 0;
		$estimated_pickup_distance = 0;
		$estimated_pickup_minute = 0;
		
		$trip_details = $this->Trip_Details_Model->getById ( $trip_id );
		if ($trip_details->tripStatus!=Trip_Status_Enum::CANCELLED_BY_PASSENGER || $trip_details->tripStatus!=Trip_Status_Enum::DRIVER_NOT_FOUND || $trip_details->tripStatus!=Trip_Status_Enum::DRIVER_REJECTED_TRIP){
		$passenger_details = $this->Passenger_Model->getById ( $trip_details->passengerId );
		
		if ($trip_details->tripType == Trip_Type_Enum::LATER) {
			$update_ride_later = $this->Trip_Details_Model->update ( array (
					'tripStatus' => Trip_Status_Enum::BOOKED,
					'notificationStatus' => Trip_Status_Enum::BOOKED,
					'driverAcceptedStatus' => Driver_Accepted_Status_Enum::NONE 
			), array (
					'id' => $trip_id 
			) );
		}
		
		if (TimeIsBetweenTwoTimes ( DAY_START_TIME, DAY_END_TIME, $trip_details->pickupDatetime )) {
			$distance = RIDE_NOW_LIMIT_DAY_ONE;
		} else if (TimeIsBetweenTwoTimes ( NIGHT_START_TIME, NIGHT_END_TIME, $trip_details->pickupDatetime )) {
			$distance = RIDE_NOW_LIMIT_NIGHT_ONE;
		}
		$driver_taxi_details = $this->Driver_Api_Webservice_Model->getDriverListForTrip ( $trip_id, $distance );
		if (! $driver_taxi_details) {
			$distance = 0;
			if (TimeIsBetweenTwoTimes ( DAY_START_TIME, DAY_END_TIME, $trip_details->pickupDatetime )) {
				$distance = RIDE_NOW_LIMIT_DAY_TWO;
			} else if (TimeIsBetweenTwoTimes ( NIGHT_START_TIME, NIGHT_END_TIME, $trip_details->pickupDatetime )) {
				$distance = RIDE_NOW_LIMIT_NIGHT_TWO;
			}
			$driver_taxi_details = $this->Driver_Api_Webservice_Model->getDriverListForTrip ( $trip_id, $distance );
		}
		$minutes = 0;
		$interval = abs ( strtotime ( $trip_details->pickupDatetime ) - getCurrentUnixDateTime () );
		$minutes = round ( $interval / 60 );
		// check taxi request for particular trip already exists or not
		$taxi_request_exists = $this->Taxi_Request_Details_Model->getByKeyValueArray ( array (
				'tripId' => $trip_id 
		) );
		if ($driver_taxi_details) {
			if ($taxi_request_exists) {
				$update_taxi_request = array (
						'selectedDriverId' => $driver_taxi_details [0]->driverId,
						'taxiRequestStatus' => Taxi_Request_Status_Enum::AVAILABLE_TRIP 
				);
				$taxi_request_id = $this->Taxi_Request_Details_Model->update ( $update_taxi_request, array (
						'tripId' => $trip_id 
				) );
			} else {
				$insert_taxi_request = array (
						'tripId' => $trip_id,
						'totalAvailableDriver' => count ( $driver_taxi_details ),
						'selectedDriverId' => $driver_taxi_details [0]->driverId,
						'taxiRequestStatus' => Taxi_Request_Status_Enum::AVAILABLE_TRIP 
				);
				$taxi_request_id = $this->Taxi_Request_Details_Model->insert ( $insert_taxi_request );
			}
			// To get estimated distance & time to reach driver for pickup location from google API
			if ((! $estimated_pickup_distance || $estimated_pickup_minute || $estimated_pickup_distance == '' || $estimated_pickup_minute == '') || ($driver_taxi_details [0]->driverLatitude && $driver_taxi_details [0]->driverLongitude && $driver_taxi_details [0]->pickupLatitude && $driver_taxi_details [0]->pickupLongitude)) {
				$URL = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=" . $driver_taxi_details [0]->driverLatitude . "," . $driver_taxi_details [0]->driverLongitude . "&destinations=" . $driver_taxi_details [0]->pickupLatitude . "," . $driver_taxi_details [0]->pickupLongitude . "&key=" . GOOGLE_MAP_API_KEY;
				
				$curl = curl_init ();
				curl_setopt ( $curl, CURLOPT_URL, $URL );
				curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, true );
				curl_setopt ( $curl, CURLOPT_HEADER, false );
				$result = curl_exec ( $curl );
				curl_close ( $curl );
				// return $result; ;
				
				$response = json_decode ( $result );
				
				if ($response->status) {
					if ($response->rows [0]->elements [0]->distance->text) {
						$distance = explode ( ' ', $response->rows [0]->elements [0]->distance->text );
						if ($distance [1] == 'm') {
							$estimated_pickup_distance = round ( ($distance [0] / 1000), 1 ) + 0.5;
						} else if ($distance [1] == 'km') {
							$estimated_pickup_distance = round ( ($distance [0]), 1 ) + 0.5;
						}
					}
					if ($response->rows [0]->elements [0]->duration->text) {
						$time = explode ( ' ', $response->rows [0]->elements [0]->duration->text );
						
						if ($time [1] == 'hour' || $time [1] == 'hours') {
							$estimated_pickup_minute += ($time [0] * 60) + $time [2];
						} else if ($time [1] == 'mins') {
							$estimated_pickup_minute = $time [0];
						}
					}
				}
			}
			// To get estimated distance & time to reach driver for pickup location from google API
			$update_trip_details = $this->Trip_Details_Model->update ( array (
					'driverId' => $driver_taxi_details [0]->driverId,
					'taxiId' => $driver_taxi_details [0]->taxiId,
					'tripStatus' => Trip_Status_Enum::DRIVER_DISPATCHED,
					'dispatchedDatetime' => getCurrentDateTime (),
					'estimatedPickupDistance' => $estimated_pickup_distance,
					'estimatedPickupMinute' => $estimated_pickup_minute,
					'notificationStatus' => Trip_Status_Enum::DRIVER_DISPATCHED,
					'driverAcceptedStatus' => Driver_Accepted_Status_Enum::NONE 
			), array (
					'id' => $trip_id 
			) );
		} else if ($trip_details->tripType == Trip_Type_Enum::NOW || ($trip_details->tripType == Trip_Type_Enum::NOW && $minutes <= 10)) {
			if ($trip_details->tripType == Trip_Type_Enum::NOW) {
				$update_trip_details = $this->Trip_Details_Model->update ( array (
						'tripStatus' => Trip_Status_Enum::DRIVER_NOT_FOUND,
						'notificationStatus' => Trip_Status_Enum::DRIVER_NOT_FOUND,
						'driverAcceptedStatus' => Driver_Accepted_Status_Enum::NONE 
				), array (
						'id' => $trip_id 
				) );
			}
			if ($trip_details->tripType == Trip_Type_Enum::LATER && $minutes <= 10) {
				$update_trip_details = $this->Trip_Details_Model->update ( array (
						'tripStatus' => Trip_Status_Enum::DRIVER_NOT_FOUND,
						'notificationStatus' => Trip_Status_Enum::DRIVER_NOT_FOUND,
						'driverAcceptedStatus' => Driver_Accepted_Status_Enum::NONE 
				), array (
						'id' => $trip_id 
				) );
				
				$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
						'title' => 'drive_later_cancelled_sms' 
				) );
				
				$message_data = $message_details->content;
				// $message = str_replace("##USERNAME##",$name,$message);
				$message_data = str_replace ( "##PASSENGERNAME##", $passenger_details->firstName . ' ' . $passenger_details->lastName, $message_data );
				$message_data = str_replace ( "##JOBCARDID##", $trip_details->jobCardId, $message_data );
				// print_r($message);exit;
				if ($passenger_details->mobile != "") {
					
					$this->Common_Api_Webservice_Model->sendSMS ( $passenger_details->mobile, $message_data );
				}
			}
		}
             }
		return $driver_taxi_details;
	}
	public function dynamicPage_post() {
		// intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = array ();
		$data = array ();
		$page_name = NULL;
		$device_type = NULL;
		$is_english = NULL;
		$response_data = array ();
		$message = '';
		// Posted json data
		
		$post_data = $this->get_post_data (); // $this->input->post ();
		                                      // debug_exit($post_data->phone);
		
		$page_name = $post_data->page_name;
		$device_type = $post_data->device_type;
		// $is_english = $post_data->is_english;
		
		if ($page_name) {
			$data_content = $this->Menu_Content_Details_Model->getOneByKeyValueArray ( array (
					'name' => $page_name,
					'status' => Status_Type_Enum::ACTIVE 
			) );
			if ($data_content) {
				if ($device_type == Device_Type_Enum::ANDROID) {
					
					$response_data ['content'] = $data_content->content;
					$response_data ['title'] = $data_content->title;
				} else if ($device_type == Device_Type_Enum::IPHONE) {
					
					$response_data ['content'] = htmlentities ( $data_content->content );
					$response_data ['title'] = $data_content->title;
				}
				$message = array (
						"message" => $msg_data ['success'],
						"details" => $response_data,
						"status" => 1 
				);
			} else {
				$message = array (
						"message" => $msg_data ['data_not_found'],
						"status" => 2 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['invalid_page'],
					"status" => - 1 
			);
		}
		echo json_encode ( $message );
	}
	public function getEmergencyContacts_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = array ();
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$emergency_contact_list = NULL;
		
		$response_data = array ();
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$user_id = NULL;
		
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$user_id = $post_data->driver_id;
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual functonality
				if ($user_id) {
					$emergency_contact_list = $this->User_Emergency_Contacts_Model->getByKeyValueArray ( array (
							'userId' => $user_id,
							'userType' => User_Type_Enum::DRIVER,
							'status' => Status_Type_Enum::ACTIVE 
					) );
					if ($emergency_contact_list) {
						foreach ( $emergency_contact_list as $list ) {
							$response_data [] = array (
									'emergency_id' => $list->id,
									'name' => $list->name,
									'mobile' => $list->mobile,
									'email' => $list->email 
							);
						}
						$message = array (
								"message" => $msg_data ['success'],
								"emergency_contact_list" => $response_data,
								"status" => 1 
						);
					} else {
						$message = array (
								"message" => $msg_data ['failed'],
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['mandatory_data'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		
		echo json_encode ( $message );
	}
	public function deleteEmergencyContact_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = array ();
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$delete_user_emergency = NULL;
		$user_id = NULL;
		$emergency_id = NULL;
		$response_data = array ();
		
		// Posted json data
		$post_data = $this->get_post_data ();
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$user_id = $post_data->driver_id;
		$emergency_id = $post_data->emergency_id;
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual functonality
				if ($user_id && $emergency_id) {
					$delete_user_emergency = $this->User_Emergency_Contacts_Model->delete ( array (
							'id' => $emergency_id,
							'userId' => $user_id,
							'userType' => User_Type_Enum::DRIVER 
					) );
					if ($delete_user_emergency) {
						$message = array (
								"message" => $msg_data ['sos_delete_success'],
								"status" => 1 
						);
					} else {
						$message = array (
								"message" => $msg_data ['sos_delete_failed'],
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['mandatory_data'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		
		echo json_encode ( $message );
	}
	
	/**
	 * used to add/edit the emergency contacts
	 */
	public function saveEmergencyContact_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$check_user_emergency = NULL;
		$user_emergency_exists = NULL;
		$insert_data = array ();
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$user_id = NULL;
		$emergency_id = NULL;
		$emergency_name = NULL;
		$emergency_mobile = NULL;
		$emergency_email = '';
		
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$user_id = $post_data->driver_id;
		$emergency_name = $post_data->emergency_name;
		$emergency_mobile = $post_data->emergency_mobile;
		if (array_key_exists ( 'emergency_email', $post_data )) {
			$emergency_email = $post_data->emergency_email;
		}
		if (array_key_exists ( 'emergency_id', $post_data )) {
			$emergency_id = $post_data->emergency_id;
		}
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual functonality
				if ($user_id && $emergency_name && $emergency_mobile) {
					$check_user_emergency = $this->User_Emergency_Contacts_Model->getByKeyValueArray ( array (
							'userId' => $user_id 
					) );
					
					if (! $emergency_id > 0) {
						$user_emergency_exists = $this->User_Emergency_Contacts_Model->getByKeyValueArray ( array (
								'userId' => $user_id,
								'userType' => User_Type_Enum::DRIVER,
								'mobile' => $emergency_mobile,
								'email' => $emergency_email,
								'status' => Status_Type_Enum::ACTIVE 
						) );
						if (! $user_emergency_exists) {
							// debug_exit(EMERGENCY_COUNT_LIMIT);
							if (count ( $check_user_emergency ) < EMERGENCY_COUNT_LIMIT) {
								$insert_data = array (
										'userId' => $user_id,
										'userType' => User_Type_Enum::DRIVER,
										'name' => $emergency_name,
										'email' => $emergency_email,
										'mobile' => $emergency_mobile,
										'status' => Status_Type_Enum::ACTIVE 
								);
								$emergency_id = $this->User_Emergency_Contacts_Model->insert ( $insert_data );
								if ($emergency_id) {
									$message = array (
											"message" => $msg_data ['sos_add_success'],
											"emergency_id" => $emergency_id,
											"status" => 1 
									);
								} else {
									$message = array (
											"message" => $msg_data ['sos_add_failed'],
											"status" => - 1 
									);
								}
							} else {
								$message = array (
										"message" => $msg_data ['sos_limit_exceeds'],
										"status" => - 1 
								);
							}
						} else {
							$message = array (
									"message" => $msg_data ['email_mobile_exists'],
									"status" => - 1 
							);
						}
					} else {
						$update_data = array (
								'userId' => $user_id,
								'userType' => User_Type_Enum::DRIVER,
								'name' => $emergency_name,
								'email' => $emergency_email,
								'mobile' => $emergency_mobile,
								'status' => Status_Type_Enum::ACTIVE 
						);
						$emergency_update = $this->User_Emergency_Contacts_Model->update ( $update_data, array (
								'id' => $emergency_id 
						) );
						if ($emergency_update) {
							$message = array (
									"message" => $msg_data ['sos_updated_success'],
									"emergency_id" => $emergency_id,
									"status" => 1 
							);
						} else {
							$message = array (
									"message" => $msg_data ['sos_add_failed'],
									"status" => - 1 
							);
						}
					}
				} else {
					$message = array (
							"message" => $msg_data ['mandatory_data'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		
		echo json_encode ( $message );
	}
	public function getMyAccount_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = array ();
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$driver_earning_details = NULL;
		$driver_today_earning_details = NULL;
		
		$response_data = array ();
		
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$driver_id = NULL;
		$driver_wallet = 00.00;
		
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$driver_id = $post_data->driver_id;
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual functonality
				if ($driver_id) {
					$driver_details = $this->Driver_Model->getById ( $driver_id );
					$driver_wallet = $driver_details->driverWallet;
					// to get the driver statistics
					
					$driver_statistics = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
							'driverId' => $driver_id 
					) );
					
					$driver_earning_details = $this->Driver_Api_Webservice_Model->getDriverEarningDetails ( $driver_id );
					$driver_today_earning_details = $this->Driver_Api_Webservice_Model->getDriverEarningDetails ( $driver_id, 1 );
					// debug_exit($driver_today_earning_details);
					// To calculate time driven on current date/today
					$total_amount = 0;
					$total_today_amount = 0;
					$time_result = '00:00';
					$actual_pickup_time = 0;
					$drop_time = 0;
					$hours = 0;
					$minutes = 0;
					$seconds = 0;
					$date_difference = 0;
					$total_differnce = 0;
					foreach ( $driver_earning_details as $get_details ) {
						$actual_pickup_time = strtotime ( $get_details->actualPickupDatetime );
						$drop_time = strtotime ( $get_details->dropDatetime );
						$date_difference = abs ( $drop_time - $actual_pickup_time );
						$total_differnce += $date_difference;
						// to get total earned amount by the driver
						$total_amount += $get_details->driverEarning;
					}
					
					foreach ( $driver_today_earning_details as $get_details ) {
						// to get total earned amount by the driver
						$total_today_amount += $get_details->driverEarning;
					}
					
					if ($total_differnce) {
						// $date_difference = $drop_time - $actual_pickup_time;
						$hours += floor ( (($total_differnce % 604800) % 86400) / 3600 );
						$minutes += floor ( ((($total_differnce % 604800) % 86400) % 3600) / 60 );
						$seconds += floor ( (((($total_differnce % 604800) % 86400) % 3600) % 60) );
						// $time_result = $minutes . ':' . $seconds;
						$time_result = $minutes . ' minutes';
						// To calculate time driven on current date/today
					}
					$statistics = array (
							"total_completed_trips" => $driver_statistics->totalCompletedTrip,
							"total_rejected_trips" => $driver_statistics->totalRejectedTrip,
							"week_completed_trips" => $driver_statistics->weekCompletedTrip,
							"week_rejected_trips" => $driver_statistics->weekRejectedTrip,
							"average_rating" => $driver_statistics->avgRating,
							"average_distace_travelled" => $driver_statistics->avgTripDistance,
							"consecutive_reject_count" => $driver_statistics->consecutiveRejectCount,
							"total_earnings" => round ( $total_amount, 2 ),
							"today_earnings" => round ( $total_today_amount, 2 ) 
					);
					// "time_driven" => $time_result
					
					$response_data = array (
							'driver_details' => array (
									'driver_id' => $driver_details->id,
									'driver_name' => $driver_details->firstName . ' ' . $driver_details->lastName,
									'driver_mobile' => $driver_details->mobile,
									'driver_referral_code' => $driver_details->driverCode,
									'driver_wallet_amount' => $driver_details->driverWallet,
									'driver_credit_amount' => $driver_details->driverCredit 
							),
							'driver_statistics' => $statistics 
					);
					
					$message = array (
							"message" => $msg_data ['success'],
							"details" => array (
									$response_data 
							),
							"status" => 1 
					);
				} else {
					$message = array (
							"message" => $msg_data ['mandatory_data'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		
		echo json_encode ( $message );
	}
	public function emergencyReport_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$user_exists = array ();
		$user_id = NULL;
		$latitude = NULL;
		$longitude = NULL;
		$current_location = NULL;
		$emergency_list = array ();
		$address = array ();
		
		// Posted json data
		$post_data = $this->get_post_data ();
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$user_id = $post_data->driver_id;
		$latitude = $post_data->latitude;
		$longitude = $post_data->longitude;
		$current_location = $post_data->current_location;
		
		$insert_data = array (
				'userId' => $user_id,
				'userType' => User_Type_Enum::DRIVER,
				'location' => $current_location,
				'latitude' => $latitude,
				'longitude' => $longitude,
				'emergencyStatus' => Emergency_Status_Enum::OPEN 
		);
		$emergency_id = $this->Emergency_Request_Details_Model->insert ( $insert_data );
		// Google map url link
		$google_map_url = "http://maps.google.com/maps?z=11&t=m&q=loc:" . $latitude . "+" . $longitude;
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual functionality
				if ($user_id) {
					$user_exists = $this->Driver_Model->getById ( $user_id );
					$entity_config_details = $this->Entity_Config_Model->getOneByKeyValueArray ( array (
							'entityId' => $user_exists->entityId 
					) );
					if (count ( $user_exists ) > 0) {
						$emergency_list = $this->User_Emergency_Contacts_Model->getByKeyValueArray ( array (
								'userId' => $user_id,
								'userType' => User_Type_Enum::DRIVER,
								'status' => Status_Type_Enum::ACTIVE 
						) );
						
						if (count ( $emergency_list ) > 0) {
							
							foreach ( $emergency_list as $list ) {
								
								if ($entity_config_details->sendSms && $list->mobile) {
									$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
											'title' => 'emergency_contact' 
									) );
									
									$message_data = $message_details->content;
									
									$message_data = str_replace ( "##PASSENGERNAME##", $user_exists->firstName . ' ' . $user_exists->lastName, $message_data );
									$message_data = str_replace ( "##LOCATION##", $current_location, $message_data );
									$message_data = str_replace ( "##GOOGLELINK##", $google_map_url, $message_data );
									
									if ($list->mobile != "") {
										
										$this->Common_Api_Webservice_Model->sendSMS ( $list->mobile, $message_data );
									}
								}
							}
							
							$message = array (
									"message" => $msg_data ['sos_notice'],
									"driver_id" => $user_id,
									"status" => 1 
							);
						} else {
							$message = array (
									"message" => $msg_data ['data_not_found'],
									"status" => - 1 
							);
						}
					} else {
						$message = array (
								"message" => $msg_data ['invalid_user'],
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['invalid_user'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	public function walletUpdateByEasyPoints_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$driver_logged_status = NULL;
		$driver_id = NULL;
		$digital_code = NULL;
		$transaction_amount = NULL;
		$exists_digital_code = NULL;
		
		// Posted json data
		$post_data = $this->get_post_data ();
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		
		$driver_id = $post_data->driver_id;
		$digital_code = $post_data->digital_code;
		
		$transaction_amount = $post_data->transaction_amount;
		
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				
				if ($driver_id) {
					$driver_logged_status = $this->Driver_Model->getOneByKeyValueArray ( array (
							'id' => $driver_id,
							'status' => Status_Type_Enum::ACTIVE,
							'loginStatus' => Status_Type_Enum::ACTIVE 
					) );
					if ($driver_logged_status) {
						$exists_digital_code = $this->Driver_Transaction_Details_Model->getOneByKeyValueArray ( array (
								'transactionId' => $digital_code 
						) );
						if (! $exists_digital_code) {
							if ($transaction_amount < 99999) {
								$response_data = $this->Driver_Api_Webservice_Model->checkEasyPoints ( $digital_code, $transaction_amount );
								//debug_exit($response_data);
								if ($response_data ['ResCode'] == 00) {
									$transaction_value = $transaction_amount*100;
									$transaction_value = str_pad((string)$transaction_value, 12, "0", STR_PAD_LEFT);
									debug($transaction_value); echo "===="; debug($response_data ['TranAmount']); echo "===="; 
									if ($response_data ['TranAmount'] == $transaction_value) {
										// get current wallet amount of driver to credit wallet payment ammount
										$current_driver_wallet_amount = $this->Driver_Model->getById ( $driver_id );
										
										// update the driver transaction by credit promocode ammount
										$insert_driver_transaction = array (
												'driverId' => $driver_id,
												'transactionAmount' => $transaction_amount,
												'previousAmount' => $current_driver_wallet_amount->driverWallet,
												'currentAmount' => ($current_driver_wallet_amount->driverWallet + $transaction_amount),
												'transactionStatus' => 'Success',
												'transactionId' => $digital_code,
												'transactionType' => Transaction_Type_Enum::EASY_POINTS,
												'transactionFrom' => Transaction_From_Enum::WALLET_ACCOUNT,
												'transactionMode' => Transaction_Mode_Enum::CREDIT 
										);
										$update_driver_transaction = $this->Driver_Transaction_Details_Model->insert ( $insert_driver_transaction );
										$remaining_driver_wallet = $current_driver_wallet_amount->driverWallet + $transaction_amount;
										$update_driver_wallet = $this->Driver_Model->update ( array (
												'driverWallet' => $remaining_driver_wallet 
										), array (
												'id' => $driver_id 
										) );
										
										// @todo update driver dispath details table
										$driver_dispatch_exists = NULL;
										$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
												'driverId' => $driver_id 
										) );
										
										if ($driver_dispatch_exists) {
											
											$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
													'walletBalance' => $remaining_driver_wallet 
											), array (
													'driverId' => $driver_id 
											) );
										} else {
											$insert_dispatch_data = array (
													'driverId' => $driver_id,
													'walletBalance' => $remaining_driver_wallet 
											);
											$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
										}
										
										// end driver dispath details table
										$message = array (
												"message" => $response_data ['ResDesc'],
												"message_code" => $response_data ['ResCode'],
												"status" => 1 
										);
									} else {
										$message = array (
												"message" => $msg_data ['amount_response_mismatch'],
												"status" => - 1 
										);
									}
								} else {
									$message = array (
												"message" => $response_data ['ResDesc'],
												"message_code" => $response_data ['ResCode'],
												"status" => -1 
										);
								}
							} else {
								$message = array (
										"message" => $msg_data ['amount_limit_exceeds'],
										"status" => - 1 
								);
							}
						} else {
							$message = array (
									"message" => $msg_data ['digital_code_exists'],
									"status" => - 1 
							);
						}
					} else {
						$message = array (
								"message" => $msg_data ['driver_login_failed'],
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['invalid_user'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
	public function tripNotification($trip_id, $user_id, $user_type, $notification_msg = "Success", $notification_status = 1, $response_data = array()) {
		// $gcm_id,$trip_id //Posted json data
		$gcm_id = '';
		// $trip_id = $post_data->trip_id;
		// $driver_id = 2;
		$response = NULL;
		$msg_data = $this->config->item ( 'api' );
		$msg = '';
		$status = 0;
		$firebase_api_key = PASSENGER_FIREBASE_API_KEY;
		
		$post_data = array ();
		$response_data = array ();
		if ($user_type == User_Type_Enum::DRIVER) {
			$user_details = $this->Driver_Model->getById ( $user_id );
		} else if ($user_type == User_Type_Enum::PASSENGER) {
			$user_details = $this->Passenger_Model->getById ( $user_id );
		}
		
		if ($user_details) {
			if ($user_details->gcmId) {
				$gcm_id = $user_details->gcmId;
				$post_data ['registration_ids'] = ( array ) $gcm_id;
				if ($gcm_id) {
					$trip_details = $this->Trip_Details_Model->getById ( $trip_id );
					
					$response_data = array (
							"message" => "trip",
							"status" => 1,
							"details" => array (
									"message" => $notification_msg,
									"status" => $notification_status 
							) 
					);
					
					$msg = $msg_data ['success'];
					$status = 1;
					$post_data ['data'] = $response_data;
				} else {
					
					$post_data ['data'] = array (
							'message' => $msg_data ['failed'],
							'status' => - 1 
					);
				}
				
				$post_data = json_encode ( $post_data );
				$url = "https://fcm.googleapis.com/fcm/send";
				
				$ch = curl_init ();
				curl_setopt ( $ch, CURLOPT_URL, $url );
				curl_setopt ( $ch, CURLOPT_HTTPHEADER, array (
						'Content-Type:application/json',
						'Authorization: key=' . $firebase_api_key 
				) );
				curl_setopt ( $ch, CURLOPT_POST, 1 );
				curl_setopt ( $ch, CURLOPT_POSTFIELDS, $post_data );
				curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
				$response = curl_exec ( $ch );
				curl_close ( $ch );
			}
		}
		return $response;
		// echo json_encode ( array('response'=>$response,'msg'=>$msg,'status'=>$status));
	}
	public function tripDispatchDriverNotification($trip_id, $driver_id, $distance = 0) {
		// $gcm_id,$trip_id //Posted json data
		$gcm_id = '';
		// $trip_id = $post_data->trip_id;
		// $driver_id = 2;
		$response = NULL;
		$msg_data = $this->config->item ( 'api' );
		$msg = '';
		$status = 0;
		$firebase_api_key = DRIVER_FIREBASE_API_KEY;
		
		$post_data = array ();
		$response_data = array ();
		
		$driver_details = $this->Driver_Model->getById ( $driver_id );
		
		if ($driver_details) {
			if ($driver_details->gcmId) {
				$gcm_id = $driver_details->gcmId;
				$post_data ['registration_ids'] = ( array ) $gcm_id;
				if ($gcm_id) {
					// $trip_details = $this->Trip_Details_Model->getById($trip_id);
					$trip_details = $this->Common_Api_Webservice_Model->getTripDetails ( $trip_id );
					$passenger_details = $this->Passenger_Model->getById ( $trip_details->passengerId );
					
					$response_data = array (
							"message" => 'driver_dispatch',
							"status" => 1,
							'trip_details' => array (
									'trip_id' => $trip_details [0]->tripId,
									'job_card_id' => $trip_details [0]->jobCardId,
									'pickup_location' => $trip_details [0]->pickupLocation,
									'pickup_latitude' => $trip_details [0]->pickupLatitude,
									'pickup_longitude' => $trip_details [0]->pickupLongitude,
									'pickup_time' => $trip_details [0]->pickupDatetime,
									'drop_location' => $trip_details [0]->dropLocation,
									'drop_latitude' => $trip_details [0]->dropLatitude,
									'drop_longitude' => $trip_details [0]->dropLongitude,
									'drop_time' => $trip_details [0]->dropDatetime,
									'land_mark' => $trip_details [0]->landmark,
									'taxi_rating' => $trip_details [0]->passengerRating,
									'taxi_comments' => $trip_details [0]->passengerComments,
									'trip_type' => $trip_details [0]->tripType,
									'trip_type_name' => $trip_details [0]->tripTypeName,
									'payment_mode' => $trip_details [0]->paymentMode,
									'payment_mode_name' => $trip_details [0]->paymentModeName,
									'trip_status' => $trip_details [0]->tripStatus,
									'trip_status_name' => $trip_details [0]->tripStatusName,
									'promocode' => $trip_details [0]->promoCode,
									'is_driver_rated' => ($trip_details [0]->driverRating) ? TRUE : FALSE 
							),
							'passenger_details' => array (
									'passenger_id' => $trip_details [0]->passengerId,
									'passenger_firstname' => $trip_details [0]->passengerFirstName,
									'passenger_lastname' => $trip_details [0]->passengerLastName,
									'passenger_mobile' => $trip_details [0]->passengerMobile,
									'passenger_latitude' => $trip_details [0]->passengerCurrentLatitude,
									'passenger_longitude' => $trip_details [0]->passengerCurrentLongitude,
									'passenger_profile_image' => ($trip_details [0]->passengerProfileImage) ? (passenger_content_url ( $trip_details [0]->passengerId . '/' . $trip_details [0]->passengerProfileImage )) : image_url ( 'app/user.png' ) 
							),
							'driver_details' => array (
									'driver_id' => $trip_details [0]->driverId,
									'driver_firstname' => $trip_details [0]->driverFirstName,
									'driver_lastname' => $trip_details [0]->driverLastName,
									'driver_mobile' => $trip_details [0]->driverMobile,
									'driver_profile_image' => ($trip_details [0]->driverProfileImage) ? (driver_content_url ( $trip_details [0]->driverId . '/' . $trip_details [0]->driverProfileImage )) : image_url ( 'app/user.png' ),
									'driver_language' => $trip_details [0]->driverLanguagesKnown,
									'driver_experience' => $trip_details [0]->driverExperience,
									'driver_age' => $trip_details [0]->driverDob,
									'driver_latitute' => $trip_details [0]->currentLatitude,
									'driver_longtitute' => $trip_details [0]->currentLongitude,
									'driver_status' => $trip_details [0]->availabilityStatus 
							),
							'taxi_details' => array (
									'taxi_id' => $trip_details [0]->taxiId,
									'taxi_name' => $trip_details [0]->taxiName,
									'taxi_model' => $trip_details [0]->taxiModel,
									'taxi_manufacturer' => $trip_details [0]->taxiManufacturer,
									'taxi_registration_no' => $trip_details [0]->taxiRegistrationNo,
									'taxi_colour' => $trip_details [0]->taxiColour 
							),
							
							"estimated_time" => $trip_details [0]->estimatedTime,
							"distance_away" => number_format ( $trip_details [0]->estimatedDistance, 1 ),
							"calculate_distance_away" => number_format ( $trip_details [0]->estimatedDistance + 0.5, 1 ),
							"notification_time" => 15 
					);
					$post_data ['data'] = array (
							
							'details' => $response_data 
					);
				} else {
					$msg = $msg_data ['failed'];
					
					$post_data ['data'] = array (
							'message' => $msg,
							'status' => - 1 
					);
				}
				
				$post_data = json_encode ( $post_data );
				$url = "https://fcm.googleapis.com/fcm/send";
				
				$ch = curl_init ();
				curl_setopt ( $ch, CURLOPT_URL, $url );
				curl_setopt ( $ch, CURLOPT_HTTPHEADER, array (
						'Content-Type:application/json',
						'Authorization: key=' . $firebase_api_key 
				) );
				curl_setopt ( $ch, CURLOPT_POST, 1 );
				curl_setopt ( $ch, CURLOPT_POSTFIELDS, $post_data );
				curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
				$response = curl_exec ( $ch );
				curl_close ( $ch );
			}
		}
		return $response;
		// echo json_encode ( array('response'=>$response,'msg'=>$msg,'status'=>$status));
	}
	public function reserveDriverNotification($driver_id, $trip_id = NULL, $distance = 0) {
		// $gcm_id,$trip_id //Posted json data
		$gcm_id = '';
		// $trip_id = $post_data->trip_id;
		// $driver_id = 2;
		$response = NULL;
		$msg_data = $this->config->item ( 'api' );
		$msg = '';
		$status = 0;
		$firebase_api_key = DRIVER_FIREBASE_API_KEY;
		
		$post_data = array ();
		$response_data = array ();
		
		$driver_details = $this->Driver_Model->getById ( $driver_id );
		
		if ($driver_details) {
			if ($driver_details->gcmId) {
				$gcm_id = $driver_details->gcmId;
				$post_data ['registration_ids'] = ( array ) $gcm_id;
				if ($gcm_id) {
					/*
					 * $trip_details = $this->Trip_Details_Model->getById ( $trip_id );
					 * $passenger_details = $this->Passenger_Model->getById ( $trip_details->passengerId );
					 *
					 * $response_data = array (
					 * "message" => 'trip_dispatch',
					 * "status" => 1,
					 * "booking_details" => array (
					 * "trip_id" => $trip_details->id,
					 * "pickup_location" => $trip_details->pickupLocation,
					 * "pickup_latitude" => $trip_details->pickupLatitude,
					 * "pickup_longitude" => $trip_details->pickupLongitude,
					 * "pickup_time" => $trip_details->pickupDatetime,
					 * "drop_location" => $trip_details->dropLocation,
					 * "drop_latitude" => $trip_details->dropLatitude,
					 * "drop_longitude" => $trip_details->dropLongitude,
					 * "driver_id" => $driver_details->id,
					 * "driver_name" => $driver_details->firstName . ' ' . $driver_details->lastName,
					 * "driver_mobile" => $driver_details->mobile,
					 * "driver_profile_image" => ($driver_details->profileImage) ? (driver_content_url ( $driver_details->id . '/' . $driver_details->profileImage )) : image_url ( 'app/user.png' ),
					 * "passenger_id" => $trip_details->passengerId,
					 * "job_card_id" => $trip_details->jobCardId,
					 * "passenger_name" => $passenger_details->firstName . ' ' . $passenger_details->lastName,
					 * "passenger_mobile" => $passenger_details->mobile,
					 * "passenger_profile_image" => ($passenger_details->profileImage) ? (passenger_content_url ( $passenger_details->id . '/' . $passenger_details->profileImage )) : image_url ( 'app/user.png' ),
					 * "estimated_time" => 0,
					 * "distance_away" => $distance,
					 * "notification_time" => 15
					 * )
					 * );
					 */
					$response_data = array (
							"message" => 'reserve_notify',
							"status" => 1,
							"description" => 'Reserve notification' 
					);
					$post_data ['data'] = array (
							
							'details' => $response_data 
					);
				} else {
					$msg = $msg_data ['failed'];
					
					$post_data ['data'] = array (
							'message' => $msg,
							'status' => - 1 
					);
				}
				
				$post_data = json_encode ( $post_data );
				$url = "https://fcm.googleapis.com/fcm/send";
				
				$ch = curl_init ();
				curl_setopt ( $ch, CURLOPT_URL, $url );
				curl_setopt ( $ch, CURLOPT_HTTPHEADER, array (
						'Content-Type:application/json',
						'Authorization: key=' . $firebase_api_key 
				) );
				curl_setopt ( $ch, CURLOPT_POST, 1 );
				curl_setopt ( $ch, CURLOPT_POSTFIELDS, $post_data );
				curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
				$response = curl_exec ( $ch );
				curl_close ( $ch );
			}
		}
		return $response;
		// echo json_encode ( array('response'=>$response,'msg'=>$msg,'status'=>$status));
	}
	public function getNotificationList_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = array ();
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$notification_list = NULL;
		
		$response_data = array ();
		foreach ( getallheaders () as $name => $value ) {
			if ($name == 'auth_key') {
				$auth_key = $value;
			}
			if ($name == 'user_id') {
				$auth_user_id = $value;
			}
			if ($name == 'user_type') {
				$auth_user_type = $value;
			}
		}
		// Checked wheather authentication authKey availablilty for specfic user(passenger/driver)
		$data_auth_key = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
				'authKey' => $auth_key,
				'userId' => $auth_user_id,
				'userType' => $auth_user_type 
		), 'id' );
		if ($data_auth_key) {
			if ($data_auth_key->authKey == $auth_key) {
				// actual functonality
				
				$notification_list = $this->Notification_Model->getByKeyValueArray ( array (
						'startDatetime<=' => getCurrentDateTime (),
						'endDatetime>=' => getCurrentDateTime (),
						'isDeleted' => Status_Type_Enum::INACTIVE,
						'notificationTo' => User_Type_Enum::DRIVER,
						'status' => Status_Type_Enum::ACTIVE 
				) );
				if ($notification_list) {
					foreach ( $notification_list as $list ) {
						$response_data [] = array (
								'id' => $list->id,
								'title' => $list->title,
								'description' => $list->description,
								'start_date_time' => $list->startDatetime,
								'end_date_time' => $list->endDatetime,
								'noticed_date_time' => $list->createdDatetime 
						);
					}
					$message = array (
							"message" => $msg_data ['success'],
							"details" => $response_data,
							"status" => 1 
					);
				} else {
					$message = array (
							"message" => $msg_data ['data_not_found'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['auth_failed'],
						"status" => 401 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['auth_failed'],
					"status" => 401 
			);
		}
		echo json_encode ( $message );
	}
}