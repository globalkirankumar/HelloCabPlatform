<?php
class Driver extends My_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->helper ( 'form' );
		$this->load->model ( 'Taxi_Query_Model' );
		$this->load->model ( 'Entity_Config_Model' );
		$this->load->model ( 'Entity_Model' );
		$this->load->model ( 'Driver_Model' );
		$this->load->model ( 'Taxi_Device_Details_Model' );
		$this->load->model ( 'Taxi_Device_Install_History_Model' );
		$this->load->model ( 'Taxi_Details_Model' );
		$this->load->model ( 'Driver_Taxi_Mapping_Model' );
		$this->load->model ( 'Zone_Details_Model' );
		$this->load->model ( 'Driver_Personal_Details_Model' );
		$this->load->model ( 'Driver_Transaction_Details_Model' );
		$this->load->model ( 'Driver_Dispatch_Details_Model' );
		$this->load->model ( 'Data_Attributes_Model' );
		$this->load->model ( 'Sms_Template_Model' );
		$this->load->model ( 'Common_Api_Webservice_Model' );
		$this->load->model ( 'Driver_Shift_History_Model' );
		$this->load->model ( 'Emergency_Request_Details_Model' );
	}
	public function index() {
	}
	public function add() {
		if ($this->User_Access_Model->createModule ( $this->session->userdata ( 'role_type' ), Module_Name_Enum::DRIVER ) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		
		$this->addJs ( 'app/driver.js' );
		$data = array (
				'mode' => EDIT_MODE 
		);
		$data ['activetab'] = "driver";
		$data ['active_menu'] = 'driver';
		$data ['zone_list'] = $this->Zone_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE 
		), 'name' );
		$data ['entity_list'] = $this->Entity_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE 
		), 'name' );
		$data ['gender_list'] = $this->Data_Attributes_Model->getColumnByKeyValueArray ( 'id,description', array (
				'tableName' => strtoupper ( 'GENDER_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		$data ['license_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'LICENSE_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		$data ['experience_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'EXPERIENCE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		
		$data ['entity_config_model'] = $this->Entity_Config_Model->getOneByKeyValueArray ( array (
				'entityId' => DEFAULT_COMPANY_ID 
		) );
		$data ['driver_model'] = $this->Driver_Model->getBlankModel ();
		$data ['driver_personal_model'] = $this->Driver_Personal_Details_Model->getBlankModel ();
		$this->render ( 'driver/add_edit_driver', $data );
	}
	public function saveDriver() {
		$this->addJs ( 'app/driver.js' );
		$data = array ();
		$data = $this->input->post ();
		// $data ['profileImage']= $this->input->post ('profileImage');
		
		$driver_id = $data ['id'];
		
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['failed'];
		if ($driver_id > 0) {
			
			$this->Driver_Model->update ( $data, array (
					'id' => $driver_id 
			) );
			$this->Driver_Personal_Details_Model->update ( $data, array (
					'driverId' => $driver_id 
			) );
			
			$file_names = array_keys ( $_FILES );
			// debug_exit($file_names);
			
			$police_img_selected = $_FILES ['policeRecommendationImage'] ['name'];
			$profile_img_selected = $_FILES ['profileImage'] ['name'];
			$license_img_selected = $_FILES ['licenseImage'] ['name'];
			$addproof_img_selected = $_FILES ['nationRegistrationImage'] ['name'];
			$home_img_selected = $_FILES ['homeRecommendationImage'] ['name'];
			$family_img_selected = $_FILES ['familyMemberListImage'] ['name'];
			$contract_paper_img_selected = $_FILES ['contractPaperImage'] ['name'];
			
			// $cheque_img_selected = isset($_FILES ['chequeImage'] ['name']) ? $_FILES ['chequeImage'] ['name'] : '' ;
			
			// License Image Upload
			if ($license_img_selected [0] != '') {
				$prefix = "LICENSE-" . $driver_id;
				$upload_path = $this->config->item ( 'driver_content_path' );
				$file_upload_path = $upload_path . $driver_id;
				file_upload ( $file_names [2], $driver_id, $license_img_selected, $prefix, $file_upload_path, 'Driver_Model', 'id' );
			}
			// Family Image Upload
			if ($family_img_selected [0] != '') {
				$prefix = "FAMILY-" . $driver_id;
				$upload_path = $this->config->item ( 'driver_content_path' );
				$file_upload_path = $upload_path . $driver_id;
				file_upload ( $file_names [5], $driver_id, $family_img_selected, $prefix, $file_upload_path, 'Driver_Personal_Details_Model', 'driverId' );
			}
			// Police Document Upload
			if ($police_img_selected [0] != '') {
				$prefix = "POLICE-" . $driver_id;
				$upload_path = $this->config->item ( 'driver_content_path' );
				$file_upload_path = $upload_path . $driver_id;
				file_upload ( $file_names [0], $driver_id, $police_img_selected, $prefix, $file_upload_path, 'Driver_Model', 'id' );
			}
			// Profile image upload
			if ($profile_img_selected [0] != '') {
				// in which name img should be uploaded
				$prefix = "PROFILE-" . $driver_id;
				$upload_path = $this->config->item ( 'driver_content_path' );
				$file_upload_path = $upload_path . $driver_id;
				file_upload ( $file_names [1], $driver_id, $profile_img_selected, $prefix, $file_upload_path, 'Driver_Model', 'id' );
			}
			// Address Proof Image Upload
			if ($addproof_img_selected [0] != '') {
				$prefix = "NATIONREGISTRATION-" . $driver_id;
				$upload_path = $this->config->item ( 'driver_content_path' );
				$file_upload_path = $upload_path . $driver_id;
				file_upload ( $file_names [3], $driver_id, $addproof_img_selected, $prefix, $file_upload_path, 'Driver_Personal_Details_Model', 'driverId' );
			}
			// ID Proof Image Upload
			if ($home_img_selected [0] != '') {
				$prefix = "HOME-" . $driver_id;
				$upload_path = $this->config->item ( 'driver_content_path' );
				$file_upload_path = $upload_path . $driver_id;
				file_upload ( $file_names [4], $driver_id, $home_img_selected, $prefix, $file_upload_path, 'Driver_Personal_Details_Model', 'driverId' );
			}
			// Contract paper card Image Upload
			if ($contract_paper_img_selected [0] != '') {
				$prefix = "CONTRACT-PAPER-" . $driver_id;
				$upload_path = $this->config->item ( 'driver_content_path' );
				$file_upload_path = $upload_path . $driver_id;
				file_upload ( $file_names [6], $driver_id, $contract_paper_img_selected, $prefix, $file_upload_path, 'Driver_Personal_Details_Model', 'driverId' );
			}
			
			$msg = $msg_data ['updated'];
		} else {
			$entity_config_details = $this->Entity_Config_Model->getOneByKeyValueArray ( array (
					'entityId' => $data ['entityId'] 
			) );
			// $password = random_string ( 'alnum', 8 );
			$password = 'hellocabs';
			$data ['password'] = hash ( "sha256", $password );
			
			$data ['otp'] = str_pad ( rand ( 0, 999999 ), 6, '0', STR_PAD_LEFT );
			
			$data ['activationStatus'] = Status_Type_Enum::ACTIVE;
			$data ['signupFrom'] = Signup_Type_Enum::BACKEND;
			$data ['registerType'] = Register_Type_Enum::MANUAL;
			$data ['deviceType'] = Device_Type_Enum::NO_DEVICE;
			$data ['driverWallet'] = $entity_config_details->driverMinWallet;
			
			$driver_id = $this->Driver_Model->insert ( $data );
			// update the Unique Driver Code
			$driver_code_id = ($driver_id <= 999999) ? str_pad ( ( string ) $driver_id, 6, '0', STR_PAD_LEFT ) : $driver_id;
			
			$driver_code = 'DRV' . $driver_code_id;
			$update_driver_code = $this->Driver_Model->update ( array (
					'driverCode' => $driver_code 
			), array (
					'id' => $driver_id 
			) );
			
			$data ['driverId'] = $driver_id;
			$driver_personal_details_id = NULL;
			
			if ($driver_id > 0) {
				$driver_personal_details_id = $this->Driver_Personal_Details_Model->insert ( $data );
				
				$file_names = array_keys ( $_FILES );
				
				$police_img_selected = $_FILES ['policeRecommendationImage'] ['name'];
				$profile_img_selected = $_FILES ['profileImage'] ['name'];
				$license_img_selected = $_FILES ['licenseImage'] ['name'];
				$addproof_img_selected = $_FILES ['nationRegistrationImage'] ['name'];
				$home_img_selected = $_FILES ['homeRecommendationImage'] ['name'];
				$family_img_selected = $_FILES ['familyMemberListImage'] ['name'];
				$contract_paper_img_selected = $_FILES ['contractPaperImage'] ['name'];
				
				// License Image Upload
				if ($license_img_selected [0] != '') {
					$prefix = "LICENSE-" . $driver_id;
					$upload_path = $this->config->item ( 'driver_content_path' );
					$file_upload_path = $upload_path . $driver_id;
					file_upload ( $file_names [2], $driver_id, $license_img_selected, $prefix, $file_upload_path, 'Driver_Model', 'id' );
				}
				// Family Image Upload
				if ($family_img_selected [0] != '') {
					$prefix = "FAMILY-" . $driver_id;
					$upload_path = $this->config->item ( 'driver_content_path' );
					$file_upload_path = $upload_path . $driver_id;
					file_upload ( $file_names [5], $driver_id, $family_img_selected, $prefix, $file_upload_path, 'Driver_Personal_Details_Model', 'driverId' );
				}
				
				// Police Document Upload
				if ($police_img_selected [0] != '') {
					$prefix = "POLICE-" . $driver_id;
					$upload_path = $this->config->item ( 'driver_content_path' );
					$file_upload_path = $upload_path . $driver_id;
					file_upload ( $file_names [0], $driver_id, $police_img_selected, $prefix, $file_upload_path, 'Driver_Model', 'id' );
				}
				// Profile image upload
				if ($profile_img_selected [0] != '') {
					// in which name img should be uploaded
					$prefix = "PROFILE-" . $driver_id;
					$upload_path = $this->config->item ( 'driver_content_path' );
					$file_upload_path = $upload_path . $driver_id;
					file_upload ( $file_names [1], $driver_id, $profile_img_selected, $prefix, $file_upload_path, 'Driver_Model', 'id' );
				}
				// Address Proof Image Upload
				if ($addproof_img_selected [0] != '') {
					$prefix = "NATIONREGISTRATION-" . $driver_id;
					$upload_path = $this->config->item ( 'driver_content_path' );
					$file_upload_path = $upload_path . $driver_id;
					file_upload ( $file_names [3], $driver_id, $addproof_img_selected, $prefix, $file_upload_path, 'Driver_Personal_Details_Model', 'driverId' );
				}
				// Home Proof Image Upload
				if ($home_img_selected [0] != '') {
					$prefix = "HOME-" . $driver_id;
					$upload_path = $this->config->item ( 'driver_content_path' );
					$file_upload_path = $upload_path . $driver_id;
					file_upload ( $file_names [4], $driver_id, $home_img_selected, $prefix, $file_upload_path, 'Driver_Personal_Details_Model', 'driverId' );
				}
				// Contract paper Image Upload
				if ($contract_paper_img_selected [0] != '') {
					$prefix = "CONTRACT-PAPER-" . $driver_id;
					$upload_path = $this->config->item ( 'driver_content_path' );
					$file_upload_path = $upload_path . $driver_id;
					file_upload ( $file_names [6], $driver_id, $contract_paper_img_selected, $prefix, $file_upload_path, 'Driver_Personal_Details_Model', 'driverId' );
				}
				$driver_dispatch_exists = NULL;
				$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
						'driverId' => $driver_id 
				) );
				if ($driver_dispatch_exists) {
					$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
							'walletBalance' => $entity_config_details->driverMinWallet,
							'status' => Status_Type_Enum::ACTIVE,
							'availabilityStatus' => Taxi_Available_Status_Enum::BUSY,
							'isEligible' => Status_Type_Enum::INACTIVE,
					), array (
							'driverId' => $driver_id 
					) );
				} else {
					$insert_dispatch_data = array (
							'driverId' => $driver_id,
							'walletBalance' => $entity_config_details->driverMinWallet,
							'status' => Status_Type_Enum::ACTIVE,
							'availabilityStatus' => Taxi_Available_Status_Enum::BUSY,
							'isEligible' => Status_Type_Enum::INACTIVE,
					);
					$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
				}
				
				$msg = $msg_data ['success'];
			}
			if ($driver_id > 0 && $driver_personal_details_id) {
				// SMS & email service start
				
				if (SMS && $driver_id) {
					$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
							'title' => 'account_create_sms' 
					) );
					
					$message_data = $message_details->content;
					
					$message_data = str_replace ( "##USERNAME##", $data ['mobile'], $message_data );
					$message_data = str_replace ( "##PASSWORD##", $password, $message_data );
					$message_data = str_replace ( "##APPLINK##", APPLINK, $message_data );
					
					// print_r($message);exit;
					if ($data ['mobile'] != "") {
						
						$this->Common_Api_Webservice_Model->sendSMS ( $data ['mobile'], $message_data );
					}
				}
				
				// SMS & email service end
			}
		}
		
		// $this->render("passenger/add_edit_passenger", $data);
		
		$response = array (
				'msg' => $msg,
				'driver_id' => $driver_id 
		);
		echo json_encode ( $response );
	}
	public function getDetailsById($driver_id, $view_mode) {
		if ($view_mode == EDIT_MODE) {
			if ($this->User_Access_Model->writeModule ( $this->session->userdata ( 'role_type' ), Module_Name_Enum::DRIVER ) === FALSE) {
				// redirect 404
				show_error ( "You do not have permission to view this page", '404' );
				return;
			}
		} else {
			if ($this->User_Access_Model->readModule ( $this->session->userdata ( 'role_type' ), Module_Name_Enum::DRIVER ) === FALSE) {
				// redirect 404
				show_error ( "You do not have permission to view this page", '404' );
				return;
			}
		}
		$data = array ();
		$msg_data = $this->config->item ( 'msg' );
		
		$this->addCss ( 'vendors/lightbox/css/imagelightbox.css' );
		$this->addJs ( 'vendors/lightbox/js/imagelightbox.js' );
		$this->addJs ( 'app/driver.js' );
		
		$data = array (
				'mode' => $view_mode 
		);
		// print_r($data);exit;
		$data ['activetab'] = "driver";
		$data ['active_menu'] = 'driver';
		$data ['zone_list'] = $this->Zone_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE 
		), 'name' );
		$data ['entity_list'] = $this->Entity_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE 
		), 'name' );
		
		$data ['gender_list'] = $this->Data_Attributes_Model->getColumnByKeyValueArray ( 'id,description', array (
				'tableName' => strtoupper ( 'GENDER_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		$data ['license_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'LICENSE_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		$data ['experience_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'EXPERIENCE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		if ($this->input->post ( 'id' )) {
			$driver_id = $this->input->post ( 'id' );
		}
		/**
		 * code to redirect driver list for invlaid id *
		 */
		$data ['validate_id'] = $this->Driver_Model->getById ( $driver_id );
		$check_valid_driver_id = count ( $data ['validate_id'] );
		if ($check_valid_driver_id == 0) {
			redirect ( base_url () . 'Driver/getDriverList' );
			exit ();
		}
		if ($driver_id > 0) {
			if ($view_mode == EDIT_MODE) {
				$locked = $this->Driver_Model->isLocked ( $driver_id );
				if ($locked) {
					$locked_user = $this->Driver_Model->getLockedUser ( $driver_id );
					$locked_date = $this->Driver_Model->getLockedDate ( $driver_id );
					$locked_date = date ( 'jS, F Y G:ia', strtotime ( $locked_date ) );
					$message = $msg_data ['locked'];
					$message = str_replace ( '##USERNAME##', $locked_user, $message );
					$message .= $locked_date;
					
					$data ['mode'] = VIEW_MODE;
				} else {
					$is_locked = $this->Driver_Model->lock ( $driver_id );
				}
			}
			
			$data ['driver_model'] = $this->Driver_Model->getById ( $driver_id );
			$data ['driver_personal_model'] = $this->Driver_Personal_Details_Model->getOneByKeyValueArray ( array (
					'driverId' => $data ['driver_model']->id 
			) );
		} else {
			// Blank Model
			$data ['driver_model'] = $this->driver_model->getBlankModel ();
			$data ['driver_personal_model'] = $this->Driver_Personal_Details_Model->getBlankModel ();
		}
		if ($this->input->post ( 'id' )) {
			
			$html = $this->load->view ( 'driver/add_edit_driver', $data );
			echo json_encode ( array (
					'html' => $html 
			) );
		} else {
			$this->render ( 'driver/add_edit_driver', $data );
		}
	}
	public function getDriverlist() {
		if ($this->User_Access_Model->showDriver () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		
		$this->addCss ( 'vendors/lightbox/css/imagelightbox.css' );
		$this->addJs ( 'vendors/lightbox/js/imagelightbox.js' );
		$this->addJs ( 'app/driver.js' );
		
		$data ['activetab'] = "driver";
		$data ['active_menu'] = 'driver';
		$data ['taxi_list'] = $this->Taxi_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE,
				'isAllocated' => Status_Type_Enum::INACTIVE,
				'isDeleted' => Status_Type_Enum::INACTIVE 
		), 'id' );
		$data ['taxi_device_list'] = $this->Taxi_Device_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE,
				'isAllocated' => Status_Type_Enum::INACTIVE,
				'isDeleted' => Status_Type_Enum::INACTIVE,
				'deviceStatus' => Device_Status_Enum::OPEN 
		), 'id' );
		
		$this->render ( 'driver/manage_driver', $data );
	}
	public function ajax_list() {
		// echo"ajaxlist";exit;
		$entity_id = $this->session->userdata ( 'user_entity' );
		$sql_query = $this->Driver_Model->getDriverListQuery ( $entity_id );
		$driver_list = $this->Driver_Model->getDataTable ( $sql_query );
		$data = array ();
		$no = $_POST ['start'];
		foreach ( $driver_list as $list ) {
			$no ++;
			$profileimage = ($list->profileImage) ? driver_content_url ( $list->driverId . "/" . $list->profileImage ) : image_url ( 'app/user.png' );
			$row = array ();
			$row [] = '<button class="btn btn-sm btn-success" id="viewdriver-' . $list->driverId . '" title="View" >
                                <i class="fa fa-eye" aria-hidden="true"></i></button> 
                                <button class="btn btn-sm btn-blue" id="editdriver-' . $list->driverId . '" title="Edit">
                                <i class="fa fa-pencil-square-o"></i></button><button class="btn btn-sm btn-danger" id="deletedriver-' . $list->driverId . '" title="Delete">
                                <i class="fa fa-trash-o"></i></button>
                                	<button class="btn btn-sm btn-warning" id="viewtransaction-' . $list->driverId . '" title="Transaction Details" >
                                 <i class="fa fa-exchange" aria-hidden="true"></i></button>
                       <button class="btn btn-sm btn-success" id="viewloghistory-' . $list->driverId . '" title="Log History" >
                                 <i class="fa fa-history" aria-hidden="true"></i></button>';
			
			$row [] = '<a class="example-image-link" href="' . $profileimage . '" data-imagelightbox="listview" data-ilb2-caption="profileImage">' . '<img src="' . $profileimage . '" width="40" height="40"></a>';
			$row [] = $list->driverCode;
			$row [] = ucfirst ( $list->firstName );
			$row [] = ucfirst ( $list->lastName );
			$row [] = $list->mobile;
			$row [] = $list->driverWallet;
			$row [] = $list->driverCredit;
			$row [] = $list->taxiRegistrationNo;
			$row [] = $list->deviceMobile;
			$row [] = round ( $list->driverAvgRating );
			$row [] = $list->experience;
			$row [] = $list->languageKnown;
			$row [] = $list->qualification;
			$row [] = $list->email;
			$row [] = $list->licenseNumber;
			$row [] = $list->licenseExpiryDate;
			$row [] = $list->licenseType;
			$row [] = $list->address;
			$row [] = $list->dob;
			$row [] = $list->gender;
			$row [] = ucfirst ( $list->bankName );
			$row [] = $list->bankBranch;
			$row [] = $list->bankAccountNo;
			$row [] = $list->groupType;
			$row [] = $list->behaviourType;
			$row [] = ucfirst ( $list->zoneName );
			$row [] = $list->lastLoggedIn;
			$log_status_class = ($list->loginStatus) ? 'class="btn btn-success"' : 'class="btn btn-danger"';
			$log_status_name = ($list->loginStatus) ? 'In' : 'Out';
			$row [] = '<button id="logstatus-' . $list->driverId . '" status="' . $list->loginStatus . '" ' . $log_status_class . '><span>' . $log_status_name . '</span></button>';
			$verify_status_class = ($list->isVerified) ? 'class="btn btn-success"' : 'class="btn btn-danger"';
			$verify_status_name = ($list->isVerified) ? 'Yes' : 'No';
			$row [] = '<button id="isverify-' . $list->driverId . '" status="' . $list->isVerified . '" ' . $verify_status_class . '><span>' . $verify_status_name . '</span></button>';
			
			$row [] = $list->appVersion;
			$row [] = date ( 'Y-m-d', strtotime ( $list->driverJoinedDate ) );
			$row [] = ($list->driverAllocated && $list->status) ? 'Not Resigned' : date ( 'Y-m-d', strtotime ( $list->driverResignededDate ) );
			
			$status_class = ($list->status) ? 'class="btn btn-success"' : 'class="btn btn-danger"';
			$status_name = ($list->status) ? 'Active' : 'Inactive';
			$row [] = '<button id="status-' . $list->driverId . '" status="' . $list->status . '" ' . $status_class . '><span>' . $status_name . '</span></button>';
			
			$resign_class = ($list->driverAllocated && $list->status) ? 'class="btn btn-success"' : 'class="btn btn-danger"';
			$resign_name = ($list->driverAllocated && $list->status) ? 'Resign' : 'Resigned';
			$resign_id = ($list->driverAllocated && $list->status) ? 'id="resign-' . $list->driverId . '"' : '';
			$row [] = '<button '.$resign_id.'" " ' . $resign_class . '><span>' . $resign_name . '</span></button>';
			
			$allocate_class = ($list->driverAllocated) ? 'class="btn btn-success"' : 'class="btn btn-danger"';
			$allocat_name = (! $list->driverAllocated) ? 'Allocate' : 'Re-Allocate';
			$driver_full_name = ucfirst ( $list->firstName ) . ' ' . ucfirst ( $list->lastName );
			$row [] = '<span id="allocatetaxi-' . $list->driverId . '" allocateStatus="' . $list->driverAllocated . '" 
                                 taxiDeviceId="' . $list->taxiDeviceId . '" driverName="' . $driver_full_name . '" 
                                 taxiDeviceCode="' . $list->taxiDeviceCode . '" taxiRegistrationNo="' . $list->taxiRegistrationNo . '"
                                 taxiId="' . $list->taxiId . '" ' . $allocate_class . '>' . $allocat_name . '</span>';
			// add html for action
			
			$data [] = $row;
		}
		
		$output = array (
				"draw" => $_POST ['draw'],
				"recordsTotal" => $this->Driver_Model->getDataTableAllCount ( $sql_query ),
				"recordsFiltered" => $this->Driver_Model->getDataTableFilterCount ( $sql_query ),
				"data" => $data 
		);
		// output to json format
		echo json_encode ( $output );
	}
	public function changeDriverStatus() {
		if ($this->User_Access_Model->fullAccessModule ( $this->session->userdata ( 'role_type' ), Module_Name_Enum::DRIVER ) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to change staus", '404' );
			return;
		}
		$data = array ();
		
		$this->addJs ( 'app/driver.js' );
		$user_updated = - 1;
		$data = $this->input->post ();
		$user_id = $data ['id'];
		$user_status = $data ['status'];
		
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['status_change_failed'];
		if ($user_id) {
			$user_updated = $this->Driver_Model->update ( array (
					'status' => $user_status 
			), array (
					'id' => $user_id 
			) );
			if ($user_updated) {
				$driver_exists = $this->Driver_Model->getById ( $user_id );
				$entity_config_details = $this->Entity_Config_Model->getOneByKeyValueArray ( array (
						'entityId' => $driver_exists->entityId 
				) );
				$driver_consective_reject = NULL;
				$driver_consective_reject = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
						'driverId' => $user_id 
				) );
				if ($driver_consective_reject->consecutiveRejectCount >= $entity_config_details->drtSecondLimit) {
					if ($user_status)
					{
					$update_data=array(
							'consecutiveRejectCount' => --$driver_consective_reject->consecutiveRejectCount,
							'availabilityStatus'=>Taxi_Available_Status_Enum::FREE,
							'isEligible' => Status_Type_Enum::ACTIVE,
							'status' => $user_status );
					}
					else 
					{
						$update_data=array(
								'availabilityStatus'=>Taxi_Available_Status_Enum::BUSY,
								'isEligible' => Status_Type_Enum::INACTIVE,
								'status' => $user_status );
					}
					$this->Driver_Dispatch_Details_Model->update ( $update_data, array (
							'driverId' => $user_id 
					) );
					
				} else {
					if ($user_status)
					{
					$update_data=array(
							'availabilityStatus'=>Taxi_Available_Status_Enum::FREE,
							'isEligible' => Status_Type_Enum::ACTIVE,
							'status' => $user_status );
					}
					else 
					{
						$update_data=array(
								'availabilityStatus'=>Taxi_Available_Status_Enum::BUSY,
								'isEligible' => Status_Type_Enum::INACTIVE,
								'status' => $user_status );
					}
					$this->Driver_Dispatch_Details_Model->update ( $update_data, array (
							'driverId' => $user_id 
					) );
				}
			}
			$msg = $msg_data ['status_change_success'];
		}
		$response = array (
				'msg' => $msg,
				'status' => $user_updated 
		);
		echo json_encode ( $response );
	}
	public function changeDriverLogStatus() {
		if ($this->User_Access_Model->fullAccessModule ( $this->session->userdata ( 'role_type' ), Module_Name_Enum::DRIVER ) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to change status", '404' );
			return;
		}
		$data = array ();
		
		$this->addJs ( 'app/driver.js' );
		$driver_updated = - 1;
		$data = $this->input->post ();
		$user_id = $data ['id'];
		$user_status = $data ['status'];
		
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['status_change_failed'];
		if ($user_id) {
			$driver_updated = $this->Driver_Model->update ( array (
					'loginStatus' => $user_status
			), array (
					'id' => $user_id
			) );
			if ($user_status)
			{
				$update_data=array(
						'availabilityStatus'=>Taxi_Available_Status_Enum::FREE,
						'isEligible' => Status_Type_Enum::ACTIVE);
			}
			else
			{
				$update_data=array(
						'availabilityStatus'=>Taxi_Available_Status_Enum::BUSY,
						'isEligible' => Status_Type_Enum::INACTIVE);
			}
			$this->Driver_Dispatch_Details_Model->update ( $update_data, array (
					'driverId' => $user_id
			) );
			
			$msg = $msg_data ['status_change_success'];
		}
		$response = array (
				'msg' => $msg,
				'status' => $driver_updated 
		);
		echo json_encode ( $response );
	}
	public function changeDriverVerifyStatus() {
		if ($this->User_Access_Model->fullAccessModule ( $this->session->userdata ( 'role_type' ), Module_Name_Enum::DRIVER ) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to change status", '404' );
			return;
		}
		$data = array ();
		
		$this->addJs ( 'app/driver.js' );
		$driver_updated = - 1;
		$data = $this->input->post ();
		$user_id = $data ['id'];
		$user_status = $data ['status'];
		
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['status_change_failed'];
		if ($user_id) {
			$driver_updated = $this->Driver_Model->update ( array (
					'isVerified' => $user_status 
			), array (
					'id' => $user_id 
			) );
			if ($user_status)
			{
				$update_data=array(
						'availabilityStatus'=>Taxi_Available_Status_Enum::FREE,
						'isEligible' => Status_Type_Enum::ACTIVE
						 );
			}
			else
			{
				$update_data=array(
						'availabilityStatus'=>Taxi_Available_Status_Enum::BUSY,
						'isEligible' => Status_Type_Enum::INACTIVE);
			}
			$this->Driver_Dispatch_Details_Model->update ( $update_data, array (
					'driverId' => $user_id
			) );
			$msg = $msg_data ['status_change_success'];
		}
		$response = array (
				'msg' => $msg,
				'status' => $driver_updated 
		);
		echo json_encode ( $response );
	}
	public function deleteDriver() {
		if ($this->User_Access_Model->deleteModule ( $this->session->userdata ( 'role_type' ), Module_Name_Enum::DRIVER ) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to delete", '404' );
			return;
		}
		$data = array ();
		
		$this->addJs ( 'app/driver.js' );
		$driver_updated = - 1;
		$data = $this->input->post ();
		$driver_id = $data ['id'];
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['delete_failed'];
		if ($driver_id) {
			$driver_updated = $this->Driver_Model->update ( array (
					'isDeleted' => Status_Type_Enum::ACTIVE 
			), array (
					'id' => $driver_id 
			) );
			$allocated_taxi_details = $this->Driver_Taxi_Mapping_Model->getOneByKeyValueArray ( array (
					'driverId' => $driver_id 
			) );
			if ($allocated_taxi_details) {
				/**
				 * *** Update previous Taxi device Id isALLOCATED to 0 ****
				 */
				$taxi_device_update = $this->Taxi_Device_Details_Model->update ( array (
						'isAllocated' => Status_Type_Enum::INACTIVE 
				), array (
						'id' => $allocated_taxi_details->taxiDeviceId 
				) );
				
				/**
				 * *** Update previous Taxi Id isALLOCATED to 0 ****
				 */
				$taxi_update = $this->Taxi_Details_Model->update ( array (
						'isAllocated' => Status_Type_Enum::INACTIVE 
				), array (
						'id' => $allocated_taxi_details->taxiId 
				) );
				
				// update the driver group type to empty because driver is deleted
				$update_driver_group = $this->Driver_Personal_Details_Model->update ( array (
						'groupType' => '' 
				), array (
						'driverId' => $driver_id 
				) );
				
				// first uninstall previous device before installing new device to taxi
				// get latest device installation history
				$latest_istallation_history = $this->Taxi_Device_Install_History_Model->getOneByKeyValueArray ( array (
						'taxiId' => $allocated_taxi_details->taxiId,
						'taxiDeviceId' => $allocated_taxi_details->taxiDeviceId 
				), 'id' );
				if ($latest_istallation_history) {
					$update_device_history = array (
							'uninstalledDatetime' => getCurrentDateTime () 
					);
					$device_history_updated = $this->Taxi_Device_Install_History_Model->update ( array (
							'uninstalledDatetime' => getCurrentDateTime () 
					), array (
							'id' => $latest_istallation_history->id 
					) );
				}
			}
			$updated_data = array (
					'taxiDeviceId' => 0,
					'taxiId' => 0,
					'taxiOwnerId' => 0 
			);
			$updated_mapping_id = $this->Driver_Taxi_Mapping_Model->update ( $updated_data, array (
					'driverId' => $driver_id 
			) );
			
			$msg = $msg_data ['delete_success'];
		}
		$response = array (
				'msg' => $msg,
				'status' => $driver_updated 
		);
		echo json_encode ( $response );
	}
	public function resignDriver() {
		$data = array ();
		
		$this->addJs ( 'app/driver.js' );
		$driver_updated = - 1;
		$data = $this->input->post ();
		$driver_id = $data ['id'];
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['resign_failed'];
		if ($driver_id) {
			$driver_updated = $this->Driver_Model->update ( array (
					'status' => Status_Type_Enum::INACTIVE 
			), array (
					'id' => $driver_id 
			) );
			$allocated_taxi_details = $this->Driver_Taxi_Mapping_Model->getOneByKeyValueArray ( array (
					'driverId' => $driver_id 
			) );
			if ($allocated_taxi_details) {
				/**
				 * *** Update previous Taxi device Id isALLOCATED to 0 ****
				 */
				$taxi_device_update = $this->Taxi_Device_Details_Model->update ( array (
						'isAllocated' => Status_Type_Enum::INACTIVE 
				), array (
						'id' => $allocated_taxi_details->taxiDeviceId 
				) );
				
				/**
				 * *** Update previous Taxi Id isALLOCATED to 0 ****
				 */
				$taxi_update = $this->Taxi_Details_Model->update ( array (
						'isAllocated' => Status_Type_Enum::INACTIVE 
				), array (
						'id' => $allocated_taxi_details->taxiId 
				) );
				
				// update the driver group type to empty because driver is resigned
				$update_driver_group = $this->Driver_Personal_Details_Model->update ( array (
						'groupType' => '' 
				), array (
						'driverId' => $driver_id 
				) );
				
				// first uninstall previous device before installing new device to taxi
				// get latest device installation history
				$latest_istallation_history = $this->Taxi_Device_Install_History_Model->getOneByKeyValueArray ( array (
						'taxiId' => $allocated_taxi_details->taxiId,
						'taxiDeviceId' => $allocated_taxi_details->taxiDeviceId 
				), 'id' );
				if ($latest_istallation_history) {
					$update_device_history = array (
							'uninstalledDatetime' => getCurrentDateTime () 
					);
					$device_history_updated = $this->Taxi_Device_Install_History_Model->update ( array (
							'uninstalledDatetime' => getCurrentDateTime () 
					), array (
							'id' => $latest_istallation_history->id 
					) );
				}
			}
			$updated_data = array (
					'taxiDeviceId' => 0,
					'taxiId' => 0,
					'taxiOwnerId' => 0 
			);
			$updated_mapping_id = $this->Driver_Taxi_Mapping_Model->update ( $updated_data, array (
					'driverId' => $driver_id 
			) );
			
			$msg = $msg_data ['resign_success'];
		}
		$response = array (
				'msg' => $msg,
				'status' => $driver_updated 
		);
		echo json_encode ( $response );
	}
	public function checkDriverEmail() {
		$data = array ();
		
		$data = $this->input->post ();
		$email = $data ['email'];
		$driver_id = $data ['driver_id'];
		$email_exists = NULL;
		$status = 0;
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['email_not_exists'];
		if ($email) {
			if ($driver_id > 0) {
				$email_exists = $this->Driver_Model->getOneByKeyValueArray ( array (
						'email' => $email,
						'id !=' => $driver_id,
						'isDeleted' => Status_Type_Enum::INACTIVE 
				) );
			} else {
				$email_exists = $this->Driver_Model->getOneByKeyValueArray ( array (
						'email' => $email,
						'isDeleted' => Status_Type_Enum::INACTIVE 
				) );
			}
			if ($email_exists) {
				$status = 1;
				$msg = $msg_data ['email_exists'];
			}
		}
		$response = array (
				'msg' => $msg,
				'email' => $email,
				'status' => $status 
		);
		echo json_encode ( $response );
	}
	public function checkDriverMobile() {
		$data = array ();
		
		$data = $this->input->post ();
		$mobile = $data ['mobile'];
		$driver_id = $data ['driver_id'];
		$mobile_exists = NULL;
		$status = 0;
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['mobile_not_exists'];
		if ($mobile) {
			if ($driver_id > 0) {
				$mobile_exists = $this->Driver_Model->getOneByKeyValueArray ( array (
						'mobile' => $mobile,
						'id !=' => $driver_id,
						'isDeleted' => Status_Type_Enum::INACTIVE 
				) );
			} else {
				
				$mobile_exists = $this->Driver_Model->getOneByKeyValueArray ( array (
						'mobile' => $mobile,
						'isDeleted' => Status_Type_Enum::INACTIVE 
				) );
			}
			if ($mobile_exists) {
				$status = 1;
				$msg = $msg_data ['mobile_exists'];
			}
		}
		$response = array (
				'msg' => $msg,
				'email' => $mobile,
				'status' => $status 
		);
		echo json_encode ( $response );
	}
	
	/*
	 * public function profile_image_upload($file_names, $user_id, $profile_img_selected) {
	 * // to get file input name from the form
	 * $profile_img_form_name = $file_names [0];
	 * // in which name img should be uploaded
	 * $profile_image_name = "PROFILE-" . $user_id;
	 * // upload path
	 * $folderName = $user_id;
	 * $profile_upload_path = $this->config->item ( 'user_content_path' ) ."/". $user_id;
	 *
	 * if (! file_exists ( $profile_upload_path )) {
	 * mkdir ( $profile_upload_path, 0777 );
	 * }
	 * // $profile_upload_path = driver_profile_url();
	 * $upload = $this->do_upload ( $profile_img_form_name, $profile_img_selected, $profile_image_name, $profile_upload_path );
	 * $upload_image_name = implode ( ',', $upload );
	 * if ($upload) {
	 * $data ['profileImage'] = $upload_image_name;
	 * $update_prof_imgname = $this->Driver_Model->update ( $data, array (
	 * 'id' => $user_id
	 * ) );
	 * }
	 * }
	 * // driver document image upload
	 * public function doc_image_upload($img_form_name, $driver_id, $img_selected, $img_prefix, $upload_path) {
	 * if (! file_exists ( $upload_path )) {
	 * mkdir ( $upload_path, 0777 );
	 * }
	 *
	 * // $profile_upload_path = driver_profile_url();
	 * $upload = $this->do_upload ( $img_form_name, $img_selected, $img_prefix, $upload_path );
	 * $upload_image_name = implode ( ',', $upload );
	 * if ($upload) {
	 * $data [$img_form_name] = $upload_image_name;
	 * $update_prof_imgname = $this->Driver_Personal_Details_Model->update ( $data, array (
	 * 'driverId' => $driver_id
	 * ) );
	 * }
	 * }
	 * public function do_upload($img_form_name, $selected_img, $img_prefix, $upload_path) {
	 * $upload_image_name = array ();
	 * // Count # of uploaded files in array
	 * $total = count ( $_FILES [$img_form_name] ['name'] );
	 * // Loop through each file
	 * for($i = 0; $i < $total; $i ++) {
	 * // Get the temp file path
	 * $tmpFilePath = $_FILES [$img_form_name] ['tmp_name'] [$i];
	 *
	 * // Make sure we have a filepath
	 * if ($tmpFilePath != "") {
	 * // get image extension
	 * $img_ext = pathinfo ( $selected_img [$i], PATHINFO_EXTENSION );
	 * $image_full_name = $img_prefix . "-$i" . '.' . $img_ext;
	 * $newFilePath = $upload_path . '/' . $image_full_name;
	 *
	 * // Upload the file into the temp dir
	 * if (move_uploaded_file ( $tmpFilePath, $newFilePath )) {
	 *
	 * $upload_image_name [] = $image_full_name;
	 * }
	 * }
	 * }
	 * return $upload_image_name;
	 * }
	 */
	public function AllocateTaxiToDriver() {
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['taxi_allocate_failed'];
		$updated_mapping_id = - 1;
		$mapping_id = NULL;
		$data = $this->input->post ();
		$driver_id = $data ['tempDriverId'];
		$taxi_id = $data ['taxiId'];
		$taxi_device_id = $data ['taxiDeviceId'];
		
		$allocated_taxi_details = $this->Driver_Taxi_Mapping_Model->getOneByKeyValueArray ( array (
				'driverId' => $driver_id 
		) );
		
		if ($allocated_taxi_details) {
			$mapping_id = $allocated_taxi_details->id;
			if ($taxi_id != $allocated_taxi_details->taxiId && $taxi_device_id != $allocated_taxi_details->taxiDeviceId) {
				// to get the taxi owner Id before mapping taxi & taxi device to driver
				$taxi_details = $this->Taxi_Details_Model->getOneByKeyValueArray ( array (
						'id' => $taxi_id 
				) );
				
				$update_driver_group = $this->Driver_Personal_Details_Model->update ( array (
						'groupType' => $taxi_details->name 
				), array (
						'driverId' => $driver_id 
				) );
				$updated_data = array (
						'taxiId' => $taxi_id,
						'taxiDeviceId' => $taxi_device_id,
						'taxiOwnerId' => $taxi_details->taxiOwnerId 
				);
				$updated_mapping_id = $this->Driver_Taxi_Mapping_Model->update ( $updated_data, array (
						'driverId' => $driver_id 
				) );
				
				/**
				 * *** Update previous Taxi device Id isALLOCATED to 0 ****
				 */
				$taxi_device_update = $this->Taxi_Device_Details_Model->update ( array (
						'isAllocated' => Status_Type_Enum::INACTIVE 
				), array (
						'Id' => $allocated_taxi_details->taxiDeviceId 
				) );
				
				/**
				 * *** Update previous Taxi Id isALLOCATED to 0 ****
				 */
				$taxi_device_update = $this->Taxi_Details_Model->update ( array (
						'isAllocated' => Status_Type_Enum::INACTIVE 
				), array (
						'Id' => $allocated_taxi_details->taxiId 
				) );
				
				// first uninstall previous device before installing new device to taxi
				$update_device_history = array (
						'uninstalledDatetime' => getCurrentDateTime () 
				);
				$device_history_updated = $this->Taxi_Device_Install_History_Model->update ( array (
						'uninstalledDatetime' => getCurrentDateTime () 
				), array (
						'taxiId' => $allocated_taxi_details->taxiId,
						'taxiDeviceId' => $allocated_taxi_details->taxiDeviceId 
				) );
				
				// to maintain new device installation history to taxi
				$insert_device_history = array (
						'taxiDeviceId' => $taxi_device_id,
						'taxiId' => $taxi_id,
						'installedDatetime' => getCurrentDateTime () 
				);
				$device_history_id = $this->Taxi_Device_Install_History_Model->insert ( $insert_device_history );
				
				$msg = $msg_data ['taxi_allocate_updated'];
			} else if ($taxi_id != $allocated_taxi_details->taxiId) {
				// to get the taxi owner Id before mapping taxi & taxi device to driver
				$taxi_details = $this->Taxi_Details_Model->getOneByKeyValueArray ( array (
						'id' => $taxi_id 
				) );
				$updated_data = array (
						'taxiId' => $taxi_id,
						'taxiOwnerId' => $taxi_details->taxiOwnerId 
				);
				$updated_mapping_id = $this->Driver_Taxi_Mapping_Model->update ( $updated_data, array (
						'driverId' => $driver_id 
				) );
				
				$update_driver_group = $this->Driver_Personal_Details_Model->update ( array (
						'groupType' => $taxi_details->name 
				), array (
						'driverId' => $driver_id 
				) );
				
				/**
				 * *** Update previous Taxi Id isALLOCATED to 0 ****
				 */
				$taxi_device_update = $this->Taxi_Details_Model->update ( array (
						'isAllocated' => Status_Type_Enum::INACTIVE 
				), array (
						'Id' => $allocated_taxi_details->taxiId 
				) );
				
				$msg = $msg_data ['taxi_allocate_updated'];
			} else if ($taxi_device_id != $allocated_taxi_details->taxiDeviceId) {
				
				$updated_data = array (
						'taxiDeviceId' => $taxi_device_id 
				);
				$updated_mapping_id = $this->Driver_Taxi_Mapping_Model->update ( $updated_data, array (
						'driverId' => $driver_id 
				) );
				
				/**
				 * *** Update previous Taxi device Id isALLOCATED to 0 ****
				 */
				$taxi_device_update = $this->Taxi_Device_Details_Model->update ( array (
						'isAllocated' => Status_Type_Enum::INACTIVE 
				), array (
						'id' => $allocated_taxi_details->taxiDeviceId 
				) );
				
				// first uninstall previous device before installing new device to taxi
				$update_device_history = array (
						'uninstalledDatetime' => getCurrentDateTime () 
				);
				$device_history_updated = $this->Taxi_Device_Install_History_Model->update ( array (
						'uninstalledDatetime' => getCurrentDateTime () 
				), array (
						'taxiId' => $taxi_id 
				) );
				
				// to maintain new device installation history to taxi
				$insert_device_history = array (
						'taxiDeviceId' => $taxi_device_id,
						'taxiId' => $taxi_id,
						'installedDatetime' => getCurrentDateTime () 
				);
				$device_history_id = $this->Taxi_Device_Install_History_Model->insert ( $insert_device_history );
				
				$msg = $msg_data ['taxi_allocate_updated'];
			} else {
				$msg = $msg_data ['taxi_allocate_no_changes'];
				$updated_mapping_id = 0;
			}
		} else {
			// to get the taxi owner Id before mapping taxi & taxi device to driver
			$taxi_details = $this->Taxi_Details_Model->getOneByKeyValueArray ( array (
					'id' => $taxi_id 
			) );
			
			$insert_data = array (
					'driverId' => $driver_id,
					'taxiId' => $taxi_id,
					'taxiDeviceId' => $taxi_device_id,
					'taxiOwnerId' => $taxi_details->taxiOwnerId 
			);
			$mapping_id = $this->Driver_Taxi_Mapping_Model->insert ( $insert_data );
			
			$updated_mapping_id = $mapping_id;
			
			$update_driver_group = $this->Driver_Personal_Details_Model->update ( array (
					'groupType' => $taxi_details->name 
			), array (
					'driverId' => $driver_id 
			) );
			
			// to maintain device installation history
			$insert_device_history = array (
					'taxiDeviceId' => $taxi_device_id,
					'taxiId' => $taxi_id,
					'installedDatetime' => getCurrentDateTime () 
			);
			$device_history_id = $this->Taxi_Device_Install_History_Model->insert ( $insert_device_history );
			
			$msg = $msg_data ['taxi_allocate_success'];
		}
		if ($mapping_id > 0 || $updated_mapping_id) {
			/**
			 * *** Update Taxi device Id isALLOCATED to 1 ****
			 */
			$taxi_device_update = $this->Taxi_Device_Details_Model->update ( array (
					'isAllocated' => Status_Type_Enum::ACTIVE 
			), array (
					'Id' => $taxi_device_id 
			) );
			
			/**
			 * *** Update Taxi Id isALLOCATED to 1 ****
			 */
			$taxi_device_update = $this->Taxi_Details_Model->update ( array (
					'isAllocated' => Status_Type_Enum::ACTIVE 
			), array (
					'Id' => $taxi_id 
			) );
		}
		
		$response = array (
				'msg' => $msg,
				'mapping_id' => $mapping_id,
				'status' => $updated_mapping_id 
		);
		echo json_encode ( $response );
	}
	/**
	 * * Driver Transaction details section starts **
	 */
	public function getDriverTransactionDetailsList($driver_id) {
		if ($this->User_Access_Model->showDriver () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		/**
		 * code to redirect driver transaction list for invlaid id *
		 */
		$data ['validate_id'] = $this->Driver_Model->getById ( $driver_id );
		$check_valid_driver_id = count ( $data ['validate_id'] );
		if ($check_valid_driver_id == 0) {
			redirect ( base_url () . 'Driver/getDriverList' );
			exit ();
		}
		$data = array ();
		if ($driver_id > 0) {
			$driver_details = $this->Driver_Model->getColumnByKeyValueArray ( 'firstName,lastName', array (
					'id' => $driver_id 
			) );
			$data ['driver_fullname'] = $driver_details [0]->firstName . ' ' . $driver_details [0]->lastName;
		} else {
			$data ['driver_fullname'] = '';
		}
		$this->addJs ( 'app/driver-transaction-details.js' );
		// echo "cool"; exit;
		$data ['activetab'] = "driver";
		$data ['active_menu'] = 'driver';
		
		$this->render ( 'driver/manage_driver_transactiondetails', $data );
	}
	public function ajax_list_drivertransaction() {
		// echo"ajaxlist";exit;
		$driver_id = $_POST ['driver_id'];
		$sql_query = $this->Driver_Transaction_Details_Model->getDriverTransactionListQuery ( $driver_id );
		$driver_list = $this->Driver_Transaction_Details_Model->getDataTable ( $sql_query );
		$data = array ();
		$no = $_POST ['start'];
		foreach ( $driver_list as $list ) {
			$no ++;
			$row = array ();
			$row [] = '<button class="btn btn-sm btn-success" id="viewdrivertransaction-' . $list->id . '" title="View">' . '<i class="fa fa-eye"></i></button>';
			
			// $row[] = ucfirst($list->driverName);
			$row [] = $list->tripId;
			$row [] = $list->transactionAmount;
			$row [] = $list->previousAmount;
			$row [] = $list->currentAmount;
			$row [] = $list->transactionStatus;
			$row [] = $list->transactionFrom;
			$row [] = $list->transactionType;
			$row [] = $list->transactionMode;
			$row [] = $list->createdDatetime;
			$row [] = $list->transactionId;
			$row [] = $list->comments;
			// add html for action
			
			$data [] = $row;
		}
		
		$output = array (
				"draw" => $_POST ['draw'],
				"recordsTotal" => $this->Driver_Transaction_Details_Model->getDataTableAllCount ( $sql_query ),
				"recordsFiltered" => $this->Driver_Transaction_Details_Model->getDataTableFilterCount ( $sql_query ),
				"data" => $data 
		);
		// output to json format
		echo json_encode ( $output );
	}
	public function addDriverTransaction($driver_id) {
		if ($this->User_Access_Model->showDriver () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		$msg_data = $this->config->item ( 'msg' );
		
		$this->addJs ( 'app/driver-transaction-details.js' );
		
		$data = array (
				'mode' => EDIT_MODE 
		);
		// print_r($data);exit;
		$data ['activetab'] = "driver";
		$data ['active_menu'] = 'driver';
		$data ['transaction_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRANSACTION_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		
		unset ( $data ['transaction_type_list'] [Transaction_Type_Enum::TWOC_TWOP] );
		
		$data ['transaction_mode_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRANSACTION_MODE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		$data ['transaction_from_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRANSACTION_FROM' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		
		/**
		 * code to redirect driver list for invlaid id *
		 */
		$data ['validate_id'] = $this->Driver_Model->getById ( $driver_id );
		$check_valid_driver_id = count ( $data ['validate_id'] );
		if ($check_valid_driver_id == 0) {
			echo "<script>window.history.back();</script>";
			exit ();
		}
		
		if ($driver_id > 0) {
			$data ['driver_model'] = $this->Driver_Model->getById ( $driver_id );
		} else {
		}
		if ($driver_id > 0) {
			
			$data ['driver_transaction_model'] = $this->Driver_Transaction_Details_Model->getById ( $driver_id );
			$data ['driver_transaction_model'] = $this->Driver_Transaction_Details_Model->getBlankModel ();
		}
		$this->render ( 'driver/add_edit_driver_transactiondetails', $data );
	}
	public function getTransactionDetailsById($driver_transaction_id, $view_mode) {
		if ($this->User_Access_Model->showDriver () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		$msg_data = $this->config->item ( 'msg' );
		
		$this->addJs ( 'app/driver-transaction-details.js' );
		
		$data = array (
				'mode' => VIEW_MODE 
		);
		// print_r($data);exit;
		$data ['activetab'] = "driver";
		$data ['active_menu'] = 'driver';
		$data ['transaction_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRANSACTION_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		unset ( $data ['transaction_type_list'] [Transaction_Type_Enum::TWOC_TWOP] );
		$data ['transaction_mode_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRANSACTION_MODE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		$data ['transaction_from_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRANSACTION_FROM' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		
		if ($this->input->post ( 'id' )) {
			$driver_transaction_id = $this->input->post ( 'id' );
		}
		
		/**
		 * code to redirect driver list for invlaid id *
		 */
		$data ['validate_id'] = $this->Driver_Transaction_Details_Model->getById ( $driver_transaction_id );
		$check_valid_driver_id = count ( $data ['validate_id'] );
		if ($check_valid_driver_id == 0) {
			echo "<script>window.history.back();</script>";
			exit ();
		}
		if ($driver_transaction_id > 0) {
			if ($view_mode == EDIT_MODE) {
				$locked = $this->Driver_Transaction_Details_Model->isLocked ( $driver_transaction_id );
				if ($locked) {
					$locked_user = $this->Driver_Transaction_Details_Model->getLockedUser ( $driver_transaction_id );
					$locked_date = $this->Driver_Transaction_Details_Model->getLockedDate ( $driver_transaction_id );
					$locked_date = date ( 'jS, F Y G:ia', strtotime ( $locked_date ) );
					$message = $msg_data ['locked'];
					$message = str_replace ( '##USERNAME##', $locked_user, $message );
					$message .= $locked_date;
					
					$data ['mode'] = VIEW_MODE;
				} else {
					$is_locked = $this->Driver_Transaction_Details_Model->lock ( $driver_transaction_id );
				}
			}
			
			$data ['driver_transaction_model'] = $this->Driver_Transaction_Details_Model->getById ( $driver_transaction_id );
			$data ['driver_model'] = $this->Driver_Model->getById ( $data ['driver_transaction_model']->driverId );
		} else {
			// Blank Model
			$data ['driver_transaction_model'] = $this->Driver_Transaction_Details_Model->getBlankModel ();
			$data ['driver_model'] = $this->Driver_Model->getBlankModel ();
		}
		if ($this->input->post ( 'id' )) {
			
			$html = $this->load->view ( 'driver/add_edit_driver_transactiondetails', $data );
			echo json_encode ( array (
					'html' => $html 
			) );
		} else {
			$this->render ( 'driver/add_edit_driver_transactiondetails', $data );
		}
	}
	public function saveDriverTransaction() {
		$this->addJs ( 'app/driver-transaction-details.js' );
		$data = array ();
		$data = $this->input->post ();
		
		$driver_transaction_id = $data ['id'];
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['failed'];
		if ($driver_transaction_id > 0) {
			$this->Driver_Transaction_Details_Model->update ( $data, array (
					'id' => $driver_transaction_id 
			) );
			if ($data ['transactionFrom'] == Transaction_From_Enum::WALLET_ACCOUNT) {
				$update_driver_wallet = $this->Driver_Model->update ( array (
						'driverWallet' => $data ['currentAmount'] 
				), array (
						'id' => $data ['driverId'] 
				) );
				// @todo update driver dispath details table
				$driver_dispatch_exists = NULL;
				$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
						'driverId' => $data ['driverId'] 
				) );
				
				if ($driver_dispatch_exists) {
					
					$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
							'walletBalance' => $data ['currentAmount'] 
					), array (
							'driverId' => $data ['driverId'] 
					) );
				} else {
					$insert_dispatch_data = array (
							'driverId' => $data ['driverId'],
							'walletBalance' => $data ['currentAmount'] 
					);
					$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
				}
			} else if ($data ['transactionFrom'] == Transaction_From_Enum::CREDIT_ACCOUNT) {
				$update_driver_wallet = $this->Driver_Model->update ( array (
						'driverCredit' => $data ['currentAmount'] 
				), array (
						'id' => $data ['driverId'] 
				) );
			}
			
			$msg = $msg_data ['updated'];
		} else {
			
			$driver_transaction_id = $this->Driver_Transaction_Details_Model->insert ( $data );
			if ($data ['transactionFrom'] == Transaction_From_Enum::WALLET_ACCOUNT) {
				$update_driver_wallet = $this->Driver_Model->update ( array (
						'driverWallet' => $data ['currentAmount'] 
				), array (
						'id' => $data ['driverId'] 
				) );
				// @todo update driver dispath details table
				$driver_dispatch_exists = NULL;
				$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
						'driverId' => $data ['driverId'] 
				) );
				
				if ($driver_dispatch_exists) {
					
					$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
							'walletBalance' => $data ['currentAmount'] 
					), array (
							'driverId' => $data ['driverId'] 
					) );
				} else {
					$insert_dispatch_data = array (
							'driverId' => $data ['driverId'],
							'walletBalance' => $data ['currentAmount'] 
					);
					$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
				}
			} else if ($data ['transactionFrom'] == Transaction_From_Enum::CREDIT_ACCOUNT) {
				$update_driver_wallet = $this->Driver_Model->update ( array (
						'driverCredit' => $data ['currentAmount'] 
				), array (
						'id' => $data ['driverId'] 
				) );
			}
			$msg = $msg_data ['success'];
		}
		
		$response = array (
				'msg' => $msg,
				'driver_transaction_id' => $driver_transaction_id 
		);
		echo json_encode ( $response );
	}
	
	/**
	 * * Driver Shift History details section starts **
	 */
	public function getDriverShiftHistoryList($driver_id) {
		if ($this->User_Access_Model->showDriver () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		/**
		 * code to redirect driver transaction list for invlaid id *
		 */
		$data ['validate_id'] = $this->Driver_Model->getById ( $driver_id );
		$check_valid_driver_id = count ( $data ['validate_id'] );
		if ($check_valid_driver_id == 0) {
			redirect ( base_url () . 'Driver/getDriverList' );
			exit ();
		}
		$data = array ();
		if ($driver_id > 0) {
			$driver_details = $this->Driver_Model->getColumnByKeyValueArray ( 'firstName,lastName', array (
					'id' => $driver_id 
			) );
			$data ['driver_fullname'] = $driver_details [0]->firstName . ' ' . $driver_details [0]->lastName;
		} else {
			$data ['driver_fullname'] = '';
		}
		$this->addJs ( 'app/driver-shift-history.js' );
		// echo "cool"; exit;
		$data ['activetab'] = "driver";
		$data ['active_menu'] = 'driver';
		
		$this->render ( 'driver/manage_driver_shift_history', $data );
	}
	public function ajax_list_driverloghistory() {
		// echo"ajaxlist";exit;
		$driver_id = $_POST ['driver_id'];
		
		$sql_query = $this->Driver_Shift_History_Model->getDriverLogHistoryListQuery ( $driver_id );
		$driver_list = $this->Driver_Shift_History_Model->getDataTable ( $sql_query );
		$data = array ();
		$no = $_POST ['start'];
		foreach ( $driver_list as $list ) {
			$no ++;
			$row = array ();
			$log_status_name = '';
			// $log_status_name=((strtotime($list->createdDatetime)==strtotime($list->updatedDatetime)) && ($list->availabilityStatus==Taxi_Available_Status_Enum::BUSY) && ($list->shiftStatus==Driver_Shift_Status_Enum::SHIFTIN))?'Logged In':((strtotime($list->createdDatetime)!=strtotime($list->updatedDatetime)) && ($list->availabilityStatus==Taxi_Available_Status_Enum::BUSY) && ($list->shiftStatus==Driver_Shift_Status_Enum::SHIFTIN))?'Free to Busy':((strtotime($list->createdDatetime)!=strtotime($list->updatedDatetime)) && ($list->availabilityStatus==Taxi_Available_Status_Enum::BUSY) && ($list->shiftStatus==Driver_Shift_Status_Enum::SHIFTOUT))?'Logged Out':'Unknown';
			if ((strtotime ( $list->createdDatetime ) == strtotime ( $list->updatedDatetime )) && ($list->availabilityStatus == Taxi_Available_Status_Enum::BUSY) && ($list->shiftStatus == Driver_Shift_Status_Enum::SHIFTIN)) {
				$log_status_name = 'Logged In With Busy';
			}
			if ((strtotime ( $list->createdDatetime ) != strtotime ( $list->updatedDatetime )) && ($list->availabilityStatus == Taxi_Available_Status_Enum::BUSY) && ($list->shiftStatus == Driver_Shift_Status_Enum::SHIFTIN)) {
				$log_status_name = 'Free To Busy';
			}
			if ((strtotime ( $list->createdDatetime ) != strtotime ( $list->updatedDatetime )) && ($list->availabilityStatus == Taxi_Available_Status_Enum::BUSY) && ($list->shiftStatus == Driver_Shift_Status_Enum::SHIFTOUT)) {
				$log_status_name = 'Logged Out with Busy';
			}
			if ((strtotime ( $list->createdDatetime ) == strtotime ( $list->updatedDatetime ) || strtotime ( $list->createdDatetime ) != strtotime ( $list->updatedDatetime )) && ($list->availabilityStatus == Taxi_Available_Status_Enum::FREE) && ($list->shiftStatus == Driver_Shift_Status_Enum::SHIFTIN)) {
				$log_status_name = 'Free Status';
			}
			$row [] = $no;
			$row [] = $log_status_name;
			$free_status_date_time = '';
			if ((strtotime ( $list->createdDatetime ) != strtotime ( $list->updatedDatetime )) && ($list->availabilityStatus == Taxi_Available_Status_Enum::BUSY) && ($list->shiftStatus == Driver_Shift_Status_Enum::SHIFTIN)) {
				$free_status_date_time = $list->createdDatetime;
			}
			if ((strtotime ( $list->createdDatetime ) == strtotime ( $list->updatedDatetime ) || strtotime ( $list->createdDatetime ) != strtotime ( $list->updatedDatetime )) && ($list->availabilityStatus == Taxi_Available_Status_Enum::FREE) && ($list->shiftStatus == Driver_Shift_Status_Enum::SHIFTIN)) {
				$free_status_date_time = $list->createdDatetime;
			}
			
			$row [] = $free_status_date_time;
			$busy_status_date_time = '';
			if ((strtotime ( $list->createdDatetime ) != strtotime ( $list->updatedDatetime )) && ($list->availabilityStatus == Taxi_Available_Status_Enum::BUSY) && ($list->shiftStatus == Driver_Shift_Status_Enum::SHIFTIN)) {
				$busy_status_date_time = $list->updatedDatetime;
			}
			if ((strtotime ( $list->createdDatetime ) != strtotime ( $list->updatedDatetime )) && ($list->availabilityStatus == Taxi_Available_Status_Enum::BUSY) && ($list->shiftStatus == Driver_Shift_Status_Enum::SHIFTOUT)) {
				$busy_status_date_time = $list->updatedDatetime;
			}
			if ((strtotime ( $list->createdDatetime ) == strtotime ( $list->updatedDatetime )) && ($list->availabilityStatus == Taxi_Available_Status_Enum::BUSY) && ($list->shiftStatus == Driver_Shift_Status_Enum::SHIFTIN)) {
				$busy_status_date_time = $list->updatedDatetime;
			}
			$row [] = $busy_status_date_time;
			$driver_email_records = NULL;
			$sms_sent_count = 0;
			$last_sms_sent_datetime = '';
			if (((strtotime ( $list->createdDatetime ) == strtotime ( $list->updatedDatetime )) && ($list->availabilityStatus == Taxi_Available_Status_Enum::FREE) && ($list->shiftStatus == Driver_Shift_Status_Enum::SHIFTIN)) || ((strtotime ( $list->createdDatetime ) != strtotime ( $list->updatedDatetime )) && ($list->availabilityStatus == Taxi_Available_Status_Enum::BUSY) && ($list->shiftStatus == Driver_Shift_Status_Enum::SHIFTIN))) {
				// debug("Free");
				$driver_email_records = $this->Emergency_Request_Details_Model->getDriverSmsSentDetails ( $driver_id, $list->createdDatetime, $list->updatedDatetime );
				if (count ( $driver_email_records ) > 0) {
					$sms_sent_count = count ( $driver_email_records );
					$last_sms_sent_datetime = $driver_email_records [$sms_sent_count - 1]->createdDatetime;
				}
			}
			
			$row [] = $sms_sent_count;
			$row [] = $last_sms_sent_datetime;
			// add html for action
			
			$data [] = $row;
		}
		
		$output = array (
				"draw" => $_POST ['draw'],
				"recordsTotal" => $this->Driver_Shift_History_Model->getDataTableAllCount ( $sql_query ),
				"recordsFiltered" => $this->Driver_Shift_History_Model->getDataTableFilterCount ( $sql_query ),
				"data" => $data 
		);
		// output to json format
		echo json_encode ( $output );
	}
	public function trackDrivers() {
		if ($this->User_Access_Model->createModule ( $this->session->userdata ( 'role_type' ), Module_Name_Enum::DRIVER ) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data ['activetab'] = "driver";
		$data ['active_menu'] = 'driver';
		$this->addJs ( 'app/driver/driver_tracking.js' );
		$zone_id = $this->uri->segment ( 3 );
		$zone_id = ($zone_id != '') ? $zone_id : 0;
		
		$data ['zone_model'] = $this->Zone_Details_Model->getById ( $zone_id );
		$polygan_points = @$data ['zone_model']->polygonPoints;
		$polygan_array = explode ( "|", $polygan_points );
		
		// echo '<pre>';
		// print_r($polygan_array);
		$ploygan_lat_long_arr = array ();
		foreach ( $polygan_array as $ploygan ) {
			$ploygan_lat_long_arr [] = explode ( ",", $ploygan );
		}
		$polyarr = array_reduce ( $ploygan_lat_long_arr, 'array_merge', array () );
		
		foreach ( $ploygan_lat_long_arr as $prow ) {
			$plat = @$prow [0];
			$plong = @$prow [1];
			$plarr [] = "{lat: $plat, lng: $plong},";
		}
		
		if (count ( $data ['zone_model'] ) > 0) {
			$data ['zone_polygan_data'] = $plarr;
			$data ['center_lat_long'] = @$data ['zone_polygan_data'] [0];
			$data ['map_zoom'] = 13;
		} else {
			$data ['center_lat_long'] = "{lat: 21.41216 , lng:96.28418 },";
			$data ['zone_polygan_data'] = array (
					"{lat: 21.41216 , lng:96.28418 }," 
			);
			$data ['map_zoom'] = 8;
		}
		$data ['zone_model'] = $this->Zone_Details_Model->getById ( $zone_id );
		
		// exit;
		$this->render ( 'driver/track_current_driver', $data );
		// $this->load->view( 'driver/track_current_driver',$data );
	}
	public function getLatLongOfDriverByZone_old() {
		$driver_zonedata = $this->Driver_Dispatch_Details_Model->getFreeAndBusyDriver ();
		$resultarr = array ();
		$zone_arr = array ();
		$rowdata = array ();
		if ($driver_zonedata) {
			foreach ( $driver_zonedata as $zdata ) {
				$driver_lat = @$zdata->driverLatitude;
				$driver_long = @$zdata->driverLongitude;
				$driver_staus = @$zdata->availabilityStatus;
				$driver_id = @$zdata->driverId;
				$driver_name = @$zdata->driverName;
				$driver_mobile = @$zdata->driverMobile;
				$taxi_registration = @$zdata->taxiRegistrationNo;
				$driverzone_arr [] = $this->getZoneByLatLongOfDriver ( $driver_lat, $driver_long, $driver_staus, $driver_id, $driver_name, $driver_mobile, $taxi_registration );
			}
			// $this->getLatLongOfDriverByZone($driver_lat_arr,$driver_long_arr,$zone_arr);
			if (count ( $driverzone_arr ) > 0) {
				$resultarr = array_reduce ( $driverzone_arr, 'array_merge', array () );
			}
			$zone_id = $this->input->post ( 'zone_id' );
			$zone_id = ($zone_id != '') ? $this->input->post ( 'zone_id' ) : 0;
			
			foreach ( $resultarr as $res ) {
				$zonedatas = explode ( ',', $res );
				if (in_array ( $zone_id, $zonedatas )) {
					$pzoneid = @$zonedatas [0];
					$lat = @$zonedatas [1];
					$long = @$zonedatas [2];
					$status = @$zonedatas [3];
					$driverid = @$zonedatas [4];
					$drivername = @$zonedatas [5];
					$drivermobile = @$zonedatas [6];
					$taxi_reg_no = @$zonedatas [7];
					if ($pzoneid == $zone_id) {
						$row ['zone_id'] = $pzoneid;
						$row ['latitude'] = $lat;
						$row ['longitutde'] = $long;
						$row ['status'] = $status;
						$row ['driver_id'] = $driverid;
						$row ['driver_name'] = $drivername;
						$row ['driver_mobile'] = $drivermobile;
						$row ['taxi_registration'] = $taxi_reg_no;
						$rowdata [] = $row;
					}
				}
			}
		}
		echo json_encode ( $rowdata, true );
	}
	public function getLatLongOfDriverByZone() {
		$driver_zonedata = $this->Driver_Dispatch_Details_Model->getFreeAndBusyDriver ();
		$resultarr = array ();
		$zone_arr = array ();
		$rowdata = array ();
		
		if ($driver_zonedata) {
			foreach ( $driver_zonedata as $zdata ) {
				$driver_lat = @$zdata->driverLatitude;
				$driver_long = @$zdata->driverLongitude;
				$driver_staus = @$zdata->availabilityStatus;
				$driver_id = @$zdata->driverId;
				$driver_name = @$zdata->driverName;
				$driver_mobile = @$zdata->driverMobile;
				$taxi_registration = @$zdata->taxiRegistrationNo;
				// $driver_lat_arr[] = $zdata->driverLatitude;
				// $driver_long_arr[] = $zdata->driverLongitude;
				$driverzone_arr [] = $this->getZoneByLatLongOfDriver ( $driver_lat, $driver_long, $driver_staus, $driver_id, $driver_name, $driver_mobile, $taxi_registration );
			}
			// $this->getLatLongOfDriverByZone($driver_lat_arr,$driver_long_arr,$zone_arr);
			
			if (count ( $driverzone_arr ) > 0) {
				$resultarr = array_reduce ( $driverzone_arr, 'array_merge', array () );
			}
			
			$zone_id = $this->input->post ( 'zone_id' );
			$zone_id = ($zone_id != '') ? $this->input->post ( 'zone_id' ) : 0;
			
			/**
			 * ** to check zone contains subzone **
			 */
			$subzone_list = $this->Zone_Details_Model->checkZoneHasParentByZoneId ( $zone_id );
			if (count ( $subzone_list ) > 0) {
				$subzone_id = @$subzone_list [0]->subzoneId;
				$pid_of_subzone = @$subzone_list [0]->zoneId;
			} else {
				$subzone_id = 0;
				$pid_of_subzone = 0;
			}
			foreach ( $resultarr as $res ) {
				$zonedatas = explode ( ',', $res );
				/**
				 * * condition to check in posted zone id *
				 */
				if ($pid_of_subzone == 0) {
					if (in_array ( $zone_id, $zonedatas )) {
						$pzoneid = @$zonedatas [0];
						$lat = @$zonedatas [1];
						$long = @$zonedatas [2];
						$status = @$zonedatas [3];
						$driverid = @$zonedatas [4];
						$drivername = @$zonedatas [5];
						$drivermobile = @$zonedatas [6];
						$taxi_reg_no = @$zonedatas [7];
						if ($pzoneid == $zone_id) {
							$row ['zone_id'] = $pzoneid;
							$row ['latitude'] = $lat;
							$row ['longitutde'] = $long;
							$row ['status'] = $status;
							$row ['driver_id'] = $driverid;
							$row ['driver_name'] = $drivername;
							$row ['driver_mobile'] = $drivermobile;
							$row ['taxi_registration'] = $taxi_reg_no;
							$rowdata [] = $row;
						}
					}
				}
				
				/**
				 * * condition to check if posted zone was a subzone **
				 */
				if ($pid_of_subzone > 0) {
					if (in_array ( $pid_of_subzone, $zonedatas )) {
						$pzoneid = @$zonedatas [0];
						$lat = @$zonedatas [1];
						$long = @$zonedatas [2];
						$status = @$zonedatas [3];
						$driverid = @$zonedatas [4];
						$drivername = @$zonedatas [5];
						$drivermobile = @$zonedatas [6];
						$taxi_reg_no = @$zonedatas [7];
						/**
						 * Check lat long exists in sub zone **
						 */
						
						$check_in_subzone = getSubZoneByLatLong ( $lat, $long );
						$resultset = array_filter ( $check_in_subzone );
						// store only if lat and long exits in subzone
						if (count ( $resultset ) > 0) {
							$row ['zone_id'] = $pzoneid;
							$row ['latitude'] = $lat;
							$row ['longitutde'] = $long;
							$row ['status'] = $status;
							$row ['driver_id'] = $driverid;
							$row ['driver_name'] = $drivername;
							$row ['driver_mobile'] = $drivermobile;
							$row ['taxi_registration'] = $taxi_reg_no;
							$rowdata [] = $row;
						}
					}
				}
			}
		}
		echo json_encode ( $rowdata, true );
	}
	/**
	 * ** Code to Find Zone Based on Lat and Long ***
	 */
	public function getZoneByLatLongOfDriver($lat = NULL, $long = NULL, $driver_staus, $driver_id, $driver_name, $driver_mobile, $taxi_registration) {
		$polyX = array (); // horizontal coordinates of corners
		$polyY = array (); // vertical coordinates of corners
		$zone_arr = array ();
		$zone_list = $this->Zone_Details_Model->getZoneListQueryForTripCreation ();
		// $zone_list = $this->db->query($zone_sql)->result();
		if ($zone_list) {
			foreach ( $zone_list as $list ) {
				$polygan_points = @$list->polygonPoints;
				$zone_name = @$list->name;
				$zone_id = @$list->zoneId;
				$zone_arr [] = $this->process_poylgan_data ( $polygan_points, $lat, $long, $zone_name, $zone_id, $driver_staus, $driver_id, $driver_name, $driver_mobile, $taxi_registration );
			}
		}
		return array_filter ( $zone_arr );
	} // end of function
	public function process_poylgan_data($polygan_points, $lat, $long, $zone_name, $zone_id, $driver_staus, $driver_id, $driver_name, $driver_mobile, $taxi_registration) {
		$zonal = $polygan_points;
		$zone_arr = array ();
		$zonalArray = explode ( "|", $zonal );
		$polySides = count ( $zonalArray ); // how many corners the polygon has
		foreach ( $zonalArray as $eachZoneLatLongs ) {
			$latlongArray = explode ( ",", $eachZoneLatLongs );
			$polyX [] = isset ( $latlongArray [0] ) ? $latlongArray [0] : '';
			$polyY [] = isset ( $latlongArray [1] ) ? $latlongArray [1] : '';
		}
		
		$vertices_x = $polyX; // x-coordinates of the vertices of the polygon
		$vertices_y = $polyY; // y-coordinates of the vertices of the polygon
		$points_polygon = count ( $vertices_x ); // number vertices
		                                         // Following Points lie inside this region
		$longitude_x = $lat; // x-coordinate of the point to test
		$latitude_y = $long; // y-coordinate of the point to test
		
		if ($this->is_in_polygon ( $points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y )) {
			// echo json_encode(array('ZoneId'=>$zone_id,'Zone_name'=>$zone_name));
			$zone_arr = $zone_id . ',' . $longitude_x . ',' . $latitude_y . ',' . $driver_staus . ',' . $driver_id . ',' . $driver_name . ',' . $driver_mobile . ',' . $taxi_registration;
			return $zone_arr;
		}
		// else {
		// return 0;
		// }
	}
	public function is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y) {
		$i = $j = $c = 0;
		
		for($i = 0, $j = $points_polygon - 1; $i < $points_polygon; $j = $i ++) {
			$vertices_y [$i] = ( float ) $vertices_y [$i];
			$vertices_y [$j] = ( float ) $vertices_y [$j];
			$vertices_x [$i] = ( float ) $vertices_x [$i];
			$vertices_x [$j] = ( float ) $vertices_x [$j];
			if ((($vertices_y [$i] > $latitude_y != ($vertices_y [$j] > $latitude_y)) && ($longitude_x < ($vertices_x [$j] - $vertices_x [$i]) * ($latitude_y - $vertices_y [$i]) / ($vertices_y [$j] - $vertices_y [$i]) + $vertices_x [$i])))
				$c = ! $c;
		}
		return $c;
	}
	public function forceUpdateAllDriver() {
		$logout_all_driver = NULL;
		$busy_all_driver = NULL;
		$status = 1;
		$msg = "Force Update failed.. Please try again later.";
		$msg = "Force Update completed";
		/*
		 * //$logout_all_driver=$this->Driver_Model->update(array('loginStatus'=>Status_Type_Enum::INACTIVE),array('id!='=>Status_Type_Enum::INACTIVE));
		 * $logout_all_driver=$this->Driver_Model->update(array('loginStatus'=>Status_Type_Enum::INACTIVE),array('id'=>723));
		 * if ($logout_all_driver)
		 * {
		 * //$busy_all_driver=$this->Driver_Dispatch_Details_Model->update(array('availabilityStatus'=>Taxi_Available_Status_Enum::BUSY),array('driverId!='=>Status_Type_Enum::INACTIVE));
		 * $busy_all_driver=$this->Driver_Dispatch_Details_Model->update(array('availabilityStatus'=>Taxi_Available_Status_Enum::BUSY),array('driverId'=>723));
		 * if ($busy_all_driver)
		 * {
		 * $status=1;
		 * $msg="Force Update completed";
		 * }
		 * }
		 */
		$response = array (
				'msg' => $msg,
				'status' => $status 
		);
		echo json_encode ( $response );
	}
}