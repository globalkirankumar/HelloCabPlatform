<?php
class TaxiTariff extends My_Controller {
	function __construct() {
		parent::__construct ();
		
		$this->load->helper ( 'form' );
		
		$this->load->model ( 'Taxi_Query_Model' );
		$this->load->model ( 'Rate_Card_Details_Model' );
		$this->load->model ( 'Rate_Card_Slab_Details_Model' );
		$this->load->model ( 'Zone_Details_Model' );
		$this->load->model ( 'Entity_Model' );
	}
	public function index() {
		
	}
	
public function add() {
	if ($this->User_Access_Model->createModule ($this->session->userdata('role_type'),Module_Name_Enum::TAXI_TARIFF) === FALSE) {
		// redirect 404
		show_error ( "You do not have permission to view this page", '404' );
		return;
	}
			// $this->addJs ( 'app/taxiTariff.js' );
		$this->addJs ( 'app/taxi-tariff/taxi-tariff-add-edit.js' );
		//$this->addJs ( 'app/taxi-tariff.js' );
		$data = array (
				'mode' => EDIT_MODE 
		);
		$data ['activetab'] = "taxitariff";
		$data ['active_menu'] = 'taxitariff';
		$data ['zone_list'] = $this->Zone_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE 
		), 'id' );
		$data ['entity_list'] = $this->Entity_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE 
		), 'name' );
		$data ['taxi_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TAXI_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		$data ['payment_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'PAYMENT_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		$data ['taxi_tariff_model'] = $this->Rate_Card_Details_Model->getBlankModel ();
		$data ['edit_distance_slab_model']=0;
		$data ['edit_time_slab_model']=0;
		$data ['edit_surge_slab_model']=0;
		$data ['taxi_tariff_distance_slab_model'] = $this->Rate_Card_Slab_Details_Model->getBlankModel ();
		$data ['taxi_tariff_time_slab_model'] = $this->Rate_Card_Slab_Details_Model->getBlankModel ();
		$data ['taxi_tariff_surge_slab_model'] = $this->Rate_Card_Slab_Details_Model->getBlankModel ();
		$this->render ( 'tariff/add_edit_taxi_tariff', $data );
	}
	
	
	
	public function getDetailsById($taxi_tariff_id, $view_mode) {
		if ($view_mode==EDIT_MODE)
		{
			if ($this->User_Access_Model->writeModule ($this->session->userdata('role_type'),Module_Name_Enum::TAXI_TARIFF) === FALSE) {
				// redirect 404
				show_error ( "You do not have permission to view this page", '404' );
				return;
			}
		}
		else
		{
			if ($this->User_Access_Model->readModule ($this->session->userdata('role_type'),Module_Name_Enum::TAXI_TARIFF) === FALSE) {
				// redirect 404
				show_error ( "You do not have permission to view this page", '404' );
				return;
			}
		}
		$this->addJs ( 'app/taxi-tariff/taxi-tariff-add-edit.js' );
		//$this->addJs ( 'app/taxi-tariff.js' );
		$data = array (
				'mode' => $view_mode 
		);
		$data ['activetab'] = "taxitariff";
		$data ['active_menu'] = 'taxitariff';
		$data ['zone_list'] = $this->Zone_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE 
		), 'id' );
		$data ['entity_list'] = $this->Entity_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE 
		), 'name' );
		$data ['taxi_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TAXI_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		$data ['payment_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'PAYMENT_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		if ($this->input->post ( 'id' )) {
			$taxi_tariff_id = $this->input->post ( 'id' );
		}
		
		if ($taxi_tariff_id > 0) {
			
			if ($view_mode==EDIT_MODE)
			{
				$locked=$this->Rate_Card_Details_Model->isLocked($taxi_tariff_id);
				if ($locked)
				{
					$locked_user=$this->Rate_Card_Details_Model->getLockedUser($taxi_tariff_id);
					$locked_date=$this->Rate_Card_Details_Model->getLockedDate($taxi_tariff_id);
					$locked_date=date('jS, F Y G:ia',strtotime($locked_date));
					$message=$msg_data ['locked'];
					$message=str_replace('##USERNAME##', $locked_user, $message);
					$message.=$locked_date;
			
					$data['mode']=VIEW_MODE;
				}
				else
				{
					$is_locked=$this->Rate_Card_Details_Model->lock($taxi_tariff_id);
				}
			}
			
			$data ['taxi_tariff_model'] = $this->Rate_Card_Details_Model->getById ( $taxi_tariff_id );
			$data ['taxi_tariff_distance_slab_model'] = $this->Rate_Card_Slab_Details_Model->getByKeyValueArray (array('rateCardId'=>$data ['taxi_tariff_model']->id,'slabType'=>Slab_Type_Enum::DISTANCE,'status'=>Status_Type_Enum::ACTIVE));
			$data ['taxi_tariff_time_slab_model'] = $this->Rate_Card_Slab_Details_Model->getByKeyValueArray (array('rateCardId'=>$data ['taxi_tariff_model']->id,'slabType'=>Slab_Type_Enum::TIME,'status'=>Status_Type_Enum::ACTIVE));
			$data ['taxi_tariff_surge_slab_model'] = $this->Rate_Card_Slab_Details_Model->getByKeyValueArray (array('rateCardId'=>$data ['taxi_tariff_model']->id,'slabType'=>Slab_Type_Enum::SURGE,'status'=>Status_Type_Enum::ACTIVE));
			$data ['edit_distance_slab_model']=1;
			if(!$data ['taxi_tariff_distance_slab_model'])
			{
				if ($view_mode==EDIT_MODE)
				{
				$data ['taxi_tariff_distance_slab_model'] = $this->Rate_Card_Slab_Details_Model->getBlankModel ();
				}
				else
				{
				$data ['taxi_tariff_distance_slab_model'] = array();
				}
				$data ['edit_distance_slab_model']=0;
			}
			$data ['edit_time_slab_model']=1;
			if(!$data ['taxi_tariff_time_slab_model'])
			{
				if ($view_mode==EDIT_MODE)
				{
				$data ['taxi_tariff_time_slab_model'] = $this->Rate_Card_Slab_Details_Model->getBlankModel ();
				}
				else
				{
				$data ['taxi_tariff_time_slab_model'] = array();
				}
				$data ['edit_time_slab_model']=0;
			}
			$data ['edit_surge_slab_model']=1;
			if(!$data ['taxi_tariff_surge_slab_model'])
			{
				if ($view_mode==EDIT_MODE)
				{
				$data ['taxi_tariff_surge_slab_model'] = $this->Rate_Card_Slab_Details_Model->getBlankModel ();
				}
				else
				{
				$data ['taxi_tariff_surge_slab_model'] = array();
				}
				$data ['edit_surge_slab_model']=0;
			}
		} else {
			$data ['taxi_tariff_model'] = $this->Rate_Card_Details_Model->getBlankModel ();
			$data ['taxi_tariff_distance_slab_model'] = $this->Rate_Card_Slab_Details_Model->getBlankModel ();
			$data ['taxi_tariff_time_slab_model'] = $this->Rate_Card_Slab_Details_Model->getBlankModel ();
			$data ['taxi_tariff_surge_slab_model'] = $this->Rate_Card_Slab_Details_Model->getBlankModel ();
			
			
			
		}
		//debug_exit($data);
		if ($this->input->post ( 'id' )) {
			
			$html = $this->load->view ( 'tariff/view_edit_taxi_tariff', $data );
			echo json_encode ( array (
					'html' => $html 
			) );
		} else {
			$this->render ( 'tariff/add_edit_taxi_tariff', $data );
		}
	}
	
	
	public function getTaxiTariffList()
	{
		$data = array ();
		$this->addJs ( 'app/taxi-tariff/taxi-tariff-add-edit.js' );
		$this->addJs ( 'app/taxi-tariff.js' );
		
		$data ['activetab'] = "taxitariff";
		$data ['active_menu'] = 'taxitariff';
		
		$this->render ( 'tariff/manage_taxi_tariff', $data );
	}
	public function ajax_list()
	{
		//echo"ajaxlist";exit;
		$sql_query=$this->Rate_Card_Details_Model->getTariffListQuery();
		$tariff_details_list = $this->Rate_Card_Details_Model->getDataTable($sql_query);
		//print_r($list);exit;
		$data = array();
		$no = $_POST['start'];
		foreach ($tariff_details_list as $list) {
			$no++;
			$row = array();
			$row[] = '
		
				  <button class="btn btn-sm btn-success" id="viewtaxitariff-'.$list->tariffId.'" title="View" ><i class="fa fa-eye" aria-hidden="true"></i></button>
				  <button class="btn btn-sm btn-blue" id="edittaxitariff-'.$list->tariffId.'"  title="Edit" ><i class="fa fa-pencil-square-o"></i></button>
                  <button class="btn btn-sm btn-danger" id="deletetaxitariff-'.$list->tariffId.'" title="Delete" ><i class="fa fa-trash-o"></i></button>';
			
			/* $slab_class=($list->tariffId)?'class="btn btn-success"':'class="btn btn-danger"';
			
			$row[] = '<button id="slabList-'.$list->tariffId.'"  '.$slab_class.'><span>Slab List</span></button>'; */
			$row[] = $list->name;
			$row[] = $list->taxiCategoryTypeName;
			$row[] = $list->dayConvenienceCharge;
			$row[] = $list->nightConvenienceCharge;
			$row[] = ($list->zoneId)?ucfirst($list->zoneName):'All Zones';
			$row[] = ($list->entityId)?ucfirst($list->entityName):'All Entity';
			
			$status_class=($list->status)?'class="btn btn-success"':'class="btn btn-danger"';
			$status_name=($list->status)?'Active':'Inactive';
			$row[] = '<button id="status-'.$list->tariffId.'" status="'.$list->status.'" '.$status_class.'><span>'.$status_name.'</span></button>';
			//add html for action
			
			$data[] = $row;
		}
	
		$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->Rate_Card_Details_Model->getDataTableAllCount($sql_query),
				"recordsFiltered" => $this->Rate_Card_Details_Model->getDataTableFilterCount($sql_query),
				"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	
	public function saveTaxiTariff() {
		$this->addJs ( 'app/taxiTariff.js' );
		$data = array ();
		$data = $this->input->post ();
		
		//debug($data);
		$taxi_tariff_id = $data ['id'];
		$data['activetab']="admin";
		$data['active_menu'] = 'admin';
		if ($data['zoneId']=='')
		{
			$data['zoneId']=0;
		}
		if ($data['entityId']=='')
		{
			$data['entityId']=0;
		}
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['failed'];
		if ($taxi_tariff_id > 0) {
			$this->Rate_Card_Details_Model->update ( $data, array (
					'id' => $taxi_tariff_id
			) );
			//Set all rate card slab status to 0 for particular taxi_tariff_id to avoid full delete and re-insert same data.
			$update_slab_status=$this->Rate_Card_Slab_Details_Model->update(array('status'=>Status_Type_Enum::INACTIVE),array('rateCardId'=>$taxi_tariff_id));
			$slab_data=array_merge($data['distanceSlab'],$data['timeSlab'],$data['surgeSlab']);
			//debug_exit($slab_data);
			foreach ($slab_data as $list)
			{
				if ($list['slabId']>0)
				{
					//update the slab record for existing slab_id
					$slab_id=$list['slabId'];
					unset($list['slabId']);
					$list['rateCardId']=$taxi_tariff_id;
					$list['status']=Status_Type_Enum::ACTIVE;
					$slab_update_data=$list;
					$this->Rate_Card_Slab_Details_Model->update( $slab_update_data,array('id'=>$slab_id));
				}
				else 
				{
					//insert the new slab record
					unset($list['slabId']);
					$list['rateCardId']=$taxi_tariff_id;
					$list['status']=Status_Type_Enum::ACTIVE;
					$slab_insert_data=$list;
					//debug_exit($slab_insert_data);
					$this->Rate_Card_Slab_Details_Model->insert( $slab_insert_data);
				}
			}
			//Delete all unwanted rate card slab where status=0 for particular taxi_tariff_id to avoid full delete and re-insert same data.
			$delete_slab_status=$this->Rate_Card_Slab_Details_Model->delete(array('rateCardId'=>$taxi_tariff_id,'status'=>Status_Type_Enum::INACTIVE));
			
			$msg = $msg_data ['updated'];
	
		} else {
			$slab_insert_data=array();
			$taxi_tariff_id = $this->Rate_Card_Details_Model->insert ( $data );
			$slab_data=array_merge($data['distanceSlab'],$data['timeSlab'],$data['surgeSlab']);
			foreach ($slab_data as $list)
			{
				unset($list['slabId']);
				$list['rateCardId']=$taxi_tariff_id;
				$slab_insert_data[]=$list;
			}
			$this->Rate_Card_Slab_Details_Model->batchInsert ( $slab_insert_data );
			$msg = $msg_data ['success'];
				
		}
		$response = array (
				'msg' => $msg,
				'taxi_tariff_id' => $taxi_tariff_id
		);
		echo json_encode ( $response );
		// $this->render("passenger/add_edit_passenger", $data);
	
	}
	public function deleteTaxiTariff() {
		if ($this->User_Access_Model->deleteModule ($this->session->userdata('role_type'),Module_Name_Enum::TAXI_TARIFF) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to delete", '404' );
			return;
		}
		$data = array ();
	
		$this->addJs ( 'app/taxi-tariff/taxi-tariff-add-edit.js' );
		$this->addJs ( 'app/taxi-tariff.js' );
		$driver_updated=-1;
		$data = $this->input->post ();
		$tariff_id = $data ['id'];
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['delete_failed'];
		if ($tariff_id) {
			$driver_updated = $this->Rate_Card_Details_Model->update ( array (
					'isDeleted' => Status_Type_Enum::ACTIVE
			), array (
					'id' => $tariff_id
			) );
			$msg = $msg_data ['delete_success'];
		}
		$response = array (
				'msg' => $msg,
				'status' => $driver_updated
		);
		echo json_encode ( $response );
	}
	public function changeTaxiTariffStatus() {
		if ($this->User_Access_Model->fullAccessModule ($this->session->userdata('role_type'),Module_Name_Enum::TAXI_TARIFF) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to change status", '404' );
			return;
		}
		$data = array ();
	
		$this->addJs ( 'app/taxi-tariff/taxi-tariff-add-edit.js' );
		$this->addJs ( 'app/taxi-tariff.js' );
		$user_updated=-1;
		$data = $this->input->post ();
		$tariff_id = $data ['id'];
		$tariff_status = $data ['status'];
	
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['status_change_failed'];
		if ($tariff_id) {
			$tariff_updated = $this->Rate_Card_Details_Model->update ( array (
					'status' => $tariff_status
			), array (
					'id' => $tariff_id
			) );
			$msg = $msg_data ['status_change_success'];
		}
		$response = array (
				'msg' => $msg,
				'status' => $tariff_updated
		);
		echo json_encode ( $response );
	}
	
	
}