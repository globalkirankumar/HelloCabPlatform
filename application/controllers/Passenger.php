<?php
class Passenger extends My_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->helper ( 'form' );
		$this->load->helper ( 'file_upload' );
		$this->load->model ( 'Taxi_Query_Model' );
		$this->load->model ( 'Passenger_Model' );
		$this->load->model ( 'Passenger_Transaction_Details_Model' );
		$this->load->model ( 'Zone_Details_Model' );
		$this->load->model ( 'Sms_Template_Model' );
		$this->load->model ( 'Common_Api_Webservice_Model' );
		$this->load->model ( 'Entity_Model' );
		$this->load->model ( 'Auth_Key_Details_Model' );
	}
	public function index() {
	}
	public function validationPassenger() {
		$this->load->library ( 'form_validation' );
		$config = array (
				array (
						'field' => 'firstName',
						'label' => 'First Name',
						'rules' => 'trim|required|alpha|min_length[3]|max_length[15]' 
				),
				array (
						'field' => 'lastName',
						'label' => 'Last Name',
						'rules' => 'trim|required|alpha|min_length[3]|max_length[15]' 
				),
				array (
						'field' => 'email',
						'label' => 'Email',
						'rules' => 'trim|required|valid_email' 
				),
				array (
						'field' => 'mobile',
						'label' => 'Mobile',
						'rules' => 'trim|required|numeric|exact_length[10]' 
				),
				array (
						'field' => 'cityId',
						'label' => 'City Name',
						'rules' => 'trim|alpha|required' 
				) 
		);
		
		$this->form_validation->set_rules ( $config );
		if ($this->form_validation->run () == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	public function add() {
	if ($this->User_Access_Model->createModule ($this->session->userdata('role_type'),Module_Name_Enum::CUSTOMER) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		
                
		$this->addJs ( 'app/passenger.js' );
		
		$data = array (
				'mode' => EDIT_MODE 
		);
                $data ['entity_list'] = $this->Entity_Model->getSelectDropdownOptions ( array (
                                            'status' => Status_Type_Enum::ACTIVE ), 'name' );
                $data ['device_type'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
                                            'tableName' => strtoupper ( 'DEVICE_TYPE' ),
                                            'isVisible' => Status_Type_Enum::ACTIVE ), 'sequenceOrder' );
                $data ['gender_list'] = $this->Data_Attributes_Model->getColumnByKeyValueArray ( 'id,description', array (
                                                                    'tableName' => strtoupper ( 'GENDER_TYPE' ),
                                                                    'isVisible' => Status_Type_Enum::ACTIVE ), 'sequenceOrder' );
		$data ['activetab'] = "passenger";
		$data ['active_menu'] = 'passenger';
		$data ['zone_list'] = $this->Zone_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE 
		), 'name' );
		
		$data ['passenger_model'] = $this->Passenger_Model->getBlankModel ();
		$this->render ( 'passenger/add_edit_passenger', $data );
	}
	/**
	 */
	public function savePassenger() {
		$this->addJs ( 'app/passenger.js' );
		$data = array ();
		$data = $this->input->post ();
		//$data ['profileImage']= $this->input->post ('profileImage');
		
		$passenger_id = $data ['id'];
	
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['failed'];
		if ($passenger_id > 0) {
                        $data ['signupFrom'] = Signup_Type_Enum::BACKEND;
			$data ['registerType'] = Register_Type_Enum::MANUAL;
                        $data ['deviceType']   = Device_Type_Enum::NO_DEVICE;
			$this->Passenger_Model->update ( $data, array ('id' => $passenger_id ) );
                        
			$msg = $msg_data ['updated'];
		} else {
			$password = random_string ( 'alnum', 8 );
			
			$data ['password'] = hash ( "sha256", $password );
			
			$data ['otp'] = str_pad ( rand ( 0, 999999 ), 6, '0', STR_PAD_LEFT );
			
			//$data ['passengerCode'] = trim ( substr ( strtoupper ( str_replace ( ' ', '', $data ['firstName'] . '' . $data ['lastName'] ) ), 0, 5 ) );
			//$data ['passengerCode'] .= $this->Passenger_Model->getAutoIncrementValue ();
			$data ['activationStatus'] = Status_Type_Enum::ACTIVE;
			$data ['signupFrom'] = Signup_Type_Enum::BACKEND;
			$data ['registerType'] = Register_Type_Enum::MANUAL;
			$data ['deviceType']   = Device_Type_Enum::NO_DEVICE;
			
			$passenger_id = $this->Passenger_Model->insert ( $data );
			//update the Unique Passenger Code
			$passenger_code_id=($passenger_id<=999999)?str_pad ( (string)$passenger_id, 6, '0', STR_PAD_LEFT ):$passenger_id;
			
			$passenger_code = 'PAS' . $passenger_code_id;
			$update_passenger_code=$this->Passenger_Model->update(array('passengerCode' => $passenger_code,),array('id'=>$passenger_id));
			
			
			$msg = $msg_data ['success'];
			if ($passenger_id > 0) {/* 
				// SMS & email service start
				
				if (SMS && $passenger_id) {
					$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
							'title' => 'account_create_sms' 
					) );
					
					$message_data = $message_details->content;
					
					$message_data = str_replace ( "##USERNAME##", $data ['mobile'], $message_data );
					$message_data = str_replace ( "##PASSWORD##", $password, $message_data );
					$message_data = str_replace ( "##APPLINK##", APPLINK, $message_data );
					
					
					// print_r($message);exit;
					if ($data ['mobile'] != "") {
						
						$this->Common_Api_Webservice_Model->sendSMS ( $data ['mobile'], $message_data );
					}
				}
				
				$from = SMTP_EMAIL_ID;
				$to = $data ['email'];
				$subject = 'Get Ready to ride with TakeARight!';
				$data = array (
						'password' => $password,
						'user_name' => $data ['firstName'] . ' ' . $data ['lastName'],
						'mobile' => $data ['mobile'],
						'email' => $data ['email'] 
				);
				$message_data = $this->load->view ( 'emailtemplate/passenger_register', $data, TRUE );
				
				if (SMTP && $passenger_id) {
					
					if ($to) {
						$this->Common_Api_Webservice_Model->sendEmail ( $to, $subject, $message_data );
					}
				} 
				// SMS & email service end
			 */}
		}
		
		// $this->render("passenger/add_edit_passenger", $data);
		
		$response = array (
				'msg' => $msg,
				'passenger_id' => $passenger_id 
		);
		echo json_encode ( $response );
	}
	
	public function getDetailsById($passenger_id, $view_mode) {
	if ($view_mode==EDIT_MODE)
	{
	if ($this->User_Access_Model->writeModule ($this->session->userdata('role_type'),Module_Name_Enum::CUSTOMER) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
	}
	else
	{
		if ($this->User_Access_Model->readModule ($this->session->userdata('role_type'),Module_Name_Enum::CUSTOMER) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
	}     
		$this->addJs ( 'app/passenger.js' );
		// get API messages from message.php
		$msg_data = $this->config->item ( 'msg' );
		$message='';
		$data = array (
				'mode' => $view_mode 
		);
		
		$data ['activetab'] = "passenger";
		$data ['active_menu'] = 'passenger';
		//to get city list
		if ($view_mode==EDIT_MODE)
		{
		$data ['zone_list'] = $this->Zone_Details_Model->getSelectDropdownOptions ( array ('status' => Status_Type_Enum::ACTIVE ), 'name' );
		}
		else 
		{
			$data ['zone_list'] = $this->Zone_Details_Model->getSelectDropdownOptions ( array (), 'id' );
		}
                $data ['device_type'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
                                            'tableName' => strtoupper ( 'DEVICE_TYPE' ),
                                            'isVisible' => Status_Type_Enum::ACTIVE ), 'sequenceOrder' );
		$data ['entity_list'] = $this->Entity_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE ), 'name' );
		$data ['gender_list'] = $this->Data_Attributes_Model->getColumnByKeyValueArray ( 'id,description', array (
				'tableName' => strtoupper ( 'GENDER_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		if ($this->input->post ( 'id' )) {
			$country_id = $this->input->post ( 'id' );
		}
		
		if ($passenger_id > 0) {
			if ($view_mode==EDIT_MODE)
			{
				$locked=$this->Passenger_Model->isLocked($passenger_id);
				if ($locked)
				{
					$locked_user=$this->Passenger_Model->getLockedUser($passenger_id);
					$locked_date=$this->Passenger_Model->getLockedDate($passenger_id);
					$locked_date=date('jS, F Y G:ia',strtotime($locked_date));
					$message=$msg_data ['locked'];
					$message=str_replace('##USERNAME##', $locked_user, $message);
					$message.=$locked_date;
					$data['mode']=VIEW_MODE;
				}
				else
				{
					$is_locked=$this->Passenger_Model->lock($passenger_id);
				}
			}
			$data ['passenger_model'] = $this->Passenger_Model->getById ( $passenger_id );
		} else {
			$data ['passenger_model'] = $this->Passenger_Model->getBlankModel ();
		}
		if ($this->input->post ( 'id' )) {
			
			$html = $this->load->view ( 'passenger/add_edit_passenger', $data );
			echo json_encode ( array (
					'html' => $html 
			) );
		} else {
			$this->render ( 'passenger/add_edit_passenger', $data );
		}
	}
	public function loadPassengerList(){
		if ($this->User_Access_Model->showPassenger () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		$this->addCss ( 'vendors/lightbox/css/imagelightbox.css' );
		$this->addJs ( 'vendors/lightbox/js/imagelightbox.js' );
		$this->addJs ( 'app/passenger.js' );
		$data ['activetab'] = "passenger";
		$data ['active_menu'] = 'passenger';
		$this->render ( 'passenger/manage_passenger', $data );
		
	}
	
	public function changePassengerStatus() {
		if ($this->User_Access_Model->fullAccessModule ($this->session->userdata('role_type'),Module_Name_Enum::CUSTOMER) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to change staus", '404' );
			return;
		}
		$data = array ();
		
		$this->addJs ( 'app/passenger.js' );
		
		$data = $this->input->post();
		$passenger_id = $data ['id'];
		$passenger_status = $data ['status'];
		
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['status_change_failed'];
		if ($passenger_id) {
			$passenger_details = $this->Passenger_Model->getById ( $passenger_id );
			$passenger_updated = $this->Passenger_Model->update ( array (
					'status' => $passenger_status 
			), array (
					'mobile' => $passenger_details->mobile 
			) );
			if ($passenger_status == Status_Type_Enum::INACTIVE) {
				
				// If user already exists update auth key
					
				if ($passenger_id > 0) {
					//Reset authkey to avoid using already logged in passenger to use APP
					$auth_key_exists = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
							'userId' => $passenger_id,
							'userType' => User_Type_Enum::PASSENGER
					) );
					
					// If user already exists update auth authKey
					if ($auth_key_exists) {
						$auth_details_update=$this->Auth_Key_Details_Model->update ( array (
								'authKey' => ''
						), array (
								'userId' => $passenger_id,
								'userType' => User_Type_Enum::PASSENGER
						) );
					}
					else 
					{
						$auth_details_id=$this->Auth_Key_Details_Model->insert( array (
								'authKey' => '',
								'userId' => $passenger_id,
								'userType' => User_Type_Enum::PASSENGER
						));
					}
					// SMS & email service start
					
					// @todo SMS template to be changed later
					/*
					 if (SMS && $passenger_updated) {
						$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
								'smsTitle' => 'forgot_password_sms' 
						) );
						
						$message_data = $message_details->smsContent;
						// $message = str_replace("##USERNAME##",$name,$message);
						// $message_data = str_replace ( "##PASSWORD##", $password, $message_data );
						// print_r($message);exit;
						if ($passenger_details->mobile != "") {
							
							// $this->Api_Webservice_Model->sendSMS ( $passenger_details->mobile, $message_data );
						}
					 }
					*/
					// SMS & email service end
				}
			}
			$msg = $msg_data ['status_change_success'];
		}
		$response = array (
				'msg' => $msg,
				'passenger_id' => $passenger_id 
		);
		echo json_encode ( $response );
	}
	public function checkPassengerEmail() {
		$data = array ();
		
		$data = $this->input->post ();
		$email = $data ['email'];
		$email_exists = NULL;
		$status = 0;
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['email_not_exists'];
		if ($email) {
			$email_exists = $this->Passenger_Model->getOneByKeyValueArray ( array (
					'email' => $email 
			) );
			if ($email_exists) {
				$status = 1;
				$msg = $msg_data ['email_exists'];
			}
		}
		$response = array (
				'msg' => $msg,
				'email' => $email,
				'status' => $status 
		);
		echo json_encode ( $response );
	}
	public function checkPassengerMobile() {
		$data = array ();
		
		$data = $this->input->post ();
		$mobile = $data ['mobile'];
		$mobile_exists = NULL;
		$status = 0;
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['mobile_not_exists'];
		if ($mobile) {
			$mobile_exists = $this->Passenger_Model->getOneByKeyValueArray ( array (
					'mobile' => $mobile 
			) );
			if ($mobile_exists) {
				$status = 1;
				$msg = $msg_data ['mobile_exists'];
			}
		}
		$response = array (
				'msg' => $msg,
				'email' => $mobile,
				'status' => $status 
		);
		echo json_encode ( $response );
	}
	
	public function getPassengerlist()
	{
		$data = array ();
                /* $this->addCss ( 'vendors/datetimepicker/css/jquery.datetimepicker.css' );
                $this->addJs ( 'vendors/datepicker/jquery.datetimepicker.full.js' ); */
                $this->addCss ( 'vendors/lightbox/css/imagelightbox.css' );
                $this->addJs ( 'vendors/lightbox/js/imagelightbox.js' );
		$this->addJs ( 'app/passenger.js' );
		
                //$this->addJs ( 'vendors/datatables/js/jquery.dataTables.min.js' );
                //$this->addJs ( 'vendors/datatables/js/dataTables.bootstrap.min.js' );
                //$this->addJs ( 'vendors/lightbox/js/lightbox.js' );
                //$this->addCss( 'vendors/lightbox/css/lightbox.css');
                //$this->addCss( 'app/pages/tables.css');
                //$this->addCss( 'app/pages/dataTables.bootstrap.css');
		
		$data ['activetab'] = "passenger";
		$data ['active_menu'] = 'passenger';
		
		$this->render ( 'passenger/manage_passenger', $data );
	}
	public function ajax_list()
	{
		//echo"ajaxlist";exit;
		$sql_query=$this->Passenger_Model->getPassengerListQuery();
		$passenger_list = $this->Passenger_Model->getDataTable($sql_query);
		//print_r($list);exit;
		$data = array();
		$no = $_POST['start'];
		foreach ($passenger_list as $list) {
			$profileimage=($list->profileImage)?passenger_content_url($list->passengerId."/".$list->profileImage):image_url('app/user.png');
			$row = array();
                        $row[] = '
			
				  <button class="btn btn-sm btn-success" id="viewpassenger-'.$list->passengerId.'" title="View" >'
                                . '<i class="fa fa-eye" aria-hidden="true"></i></button> '
                                . '<button class="btn btn-sm btn-blue" id="editpassenger-'.$list->passengerId.'" title="Edit">'
                                . '<i class="fa fa-pencil-square-o"></i></button>
                   <button class="btn btn-sm btn-warning" id="viewpassengertransaction-' . $list->passengerId . '" title="Transaction Details" >
                                 <i class="fa fa-exchange" aria-hidden="true"></i></button>';
			
			$row[] = '<a class="example-image-link" href="'.$profileimage.'" data-imagelightbox="listview" data-ilb2-caption="profileImage">'
					. '<img src="'.$profileimage.'" width="40" height="40"></a>';
			$row[] = $list->passengerCode;
			$row[] = ucfirst($list->firstName);
			$row[] = ucfirst($list->lastName);
			$row[] = $list->mobile;
			$row[] = $list->email;
			$row[] = round($list->passengerAvgRating);
			$row[] = $list->walletAmount;
			$row[] = $list->joinedDatetime;
			$row[] = ucfirst($list->zoneName);
			$row[] = $list->lastLoggedIn;
			$log_status_class=($list->loginStatus)?'class="rbutton btn-success"':'class="rbutton btn-danger"';
			$log_status_name=($list->loginStatus)?'In':'Out';
			$log_status_id=($list->loginStatus)?'id="logstatus-'.$list->passengerId.'"':'';
			$row[] = '<button '.$log_status_id.' status="'.$list->loginStatus.'" '.$log_status_class.'><span>'.$log_status_name.'</span></button>';
			$status_class=($list->status)?'class="label label-sm label-success"':'class="label label-sm label-danger"';
			$status_name=($list->status)?'Active':'Inactive';
			
			$row[] = '<span id="status-'.$list->passengerId.'" status="'.$list->status.'" '.$status_class.'>'.$status_name.'</span>';
			//add html for action
			
				
                 // <button class="btn btn-sm btn-danger" id="deletepassenger-'.$list->passengerId.'" title="Delete" ><i class="fa fa-trash-o"></i></button>';
			
			$data[] = $row;
		}
	
		$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->Passenger_Model->getDataTableAllCount($sql_query),
				"recordsFiltered" => $this->Passenger_Model->getDataTableFilterCount($sql_query),
				"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
      
	public function save_guestpassenger(){
		$data = array ();
		$data = $this->input->post();
		$data ['activationStatus']  = Status_Type_Enum::ACTIVE;
		$data ['signupFrom']        = Signup_Type_Enum::BACKEND;
		$data ['registerType']      = Register_Type_Enum::MANUAL;
		$data ['deviceType']        = Device_Type_Enum::NO_DEVICE;
		$name                       = $data ['passengerName'];
		$mobile                     = $data ['passengerMobile'];
		$email                      = $data ['passengerEmail'];
		$mobile_exists              = $this->Passenger_Model->getOneByKeyValueArray ( array ('mobile' => $mobile ) );
		if($mobile_exists){
			echo "0";
			exit;
		}
		$data ['firstName']         = $data ['passengerName'];
		$data ['mobile']            = $data ['passengerMobile'];
		$data ['email']             = $data ['passengerEmail'];
		$data ['entityId']          = DEFAULT_COMPANY_ID;
	
		$passenger_id = $this->Passenger_Model->insert($data);
		//update the Unique Passenger Code
		$passenger_code_id=($passenger_id<=999999)?str_pad ( (string)$passenger_id, 6, '0', STR_PAD_LEFT ):$passenger_id;
			
		$passenger_code = 'PAS' . $passenger_code_id;
		$update_passenger_code=$this->Passenger_Model->update(array('passengerCode' => $passenger_code,),array('id'=>$passenger_id));
			
		echo $passenger_id;
	}
	/**
	 * * Driver Transaction details section starts **
	 */
	public function getpassengerTransactionDetailsList($passenger_id) {
		if ($this->User_Access_Model->showDriver () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		/**
		 * code to redirect driver transaction list for invlaid id *
		 */
		$data ['validate_id'] = $this->Passenger_Model->getById ( $passenger_id );
		$check_valid_passenger_id = count ( $data ['validate_id'] );
		if ($check_valid_passenger_id == 0) {
			redirect ( base_url () . 'Driver/getPassengerList' );
			exit ();
		}
		$data = array ();
		if ($passenger_id > 0) {
			$passenger_details = $this->Passenger_Model->getColumnByKeyValueArray ( 'firstName,lastName', array (
					'id' => $passenger_id
			) );
			$data ['passenger_fullname'] = $passenger_details [0]->firstName . ' ' . $passenger_details [0]->lastName;
		} else {
			$data ['passenger_fullname'] = '';
		}
		$this->addJs ( 'app/passenger-transaction-details.js' );
		// echo "cool"; exit;
		$data ['activetab'] = "passenger";
		$data ['active_menu'] = 'passenger';
	
		$this->render ( 'passenger/manage_passenger_transactiondetails', $data );
	}
	public function ajax_list_passengertransaction() {
		// echo"ajaxlist";exit;
		$passenger_id = $_POST ['passenger_id'];
		$sql_query = $this->Passenger_Transaction_Details_Model->getPassengerTransactionListQuery ( $passenger_id );
		$passenger_list = $this->Passenger_Transaction_Details_Model->getDataTable ( $sql_query );
		$data = array ();
		$no = $_POST ['start'];
		foreach ( $passenger_list as $list ) {
			$no ++;
			$row = array ();
			$row [] = '<button class="btn btn-sm btn-success" id="viewpassengertransaction-' . $list->id . '" title="View">' . '<i class="fa fa-eye"></i></button>';
				
			// $row[] = ucfirst($list->driverName);
			$row [] = $list->tripId;
			$row [] = $list->transactionAmount;
			$row [] = $list->previousAmount;
			$row [] = $list->currentAmount;
			$row [] = $list->transactionStatus;
			$row [] = $list->transactionFrom;
			$row [] = $list->transactionType;
			$row [] = $list->transactionMode;
			$row [] = $list->createdDatetime;
			$row [] = $list->transactionId;
			$row [] = $list->comments;
			// add html for action
				
			$data [] = $row;
		}
	
		$output = array (
				"draw" => $_POST ['draw'],
				"recordsTotal" => $this->Passenger_Transaction_Details_Model->getDataTableAllCount ( $sql_query ),
				"recordsFiltered" => $this->Passenger_Transaction_Details_Model->getDataTableFilterCount ( $sql_query ),
				"data" => $data
		);
		// output to json format
		echo json_encode ( $output );
	}
	public function addPassengerTransaction($passenger_id) {
		if ($this->User_Access_Model->showDriver () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		$msg_data = $this->config->item ( 'msg' );
	
		$this->addJs ( 'app/passenger-transaction-details.js' );
	
		$data = array (
				'mode' => EDIT_MODE
		);
		// print_r($data);exit;
		$data ['activetab'] = "passenger";
		$data ['active_menu'] = 'passenger';
		$data ['transaction_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRANSACTION_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		unset($data ['transaction_type_list'][Transaction_Type_Enum::EASY_POINTS]);
		unset($data ['transaction_type_list'][Transaction_Type_Enum::PROMOCODE]);
		unset($data ['transaction_type_list'][Transaction_Type_Enum::TRIP_COMMISSION]);
		unset($data ['transaction_type_list'][Transaction_Type_Enum::TRIP_WALLET]);
		unset($data ['transaction_type_list'][Transaction_Type_Enum::TRIP_REJECT_PENALTY]);
		unset($data ['transaction_type_list'][Transaction_Type_Enum::BOOKING_CHARGE]);
		unset($data ['transaction_type_list'][Transaction_Type_Enum::PASSENGER_PENALTY_CHARGE]);
		unset($data ['transaction_type_list'][Transaction_Type_Enum::TAX_AMOUNT]);
		
		
		$data ['transaction_mode_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRANSACTION_MODE' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		$data ['transaction_from_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRANSACTION_FROM' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		unset($data ['transaction_from_list'][Transaction_From_Enum::CREDIT_ACCOUNT]);
		unset($data ['transaction_from_list'][Transaction_From_Enum::CASH]);
		/**
		 * code to redirect driver list for invlaid id *
		*/
		$data ['validate_id'] = $this->Passenger_Model->getById ( $passenger_id );
		$check_valid_passenger_id = count ( $data ['validate_id'] );
		if ($check_valid_passenger_id == 0) {
			echo "<script>window.history.back();</script>";
			exit ();
		}
	
		if ($passenger_id > 0) {
			$data ['passenger_model'] = $this->Passenger_Model->getById ( $passenger_id );
		} else {
		}
		if ($passenger_id > 0) {
				
			$data ['passenger_transaction_model'] = $this->Passenger_Transaction_Details_Model->getById ( $passenger_id );
			$data ['passenger_transaction_model'] = $this->Passenger_Transaction_Details_Model->getBlankModel ();
		}
		$this->render ( 'passenger/add_edit_passenger_transactiondetails', $data );
	}
	public function getTransactionDetailsById($passenger_transaction_id, $view_mode) {
		if ($this->User_Access_Model->showDriver () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		$msg_data = $this->config->item ( 'msg' );
	
		$this->addJs ( 'app/passenger-transaction-details.js' );
	
		$data = array (
				'mode' => VIEW_MODE
		);
		// print_r($data);exit;
		$data ['activetab'] = "driver";
		$data ['active_menu'] = 'driver';
		$data ['transaction_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRANSACTION_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		unset($data ['transaction_type_list'][Transaction_Type_Enum::EASY_POINTS]);
		unset($data ['transaction_type_list'][Transaction_Type_Enum::PROMOCODE]);
		unset($data ['transaction_type_list'][Transaction_Type_Enum::TRIP_COMMISSION]);
		unset($data ['transaction_type_list'][Transaction_Type_Enum::TRIP_WALLET]);
		unset($data ['transaction_type_list'][Transaction_Type_Enum::TRIP_REJECT_PENALTY]);
		unset($data ['transaction_type_list'][Transaction_Type_Enum::BOOKING_CHARGE]);
		unset($data ['transaction_type_list'][Transaction_Type_Enum::PASSENGER_PENALTY_CHARGE]);
		unset($data ['transaction_type_list'][Transaction_Type_Enum::TAX_AMOUNT]);
		
		$data ['transaction_mode_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRANSACTION_MODE' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		$data ['transaction_from_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRANSACTION_FROM' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		unset($data ['transaction_from_list'][Transaction_From_Enum::CREDIT_ACCOUNT]);
		if ($this->input->post ( 'id' )) {
			$passenger_transaction_id = $this->input->post ( 'id' );
		}
	
		/**
		 * code to redirect driver list for invlaid id *
		 */
		$data ['validate_id'] = $this->Passenger_Transaction_Details_Model->getById ( $passenger_transaction_id );
		$check_valid_passenger_id = count ( $data ['validate_id'] );
		if ($check_valid_passenger_id == 0) {
			echo "<script>window.history.back();</script>";
			exit ();
		}
		if ($passenger_transaction_id > 0) {
			if ($view_mode == EDIT_MODE) {
				$locked = $this->Passenger_Transaction_Details_Model->isLocked ( $passenger_transaction_id );
				if ($locked) {
					$locked_user = $this->Passenger_Transaction_Details_Model->getLockedUser ( $passenger_transaction_id );
					$locked_date = $this->Passenger_Transaction_Details_Model->getLockedDate ( $passenger_transaction_id );
					$locked_date = date ( 'jS, F Y G:ia', strtotime ( $locked_date ) );
					$message = $msg_data ['locked'];
					$message = str_replace ( '##USERNAME##', $locked_user, $message );
					$message .= $locked_date;
						
					$data ['mode'] = VIEW_MODE;
				} else {
					$is_locked = $this->Passenger_Transaction_Details_Model->lock ( $passenger_transaction_id );
				}
			}
				
			$data ['passenger_transaction_model'] = $this->Passenger_Transaction_Details_Model->getById ( $passenger_transaction_id );
			$data ['passenger_model'] = $this->Passenger_Model->getById ( $data ['passenger_transaction_model']->passengerId );
		} else {
			// Blank Model
			$data ['passenger_transaction_model'] = $this->Passenger_Transaction_Details_Model->getBlankModel ();
			$data ['passenger_model'] = $this->Passenger_Model->getBlankModel ();
		}
		if ($this->input->post ( 'id' )) {
				
			$html = $this->load->view ( 'passenger/add_edit_passenger_transactiondetails', $data );
			echo json_encode ( array (
					'html' => $html
			) );
		} else {
			$this->render ( 'passenger/add_edit_passenger_transactiondetails', $data );
		}
	}
	public function savePassengerTransaction() {
		$this->addJs ( 'app/passenger-transaction-details.js' );
		$data = array ();
		$data = $this->input->post ();
	//debug_exit($data);
		$passenger_transaction_id = $data ['id'];
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['failed'];
		if ($passenger_transaction_id > 0) {
			$this->Passenger_Transaction_Details_Model->update ( $data, array (
					'id' => $passenger_transaction_id
			) );
			
				$update_passenger_wallet = $this->Passenger_Model->update ( array (
						'walletAmount' => $data ['currentAmount']
				), array (
						'id' => $data ['passengerId']
				) );
			
				
			$msg = $msg_data ['updated'];
		} else {
				
			$passenger_transaction_id = $this->Passenger_Transaction_Details_Model->insert ( $data );
			
				$update_passenger_wallet = $this->Passenger_Model->update ( array (
						'walletAmount' => $data ['currentAmount']
				), array (
						'id' => $data ['passengerId']
				) );
				
			$msg = $msg_data ['success'];
		}
	
		$response = array (
				'msg' => $msg,
				'passenger_transaction_id' => $passenger_transaction_id
		);
		echo json_encode ( $response );
	}
	
	public function changePassengerLogStatus() {
		if ($this->User_Access_Model->fullAccessModule ( $this->session->userdata ( 'role_type' ), Module_Name_Enum::CUSTOMER ) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to change status", '404' );
			return;
		}
		$data = array ();
	
		$this->addJs ( 'app/passenger.js' );
		$passenger_updated = - 1;
		$data = $this->input->post ();
		$passenger_id = $data ['id'];
		$user_status = $data ['status'];
	
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['status_change_failed'];
		if ($passenger_id) {
			$passenger_updated = $this->Passenger_Model->update ( array (
					'loginStatus' => $user_status
			), array (
					'id' => $passenger_id
			) );
			$update_auth_key = $this->Auth_Key_Details_Model->update ( array ('authKey' => '',), array('userId' => $passenger_id,'userType' =>User_Type_Enum::PASSENGER) );
			$msg = $msg_data ['status_change_success'];
		}
		$response = array (
				'msg' => $msg,
				'status' => $passenger_updated
		);
		echo json_encode ( $response );
	}
}// end of class