<?php
require_once (APPPATH . 'libraries/Rest_Controller.php');
class CommonApiWebService extends Rest_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->model ( 'Common_Api_Webservice_Model' );
		$this->load->model ( 'Passenger_Model' );
		$this->load->model ( 'Auth_Key_Details_Model' );
		$this->load->model ( 'Common_Api_Webservice_Model' );
		$this->load->model ( 'Merchant_Details_Model' );
		$this->load->model ( 'Referral_Details_Model' );
		$this->load->model ( 'Merchant_Transaction_Details_Model' );
		$this->load->model ( 'Passenger_Transaction_Details_Model' );
		$this->load->model ( 'Country_Model' );
		
		$this->load->model ( 'Sms_Template_Model' );
		
		$this->load->model ( 'Data_Attributes_Model' );
		$this->load->model ( 'Partner_Entity_Model' );
		$this->load->model ( 'Entity_Model' );
		$this->load->model ( 'User_Login_Details_Model' );
		$this->load->model ( 'User_Personal_Details_Model' );
	}
	public function get_post_data($validate = FALSE) {
		$data = file_get_contents ( 'php://input' );
		$post = json_decode ( $data );
		if (! $post) {
			$post = json_decode ( json_encode ( $_POST ), FALSE );
		}
		return $post;
	}
	public function getbase64PassengerImageUrl($passenger_id, $image) {
		$file_image = '';
		$img = $image;
		$img = str_replace ( 'data:image/png;base64,', '', $img );
		$img = str_replace ( ' ', '+', $img );
		$data = base64_decode ( $img );
		$file_name = "profile_image.jpg";
		$upload_path = $this->config->item ( 'passenger_content_path' ) . "/" . $passenger_id;
		if (! file_exists ( $upload_path )) {
			mkdir ( $upload_path, 0777 );
		}
		$file = $upload_path . "/" . $file_name;
		$success = file_put_contents ( $file, $data );
		if ($success) {
			$file_image = $file_name;
		} else {
			$file_image = '';
		}
		return $file_image;
	}
	public function getbase64MerchantImageUrl($merchant_id, $image) {
		$file_image = '';
		$img = $image;
		$img = str_replace ( 'data:image/png;base64,', '', $img );
		$img = str_replace ( ' ', '+', $img );
		$data = base64_decode ( $img );
		$file_name = "profile_image.jpg";
		$upload_path = $this->config->item ( 'merchant_content_path' ) . "/" . $merchant_id;
		if (! file_exists ( $upload_path )) {
			mkdir ( $upload_path, 0777 );
		}
		$file = $upload_path . "/" . $file_name;
		$success = file_put_contents ( $file, $data );
		if ($success) {
			$file_image = $file_name;
		} else {
			$file_image = '';
		}
		return $file_image;
	}
	public function getbase64UserImageUrl($user_id, $image) {
		$file_image = '';
		$img = $image;
		$img = str_replace ( 'data:image/png;base64,', '', $img );
		$img = str_replace ( ' ', '+', $img );
		$data = base64_decode ( $img );
		$file_name = "profile_image.jpg";
		$upload_path = $this->config->item ( 'user_content_path' ) . "/" . $user_id;
		if (! file_exists ( $upload_path )) {
			mkdir ( $upload_path, 0777 );
		}
		$file = $upload_path . "/" . $file_name;
		$success = file_put_contents ( $file, $data );
		if ($success) {
			$file_image = $file_name;
		} else {
			$file_image = '';
		}
		return $file_image;
	}
	public function signup_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		
		$referrer_id = Null;
		$referral_insert_id = NULL;
		$referrer_exists = array ();
		$referrer_type = User_Type_Enum::NONE;
		
		$merchant_id = NULL;
		$passenger_id = NULL;
		$merchant_exists = NULL;
		$passenger_exists = NULL;
		$user_exists=NULL;
		$update_merchant = NULL;
		$update_passenger = NULL;
		$response_data = array ();
		
		$email = NULL;
		$company_name = '';
		$first_name = NULL;
		$last_name = NULL;
		$mobile = NULL;
		$password = NULL;
		$city_id = NULL;
		$device_id = NULL;
		$device_type = NULL;
		$refered_code = NULL;
		$profile_image = NULL;
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$email = $post_data->email;
		$first_name = $post_data->first_name;
		$last_name = $post_data->last_name;
		$mobile = $post_data->mobile;
		$password = $post_data->password;
		$device_id = $post_data->device_id;
		$device_type = $post_data->device_type;
		$refered_code = $post_data->refered_code;
		$profile_image = $post_data->profile_image;
		$city_id = $post_data->city_id;
		if (array_key_exists ( 'company_name', $post_data )) {
			$company_name = $post_data->company_name;
		}
		
		// actual functionality
		// to get site information details
		$entity_details = $this->Entity_Model->getById ( DEFAULT_COMPANY_ID );
		
		if ($email && $mobile && $password) {
			
			$password = hash ( "sha256", $password );
			// @todo generate 6 digit otp common otp for both merchnat & passenger
			$otp = str_pad ( rand ( 0, 999999 ), 6, '0', STR_PAD_LEFT );
			
			$merchant_exists = $this->Merchant_Details_Model->getOneByKeyValueArray ( array (
					'email' => $email 
			) );
			$passenger_exists = $this->Passenger_Model->getOneByKeyValueArray ( array (
					'email' => $email 
			) );
			
			$user_exists=$this->User_Login_Details_Model->getOneByKeyValueArray ( array (
					'loginEmail' => $email 
			) );
			if ($merchant_exists || $passenger_exists || $user_exists) {
				$message = array (
						"message" => $msg_data ['email_exists'],
						"status" => - 1 
				);
			} else if (! $merchant_exists && ! $passenger_exists || ! $user_exists) {
				$merchant_exists = $this->Merchant_Details_Model->getOneByKeyValueArray ( array (
						'mobile' => $mobile 
				) );
				$passenger_exists = $this->Passenger_Model->getOneByKeyValueArray ( array (
						'mobile' => $mobile 
				) );
				$user_exists = $this->User_Login_Details_Model->getOneByKeyValueArray ( array (
						'mobile' => $mobile
				) );
			
			if ($merchant_exists || $passenger_exists || $user_exists) {
				$message = array (
						"message" => $msg_data ['mobile_exists'],
						"status" => - 1 
				);
			}
			} 
			if (! $merchant_exists && ! $passenger_exists && ! $user_exists) {
				
				if ($refered_code) {
					$referrer_exists = $this->Passenger_Model->getOneByKeyValueArray ( array (
							'passengerCode' => $refered_code 
					) );
					$referrer_type = User_Type_Enum::PASSENGER;
					if (! $referrer_exists) {
						$referrer_exists = $this->Merchant_Details_Model->getOneByKeyValueArray ( array (
								'merchantCode' => $refered_code 
						) );
						$referrer_type = User_Type_Enum::MERCHANT;
					}
					if ($referrer_exists) {
						$referrer_id = $referrer_exists->id;
					} else {
						
						$message = array (
								"message" => $msg_data ['valid_referral_code'],
								"status" => - 1 
						);
					}
				}
				// merchant account ceation
				// merchant unique code
				$merchant_code = trim ( substr ( strtoupper ( str_replace ( ' ', '', $first_name . '' . $last_name ) ), 0, 5 ) );
				$merchant_code .= $this->Merchant_Details_Model->getAutoIncrementValue ();
				
				$insert_data = array (
						'companyName' => $company_name,
						'firstName' => $first_name,
						'lastName' => $last_name,
						'email' => $email,
						'mobile' => $mobile,
						'password' => $password,
						'otp' => $otp,
						'deviceId' => $device_id,
						'deviceType' => $device_type,
						'cityId' => $city_id,
						'entityId' => DEFAULT_COMPANY_ID,
						'merchantCode' => $merchant_code,
						'merchantType' => Merchant_Type_Enum::DIRECT,
						'signupFrom' => Signup_Type_Enum::MOBILE,
						'registerType' => Register_Type_Enum::MANUAL 
				);
				$merchant_id = $this->Merchant_Details_Model->insert ( $insert_data );
				
				// merchant admin user account creation
				$user_identity = 'MER' . str_pad ( $this->User_Login_Details_Model->getAutoIncrementValue (), 4, '0', STR_PAD_LEFT );
				$insert_user_data = array (
						'userIdentity' => $user_identity,
						'firstName' => $first_name,
						'lastName' => $last_name,
						'loginEmail' => $email,
						'mobile' => $mobile,
						'password' => $password,
						'designation' => 'Merchant',
						'doj' => getCurrentDate (),
						'roleType' => Role_Type_Enum::MERCHANT,
						'roleUserId' => $merchant_id,
						'cityId' => $city_id,
						'entityId' => DEFAULT_COMPANY_ID 
				);
				$user_id = $this->User_Login_Details_Model->insert ( $insert_user_data );
				if ($user_id) {
					
					$insert_user_personal_data = array (
							'userId' => $user_id,
							'mobile' => $mobile,
							'email' => $email 
					);
					$user_personal_id = $this->User_Personal_Details_Model->insert ( $insert_user_personal_data );
				}
				
				if ($merchant_id) {
					// merchant profile image upload
					if (array_key_exists ( 'profile_image', $post_data ) && $profile_image) {
						$data ['file_name'] = $this->getbase64MerchantImageUrl ( $merchant_id, $profile_image );
						
						if ($data) {
							
							$update_merchant = $this->Merchant_Details_Model->update ( array (
									'profileImage' => $data ['file_name'] 
							), array (
									'id' => $merchant_id 
							) );
							
							if ($user_id) {
								$data ['file_name'] = $this->getbase64UserImageUrl ( $user_id, $profile_image );
								$update_user = $this->User_Login_Details_Model->update ( array (
										'profileImage' => $data ['file_name'] 
								), array (
										'id' => $user_id 
								) );
							}
						}
					}
					// passenger account creation
					// passenger unique code
					$passenger_code = trim ( substr ( strtoupper ( str_replace ( ' ', '', $first_name . '' . $last_name ) ), 0, 5 ) );
					$passenger_code .= $this->Passenger_Model->getAutoIncrementValue ();
					
					$insert_data = array (
							'firstName' => $first_name,
							'lastName' => $last_name,
							'email' => $email,
							'mobile' => $mobile,
							'password' => $password,
							'otp' => $otp,
							'deviceId' => $device_id,
							'deviceType' => $device_type,
							'cityId' => $city_id,
							'entityId' => DEFAULT_COMPANY_ID,
							'passengerCode' => $passenger_code,
							'signupFrom' => Signup_Type_Enum::MOBILE,
							'registerType' => Register_Type_Enum::MANUAL 
					);
					$passenger_id = $this->Passenger_Model->insert ( $insert_data );
					
					if ($passenger_id) {
						// passenger profile image upload
						if (array_key_exists ( 'profile_image', $post_data ) && $profile_image) {
							$data ['file_name'] = $this->getbase64PassengerImageUrl ( $passenger_id, $profile_image );
							
							if ($data) {
								
								$update_passenger = $this->Passenger_Model->update ( array (
										'profileImage' => $data ['file_name'] 
								), array (
										'id' => $passenger_id 
								) );
							}
						}
						
						if ($referrer_id) {
							$insert_data = array (
									'referrerId' => $referrer_id,
									'registeredId' => $passenger_id,
									'isReferrerEarned' => Status_Type_Enum::ACTIVE,
									'earnedAmount' => $entity_details->referralDiscountAmount,
									'userType' => User_Type_Enum::BOTH,
									'referrerType' => $referrer_type,
									'status' => Status_Type_Enum::ACTIVE 
							);
							$referral_insert_id = $this->Referral_Details_Model->insert ( $insert_data );
							$wallet_amount = $referrer_exists->walletAmount + $entity_details->referralDiscountAmount;
							if ($referrer_type == User_Type_Enum::MERCHANT) {
								$referrer_wallet_update = $this->Merchant_Details_Model->update ( array (
										'walletAmount' => $wallet_amount 
								), array (
										'id' => $referrer_id 
								) );
								$transaction_insert_data = array (
										'merchantId' => $referrer_id,
										'referralId' => $referral_insert_id,
										'transactionAmount' => $entity_details->referralDiscountAmount,
										'transactionStatus' => 'Success',
										'previousAmount' => $referrer_exists->walletAmount,
										'currentAmount' => $wallet_amount,
										'transactionMode' => Transaction_Mode_Enum::DEBIT,
										'transactionType' => Transaction_Type_Enum::REFERRAL 
								);
								$merchant_transaction_id = $this->Merchant_Transaction_Details_Model->insert ( $transaction_insert_data );
							}
							if ($referrer_type == User_Type_Enum::PASSENGER) {
								$referrer_wallet_update = $this->Passenger_Model->update ( array (
										'walletAmount' => $wallet_amount 
								), array (
										'id' => $referrer_id 
								) );
								$transaction_insert_data = array (
										'passengerId' => $referrer_id,
										'referralId' => $referral_insert_id,
										'transactionAmount' => $entity_details->referralDiscountAmount,
										'transactionStatus' => 'Success',
										'previousAmount' => $referrer_exists->walletAmount,
										'currentAmount' => $wallet_amount,
										'transactionMode' => Transaction_Mode_Enum::DEBIT,
										'transactionType' => Transaction_Type_Enum::REFERRAL 
								);
								$passenger_transaction_id = $this->Passenger_Transaction_Details_Model->insert ( $transaction_insert_data );
							}
						}
						
						if (SMS && $passenger_id && $merchant_id) {
							$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
									'title' => 'otp' 
							) );
							
							$message_data = $message_details->content;
							// $message = str_replace("##USERNAME##",$name,$message);
							$message_data = str_replace ( "##OTP##", $otp, $message_data );
							// print_r($message);exit;
							if ($mobile != "") {
								
								$this->Common_Api_Webservice_Model->sendSMS ( $mobile, $message_data );
							}
						}
						
						/*if (SMTP && $passenger_id && $merchant_id) {
						 * $from = SMTP_EMAIL_ID;
						 * $to = $email;
						 * $subject = 'TakeARight OTP';
						 * $data = array (
						 * 'otp' => $otp
						 * );
						 * $message_data = $this->load->view ( 'emailtemplate/otp', $data, TRUE );
						 *
						 * 
						 *
						 * if ($to) {
						 * $this->Common_Api_Webservice_Model->sendEmail ( $to, $subject, $message_data );
						 * }
						 * } else {
						 *
						 * // To send HTML mail, the Content-type header must be set
						 * $headers = 'MIME-Version: 1.0' . "\r\n";
						 * $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
						 * // Additional headers
						 * $headers .= 'From: Zuver<' . $from . '>' . "\r\n";
						 * $headers .= 'To: <' . $to . '>' . "\r\n";
						 * if (! filter_var ( $to, FILTER_VALIDATE_EMAIL ) === false) {
						 * mail ( $to, $subject, $message_data, $headers );
						 * }
						 * }
						 */
						
						// response data
						$response_data = array (
								"passenger_id" => $passenger_id,
								"merchant_id" => $merchant_id,
								'otp' => $otp 
						);
						
						if ($update_passenger && $passenger_id && $update_merchant && $merchant_id) {
							$message = array (
									"message" => $msg_data ['signup_success'],
									"detail" => $response_data,
									"status" => 1 
							);
						} else if (! $update_passenger && $passenger_id && ! $update_merchant && $merchant_id) {
							$message = array (
									"message" => $msg_data ['signup_success_image_failed'],
									"detail" => $response_data,
									"status" => 1 
							);
						}
					} 

					else {
						$message = array (
								"message" => $msg_data ['signup_failed'],
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['signup_failed'],
							"status" => - 1 
					);
				}
			}
		} else {
			$message = array (
					"message" => $msg_data ['mandatory_data'],
					"status" => - 1 
			);
		}
		
		echo json_encode ( $message );
	}
	public function otpVerification_post() {
		
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		
		$message = '';
		$signup_form = NULL;
		$check_otp = NULL;
		$user_exists = array ();
		$response_data = array ();
		
		$merchant_id = NULL;
		$passenger_id = NULL;
		$otp = NULL;
		
		// Posted json data
		$post_data = $this->get_post_data ();
		
		$merchant_id = $post_data->merchant_id;
		$passenger_id = $post_data->passenger_id;
		$otp = $post_data->otp;
		
		// actual functionality
		
		if ($merchant_id && $otp) {
			
			// To get tax & admin commission entity details
			$partner_entity_details = $this->Partner_Entity_Model->getOneByKeyValueArray ( array (
					'id' => DEFAULT_COMPANY_ID 
			) );
			$entity_details = $this->Entity_Model->getOneByKeyValueArray ( array (
					'id' => $partner_entity_details->entityId 
			) );
			
			$user_exists = $this->Merchant_Details_Model->getOneByKeyValueArray ( array (
					'id' => $merchant_id,
					'otp' => $otp 
			) );
			if ($user_exists) {
				
				$check_otp = $this->Merchant_Details_Model->update ( array (
						'activationStatus' => Status_Type_Enum::ACTIVE,
						'loginStatus' => Status_Type_Enum::ACTIVE,
						'lastLoggedIn' => getCurrentDateTime () 
				), array (
						'id' => $merchant_id 
				) );
			}
			
			$user_exists = $this->Passenger_Model->getOneByKeyValueArray ( array (
					'id' => $passenger_id,
					'otp' => $otp 
			) );
			if ($user_exists) {
				
				$check_otp = $this->Passenger_Model->update ( array (
						'activationStatus' => Status_Type_Enum::ACTIVE,
						'loginStatus' => Status_Type_Enum::ACTIVE,
						'lastLoggedIn' => getCurrentDateTime () 
				), array (
						'id' => $passenger_id 
				) );
			}
			
			if ($user_exists && $check_otp) {
				
				// Auth details
				$key = random_string ( 'alnum', 16 );
				$auth_key = hash ( "sha256", $key );
				$auth_key_exists = $this->Auth_Key_Details_Model->getOneByKeyValueArray ( array (
						'userId' => $user_exists->id,
						'userType' => User_Type_Enum::PASSENGER 
				) );
				
				// If user already exists update auth authKey
				if ($auth_key_exists) {
					$this->Auth_Key_Details_Model->update ( array (
							'authKey' => $auth_key 
					), array (
							'userId' => $user_exists->id,
							'userType' => User_Type_Enum::PASSENGER 
					) );
				}  // create new auth authKey for passenger
else {
					$this->Auth_Key_Details_Model->insert ( array (
							'authKey' => $auth_key,
							'userId' => $user_exists->id,
							'userType' => User_Type_Enum::PASSENGER 
					) );
				}
				// Authentication details
				$auth_details = array (
						'auth_key' => $auth_key,
						'user_id' => $user_exists->id,
						'user_type' => User_Type_Enum::PASSENGER 
				);
				if (SMS && $check_otp) {
					$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
							'title' => 'welcome' 
					) );
					
					$message_data = $message_details->content;
					$message_data = str_replace ( "##USERNAME##", $user_exists->email . '/' . $user_exists->mobile, $message_data );
					// print_r($message);exit;
					if ($user_exists->mobile != "") {
						
						$this->Common_Api_Webservice_Model->sendSMS ( $user_exists->mobile, $message_data );
					}
				}
				/*
				 *
				 * $from = SMTP_EMAIL_ID;
				 * $to = $user_exists->email;
				 * $subject = 'Get Ready to ride with Zuver!';
				 * $data = array (
				 * 'user_name' => $user_exists->firstName . ' ' . $user_exists->lastName,
				 * 'mobile' => $user_exists->mobile,
				 * 'email' => $user_exists->email,
				 * 'password' => NULL
				 * );
				 * $message_data = $this->load->view ( 'emailtemplate/passenger_register', $data, TRUE );
				 *
				 * if (SMTP && $check_otp) {
				 *
				 * if ($to) {
				 * $this->Common_Api_Webservice_Model->sendEmail ( $to, $subject, $message_data );
				 * }
				 * } else {
				 *
				 * // To send HTML mail, the Content-type header must be set
				 * $headers = 'MIME-Version: 1.0' . "\r\n";
				 * $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				 * // Additional headers
				 * $headers .= 'From: Zuver<' . $from . '>' . "\r\n";
				 * $headers .= 'To: <' . $to . '>' . "\r\n";
				 * if (! filter_var ( $to, FILTER_VALIDATE_EMAIL ) === false) {
				 * mail ( $to, $subject, $message_data, $headers );
				 * }
				 * }
				 */
				// response_data
				
				// to get brand_category_type list from data attributes
				$brand_category_type_list = $this->Data_Attributes_Model->getColumnByKeyValueArray ( 'id,description', array (
						'tableName' => strtoupper('ADVT_CATEGORY'),'isVisible'=>Status_Type_Enum::ACTIVE
				) );
				
				// to get the referral message template
				$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
						'title' => 'referral_sms' 
				) );
				
				$referral_message_data = $message_details->content;
				
				$referral_message_data = str_replace ( "##REFERRALCODE##", $user_exists->passengerCode, $referral_message_data );
				$referral_message_data = str_replace ( "##REFERRALDISCOUNT##", $entity_details->referralDiscountAmount, $referral_message_data );
				
				// to get the share message template
				$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
						'title' => 'share_sms' 
				) );
				$share_message_data = $message_details->content;
				$share_message_data = str_replace ( "##USERNAME##", $user_exists->firstName . ' ' . $user_exists->lastName, $share_message_data );
				
				$response_data = array (
						"auth_details" => $auth_details,
						"passenger_id" => $user_exists->id,
						"full_name" => $user_exists->firstName . ' ' . $user_exists->lastName,
						"mobile" => $user_exists->mobile,
						"email" => $user_exists->email,
						'wallet_amount' => $user_exists->walletAmount,
						'share_message' => $share_message_data,
						'referral_message' => $referral_message_data,
						"profile_image" => ($user_exists->profileImage) ? passenger_content_url ( '/' . $user_exists->id . '/' . $user_exists->profileImage ) : '',
						"passenger_referral_code" => ($user_exists->passengerCode) ? $user_exists->passengerCode : '' ,
						"advt_category_type"=>$brand_category_type_list
				);
				// response data
				$message = array (
						"message" => $msg_data ['activation_success'],
						"detail" => $response_data,
						"status" => 1 
				);
			} else {
				$message = array (
						"message" => $msg_data ['invaild_otp'],
						"status" => - 1 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['mandatory_data'],
					"status" => - 1 
			);
		}
		
		echo json_encode ( $message );
	}
	public function getCoreConfig_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		
		$post_data = array ();
		$response_data = array ();
		$data_auth_key = FALSE;
		
		$message = '';
		// Posted json data
		$post_data = $this->get_post_data ();
		
		// To get tax & admin commission entity details
		$partner_entity_details = $this->Partner_Entity_Model->getOneByKeyValueArray ( array (
				'id' => DEFAULT_COMPANY_ID 
		) );
		$entity_details = $this->Entity_Model->getOneByKeyValueArray ( array (
				'id' => $partner_entity_details->entityId 
		) );
		// to get currency code/currency symbol currencySymbol
		$currency_details = $this->Country_Model->getColumnByKeyValueArray ( 'currencySymbol', array (
				'id' => $entity_details->countryId 
		) );
		// to get trip_type list from data attributes
		$trip_type_list = $this->Data_Attributes_Model->getColumnByKeyValueArray ( 'id,description', array (
				'tableName' => strtoupper ( 'TRIP_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		) );
		
		// to get payment_mode list from data attributes
		$payment_mode_list = $this->Data_Attributes_Model->getColumnByKeyValueArray ( 'id,description', array (
				'tableName' => strtoupper ( 'PAYMENT_MODE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		) );
		
		// to get taxi_category_type list from data attributes
		$taxi_category_type_list = $this->Data_Attributes_Model->getColumnByKeyValueArray ( 'id,description', array (
				'tableName' => strtoupper ( 'TAXI_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		) );
		
		// to get advt_category_type list from data attributes
		$advt_category_type_list = $this->Data_Attributes_Model->getColumnByKeyValueArray ( 'id,description', array (
				'tableName' => strtoupper ( 'ADVT_CATEGORY' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		) );
		
		if ($entity_details) {
			
			$response_data = array (
					'api_base_url' => base_url (),
					'logo_image_url' => image_url ( 'app/' . $entity_details->logo ),
					'aboutpage_description' => $entity_details->description,
					'contact_email' => $entity_details->email,
					"currency_symbol" => $currency_details [0]->currencySymbol,
					'android_passenger_app_ver' => ANDROID_PASSENGER_APP_VER,
					'iphone_passenger_app_ver' => IPHONE_PASSENGER_APP_VER,
					'referral_amount' => $entity_details->referralDiscountAmount,
					'trip_type_list' => $trip_type_list,
					'payment_mode_list' => $payment_mode_list,
					'taxi_category_type_list' => $taxi_category_type_list,
					'advt_category_type' => $advt_category_type_list 
			);
			
			$message = array (
					"message" => $msg_data ['success'],
					"detail" => $response_data,
					"status" => 1 
			);
		} else {
			$message = array (
					"message" => $msg_data ['failed'],
					"status" => - 1 
			);
		}
		
		echo json_encode ( $message );
	}
	
	public function resetPassword_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$message = '';
		$password = NULL;
		$update_password = NULL;
		
		$passenger_exist = NULL;
		$merchant_exist = NULL;
		
		$passenger_email = NULL;
		$merchant_email = NULL;
		
		$passenger_mobile = NULL;
		$merchant_mobile = NULL;
	
		$email = NULL;
		$mobile = NULL;
		// Posted json data
		$post_data = $this->get_post_data ();
	
		if (array_key_exists ( 'email', $post_data )) {
			$email = $post_data->email;
			$passenger_exist = $this->Passenger_Model->getOneByKeyValueArray ( array (
					'email' => $email
			) );
			if ($passenger_exist)
			{
				$passenger_email=$passenger_exist->email;
			}
			$merchant_exist = $this->Merchant_Details_Model->getOneByKeyValueArray ( array (
					'email' => $email
			) );
			if ($merchant_exist)
			{
				$merchant_email=$merchant_exist->email;
			}
		}
		if (array_key_exists ( 'mobile', $post_data )) {
				$mobile = $post_data->mobile;
				
				$mobile = $post_data->mobile;
				$passenger_exist = $this->Passenger_Model->getOneByKeyValueArray ( array (
						'mobile' => $mobile
				) );
				if ($passenger_exist)
				{
					$passenger_mobile=$passenger_exist->mobile;
				}
				$merchant_exist = $this->Merchant_Details_Model->getOneByKeyValueArray ( array (
						'mobile' => $mobile
				) );
				if ($merchant_exist)
				{
					$merchant_mobile=$merchant_exist->mobile;
				}
			
		}
		if ($passenger_exist || $merchant_exist ) {
				
			$password = random_string ( 'alnum', 8 );
			// encrypted password
			$password_key = hash ( "sha256", $password );
			if ($passenger_mobile || $passenger_email)
			{
				$update_password = $this->Passenger_Model->update ( array (
						'password' => $password_key
				), array (
						'id' => $passenger_exist->id
				) );
			}
			if($merchant_mobile || $merchant_email)
			{
				$update_password = $this->Merchant_Details_Model->update ( array (
						'password' => $password_key
				), array (
						'id' => $merchant_exist->id
				) );
				
				$user_email_exist = $this->User_Login_Details_Model->getOneByKeyValueArray ( array (
						'loginEmail' => $email
				) );
				
				$user_mobile_exist = $this->User_Login_Details_Model->getOneByKeyValueArray ( array (
						'mobile' => $mobile
				) );
				if ($user_email_exist || $user_mobile_exist)
				{
					$update_user_password = $this->User_Login_Details_Model->update ( array (
							'password' => $password_key
					), array (
							'roleType'=>Role_Type_Enum::MERCHANT,
							'roleUserId' => $merchant_exist->id
					) );
				}
			}
			if (SMS && $update_password) {
				$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
						'title' => 'forgot_password_sms'
				) );
	
				$message_data = $message_details->content;
				// $message = str_replace("##USERNAME##",$name,$message);
				$message_data = str_replace ( "##PASSWORD##", $password, $message_data );
				// print_r($message);exit;
				if ($merchant_mobile != "") {
						
					$this->Common_Api_Webservice_Model->sendSMS ( $merchant_mobile, $message_data );
				}
				else if ($passenger_mobile != "") {
				
					$this->Common_Api_Webservice_Model->sendSMS ( $passenger_mobile, $message_data );
				}
			}
			/*
				$from = SMTP_EMAIL_ID;
				$to = $email;
				$subject = 'Take A Right Forgot Password';
				$data = array (
				'password' => $password
				);
				$message_data = $this->load->view ( 'emailtemplate/forgotpassword', $data, TRUE );
					
				if (SMTP && $update_password) {
	
				if ($to) {
				$this->Common_Api_Webservice_Model->sendEmail ( $to, $subject, $message_data );
				}
				} else {
	
				// To send HTML mail, the Content-type header must be set
				$headers = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				// Additional headers
				$headers .= 'From: Zuver<' . $from . '>' . "\r\n";
				$headers .= 'To: <' . $to . '>' . "\r\n";
				if (! filter_var ( $to, FILTER_VALIDATE_EMAIL ) === false) {
				mail ( $to, $subject, $message_data, $headers );
				}
				}
				*/
			$message = array (
					"message" => $msg_data ['reset_password'],
					'status' => 1
			);
		} else {
			$message = array (
					"message" => $msg_data ['invalid_user'],
					'status' => - 1
			);
		}
	
		echo json_encode ( $message );
	}
}