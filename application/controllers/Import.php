<?php
class Import extends My_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->model ( 'Common_Api_Webservice_Model' );
		$this->load->model ( 'Driver_Model' );
		$this->load->model ( 'Passenger_Model' );
		$this->load->model ( 'Driver_Personal_Details_Model' );
		$this->load->model ( 'Driver_Dispatch_Details_Model' );
		$this->load->model ( 'Taxi_Device_Details_Model' );
		$this->load->model ( 'Taxi_Owner_Details_Model' );
		$this->load->model ( 'Taxi_Details_Model' );
		$this->load->model ( 'Driver_Taxi_Mapping_Model' );
		$this->load->model ( 'Sms_Template_Model' );
		$this->load->model ( 'Taxi_Device_Install_History_Model' );
	}
	public function index() {
		// echo date_default_timezone_set('Asia/Rangoon');
	}
	public function driverWalletImport() {
		$file_path = image_url ( 'driver_wallet_import.csv' ); // debug_exit($file_path);
		$file_open = fopen ( $file_path, "r" );
		$count_no_lines = count ( $file_open );
		while ( ($data = fgetcsv ( $file_open, 1000, "," )) !== FALSE ) {
			$driver_id = $data [0];
			
			if ($data [8] <= 1000) {
				$wallet_balance = 2000;
			} else {
				$wallet_balance = $data [8];
			}
			$update_driver_data = array (
					'driverWallet' => $wallet_balance 
			);
			$update_driver_code = $this->Driver_Model->update ( $update_driver_data, array (
					'id' => $driver_id 
			) );
			
			$update_driver_dispatch_data = array (
					'walletBalance' => $wallet_balance 
			);
			
			$driver_dispatch_id = $this->Driver_Dispatch_Details_Model->update ( $update_driver_dispatch_data, array (
					'driverId' => $driver_id 
			) );
		}
		debug ( 'import completed' );
	}
	public function driverBankInfoImport() {
		$driver_mobile_exists = NULL;
		$driver_ncr_exists = NULL;
		$file_path = image_url ( 'driver_bank_info_import.csv' );  debug_exit($file_path);
		$file_open = fopen ( $file_path, "r" );
		$count_no_lines = count ( $file_open );
		while ( ($data = fgetcsv ( $file_open, 1000, "," )) !== FALSE ) {
			$account_no = trim ( str_replace ( ' ', '', $data [4] ) );
			
			if ($account_no) {
				$driver_mobile_exists = $this->Driver_Model->getOneByKeyValueArray ( array (
						'mobile' => $data [2] 
				) );
				if ($driver_mobile_exists) {
					$driver_ncr_exists = $this->Driver_Personal_Details_Model->getOneByKeyValueArray ( array (
							'nationRegistrationNo' => $data [6],
							'driverId'=> $driver_mobile_exists->id
					) );
					if ($driver_ncr_exists) {
						//debug_exit($account_no);
						$update_driver_data = array (
								'bankAccountNo' => $account_no,
								'bankBranch' => $data [1],
								'bankName' => $data [0] 
						);
						$update_driver_code = $this->Driver_Personal_Details_Model->update ( $update_driver_data, array (
								'id' => $driver_mobile_exists->id 
						) );
					}
				}
			}
		}
		
		debug ( 'import completed' );
	}
	public function DriverImport() {
		$file_path = image_url ( 'driver_import3.csv' ); // debug_exit($file_path);
		$file_open = fopen ( $file_path, "r" );
		$count_no_lines = count ( $file_open );
		while ( ($data = fgetcsv ( $file_open, 1000, "," )) !== FALSE ) {
			
			$driver_id = 0;
			$taxi_owner_id = 0;
			$taxi_id = 0;
			$taxi_device_id = 0;
			$taxi_owner_exists = NULL;
			$taxi_exists = NULL;
			$driver_exists = NULL;
			$taxi_device_exists = NULL;
			
			//check taxi owner exists
			$taxi_owner_exists = $this->Taxi_Owner_Details_Model->getOneByKeyValueArray ( array (
					'mobile' => $data [10] 
			) );
			if ($taxi_owner_exists) {
				$taxi_owner_id = $taxi_owner_exists->id;
			} else {
				$insert_owner_data = array (
						'firstName' => $data [9],
						'mobile' => $data [10],
						'zoneId' => 1,
						'entityId' => 1,
						'isDriver' => 1 
				);
				$taxi_owner_id = $this->Taxi_Owner_Details_Model->insert ( $insert_owner_data );
				// update the taxi device Code
				$taxi_owner_code_id = ($taxi_owner_id <= 999999) ? str_pad ( ( string ) $taxi_owner_id, 6, '0', STR_PAD_LEFT ) : $taxi_owner_id;
				
				$taxi_owner_code = 'TAXIOWN' . $taxi_owner_code_id;
				$update_device_code = $this->Taxi_Owner_Details_Model->update ( array (
						'taxiOwnerCode' => $taxi_owner_code 
				), array (
						'id' => $taxi_owner_id 
				) );
			}
			if ($taxi_owner_id) {
				$taxi_exists = $this->Taxi_Details_Model->getOneByKeyValueArray ( array (
						'registrationNo' => $data [8] 
				) );
				
				if ($taxi_exists) {
					$taxi_id = $taxi_exists->id;
				} else {
					$insert_taxi_data = array (
							
							'registrationNo' => $data [8],
							'taxiOwnerId' => $taxi_owner_id,
							'taxiCategoryType' => Taxi_Type_Enum::STANDARD,
							'zoneId' => 1,
							'serviceStartDate' => getCurrentDateTime (),
							'entityId' => 1,
							'isAllocated' => Status_Type_Enum::ACTIVE
					);
					
					$taxi_id = $this->Taxi_Details_Model->insert ( $insert_taxi_data );
				}
			}
			if ($taxi_id) {
				
				$taxi_device_exists = $this->Taxi_Device_Details_Model->getOneByKeyValueArray ( array (
						'mobile' => $data [6] 
				) );
				
				if ($taxi_device_exists) {
					$taxi_device_id = $taxi_device_exists->id;
				} else {
					$insert_taxi_device_data = array (
							'imeiNo'=>$data [7],
							'mobile' => $data [6],
							'deviceStatus' => Device_Status_Enum::OPEN,
							'isWarranty' => Status_Type_Enum::ACTIVE,
							'isAllocated' => Status_Type_Enum::ACTIVE,
							'purchasedDate' => getCurrentDate () 
					);
					
					$taxi_device_id = $this->Taxi_Device_Details_Model->insert ( $insert_taxi_device_data );
					$taxi_device_code_id = ($taxi_device_id <= 999999) ? str_pad ( ( string ) $taxi_device_id, 6, '0', STR_PAD_LEFT ) : $taxi_device_id;
					
					$taxi_device_code = 'DEV' . $taxi_device_code_id;
					$update_device_code = $this->Taxi_Device_Details_Model->update ( array (
							'deviceCode' => $taxi_device_code 
					), array (
							'id' => $taxi_device_id 
					) );
				}
				
				//to maintain new device installation history to taxi
				$insert_device_history=array('taxiDeviceId'=>$taxi_device_id,'taxiId'=>$taxi_id,'installedDatetime'=>getCurrentDateTime());
				$device_history_id=$this->Taxi_Device_Install_History_Model->insert($insert_device_history);
			
				$driver_exists = $this->Driver_Model->getOneByKeyValueArray ( array (
						'mobile' => $data [3]
						
				) );
				
				if ($driver_exists) {
					$driver_id = $driver_exists->id;
					debug($data[5]);
					if ($data[5]<=1000)
					{
						debug('1000');
						$wallet_balance=2000;
					}
					else
					{
						debug($data[5]);
						$wallet_balance=$data[5];
					}
					debug_exit($wallet_balance);
					$update_driver_data = array (
							'driverWallet' => $wallet_balance
					);
					$update_driver_code = $this->Driver_Model->update ( $update_driver_data, array (
							'id' => $driver_id
					) );
						
					$update_driver_dispatch_data = array (
							'walletBalance' => $wallet_balance
					);
						
					$driver_dispatch_id = $this->Driver_Dispatch_Details_Model->update ( $update_driver_dispatch_data, array (
							'driverId' => $driver_id
					) );
				} else {
					if ($data[5]<=1500)
					{
						$wallet_balance=1500;
					}
					else 
					{
						$wallet_balance=$data[5];
					}
					$insert_driver_data = array (
							
							'mobile' => $data [3],
							'firstName' => $data [0],
							'lastName' => $data [1],
							'isVerified'=>1,
							'driverWallet'=>$wallet_balance,
							'deviceType'=>Device_Type_Enum::ANDROID,
							'signupFrom'=>Signup_Type_Enum::BACKEND,
							'registerType'=>Register_Type_Enum::MANUAL,
							'licenseType'=>License_Type_Enum::TRANSPORT,
							'experience'=>Experience_Enum::ONE_TO_THREE,
							'zoneId'=>1,
							'entityId'=>1
					);
					$driver_id=$this->Driver_Model->insert($insert_driver_data);
					// update the Unique Driver Code
					$driver_code_id = ($driver_id <= 999999) ? str_pad ( ( string ) $driver_id, 6, '0', STR_PAD_LEFT ) : $driver_id;
					
					$driver_code = 'DRV' . $driver_code_id;
					$password = 'hellocabs';
					$password = ($password) ? (hash ( "sha256", $password )) : '';
					
					$update_driver_code = $this->Driver_Model->update ( array (
							'driverCode' => $driver_code,
							'password'=>$password
					), array (
							'id' => $driver_id 
					) );
					
					$data ['driverId'] = $driver_id;
					$driver_personal_details_id = NULL;
					
					if ($driver_id > 0) {
						
						$insert_driver_personal_data = array (
								
								'gender' => $data [2],
								'nationRegistrationNo'=>$data [4],
								'driverId' => $driver_id 
						)
						;
						$driver_personal_details_id = $this->Driver_Personal_Details_Model->insert ( $insert_driver_personal_data );
						
						$insert_driver_dispatch_data = array (
						
								'walletBalance' => $wallet_balance,
								'availabilityStatus' => Taxi_Available_Status_Enum::BUSY,
								'driverId' => $driver_id
						)
						;
						$driver_dispatch_id = $this->Driver_Dispatch_Details_Model->insert ( $insert_driver_dispatch_data );
						
						$insert_driver_mapping_data = array (
								'taxiOwnerId' => $taxi_owner_id,
								'taxiDeviceId' => $taxi_device_id,
								'taxiId' => $taxi_id,
								'driverId' => $driver_id 
						)
						;
						$driver_taxi_mapping_id = $this->Driver_Taxi_Mapping_Model->insert ( $insert_driver_mapping_data );
					}
				}
			}
		}
		debug ( 'import completed' );
	}
	public function PassengerImport() {
		$passenger_info = array ();
		$file_path = image_url ( 'passenger_import.csv' ); // debug_exit($file_path);
		$file_open = fopen ( $file_path, "r" );
		$count_no_lines = count ( $file_open );
		while ( ($data = fgetcsv ( $file_open, 1000, "," )) !== FALSE ) {
			// debug($data);
			$passenger_code = '';
			
			if (strlen ( $data [2] ) >= 8 && strlen ( $data [2] ) <= 10) {
				
				$passenger_info = explode ( '.', $data [1] );
				
				$passenger_name = trim ( $passenger_info [1] );
				$passenger_gender = ($passenger_info [0] == 'Mr') ? 'Male' : 'Female';
				$insert_data = array (
						'firstName' => $passenger_name,
						'mobile' => $data [2],
						'gender' => $passenger_gender,
						'deviceType' => Device_Type_Enum::NO_DEVICE,
						'signupFrom' => Signup_Type_Enum::BACKEND,
						'registerType' => Register_Type_Enum::MANUAL,
						'isEmailVerified' => Status_Type_Enum::INACTIVE,
						'activationStatus' => Status_Type_Enum::ACTIVE,
						'status' => Status_Type_Enum::ACTIVE ,
						'zoneId'=>1,
						'entityId'=>1
				);
				
				$passenger_id = $this->Passenger_Model->insert ( $insert_data );
				// update the Unique Passenger Code
				$passenger_code_id = ($passenger_id <= 999999) ? str_pad ( ( string ) $passenger_id, 6, '0', STR_PAD_LEFT ) : $passenger_id;
				
				$passenger_code = 'PAS' . $passenger_code_id;
				$password = $passenger_code;
				$password = ($password) ? (hash ( "sha256", $password )) : '';
				
				$update_passenger_code = $this->Passenger_Model->update ( array (
						'passengerCode' => $passenger_code,
						'password' => '' 
				), array (
						'id' => $passenger_id 
				) );
				/*
				 * if ($passenger_id) {
				 *
				 * $message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
				 * 'title' => 'account_create_sms'));
				 *
				 * $message_data = $message_details->content;
				 *
				 * $message_data = str_replace ( "##USERNAME##", $data ['mobile'], $message_data );
				 * $message_data = str_replace ( "##PASSWORD##", $password, $message_data );
				 * $message_data = str_replace ( "##APPLINK##", 'https://play.google.com/store/apps/details?id=com.hellocabs', $message_data );
				 *
				 *
				 * // print_r($message);exit;
				 * if ($data [2] != "") {
				 *
				 * $this->Common_Api_Webservice_Model->sendSMS ( $data [2], $message_data );
				 * }
				 * }
				 */
			}
		}
		debug ( 'import completed' );
	}
}