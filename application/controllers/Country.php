<?php
class Country extends My_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->helper ( 'form' );
		$this->load->model ( 'Taxi_Query_Model' );
		$this->load->model ( 'Country_Model' );
	}
	public function index() {
	}
	public function add() {
	if ($this->User_Access_Model->createModule ($this->session->userdata('role_type'),Module_Name_Enum::COUNTRY) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$this->addJs ( 'app/country.js' );
		
		$data = array (
				'mode' => EDIT_MODE 
		);
		$data ['activetab'] = "country";
		$data ['active_menu'] = 'country';
		
		$data ['country_model'] = $this->Country_Model->getBlankModel ();
		
		$this->render ( 'country/add_edit_country', $data );
	}
	public function getDetailsById($country_id, $view_mode) {
	if ($view_mode==EDIT_MODE)
	{
	if ($this->User_Access_Model->writeModule ($this->session->userdata('role_type'),Module_Name_Enum::COUNTRY) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
	}
	else
	{
		if ($this->User_Access_Model->readModule ($this->session->userdata('role_type'),Module_Name_Enum::COUNTRY) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
	}
		$this->addJs ( 'app/country.js' );
		
		$data = array (
				'mode' => $view_mode 
		);
		$data ['activetab'] = "country";
		$data ['active_menu'] = 'country';
		if ($this->input->post ( 'id' )) {
			$country_id = $this->input->post ( 'id' );
		}
		if ($country_id > 0) {
			if ($view_mode==EDIT_MODE)
			{
				$locked=$this->Country_Model->isLocked($country_id);
				if ($locked)
				{
					$locked_user=$this->Country_Model->getLockedUser($country_id);
					$locked_date=$this->Country_Model->getLockedDate($country_id);
					$locked_date=date('jS, F Y G:ia',strtotime($locked_date));
					$message=$msg_data ['locked'];
					$message=str_replace('##USERNAME##', $locked_user, $message);
					$message.=$locked_date;
						
					$data['mode']=VIEW_MODE;
				}
				else
				{
					$is_locked=$this->Country_Model->lock($country_id);
				}
			}
			$data ['country_model'] = $this->Country_Model->getById ( $country_id );
		} else {
			$data ['country_model'] = $this->Country_Model->getBlankModel ();
		}
		if ($this->input->post ( 'id' )) {
			
			$html = $this->load->view ( 'country/add_edit_country', $data );
			echo json_encode ( array (
					'html' => $html 
			) );
		} else {
			$this->render ( 'country/add_edit_country', $data );
		}
	}
	
	public function getCountrylist()
	{
		if ($this->User_Access_Model->showSettings() === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		
		$this->addJs ( 'app/country.js' );
		
		
		
		$data ['activetab'] = "country";
		$data ['active_menu'] = 'country';
		
		$this->render ( 'country/manage_country', $data );
	}
	public function ajax_list()
	{
		$sql_query=$this->Country_Model->getCountryListQuery();
		$country_list = $this->Country_Model->getDataTable($sql_query);
		$data = array();
		$no = $_POST['start'];
		foreach ($country_list as $list) {
			$no++;
			$row = array();
			$row[] = '
			<button class="btn btn-sm btn-success" id="viewcountry-'.$list->countryId.'" title="View" ><i class="fa fa-eye" aria-hidden="true"></i></button>
            <button class="btn btn-sm btn-blue" id="editcountry-'.$list->countryId.'" title="Edit"><i class="fa fa-pencil-square-o"></i></button>';
            //<button class="btn btn-sm btn-danger" id="deletecountry-'.$list->countryId.'" title="Delete"><i class="fa fa-trash-o"></i></button>';
			$row[] = ucfirst($list->name);
			$row[] = $list->isoCode;
			$row[] = $list->telephoneCode;
			$row[] = $list->currencyCode;
			$row[] = $list->currencySymbol;
			$row[] = $list->timeZone;
			$status_class=($list->status)?'class="btn btn-success"':'class="btn btn-danger"';
			$status_name=($list->status)?'Active':'Inactive';
			$row[] = '<button id="status-'.$list->countryId.'" status="'.$list->status.'" '.$status_class.'><span>'.$status_name.'</span></button>';
	
			//add html for action
			
			
			$data[] = $row;
		}
	
		$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->Country_Model->getDataTableAllCount($sql_query),
				"recordsFiltered" => $this->Country_Model->getDataTableFilterCount($sql_query),
				"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	
	public function saveCountry() {
		$this->addJs ( 'app/country.js' );
		$data = array ();
		$data = $this->input->post ();
		//print_r($data);exit;
		$country_id = $data ['id'];
		$data['activetab']="admin";
		$data['active_menu'] = 'admin';
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['failed'];
		if ($country_id > 0) {
			$this->Country_Model->update ( $data, array (
					'id' => $country_id
			) );
			$msg = $msg_data ['updated'];
	
		} else {
			
			$country_id = $this->Country_Model->insert ( $data );
			$msg = $msg_data ['success'];
		}
		$response = array (
				'msg' => $msg,
				'country_id' => $country_id
		);
		echo json_encode ( $response );
		// $this->render("passenger/add_edit_passenger", $data);
	
	}
}