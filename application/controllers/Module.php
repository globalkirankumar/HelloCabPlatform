<?php
class Module extends MY_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->helper ( 'form' );
		$this->load->model ( 'Module_Model' );
		$this->load->model ( 'Module_Access_Permission_Model' );
		$this->load->model ( 'Common_Api_Webservice_Model' );
	}
	
	public function index() {
		if ($this->User_Access_Model->writeModule ($this->session->userdata('role_type'),Module_Name_Enum::MODULE) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		
		$this->addJs ( 'app/module.js' );
		
		$data = array (
				'mode' => EDIT_MODE 
		);
		$data ['role_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'ROLE_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		$data ['activetab'] = "module";
		$data ['active_menu'] = 'module';
		//$data ['module_access_model'] = $this->Module_Access_Permission_Model->getBlankModel ();
		$this->render ( 'module/module_layout', $data );
	}
	/**
	 */
	public function saveModulePermission() {
		$this->addJs ( 'app/module.js' );
		$data = array ();
		$data = $this->input->post ();
		//debug_exit($data);
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['failed'];
		
		$role_type=$data['roleType'];
		unset($data['roleType']);
		if ($data['module'])
		{
		foreach ($data['module'] as $list)
		{
			$module_id = $list['moduleAccessId'];
			unset($list['moduleAccessId']);
			
			if (!array_key_exists('isAccess', $list))
			{
				$list['isAccess']=0;
			}
			else 
			{
				$list['isAccess']=1;
			}
			
			if (!array_key_exists('isCreate', $list))
			{
				$list['isCreate']=0;
			}
			else
			{
				$list['isCreate']=1;
			}
			
			if (!array_key_exists('isWrite', $list))
			{
				$list['isWrite']=0;
			}
			else 
			{
				$list['isWrite']=1;
			}
			
			if (!array_key_exists('isRead', $list))
			{
				$list['isRead']=0;
			}
			else {
				$list['isRead']=1;
			}
			
			if (!array_key_exists('isDelete', $list))
			{
				$list['isDelete']=0;
			}
			else
			{
				$list['isDelete']=1;
			}
			if (!array_key_exists('isFullAccess', $list))
			{
				$list['isFullAccess']=0;
			}
			else
			{
				$list['isFullAccess']=1;
			}
			$update_data=$list;
			//debug_exit($update_data);
			if ($module_id > 0) {
				
				$update_module_access=$this->Module_Access_Permission_Model->update ( $update_data, array (
						'id' => $module_id,'roleType'=>$role_type 
				) );
				
			} 
		}
		$msg = $msg_data ['updated'];
		}
		$response = array (
				'msg' => $msg,
				'role_type' => $role_type 
		);
		echo json_encode ( $response );
	}
	public function getDetailsById($view_mode) {
		if ($this->User_Access_Model->writeModule ($this->session->userdata('role_type'),Module_Name_Enum::MODULE) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		
		$this->addJs ( 'app/module.js' );
		
		$data = array (
				'mode' => $view_mode 
		);
		
		$data ['activetab'] = "module";
		$data ['active_menu'] = 'module';
		
			$role_id = $this->input->post ( 'id' );
		
		if ($role_id > 0) {
			
			$data ['module_access_model'] = $this->Module_Access_Permission_Model->getModuleAccessList($role_id);
		} 
		
		//debug_exit($data);
			$html = $this->load->view ( 'module/add_edit_module', $data,TRUE );
			echo json_encode ( array (
					'html' => $html 
			) );
		
	}
	
}