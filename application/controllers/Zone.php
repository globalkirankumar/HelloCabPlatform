<?php
class Zone extends My_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->helper ( 'form' );
		$this->load->model ( 'Taxi_Query_Model' );
		$this->load->model ( 'Zone_Details_Model' );
		$this->load->model ( 'Taxi_Device_Details_Model' );
		$this->load->model ( 'Taxi_Device_Install_History_Model' );
		$this->load->model ( 'Taxi_Details_Model' );
		$this->load->model ( 'Driver_Taxi_Mapping_Model' );
		$this->load->model ( 'Zone_Details_Model' );
		$this->load->model ( 'Country_Model' );
		$this->load->model ( 'Driver_Personal_Details_Model' );
		$this->load->model ( 'Data_Attributes_Model' );
		$this->load->model ( 'Sms_Template_Model' );
		$this->load->model ( 'Common_Api_Webservice_Model' );
                $this->load->model ( 'Entity_Config_Model' );
                $this->load->model ( 'Entity_Model' );
		
	}
        
 public function add() {
 if ($this->User_Access_Model->createModule ($this->session->userdata('role_type'),Module_Name_Enum::ZONE) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$this->addJs ( 'app/zone.js' );
		
		$data = array (
				'mode' => EDIT_MODE 
		);
		$data ['activetab'] = "entity";
		$data ['active_menu'] = 'entity';
		$data ['zone_list'] = $this->Zone_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE,
				'isDeleted'=> Status_Type_Enum::INACTIVE,
				'zoneId'=>Status_Type_Enum::INACTIVE,
		), 'name' );
		$data ['country_list'] = $this->Country_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE
		), 'name' );
		$data ['dispatch_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'DISPATCH_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		$data ['zone_model'] = $this->Zone_Details_Model->getBlankModel ();
		$this->render ( 'zone/add_edit_zone', $data );
	}
	
	public function saveZone() {
		$this->addJs ( 'app/zone.js' );
		$data = array ();
		$data = $this->input->post ();
		//print_r($data);exit;
		$zone_id = $data ['id'];
		$data['activetab']="zone";
		$data['active_menu'] = 'zone';
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['failed'];
		if ($zone_id > 0) {
			$this->Zone_Details_Model->update ( $data, array ( 'id' => $zone_id ) );
                        
			$msg = $msg_data ['updated'];
	
		} else {
			
			$zone_id = $this->Zone_Details_Model->insert ( $data );
			$msg = $msg_data ['success'];
				
		}
		$response = array (
				'msg' => $msg,
				'zone_id' => $zone_id
		);
		echo json_encode ( $response );
		// $this->render("passenger/add_edit_passenger", $data);
	
	}
	
	public function getDetailsById($zone_id, $view_mode) {
	if ($view_mode==EDIT_MODE)
	{
	if ($this->User_Access_Model->writeModule ($this->session->userdata('role_type'),Module_Name_Enum::ZONE) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
	}
	else
	{
		if ($this->User_Access_Model->readModule ($this->session->userdata('role_type'),Module_Name_Enum::ZONE) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
	}
		$this->addJs ( 'app/zone.js' );
		// get API messages from message.php
		$msg_data = $this->config->item ( 'msg' );
		$message='';
		$data = array (
				'mode' => $view_mode 
		);
		
		$data ['activetab'] = "entity";
		$data ['active_menu'] = 'entity';
		/*$data ['zone_list'] = $this->Zone_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE,
				'isDeleted'=> Status_Type_Enum::INACTIVE,
				'zoneId'=>Status_Type_Enum::INACTIVE,
				'id!='=>$zone_id
		), 'name' );*/
                $data ['zone_list'] = $this->Zone_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE,
				'isDeleted'=> Status_Type_Enum::INACTIVE,
				'zoneId'=>Status_Type_Enum::INACTIVE,
				'id!='=>$zone_id
                            ), 'name' );
		$data ['country_list'] = $this->Country_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE 
				
		), 'name' );
		$data ['dispatch_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'DISPATCH_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		/*$data ['state_list'] = $this->State_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE 
		), 'name' );*/
		
		if ($this->input->post ( 'id' )) {
			$zone_id = $this->input->post ( 'id' );
		}
		
		if ($zone_id > 0) {
			
			if ($view_mode==EDIT_MODE)
			{
				$locked=$this->Zone_Details_Model->isLocked($zone_id);
				if ($locked)
				{
					$locked_user=$this->Zone_Details_Model->getLockedUser($zone_id);
					$locked_date=$this->Zone_Details_Model->getLockedDate($zone_id);
					$locked_date=date('jS, F Y G:ia',strtotime($locked_date));
					$message=$msg_data ['locked'];
					$message=str_replace('##USERNAME##', $locked_user, $message);
					$message.=$locked_date;
			
					$data['mode']=VIEW_MODE;
				}
				else
				{
					$is_locked=$this->Zone_Details_Model->lock($zone_id);
				}
			}
			
			$data ['zone_model'] = $this->Zone_Details_Model->getById ( $zone_id );
			$polygan_points =   @$data ['zone_model']->polygonPoints;
			$polygan_array = explode("|",$polygan_points);
			$this->load->library('googlemap');
			$config['center'] = '21.9162, 95.9560';
			$config['zoom'] = 'auto';
			$this->googlemap->initialize($config);
			$polygon = array();
			$polygon['points'] = $polygan_array;
			$polygon['strokeColor'] = '#FF0000';
			$polygon['fillColor'] = '#F57F17';
			$this->googlemap->add_polygon($polygon);
			$data['map'] = $this->googlemap->create_map();
                        
		} else {
			$data ['zone_model'] = $this->Zone_Details_Model->getBlankModel ();
		}
		if ($this->input->post ( 'id' )) {
			
			$html = $this->load->view ( 'entity/add_edit_zone', $data );
			echo json_encode ( array (
					'html' => $html 
			) );
		} else {
			$this->render ( 'zone/add_edit_zone', $data );
		}
	}
	
	
	public function getZoneList()
	{
		if ($this->User_Access_Model->showZone() === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return false;
		}
                $data = array ();
		$this->addJs('app/zone.js');
		$data ['activetab'] = "zone";
		$data ['active_menu'] = 'zone';
                $data ['taxi_list'] = $this->Taxi_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE,'isAllocated'=>Status_Type_Enum::INACTIVE,'isDeleted'=>Status_Type_Enum::INACTIVE
		), 'id' );
		$data ['taxi_device_list'] = $this->Taxi_Device_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE,'isAllocated'=>Status_Type_Enum::INACTIVE,'isDeleted'=>Status_Type_Enum::INACTIVE,'deviceStatus'=> Device_Status_Enum::OPEN
		), 'id' );
		$this->render('zone/manage_zone', $data);
	}
	public function ajax_list()
	{
		//echo"ajaxlist";exit;
		$sql_query = $this->Zone_Details_Model->getZoneListQuery();
		$zone_list = $this->Zone_Details_Model->getDataTable($sql_query);
              $data = array();
		$no = $_POST['start'];
		foreach ($zone_list as $list) {
			$no++;
			$row = array();
                         $row[] = '
			
				  <button class="btn btn-sm btn-success" id="viewzone-'.$list->zoneId.'" title="View" >'
                                . '<i class="fa fa-eye" aria-hidden="true"></i></button> '
                                . '<button class="btn btn-sm btn-blue" id="editzone-'.$list->zoneId.'" title="Edit">'
                                . '<i class="fa fa-pencil-square-o"></i>';
                               // </button><button class="btn btn-sm btn-danger" id="deletezone-'.$list->zoneId.'" title="Edit"><i class="fa fa-trash-o"></i></button>';
                        
			$row[] = $list->name;
			$row[] = $list->polygonPoints;
			$row[] = ($list->parentZoneName)?$list->parentZoneName:'NULL';
			$row[] = $list->countryName;
			$row[] = $list->dispatchTypeName;
                        $status_class=($list->status)?'class="label label-sm label-success"':'class="label label-sm label-danger"';
                        $status_name=($list->status)?'Active':'Inactive';
                        $row[] = '<span id="status-'.$list->zoneId.'" status="'.$list->status.'" '.$status_class.'>'.$status_name.'</span>';
                        
                        $data[] = $row;
		}
	
		$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->Zone_Details_Model->getDataTableAllCount($sql_query),
				"recordsFiltered" => $this->Zone_Details_Model->getDataTableFilterCount($sql_query),
				"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}       
        
        
        public function deleteZone() {
        	if ($this->User_Access_Model->deleteModule ($this->session->userdata('role_type'),Module_Name_Enum::ZONE) === FALSE) {
        		// redirect 404
        		show_error ( "You do not have permission to delete", '404' );
        		return;
        	}
		$data = array ();
	
		$this->addJs ( 'app/zone.js' );
		$zone_updated=-1;
		$data = $this->input->post ();
		$user_id = $data ['id'];
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['delete_failed'];
		if ($user_id) {
			$zone_updated = $this->Zone_Details_Model->update ( array (
					'isDeleted' => Status_Type_Enum::ACTIVE
			), array (
					'id' => $user_id
			) );
			$msg = $msg_data ['delete_success'];
		}
		$response = array (
				'msg' => $msg,
				'status' => $zone_updated
		);
		echo json_encode ( $response );
	}
        public function changeZoneStatus() {
        	
        	if ($this->User_Access_Model->fullAccessModule ($this->session->userdata('role_type'),Module_Name_Enum::ZONE) === FALSE) {
        		// redirect 404
        		show_error ( "You do not have permission to change status", '404' );
        		return;
        	}
		$data = array ();
	
		$this->addJs ( 'app/zone.js' );
		$user_updated=-1;
		$data = $this->input->post ();
		$user_id = $data ['id'];
		$user_status = $data ['status'];
	
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['status_change_failed'];
		if ($user_id) {
			$user_updated = $this->Zone_Details_Model->update ( array (
					'status' => $user_status
			), array (
					'id' => $user_id
			) );
			$msg = $msg_data ['status_change_success'];
		}
		$response = array (
				'msg' => $msg,
				'status' => $user_updated
		);
		echo json_encode ( $response );
	}
    /*** code to insert polygan values in to Zone Table starts***/    
    public function insertpoylgans(){
        $polygan_values =  file_get_contents(base_url()."zones_values.txt");
        $first_level_points =explode('::',$polygan_values);
   
        $i=1;
        foreach($first_level_points as $first_points){
            $name               = "Zone-".$i;
            $polygonPoints      = $first_points;
            $zoneId             = 0;
            $country_id         = 1;
            $dispatchType       = 1;
            $status             = 1;
            $isDeleted          = 0;
            $createdBy          = 1;
            $updatedBy          = 1;
            $data[]    = array('name'=>$name,'polygonPoints'=>trim($polygonPoints),'zoneId'=>$zoneId,'countryId'=>$country_id,'dispatchType'=>$dispatchType,
                               'status'=>$status,'isDeleted'=>$isDeleted,'createdBy'=>$createdBy,'updatedBy'=>$updatedBy);
            $i++;
        }
        $zone_id = $this->Zone_Details_Model->batchInsert($data,false);
        if($zone_id){
            echo "Inserted Completed";
        }
    }    
    /*** code to insert polygan values in to Zone Table ends***/ 
    
   /**** Code to Find Zone Based on Lat and Long ****/
    public function getZoneByLatLong($lat=NULL,$long=NULL){
    	if ($this->input->post('latitude'))
    	{
        $lat        = $this->input->post('latitude');
    	}
    	if($this->input->post('longitude'))
    	{
        $long       = $this->input->post('longitude');
    	}
        $polyX      =  array();//horizontal coordinates of corners
        $polyY      =  array();//vertical coordinates of corners

        $zone_list  = $this->Zone_Details_Model->getZoneListQueryForTripCreation();
       
        if($zone_list){
                foreach($zone_list as $list){
                    $polygan_points =  $list->polygonPoints;
                    $zone_name      =  $list->name;
                    $zone_id        =  $list->zoneId;
                    $this->process_poylgan_data($polygan_points,$lat,$long,$zone_name,$zone_id);
                }
        }
    }// end of function
    
    /*** Code to Get SubZone POlygan ***/
    public function getSubZoneByLatLong($lat=NULL,$long=NULL){
    	if ($this->input->post('latitude'))
    	{
    		$lat        = $this->input->post('latitude');
    	}
    	if($this->input->post('longitude'))
    	{
    		$long       = $this->input->post('longitude');
    	}
        $polyX      =  array();//horizontal coordinates of corners
        $polyY      =  array();//vertical coordinates of corners

        $zone_list  = $this->Zone_Details_Model->getSubZoneListQueryForTripCreation();
       
        if($zone_list){
                foreach($zone_list as $list){
                    $polygan_points =  $list->polygonPoints;
                    $zone_name      =  $list->name;
                    $zone_id        =  $list->zoneId;
                   $res[] =  $this->subzone_process_poylgan_data($polygan_points,$lat,$long,$zone_name,$zone_id);
                }
                return $res;
        }
    }
    
    public function process_poylgan_data($polygan_points,$lat,$long,$zone_name,$zone_id){
         //$zonal      =   "16.78205,96.14767:16.78252,96.13659:16.77558,96.13608:16.7736,96.13544:16.77097,96.14895:16.76875,96.15818:16.78145,96.15878:16.78205,96.14767"; 
        $zonal      =   $polygan_points; 
	$subzone_arr=array();
        $zonalArray =   explode("|",$zonal);
        $polySides  = count($zonalArray); //how many corners the polygon has 
            foreach($zonalArray as $eachZoneLatLongs)
            {
                $latlongArray   =   explode(",",$eachZoneLatLongs);
                $polyX[]    =   isset($latlongArray[0]) ? $latlongArray[0] : '';
                $polyY[]    =   isset($latlongArray[1]) ? $latlongArray[1] : '';
            }
            
            $vertices_x = $polyX;                     // x-coordinates of the vertices of the polygon
            $vertices_y = $polyY;                     // y-coordinates of the vertices of the polygon
            $points_polygon = count($vertices_x);     // number vertices
            #Following Points lie inside this region
            $longitude_x = $lat ;                     // x-coordinate of the point to test
            $latitude_y  = $long;                     // y-coordinate of the point to test 
            
            if ($this->is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y)){
              //echo "Is in polygon: $zone_name".'<br>';
                  $subzone_data = array_filter($this->getSubZoneByLatLong($longitude_x,$latitude_y));
//debug($this->getSubZoneByLatLong($longitude_x,$latitude_y,$zone_id));
		     $subzone_data = array_values($subzone_data);
                  if(count($subzone_data) > 0){
                  	
                      $subzone_arr = explode(',',@$subzone_data[0]);
			
                      echo json_encode(array('ZoneId'=>$zone_id,'Zone_name'=>$zone_name,'subZoneId'=>$subzone_arr[0],'subZone_name'=>$subzone_arr[1]));
                  }else{
                       echo json_encode(array('ZoneId'=>$zone_id,'Zone_name'=>$zone_name,'subZoneId'=>'0','subZone_name'=>'0'));
                  }

            }
            
    }
    
    
    
    public function subzone_process_poylgan_data($polygan_points,$lat,$long,$zone_name,$zone_id){
        $zonal      =   $polygan_points; 
        $zonalArray =   explode("|",$zonal);
        $polySides  = count($zonalArray); //how many corners the polygon has 
            foreach($zonalArray as $eachZoneLatLongs)
            {
                $latlongArray   =   explode(",",$eachZoneLatLongs);
                $polyX[]    =   isset($latlongArray[0]) ? $latlongArray[0] : '';
                $polyY[]    =   isset($latlongArray[1]) ? $latlongArray[1] : '';
            }
            
            $vertices_x = $polyX;                     // x-coordinates of the vertices of the polygon
            $vertices_y = $polyY;                     // y-coordinates of the vertices of the polygon
            $points_polygon = count($vertices_x);     // number vertices
            #Following Points lie inside this region
            $longitude_x = $lat ;                     // x-coordinate of the point to test
            $latitude_y  = $long;                     // y-coordinate of the point to test 
            
            if ($this->is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y)){
                  return $zone_id.','.$zone_name;
            }

    }
   public function is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y){
          $i = $j = $c = 0;
         
          for ($i = 0, $j = $points_polygon-1 ; $i < $points_polygon; $j = $i++) {
          	$vertices_y[$i]=(float)$vertices_y[$i];
          	$vertices_y[$j]=(float)$vertices_y[$j];
          	$vertices_x[$i]=(float)$vertices_x[$i];
          	$vertices_x[$j]=(float)$vertices_x[$j];
            if ( (($vertices_y[$i] > $latitude_y != ($vertices_y[$j] > $latitude_y)) &&
            ($longitude_x < ($vertices_x[$j] - $vertices_x[$i]) * ($latitude_y - $vertices_y[$i]) / ($vertices_y[$j] - $vertices_y[$i]) + $vertices_x[$i]) ) ) 
                $c = !$c;
            }
            return $c;
    }
    
    public function getParentZoneList(){
        try {
            $result      = $this->Zone_Details_Model->getZoneListQueryForTripCreation();
           
            
                if(!$result) {
                  throw new exception("Country not found.");
                }
                $res = array();
                foreach($result as $resultSet) {
                 $res[] = array('zoneId'=>$resultSet->zoneId,'name'=>$resultSet->name);
                }
                $data = $res;
          } catch (Exception $e) {
                $data = array('status'=>'error', 'count'=>0, 'msg'=>$e->getMessage());
          } finally {
             echo  json_encode($data);
        }
    }
        
    public function getSubZoneListByZoneId(){
        try {
            $zone_id = $this->input->post('zone_id');
            $result      = $this->Zone_Details_Model->getSubZoneListByZoneId($zone_id);
           
        if(!$result) {
                  throw new exception("Country not found.");
                }
            $res = array();
                foreach($result as $resultSet) {
                 $res[] = array('zoneId'=>$resultSet->zoneId,'name'=>$resultSet->name);
                }
                $data = $res;
          } catch (Exception $e) {
                $data = array('status'=>'error', 'count'=>0, 'msg'=>$e->getMessage());
          } finally {
             echo  json_encode($data);
        }
        
    }
    
    public function getFullZoneList(){
    	try {
    		$result      = $this->Zone_Details_Model->getFullZoneListDetail();
    		//$result   = $this->db->query($sql)->result();
    
    		if(!$result) {
    			throw new exception("Country not found.");
    		}
    		$res = array();
    		foreach($result as $resultSet) {
    			$res[] = array('zoneId'=>$resultSet->zoneId,'name'=>$resultSet->name);
    		}
    		$data = $res;
    	} catch (Exception $e) {
    		$data = array('status'=>'error', 'count'=>0, 'msg'=>$e->getMessage());
    	} finally {
    		echo  json_encode($data);
    	}
    }
    
}// end of class