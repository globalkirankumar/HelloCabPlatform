<?php
class Notification extends MY_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->helper ( 'form' );
		$this->load->model ( 'Driver_Model' );
		$this->load->model ( 'Passenger_Model' );
		$this->load->model ( 'Notification_Model' );
		$this->load->model ( 'Common_Api_Webservice_Model' );
	}
	public function add() {
		if ($this->User_Access_Model->createModule ( $this->session->userdata ( 'role_type' ), Module_Name_Enum::NOTIFICATION ) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		
		$this->addJs ( 'app/notification.js' );
		
		$data = array (
				'mode' => EDIT_MODE 
		);
		
		$data ['activetab'] = "notification";
		$data ['active_menu'] = 'notification';
		$data ['notification_model'] = $this->Notification_Model->getBlankModel ();
		$this->render ( 'notification/add_edit_notification', $data );
	}
	/**
	 */
	public function saveNotification($is_save) {
		$this->addJs ( 'app/notification.js' );
		$data = array ();
		$data = $this->input->post ();
		// $data ['profileImage']= $this->input->post ('profileImage');
		
		$notification_id = $data ['id'];
		
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['failed'];
		if ($is_save) {
			if ($notification_id > 0) {
				
				$update_notification = $this->Notification_Model->update ( $data, array (
						'id' => $notification_id 
				) );
				$msg = $msg_data ['updated'];
			} else {
				$notification_id = $this->Notification_Model->insert ( $data );
				$msg = $msg_data ['success'];
			}
		} else {
			$msg = $msg_data ['notification_send'];
		}
		$response_data = $data;
		unset ( $response_data ['notificationTo'] );
		unset ( $response_data ['notificationType'] );
		unset ( $response_data ['startDatetime'] );
		unset ( $response_data ['endDatetime'] );
		unset ( $response_data ['id'] );
		$response_data ['createdDatetime'] = getCurrentDateTime ();
		
		if ($data ['notificationTo'] == User_Type_Enum::DRIVER) {
			$this->driverNotification ( $response_data, $data ['notificationType'] );
		} else if ($data ['notificationTo'] == User_Type_Enum::PASSENGER) {
			$this->passengerNotification ( $response_data, $data ['notificationType'] );
		}
		$response = array (
				'msg' => $msg,
				'notification_id' => $notification_id 
		);
		echo json_encode ( $response );
	}
	public function getDetailsById($notification_id, $view_mode) {
		if ($view_mode == EDIT_MODE) {
			if ($this->User_Access_Model->writeModule ( $this->session->userdata ( 'role_type' ), Module_Name_Enum::NOTIFICATION ) === FALSE) {
				// redirect 404
				show_error ( "You do not have permission to view this page", '404' );
				return;
			}
		} else {
			if ($this->User_Access_Model->readModule ( $this->session->userdata ( 'role_type' ), Module_Name_Enum::NOTIFICATION ) === FALSE) {
				// redirect 404
				show_error ( "You do not have permission to view this page", '404' );
				return;
			}
		}
		$this->addJs ( 'app/notification.js' );
		// get API messages from message.php
		$msg_data = $this->config->item ( 'msg' );
		$message = '';
		$data = array (
				'mode' => $view_mode 
		);
		
		$data ['activetab'] = "notification";
		$data ['active_menu'] = 'notification';
		// to get city list
		
		if ($this->input->post ( 'id' )) {
			$notification_id = $this->input->post ( 'id' );
		}
		
		if ($notification_id > 0) {
			if ($view_mode == EDIT_MODE) {
				$locked = $this->Notification_Model->isLocked ( $notification_id );
				if ($locked) {
					$locked_user = $this->Notification_Model->getLockedUser ( $notification_id );
					$locked_date = $this->Notification_Model->getLockedDate ( $notification_id );
					$locked_date = date ( 'jS, F Y G:ia', strtotime ( $locked_date ) );
					$message = $msg_data ['locked'];
					$message = str_replace ( '##USERNAME##', $locked_user, $message );
					$message .= $locked_date;
					$data ['mode'] = VIEW_MODE;
				} else {
					$is_locked = $this->Notification_Model->lock ( $notification_id );
				}
			}
			$data ['notification_model'] = $this->Notification_Model->getById ( $notification_id );
		} else {
			$data ['notification_model'] = $this->Notification_Model->getBlankModel ();
		}
		if ($this->input->post ( 'id' )) {
			
			$html = $this->load->view ( 'notification/add_edit_notification', $data );
			echo json_encode ( array (
					'html' => $html 
			) );
		} else {
			$this->render ( 'notification/add_edit_notification', $data );
		}
	}
	public function getNotificationList() {
		if ($this->User_Access_Model->showPassenger () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		
		$this->addJs ( 'app/notification.js' );
		
		$data ['activetab'] = "notification";
		$data ['active_menu'] = 'notification';
		
		$this->render ( 'notification/manage_notification', $data );
	}
	public function changeNotificationStatus() {
		if ($this->User_Access_Model->fullAccessModule ( $this->session->userdata ( 'role_type' ), Module_Name_Enum::NOTIFICATION ) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to change staus", '404' );
			return;
		}
		$data = array ();
		
		$this->addJs ( 'app/notification.js' );
		$notification_updated = - 1;
		$data = $this->input->post ();
		$notification_id = $data ['id'];
		$notification_status = $data ['status'];
		
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['status_change_failed'];
		if ($notification_id) {
			$notification_updated = $this->Notification_Model->update ( array (
					'status' => $notification_status 
			), array (
					'id' => $notification_id 
			) );
			
			$msg = $msg_data ['status_change_success'];
		}
		$response = array (
				'msg' => $msg,
				'status' => $notification_updated 
		);
		echo json_encode ( $response );
	}
	public function ajax_list() {
		// echo"ajaxlist";exit;
		$sql_query = $this->Notification_Model->getNotificationListQuery ();
		$notification_list = $this->Notification_Model->getDataTable ( $sql_query );
		// print_r($list);exit;
		$data = array ();
		$no = $_POST ['start'];
		foreach ( $notification_list as $list ) {
			
			$row = array ();
			$row [] = '
			
				  <button class="btn btn-sm btn-success" id="viewnotification-' . $list->notificationId . '" title="View" >
                                <i class="fa fa-eye" aria-hidden="true"></i></button> 
                                <button class="btn btn-sm btn-blue" id="editnotification-' . $list->notificationId . '" title="Edit">
                                <i class="fa fa-pencil-square-o"></i></button><button class="btn btn-sm btn-danger" id="deletenotification-' . $list->notificationId . '" title="Delete">
                                <i class="fa fa-trash-o"></i></button>';
			
			$row [] = $list->title;
			$row [] = $list->description;
			$row [] = ($list->notificationTo == User_Type_Enum::DRIVER) ? 'Driver' : 'Passenger';
			$row [] = ($list->notificationType == 'P') ? 'Google Push' : 'SMS';
			$row [] = $list->startDatetime;
			$row [] = $list->endDatetime;
			$status_class = ($list->status) ? 'class="label label-sm label-success"' : 'class="label label-sm label-danger"';
			$status_name = ($list->status) ? 'Active' : 'Inactive';
			$row [] = '<span id="status-' . $list->notificationId . '" status="' . $list->status . '" ' . $status_class . '>' . $status_name . '</span>';
			// add html for action
			
			// <button class="btn btn-sm btn-danger" id="deletepassenger-'.$list->passengerId.'" title="Delete" ><i class="fa fa-trash-o"></i></button>';
			
			$data [] = $row;
		}
		
		$output = array (
				"draw" => $_POST ['draw'],
				"recordsTotal" => $this->Notification_Model->getDataTableAllCount ( $sql_query ),
				"recordsFiltered" => $this->Notification_Model->getDataTableFilterCount ( $sql_query ),
				"data" => $data 
		);
		// output to json format
		echo json_encode ( $output );
	}
	public function deleteNotification() {
		$data = array ();
		
		$this->addJs ( 'app/notification.js' );
		$notification_updated = - 1;
		$data = $this->input->post ();
		$notification_id = $data ['id'];
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['delete_failed'];
		if ($notification_id) {
			$notification_updated = $this->Notification_Model->update ( array (
					'isDeleted' => Status_Type_Enum::ACTIVE 
			), array (
					'id' => $notification_id 
			) );
			$msg = $msg_data ['delete_success'];
		}
		$response = array (
				'msg' => $msg,
				'status' => $notification_updated 
		);
		echo json_encode ( $response );
	}
	public function driverNotification($response_data, $notification_type = 'P') {
		
		// $gcm_id,$trip_id //Posted json data
		$gcm_id = '';
		$response = NULL;
		$msg_data = $this->config->item ( 'api' );
		$msg = '';
		$status = 0;
		$firebase_api_key = DRIVER_FIREBASE_API_KEY;
		$driver_list = NULL;
		$post_data = array ();
		$gcm_ids = array ();
		$registration_ids = array ();
		$array_slice = 20;
		
		if ($notification_type == 'P') {
			$driver_list = $this->Driver_Model->getByKeyValueArray ( array (
					'loginStatus' => Status_Type_Enum::ACTIVE 
			) );
			
			if ($driver_list) {
				$response_post_data = array (
						"message" => 'notification',
						"status" => 1,
						"description" => $response_data ['description'] 
				);
				
				$post_data = array (
						'details' => $response_post_data 
				);
				
				// split the gcmId column contents from passenger_list
				$gcm_ids = array_column ( $driver_list, 'gcmId' );
					
				// gcmId contents without empty values
				$gcm_ids = array_filter ( $gcm_ids );
					
				foreach ( array_chunk ( $gcm_ids, $array_slice ) as $registration_ids ) {
					$response = $this->fcmPushNotification ( $firebase_api_key, $registration_ids, $post_data );
				}
			}
		} else if ($notification_type == 'S') {
			if ($response_data ['description'] != '') {
				$driver_list = $this->Driver_Model->getByKeyValueArray ( array (
						'status' => Status_Type_Enum::ACTIVE,
						'isDeleted' => Status_Type_Enum::INACTIVE,
						'isVerified' => Status_Type_Enum::ACTIVE 
				) );
				
				if ($driver_list) {
					foreach ( $driver_list as $list ) {
						if ($list->mobile != "") {
							
							$this->Common_Api_Webservice_Model->sendSMS ( $list->mobile, $response_data ['description'] );
						}
					}
				}
			}
		}
	}
	public function passengerNotification($response_data, $notification_type = 'P') {
		// $gcm_id,$trip_id //Posted json data
		$gcm_id = '';
		$response = NULL;
		$msg_data = $this->config->item ( 'api' );
		$msg = '';
		$status = 0;
		$firebase_api_key = PASSENGER_FIREBASE_API_KEY;
		$passenger_list = NULL;
		$post_data = array ();
		$gcm_ids = array ();
		$registration_ids = array ();
		$array_slice = 20;
		
		if ($notification_type == 'P') {
			$passenger_list = $this->Passenger_Model->getByKeyValueArray ( array (
					'loginStatus' => Status_Type_Enum::ACTIVE,
			) );
			
			if ($passenger_list) {
				$post_data = array (
						'message' => "notification",
						'details' => array (
								$response_data 
						),
						'status' => 1 
				);
				
				// split the gcmId column contents from passenger_list
				$gcm_ids = array_column ( $passenger_list, 'gcmId' );
					
				// gcmId contents without empty values
				$gcm_ids = array_filter ( $gcm_ids );
					
				foreach ( array_chunk ( $gcm_ids, $array_slice ) as $registration_ids ) {
					$response = $this->fcmPushNotification ( $firebase_api_key, $registration_ids, $post_data );
				}
			}
			
		} else if ($notification_type == 'S') {
			if ($response_data ['description'] != '') {
				$passenger_list = $this->Passenger_Model->getByKeyValueArray ( array (
						'status' => Status_Type_Enum::ACTIVE,
						'activationStatus' => Status_Type_Enum::ACTIVE 
				) );
				
				if ($passenger_list) {
					foreach ( $passenger_list as $list ) {
						if ($list->mobile != "") {
							
							$this->Common_Api_Webservice_Model->sendSMS ( $list->mobile, $response_data ['description'] );
						}
					}
				}
			}
		}
	}
	private function fcmPushNotification($firebase_api_key, $registration_ids, $post_data) {
		if (is_array ( $registration_ids )) {
			$post_data ['registration_ids'] = $registration_ids;
		} else {
			$post_data ['to'] = $registration_ids;
		}
		$post_data ['priority'] = 'high';
		$post_data ['data'] = $post_data;
		
		$post_data = json_encode ( $post_data );
		$url = "https://fcm.googleapis.com/fcm/send";
		
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, array (
				'Content-Type:application/json',
				'Authorization: key=' . $firebase_api_key 
		) );
		curl_setopt ( $ch, CURLOPT_POST, 1 );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $post_data );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		$response = curl_exec ( $ch );
		curl_close ( $ch );
		return $response;
	}
	public function driverNotification_old($response_data, $notification_type = 'P') {
		
		// $gcm_id,$trip_id //Posted json data
		$gcm_id = '';
		$response = NULL;
		$msg_data = $this->config->item ( 'api' );
		$msg = '';
		$status = 0;
		$firebase_api_key = DRIVER_FIREBASE_API_KEY;
		$driver_list = NULL;
		
		if ($notification_type == 'P') {
			$driver_list = $this->Driver_Model->getByKeyValueArray ( array (
					'loginStatus' => Status_Type_Enum::ACTIVE 
			) );
			
			if ($driver_list) {
				foreach ( $driver_list as $list ) {
					if ($list->gcmId) {
						$post_data = array ();
						$gcm_id = $list->gcmId;
						$post_data ['to'] = $gcm_id;
						$post_data ['priority'] = 'high';
						
						$response_post_data = array (
								"message" => 'notification',
								"status" => 1,
								"description" => $response_data ['description'] 
						);
						
						$post_data ['data'] = array (
								'details' => $response_post_data 
						);
						
						$post_data = json_encode ( $post_data );
						
						$url = "https://fcm.googleapis.com/fcm/send";
						
						$ch = curl_init ();
						curl_setopt ( $ch, CURLOPT_URL, $url );
						curl_setopt ( $ch, CURLOPT_HTTPHEADER, array (
								'Content-Type:application/json',
								'Authorization: key=' . $firebase_api_key 
						) );
						curl_setopt ( $ch, CURLOPT_POST, 1 );
						curl_setopt ( $ch, CURLOPT_POSTFIELDS, $post_data );
						curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
						$response = curl_exec ( $ch );
						curl_close ( $ch );
					}
				}
			}
		} else if ($notification_type == 'S') {
			if ($response_data ['description'] != '') {
				$driver_list = $this->Driver_Model->getByKeyValueArray ( array (
						'status' => Status_Type_Enum::ACTIVE,
						'isDeleted' => Status_Type_Enum::INACTIVE,
						'isVerified' => Status_Type_Enum::ACTIVE 
				) );
				
				if ($driver_list) {
					foreach ( $driver_list as $list ) {
						if ($list->mobile != "") {
							
							$this->Common_Api_Webservice_Model->sendSMS ( $list->mobile, $response_data ['description'] );
						}
					}
				}
			}
		}
	}
	public function passengerNotification_old($response_data, $notification_type = 'P') {
		// $gcm_id,$trip_id //Posted json data
		$gcm_id = '';
		$response = NULL;
		$msg_data = $this->config->item ( 'api' );
		$msg = '';
		$status = 0;
		$firebase_api_key = PASSENGER_FIREBASE_API_KEY;
		$passenger_list = NULL;
		$post_data = array ();
		if ($notification_type == 'P') {
			$passenger_list = $this->Passenger_Model->getByKeyValueArray ( array (
					'loginStatus' => Status_Type_Enum::ACTIVE 
			) )
			// 'id'=>46677
			
			;
			
			if ($passenger_list) {
				foreach ( $passenger_list as $list ) {
					if ($list->gcmId) {
						$post_data = array ();
						$gcm_id = $list->gcmId;
						$post_data ['to'] = $gcm_id;
						$post_data ['priority'] = 'high';
						$post_data ['data'] = array (
								'message' => "notification",
								'details' => array (
										$response_data 
								),
								'status' => 1 
						);
						
						$post_data = json_encode ( $post_data );
						$url = "https://fcm.googleapis.com/fcm/send";
						
						$ch = curl_init ();
						curl_setopt ( $ch, CURLOPT_URL, $url );
						curl_setopt ( $ch, CURLOPT_HTTPHEADER, array (
								'Content-Type:application/json',
								'Authorization: key=' . $firebase_api_key 
						) );
						curl_setopt ( $ch, CURLOPT_POST, 1 );
						curl_setopt ( $ch, CURLOPT_POSTFIELDS, $post_data );
						curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
						$response = curl_exec ( $ch );
						
						curl_close ( $ch );
					}
				}
			}
		} else if ($notification_type == 'S') {
			if ($response_data ['description'] != '') {
				$passenger_list = $this->Passenger_Model->getByKeyValueArray ( array (
						'status' => Status_Type_Enum::ACTIVE,
						'activationStatus' => Status_Type_Enum::ACTIVE 
				) );
				
				if ($passenger_list) {
					foreach ( $passenger_list as $list ) {
						if ($list->mobile != "") {
							
							$this->Common_Api_Webservice_Model->sendSMS ( $list->mobile, $response_data ['description'] );
						}
					}
				}
			}
		}
	}
}