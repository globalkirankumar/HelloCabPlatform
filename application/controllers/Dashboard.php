<?php
class Dashboard extends MY_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->helper ( 'form' );
		$this->load->model ( 'Driver_Model' );
		$this->load->model ( 'Common_Api_Webservice_Model' );
		$this->load->model ( 'Driver_Dispatch_Details_Model' );
		$this->load->model ( 'Zone_Details_Model' );
		$this->load->model ( 'Entity_Config_Model' );
		$this->load->model ( 'Entity_Model' );
		$this->load->model ( 'Trip_Details_Model' );
	}
	public function index() {
		
		$data = array ();
		$zone_arr = array ();
		$role_type = $this->session->userdata ( 'role_type' );
		$entity_id = $this->session->userdata ( 'user_entity' );
		$driver_walletdata = $this->Driver_Dispatch_Details_Model->getDriverWalletDetailsForEntity ( $entity_id );
		$data ['activetab'] = "dashboard";
		$data ['active_menu'] = 'dashboard';
		$this->addCss ( 'vendors/chart/css/morris.css' );
		$this->addJs ( 'vendors/chart/js/morris.js' );
		// $this->addJs ( 'jquery.min.js' );
		$this->addJs ( 'vendors/chart/js/raphael-min.js' );
		$this->addJs ( 'app/dashboard.js' );
		$trip_detail_chartdata = ($this->uri->segment ( 3 ) != '') ? $this->uri->segment ( 3 ) : 0;
		$zone_id = ($this->uri->segment ( 4 ) != '') ? $this->uri->segment ( 4 ) : 0;
		$sub_zone = ($this->uri->segment ( 5 ) != '') ? $this->uri->segment ( 5 ) : 0;
		$data ['chartdata'] = $this->getChartDetailsForTripData ( $trip_detail_chartdata, $zone_id, $sub_zone );
		if ($zone_id > 0) {
			$availablecabs_data = $this->Driver_Dispatch_Details_Model->getLatLongQueryByStatus ( Taxi_Available_Status_Enum::FREE);
			$data ['free_status_cabs'] = $this->getCabsCountByLatLongData ( $availablecabs_data, $zone_id );//$data ['free_status_cabs']=1;
			$busycabs_data = $this->Driver_Dispatch_Details_Model->getLatLongQueryByStatus ( Taxi_Available_Status_Enum::BUSY,Status_Type_Enum::INACTIVE);
			
			$data ['busy_status_cabs'] = $this->getCabsCountByLatLongData ( $busycabs_data, $zone_id );
			
			$ontrip_busycabs_data = $this->Driver_Dispatch_Details_Model->getLatLongQueryByStatus ( Taxi_Available_Status_Enum::BUSY,Status_Type_Enum::ACTIVE);
			$data ['ontrip_busy_status_cabs'] = $this->getCabsCountByLatLongData ( $ontrip_busycabs_data, $zone_id );
			
			$total_drivers=$this->Driver_Dispatch_Details_Model->getLoggedInDriverDetails();
			//$data ['busy_status_cabs'] = $this->getCabsCountByLatLongData ( $total_drivers, $zone_id );
			$logout_cabsdata = $this->Driver_Dispatch_Details_Model->getLogStatusCabsByZone (Status_Type_Enum::INACTIVE);
			$data ['log_out_cabs'] = $this->getCabsCountByLatLongData ( $logout_cabsdata, $zone_id );
			$login_cabsdata = $this->Driver_Dispatch_Details_Model->getLogStatusCabsByZone (Status_Type_Enum::ACTIVE);
			$data ['log_in_cabs'] = $this->getCabsCountByLatLongData ( $login_cabsdata, $zone_id );
			$total_drivers=$this->Driver_Model->getByKeyValueArray(array('isDeleted'=>Status_Type_Enum::INACTIVE));
			
			$data ['unknown_log_cabs']=(count($total_drivers)-($data ['log_in_cabs']+$data ['log_out_cabs']));
			$data ['unknown_status_cabs']=$data ['log_in_cabs']-($data ['free_status_cabs']+$data ['busy_status_cabs']+$data ['ontrip_busy_status_cabs']);
		} else {
			$data ['free_status_cabs'] = $this->Driver_Dispatch_Details_Model->getAvailablityCount ( Taxi_Available_Status_Enum::FREE );
			$data ['busy_status_cabs'] = $this->Driver_Dispatch_Details_Model->getAvailablityCount ( Taxi_Available_Status_Enum::BUSY,Status_Type_Enum::INACTIVE );
			
			$data ['ontrip_busy_status_cabs'] = $this->Driver_Dispatch_Details_Model->getAvailablityCount ( Taxi_Available_Status_Enum::BUSY,Status_Type_Enum::ACTIVE );
			
			$total_drivers=$this->Driver_Model->getByKeyValueArray(array('isDeleted'=>Status_Type_Enum::INACTIVE,'loginStatus'=>Status_Type_Enum::ACTIVE));
			
			$data ['log_out_cabs'] = $this->Driver_Dispatch_Details_Model->getLogStatusCabs (Status_Type_Enum::INACTIVE);
			$data ['log_in_cabs'] = $this->Driver_Dispatch_Details_Model->getLogStatusCabs (Status_Type_Enum::ACTIVE);
			$total_drivers=$this->Driver_Model->getByKeyValueArray(array('isDeleted'=>Status_Type_Enum::INACTIVE));
			
			$data ['unknown_log_cabs']=(count($total_drivers)-($data ['log_in_cabs']+$data ['log_out_cabs']));
			$data ['unknown_status_cabs']=$data ['log_in_cabs']-($data ['free_status_cabs']+$data ['busy_status_cabs']+$data ['ontrip_busy_status_cabs']);
		}
		$entity_config_details=$this->Entity_Config_Model->getOneByKeyValueArray(array('entityId'=>DEFAULT_COMPANY_ID));
		for($i=1;$i<=$entity_config_details->drtSecondLimit;$i++)
		{
			$data ['consecutive_count'][$i]= $this->Driver_Dispatch_Details_Model->getConsecutiveRejectCount ( $i);
		}
		//$data ['two_consecutive_count'] = $this->Driver_Dispatch_Details_Model->getConsecutiveRejectCount ( 2 );
		//$data ['three_consecutive_count'] = $this->Driver_Dispatch_Details_Model->getConsecutiveRejectCount ( 3 );
		//$data ['four_consecutive_count'] = $this->Driver_Dispatch_Details_Model->getConsecutiveRejectCount ( 4 );
		
		$data ['role_type'] = $role_type;
		
		
		$this->render ( "dashboard/admin", $data );
	}
	/* public function getCabsCountByLatLongData($cabs_data, $zone_id) {
		$zone_arr = array ();
		$cab_count = 0;
		$resultarr =array();
		$rowdata = array();
		if (count ( $cabs_data ) > 0) {
			foreach ( $cabs_data as $availabledata ) {
				$lat = $availabledata->driverLatitude;
				$long = $availabledata->driverLongitude;
				$zone_arr [] = $this->getZoneByLatLongOfDriver ( $lat, $long );
			}
		}
		// $resultarr = array_reduce ( $zone_arr, 'array_merge', array () );
		
		// $this->getLatLongOfDriverByZone($driver_lat_arr,$driver_long_arr,$zone_arr);
		if (count ( $zone_arr ) > 0) {
			$resultarr = array_reduce ( $zone_arr, 'array_merge', array () );
		}
		
		foreach ( $resultarr as $res ) {
			$zonedatas = explode ( ',', $res );
			if (in_array ( $zone_id, $zonedatas )) {
				$pzoneid = @$zonedatas [0];
				$lat = @$zonedatas [1];
				$long = @$zonedatas [2];
				$status = @$zonedatas [3];
				$driverid = @$zonedatas [4];
				$drivername = @$zonedatas [5];
				$drivermobile = @$zonedatas [6];
				if ($pzoneid == $zone_id) {
					$row ['zone_id'] = $pzoneid;
					$row ['latitude'] = $lat;
					$row ['longitutde'] = $long;
					$row ['status'] = $status;
					$row ['driver_id'] = $driverid;
					$row ['driver_name'] = $drivername;
					$row ['driver_mobile'] = $drivermobile;
					$rowdata [] = $row;
				}
			}
		}
		$available_cabs = count ( $rowdata );
		$cab_count = isset ( $available_cabs [$zone_id] ) ? $available_cabs [$zone_id] : 0;
		return $cab_count;
	}
*/
	public function getCabsCountByLatLongData($cabs_data, $zone_id) {
		$zone_arr = array ();
		$cab_count = 0;
		if (count ( $cabs_data ) > 0) {
			foreach ( $cabs_data as $availabledata ) {
				$lat = $availabledata->driverLatitude;
				$long = $availabledata->driverLongitude;
				$zone_arr [] = $this->getZoneByLatLongOfDriver ( $lat, $long );
			}
		}
		$resultarr = array_reduce ( $zone_arr, 'array_merge', array () );
		$available_cabs = array_count_values ( $resultarr );
		$cab_count = isset ( $available_cabs [$zone_id] ) ? $available_cabs [$zone_id] : 0;
		return $cab_count;
	} 
	
	/* public function tripDispatchNotification() {
		// $gcm_id,$trip_id //Posted json data
		$gcm_id = '';
		$response = NULL;
		$msg_data = $this->config->item ( 'api' );
		$msg = '';
		$status = 0;
		$firebase_api_key = 'AIzaSyBYtAN_5lgfexbcyjgwX1FBDVIVzjmgeQE'; // FIREBASE_API_KEY;
		$driver_list = NULL;
		$post_data = array ();
		
		$driver_list = $this->Driver_Model->getByKeyValueArray ( array (
				'loginStatus' => Status_Type_Enum::ACTIVE 
		) );
		
		if ($driver_list) {
			foreach ( $driver_list as $list ) {
				if ($list->gcmId) {
					$gcm_id = $list->gcmId;
					$post_data ['registration_ids'] = ( array ) $gcm_id;
					if ($gcm_id) {
						$msg = $msg_data ['success'];
						$status = 1;
						$post_data ['data'] = array (
								'message' => "trip_dispatch",
								'status' => 1 
						);
					} else {
						$msg = $msg_data ['failed'];
						$post_data ['data'] = array (
								'message' => $msg_data ['failed'],
								'status' => - 1 
						);
					}
					
					$post_data = json_encode ( $post_data );
					$url = "https://fcm.googleapis.com/fcm/send";
					
					$ch = curl_init ();
					curl_setopt ( $ch, CURLOPT_URL, $url );
					curl_setopt ( $ch, CURLOPT_HTTPHEADER, array (
							'Content-Type:application/json',
							'Authorization: key=' . $firebase_api_key 
					) );
					curl_setopt ( $ch, CURLOPT_POST, 1 );
					curl_setopt ( $ch, CURLOPT_POSTFIELDS, $post_data );
					curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
					$response = curl_exec ( $ch );
					curl_close ( $ch );
				}
			}
		}
		
		echo json_encode ( array (
				'response' => $response,
				'msg' => $msg,
				'status' => $status 
		) );
	}
	public function tripStartNotification() {
		// $gcm_id,$trip_id //Posted json data
		$gcm_id = '';
		// $trip_id = $post_data->trip_id;
		// $driver_id = 2;
		$response = NULL;
		$msg_data = $this->config->item ( 'api' );
		$msg = '';
		$status = 0;
		$firebase_api_key = 'AIzaSyBYtAN_5lgfexbcyjgwX1FBDVIVzjmgeQE'; // FIREBASE_API_KEY;
		$driver_list = NULL;
		$post_data = array ();
		
		$driver_list = $this->Driver_Model->getByKeyValueArray ( array (
				'loginStatus' => Status_Type_Enum::ACTIVE 
		) );
		
		if ($driver_list) {
			foreach ( $driver_list as $list ) {
				if ($list->gcmId) {
					$gcm_id = $list->gcmId;
					$post_data ['registration_ids'] = ( array ) $gcm_id;
					if ($gcm_id) {
						$msg = $msg_data ['success'];
						$status = 1;
						$post_data ['data'] = array (
								'message' => "trip_start",
								'status' => 1 
						);
					} else {
						$msg = $msg_data ['failed'];
						
						$post_data ['data'] = array (
								'message' => $msg_data ['failed'],
								'status' => - 1 
						);
					}
					
					$post_data = json_encode ( $post_data );
					$url = "https://fcm.googleapis.com/fcm/send";
					
					$ch = curl_init ();
					curl_setopt ( $ch, CURLOPT_URL, $url );
					curl_setopt ( $ch, CURLOPT_HTTPHEADER, array (
							'Content-Type:application/json',
							'Authorization: key=' . $firebase_api_key 
					) );
					curl_setopt ( $ch, CURLOPT_POST, 1 );
					curl_setopt ( $ch, CURLOPT_POSTFIELDS, $post_data );
					curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
					$response = curl_exec ( $ch );
					curl_close ( $ch );
					// return $response;
				}
			}
		}
		echo json_encode ( array (
				'response' => $response,
				'msg' => $msg,
				'status' => $status 
		) );
	}
	public function tripEndNotification() {
		// $gcm_id,$trip_id //Posted json data
		$gcm_id = '';
		// $trip_id = $post_data->trip_id;
		// $driver_id = 2;
		$response = NULL;
		$msg_data = $this->config->item ( 'api' );
		$msg = '';
		$status = 0;
		$firebase_api_key = 'AIzaSyBYtAN_5lgfexbcyjgwX1FBDVIVzjmgeQE'; // FIREBASE_API_KEY;
		$driver_list = NULL;
		$post_data = array ();
		
		$driver_list = $this->Driver_Model->getByKeyValueArray ( array (
				'loginStatus' => Status_Type_Enum::ACTIVE 
		) );
		
		if ($driver_list) {
			foreach ( $driver_list as $list ) {
				if ($list->gcmId) {
					$gcm_id = $list->gcmId;
					$post_data ['registration_ids'] = ( array ) $gcm_id;
					if ($gcm_id) {
						$msg = $msg_data ['success'];
						$status = 1;
						$post_data ['data'] = array (
								'message' => "trip_end",
								'status' => 1 
						);
					} else {
						$msg = $msg_data ['failed'];
						
						$post_data ['data'] = array (
								'message' => $msg_data ['failed'],
								'status' => - 1 
						);
					}
					
					$post_data = json_encode ( $post_data );
					$url = "https://fcm.googleapis.com/fcm/send";
					
					$ch = curl_init ();
					curl_setopt ( $ch, CURLOPT_URL, $url );
					curl_setopt ( $ch, CURLOPT_HTTPHEADER, array (
							'Content-Type:application/json',
							'Authorization: key=' . $firebase_api_key 
					) );
					curl_setopt ( $ch, CURLOPT_POST, 1 );
					curl_setopt ( $ch, CURLOPT_POSTFIELDS, $post_data );
					curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
					$response = curl_exec ( $ch );
					curl_close ( $ch );
					// return $response;
				}
			}
		}
		
		
		// redirect('dashboard');
		echo json_encode ( array (
				'response' => $response,
				'msg' => $msg,
				'status' => $status 
		) );
	} */
	public function getChartDetailsForTripData($trip_detail_chartdata, $zone_id, $sub_zone) {
		/**
		 * To Get Completed Trip Count For 24 hours *
		 */
		if ($zone_id != '') {
			$zone_qry = "AND zoneId=$zone_id";
		} else {
			$zone_qry = "";
		}
		if ($sub_zone != '') {
			$subzone_qry = "AND subZoneId=$sub_zone";
		} else {
			$subzone_qry = "";
		}
		if ($trip_detail_chartdata == 0) {
			$day_qry = "WHERE DATE(`pickupDatetime`) = CURDATE()"; // to get todays data
		} else if ($trip_detail_chartdata == 1) {
			$day_qry = "WHERE tripdetails.`pickupDatetime` > DATE_SUB(CURDATE(), INTERVAL 1 DAY)"; // to get 24 hrs data
		} else if ($trip_detail_chartdata == 2) {
			$day_qry = "WHERE tripdetails.`pickupDatetime` > DATE_SUB(CURDATE(), INTERVAL 2 DAY)"; // to get 48 hrs data
		} else {
			$day_qry = "WHERE DATE(`pickupDatetime`) = CURDATE()"; // to get todays data
		}
		
		$sql = "SELECT HOUR(pickupDatetime) AS hours, COUNT(*) as completed
                        FROM tripdetails $day_qry
                        AND tripStatus=" . Trip_Status_Enum::TRIP_COMPLETED . " $zone_qry $subzone_qry
                        GROUP BY hours";
		$result = $this->db->query ( $sql )->result ();
		$cmp_zero_2 = $cmp_two_4 = $cmp_four_6 = $cmp_six_8 = $cmp_eight_10 = $cmp_ten_12 = 0;
		$cmp_twelve_14 = $cmp_twelve_14 = $cmp_fourteen_16 = $cmp_fourteen_16 = $cmp_sixteen_18 = $cmp_eighteen_20 = $cmp_twenty_22 = $cmp_twentytwo_24 = 0;
		if ($result) {
			foreach ( $result as $row ) {
				$hours = $row->hours;
				$completed = $row->completed;
				$hours_arr [] = $hours;
				$completed_arr [] = $completed;
			}
			$arr = array_combine ( $hours_arr, $completed_arr );
			$cmp_zero_2 = $this->getArrayValuesByKeyRange ( $arr, 0, 2 );
			$cmp_two_4 = $this->getArrayValuesByKeyRange ( $arr, 2, 4 );
			$cmp_four_6 = $this->getArrayValuesByKeyRange ( $arr, 4, 6 );
			$cmp_six_8 = $this->getArrayValuesByKeyRange ( $arr, 6, 8 );
			$cmp_eight_10 = $this->getArrayValuesByKeyRange ( $arr, 8, 10 );
			$cmp_ten_12 = $this->getArrayValuesByKeyRange ( $arr, 10, 12 );
			$cmp_twelve_14 = $this->getArrayValuesByKeyRange ( $arr, 12, 14 );
			$cmp_fourteen_16 = $this->getArrayValuesByKeyRange ( $arr, 14, 16 );
			$cmp_sixteen_18 = $this->getArrayValuesByKeyRange ( $arr, 16, 18 );
			$cmp_eighteen_20 = $this->getArrayValuesByKeyRange ( $arr, 18, 20 );
			$cmp_twenty_22 = $this->getArrayValuesByKeyRange ( $arr, 20, 22 );
			$cmp_twentytwo_24 = $this->getArrayValuesByKeyRange ( $arr, 22, 24 );
		}
		/**
		 * To Get In Progress Trip Count For 24 hours *
		 */
		$ip_sql = "SELECT HOUR(pickupDatetime) AS hours, COUNT(*) as completed
                            FROM tripdetails $day_qry
                            AND tripStatus=" . Trip_Status_Enum::IN_PROGRESS . " $zone_qry $subzone_qry
                            GROUP BY hours";
		$ip_result = $this->db->query ( $ip_sql )->result ();
		$ip_zero_2 = $ip_two_4 = $ip_four_6 = $ip_six_8 = $ip_eight_10 = $ip_ten_12 = 0;
		$ip_twelve_14 = $ip_twelve_14 = $ip_fourteen_16 = $ip_fourteen_16 = $ip_sixteen_18 = $ip_eighteen_20 = $ip_twenty_22 = $ip_twentytwo_24 = 0;
		if ($ip_result) {
			foreach ( $ip_result as $row ) {
				$ip_hours = $row->hours;
				$ip_completed = $row->completed;
				$ip_hours_arr [] = $ip_hours;
				$ip_completed_arr [] = $ip_completed;
			}
			$ip_arr = array_combine ( $ip_hours_arr, $ip_completed_arr );
			$ip_zero_2 = $this->getArrayValuesByKeyRange ( $ip_arr, 0, 2 );
			$ip_two_4 = $this->getArrayValuesByKeyRange ( $ip_arr, 2, 4 );
			$ip_four_6 = $this->getArrayValuesByKeyRange ( $ip_arr, 4, 6 );
			$ip_six_8 = $this->getArrayValuesByKeyRange ( $ip_arr, 6, 8 );
			$ip_eight_10 = $this->getArrayValuesByKeyRange ( $ip_arr, 8, 10 );
			$ip_ten_12 = $this->getArrayValuesByKeyRange ( $ip_arr, 10, 12 );
			$ip_twelve_14 = $this->getArrayValuesByKeyRange ( $ip_arr, 12, 14 );
			$ip_fourteen_16 = $this->getArrayValuesByKeyRange ( $ip_arr, 14, 16 );
			$ip_sixteen_18 = $this->getArrayValuesByKeyRange ( $ip_arr, 16, 18 );
			$ip_eighteen_20 = $this->getArrayValuesByKeyRange ( $ip_arr, 18, 20 );
			$ip_twenty_22 = $this->getArrayValuesByKeyRange ( $ip_arr, 20, 22 );
			$ip_twentytwo_24 = $this->getArrayValuesByKeyRange ( $ip_arr, 22, 24 );
		}
		/**
		 * To Get Cancelled Trip Count For 24 hours *
		 */
		$cd_sql = "SELECT HOUR(pickupDatetime) AS hours, COUNT(*) as completed
                            FROM tripdetails
                            $day_qry
                            AND (tripStatus=" . Trip_Status_Enum::CANCELLED_BY_DRIVER . " OR tripStatus=" . Trip_Status_Enum::CANCELLED_BY_PASSENGER . ") 
                            $zone_qry $subzone_qry
                            GROUP BY hours";
		$cd_result = $this->db->query ( $cd_sql )->result ();
		$cd_zero_2 = $cd_two_4 = $cd_four_6 = $cd_six_8 = $cd_eight_10 = $cd_ten_12 = $cd_twelve_14 = 0;
		$cd_fourteen_16 = $cd_sixteen_18 = $cd_eighteen_20 = $cd_twenty_22 = $cd_twentytwo_24 = 0;
		if ($cd_result) {
			foreach ( $cd_result as $row ) {
				$cd_hours = $row->hours;
				$cd_completed = $row->completed;
				$cd_hours_arr [] = $cd_hours;
				$cd_completed_arr [] = $cd_completed;
			}
			$cd_arr = array_combine ( $cd_hours_arr, $cd_completed_arr );
			$cd_zero_2 = $this->getArrayValuesByKeyRange ( $cd_arr, 0, 2 );
			$cd_two_4 = $this->getArrayValuesByKeyRange ( $cd_arr, 2, 4 );
			$cd_four_6 = $this->getArrayValuesByKeyRange ( $cd_arr, 4, 6 );
			$cd_six_8 = $this->getArrayValuesByKeyRange ( $cd_arr, 6, 8 );
			$cd_eight_10 = $this->getArrayValuesByKeyRange ( $cd_arr, 8, 10 );
			$cd_ten_12 = $this->getArrayValuesByKeyRange ( $cd_arr, 10, 12 );
			$cd_twelve_14 = $this->getArrayValuesByKeyRange ( $cd_arr, 12, 14 );
			$cd_fourteen_16 = $this->getArrayValuesByKeyRange ( $cd_arr, 14, 16 );
			$cd_sixteen_18 = $this->getArrayValuesByKeyRange ( $cd_arr, 16, 18 );
			$cd_eighteen_20 = $this->getArrayValuesByKeyRange ( $cd_arr, 18, 20 );
			$cd_twenty_22 = $this->getArrayValuesByKeyRange ( $cd_arr, 20, 22 );
			$cd_twentytwo_24 = $this->getArrayValuesByKeyRange ( $cd_arr, 22, 24 );
		}
		
		$chartdata = "{ y: '0-2',   a: $cmp_zero_2,   b: $ip_zero_2 ,  c:$cd_zero_2 },
                          { y: '2-4',   a: $cmp_two_4,  b: $ip_two_4,  c:$cd_two_4 },
                          { y: '4-6',   a: $cmp_four_6,  b: $ip_four_6 , c:$cd_four_6 },
                          { y: '6-8',   a: $cmp_six_8,  b: $ip_six_8,  c:$cd_six_8 },
                          { y: '8-10',  a: $cmp_eight_10,  b: $ip_eight_10,  c:$cd_eight_10 },
                          { y: '10-12', a: $cmp_ten_12,  b: $ip_ten_12,  c:$cd_ten_12 },
                          { y: '12-14', a: $cmp_twelve_14,   b: $ip_twelve_14,   c:$cd_twelve_14 },
                          { y: '14-16', a: $cmp_fourteen_16,   b: $ip_fourteen_16,   c:$cd_fourteen_16 },
                          { y: '16-18', a: $cmp_sixteen_18,   b: $ip_sixteen_18,   c:$cd_sixteen_18 },
                          { y: '18-20', a: $cmp_eighteen_20,   b: $ip_eighteen_20,   c:$cd_eighteen_20 },
                          { y: '20-22', a: $cmp_twenty_22,   b: $ip_twenty_22,   c:$cd_twenty_22 },
                          { y: '22-24', a: $cmp_twentytwo_24,   b: $ip_twentytwo_24,   c:$cd_twentytwo_24 },";
		return $chartdata;
	}
	public function getArrayValuesByKeyRange($arr, $key1, $key2) {
		$arr_count = array ();
		for($i = $key1; $i <= $key2; $i ++) {
			if (array_key_exists ( $i, $arr )) {
				$arr_count [] = $arr [$i];
			}
		}
		return array_sum ( $arr_count );
	}
	
	/**
	 * ** Code to Find Zone Based on Lat and Long ***
	 */
	public function getZoneByLatLongOfDriver($lat = NULL, $long = NULL) {
		$polyX = array (); // horizontal coordinates of corners
		$polyY = array (); // vertical coordinates of corners
		$zone_arr = array ();
		$zone_list = $this->Zone_Details_Model->getZoneListQueryForTripCreation ();
		
		if ($zone_list) {
			foreach ( $zone_list as $list ) {
				$polygan_points = $list->polygonPoints;
				$zone_name = $list->name;
				$zone_id = $list->zoneId;
				$zone_arr [] = $this->process_poylgan_data ( $polygan_points, $lat, $long, $zone_name, $zone_id );
			}
		}
		return array_filter ( $zone_arr );
	} // end of function
	public function process_poylgan_data($polygan_points, $lat, $long, $zone_name, $zone_id) {
		$zonal = $polygan_points;
		$zone_arr = array ();
		$zonalArray = explode ( "|", $zonal );
		$polySides = count ( $zonalArray ); // how many corners the polygon has
		foreach ( $zonalArray as $eachZoneLatLongs ) {
			$latlongArray = explode ( ",", $eachZoneLatLongs );
			$polyX [] = isset ( $latlongArray [0] ) ? $latlongArray [0] : '';
			$polyY [] = isset ( $latlongArray [1] ) ? $latlongArray [1] : '';
		}
		
		$vertices_x = $polyX; // x-coordinates of the vertices of the polygon
		$vertices_y = $polyY; // y-coordinates of the vertices of the polygon
		$points_polygon = count ( $vertices_x ); // number vertices
		                                      // Following Points lie inside this region
		$longitude_x = $lat; // x-coordinate of the point to test
		$latitude_y = $long; // y-coordinate of the point to test
		
		if ($this->is_in_polygon ( $points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y )) {
			// echo json_encode(array('ZoneId'=>$zone_id,'Zone_name'=>$zone_name));
			$zone_arr = $zone_id;
			return $zone_arr;
		}
		// else {
		// return 0;
		// }
	}
	public function is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y) {
		$i = $j = $c = 0;
		
		for($i = 0, $j = $points_polygon - 1; $i < $points_polygon; $j = $i ++) {
			$vertices_y [$i] = ( float ) $vertices_y [$i];
			$vertices_y [$j] = ( float ) $vertices_y [$j];
			$vertices_x [$i] = ( float ) $vertices_x [$i];
			$vertices_x [$j] = ( float ) $vertices_x [$j];
			if ((($vertices_y [$i] > $latitude_y != ($vertices_y [$j] > $latitude_y)) && ($longitude_x < ($vertices_x [$j] - $vertices_x [$i]) * ($latitude_y - $vertices_y [$i]) / ($vertices_y [$j] - $vertices_y [$i]) + $vertices_x [$i])))
				$c = ! $c;
		}
		return $c;
	}
}// end of class

