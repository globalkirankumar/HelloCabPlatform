<?php
class Trip extends My_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->helper ( 'form' );
		$this->load->model ( 'Taxi_Query_Model' );
		$this->load->model ( 'Driver_Model' );
		$this->load->model ( 'Taxi_Device_Details_Model' );
		$this->load->model ( 'Driver_Dispatch_Details_Model' );
		$this->load->model ( 'Taxi_Rejected_Trip_Details_Model' );
		$this->load->model ( 'Rate_Card_Details_Model' );
		$this->load->model ( 'Rate_Card_Slab_Details_Model' );
		$this->load->model ( 'Driver_Transaction_Details_Model' );
		$this->load->model ( 'Passenger_Transaction_Details_Model' );
		$this->load->model ( 'Taxi_Request_Details_Model' );
		$this->load->model ( 'Driver_Shift_History_Model' );
		$this->load->model ( 'Promocode_Details_Model' );
		$this->load->model ( 'Taxi_Details_Model' );
		$this->load->model ( 'Trip_Details_Model' );
		$this->load->model ( 'Driver_Taxi_Mapping_Model' );
		$this->load->model ( 'Zone_Details_Model' );
		$this->load->model ( 'Entity_Model' );
		$this->load->model ( 'Entity_Config_Model' );
		$this->load->model ( 'Driver_Personal_Details_Model' );
		$this->load->model ( 'Data_Attributes_Model' );
		$this->load->model ( 'Sms_Template_Model' );
		$this->load->model ( 'v1/Common_Api_Webservice_Model' );
		$this->load->model ( 'v1/Driver_Api_Webservice_Model' );
		$this->load->model ( 'Referral_Details_Model' );
		$this->load->model ( 'Passenger_Model' );
		$this->load->model ( 'Trip_Temp_Tracking_Details_Model' );
		$this->load->model ( 'User_Login_Details_Model' );
		
		
		$this->load->model ( 'Trip_Driver_List_Model' );
	}
	public function index() {
		
	}
	public function add() {
		if ($this->User_Access_Model->createModule ( $this->session->userdata ( 'role_type' ), Module_Name_Enum::TRIP ) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		
		$this->addJs ( 'app/trip.js' );
		// $this->addCss ( 'app/colorbox.css' );
		$this->addJs ( 'app/trip/trip_pickuplocation.js' );
		$this->addJs ( 'app/trip/trip_droplocation.js' );
		
		$data = array (
				'mode' => EDIT_MODE 
		);
		$data ['activetab'] = "trip";
		$data ['active_menu'] = 'trip';
		$data ['zone_list'] = $this->Zone_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE 
		), 'name' );
		$data ['sub_zone_list'] = $this->Zone_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE,
				'zoneId !=' => Status_Type_Enum::INACTIVE 
		), 'name' );
		
		$data ['entity_list'] = $this->Entity_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE 
		), 'name' );
		
		$data ['trip_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRIP_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		$data ['taxi_category_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TAXI_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		
		$data ['payment_mode_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'PAYMENT_MODE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		$data ['trip_status_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRIP_STATUS' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		
		$data ['passenger_model'] = $this->Passenger_Model->getBlankModel ();
		$data ['driver_model'] = $this->Driver_Model->getBlankModel ();
		$data ['user_model'] = $this->User_Login_Details_Model->getBlankModel ();
		$data ['trip_model'] = $this->Trip_Details_Model->getBlankModel ();
		$this->render ( 'trip/add_edit_trip', $data );
	}
	public function saveTrip() {
		$this->addJs ( 'app/trip.js' );
		$data = array ();
		$data = $this->input->post ();
		// print_r($data);exit;
		$trip_id = $data ['id'];
		$data ['activetab'] = "trip";
		$data ['active_menu'] = 'trip';
		// debug_exit($data);
		$msg_data = $this->config->item ( 'api' );
		$msg = $msg_data ['booking_failed'];
		$estimated_distance = 0;
		$estimated_time = 0;
		$is_dispatched=0;
		$drop_zone_id=0;
		$zone_id=0;
		
		$zone_id = $data ['zoneId'];
		if ($data ['subZoneId']) {
			$data ['zoneId'] = $data ['subZoneId'];
			$data ['subZoneId'] = $zone_id;
		}
		$drop_zone_id = $data ['dropZoneId'];
		if ($data ['dropSubZoneId']) {
			$data ['dropZoneId'] = $data ['dropSubZoneId'];
			$data ['dropZoneId'] = $drop_zone_id;
		}
		if ($trip_id > 0) {
			$this->Trip_Details_Model->update ( $data, array (
					'id' => $trip_id 
			) );
			$msg = $msg_data ['success'];
		} else {
			if ($data ['pickupDatetime'] == 0 || $data ['pickupDatetime'] == '0' || ! $data ['pickupDatetime']) {
				$pickup_time = getCurrentUnixDateTime ();
				$pickup_time = $pickup_time + (60 * 1);
				$pickup_time = date ( "Y-m-d H:i:s", $pickup_time );
				$data ['pickupDatetime'] = $pickup_time;
			}
			if ($data ['tripType'] == Trip_Type_Enum::NOW) {
				$pickup_time = getCurrentUnixDateTime ();
				$pickup_time = $pickup_time + (60 * 1);
				$pickup_time = date ( "Y-m-d H:i:s", $pickup_time );
				$data ['pickupDatetime'] = $pickup_time;
			} else if ($data ['tripType'] == Trip_Type_Enum::LATER) {
				$min_pickup_time = getCurrentUnixDateTime ();
				$min_pickup_time = $min_pickup_time + (60 * 60);
				// echo $min_pickup_time1 = date ( "Y-m-d H:i:s", $min_pickup_time );
				// $max_pickup_time = getCurrentUnixDateTime ();
				$max_pickup_time = $min_pickup_time + (60 * 10080);
				// echo $max_pickup_time1 = date ( "Y-m-d H:i:s", $max_pickup_time );
				$pickup_time = strtotime ( $data ['pickupDatetime'] );
				if (! ($pickup_time >= $min_pickup_time && $pickup_time <= $max_pickup_time)) {
					$response = array (
							'msg' => $msg_data ['invalid_pickup_time'],
							'trip_id' => $trip_id 
					);
					echo json_encode ( $response );
					return false;
				}
			}
			// To get trip estimated distance and time from google API
			$URL = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=" . $data ['pickupLatitude'] . "," . $data ['pickupLongitude'] . "&destinations=" . $data ['dropLatitude'] . "," . $data ['dropLongitude'] . "&key=" . GOOGLE_MAP_API_KEY;
			
			$curl = curl_init ();
			curl_setopt ( $curl, CURLOPT_URL, $URL );
			curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, true );
			curl_setopt ( $curl, CURLOPT_HEADER, false );
			$result = curl_exec ( $curl );
			curl_close ( $curl );
			// return $result; ;
			
			$response = json_decode ( $result );
			
			if ($response->status) {
				if ($response->rows [0]->elements [0]->distance->text) {
					$distance = explode ( ' ', $response->rows [0]->elements [0]->distance->text );
					if ($distance [1] == 'm') {
						$estimated_distance = round ( ($distance [0] / 1000), 1 );
					} else if ($distance [1] == 'km') {
						$estimated_distance = round ( ($distance [0]), 1 );
					}
				}
				if ($response->rows [0]->elements [0]->duration->text) {
					$time = explode ( ' ', $response->rows [0]->elements [0]->duration->text );
					
					if ($time [1] == 'hour' || $time [1] == 'hours') {
						$estimated_time += ($time [0] * 60) + $time [2];
					} else if ($time [1] == 'mins') {
						$estimated_time = $time [0];
					}
				}
			}
			// To get trip estimated distance and time from google API
			
			$data ['estimatedDistance'] = $estimated_distance;
			$data ['estimatedTime'] = $estimated_time;
			$data ['bookedFrom'] = Signup_Type_Enum::BACKEND;
			$data ['tripStatus'] = Trip_Status_Enum::BOOKED;
			$data ['notificationStatus'] = Trip_Status_Enum::BOOKED;
			$data ['customerReferenceCode'] = str_pad ( ( string ) rand ( 0, 9999 ), 4, '0', STR_PAD_LEFT );
			
			$trip_id = $this->Trip_Details_Model->insert ( $data );
			
			
			$entity_details = $this->Entity_Model->getById ( $data ['entityId'] );
			// update the Job card id
			$trip_job_id = (($trip_id <= 999999)) ? str_pad ( ( string ) $trip_id, 6, '0', STR_PAD_LEFT ) : $trip_id;
			
			$job_card_id = $entity_details->entityPrefix . '' . $trip_job_id;
			$update_job_card_id = $this->Trip_Details_Model->update ( array (
					'jobCardId' => $job_card_id 
			), array (
					'id' => $trip_id 
			) );
			
			
			$taxi_request_details = NULL;
			
			if ($data ['tripType'] == Trip_Type_Enum::NOW) {
				$taxi_request_details = $this->getTaxiForTrip ( $trip_id );
				
				if ($taxi_request_details) {
					// $driver_dispatched=$this->tripDispatchDriverNotification($taxi_request_details[0]->tripId,$taxi_request_details[0]->driverId,$taxi_request_details[0]->distance);
					$msg = $msg_data ['booking_success'];
				} else {
					sleep(1);
					$taxi_request_exists=NULL;
					// check taxi request for particular trip already exists or not
					$taxi_request_exists = $this->Taxi_Request_Details_Model->getByKeyValueArray ( array (
							'tripId' => $trip_id,
							'selectedDriverId>='=>Status_Type_Enum::ACTIVE,
							'taxiRequestStatus'=>Taxi_Request_Status_Enum::DRIVER_NOT_FOUND
					) );
					
					
					if (!$taxi_request_exists)
					{
					$msg = $msg_data ['driver_not_found'];
					$is_dispatched=0;
					}
				}
			} else if ($data ['tripType'] == Trip_Type_Enum::LATER) {
				$msg = $msg_data ['booking_success'];
				$is_dispatched=0;
			}
		}
		
		$response = array (
				'msg' => $msg,
				'trip_id' => $trip_id,
				'dispatch_status'=>$is_dispatched
		);
		echo json_encode ( $response );
		// $this->render("passenger/add_edit_passenger", $data);
	}
	public function getDetailsById($trip_id, $view_mode) {
		if ($view_mode == EDIT_MODE) {
			if ($this->User_Access_Model->writeModule ( $this->session->userdata ( 'role_type' ), Module_Name_Enum::TRIP ) === FALSE) {
				// redirect 404
				show_error ( "You do not have permission to view this page", '404' );
				return;
			}
		} else {
			if ($this->User_Access_Model->readModule ( $this->session->userdata ( 'role_type' ), Module_Name_Enum::TRIP ) === FALSE) {
				// redirect 404
				show_error ( "You do not have permission to view this page", '404' );
				return;
			}
		}
		$data = array ();
		$msg_data = $this->config->item ( 'msg' );
		$this->addJs ( 'app/trip/trip-table.js' );
		$this->addJs ( 'app/trip.js' );
		// $this->addCss ( 'app/colorbox.css' );
		$this->addJs ( 'app/trip/trip_pickuplocation.js' );
		$this->addJs ( 'app/trip/trip_droplocation.js' );
		if ($view_mode == 'view') {
			$this->addJs ( 'app/trip/trip_tracking.js' );
		}
		$data = array (
				'mode' => VIEW_MODE 
		);
		$data ['activetab'] = "trip";
		$data ['active_menu'] = 'trip';
		$data ['zone_list'] = $this->Zone_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE 
		), 'name' );
		$data ['sub_zone_list'] = $this->Zone_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE,
				'zoneId !=' => Status_Type_Enum::INACTIVE 
		), 'name' );
		$data ['entity_list'] = $this->Entity_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE 
		), 'name' );
		
		$data ['trip_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRIP_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		$data ['taxi_category_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TAXI_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		
		$data ['payment_mode_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'PAYMENT_MODE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		$data ['trip_status_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRIP_STATUS' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		if ($trip_id > 0) {
			if ($view_mode == EDIT_MODE) {
				$locked = $this->Trip_Details_Model->isLocked ( $trip_id );
				if ($locked) {
					$locked_user = $this->Trip_Details_Model->getLockedUser ( $trip_id );
					$locked_date = $this->Trip_Details_Model->getLockedDate ( $trip_id );
					$locked_date = date ( 'jS, F Y G:ia', strtotime ( $locked_date ) );
					$message = $msg_data ['locked'];
					$message = str_replace ( '##USERNAME##', $locked_user, $message );
					$message .= $locked_date;
					
					$data ['mode'] = VIEW_MODE;
				} else {
					$is_locked = $this->Trip_Details_Model->lock ( $trip_id );
				}
			}
			
			$data ['trip_model'] = $this->Trip_Details_Model->getById ( $trip_id );
			
			$data ['tt_pickupLatLong'] = @$data ['trip_model']->pickupLatitude . ',' . @$data ['trip_model']->pickupLongitude;
			// $data['tt_pickupLongitude'] = @$data ['trip_model']->pickupLongitude;
			$data ['tt_dropLatLong'] = @$data ['trip_model']->dropLatitude . ',' . @$data ['trip_model']->dropLongitude;
			// $data['tt_dropLongitude'] = @$data ['trip_model']->dropLongitude;
			$data ['passenger_model'] = $this->Passenger_Model->getBlankModel ();
			$data ['user_model'] = $this->User_Login_Details_Model->getBlankModel ();
			if ($data ['trip_model']->passengerId) {
				
				$data ['passenger_model'] = $this->Passenger_Model->getById ( $data ['trip_model']->passengerId );
			}
			if ($data ['trip_model']->createdBy) {
				
				$data ['user_model'] = $this->User_Login_Details_Model->getById ( $data ['trip_model']->createdBy );
			}
			
			$data ['driver_model'] = $this->Driver_Model->getBlankModel ();
			if ($data ['trip_model']->driverId) {
				$data ['driver_model'] = $this->Driver_Model->getById ( $data ['trip_model']->driverId );
			}
		} else {
			// Blank Model
			$data ['trip_model'] = $this->Trip_Details_Model->getBlankModel ();
			$data ['driver_model'] = $this->Driver_Model->getBlankModel ();
			$data ['passenger_model'] = $this->Passenger_Model->getBlankModel ();
			$data ['user_model'] = $this->User_Login_Details_Model->getBlankModel ();
		}
		
		if ($this->input->post ( 'id' )) {
			
			$html = $this->load->view ( 'trip/add_edit_trip', $data );
			echo json_encode ( array (
					'html' => $html 
			) );
		} else {
			$this->render ( 'trip/add_edit_trip', $data );
		}
	}
	public function getTriplist() {
		if ($this->User_Access_Model->showDriver () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		$data ['entity_list'] = $this->Entity_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE,
				'isDeleted' => Status_Type_Enum::INACTIVE 
		), 'name' );
		$data ['zone_list'] = $this->Zone_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE,
				'isDeleted' => Status_Type_Enum::INACTIVE 
		), 'name' );
		
		$data ['trip_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRIP_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		$data ['trip_status_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRIP_STATUS' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		$this->addJs ( 'app/trip/trip-table.js' );
		$this->addJs ( 'app/trip.js' );
		$data ['activetab'] = "trip";
		$data ['active_menu'] = 'trip';
		
		$this->render ( 'trip/manage_trip', $data );
	}
	public function ajax_list() {
		// echo"ajaxlist";exit;
		$start_date = NULL;
		$end_date = NULL;
		$trip_type = NULL;
		$trip_status = NULL;
		$zone_id = NULL;
		$entity_id = NULL;
		
		// Posted json data
		$post_data = $this->input->post ();
		$start_date = getCurrentDateTime ();
		if (array_key_exists ( 'start_date', $post_data ) && $post_data ['start_date'] != '') {
			$start_date = $post_data ['start_date'];
		}
		$end_date = $post_data ['end_date'];
		$trip_type = $post_data ['trip_type'];
		$trip_status = implode ( ',', $post_data ['trip_status'] );
		$entity_id = $post_data ['entity_id'];
		$zone_id = $post_data ['zone_id'];
		// debug_exit($trip_status);
		$sql_query = $this->Trip_Details_Model->getTripListQuery ( $start_date, $end_date, $trip_type, $trip_status, $zone_id, $entity_id );
		$trip_list = $this->Trip_Details_Model->getDataTable ( $sql_query );
		$data = array ();
		$no = $_POST ['start'];
		foreach ( $trip_list as $list ) {
			$no ++;
			
			$row = array ();
			$row [] = '
			
				  <button class="btn btn-sm btn-success" id="viewtrip-' . $list->tripId . '" title="View" tripstatus=".$list->tripStatus."><i class="fa fa-eye" aria-hidden="true"></i></button>';
			// <button class="btn btn-sm btn-blue" id="edittrip-'.$list->tripId.'" title="Edit" ><i class="fa fa-pencil-square-o"></i></button>';
			// <button class="btn btn-sm btn-danger" id="deletetrip-'.$list->tripId.'" title="Delete"><i class="fa fa-trash-o"></i></button>';
			$row [] = $list->tripStatusName;
			$row [] = "$list->jobCardId";
			$row [] = "$list->entityName";
			$row [] = $list->pickupLocation;
			$row [] = $list->pickupDatetime;
			$row [] = $list->dispatchedDatetime;
			$row [] = $list->driverAcceptedDatetime;
			$row [] = $list->taxiArrivedDatetime;
			$row [] = $list->actualPickupDatetime;
			$row [] = $list->dropLocation;
			$row [] = $list->taxiRegistrationNo;
			$row [] = $list->passengerName;
			$row [] = $list->passengerMobile;
			$row[] = $list->driverCode;
			$row [] = $list->driverName;
			$row [] = $list->driverMobile;
			$row [] = $list->taxiCategoryTypeName;
			$row [] = $list->tripTypeName;
			$row [] = ($list->bookedFrom == Signup_Type_Enum::BACKEND) ? $list->bookedFromName . ' (' . $list->userFirstName . ' ' . $list->userLastName . ')' : $list->bookedFromName;
			$row [] = $list->paymentModeName;
			$row [] = $list->promoCode;
			$row [] = $list->bookingZone;
			$data [] = $row;
			// add html for action
			
			
		}
		
		$output = array (
				"draw" => $_POST ['draw'],
				"recordsTotal" => $this->Trip_Details_Model->getDataTableAllCount ( $sql_query ),
				"recordsFiltered" => $this->Trip_Details_Model->getDataTableFilterCount ( $sql_query ),
				"data" => $data 
		);
		// output to json format
		echo json_encode ( $output );
	}
	public function driverTripReject() {
		if ($this->User_Access_Model->fullAccessModule ( $this->session->userdata ( 'role_type' ), Module_Name_Enum::TRIP ) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to change status", '404' );
			return;
		}
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		
		$message = '';
		
		$company_id = NULL;
		$reject_trip_id = NULL;
		$trip_status_update = NULL;
		$trip_request_status_update = NULL;
		$response_data = array ();
		$taxi_id = NULL;
		
		$driver_id = NULL;
		$reject_type = NULL;
		$trip_id = NULL;
		
		// Posted json data
		$post_data = $this->input->post ();
		
		$driver_id = $post_data ['driver_id'];
		$reject_type = $post_data ['reject_type'];
		$trip_id = $post_data ['trip_id'];
		
		// actual functionality
		if ($trip_id && $driver_id) {
			// To get the passenger id from trip id to update the the trip rejected by driver
			$trip_details = $this->Trip_Details_Model->getById ( $trip_id );
			
			// To update trip status on tripdetails
			$trip_status_update = $this->Trip_Details_Model->update ( array (
					'tripStatus' => Trip_Status_Enum::DRIVER_REJECTED_TRIP,
					'notificationStatus' => Trip_Status_Enum::DRIVER_REJECTED_TRIP,
					'driverId' => 0,
					'taxiId' => 0,
					'driverAcceptedDatetime' => 0,
					'driverAcceptedStatus' => Driver_Accepted_Status_Enum::REJECTED 
			), array (
					'id' => $trip_id,
					'driverId' => $driver_id 
			) );
			
			if ($trip_status_update) {
				// To update trip status on driverrequesttripdetails
				// to get rejected drivers for particular trip_id
				$rejected_driver_list = NULL;
				$rejected_drivers = $this->Taxi_Request_Details_Model->getOneByKeyValueArray ( array (
						'tripId' => $trip_id 
				) );
				
				if ($rejected_drivers) {
					if ($rejected_drivers->rejectedDriverList) {
						$rejected_driver_list = $rejected_drivers->rejectedDriverList;
						$rejected_driver_list .= ',' . $driver_id;
					} else {
						$rejected_driver_list = $driver_id;
					}
				}
				
				$trip_request_status_update = $this->Taxi_Request_Details_Model->update ( array (
						'taxiRequestStatus' => Taxi_Request_Status_Enum::DRIVER_REJECTED,
						'selectedDriverId' => 0,
						'rejectedDriverList' => $rejected_driver_list 
				), array (
						'tripId' => $trip_id 
				) );
			}
			if ($trip_request_status_update) {
				// to get last shift in status of driver before updating the shift out status
				$last_driver_shift_id = $this->Driver_Shift_History_Model->getOneByKeyValueArray ( array (
						'driverId' => $driver_id 
				), 'id DESC' );
				$driver_shift_insert_id = $this->Driver_Shift_History_Model->update ( array (
						'availabilityStatus' => Taxi_Available_Status_Enum::FREE 
				), array (
						'id' => $last_driver_shift_id->id,
						'driverId' => $driver_id 
				) );
				
				// Insert/update data to driverrejectedtripdetails table
				
				$insert_data = array (
						'tripId' => $trip_id,
						'driverId' => $driver_id,
						'taxiId' => $trip_details->taxiId,
						'passengerId' => $trip_details->passengerId,
						'rejectionType' => Driver_Accepted_Status_Enum::REJECTED 
				);
				$exist_rejected_trip = $this->Taxi_Rejected_Trip_Details_Model->getOneByKeyValueArray ( array (
						'tripId' => $trip_id,
						'driverId' => $driver_id 
				) );
				
				if (! $exist_rejected_trip) {
					$reject_trip_id = $this->Taxi_Rejected_Trip_Details_Model->insert ( $insert_data );
				} else {
					$update_data = array (
							'rejectionType' => Driver_Accepted_Status_Enum::REJECTED 
					);
					$reject_trip_id = $this->Taxi_Rejected_Trip_Details_Model->update ( $update_data, array (
							'tripId' => $trip_id,
							'driverId' => $driver_id 
					) );
				}
				// @todo Create Promocode for passenger DRT fllowed by Jobcard id
				$entity_config_details = $this->Entity_Config_Model->getOneByKeyValueArray ( array (
						'entityId' => $trip_details->entityId 
				) );
				$promo_end_datetime = date ( 'Y-m-d H:i:s', strtotime ( getCurrentDateTime () . ' + ' . $entity_config_details->drtPromoDaysLimit . ' days' ) );
				$insert_promocode_data = array (
						'promoCode' => 'DRT' . $trip_details->jobCardId,
						'passengerId' => $trip_details->passengerId,
						'zoneId' => $trip_details->zoneId,
						'entityId' => $trip_details->entityId,
						'promoDiscountAmount' => $entity_config_details->drtPromoAmount,
						'promoDiscountType' => Payment_Type_Enum::AMOUNT,
						'promocodeType' => Promocode_Type_Enum::GENERIC,
						'promoStartDatetime' => getCurrentDateTime (),
						'promoEndDatetime' => $promo_end_datetime,
						'promoCodeLimit' => 1,
						'promoCodeUserLimit' => 1 
				);
				$promocode_id = $this->Promocode_Details_Model->insert ( $insert_promocode_data );
				$promocode_details = $this->Promocode_Details_Model->getById ( $promocode_id );
				$entity_config_details = $this->Entity_Config_Model->getOneByKeyValueArray ( array (
						'entityId' => $trip_details->entityId 
				) );
				if ($entity_config_details) {
					if ($entity_config_details->sendSms) {
						// get passenger details
						$passenger_data = $this->Passenger_Model->getById ( $trip_details->passengerId );
						$driver_data = $this->Driver_Model->getById ( $trip_details->driverId );
						// Trip accept SMS to passenger with driver details
						$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
								'title' => 'drt_promocode_sms' 
						) );
						
						$message_data = $message_details->content;
						
						$message_data = str_replace ( "##PASSENGERNAME##", $passenger_data->firstName . ' ' . $passenger_data->lastName, $message_data );
						$message_data = str_replace ( "##PROMOCODE##", $promocode_details->promoCode, $message_data );
						$message_data = str_replace ( "##PROMOVALID##", $promocode_details->promoEndDatetime, $message_data );
						$message_data = str_replace ( "##PROMOVALUE##", $promocode_details->promoDiscountAmount, $message_data );
						if ($passenger_data->mobile != "") {
							
							$this->Common_Api_Webservice_Model->sendSMS ( $passenger_data->mobile, $message_data );
						}
					}
				}
				// end Promocode for passenger DRT fllowed by Jobcard id
				// @todo update driiver rejected count $ consecutive reject count of driver to driver dispath details table
				$driver_dispatch_exists = NULL;
				$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
						'driverId' => $driver_id 
				) );
				if ($driver_dispatch_exists) {
					
					$week_reject_count = NULL;
					$hour_reject_count = NULL;
					$week_reject_count = $this->Driver_Api_Webservice_Model->getWeekRejectedTripCount ( $driver_id );
					$hour_reject_count = $this->Driver_Api_Webservice_Model->getHourRejectedTripCount ( $driver_id );
					if ($week_reject_count) {
						$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
								'weekRejectedTrip' => $week_reject_count [0]->weekRejectedTripCount,
								'hourRejectedTrip'=>($hour_reject_count)?$hour_reject_count[0]->hourRejectedTripCount:1,
								'totalRejectedTrip' => ($driver_dispatch_exists->totalRejectedTrip + 1),
								'consecutiveRejectCount' => ($driver_dispatch_exists->consecutiveRejectCount + 1),
								'availabilityStatus' => Taxi_Available_Status_Enum::FREE,
								'isEligible' => Status_Type_Enum::ACTIVE,
								'tripId' => Status_Type_Enum::INACTIVE 
						), array (
								'driverId' => $driver_id 
						) );
					} else {
						$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
								'weekRejectedTrip' => 1,
								'hourRejectedTrip'=>1,
								'totalRejectedTrip' => ($driver_dispatch_exists->totalRejectedTrip + 1),
								'consecutiveRejectCount' => ($driver_dispatch_exists->consecutiveRejectCount + 1),
								'availabilityStatus' => Taxi_Available_Status_Enum::FREE,
								'isEligible' => Status_Type_Enum::ACTIVE,
								'tripId' => Status_Type_Enum::INACTIVE 
						), array (
								'driverId' => $driver_id 
						) );
					}
				} else {
					$insert_dispatch_data = array (
							'driverId' => $driver_id,
							'totalRejectedTrip' => 1,
							'weekRejectedTrip' => 1,
							'consecutiveRejectCount' => 1,
							'availabilityStatus' => Taxi_Available_Status_Enum::FREE,
							'isEligible' => Status_Type_Enum::ACTIVE,
							'tripId' => Status_Type_Enum::INACTIVE 
					);
					$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
				}
				// end driiver rejected count $ consecutive reject count of driver to driver dispath details table
				// check driver consecutive Reject Count
				$driver_consective_reject = NULL;
				$driver_consective_reject = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
						'driverId' => $driver_id 
				) );
				if ($driver_consective_reject->consecutiveRejectCount == $entity_config_details->drtFirstLimit) {
					// update the driver transaction by debiting the DRT penalty
					$current_driver_wallet_amount = $this->Driver_Model->getById ( $driver_id );
					$insert_driver_transaction = array (
							'driverId' => $driver_id,
							'tripId' => $trip_id,
							'transactionAmount' => $entity_config_details->drtFirstlLimitCharge,
							'previousAmount' => $current_driver_wallet_amount->driverWallet,
							'currentAmount' => ($current_driver_wallet_amount->driverWallet - $entity_config_details->drtFirstlLimitCharge),
							'transactionStatus' => 'Success',
							'transactionType' => Transaction_Type_Enum::TRIP_REJECT_PENALTY,
							'transactionFrom' => Transaction_From_Enum::WALLET_ACCOUNT,
							'transactionMode' => Transaction_Mode_Enum::DEBIT 
					);
					$update_driver_transaction = $this->Driver_Transaction_Details_Model->insert ( $insert_driver_transaction );
					$remaining_driver_wallet = $current_driver_wallet_amount->driverWallet - $admin_commission_amount;
					$update_driver_wallet = $this->Driver_Model->update ( array (
							'driverWallet' => $remaining_driver_wallet 
					), array (
							'id' => $driver_id 
					) );
					// @todo update driver dispath details table
					$driver_dispatch_exists = NULL;
					$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
							'driverId' => $driver_id 
					) );
					
					if ($driver_dispatch_exists) {
						$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
								'walletBalance' => $remaining_driver_wallet 
						), array (
								'driverId' => $driver_id 
						) );
					} else {
						$insert_dispatch_data = array (
								'driverId' => $driver_id,
								'walletBalance' => $remaining_driver_wallet 
						);
						$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
					}
					
					// end driver dispath details table
				}
				if ($driver_consective_reject->consecutiveRejectCount >= $entity_config_details->drtSecondLimit) {
					$change_driver_status = $this->Driver_Model->update ( array (
							'status' => Status_Type_Enum::INACTIVE 
					), array (
							'id' => $driver_id 
					) );
					// @todo update driver dispath details table
					$driver_dispatch_exists = NULL;
					$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
							'driverId' => $driver_id 
					) );
					
					if ($driver_dispatch_exists) {
						$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
								'availabilityStatus' => Taxi_Available_Status_Enum::Free,
								'isEligible' => Status_Type_Enum::ACTIVE,
								'status' => Status_Type_Enum::INACTIVE,
								'tripId' => Status_Type_Enum::INACTIVE 
						), array (
								'driverId' => $driver_id 
						) );
					} else {
						$insert_dispatch_data = array (
								'driverId' => $driver_id,
								'availabilityStatus' => Taxi_Available_Status_Enum::FREE,
								'isEligible' => Status_Type_Enum::ACTIVE,
								'status' => Status_Type_Enum::INACTIVE,
								'tripId' => Status_Type_Enum::INACTIVE 
						);
						$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
					}
					
					// end driver dispath details table
				}
				
				$message = array (
						"message" => $msg_data ['driver_rejected'],
						
						"status" => 1 
				);
			} else {
				$message = array (
						"message" => $msg_data ['trip_status_failed'] . '' . $trip_id,
						"status" => - 1 
				);
				// log_message ( 'debug', 'Failed to update trip status & trip request status for trip_id=' . $trip_id );
			}
		} else {
			$message = array (
					"message" => $msg_data ['invalid_trip'],
					"status" => - 1 
			);
		}
		
		echo json_encode ( $message );
	}
	public function driverTripRelease() {
		if ($this->User_Access_Model->fullAccessModule ( $this->session->userdata ( 'role_type' ), Module_Name_Enum::TRIP ) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to change status", '404' );
			return;
		}
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		
		$message = '';
		
		$company_id = NULL;
		$reject_trip_id = NULL;
		$trip_status_update = NULL;
		$trip_request_status_update = NULL;
		$response_data = array ();
		$taxi_id = NULL;
		
		$driver_id = NULL;
		$trip_id = NULL;
		
		// Posted json data
		$post_data = $this->input->post ();
		
		$driver_id = $post_data ['driver_id'];
		$trip_id = $post_data ['trip_id'];
		
		// actual functionality
		if ($trip_id && $driver_id) {
			// To get the passenger id from trip id to update the the trip rejected by driver
			$trip_details = $this->Trip_Details_Model->getById ( $trip_id );
			
			// To update trip status on tripdetails
			$trip_status_update = $this->Trip_Details_Model->update ( array (
					'tripStatus' => Trip_Status_Enum::DRIVER_NOT_FOUND,
					'notificationStatus' => Trip_Status_Enum::DRIVER_NOT_FOUND,
					'driverId' => 0,
					'taxiId' => 0 
			), array (
					'id' => $trip_id,
					'driverId' => $driver_id 
			) );
			
			if ($trip_status_update) {
				// To update trip status on driverrequesttripdetails
				
				$trip_request_status_update = $this->Taxi_Request_Details_Model->update ( array (
						'taxiRequestStatus' => Taxi_Request_Status_Enum::DRIVER_NOT_FOUND,
						'selectedDriverId' => 0 
				), array (
						'tripId' => $trip_id 
				) );
			}
			if ($trip_request_status_update) {
				// to get last shift in status of driver before updating the shift out status
				$last_driver_shift_id = $this->Driver_Shift_History_Model->getOneByKeyValueArray ( array (
						'driverId' => $driver_id 
				), 'id DESC' );
				$driver_shift_insert_id = $this->Driver_Shift_History_Model->update ( array (
						'availabilityStatus' => Taxi_Available_Status_Enum::FREE 
				), array (
						'id' => $last_driver_shift_id->id,
						'driverId' => $driver_id 
				) );
				
				// @todo update driiver dispath details table
				$driver_dispatch_exists = NULL;
				$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
						'driverId' => $driver_id 
				) );
				if ($driver_dispatch_exists) {
					
					$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
							'availabilityStatus' => Taxi_Available_Status_Enum::FREE,
							'isEligible' => Status_Type_Enum::ACTIVE,
							'tripId' => Status_Type_Enum::INACTIVE 
					), array (
							'driverId' => $driver_id 
					) );
				} else {
					$insert_dispatch_data = array (
							'availabilityStatus' => Taxi_Available_Status_Enum::FREE,
							'isEligible' => Status_Type_Enum::ACTIVE,
							'tripId' => Status_Type_Enum::INACTIVE 
					);
					$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
				}
				
				$message = array (
						"message" => $msg_data ['driver_rejected'],
						
						"status" => 1 
				);
			} else {
				$message = array (
						"message" => $msg_data ['trip_status_failed'] . '' . $trip_id,
						"status" => - 1 
				);
				// log_message ( 'debug', 'Failed to update trip status & trip request status for trip_id=' . $trip_id );
			}
		} else {
			$message = array (
					"message" => $msg_data ['invalid_trip'],
					"status" => - 1 
			);
		}
		
		echo json_encode ( $message );
	}
	public function tripStart() {
		if ($this->User_Access_Model->fullAccessModule ( $this->session->userdata ( 'role_type' ), Module_Name_Enum::TRIP ) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to change status", '404' );
			return;
		}
		
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		
		$message = '';
		$driver_logged_status = NULL;
		$check_trip_avilablity = NULL;
		$trip_status_update = NULL;
		$trip_request_status_update = NULL;
		$trip_tracking_update = NULL;
		
		$response_data = array ();
		
		$driver_id = NULL;
		$trip_id = NULL;
		
		// Posted json data
		$post_data = $this->input->post ();
		
		$driver_id = $post_data ['driver_id'];
		$trip_id = $post_data ['trip_id'];
		
		// actual functionality
		if ($driver_id) {
			
			// get the trip details
			$trip_details = $this->Trip_Details_Model->getById ( $trip_id );
			if ($trip_details) {
				// check trip avilable for driver/any in progress trip before logout
				$check_trip_avilablity = $this->Taxi_Request_Details_Model->getOneByKeyValueArray ( array (
						'tripId' => $trip_id 
				) );
				if ($check_trip_avilablity) {
					if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::DRIVER_ACCEPTED && $trip_details->tripStatus == Trip_Status_Enum::DRIVER_ARRIVED) {
						// getting valid & avilable promocode if trip is first ride for passenger & if no promocode applied by passenger
						$promocode = '';
						if (! $trip_details->promoCode) {
							$promocode = $this->Promocode_Details_Model->getFreeRidePromoCode ( $trip_details->passengerId );
						}
						
						// To update trip status on tripdetails
						$trip_status_update = $this->Trip_Details_Model->update ( array (
								'tripStatus' => Trip_Status_Enum::IN_PROGRESS,
								'notificationStatus' => Trip_Status_Enum::IN_PROGRESS,
								'actualPickupDatetime' => getCurrentDateTime (),
								'promoCode' => ($promocode) ? $promocode : '' 
						), array (
								'id' => $trip_id,
								'driverId' => $driver_id 
						) );
						
						if ($trip_status_update) {
							// To update trip status on taxirequesttripdetails
							$trip_request_status_update = $this->Taxi_Request_Details_Model->update ( array (
									'taxiRequestStatus' => Taxi_Request_Status_Enum::DRIVER_ACCEPTED 
							), array (
									'tripId' => $trip_id,
									'selectedDriverId' => $driver_id 
							) );
						}
						if ($trip_request_status_update) {
							
							$trip_details = $this->Common_Api_Webservice_Model->getTripDetails ( $trip_id );
							// send FCM/GCM notification to passenger trip started
							$this->tripNotification ( $trip_id, $trip_details [0]->passengerId, User_Type_Enum::PASSENGER, $msg_data ['trip_progress'], 3 );
							$message = array (
									"message" => $msg_data ['driver_start_trip'],
									"status" => 1 
							);
						} else {
							$message = array (
									"message" => $msg_data ['trip_status_failed'] . '' . $trip_id,
									"status" => - 1 
							);
							// log_message ( 'debug', 'Failed to update trip status & trip request status for trip_id=' . $trip_id );
						}
					} else if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::DRIVER_ACCEPTED && $trip_details->tripStatus == Trip_Status_Enum::IN_PROGRESS) {
						$message = array (
								"message" => $msg_data ['trip_started'],
								"status" => 2 
						);
					} 

					else {
						
						if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::PASSENGER_CANCELLED) {
							$message = array (
									"message" => $msg_data ['trip_reject_passenger'],
									"status" => - 1 
							);
						} else if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::COMPLETED_TRIP) {
							$message = array (
									"message" => $msg_data ['trip_completed'],
									"status" => - 1 
							);
						}
					}
				} else {
					$message = array (
							"message" => $msg_data ['invalid_trip_request'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['invalid_trip'],
						"status" => - 1 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['invalid_user'],
					"status" => - 1 
			);
		}
		
		echo json_encode ( $message );
	}
	public function tripEnd() {
		if ($this->User_Access_Model->fullAccessModule ( $this->session->userdata ( 'role_type' ), Module_Name_Enum::TRIP ) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to change status", '404' );
			return;
		}
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		
		$message = '';
		$driver_logged_status = NULL;
		$check_trip_avilablity = NULL;
		$trip_status_update = NULL;
		$trip_request_status_update = NULL;
		$rate_card_details = NULL;
		$rate_card_distance_slab_details = NULL;
		$rate_card_time_slab_details = NULL;
		$rate_card_surge_slab_details = NULL;
		$response_data = array ();
		$trip_tracking_update = NULL;
		$wallet_transaction_id = NULL;
		$driver_id = NULL;
		
		$trip_id = NULL;
		$travelled_distance = 0;
		$total_travelled_minute = 0;
		$invoice_no = '';
		
		// Posted json data
		$post_data = $this->input->post ();
		$trip_id = $post_data ['trip_id'];
		
		// actual functionality
		if ($trip_id > 0) {
			// get the trip details
			$trip_details = $this->Trip_Details_Model->getById ( $trip_id );
			$travelled_distance = $trip_details->estimatedDistance + 0.5;
			if ($trip_details) {
				$taxi_details = $this->Taxi_Details_Model->getById ( $trip_details->taxiId );
				// To get tax & admin commission entity details
				$entity_details = $this->Entity_Model->getOneByKeyValueArray ( array (
						'id' => $trip_details->entityId 
				) );
				
				$entity_config_details = $this->Entity_Config_Model->getOneByKeyValueArray ( array (
						'entityId' => $trip_details->entityId 
				) );
				
				if ($trip_details->driverId) {
					
					// get check wheather trip is allredy ended but waiting for payment
					if ($trip_details->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYMENT) {
						$message = array (
								"message" => $msg_data ['trip_waiting_payment'],
								"status" => 2 
						);
						echo json_encode ( $message );
						exit ();
					}
					// check trip avilable for driver/any in progress trip before logout
					$check_trip_avilablity = $this->Taxi_Request_Details_Model->getOneByKeyValueArray ( array (
							'tripId' => $trip_id 
					) );
					if ($check_trip_avilablity) {
						if ($check_trip_avilablity->taxiRequestStatus != Taxi_Request_Status_Enum::DRIVER_REJECTED && $check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::DRIVER_ACCEPTED) {
							// intialized for calculation purpose
							
							$actual_pickup_time = $trip_details->actualPickupDatetime;
							$actual_drop_time = getCurrentDateTime ();
							$travelled_hours = 0;
							$travelled_minutes = 0;
							$travelled_seconds = 0;
							$travel_hours_minutes = 0;
							$convenience_charge = 0;
							$travel_charge = 0;
							$total_trip_cost = 0;
							$promo_discount_amount = 0;
							$tax_amount = 0;
							$wallet_payment_amount = 0;
							$travelled_distance_charge = 0;
							$travelled_time_charge = 0;
							$admin_commission_amount = 0;
							$booking_charge = 0;
							$penalty_charge = 0;
							$surge_charge = 0;
							$parking_charge = 0;
							$toll_charge = 0;
							
							// get rate card details based on the cityId & trip type
							$rate_card_details = $this->Rate_Card_Details_Model->getOneByKeyValueArray ( array (
									'zoneId' => $trip_details->zoneId,
									'entityId' => $trip_details->entityId,
									'taxiCategoryType' => $taxi_details->taxiCategoryType,
									'status' => Status_Type_Enum::ACTIVE,
									'isDeleted' => Status_Type_Enum::INACTIVE 
							) );
							if (! $rate_card_details) {
								
								$rate_card_details = $this->Rate_Card_Details_Model->getOneByKeyValueArray ( array (
										'zoneId' => Status_Type_Enum::INACTIVE,
										'entityId' => $trip_details->entityId,
										'taxiCategoryType' => $trip_details->taxiCategoryType,
										'status' => Status_Type_Enum::ACTIVE,
										'isDeleted' => Status_Type_Enum::INACTIVE 
								) );
							}
							if (! $rate_card_details) {
								
								$rate_card_details = $this->Rate_Card_Details_Model->getOneByKeyValueArray ( array (
										'zoneId' => $trip_details->zoneId,
										'entityId' => Status_Type_Enum::INACTIVE,
										'taxiCategoryType' => $trip_details->taxiCategoryType,
										'status' => Status_Type_Enum::ACTIVE,
										'isDeleted' => Status_Type_Enum::INACTIVE 
								) );
							}
							if (! $rate_card_details) {
								
								$rate_card_details = $this->Rate_Card_Details_Model->getOneByKeyValueArray ( array (
										'zoneId' => Status_Type_Enum::INACTIVE,
										'entityId' => Status_Type_Enum::INACTIVE,
										'taxiCategoryType' => $trip_details->taxiCategoryType,
										'status' => Status_Type_Enum::ACTIVE,
										'isDeleted' => Status_Type_Enum::INACTIVE 
								) );
							}
							
							if (count ( $rate_card_details ) > 0) {
								if (strtotime ( $actual_pickup_time ) < strtotime ( $actual_drop_time )) {
									
									$dateDiff = intval ( (strtotime ( $actual_pickup_time ) - strtotime ( $actual_drop_time )) / 60 );
									$total_calculated_hours = round ( abs ( strtotime ( $actual_pickup_time ) - strtotime ( $actual_drop_time ) ) / 3600, 2 );
									$travelled_hours = intval ( abs ( $dateDiff / 60 ) );
									$travelled_minutes = intval ( abs ( $dateDiff % 60 ) );
									$travelled_seconds = intval ( abs ( (strtotime ( $actual_pickup_time ) - strtotime ( $actual_drop_time )) % 3600 ) % 60 );
									
									$travel_hours_minutes = $travelled_hours . '.' . $travelled_minutes;
									$total_travelled_minute = ($travelled_hours * 60) + $travelled_minutes;
									// to get convenience charge based pickup time day/night
									
									if (TimeIsBetweenTwoTimes ( DAY_START_TIME, DAY_END_TIME, $actual_pickup_time )) {
										
										$convenience_charge += $rate_card_details->dayConvenienceCharge;
									} else if (TimeIsBetweenTwoTimes ( NIGHT_START_TIME, NIGHT_END_TIME, $actual_pickup_time )) {
										$convenience_charge += $rate_card_details->nightConvenienceCharge;
									}
									$total_trip_cost += $convenience_charge;
									// to calculate the distance slab trip journey amount
									$rate_card_distance_slab_details = $this->Rate_Card_Slab_Details_Model->getByKeyValueArray ( array (
											'rateCardId' => $rate_card_details->id,
											'slabType' => Slab_Type_Enum::DISTANCE 
									) );
									if ($travelled_distance) {
										$available_travelled_distance = $travelled_distance;
										$calculated_travelled_distance = 0;
										$calculate_to_distance = 0;
										foreach ( $rate_card_distance_slab_details as $rate_distance ) {
											if ($available_travelled_distance) {
												if ($rate_distance->slabEnd) {
													
													if (! $rate_distance->slabStart) {
														$calculate_to_distance = $rate_distance->slabEnd;
													} else {
														$calculate_to_distance = ($rate_distance->slabEnd - $rate_distance->slabStart);
													}
													$calculated_travelled_distance += $calculate_to_distance;
													// $available_travelled_distance -= $calculate_to_distance;
													if ($available_travelled_distance <= $calculate_to_distance) {
														$calculate_to_distance = $available_travelled_distance;
														$available_travelled_distance = 0;
													} else {
														$available_travelled_distance -= $calculate_to_distance;
													}
												} else if ($rate_distance->slabStart && ! $rate_distance->slabEnd) {
													$calculate_to_distance = $available_travelled_distance;
													
													$calculated_travelled_distance += $calculate_to_distance;
													$available_travelled_distance -= $calculate_to_distance;
												}
												
												if ($calculate_to_distance) {
													if ($rate_distance->slabCharge) {
														
														$travelled_distance_charge += $calculate_to_distance * $rate_distance->slabCharge;
														$calculate_to_distance = 0;
													}
												}
											}
										}
									}
									// /add to travel charge
									$travel_charge += $travelled_distance_charge;
									
									// to calculate the time slab trip journey amount
									$rate_card_time_slab_details = $this->Rate_Card_Slab_Details_Model->getByKeyValueArray ( array (
											'rateCardId' => $rate_card_details->id,
											'slabType' => Slab_Type_Enum::TIME 
									) );
									if ($total_travelled_minute) {
										
										$available_travelled_minute = $total_travelled_minute;
										$calculated_travelled_minute = 0;
										$calculate_to_minute = 0;
										foreach ( $rate_card_time_slab_details as $rate_time ) {
											if ($available_travelled_minute) {
												if ($rate_time->slabEnd) {
													
													if (! $rate_time->slabStart) {
														$calculate_to_minute = $rate_time->slabEnd;
													} else {
														$calculate_to_minute = ($rate_time->slabEnd - $rate_time->slabStart);
													}
													$calculated_travelled_minute += $calculate_to_minute;
													// $available_travelled_distance -= $calculate_to_distance;
													if ($available_travelled_minute <= $calculate_to_minute) {
														$calculate_to_minute = $available_travelled_minute;
														$available_travelled_minute = 0;
													} else {
														$available_travelled_minute -= $calculate_to_minute;
													}
												} else if ($rate_time->slabStart && ! $rate_time->slabEnd) {
													$calculate_to_minute = $available_travelled_minute;
													
													$calculated_travelled_minute += $calculate_to_minute;
													$available_travelled_minute -= $calculate_to_minute;
												}
												
												if ($calculate_to_minute) {
													if ($rate_time->slabCharge) {
														
														$travelled_time_charge += $calculate_to_minute * $rate_time->slabCharge;
														$calculate_to_minute = 0;
													}
												}
											}
										}
									}
									
									// /add to travel charge
									$travel_charge += $travelled_time_charge;
									
									// to calculate the surge slab trip journey amount
									$rate_card_surge_slab_details = $this->Rate_Card_Slab_Details_Model->getByKeyValueArray ( array (
											'rateCardId' => $rate_card_details->id,
											'slabType' => Slab_Type_Enum::SURGE 
									) );
									if ($rate_card_surge_slab_details) {
										foreach ( $rate_card_surge_slab_details as $rate_surge ) {
											$slab_start_time = $rate_surge->slabStart;
											$slab_end_time = $rate_surge->slabEnd;
											// $user_st=$user_et="2017-08-02 09:29:12";
											if (((strtotime ( $actual_pickup_time ) >= strtotime ( $slab_start_time )) && (strtotime ( $actual_pickup_time ) <= strtotime ( $slab_end_time ))) || (strtotime ( $actual_pickup_time ) >= strtotime ( $slab_start_time )) && (strtotime ( $actual_pickup_time ) <= strtotime ( $slab_end_time ))) {
												if ($rate_surge->slabChargeType == Payment_Type_Enum::AMOUNT) {
													$surge_charge = $rate_surge->slabCharge;
													$total_trip_cost += $surge_charge;
													break;
												} else if ($rate_surge->slabChargeType == Payment_Type_Enum::PERCENTAGE) {
													$surge_charge = ($rate_surge->slabCharge / 100) * ($convenience_charge + $travel_charge);
													$surge_charge = round ( $surge_charge, 2 );
													$total_trip_cost += $surge_charge;
													break;
												}
											}
										}
									}
									$travel_charge += $surge_charge;
									$total_trip_cost += $travel_charge;
									// debug_exit($total_trip_cost);
									// tax calculation based on data of entity config details info table
									if ($total_trip_cost > 0) {
										$tax = $entity_config_details->tax;
										if ($tax > 0) {
											$tax_amount = ($tax / 100) * $total_trip_cost;
											$tax_amount = round ( $tax_amount, 2 );
											$total_trip_cost += $tax_amount;
										}
									}
									// calculation booking charge if booking from admib panel based on entity config details info table
									if ($trip_details->bookedFrom == Signup_Type_Enum::BACKEND) {
										$booking_charge = $entity_config_details->adminBookingCharge;
										$total_trip_cost += $booking_charge;
									}
									// get passenger existing trip reject count to calucalate penalty and reset the penalty reject trip count
									$passenger_details = $this->Passenger_Model->getById ( $trip_details->passengerId );
									if ($passenger_details->penaltyTripCount) {
										$penalty_charge = $passenger_details->penaltyTripCount * $entity_config_details->passengerRejectionCharge;
										$passenger_updated = $this->Passenger_Model->update ( array (
												'penaltyTripCount' => 0 
										), array (
												'id' => $trip_details->passengerId 
										) );
										$total_trip_cost += $penalty_charge;
										// set variable to NULL because it reused below
										$passenger_details = NULL;
										$passenger_updated = NULL;
									}
									
									// $total_trip_cost = $convenience_charge + $travel_charge;
									// calculate promocode discount if applicable
									if (! empty ( $trip_details->promoCode ) && $trip_details->promoCode != '') {
										
										// Check promocode exists or not for particular passenger at particular city
										$promocode_exists = $this->Common_Api_Webservice_Model->checkPromocodeAvailablity ( $trip_details->promoCode, $trip_details->passengerId, $trip_details->zoneId,$trip_details->dropZoneId,$trip_details->entityId );
										
										if ($promocode_exists && count ( $promocode_exists ) > 0) {
											if (strtotime ( $promocode_exists [0]->promoStartDate ) < getCurrentUnixDateTime () && strtotime ( $promocode_exists [0]->promoEndDate ) > getCurrentUnixDateTime ()) {
												
												// check promocode limit exceeds or not for specific passenger (note: only completed trip)
												$promocode_user_limit_exists = $this->Trip_Details_Model->getByKeyValueArray ( array (
														'passengerId' => $trip_details->passengerId,
														'promoCode' => $trip_details->promoCode,
														'tripStatus' => Trip_Status_Enum::TRIP_COMPLETED 
												) );
												// check promocode limit exceeds or not total limit (note: only completed trip)
												$promocode_limit_exists = $this->Trip_Details_Model->getUsedPromocodeLimitCount ($trip_details->promoCode);
												
												if ((count ( $promocode_limit_exists ) < $promocode_exists [0]->promoCodeLimit) && (count ( $promocode_user_limit_exists ) < $promocode_exists [0]->promoCodeUserLimit)) {
													// get promo code details
													$promocode_details = $this->Promocode_Details_Model->getOneByKeyValueArray ( array (
															'promoCode' => $trip_details->promoCode 
													) );
													
													$promo_discount = $promocode_details->promoDiscountAmount;
													$promo_type = $promocode_details->promoDiscountType;
													if ($promo_type == Payment_Type_Enum::PERCENTAGE) {
														// discount in %
														$calculate_amt = ($promo_discount / 100) * $total_trip_cost;
														$promo_discount_amount = round ( $calculate_amt, 2 );
														$total_trip_cost = $total_trip_cost - $promo_discount_amount;
														$promo_discount = $promo_discount_amount;
													} else if ($promo_type == Payment_Type_Enum::AMOUNT) {
														// discount in Rs
														if ($promo_discount > $total_trip_cost) {
															
															$promo_discount_amount = $total_trip_cost;
															$promo_discount = $promo_discount_amount;
														} else {
															
															$promo_discount_amount = $promo_discount;
														}
														$total_trip_cost = $total_trip_cost - $promo_discount;
													}
													
													if ($total_trip_cost < 0) {
														$total_trip_cost = 0;
													}
												}
											}
										}
									}
									// deduction from wallet if payment mode is wallet
									if ($trip_details->paymentMode == Payment_Mode_Enum::WALLET) {
										
										if ($total_trip_cost > 0) {
											// get current wallet amount of passenger
											$current_wallet_amount = $this->Passenger_Model->getById ( $trip_details->passengerId );
											
											if ($total_trip_cost >= $current_wallet_amount->walletAmount) {
												$wallet_payment_amount = $current_wallet_amount->walletAmount;
												// $total_trip_cost = $total_trip_cost - $current_wallet_amount->walletAmount;
												$remaing_wallet_amount ['walletAmount'] = 0;
											} else {
												$actual_wallet_amount = $current_wallet_amount->walletAmount;
												
												$current_wallet_amount->walletAmount = $current_wallet_amount->walletAmount - $total_trip_cost;
												$wallet_payment_amount = $total_trip_cost;
												$remaing_wallet_amount ['walletAmount'] = $current_wallet_amount->walletAmount;
												// $total_trip_cost = 0;
											}
											$wallet_payment_update = $this->Passenger_Model->update ( $remaing_wallet_amount, array (
													'id' => $trip_details->passengerId 
											) );
										}
									}
									// get current wallet amount of driver to debit admin commisiion charge
									$current_driver_wallet_amount = $this->Driver_Model->getById ( $trip_details->driverId );
									
									// calculate admin commision
									$total_bill_amount = $convenience_charge + $travel_charge;
									$admin_commission_amount = ($total_bill_amount / 100) * $entity_config_details->adminCommission;
									$admin_commission_amount = round ( $admin_commission_amount, 2 );
									
									// update the driver transaction by debiting the admin commisiion charge
									$insert_driver_transaction = array (
											'driverId' => $trip_details->driverId,
											'tripId' => $trip_id,
											'transactionAmount' => $admin_commission_amount,
											'previousAmount' => $current_driver_wallet_amount->driverWallet,
											'currentAmount' => ($current_driver_wallet_amount->driverWallet - $admin_commission_amount),
											'transactionStatus' => 'Success',
											'transactionType' => Transaction_Type_Enum::TRIP_COMMISSION,
											'transactionFrom' => Transaction_From_Enum::WALLET_ACCOUNT,
											'transactionMode' => Transaction_Mode_Enum::DEBIT 
									);
									$update_driver_transaction = $this->Driver_Transaction_Details_Model->insert ( $insert_driver_transaction );
									$remaining_driver_wallet = $current_driver_wallet_amount->driverWallet - $admin_commission_amount;
									$update_driver_wallet = $this->Driver_Model->update ( array (
											'driverWallet' => $remaining_driver_wallet 
									), array (
											'id' => $trip_details->driverId 
									) );
									// @todo update driver dispath details table
									$driver_dispatch_exists = NULL;
									$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
											'driverId' => $trip_details->driverId 
									) );
									
									if ($driver_dispatch_exists) {
										$avg_distance = round ( ($driver_dispatch_exists->avgTripDistance + $travelled_distance) / ($driver_dispatch_exists->totalCompletedTrip + 1), 2 );
										$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
												'walletBalance' => $remaining_driver_wallet,
												'avgTripDistance' => $avg_distance 
										), array (
												'driverId' => $trip_details->driverId 
										) );
									} else {
										$insert_dispatch_data = array (
												'driverId' => $trip_details->driverId,
												'walletBalance' => $remaining_driver_wallet,
												'avgTripDistance' => $travelled_distance 
										);
										$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
									}
									
									// end driver dispath details table
									
									if ($promo_discount_amount) {
										sleep ( 1 );
										// get current wallet amount of driver to credit promocode ammount
										$current_driver_credit_amount = $this->Driver_Model->getById ( $trip_details->driverId );
										
										// update the driver transaction by credit promocode ammount
										$insert_driver_transaction = array (
												'driverId' => $trip_details->driverId,
												'tripId' => $trip_id,
												'transactionAmount' => $promo_discount_amount,
												'previousAmount' => $current_driver_credit_amount->driverCredit,
												'currentAmount' => ($current_driver_credit_amount->driverCredit + $promo_discount_amount),
												'transactionStatus' => 'Success',
												'transactionType' => Transaction_Type_Enum::PROMOCODE,
												'transactionFrom' => Transaction_From_Enum::CREDIT_ACCOUNT,
												'transactionMode' => Transaction_Mode_Enum::CREDIT 
										);
										$update_driver_transaction = $this->Driver_Transaction_Details_Model->insert ( $insert_driver_transaction );
										$remaining_driver_credit = $current_driver_credit_amount->driverCredit + $promo_discount_amount;
										$update_driver_wallet = $this->Driver_Model->update ( array (
												'driverCredit' => $remaining_driver_credit 
										), array (
												'id' => $trip_details->driverId 
										) );
									}
									if ($tax_amount) {
										sleep ( 1 );
										// get current wallet amount of driver to credit promocode ammount
										$current_driver_wallet_amount = $this->Driver_Model->getById ( $trip_details->driverId );
										
										// update the driver transaction by credit promocode ammount
										$insert_driver_transaction = array (
												'driverId' => $trip_details->driverId,
												'tripId' => $trip_id,
												'transactionAmount' => $tax_amount,
												'previousAmount' => $current_driver_wallet_amount->driverWallet,
												'currentAmount' => ($current_driver_wallet_amount->driverWallet - $tax_amount),
												'transactionStatus' => 'Success',
												'transactionType' => Transaction_Type_Enum::TAX_AMOUNT,
												'transactionFrom' => Transaction_From_Enum::WALLET_ACCOUNT,
												'transactionMode' => Transaction_Mode_Enum::DEBIT 
										);
										$update_driver_transaction = $this->Driver_Transaction_Details_Model->insert ( $insert_driver_transaction );
										$remaining_driver_wallet = $current_driver_wallet_amount->driverWallet - $tax_amount;
										$update_driver_wallet = $this->Driver_Model->update ( array (
												'driverWallet' => $remaining_driver_wallet 
										), array (
												'id' => $trip_details->driverId 
										) );
										
										// @todo update driver dispath details table
										$driver_dispatch_exists = NULL;
										$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
												'driverId' => $trip_details->driverId 
										) );
										
										if ($driver_dispatch_exists) {
											
											$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
													'walletBalance' => $remaining_driver_wallet 
											), array (
													'driverId' => $trip_details->driverId 
											) );
										} else {
											$insert_dispatch_data = array (
													'driverId' => $trip_details->driverId,
													'walletBalance' => $remaining_driver_wallet 
											);
											$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
										}
										
										// end driver dispath details table
									}
									if ($booking_charge) {
										sleep ( 1 );
										// get current wallet amount of driver to credit promocode ammount
										$current_driver_wallet_amount = $this->Driver_Model->getById ( $trip_details->driverId );
										
										// update the driver transaction by credit promocode ammount
										$insert_driver_transaction = array (
												'driverId' => $trip_details->driverId,
												'tripId' => $trip_id,
												'transactionAmount' => $booking_charge,
												'previousAmount' => $current_driver_wallet_amount->driverWallet,
												'currentAmount' => ($current_driver_wallet_amount->driverWallet - $booking_charge),
												'transactionStatus' => 'Success',
												'transactionType' => Transaction_Type_Enum::BOOKING_CHARGE,
												'transactionFrom' => Transaction_From_Enum::WALLET_ACCOUNT,
												'transactionMode' => Transaction_Mode_Enum::DEBIT 
										);
										$update_driver_transaction = $this->Driver_Transaction_Details_Model->insert ( $insert_driver_transaction );
										$remaining_driver_wallet = $current_driver_wallet_amount->driverWallet - $booking_charge;
										$update_driver_wallet = $this->Driver_Model->update ( array (
												'driverWallet' => $remaining_driver_wallet 
										), array (
												'id' => $trip_details->driverId 
										) );
										
										// @todo update driver dispath details table
										$driver_dispatch_exists = NULL;
										$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
												'driverId' => $trip_details->driverId 
										) );
										
										if ($driver_dispatch_exists) {
											
											$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
													'walletBalance' => $remaining_driver_wallet 
											), array (
													'driverId' => $trip_details->driverId 
											) );
										} else {
											$insert_dispatch_data = array (
													'driverId' => $trip_details->driverId,
													'walletBalance' => $remaining_driver_wallet 
											);
											$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
										}
										
										// end driver dispath details table
									}
									if ($penalty_charge) {
										sleep ( 1 );
										// get current wallet amount of driver to credit promocode ammount
										$current_driver_wallet_amount = $this->Driver_Model->getById ( $trip_details->driverId );
										
										// update the driver transaction by credit promocode ammount
										$insert_driver_transaction = array (
												'driverId' => $trip_details->driverId,
												'tripId' => $trip_id,
												'transactionAmount' => $penalty_charge,
												'previousAmount' => $current_driver_wallet_amount->driverWallet,
												'currentAmount' => ($current_driver_wallet_amount->driverWallet - $penalty_charge),
												'transactionStatus' => 'Success',
												'transactionType' => Transaction_Type_Enum::PASSENGER_PENALTY_CHARGE,
												'transactionFrom' => Transaction_From_Enum::WALLET_ACCOUNT,
												'transactionMode' => Transaction_Mode_Enum::DEBIT 
										);
										$update_driver_transaction = $this->Driver_Transaction_Details_Model->insert ( $insert_driver_transaction );
										$remaining_driver_wallet = $current_driver_wallet_amount->driverWallet - $penalty_charge;
										$update_driver_wallet = $this->Driver_Model->update ( array (
												'driverWallet' => $remaining_driver_wallet 
										), array (
												'id' => $trip_details->driverId 
										) );
										
										// @todo update driver dispath details table
										$driver_dispatch_exists = NULL;
										$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
												'driverId' => $trip_details->driverId 
										) );
										
										if ($driver_dispatch_exists) {
											
											$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
													'walletBalance' => $remaining_driver_wallet 
											), array (
													'driverId' => $trip_details->driverId 
											) );
										} else {
											$insert_dispatch_data = array (
													'driverId' => $trip_details->driverId,
													'walletBalance' => $remaining_driver_wallet 
											);
											$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
										}
										
										// end driver dispath details table
									}
									
									if ($wallet_payment_amount) {
										sleep ( 1 );
										// get current wallet amount of driver to credit wallet payment ammount
										$current_driver_credit_amount = $this->Driver_Model->getById ( $trip_details->driverId );
										
										// update the driver transaction by credit promocode ammount
										$insert_driver_transaction = array (
												'driverId' => $trip_details->driverId,
												'tripId' => $trip_id,
												'transactionAmount' => $wallet_payment_amount,
												'previousAmount' => $current_driver_credit_amount->driverCredit,
												'currentAmount' => ($current_driver_credit_amount->driverCredit + $wallet_payment_amount),
												'transactionStatus' => 'Success',
												'transactionType' => Transaction_Type_Enum::TRIP_WALLET,
												'transactionFrom' => Transaction_From_Enum::CREDIT_ACCOUNT,
												'transactionMode' => Transaction_Mode_Enum::CREDIT 
										);
										$update_driver_transaction = $this->Driver_Transaction_Details_Model->insert ( $insert_driver_transaction );
										$remaining_driver_credit = $current_driver_credit_amount->driverCredit + $wallet_payment_amount;
										$update_driver_wallet = $this->Driver_Model->update ( array (
												'driverCredit' => $remaining_driver_credit 
										), array (
												'id' => $trip_details->driverId 
										) );
									}
									// update the trip details trip status to waiting for payment ,driver shift status to free also trip request details trip status to completed
									
									$trip_status = NULL;
									$notification_status = NULL;
									if ($trip_details->paymentMode == Payment_Mode_Enum::WALLET || $trip_details->paymentMode == Payment_Mode_Enum::CASH) {
										$trip_status = Trip_Status_Enum::WAITING_FOR_PAYMENT;
										$notification_status = Trip_Status_Enum::WAITING_FOR_PAYMENT;
									} else if ($trip_details->paymentMode == Payment_Mode_Enum::PAYTM) {
										$trip_status = Trip_Status_Enum::NO_NOTIFICATION;
										$notification_status = Trip_Status_Enum::NO_NOTIFICATION;
									} else if ($trip_details->paymentMode == Payment_Mode_Enum::INVOICE) {
										$trip_status = Trip_Status_Enum::WAITING_FOR_PAYMENT;
										$notification_status = Trip_Status_Enum::WAITING_FOR_PAYMENT;
									}
									$trip_status_update = $this->Trip_Details_Model->update ( array (
											'tripStatus' => $trip_status,
											'notificationStatus' => $notification_status,
											'dropDatetime' => $actual_drop_time 
									), array (
											'id' => $trip_id 
									) );
									
									$is_first_ride = $this->Trip_Details_Model->getByKeyValueArray ( array (
											'passengerId' => $trip_details->passengerId,
											'tripStatus' => Trip_Status_Enum::TRIP_COMPLETED 
									), 'id' );
									if (count ( $is_first_ride ) <= 0) {
										$passenger_referral_details = $this->Referral_Details_Model->getOneByKeyValueArray ( array (
												'registeredId' => $trip_details->passengerId 
										), 'id DESC' );
										if ($passenger_referral_details) {
											$passenger_referral_update = $this->Referral_Details_Model->update ( array (
													'registeredTripId' => $trip_id,
													'isReferrerEarned' => Status_Type_Enum::ACTIVE 
											), array (
													'registeredId' => $trip_details->passengerId 
											) );
											$passenger_details = $this->Passenger_Model->getById ( $passenger_referral_details->passengerId );
											$current_balance = $passenger_details->walletAmount + $passenger_referral_details->earnedAmount;
											$insert_data = array (
													'passengerId' => $passenger_referral_details->passengerId,
													'passengerReferralId' => $trip_details->passengerId,
													'tripId' => $trip_id,
													'transactionAmount' => $passenger_referral_details->earnedAmount,
													'previousAmount' => $passenger_details->walletAmount,
													'currentAmount' => $current_balance,
													'transactionStatus' => 'Success',
													'transactionType' => Transaction_Type_Enum::REFERRAL,
													'transactionFrom' => Transaction_From_Enum::WALLET_ACCOUNT,
													'manipulationType' => Transaction_Mode_Enum::CREDIT 
											);
											$passenger_wallet_updated = $this->Passenger_Transaction_Details_Model->insert ( $insert_data );
											$passenger_updated = $this->Passenger_Model->update ( array (
													'walletAmount' => $current_balance 
											), array (
													'id' => $passenger_referral_details->passengerId 
											) );
										}
									}
									// //Tripfare Update Functionality Start
									
									$total_trip_cost_insert = $total_trip_cost + $parking_charge + $toll_charge;
									
									$driver_earning = round ( (($convenience_charge + $travel_charge) - $admin_commission_amount), 2 );
									
									$passenger_details = $this->Passenger_Model->getById ( $trip_details->passengerId );
									
									$insert_data = array (
											'tripId' => $trip_id,
											'travelledDistance' => $travelled_distance,
											'travelledPeriod' => $total_travelled_minute,
											'convenienceCharge' => $convenience_charge,
											'travelCharge' => $travel_charge,
											'parkingCharge' => $parking_charge,
											'tollCharge' => $toll_charge,
											'taxCharge' => $tax_amount,
											'bookingCharge' => $booking_charge,
											'penaltyCharge' => $penalty_charge,
											'surgeCharge' => $surge_charge,
											'totalTripCharge' => $total_trip_cost_insert,
											'adminAmount' => $admin_commission_amount,
											'promoDiscountAmount' => $promo_discount_amount,
											'walletPaymentAmount' => $wallet_payment_amount,
											'taxPercentage' => $entity_config_details->tax,
											'driverEarning' => $driver_earning,
											'tripType' => $trip_details->tripType,
											'paymentMode' => $trip_details->paymentMode,
											'paymentStatus' => ($trip_details->paymentMode == Payment_Mode_Enum::INVOICE) ? '' : (($trip_details->paymentMode != Payment_Mode_Enum::AGD) ? 'Success' : 'Waiting for PAYTM payment'),
											'transactionId' => 0 
									);
									
									// to check transcation table exits trip record
									$transaction_exists = $this->Trip_Transaction_Details_Model->getOneByKeyValueArray ( array (
											'tripId' => $trip_id 
									) );
									if ($transaction_exists) {
										// update the transaction table
										
										$transaction_insert_id = $this->Trip_Transaction_Details_Model->update ( $insert_data, array (
												'id' => $transaction_exists->id 
										) );
										$transaction_insert_id = $transaction_exists->id;
									} else {
										// insert data to transaction table
										$transaction_insert_id = $this->Trip_Transaction_Details_Model->insert ( $insert_data );
										if ($transaction_insert_id) {
											$invoice_id = ($transaction_insert_id <= 999999) ? str_pad ( ( string ) $transaction_insert_id, 6, '0', STR_PAD_LEFT ) : $transaction_insert_id;
											$invoice_no = 'INV' . $invoice_id;
											$update_invoice_no = $this->Trip_Transaction_Details_Model->update ( array (
													'invoiceNo' => $invoice_no 
											), array (
													'id' => $transaction_insert_id 
											) );
										}
									}
									if ($transaction_insert_id) {
										
										// passenger transaction history
										$transaction_amount = $total_trip_cost_insert - $wallet_payment_amount;
										if ($transaction_amount > 0 && $passenger_details) {
											$insert_wallet_transaction = array (
													'passengerId' => $passenger_details->id,
													'tripId' => $trip_id,
													'transactionAmount' => $transaction_amount,
													'transactionStatus' => 'Success',
													'transactionType' => Transaction_Type_Enum::TRIP,
													'transactionFrom' => Transaction_From_Enum::CASH,
													'manipulationType' => Transaction_Mode_Enum::DEBIT 
											);
											$wallet_transaction_id = $this->Passenger_Transaction_Details_Model->insert ( $insert_wallet_transaction );
										}
										if ($wallet_payment_amount > 0 && $passenger_details) {
											$current_amount = $passenger_details->walletAmount - $wallet_payment_amount;
											$insert_wallet_transaction = array (
													'passengerId' => $passenger_details->id,
													'tripId' => $trip_id,
													'transactionAmount' => $wallet_payment_amount,
													'previousAmount' => $passenger_details->walletAmount,
													'currentAmount' => $current_amount,
													'transactionStatus' => 'Success',
													'transactionType' => Transaction_Type_Enum::TRIP,
													'transactionFrom' => Transaction_From_Enum::WALLET_ACCOUNT,
													'manipulationType' => Transaction_Mode_Enum::DEBIT 
											);
											$wallet_transaction_id = $this->Passenger_Transaction_Details_Model->insert ( $insert_wallet_transaction );
										}
										// get the trip details
										$trip_details = $this->Trip_Details_Model->getById ( $trip_id );
										
										$trip_status = NULL;
										$notification_status = NULL;
										if ($trip_details->paymentMode == Payment_Mode_Enum::WALLET || $trip_details->paymentMode == Payment_Mode_Enum::CASH) {
											$trip_status = Trip_Status_Enum::TRIP_COMPLETED;
											$notification_status = Trip_Status_Enum::NO_NOTIFICATION;
										} else if ($trip_details->paymentMode == Payment_Mode_Enum::AGD) {
											$trip_status = Trip_Status_Enum::WAITING_FOR_PAYMENT;
											$notification_status = Trip_Status_Enum::WAITING_FOR_PAYMENT;
										} else if ($trip_details->paymentMode == Payment_Mode_Enum::INVOICE) {
											$trip_status = Trip_Status_Enum::TRIP_COMPLETED;
											$notification_status = Trip_Status_Enum::TRIP_COMPLETED;
										}
										
										if ($trip_details->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYMENT || $trip_details->tripStatus == Trip_Status_Enum::NO_NOTIFICATION) {
											$trip_status_update = $this->Trip_Details_Model->update ( array (
													'tripStatus' => $trip_status,
													'notificationStatus' => $notification_status 
											), array (
													'id' => $trip_id 
											) );
											
											$trip_request_status_update = $this->Taxi_Request_Details_Model->update ( array (
													'taxiRequestStatus' => Taxi_Request_Status_Enum::COMPLETED_TRIP,
													'selectedDriverId' => 0 
											), array (
													'tripId' => $trip_id 
											) );
											// get last inserted shift id for particular driver
											$get_last_shift_id = $this->Driver_Shift_History_Model->getOneByKeyValueArray ( array (
													'driverId' => $trip_details->driverId 
											), 'id DESC' );
											$driver_shift_status_update = $this->Driver_Shift_History_Model->update ( array (
													'availabilityStatus' => Taxi_Available_Status_Enum::FREE 
											), array (
													'id' => $get_last_shift_id->id 
											) );
										}
										// response data
										$response_data = array (
												"total_trip_cost" => $total_trip_cost,
												"pickup_location" => $trip_details->pickupLocation,
												"invoice_no" => $invoice_no,
												"trip_id" => $trip_id 
										);
										
										//
										
										// @todo update driver dispath details table
										$driver_dispatch_exists = NULL;
										$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
												'driverId' => $trip_details->driverId 
										) );
										if ($driver_dispatch_exists) {
											$hour_completed_count=NULL;
												$week_completed_count = $this->Driver_Api_Webservice_Model->getWeekCompletedTripCount ( $trip_details->driverId );
												$hour_completed_count = $this->Driver_Api_Webservice_Model->getHourCompletedTripCount ( $trip_details->driverId );
											if ($week_completed_count) {
												$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
														'weekCompletedTrip' => $week_completed_count [0]->weekCompletedTripCount,
														'hourCompletedTrip'=>($hour_completed_count)?$hour_completed_count [0]->hourCompletedTripCount:1,
														'totalCompletedTrip' => ($driver_dispatch_exists->totalCompletedTrip + 1),
														'consecutiveRejectCount' => 0,
														'availabilityStatus' => Taxi_Available_Status_Enum::FREE,
														'isEligible' => Status_Type_Enum::ACTIVE,
														'tripId' => Status_Type_Enum::INACTIVE 
												), array (
														'driverId' => $trip_details->driverId 
												) );
											} else {
												$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
														'weekCompletedTrip' => 1,
														'hourCompletedTrip'=>1,
														'totalCompletedTrip' => ($driver_dispatch_exists->totalCompletedTrip + 1),
														'consecutiveRejectCount' => 0,
														'availabilityStatus' => Taxi_Available_Status_Enum::FREE,
														'isEligible' => Status_Type_Enum::ACTIVE,
														'tripId' => Status_Type_Enum::INACTIVE 
												), array (
														'driverId' => $trip_details->driverId 
												) );
											}
										} else {
											$insert_dispatch_data = array (
													'driverId' => $trip_details->driverId,
													'weekCompletedTrip' => 1,
													'totalCompletedTrip' => 1,
													'availabilityStatus' => Taxi_Available_Status_Enum::FREE,
													'isEligible' => Status_Type_Enum::ACTIVE,
											);
											$insert_driver_dispatch = $this->Driver_Dispatch_Details_Model->insert ( $insert_dispatch_data );
										}
										// @update the week completed trip count
										// end driver dispath details table
										// send FCM/GCM notification to passenger trip ended waiting for payment
										$this->tripNotification ( $trip_id, $trip_details->passengerId, User_Type_Enum::PASSENGER, $msg_data ['trip_completed'], 5 );
										
										if ($trip_details->paymentMode == Payment_Mode_Enum::AGD) {
											$message = array (
													"message" => $msg_data ['trip_finalize_waiting_paytm_payment'],
													"detail" => $response_data,
													"status" => 1 
											);
										} else {
											
											$message = array (
													"message" => $msg_data ['trip_finalize_success'],
													"detail" => $response_data,
													"status" => 1 
											);
										}
									} else {
										$message = array (
												"message" => $msg_data ['trip_finalize_failed'],
												"detail" => $response_data,
												"status" => - 1 
										);
									}
									// Trip fare update functionlity End
									
									$message = array (
											"message" => $msg_data ['driver_start_complete'],
											
											"status" => 1 
									);
								} else {
									$message = array (
											"message" => $msg_data ['invalid_drop_time'],
											"status" => - 1 
									);
								}
							} else {
								
								$message = array (
										"message" => $msg_data ['invalid_rate_card'],
										"status" => - 1 
								);
							}
						} else {
							if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::PASSENGER_CANCELLED) {
								$message = array (
										"message" => $msg_data ['trip_reject_passenger'],
										"status" => - 1 
								);
							} else if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::COMPLETED_TRIP) {
								$message = array (
										"message" => $msg_data ['trip_completed'],
										"status" => - 1 
								);
							} else if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::AVAILABLE_TRIP) {
								$message = array (
										"message" => $msg_data ['trip_not_started'],
										"status" => - 1 
								);
							}
						}
					} else {
						$message = array (
								"message" => $msg_data ['invalid_trip_request'],
								"status" => - 1 
						);
					}
				} else {
					$message = array (
							"message" => $msg_data ['invalid_user'],
							"status" => - 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['invalid_trip'],
						"status" => - 1 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['invalid_trip'],
					"status" => - 1 
			);
		}
		
		echo json_encode ( $message );
	}
	public function getPassengerInfo() {
		$keyword = $this->input->post ( 'keyword' );
		$type = $this->input->post ( 'type' );
		$data ['passenger_info'] = $this->Passenger_Model->getPassengerInfoForTripBooking ( $keyword, $type );
		if (! empty ( $data ['passenger_info'] )) {
			echo "<ul id='passenger-autocompletelist' style=''>";
			foreach ( $data ['passenger_info'] as $passenger ) {
				$passneger_name = $passenger->passenger_name;
				$passneger_id = $passenger->passenger_id;
				$passenger_email = $passenger->passenger_email;
				$passenger_mobile = $passenger->passenger_mobile;
				echo "<li onClick=\"selectPassenger('$passneger_id','$passenger_mobile','$passenger_email','$passneger_name');\"> $passneger_name </li> ";
			}
			echo "</ul>";
		}
	}
	public function tripNotification($trip_id, $user_id, $user_type, $notification_msg = "Success", $notification_status = 1, $response_data = array()) {
		// $gcm_id,$trip_id //Posted json data
		$gcm_id = '';
		// $trip_id = $post_data->trip_id;
		// $driver_id = 2;
		$response = NULL;
		$msg_data = $this->config->item ( 'api' );
		$msg = '';
		$status = 0;
		$firebase_api_key = PASSENGER_FIREBASE_API_KEY;
		
		$post_data = array ();
		$response_data = array ();
		if ($user_type == User_Type_Enum::DRIVER) {
			$user_details = $this->Driver_Model->getById ( $user_id );
		} else if ($user_type == User_Type_Enum::PASSENGER) {
			$user_details = $this->Passenger_Model->getById ( $user_id );
		}
		
		if ($user_details) {
			if ($user_details->gcmId) {
				$gcm_id = $user_details->gcmId;
				$post_data ['registration_ids'] = ( array ) $gcm_id;
				if ($gcm_id) {
					$trip_details = $this->Trip_Details_Model->getById ( $trip_id );
					
					$response_data = array (
							"message" => "trip",
							"status" => 1,
							"details" => array (
									"message" => $notification_msg,
									"status" => $notification_status 
							) 
					);
					
					$msg = $msg_data ['success'];
					$status = 1;
					$post_data ['data'] = $response_data;
				} else {
					
					$post_data ['data'] = array (
							'message' => $msg_data ['failed'],
							'status' => - 1 
					);
				}
				
				$post_data = json_encode ( $post_data );
				$url = "https://fcm.googleapis.com/fcm/send";
				
				$ch = curl_init ();
				curl_setopt ( $ch, CURLOPT_URL, $url );
				curl_setopt ( $ch, CURLOPT_HTTPHEADER, array (
						'Content-Type:application/json',
						'Authorization: key=' . $firebase_api_key 
				) );
				curl_setopt ( $ch, CURLOPT_POST, 1 );
				curl_setopt ( $ch, CURLOPT_POSTFIELDS, $post_data );
				curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
				$response = curl_exec ( $ch );
				curl_close ( $ch );
			}
		}
		return $response;
		// echo json_encode ( array('response'=>$response,'msg'=>$msg,'status'=>$status));
	}
	private function getTaxiForTrip($trip_id) {
		sleep ( 1 );
		$taxi_request_id = FALSE;
		$driver_taxi_details = NULL;
		$distance = 0;
		$trip_details = $this->Trip_Details_Model->getById ( $trip_id );
		if (TimeIsBetweenTwoTimes ( DAY_START_TIME, DAY_END_TIME, $trip_details->pickupDatetime )) {
			$distance = RIDE_NOW_LIMIT_DAY_ONE;
		} else if (TimeIsBetweenTwoTimes ( NIGHT_START_TIME, NIGHT_END_TIME, $trip_details->pickupDatetime )) {
			$distance = RIDE_NOW_LIMIT_NIGHT_ONE;
		}
		$driver_taxi_details = $this->Driver_Api_Webservice_Model->getDriverListForTrip ( $trip_id, $distance );
		if (! $driver_taxi_details) {
			$distance = 0;
			if (TimeIsBetweenTwoTimes ( DAY_START_TIME, DAY_END_TIME, $trip_details->pickupDatetime )) {
				$distance = RIDE_NOW_LIMIT_DAY_TWO;
			} else if (TimeIsBetweenTwoTimes ( NIGHT_START_TIME, NIGHT_END_TIME, $trip_details->pickupDatetime )) {
				$distance = RIDE_NOW_LIMIT_NIGHT_TWO;
			}
			$driver_taxi_details = $this->Driver_Api_Webservice_Model->getDriverListForTrip ( $trip_id, $distance );
		}
		// check taxi request for particular trip already exists or not
		$taxi_request_exists = $this->Taxi_Request_Details_Model->getByKeyValueArray ( array (
				'tripId' => $trip_id 
		) );
		if ($driver_taxi_details) {
			if ($taxi_request_exists) {
				$update_taxi_request = array (
						'selectedDriverId' => $driver_taxi_details [0]->driverId,
						'taxiRequestStatus' => Taxi_Request_Status_Enum::AVAILABLE_TRIP 
				);
				$taxi_request_id = $this->Taxi_Request_Details_Model->update ( $update_taxi_request, array (
						'tripId' => $trip_id 
				) );
			} else {
				$insert_taxi_request = array (
						'tripId' => $trip_id,
						'totalAvailableDriver' => count ( $driver_taxi_details ),
						'selectedDriverId' => $driver_taxi_details [0]->driverId,
						'taxiRequestStatus' => Taxi_Request_Status_Enum::AVAILABLE_TRIP 
				);
				$taxi_request_id = $this->Taxi_Request_Details_Model->insert ( $insert_taxi_request );
			}
			$update_trip_details = $this->Trip_Details_Model->update ( array (
					'driverId' => $driver_taxi_details [0]->driverId,
					'taxiId' => $driver_taxi_details [0]->taxiId,
					'tripStatus' => Trip_Status_Enum::DRIVER_DISPATCHED,
					'dispatchedDatetime' => getCurrentDateTime (),
					'notificationStatus' => Trip_Status_Enum::DRIVER_DISPATCHED,
					'driverAcceptedStatus' => Driver_Accepted_Status_Enum::NONE 
			), array (
					'id' => $trip_id 
			) );
		} else {
			$update_trip_details = $this->Trip_Details_Model->update ( array (
					'tripStatus' => Trip_Status_Enum::DRIVER_NOT_FOUND,
					'notificationStatus' => Trip_Status_Enum::NO_NOTIFICATION,
					'driverAcceptedStatus' => Driver_Accepted_Status_Enum::NONE 
			), array (
					'id' => $trip_id 
			) );
		}
		return $driver_taxi_details;
	}
	public function getTripTrackingLatLong() {
		$trip_id = $this->input->post ( 'trip_id' );
		// $trip_id = 1660;
		
		$driver_latlong = $this->Trip_Temp_Tracking_Details_Model->getDriverRecentLatLong ( $trip_id );
		if (count ( $driver_latlong ) > 0) {
			$latitude = $driver_latlong->latitude;
			$longitude = $driver_latlong->longitude;
		} else {
			$latitude = 0;
			$longitude = 0;
		}
		/*
		 * $markers = "[
		 * {'id':1,'name':'Driver','position':{'lat':$latitude,'long':$longitude}}
		 * ]";
		 */
		$markers = '[
                            {"id":1,"name":"Driver","position":{"lat":"' . $latitude . '","long":"' . $longitude . '"}}
                         ]';
		echo $markers;
	}
	public function passengerTripReject() {
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$message = '';
		$check_trip_avilablity = NULL;
		$trip_details = NULL;
		
		$trip_id = NULL;
		
		// Posted json data
		$post_data = $this->input->post ();
		
		$trip_id = $post_data ['trip_id'];
		
		// get trip details
		$trip_details = $this->Trip_Details_Model->getById ( $trip_id );
		$trip_details = $this->Common_Api_Webservice_Model->getTripDetails ( $trip_id, $trip_details->passengerId );
		// check trip avilable for driver/any in progress trip before logout
		$check_trip_avilablity = $this->Taxi_Request_Details_Model->getOneByKeyValueArray ( array (
				'tripId' => $trip_id 
		), 'id DESC' );
		if ($check_trip_avilablity) {
			if (($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::DRIVER_ACCEPTED) && ($trip_details [0]->tripStatus == Trip_Status_Enum::IN_PROGRESS || $trip_details [0]->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYMENT)) {
				$message = array (
						"message" => $msg_data ['trip_progress'],
						"status" => - 1 
				);
			} else if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::PASSENGER_CANCELLED) {
				$message = array (
						"message" => $msg_data ['trip_reject_passenger'],
						"status" => - 1 
				);
			} else if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::COMPLETED_TRIP) {
				$message = array (
						"message" => $msg_data ['trip_completed'],
						"status" => - 1 
				);
			} else if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::DRIVER_REJECTED) {
				$message = array (
						"message" => $msg_data ['trip_reject_driver'],
						"status" => - 1 
				);
			} else if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::DRIVER_NOT_FOUND) {
				$message = array (
						"message" => $msg_data ['driver_not_found'],
						"status" => - 1 
				);
			} else if (($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::DRIVER_ACCEPTED || $check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::AVAILABLE_TRIP) && ($trip_details [0]->tripStatus == Trip_Status_Enum::DRIVER_ACCEPTED || $trip_details [0]->tripStatus == Trip_Status_Enum::DRIVER_DISPATCHED || $trip_details [0]->tripStatus == Trip_Status_Enum::DRIVER_ARRIVED)) {
				
				$trip_status_update = $this->Trip_Details_Model->update ( array (
						'tripStatus' => Trip_Status_Enum::CANCELLED_BY_PASSENGER,
						'notificationStatus' => Trip_Status_Enum::CANCELLED_BY_PASSENGER 
				), array (
						'id' => $trip_id 
				) );
				
				$trip_request_status_update = $this->Taxi_Request_Details_Model->update ( array (
						'taxiRequestStatus' => Taxi_Request_Status_Enum::PASSENGER_CANCELLED 
				), array (
						'tripId' => $trip_id 
				) ); // get last inserted shift id for particular driver
				$get_last_shift_id = $this->Driver_Shift_History_Model->getOneByKeyValueArray ( array (
						'driverId' => $trip_details [0]->driverId 
				), 'id DESC' );
				$driver_shift_status_update = $this->Driver_Shift_History_Model->update ( array (
						'availabilityStatus' => Taxi_Available_Status_Enum::FREE 
				), array (
						'id' => $get_last_shift_id->id 
				) );
				if ($check_trip_avilablity->taxiRequestStatus == Taxi_Request_Status_Enum::DRIVER_ACCEPTED && $trip_details [0]->tripStatus == Trip_Status_Enum::DRIVER_ACCEPTED || $trip_details [0]->tripStatus == Trip_Status_Enum::DRIVER_ARRIVED) {
					// update the penalty trip reject count by one
					$passenger_details = $this->Passenger_Model->getById ( $trip_details [0]->passengerId );
					$penalty_trip_count = ++ $passenger_details->penaltyTripCount;
					$update_penalty_count = $this->Passenger_Model->update ( array (
							'penaltyTripCount' => $penalty_trip_count 
					), array (
							'id' => $passenger_details->id 
					) );
				}
				$entity_config_details = $this->Entity_Config_Model->getOneByKeyValueArray ( array (
						'entityId' => $trip_details [0]->entityId 
				) );
				// sms & email start
				if ($entity_config_details->sendSms && $trip_request_status_update) {
					// SMS To driver
					
					$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
							'title' => 'passenger_cancelled_driver_sms' 
					) );
					
					$message_data = $message_details->content;
					$message_data = str_replace ( "##JOBCARDID##", $trip_details [0]->jobCardId, $message_data );
					if ($trip_details [0]->driverMobile) {
						
						$this->Common_Api_Webservice_Model->sendSMS ( $trip_details [0]->driverMobile, $message_data );
					}
				}
				
				$driver_dispatch_exists = NULL;
				$driver_dispatch_exists = $this->Driver_Dispatch_Details_Model->getOneByKeyValueArray ( array (
						'tripId' => $trip_id 
				) );
				if ($driver_dispatch_exists) {
					$update_driver_dispatch = $this->Driver_Dispatch_Details_Model->update ( array (
							'tripId' => Status_Type_Enum::INACTIVE,
							'availabilityStatus' => Taxi_Available_Status_Enum::FREE,
							'isEligible' => Status_Type_Enum::ACTIVE,
					), array (
							'tripId' => $trip_id 
					) );
				}
				// sms & email end
				$message = array (
						"message" => $msg_data ['passenger_cancel_success'],
						"status" => 1 
				);
			} else {
				$message = array (
						"message" => $msg_data ['invalid_trip'],
						"status" => - 1 
				);
			}
		} else {
			if ($trip_details) {
				$trip_status_update = $this->Trip_Details_Model->update ( array (
						'tripStatus' => Trip_Status_Enum::CANCELLED_BY_PASSENGER,
						'notificationStatus' => Trip_Status_Enum::CANCELLED_BY_PASSENGER 
				), array (
						'id' => $trip_id 
				) );
				$entity_config_details = $this->Entity_Config_Model->getOneByKeyValueArray ( array (
						'entityId' => $trip_details [0]->entityId 
				) );
				// sms & email start
				if ($entity_config_details->sendSms && $trip_status_update) {
					// SMS To driver
					
					$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
							'title' => 'passenger_cancelled_driver_sms' 
					) );
					
					$message_data = $message_details->content;
					$message_data = str_replace ( "##JOBCARDID##", $trip_details [0]->jobCardId, $message_data );
					if ($trip_details [0]->driverMobile) {
						
						$this->Common_Api_Webservice_Model->sendSMS ( $trip_details [0]->driverMobile, $message_data );
					}
				}
				
				// sms & email end
				$message = array (
						"message" => $msg_data ['passenger_cancel_success'],
						"status" => 1 
				);
			} else {
				$message = array (
						"message" => $msg_data ['invalid_trip'],
						"status" => - 1 
				);
			}
		}
		echo json_encode ( $message );
	}
	public function validatePromocode() {
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$message = '';
		$check_trip_avilablity = NULL;
		$trip_details = NULL;
		$zone_id=NULL;
		$drop_zone_id=NULL;
		$entity_id=NULL;
		$promocode = NULL;
		$passenger_id = 0;
		// Posted json data
		$post_data = $this->input->post ();
		
		$zone_id = $data ['zoneId'];
		if ($data ['subZoneId']) {
			$data ['zoneId'] = $data ['subZoneId'];
			$data ['subZoneId'] = $zone_id;
		}
		$drop_zone_id = $data ['dropZoneId'];
		if ($data ['dropSubZoneId']) {
			$data ['dropZoneId'] = $data ['dropSubZoneId'];
			$data ['dropZoneId'] = $drop_zone_id;
		}
		$entity_id = $post_data ['entityId'];
		$promocode = $post_data ['promocode'];
		if (array_key_exists ( 'passenger_id', $post_data )) {
			$passenger_id = $post_data ['passenger_id'];
		}
		// actual functionality
		
		// Check promocode exists or not
		if (($promocode && $zone_id && $entity_id) || $drop_zone_id ) {
			
			// Check promocode exists or not for particular passenger at particular city
			$promocode_exists = $this->Common_Api_Webservice_Model->checkPromocodeAvailablity ( $promocode, $passenger_id,$zone_id,$drop_zone_id,$entity_id );
			
			if ($promocode_exists && count ( $promocode_exists ) > 0) {
				
				// check promocode limit exceeds or not for specific passenger (note: only completed trip)
				$promocode_user_limit_exists = $this->Trip_Details_Model->getByKeyValueArray ( array (
						'passengerId' => $passenger_id,
						'promoCode' => $promocode,
						'tripStatus' => Trip_Status_Enum::TRIP_COMPLETED 
				) );
				// check promocode limit exceeds or not total limit (note: only completed trip)
				$promocode_limit_exists = $this->Trip_Details_Model->getUsedPromocodeLimitCount($promocode);
				if (count ( $promocode_limit_exists ) >= $promocode_exists [0]->promoCodeLimit) {
					$message = array (
							"message" => $msg_data ['promocode_total_limit'],
							"status" => - 1 
					);
				} else if (count ( $promocode_user_limit_exists ) >= $promocode_exists [0]->promoCodeUserLimit) {
					$message = array (
							"message" => $msg_data ['promocode_user_limit'],
							"status" => - 1 
					);
				} else if (strtotime ( $promocode_exists [0]->promoStartDatetime ) > getCurrentUnixDateTime ()) {
					$message = array (
							"message" => $msg_data ['promocode_start'],
							"status" => - 1 
					);
				} else if (strtotime ( $promocode_exists [0]->promoEndDatetime ) < getCurrentUnixDateTime ()) {
					$message = array (
							"message" => $msg_data ['promocode_expiry'],
							"status" => - 1 
					);
				} else {
					$message = array (
							"message" => $msg_data ['promocode_success'],
							"status" => 1 
					);
				}
			} else {
				$message = array (
						"message" => $msg_data ['invalid_promocode'],
						"status" => - 1 
				);
			}
		} else {
			$message = array (
					"message" => $msg_data ['mandatory_data'],
					"status" => - 1 
			);
		}
		echo json_encode ( $message );
	}
	public function tripEstimate() {
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$message = '';
		
		$zone_id = NULL;
		$entity_id = NULL;
		$taxi_category_type = NULL;
		$travelled_distance = 0;
		$total_travelled_minute = 0;
		$pickup_date_time = NULL;
		
		$rate_card_details = NULL;
		$rate_card_distance_slab_details = NULL;
		$rate_card_time_slab_details = NULL;
		$rate_card_surge_slab_details = NULL;
		
		// Posted json data
		$post_data = $this->input->post ();
		
		$zone_id = $post_data ['zoneId'];
		if ($post_data ['subZoneId']) {
			$post_data ['zoneId'] = $post_data ['subZoneId'];
			$post_data ['subZoneId'] = $zone_id;
		}
		$zone_id = $post_data ['zoneId'];
		
		$taxi_category_type = $post_data ['taxiCategoryType'];
		$entity_id = $post_data ['entityId'];
		$pickup_date_time = $post_data ['pickupDatetime'];
		
		$actual_pickup_time = date ( 'Y-m-d H:i:s', strtotime ( $pickup_date_time ) );
		$convenience_charge = 0;
		$travel_charge = 0;
		$total_trip_cost = 0;
		$travelled_distance_charge = 0;
		$travelled_time_charge = 0;
		$surge_charge = 0;
		
		// To get trip estimated distance and time from google API
		$URL = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=" . $post_data ['pickupLatitude'] . "," . $post_data ['pickupLongitude'] . "&destinations=" . $post_data ['dropLatitude'] . "," . $post_data ['dropLongitude'] . "&key=" . GOOGLE_MAP_API_KEY;
		// $URL="https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=12.93455,77.53447&destinations=15.00164,74.05266&key=AIzaSyBcN2hpqU2MY00KFk3jVtrsLJvrucZBr6I";
		$curl = curl_init ();
		curl_setopt ( $curl, CURLOPT_URL, $URL );
		curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $curl, CURLOPT_HEADER, false );
		$result = curl_exec ( $curl );
		curl_close ( $curl );
		// return $result; ;
		
		$response = json_decode ( $result );
		
		if ($response->status) {
			if ($response->rows [0]->elements [0]->distance->text) {
				// $distance = explode ( ' ', $response->rows [0]->elements [0]->distance->text );
				// $travelled_distance = round ( ($distance [0] * 1.609344), 1 ) + 0.5;
				
				$distance = explode ( ' ', $response->rows [0]->elements [0]->distance->text );
				if ($distance [1] == 'm') {
					$travelled_distance = round ( ($distance [0] / 1000), 1 );
				} else if ($distance [1] == 'km') {
					$travelled_distance = round ( ($distance [0]), 1 );
				}
			}
			if ($response->rows [0]->elements [0]->duration->text) {
				$time = explode ( ' ', $response->rows [0]->elements [0]->duration->text );
				if ($time [1] == 'hour' || $time [1] == 'hours') {
					$total_travelled_minute += ($time [0] * 60) + $time [2];
				} else if ($time [1] == 'mins') {
					$total_travelled_minute = $time [1];
				}
			}
		}
		
		// To get trip estimated distance and time from google API
		
		if ($zone_id && $taxi_category_type && $travelled_distance >= 0 && $total_travelled_minute >= 0) {
			// get rate card details based on the cityId & trip type
			$rate_card_details = $this->Rate_Card_Details_Model->getOneByKeyValueArray ( array (
					'zoneId' => $zone_id,
					'entityId' => $entity_id,
					'taxiCategoryType' => $taxi_category_type,
					'status' => Status_Type_Enum::ACTIVE,
					'isDeleted' => Status_Type_Enum::INACTIVE 
			) );
			
			if (! $rate_card_details) {
				
				$rate_card_details = $this->Rate_Card_Details_Model->getOneByKeyValueArray ( array (
						'zoneId' => Status_Type_Enum::INACTIVE,
						'entityId' => $entity_id,
						'taxiCategoryType' => $taxi_category_type,
						'status' => Status_Type_Enum::ACTIVE,
						'isDeleted' => Status_Type_Enum::INACTIVE 
				) );
			}
			if (! $rate_card_details) {
				
				$rate_card_details = $this->Rate_Card_Details_Model->getOneByKeyValueArray ( array (
						'zoneId' => $zone_id,
						'entityId' => Status_Type_Enum::INACTIVE,
						'taxiCategoryType' => $taxi_category_type,
						'status' => Status_Type_Enum::ACTIVE,
						'isDeleted' => Status_Type_Enum::INACTIVE 
				) );
			}
			if (! $rate_card_details) {
				
				$rate_card_details = $this->Rate_Card_Details_Model->getOneByKeyValueArray ( array (
						'zoneId' => Status_Type_Enum::INACTIVE,
						'entityId' => Status_Type_Enum::INACTIVE,
						'taxiCategoryType' => $taxi_category_type,
						'status' => Status_Type_Enum::ACTIVE,
						'isDeleted' => Status_Type_Enum::INACTIVE 
				) );
			}
			
			// to get convenience charge based pickup time day/night
			
			if (TimeIsBetweenTwoTimes ( DAY_START_TIME, DAY_END_TIME, $actual_pickup_time )) {
				
				$convenience_charge += $rate_card_details->dayConvenienceCharge;
			} else if (TimeIsBetweenTwoTimes ( NIGHT_START_TIME, NIGHT_END_TIME, $actual_pickup_time )) {
				$convenience_charge += $rate_card_details->nightConvenienceCharge;
			}
			// to calculate the distance slab trip journey amount
			$rate_card_distance_slab_details = $this->Rate_Card_Slab_Details_Model->getByKeyValueArray ( array (
					'rateCardId' => $rate_card_details->id,
					'slabType' => Slab_Type_Enum::DISTANCE 
			) );
			$travelled_distance=$travelled_distance+0.5;
			if ($travelled_distance) {
				$available_travelled_distance = $travelled_distance;
				$calculated_travelled_distance = 0;
				$calculate_to_distance = 0;
				foreach ( $rate_card_distance_slab_details as $rate_distance ) {
					if ($available_travelled_distance) {
						if ($rate_distance->slabEnd) {
							
							if (! $rate_distance->slabStart) {
								$calculate_to_distance = $rate_distance->slabEnd;
							} else {
								$calculate_to_distance = ($rate_distance->slabEnd - $rate_distance->slabStart);
							}
							$calculated_travelled_distance += $calculate_to_distance;
							// $available_travelled_distance -= $calculate_to_distance;
							if ($available_travelled_distance <= $calculate_to_distance) {
								$calculate_to_distance = $available_travelled_distance;
								$available_travelled_distance = 0;
							} else {
								$available_travelled_distance -= $calculate_to_distance;
							}
						} else if ($rate_distance->slabStart && ! $rate_distance->slabEnd) {
							$calculate_to_distance = $available_travelled_distance;
							
							$calculated_travelled_distance += $calculate_to_distance;
							$available_travelled_distance -= $calculate_to_distance;
						}
						
						if ($calculate_to_distance) {
							if ($rate_distance->slabCharge) {
								
								$travelled_distance_charge += $calculate_to_distance * $rate_distance->slabCharge;
								$calculate_to_distance = 0;
							}
						}
					}
				}
			}
			// /add to travel charge
			$travel_charge += $travelled_distance_charge;
			
			// to calculate the time slab trip journey amount
			$rate_card_time_slab_details = $this->Rate_Card_Slab_Details_Model->getByKeyValueArray ( array (
					'rateCardId' => $rate_card_details->id,
					'slabType' => Slab_Type_Enum::TIME 
			) );
			if ($total_travelled_minute) {
				
				$available_travelled_minute = $total_travelled_minute;
				$calculated_travelled_minute = 0;
				$calculate_to_minute = 0;
				foreach ( $rate_card_time_slab_details as $rate_time ) {
					if ($available_travelled_minute) {
						if ($rate_time->slabEnd) {
							
							if (! $rate_time->slabStart) {
								$calculate_to_minute = $rate_time->slabEnd;
							} else {
								$calculate_to_minute = ($rate_time->slabEnd - $rate_time->slabStart);
							}
							$calculated_travelled_minute += $calculate_to_minute;
							// $available_travelled_distance -= $calculate_to_distance;
							if ($available_travelled_minute <= $calculate_to_minute) {
								$calculate_to_minute = $available_travelled_minute;
								$available_travelled_minute = 0;
							} else {
								$available_travelled_minute -= $calculate_to_minute;
							}
						} else if ($rate_time->slabStart && ! $rate_time->slabEnd) {
							$calculate_to_minute = $available_travelled_minute;
							
							$calculated_travelled_minute += $calculate_to_minute;
							$available_travelled_minute -= $calculate_to_minute;
						}
						
						if ($calculate_to_minute) {
							if ($rate_time->slabCharge) {
								
								$travelled_time_charge += $calculate_to_minute * $rate_time->slabCharge;
								$calculate_to_minute = 0;
							}
						}
					}
				}
			}
			
			// /add to travel charge
			$travel_charge += $travelled_time_charge;
			
			// to calculate the surge slab trip journey amount
			$rate_card_time_slab_details = $this->Rate_Card_Slab_Details_Model->getByKeyValueArray ( array (
					'rateCardId' => $rate_card_details->id,
					'slabType' => Slab_Type_Enum::SURGE 
			) );
			if ($rate_card_surge_slab_details) {
				foreach ( $rate_card_surge_slab_details as $rate_surge ) {
					$slab_start_time = $rate_surge->slabStart;
					$slab_end_time = $rate_surge->slabEnd;
					// $user_st=$user_et="2017-08-02 09:29:12";
					if (TimeIsBetweenTwoTimes ( $slab_start_time, $slab_end_time, $actual_pickup_time )){
							//if (((strtotime ( $actual_pickup_time ) >= strtotime ( $slab_start_time )) && (strtotime ( $actual_pickup_time ) <= strtotime ( $slab_end_time ))) || (strtotime ( $actual_pickup_time ) >= strtotime ( $slab_start_time )) && (strtotime ( $actual_pickup_time ) <= strtotime ( $slab_end_time ))) {
								if ($rate_surge->slabChargeType == Payment_Type_Enum::AMOUNT) {
							$surge_charge = $rate_surge->slabCharge;
							$total_trip_cost += $surge_charge;
							break;
						} else if ($rate_surge->slabChargeType == Payment_Type_Enum::PERCENTAGE) {
							$surge_charge = ($rate_surge->slabCharge / 100) * ($convenience_charge + $travel_charge);
							$surge_charge = round ( $surge_charge, 2 );
							$total_trip_cost += $surge_charge;
							break;
						}
					}
				}
			}
			// /add to travel charge
			$travel_charge += $surge_charge;
			
			$total_trip_cost = $convenience_charge + $travel_charge;
			$total_trip_cost = round ( $total_trip_cost, 2 );
			
			$message = array (
					"message" => 'Estimated Bill Amount is ' . $total_trip_cost,
					"travel_charge" => $travel_charge,
					"total_trip_cost" => round ( $total_trip_cost / 100 ) * 100,
					"base_charge" => $convenience_charge,
					"time" => $total_travelled_minute,
					"distance" => $travelled_distance,
					"status" => 1 
			);
		} else {
			
			$message = array (
					"message" => $msg_data ['mandatory_data'],
					"status" => - 1 
			);
		}
		echo json_encode ( $message );
	}
	private function tripDispatchDriverNotification($trip_id, $driver_id, $distance = 0) {
		// $gcm_id,$trip_id //Posted json data
		$gcm_id = '';
		// $trip_id = $post_data->trip_id;
		// $driver_id = 2;
		$response = NULL;
		$msg_data = $this->config->item ( 'api' );
		$msg = '';
		$status = 0;
		$firebase_api_key = DRIVER_FIREBASE_API_KEY;
		
		$post_data = array ();
		$response_data = array ();
		
		$driver_details = $this->Driver_Model->getById ( $driver_id );
		
		if ($driver_details) {
			if ($driver_details->gcmId) {
				$gcm_id = $driver_details->gcmId;
				$post_data ['registration_ids'] = ( array ) $gcm_id;
				if ($gcm_id) {
					// $trip_details = $this->Trip_Details_Model->getById($trip_id);
					$trip_details = $this->Common_Api_Webservice_Model->getTripDetails ( $trip_id );
					$passenger_details = $this->Passenger_Model->getById ( $trip_details->passengerId );
					
					$response_data = array (
							"message" => 'driver_dispatch',
							"status" => 1,
							'trip_details' => array (
									'trip_id' => $trip_details [0]->tripId,
									'job_card_id' => $trip_details [0]->jobCardId,
									'pickup_location' => $trip_details [0]->pickupLocation,
									'pickup_latitude' => $trip_details [0]->pickupLatitude,
									'pickup_longitude' => $trip_details [0]->pickupLongitude,
									'pickup_time' => $trip_details [0]->pickupDatetime,
									'drop_location' => $trip_details [0]->dropLocation,
									'drop_latitude' => $trip_details [0]->dropLatitude,
									'drop_longitude' => $trip_details [0]->dropLongitude,
									'drop_time' => $trip_details [0]->dropDatetime,
									'land_mark' => $trip_details [0]->landmark,
									'taxi_rating' => $trip_details [0]->passengerRating,
									'taxi_comments' => $trip_details [0]->passengerComments,
									'trip_type' => $trip_details [0]->tripType,
									'trip_type_name' => $trip_details [0]->tripTypeName,
									'payment_mode' => $trip_details [0]->paymentMode,
									'payment_mode_name' => $trip_details [0]->paymentModeName,
									'trip_status' => $trip_details [0]->tripStatus,
									'trip_status_name' => $trip_details [0]->tripStatusName,
									'promocode' => $trip_details [0]->promoCode,
									'is_driver_rated' => ($trip_details [0]->driverRating) ? TRUE : FALSE 
							),
							'passenger_details' => array (
									'passenger_id' => $trip_details [0]->passengerId,
									'passenger_firstname' => $trip_details [0]->passengerFirstName,
									'passenger_lastname' => $trip_details [0]->passengerLastName,
									'passenger_mobile' => $trip_details [0]->passengerMobile,
									'passenger_latitude' => $trip_details [0]->passengerCurrentLatitude,
									'passenger_longitude' => $trip_details [0]->passengerCurrentLongitude,
									'passenger_profile_image' => ($trip_details [0]->passengerProfileImage) ? (passenger_content_url ( $trip_details [0]->passengerId . '/' . $trip_details [0]->passengerProfileImage )) : image_url ( 'app/user.png' ) 
							),
							'driver_details' => array (
									'driver_id' => $trip_details [0]->driverId,
									'driver_firstname' => $trip_details [0]->driverFirstName,
									'driver_lastname' => $trip_details [0]->driverLastName,
									'driver_mobile' => $trip_details [0]->driverMobile,
									'driver_profile_image' => ($trip_details [0]->driverProfileImage) ? (driver_content_url ( $trip_details [0]->driverId . '/' . $trip_details [0]->driverProfileImage )) : image_url ( 'app/user.png' ),
									'driver_language' => $trip_details [0]->driverLanguagesKnown,
									'driver_experience' => $trip_details [0]->driverExperience,
									'driver_age' => $trip_details [0]->driverDob,
									'driver_latitute' => $trip_details [0]->currentLatitude,
									'driver_longtitute' => $trip_details [0]->currentLongitude,
									'driver_status' => $trip_details [0]->availabilityStatus 
							),
							'taxi_details' => array (
									'taxi_id' => $trip_details [0]->taxiId,
									'taxi_name' => $trip_details [0]->taxiName,
									'taxi_model' => $trip_details [0]->taxiModel,
									'taxi_manufacturer' => $trip_details [0]->taxiManufacturer,
									'taxi_registration_no' => $trip_details [0]->taxiRegistrationNo,
									'taxi_colour' => $trip_details [0]->taxiColour 
							),
							
							"estimated_time" => $trip_details [0]->estimatedTime,
							"distance_away" => number_format ( $trip_details [0]->estimatedDistance, 1 ),
							"calculate_distance_away" => number_format ( $trip_details [0]->estimatedDistance + 0.5, 1 ),
							"notification_time" => 20 
					)
					;
					$post_data ['data'] = array (
							
							'details' => $response_data 
					)
					;
				} else {
					$msg = $msg_data ['failed'];
					
					$post_data ['data'] = array (
							'message' => $msg,
							'status' => - 1 
					);
				}
				
				$post_data = json_encode ( $post_data );
				$url = "https://fcm.googleapis.com/fcm/send";
				
				$ch = curl_init ();
				curl_setopt ( $ch, CURLOPT_URL, $url );
				curl_setopt ( $ch, CURLOPT_HTTPHEADER, array (
						'Content-Type:application/json',
						'Authorization: key=' . $firebase_api_key 
				) );
				curl_setopt ( $ch, CURLOPT_POST, 1 );
				curl_setopt ( $ch, CURLOPT_POSTFIELDS, $post_data );
				curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
				$response = curl_exec ( $ch );
				curl_close ( $ch );
			}
		}
		return $response;
		// echo json_encode ( array('response'=>$response,'msg'=>$msg,'status'=>$status));
	}
	
	public function trip_driver_ajax_list() {
		// echo"ajaxlist";exit;
		$start_date = NULL;
		$end_date = NULL;
		$trip_type = NULL;
		$trip_status = NULL;
		$zone_id = NULL;
		$entity_id = NULL;
	
		// Posted json data
		$post_data = $this->input->post ();
		
		$trip_id = $post_data ['trip_id'];
		
		$sql_query = $this->Trip_Driver_List_Model->getDriverAvailableListQuery ( $trip_id);
		$trip_list = $this->Trip_Driver_List_Model->getDataTable ( $sql_query );
		$data = array ();
		$no = $_POST ['start'];
		//debug_exit($trip_list);
		
		foreach ( $trip_list as $list ) {
			$no ++;
			$estimated_distance=0;
			$estimated_time=0;
			// To get trip estimated distance and time from google API
			$URL = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=" . $list->pickupLatitude . "," . $list->pickupLongitude . "&destinations=" . $list->driverLatitude . "," . $list->driverLongitude . "&key=" . GOOGLE_MAP_API_KEY;
				
			$curl = curl_init ();
			curl_setopt ( $curl, CURLOPT_URL, $URL );
			curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, true );
			curl_setopt ( $curl, CURLOPT_HEADER, false );
			$result = curl_exec ( $curl );
			curl_close ( $curl );
			// return $result; ;
				
			$response = json_decode ( $result );
				
			if ($response->status) {
				if ($response->rows [0]->elements [0]->distance->text) {
					$distance = explode ( ' ', $response->rows [0]->elements [0]->distance->text );
					if ($distance [1] == 'm') {
						$estimated_distance = round ( ($distance [0] / 1000), 1 );
					} else if ($distance [1] == 'km') {
						$estimated_distance = round ( ($distance [0]), 1 );
					}
				}
				if ($response->rows [0]->elements [0]->duration->text) {
					$time = explode ( ' ', $response->rows [0]->elements [0]->duration->text );
						
					if ($time [1] == 'hour' || $time [1] == 'hours') {
						$estimated_time += ($time [0] * 60) + $time [2];
					} else if ($time [1] == 'mins') {
						$estimated_time = $time [0];
					}
				}
			}
			// To get trip estimated distance and time from google API
			
			$row = array ();
			
			$row [] = '<button class="btn btn gpblue btn-sm btn-success" id="allocateTaxiToTrip" taxiId="' . $list->taxiId . '" driverId="' . $list->driverId . '" driverCount="'.count($trip_list).'" title="View" tripId="'.$list->tripId.'">Select</button>';
			$row [] = $list->driverCode;
			$row [] = "$list->driverName";
			$row [] = "$list->driverMobile";
			$row [] = $list->driverWallet;
			$row [] = $estimated_distance."Km";
			$row [] = $estimated_time."mins";
			$row [] = $list->taxiRegistrationNo;
			$row [] = $list->driverRating;
			
			$data [] = $row;
			// add html for action
				
		}
		
		$output = array (
				"draw" => $_POST ['draw'],
				"recordsTotal" => $this->Trip_Driver_List_Model->getDataTableAllCount ( $sql_query ),
				"recordsFiltered" => $this->Trip_Driver_List_Model->getDataTableFilterCount ( $sql_query ),
				"data" => $data
		);
		// output to json format
		echo json_encode ( $output );
	}
	
	public function allocateTaxiToTrip()
	{
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		// Posted json data
		$post_data = $this->input->post ();
		
		$trip_id = $post_data ['trip_id'];
		$driver_id = $post_data ['driver_id'];
		$taxi_id = $post_data ['taxi_id'];
		$driver_count = $post_data ['driver_count'];
		$taxi_request_id = FALSE;
		
		$trip_details = $this->Trip_Details_Model->getById ( $trip_id );
		
		// check taxi request for particular trip already exists or not
		$taxi_request_exists = $this->Taxi_Request_Details_Model->getByKeyValueArray ( array (
				'tripId' => $trip_id
		) );
		if ($driver_id && $taxi_id && $driver_count) {
			if ($taxi_request_exists) {
				$update_taxi_request = array (
						'selectedDriverId' => $driver_id,
						'taxiRequestStatus' => Taxi_Request_Status_Enum::AVAILABLE_TRIP
				);
				$taxi_request_id = $this->Taxi_Request_Details_Model->update ( $update_taxi_request, array (
						'tripId' => $trip_id
				) );
			} else {
				$insert_taxi_request = array (
						'tripId' => $trip_id,
						'selectedDriverId' => $driver_id,
						'totalAvailableDriver'=>$driver_count,
						'taxiRequestStatus' => Taxi_Request_Status_Enum::AVAILABLE_TRIP
				);
				$taxi_request_id = $this->Taxi_Request_Details_Model->insert ( $insert_taxi_request );
			}
			$update_trip_details = $this->Trip_Details_Model->update ( array (
					'driverId' => $driver_id,
					'taxiId' => $taxi_id,
					'tripStatus' => Trip_Status_Enum::DRIVER_DISPATCHED,
					'dispatchedDatetime' => getCurrentDateTime (),
					'notificationStatus' => Trip_Status_Enum::DRIVER_DISPATCHED,
					'driverAcceptedStatus' => Driver_Accepted_Status_Enum::NONE
			), array (
					'id' => $trip_id
			) );
			$message = array (
					"message" => $msg_data ['success'],
					"status" => 1
			);
		} else {
		
			$message = array (
					"message" => $msg_data ['failed'],
					"status" => - 1
			);
		/* 
			if ($taxi_request_exists) {
				$update_taxi_request = array (
						'taxiRequestStatus' => Taxi_Request_Status_Enum::DRIVER_NOT_FOUND
				);
				$taxi_request_id = $this->Taxi_Request_Details_Model->update ( $update_taxi_request, array (
						'tripId' => $trip_id
				) );
			} else {
				$insert_taxi_request = array (
						'tripId' => $trip_id,
						'taxiRequestStatus' => Taxi_Request_Status_Enum::DRIVER_NOT_FOUND
				);
				$taxi_request_id = $this->Taxi_Request_Details_Model->insert ( $insert_taxi_request );
			}
			$update_trip_details = $this->Trip_Details_Model->update ( array (
					'tripStatus' => Trip_Status_Enum::DRIVER_NOT_FOUND,
					'notificationStatus' => Trip_Status_Enum::NO_NOTIFICATION,
					'driverAcceptedStatus' => Driver_Accepted_Status_Enum::NONE
			), array (
					'id' => $trip_id
			) );
		 */}
		echo json_encode ( $message );
	}
	
	public function driverNotFound()
	{
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		// Posted json data
		$post_data = $this->input->post ();
	
		$trip_id = $post_data ['trip_id'];
		
		$taxi_request_id = FALSE;
		$driver_taxi_details = NULL;
		
		// check taxi request for particular trip already exists or not
		$taxi_request_exists = $this->Taxi_Request_Details_Model->getByKeyValueArray ( array (
				'tripId' => $trip_id
		) );
		if ($trip_id) {
		if ($taxi_request_exists) {
				$update_taxi_request = array (
						'selectedDriverId' => $driver_id,
						'taxiRequestStatus' => Taxi_Request_Status_Enum::DRIVER_NOT_FOUND
				);
				$taxi_request_id = $this->Taxi_Request_Details_Model->update ( $update_taxi_request, array (
						'tripId' => $trip_id
				) );
			} else {
				$insert_taxi_request = array (
						'tripId' => $trip_id,
						'taxiRequestStatus' => Taxi_Request_Status_Enum::DRIVER_NOT_FOUND
				);
				$taxi_request_id = $this->Taxi_Request_Details_Model->insert ( $insert_taxi_request );
			}
			$update_trip_details = $this->Trip_Details_Model->update ( array (
					'tripStatus' => Trip_Status_Enum::DRIVER_NOT_FOUND,
					'notificationStatus' => Trip_Status_Enum::NO_NOTIFICATION,
					'driverAcceptedStatus' => Driver_Accepted_Status_Enum::NONE
			), array (
					'id' => $trip_id
			) );
			$message = array (
					"message" => $msg_data ['failed'],
					"status" => 1
			);
		} 
		else {
		
			$message = array (
					"message" => $msg_data ['failed'],
					"status" => - 1
			);
		}
	echo json_encode ( $message );
	}
} // end of class