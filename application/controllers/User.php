<?php
class User extends My_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->helper ( 'form' );
		$this->load->model ( 'Taxi_Query_Model' );
		$this->load->model ( 'User_Login_Details_Model' );
		$this->load->model ( 'Zone_Details_Model' );
		$this->load->model ( 'Entity_Model' );
		$this->load->model ( 'User_Personal_Details_Model' );
		$this->load->model ( 'Data_Attributes_Model' );
		$this->load->model ( 'Sms_Template_Model' );
		$this->load->model ( 'Common_Api_Webservice_Model' );
	}
	public function index() {
	}
	public function add() {
		if ($this->User_Access_Model->createModule ( $this->session->userdata ( 'role_type' ), Module_Name_Enum::USERS ) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		
		$this->addJs ( 'app/user.js' );
		
		$data = array (
				'mode' => EDIT_MODE 
		);
		$data ['activetab'] = "user";
		$data ['active_menu'] = 'user';
		
		$data ['zone_list'] = $this->Zone_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE 
		), 'name' );
		$data ['entity_list'] = $this->Entity_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE 
		), 'name' );
		$data ['gender_list'] = $this->Data_Attributes_Model->getColumnByKeyValueArray ( 'id,description', array (
				'tableName' => strtoupper ( 'GENDER_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		$data ['role_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'ROLE_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		$data ['experience_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'EXPERIENCE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		$data ['user_model'] = $this->User_Login_Details_Model->getBlankModel ();
		$data ['user_personal_model'] = $this->User_Personal_Details_Model->getBlankModel ();
		$this->render ( 'user/add_edit_user', $data );
	}
	public function getDetailsById($user_id, $view_mode) {
		if ($view_mode == EDIT_MODE) {
			if ($this->User_Access_Model->writeModule ( $this->session->userdata ( 'role_type' ), Module_Name_Enum::USERS ) === FALSE) {
				// redirect 404
				show_error ( "You do not have permission to view this page", '404' );
				return;
			}
		} else {
			if ($this->User_Access_Model->readModule ( $this->session->userdata ( 'role_type' ), Module_Name_Enum::USERS ) === FALSE) {
				// redirect 404
				show_error ( "You do not have permission to view this page", '404' );
				return;
			}
		}
		$data = array ();
		
		$this->addJs ( 'app/user.js' );
		// get API messages from message.php
		$msg_data = $this->config->item ( 'msg' );
		$data = array (
				'mode' => $view_mode 
		);
		
		$data ['activetab'] = "user";
		$data ['active_menu'] = 'user';
		
		$data ['zone_list'] = $this->Zone_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE 
		), 'name' );
		$data ['entity_list'] = $this->Entity_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE 
		), 'name' );
		$data ['gender_list'] = $this->Data_Attributes_Model->getColumnByKeyValueArray ( 'id,description', array (
				'tableName' => strtoupper ( 'GENDER_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		$data ['role_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'ROLE_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		$data ['experience_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'EXPERIENCE' ),
				'isVisible' => Status_Type_Enum::ACTIVE 
		), 'sequenceOrder' );
		if ($this->input->post ( 'id' )) {
			$user_id = $this->input->post ( 'id' );
		}
		if ($user_id > 0) {
			
			if ($view_mode == EDIT_MODE) {
				$locked = $this->User_Login_Details_Model->isLocked ( $user_id );
				if ($locked) {
					$locked_user = $this->User_Login_Details_Model->getLockedUser ( $user_id );
					$locked_date = $this->User_Login_Details_Model->getLockedDate ( $user_id );
					$locked_date = date ( 'jS, F Y G:ia', strtotime ( $locked_date ) );
					$message = $msg_data ['locked'];
					$message = str_replace ( '##USERNAME##', $locked_user, $message );
					$message .= $locked_date;
					
					$data ['mode'] = VIEW_MODE;
				} else {
					$is_locked = $this->User_Login_Details_Model->lock ( $user_id );
				}
			}
			
			$data ['user_model'] = $this->User_Login_Details_Model->getById ( $user_id );
			$data ['user_personal_model'] = $this->User_Personal_Details_Model->getOneByKeyValueArray ( array (
					'userId' => $data ['user_model']->id 
			) );
		} else {
			// Blank Model
			$data ['user_model'] = $this->User_Login_Details_Model->getBlankModel ();
			$data ['user_personal_model'] = $this->User_Personal_Details_Model->getBlankModel ();
		}
		if ($this->input->post ( 'id' )) {
			
			$html = $this->load->view ( 'user/add_edit_user', $data );
			echo json_encode ( array (
					'html' => $html 
			) );
		} else {
			$this->render ( 'user/add_edit_user', $data );
		}
	}
	public function saveUser() {
		$this->addJs ( 'app/user.js' );
		$data = array ();
		$data = $this->input->post ();
		
		// $data ['profileImage']= $this->input->post ('profileImage');
		
		$user_id = $data ['id'];
		
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['failed'];
		if ($user_id > 0) {
			$this->User_Login_Details_Model->update ( $data, array (
					'id' => $user_id 
			) );
			$file_names = array_keys ( $_FILES );
			// $data ['profileImage']= $this->input->post ('profileImage');
			$profile_img_selected = $_FILES ['profileImage'] ['name'];
			
			// upload profile image
			if ($profile_img_selected [0] != '') {
				$prefix = "PROFILE-" . $user_id;
				$upload_path = $this->config->item ( 'user_content_path' );
				$file_upload_path = $upload_path . $user_id;
				file_upload ( $file_names [0], $driver_id, $profile_img_selected, $prefix, $file_upload_path, 'User_Login_Details_Model', 'id' );
			}
			if ($profile_img_selected [0] != '') {
				
				$this->profile_image_upload ( $file_names, $user_id, $profile_img_selected );
			}
			
			$this->User_Personal_Details_Model->update ( $data, array (
					'userId' => $user_id 
			) );
			
			$msg = $msg_data ['updated'];
		} else {
			//$password = random_string ( 'alnum', 8 );
			$password='hellocab';
			$data ['password'] = hash ( "sha256", $password );
			// $data ['userIdentity'] = trim ( substr ( strtoupper ( str_replace ( ' ', '', $data ['firstName'] . '' . $data ['lastName'] ) ), 0, 5 ) );
			// $data ['userIdentity'] = 'TAR'.str_pad ( $this->User_Login_Details_Model->getAutoIncrementValue (), 4, '0', STR_PAD_LEFT );
			
			$user_id = $this->User_Login_Details_Model->insert ( $data );
			
			// update the Unique User Code
			$user_identity_id = ($user_id <= 999999) ? str_pad ( ( string ) $user_id, 6, '0', STR_PAD_LEFT ) : $user_id;
			
			$user_identity = 'USR' . $user_identity_id;
			$update_user_identity = $this->User_Login_Details_Model->update ( array (
					'userIdentity' => $user_identity 
			), array (
					'id' => $user_id 
			) );
			
			$data ['userId'] = $user_id;
			
			$file_names = array_keys ( $_FILES );
			// $data ['profileImage']= $this->input->post ('profileImage');
			$profile_img_selected = $_FILES ['profileImage'] ['name'];
			
			// upload profile image
			if ($profile_img_selected [0] != '') {
				
				$this->profile_image_upload ( $file_names, $user_id, $profile_img_selected );
			}
			
			$user_personal_details_id = NULL;
			if ($user_id > 0) {
				$user_personal_details_id = $this->User_Personal_Details_Model->insert ( $data );
				$msg = $msg_data ['success'];
			}
			if ($user_id > 0 && $user_personal_details_id) {
				// SMS & email service start
				
				if (SMS && $user_id) {
					$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
							'title' => 'account_create_sms' 
					) );
					
					$message_data = $message_details->content;
					
					$message_data = str_replace ( "##USERNAME##", $data ['mobile'], $message_data );
					$message_data = str_replace ( "##PASSWORD##", $password, $message_data );
					$message_data = str_replace ( "##APPLINK##", APPLINK, $message_data );
					
					// print_r($message);exit;
					if ($data ['mobile'] != "") {
						
						$this->Common_Api_Webservice_Model->sendSMS ( $data ['mobile'], $message_data );
					}
				}
				
				/*
				 * $from = SMTP_EMAIL_ID;
				 * $to = $data ['email'];
				 * $subject = 'Get Ready to ride with Zuver!';
				 * $data = array (
				 * 'password' => $password,
				 * 'user_name' => $data ['firstName'] . ' ' . $data ['lastName'],
				 * 'mobile' => $data ['mobile'],
				 * 'email' => $data ['email']
				 * );
				 * $message_data = $this->load->view ( 'emailtemplate/passenger_register', $data, TRUE );
				 *
				 * if (SMTP && $driver_id) {
				 *
				 * if ($to) {
				 * $this->Api_Webservice_Model->sendEmail ( $to, $subject, $message_data );
				 * }
				 * } else {
				 *
				 * // To send HTML mail, the Content-type header must be set
				 * $headers = 'MIME-Version: 1.0' . "\r\n";
				 * $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				 * // Additional headers
				 * $headers .= 'From: Zuver<' . $from . '>' . "\r\n";
				 * $headers .= 'To: <' . $to . '>' . "\r\n";
				 * if (! filter_var ( $to, FILTER_VALIDATE_EMAIL ) === false) {
				 * mail ( $to, $subject, $message_data, $headers );
				 * }
				 * }
				 */
				// SMS & email service end
			}
		}
		
		// $this->render("passenger/add_edit_passenger", $data);
		
		$response = array (
				'msg' => $msg,
				'user_id' => $user_id 
		);
		echo json_encode ( $response );
	}
	public function getUserList() {
		if ($this->User_Access_Model->showUser () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		
		$this->addJs ( 'app/user.js' );
		
		$this->addCss ( 'vendors/lightbox/css/imagelightbox.css' );
		$this->addJs ( 'vendors/lightbox/js/imagelightbox.js' );
		
		$data ['activetab'] = "user";
		$data ['active_menu'] = 'user';
		
		$this->render ( 'user/manage_user', $data );
	}
	public function ajax_list() {
		$sql_query = $this->User_Login_Details_Model->getUserListQuery ();
		$user_details_list = $this->User_Login_Details_Model->getDataTable ( $sql_query );
		$data = array ();
		$no = $_POST ['start'];
		foreach ( $user_details_list as $list ) {
			$no ++;
			$profileimage = ($list->profileImage) ? user_content_url ( $list->userId . "/" . $list->profileImage ) : image_url ( 'app/user.png' );
			$row = array ();
			$row [] = '
		
				  <button class="btn btn-sm btn-success" id="viewuser-' . $list->userId . '"  title="View" ><i class="fa fa-eye" aria-hidden="true"></i></button>
				  <button class="btn btn-sm btn-blue" id="edituser-' . $list->userId . '"  title="Edit" ><i class="fa fa-pencil-square-o"></i></button>
                  <button class="btn btn-sm btn-danger" id="deleteuser-' . $list->userId . '"  title="Delete"><i class="fa fa-trash-o"></i></button>';
			
			$row [] = '<a class="example-image-link" href="' . $profileimage . '" data-imagelightbox="listview" data-ilb2-caption="profileImage">' . '<img src="' . $profileimage . '" width="40" height="40"></a>';
			$row [] = ucfirst ( $list->firstName );
			$row [] = ucfirst ( $list->lastName );
			$row [] = $list->loginEmail;
			$row [] = $list->designation;
			$row [] = $list->mobile;
			$row [] = $list->email;
			$row [] = $list->roleTypeName;
			$row [] = $list->qualification;
			$row [] = $list->experience;
			$row [] = $list->communicationAddress;
			$row [] = $list->alternateMobile;
			$row [] = $list->dob;
			$row [] = $list->doj;
			$row [] = $list->gender;
			$row [] = ucfirst ( $list->zoneName );
			$row [] = ucfirst ( $list->entityName );
			$status_class = ($list->status) ? 'class="btn btn-success"' : 'class="btn btn-danger"';
			$status_name = ($list->status) ? 'Active' : 'Inactive';
			$row [] = '<button id="status-' . $list->userId . '" status="' . $list->status . '" ' . $status_class . '><span>' . $status_name . '</span></button>';
			// add html for action
			/*
			 * $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_person('."'".$list->userId."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
			 * <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('."'".$list->userId."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			 *
			 * $data[] = $row;
			 */
			
			$data [] = $row;
		}
		
		$output = array (
				"draw" => $_POST ['draw'],
				"recordsTotal" => $this->User_Login_Details_Model->getDataTableAllCount ( $sql_query ),
				"recordsFiltered" => $this->User_Login_Details_Model->getDataTableFilterCount ( $sql_query ),
				"data" => $data 
		);
		// output to json format
		echo json_encode ( $output );
	}
	public function changeUserStatus() {
		if ($this->User_Access_Model->fullAccessModule ( $this->session->userdata ( 'role_type' ), Module_Name_Enum::USERS ) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to change status", '404' );
			return;
		}
		$data = array ();
		
		$this->addJs ( 'app/user.js' );
		$user_updated = - 1;
		$data = $this->input->post ();
		$user_id = $data ['id'];
		$user_status = $data ['status'];
		
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['status_change_failed'];
		if ($user_id) {
			$user_updated = $this->User_Login_Details_Model->update ( array (
					'status' => $user_status 
			), array (
					'id' => $user_id 
			) );
			$msg = $msg_data ['status_change_success'];
		}
		$response = array (
				'msg' => $msg,
				'status' => $user_updated 
		);
		echo json_encode ( $response );
	}
	public function deleteUser() {
		if ($this->User_Access_Model->deleteModule ( $this->session->userdata ( 'role_type' ), Module_Name_Enum::USERS ) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to delete", '404' );
			return;
		}
		$data = array ();
		
		$this->addJs ( 'app/user.js' );
		$user_updated = - 1;
		$data = $this->input->post ();
		$user_id = $data ['id'];
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['delete_failed'];
		if ($user_id) {
			$user_updated = $this->User_Login_Details_Model->update ( array (
					'isDeleted' => Status_Type_Enum::ACTIVE 
			), array (
					'id' => $user_id 
			) );
			$msg = $msg_data ['delete_success'];
		}
		$response = array (
				'msg' => $msg,
				'status' => $user_updated 
		);
		echo json_encode ( $response );
	}
	public function checkUserLoginEmail() {
		$data = array ();
		
		$data = $this->input->post ();
		$email = $data ['email'];
		$user_id = $data ['user_id'];
		$email_exists = NULL;
		$status = 0;
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['email_not_exists'];
		if ($email) {
			if ($user_id>0)
			{
			$email_exists = $this->User_Login_Details_Model->getOneByKeyValueArray ( array (
					'loginEmail' => $email,
					'id !='=>$user_id,
					'isDeleted'=>Status_Type_Enum::INACTIVE
			) );
			}
			else 
			{
				$email_exists = $this->User_Login_Details_Model->getOneByKeyValueArray ( array (
						'loginEmail' => $email,
						'isDeleted'=>Status_Type_Enum::INACTIVE
				) );
			}
			if ($email_exists) {
				$status = 1;
				$msg = $msg_data ['email_exists'];
			}
		}
		$response = array (
				'msg' => $msg,
				'email' => $email,
				'status' => $status 
		);
		echo json_encode ( $response );
	}
	public function checkUserMobile() {
		$data = array ();
		
		$data = $this->input->post ();
		$mobile = $data ['mobile'];
		$user_id = $data ['user_id'];
		$mobile_exists = NULL;
		$status = 0;
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['mobile_not_exists'];
		if ($mobile) {
			if ($user_id>0)
			{
			$mobile_exists = $this->User_Personal_Details_Model->getOneByKeyValueArray ( array (
					'mobile' => $mobile,
					'id !='=>$user_id
			) );
			}
			else 
			{
				$mobile_exists = $this->User_Personal_Details_Model->getOneByKeyValueArray ( array (
						'mobile' => $mobile
				) );
			}
			if ($mobile_exists) {
				$status = 1;
				$msg = $msg_data ['mobile_exists'];
			}
		}
		$response = array (
				'msg' => $msg,
				'email' => $mobile,
				'status' => $status 
		);
		echo json_encode ( $response );
	}
	public function changePassword() {
		$this->addJs('app/user.js');
		$data=array();
		$data ['activetab'] = "user";
		$data ['active_menu'] = 'user';
		
		if($this->session->userdata('user_id'))
		{
			$this->render ( 'user/change_user_password', $data );
			
		}
		else
		{
			redirect('dashboard');
		}
	}
	public function changeUserPassword() {
		$data = array ();
		
		$data = $this->input->post ();
		$user_id=$this->session->userdata('user_id');
		$old_password = $data ['oldPassword'];
		$new_password = $data ['newPassword'];
		$confirm_password = $data ['confirmPassword'];
		
		$msg_data = $this->config->item ( 'api' );
		
		if ($old_password && $new_password && $confirm_password) {
			
			$user_exists = $this->User_Login_Details_Model->getById ( $user_id );
			if ($user_exists) {
				$password = $user_exists->password;
				$raw_confirm_password = $confirm_password;
				$confirm_password = hash ( "sha256", $confirm_password );
				
				if ($password != hash ( "sha256", $old_password )) {
					$password_status = - 1;
				} else if ($new_password != $raw_confirm_password) {
					$password_status = - 2;
				} else if ($password == $confirm_password) {
					$password_status = - 3;
				} else {
					$update_password = $this->User_Login_Details_Model->update ( array (
							'password' => $confirm_password 
					), array (
							'id' => $user_exists->id 
					) );
					if ($update_password) {
						$password_status = 1;
					}
				}
			} else {
				$password_status = - 4;
			}
		}
		
		switch ($password_status) {
			case - 1 :
				$message = array (
				"message" => $msg_data ['incorrect_old_password'],
				"status" => - 1
				);
				break;
			case - 2 :
				$message = array (
				"message" => $msg_data ['confirm_password'],
				"status" => - 1
				);
				break;
			case - 3 :
				$message = array (
				"message" => $msg_data ['old_new_password_same'],
				"status" => - 1
				);
				break;
			case 1 :
				$message = array (
				"message" => $msg_data ['password_success'],
				"status" => 1
				);
				break;
			case - 4 :
				$message = array (
				"message" => $msg_data ['invalid_user'],
				"status" => - 1
				);
				break;
		}
		
		echo json_encode ( $message );
	}
}