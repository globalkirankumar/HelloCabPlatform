<?php
class Logout extends My_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in();
		$this->load->helper ( 'form' );
	}
	public function index() {
		// $data = array();
		
		// $this->render("driver/add_edit_driver", $data);
	}
	public function signOut() {
		$this->session->unset_userdata ( 'user_id' );
		$this->session->unset_userdata ( 'user_email' );
		$this->session->unset_userdata ( 'role_type' );
		$this->session->unset_userdata ( 'role_user_id' );
		$this->session->unset_userdata ( 'user_name' );
		$this->session->unset_userdata ( 'user_city' );
		//$this->session->unset_userdata ( 'user_company' );
		$this->session->unset_userdata ( 'user_profile' );
		
		$this->session->sess_destroy();
		
		redirect ( 'login' );
	}
}
