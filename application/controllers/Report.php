<?php

class Report extends My_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->model ( 'v1/Common_Api_Webservice_Model' );
		$this->load->model ( 'Driver_Transaction_Report_Query_Model' );
		$this->load->model ( 'Driver_Taxi_Mapping_Report_Query_Model' );
		$this->load->model ( 'Trip_Details_Report_Query_Model' );
		$this->load->model ( 'Driver_Available_Report_Query_Model' );
		$this->load->model ( 'Driver_Reject_Report_Query_Model' );
		$this->load->model ( 'Driver_Wallet_Report_Query_Model' );
		$this->load->model ( 'Cancelled_Trip_Report_Query_Model' );
		$this->load->model ( 'Sms_Template_Model' );
		$this->load->model ( 'Driver_Model' );
		$this->load->model ( 'Driver_Transaction_Details_Model' );
		$this->load->model ( 'Zone_Details_Model' );
		$this->load->model ( 'Entity_Model' );
		$this->load->model ( 'Entity_Config_Model' );
		$this->load->model ( 'Promocode_Report_Query_Model' );
		$this->load->model ( 'Promocode_Details_Model' );
		$this->load->model ( 'Driver_Credit_Report_Query_Model' );
		$this->load->model ( 'Driver_Daily_Report_Query_Model' );
		
	}
	public function index() {
	}
	
	public function getDriverTransactionReport()
	{
		$data = array ();
			$this->addJs ( 'app/report/driver-transaction-report.js' );
			$data ['activetab'] = "report";
			$data ['active_menu'] = 'report';
			$data ['transaction_mode_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
					'tableName' => strtoupper ( 'TRANSACTION_MODE' ),
					'isVisible' => Status_Type_Enum::ACTIVE
			), 'sequenceOrder' );
			$data ['transaction_from_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
					'tableName' => strtoupper ( 'TRANSACTION_FROM' ),
					'isVisible' => Status_Type_Enum::ACTIVE
			), 'sequenceOrder' );
			$data ['transaction_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
					'tableName' => strtoupper ( 'TRANSACTION_TYPE' ),
					'isVisible' => Status_Type_Enum::ACTIVE
			), 'sequenceOrder' );
			
			unset($data ['transaction_type_list'][Transaction_Type_Enum::TWOC_TWOP]);
			
			$data ['zone_list'] = $this->Zone_Details_Model->getSelectDropdownOptions ( array (
					'status' => Status_Type_Enum::ACTIVE
			), 'name' );
			//$data['device_report']=$this->Report_Query_Model->getDriverTransactionListQuery();
			$this->render ( 'report/driver_transaction_report_layout', $data );
		
	}
	
	public function ajax_list_drivertransaction()
	{
		// Posted json data
		$post_data = $this->input->post ();
		
		$driver_id = $post_data['driver_id'];
		$trnsaction_from = $post_data['transaction_from'];
		$transaction_mode = $post_data['transaction_mode'];
		$transaction_type = $post_data['transaction_type'];
		$start_date = $post_data['start_date'];
		$end_date = $post_data['end_date'];
		$zone_id = $post_data['zone_id'];
		
		$sql_query      = $this->Driver_Transaction_Report_Query_Model->getDriverTransactionListQuery($driver_id,$start_date,$end_date,$trnsaction_from,$transaction_mode,$transaction_type,$zone_id);
		$driver_list    = $this->Driver_Transaction_Report_Query_Model->getDataTable($sql_query);
		$data = array();
		$no = $_POST['start'];
		foreach ($driver_list as $list) {
			$no++;
			$row = array();
			$row[] =$no;
			$row[] = chunk_split($list->driverAccountNo,8);
			$row[] = $list->driverCode;
			$row[] = ucfirst($list->driverName);
			$row[] = $list->driverMobile;
			$row[] = $list->taxiRegistrationNo;
			$row[] = $list->jobCardId;
			$row[] = $list->transactionMode;
			$row[] = $list->previousAmount;
			$row[] = $list->transactionAmount;
			$row[] = $list->currentAmount;
			$row[] = $list->transactionStatus;
			$row[] = $list->transactionFrom;
			$row[] = $list->transactionTypeName;
			$row[] = ($list->transactionType==Transaction_Type_Enum::PROMOCODE)?$list->promoCode:'--';
			
			$row[] = $list->createdDatetime;
			$row[] = chunk_split($list->transactionId,8);
			$row[] = $list->comments;
			$row[] = $list->zoneName;
			//add html for action
	
			$data[] = $row;
		}
	
		$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->Driver_Transaction_Report_Query_Model->getDataTableAllCount($sql_query),
				"recordsFiltered" => $this->Driver_Transaction_Report_Query_Model->getDataTableFilterCount($sql_query),
				"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	
	public function getDriverDetails() {
		$keyword = $this->input->post ( 'keyword' );
		$data ['driver_info'] = $this->Driver_Model->getDriverDetails( $keyword);
		if (! empty ( $data ['driver_info'] )) {
			echo "<ul id='passenger-autocompletelist' style=''>";
			foreach ( $data ['driver_info'] as $list ) {
				$driver_name = $list->driverName;
				$driver_id = $list->driverId;
				$driver_mobile = $list->driverMobile;
				$driver_code = $list->driverCode;
				$driver_email = $list->driverEmail;
				$driver_data=(trim($driver_mobile)!='')?$driver_name."(".$driver_mobile.")": $driver_name;
				echo "<li onClick=\"selectDriver('$driver_id','$driver_mobile','$driver_email','$driver_name','$driver_code');\"> $driver_data </li> ";
			}
			echo "</ul>";
		}
	}
	
	public function getDriverTaxiMappingReport()
	{
		$data = array ();
		$this->addJs ( 'app/report/driver-taxi-mapping-report.js' );
		$data ['activetab'] = "report";
		$data ['active_menu'] = 'report';
		$data ['transaction_mode_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRANSACTION_MODE' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		$data ['transaction_from_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRANSACTION_FROM' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		$data ['zone_list'] = $this->Zone_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE
		), 'name' );
		//$data['device_report']=$this->Report_Query_Model->getDriverTransactionListQuery();
		$this->render ( 'report/driver_taxi_mapping_report_layout', $data );
	
	}
	
	public function ajax_list_drivertaximapping()
	{
		// Posted json data
		$post_data = $this->input->post ();
	
		/* $driver_id = $post_data['driver_id'];
		$trnsaction_from = $post_data['transaction_from'];
		$transaction_mode = $post_data['transaction_mode'];
		$start_date = $post_data['start_date'];
		$end_date = $post_data['end_date'];
		$zone_id = $post_data['zone_id'];*/
	
		$sql_query      = $this->Driver_Taxi_Mapping_Report_Query_Model->getDriverTaxiMappingQuery();
		$driver_list    = $this->Driver_Taxi_Mapping_Report_Query_Model->getDataTable($sql_query);
		$data = array();
		$no = $_POST['start'];
		foreach ($driver_list as $list) {
			$no++;
			$row = array();
			
			
			$row[] = $list->driverCode;
			$row[] = ucfirst($list->driverName);
			$row[] = $list->driverMobile;
			$row[] = $list->zoneName;
			$row[] = $list->taxiRegistrationNo;
			$row[] = $list->totalCompletedTrip;
			$row[] = $list->totalRejectedTrip;
			$row[] = $list->weekCompletedTrip;
			$row[] = $list->weekRejectedTrip;
			$row[] = $list->taxiOwnerCode;
			$row[] = $list->taxiOwnerName;
			$row[] = $list->taxiOwnerMobile;
			$row[] = $list->taxiDeviceCode;
			$row[] = $list->taxiDeviceImeiNo;
			$row[] = $list->taxiDeviceMobile;
			
			//add html for action
	
			$data[] = $row;
		}
	
		$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->Driver_Taxi_Mapping_Report_Query_Model->getDataTableAllCount($sql_query),
				"recordsFiltered" => $this->Driver_Taxi_Mapping_Report_Query_Model->getDataTableFilterCount($sql_query),
				"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	public function getTripDetailsReport()
	{
		$data = array ();
		$this->addJs ( 'app/report/trip-details-report.js' );
		$data ['activetab'] = "report";
		$data ['active_menu'] = 'report';
		$data ['entity_list'] = $this->Entity_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE,
				'isDeleted' => Status_Type_Enum::INACTIVE
		), 'name' );
		$data ['zone_list'] = $this->Zone_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE,
				'isDeleted' => Status_Type_Enum::INACTIVE
		), 'name' );
		//$data['device_report']=$this->Report_Query_Model->getDriverTransactionListQuery();
		$this->render ( 'report/trip_details_report_layout', $data );
	
	}
	
	public function ajax_list_tripdetails()
	{
		// Posted json data
		$post_data = $this->input->post ();
	
		$start_date = $post_data['start_date'];
		$end_date = $post_data['end_date'];
		$entity_id = $post_data['entity_id'];
		$zone_id = $post_data['zone_id'];
		
		$total_unassigned_trip=0;
		$total_upcoming_trip=0;
		$total_ongoing_trip=0;
		$total_completed_trip=0;
		$total_passenger_cancelled_trip=0;
		$total_driver_cancelled_trip=0;
		$total_driver_rejected_trip=0;
		$total_driver_not_found_trip=0;
		$total_trip_count=0;
		$total_ops_booking_count=0;
		$total_app_booking_count=0;
	
		$sql_query      = $this->Trip_Details_Report_Query_Model->getTripDetailsListQuery($start_date,$end_date,$entity_id,$zone_id);
		$trip_list    = $this->Trip_Details_Report_Query_Model->getDataTable($sql_query);
		$data = array();
		$no = $_POST['start'];
		foreach ($trip_list as $list) {
			$total_unassigned_trip+=$list->totalUnassignedTrip;
			$total_upcoming_trip+=$list->totalUpcomingTrip;
			$total_ongoing_trip+=$list->totalOngoingTrip;
			$total_completed_trip+=$list->totalCompletedTrip;
			$total_passenger_cancelled_trip+=$list->totalPassengerCancelledTrip;
			$total_driver_cancelled_trip+=$list->totalDriverCancelledTrip;
			$total_driver_rejected_trip+=$list->totalDriverRejectedTrip;
			$total_driver_not_found_trip+=$list->totalDriverNotFoundTrip;
			$total_trip_count+=$list->totalTripCount;
			$total_ops_booking_count+=$list->totalOpsBookingTrip;
			$total_app_booking_count+=$list->totalMobileBookingTrip;
			$no++;
			$row = array();
			$row[] = ucfirst($list->zoneName);
			$row[] = $list->totalUnassignedTrip;
			$row[] = $list->totalUpcomingTrip;
			$row[] = $list->totalOngoingTrip;
			$row[] = $list->totalCompletedTrip;
			$row[] = $list->totalPassengerCancelledTrip;
			$row[] = $list->totalDriverCancelledTrip;
			$row[] = $list->totalDriverRejectedTrip;
			$row[] = $list->totalDriverNotFoundTrip;
			$row[] = $list->totalTripCount;
			$row[] = round(($list->totalOpsBookingTrip*100/$list->totalTripCount),2).'%';
			$row[] = round(($list->totalMobileBookingTrip*100/$list->totalTripCount),2).'%';
			
			//add html for action
			$data[] = $row;
		}
		$row = array();
		$row[] = "<strong>Total</strong>";
		$row[] = "<strong>".$total_unassigned_trip."</strong>";
		$row[] = "<strong>".$total_upcoming_trip."</strong>";
		$row[] = "<strong>".$total_ongoing_trip."</strong>";
		$row[] = "<strong>".$total_completed_trip."</strong>";
		$row[] = "<strong>".$total_passenger_cancelled_trip."</strong>";
		$row[] = "<strong>".$total_driver_cancelled_trip."</strong>";
		$row[] = "<strong>".$total_driver_rejected_trip."</strong>";
		$row[] = "<strong>".$total_driver_not_found_trip."</strong>";
		$row[] = "<strong>".$total_trip_count."</strong>";
		$row[] = "<strong>".round(($total_ops_booking_count*100/$total_trip_count),2)."%</strong>";
		$row[] = "<strong>".round(($total_app_booking_count*100/$total_trip_count),2)."%</strong>";
		$data[] = $row;
		$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->Trip_Details_Report_Query_Model->getDataTableAllCount($sql_query),
				"recordsFiltered" => $this->Trip_Details_Report_Query_Model->getDataTableFilterCount($sql_query),
				"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	
	public function getDriverAvailableReport()
	{
		$data = array ();
		$this->addJs ( 'app/report/driver-available-report.js' );
		$data ['activetab'] = "report";
		$data ['active_menu'] = 'report';
		$data ['time_period_list'] = array(''=>'--Select--','6000'=>'Last 1 hour','12000'=>'Last 2 hours','36000'=>'Last 6 hours','72000'=>'Last 12 hours','144000'=>'Last 24 hours');
		$data ['log_status_list'] =array(''=>'--Select--','0'=>'Log Out','1'=>'Log In');
		$data ['available_status_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TAXI_AVAILABLE_STATUS' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		
		$this->render ( 'report/driver_available_report_layout', $data );
	
	}
	
	public function ajax_list_driveravailable()
	{
		// Posted json data
		$post_data = $this->input->post ();
		$log_status=1;
		if (array_key_exists('log_status', $post_data) && $post_data['log_status']!='')
		{
			$log_status = $post_data['log_status'];
		}
		$available_status = $post_data['available_status'];
		//$time_period = $post_data['time_period'];
		$time_period=200;
		$sql_query      = $this->Driver_Available_Report_Query_Model->getDriverAvailableQuery($time_period,$log_status,$available_status);
		$driver_list    = $this->Driver_Available_Report_Query_Model->getDataTable($sql_query);
		$data = array();
		$no = $_POST['start'];
		foreach ($driver_list as $list) {
			
			$no++;
			$row = array();
			$row[] = $list->driverCode;
			$row[] = ucfirst($list->driverName);
			$row[] = $list->driverMobile;
			$row[] = $list->driverWallet;
			$row[] = $list->appVersion;
			$row[] = $list->logStatusName;
			$row[] = $list->lastLoggedIn;
			$row[] = $list->availabilityStatusName;
			$row[] = $list->jobCardId;
			$row[] = $list->taxiRegistrationNo;
			$row[] = $list->taxiDeviceCode;
			$row[] = $list->taxiDeviceMobile;
				
			//add html for action
			$data[] = $row;
		}
		
		$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->Driver_Available_Report_Query_Model->getDataTableAllCount($sql_query),
				"recordsFiltered" => $this->Driver_Available_Report_Query_Model->getDataTableFilterCount($sql_query),
				"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	
	public function getDriverRejectReport()
	{
		$data = array ();
		$this->addJs ( 'app/report/driver-reject-report.js' );
		$data ['activetab'] = "report";
		$data ['active_menu'] = 'report';
		$data ['time_period_list'] = array(''=>'--Select--','6000'=>'Last 1 hour','12000'=>'Last 2 hours','36000'=>'Last 6 hours','72000'=>'Last 12 hours','144000'=>'Last 24 hours');
		$entity_config_details=$this->Entity_Config_Model->getOneByKeyValueArray(array('entityId'=>DEFAULT_COMPANY_ID));
		for($i=0;$i<=$entity_config_details->drtSecondLimit;$i++)
		{
			$data ['consecutive_reject_list'][]=$i;
		}
		$this->render ( 'report/driver_reject_report_layout', $data );
	
	}
	
	public function ajax_list_driverreject()
	{
		// Posted json data
		$post_data = $this->input->post ();
	
		//$available_status = $post_data['available_status'];
		$time_period = $post_data['time_period'];
		$consecutive_reject=$post_data['consecutive_reject'];
		$sql_query      = $this->Driver_Reject_Report_Query_Model->getDriverRejectQuery($time_period,$consecutive_reject);
		$driver_list    = $this->Driver_Reject_Report_Query_Model->getDataTable($sql_query);
		$data = array();
		$no = $_POST['start'];
		foreach ($driver_list as $list) {
				
			$no++;
			$row = array();
			$row[] = $list->driverCode;
			$row[] = ucfirst($list->driverName);
			$row[] = $list->driverMobile;
			$row[] = $list->statusName;
			$row[] = $list->consecutiveRejectCount;
			$row[] = $list->taxiRegistrationNo;
			$row[] = $list->taxiDeviceCode;
			$row[] = $list->taxiDeviceMobile;
	
			//add html for action
			$data[] = $row;
		}
	
		$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->Driver_Reject_Report_Query_Model->getDataTableAllCount($sql_query),
				"recordsFiltered" => $this->Driver_Reject_Report_Query_Model->getDataTableFilterCount($sql_query),
				"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	
	public function getDriverWalletReport()
	{
		$data = array ();
		$this->addJs ( 'app/report/driver-wallet-warning-report.js' );
		$data ['activetab'] = "report";
		$data ['active_menu'] = 'report';
		$data ['transaction_mode_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRANSACTION_MODE' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		$data ['transaction_from_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRANSACTION_FROM' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		$data ['zone_list'] = $this->Zone_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE
		), 'name' );
		//$data['device_report']=$this->Report_Query_Model->getDriverTransactionListQuery();
		$this->render ( 'report/driver_wallet_warning_report_layout', $data );
	
	}
	
	public function ajax_list_driverwalletwarning()
	{
		// Posted json data
		$entity_id = $this->session->userdata('user_entity');
		$sql_query      = $this->Driver_Wallet_Report_Query_Model->getDriverWalletWarningQuery($entity_id);
		$driver_list    = $this->Driver_Wallet_Report_Query_Model->getDataTable($sql_query);
		$data = array();
		$no = $_POST['start'];
		foreach ($driver_list as $list) {
			$no++;
			$row = array();
				
				
			$row = array();
			$row[] = $list->driverCode;
			$row[] = ucfirst($list->driverName);
			$row[] = $list->driverMobile;
			$row[] = $list->driverWallet;
			$row[] = round($list->WalletPercentage,2).' %';
			$row[] = $list->taxiRegistrationNo;
			$row[] = $list->taxiDeviceCode;
			$row[] = $list->taxiDeviceMobile;
				
			//add html for action
	
			$data[] = $row;
		}
	
		$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->Driver_Wallet_Report_Query_Model->getDataTableAllCount($sql_query),
				"recordsFiltered" => $this->Driver_Wallet_Report_Query_Model->getDataTableFilterCount($sql_query),
				"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	
	public function getCancelledTripReport()
	{
		$data = array ();
		$this->addJs ( 'app/report/cancelled-trip-report.js' );
		$data ['activetab'] = "report";
		$data ['active_menu'] = 'report';
		$data ['trip_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRIP_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		$data ['trip_status_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRIP_STATUS' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		unset($data ['trip_status_list'][Trip_Status_Enum::BOOKED]);
		unset($data ['trip_status_list'][Trip_Status_Enum::NO_NOTIFICATION]);
		unset($data ['trip_status_list'][Trip_Status_Enum::DRIVER_ACCEPTED]);
		unset($data ['trip_status_list'][Trip_Status_Enum::DRIVER_ARRIVED]);
		unset($data ['trip_status_list'][Trip_Status_Enum::DRIVER_DISPATCHED]);
		unset($data ['trip_status_list'][Trip_Status_Enum::IN_PROGRESS]);
		unset($data ['trip_status_list'][Trip_Status_Enum::QUEUING]);
		unset($data ['trip_status_list'][Trip_Status_Enum::WAITING_FOR_PAYMENT]);
		$this->render ( 'report/cancelled_trip_report_layout', $data );
	
	}
	
	public function ajax_list_cancelledtrip()
	{
	// Posted json data
		$post_data = $this->input->post ();
	//debug_exit($post_data);
		//$available_status = $post_data['available_status'];
		$start_date = $post_data['start_date'];
		$end_date = $post_data['end_date'];
		$trip_type=$post_data['trip_type'];
		$trip_status=$post_data['trip_status'];
		$sql_query      = $this->Cancelled_Trip_Report_Query_Model->getCancelledTripReportQuery($start_date,$end_date,$trip_type,$trip_status);
			$driver_list    = $this->Cancelled_Trip_Report_Query_Model->getDataTable($sql_query);
			$data = array();
			$no = $_POST['start'];
			foreach ($driver_list as $list) {
	
			$no++;
			$row = array();
			$row[] = $list->jobCardId;
			$row[] = ucfirst($list->driverName);
			$row[] = $list->tripTypeName;
			$row[] = $list->tripStatusName;
			$row[] = $list->rejectedDrivers;
			$row[] = $list->requestedPickUpDatetime;
			$row[] = $list->dispatchedDatetime;
			$row[] = ($list->tripType==Trip_Type_Enum::LATER && $list->tripStatus==Trip_Status_Enum::DRIVER_NOT_FOUND)?$list->lastCronJob:'--';
	
			//add html for action
			$data[] = $row;
		}
	
		$output = array(
		"draw" => $_POST['draw'],
		"recordsTotal" => $this->Cancelled_Trip_Report_Query_Model->getDataTableAllCount($sql_query),
		"recordsFiltered" => $this->Cancelled_Trip_Report_Query_Model->getDataTableFilterCount($sql_query),
		"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	
	public function getPromocodeReport()
	{
		$data = array ();
		$this->addJs ( 'app/report/promocode-report.js' );
		$data ['activetab'] = "report";
		$data ['active_menu'] = 'report';
		$promo_end_date = getCurrentUnixDateTime ();
		$promo_end_date = $promo_end_date - (60 * 14400);
		$promo_end_date=date('Y-m-d H:i:s',$promo_end_date);
		//debug(getCurrentDateTime());debug_exit($promo_end_date);
		$data ['promocode_list'] = $this->Promocode_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE,
				'promoStartDatetime<='=>getCurrentDateTime(),
				'promoEndDatetime>='=>$promo_end_date
		), 'promoCode' );
		$data ['payment_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'PAYMENT_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
			
		$this->render ( 'report/promocode_report_layout', $data );
	
	}
	
	public function ajax_list_promocode()
	{
		// Posted json data
		$post_data = $this->input->post ();
		//debug_exit($post_data);
		//$available_status = $post_data['available_status'];
		$promocode = $post_data['promocode'];
		$sql_query      = $this->Promocode_Report_Query_Model->getPromocodeReportQuery($promocode);
		$driver_list    = $this->Promocode_Report_Query_Model->getDataTable($sql_query);
		$data = array();
		$no = $_POST['start'];
		foreach ($driver_list as $list) {
	
			$no++;
			$row = array();
			$href="TripTransaction/getDetailsById/". $list->transactionId ."/view";
			$row[] = '<a href="'.base_url($href).'" target="blank" style="text-decoration: none;">'.$list->invoiceNo.'</a>';
			$row[] = $list->jobCardId;
			$row[] = number_format($list->totalTripCharge+$list->promoDiscountAmount,2);
			$row[] = number_format($list->promoDiscountAmount,2);
			$row[] = number_format($list->paidTripCharge,2);
			$row[] = $list->passengerCode;
			$passenger_mobile=($list->passengerMobile)?'('.$list->passengerMobile.')':'';
			$row[] = ucfirst($list->passengerName)." ".$passenger_mobile;
			$row[] = $list->driverCode;
			$driver_mobile=($list->driverMobile)?'('.$list->driverMobile.')':'';
			$row[] = ucfirst($list->driverName)." ".$driver_mobile;
			
			//add html for action 
			$data[] = $row;
		}
	
		$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->Promocode_Report_Query_Model->getDataTableAllCount($sql_query),
				"recordsFiltered" => $this->Promocode_Report_Query_Model->getDataTableFilterCount($sql_query),
				"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	
	public function getPromocodeDetails()
	{
		$data = $this->input->post();
		
		// debug($data);
		$promocode = $data ['promocode'];
		$promocode_type_list = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'PROMOCODE_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		$zone_list = $this->Zone_Details_Model->getSelectDropdownOptions ( array (
				'id!=' => Status_Type_Enum::INACTIVE
		), 'id' );
		$entity_list = $this->Entity_Model->getSelectDropdownOptions ( array (
				'id!=' => Status_Type_Enum::INACTIVE
		), 'id' );
		$promocode_details=NULL;
		$status=FALSE;
		$promocode_details=$this->Promocode_Details_Model->getById($promocode);
		$promocode_details->promocodeTypeName=$promocode_type_list[$promocode_details->promocodeType];
		$promocode_details->zoneName1='All Zones';
		if($promocode_details->zoneId)
		{
		$promocode_details->zoneName1=$zone_list[$promocode_details->zoneId];
		}
		$promocode_details->zoneName2='';
		if ($promocode_details->promocodeType==Promocode_Type_Enum::COMBINATION)
		{
		$promocode_details->zoneName2=$zone_list[$promocode_details->zoneId2];
		}
		$promocode_details->entityName=$entity_list[$promocode_details->entityId];
		
		if ($promocode_details)
		{
			$status=TRUE;
		}
		$response = array (
				'data' => $promocode_details,
				'status' => $status
		);
		echo json_encode ( $response );
	}
	
	public function getDriverCreditReport()
	{
		$data = array ();
		$this->addJs ( 'app/report/driver-credit-report.js' );
		$data ['activetab'] = "report";
		$data ['active_menu'] = 'report';
		
		$this->render ( 'report/driver_credit_report_layout', $data );
	
	}
	
	public function ajax_list_drivercredit()
	{
		// Posted json data
		$post_data = $this->input->post ();
	
		/* $driver_id = $post_data['driver_id'];
		$trnsaction_from = $post_data['transaction_from'];
		$transaction_mode = $post_data['transaction_mode'];
		$start_date = $post_data['start_date'];
		$end_date = $post_data['end_date'];
		$zone_id = $post_data['zone_id'];*/
	
		$sql_query      = $this->Driver_Credit_Report_Query_Model->getDriverCreditReportQuery();
		$driver_list    = $this->Driver_Credit_Report_Query_Model->getDataTable($sql_query);
		$data = array();
		$no = $_POST['start'];
		$count=1;
		foreach ($driver_list as $list) {
			$no++;
			$row = array();
			$row[] = $count;
			$credit_status_class = ($list->driverCredit>0) ? 'class="btn btn-danger"':'class="btn btn-success"';
			$credit_status_name = ($list->driverCredit>0) ? 'Clear Credit' : 'Cleared';
			$credit_status_id = ($list->driverCredit>0) ? 'id="clearcredit-' . $list->driverId . '"' : '';
			$row [] = '<button '.$credit_status_id .' ' . $credit_status_class . '><span>' . $credit_status_name . '</span></button>';
			$row[] = $list->driverCode;
			$row[] = ucfirst($list->driverName);
			$row[] = $list->driverMobile;
			$row[] = chunk_split($list->bankAccountNo,8);
			$row[] = 1;
			$row[] = $list->bankBranch;
			$row[] = $list->currencyCode;
			$row[] = ($list->driverName)?stringInitials($list->driverName):'';
			$row[] = ($list->driverName)?stringInitials($list->driverName):'';
			$row[] = 'CR';
			$row[] = $list->driverCredit;
			
			//add html for action
			$count++;
			$data[] = $row;
		}
	
		$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->Driver_Credit_Report_Query_Model->getDataTableAllCount($sql_query),
				"recordsFiltered" => $this->Driver_Credit_Report_Query_Model->getDataTableFilterCount($sql_query),
				"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	
	public function clearDriverCredit()
	{
		if ($this->User_Access_Model->deleteModule ( $this->session->userdata ( 'role_type' ), Module_Name_Enum::DRIVER ) === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to delete", '404' );
			return;
		}
		$data = array ();
	
		$this->addJs ( 'app/report/driver-credit-report.js' );
		$driver_updated = - 1;
		$data = $this->input->post ();
		$driver_id = $data ['id'];
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['credit_failed'];
		if ($driver_id) {
			$current_driver_wallet_amount = $this->Driver_Model->getById ( $driver_id );
			$driver_updated = $this->Driver_Model->update ( array (
					'driverCredit' => Status_Type_Enum::INACTIVE
			), array (
					'id' => $driver_id
			) );
			if ($driver_updated)
			{
				
				$insert_driver_transaction = array (
						'driverId' => $driver_id,
						'transactionAmount' => $current_driver_wallet_amount->driverCredit,
						'previousAmount' => $current_driver_wallet_amount->driverCredit,
						'currentAmount' => ($current_driver_wallet_amount->driverCredit - $current_driver_wallet_amount->driverCredit),
						'transactionStatus' => 'Success',
						'transactionType' => Transaction_Type_Enum::BANK,
						'transactionFrom' => Transaction_From_Enum::CREDIT_ACCOUNT,
						'transactionMode' => Transaction_Mode_Enum::CREDIT
				);
				$update_driver_transaction = $this->Driver_Transaction_Details_Model->insert ( $insert_driver_transaction );
					
			}
			$msg = $msg_data ['credit_success'];
		}
		$response = array (
				'msg' => $msg,
				'status' => $driver_updated
		);
		echo json_encode ( $response );
	
	}
	
	public function getDriverDailyReport()
	{
		$data = array ();
		$this->addJs ( 'app/report/driver-daily-report.js' );
		$data ['activetab'] = "report";
		$data ['active_menu'] = 'report';
	
		$this->render ( 'report/driver_daily_report_layout', $data );
	
	}
	public function ajax_list_driverdaily()
	{
		// Posted json data
		$post_data = $this->input->post ();
		$report_date = $post_data['report_date'];
		
		$sql_query      = $this->Driver_Daily_Report_Query_Model->getDriverDailyReportQuery($report_date);
		$driver_list    = $this->Driver_Daily_Report_Query_Model->getDataTable($sql_query);
		$data = array();
		$no = $_POST['start'];
		$count=1;
		foreach ($driver_list as $list) {
			$no++;
			$row = array();
			
			$row[] = $list->driverCode;
			$row[] = ucfirst($list->driverName);
			$row[] = $list->driverMobile;
			$row[] = $list->taxiRegistrationNo;
			$row[] = $list->hour;
			$row[] = $list->minute;
				
			//add html for action
			$count++;
			$data[] = $row;
		}
	
		$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->Driver_Daily_Report_Query_Model->getDataTableAllCount($sql_query),
				"recordsFiltered" => $this->Driver_Daily_Report_Query_Model->getDataTableFilterCount($sql_query),
				"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
}