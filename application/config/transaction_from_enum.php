<?php
require_once(APPPATH . 'config/base_enum.php');

class Transaction_From_Enum extends Base_Enum {

	const
	WALLET_ACCOUNT =  118,
	CREDIT_ACCOUNT =  119,
	CASH =  123;
	
}