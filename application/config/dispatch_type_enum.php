<?php
require_once(APPPATH . 'config/base_enum.php');

class Dispatch_Type_Enum extends Base_Enum {

	const
	ROUND_ROBIN = 113,
	FIFO = 114;
		
}