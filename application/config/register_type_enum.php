<?php
require_once(APPPATH . 'config/base_enum.php');

class Register_Type_Enum extends Base_Enum {

	const
	NONE = 21,
	MANUAL = 22,
	FACEBOOK = 23,
	GOOGLE = 24,
	OTHERS = 25;
	
	
	
}