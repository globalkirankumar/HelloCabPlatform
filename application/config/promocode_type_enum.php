<?php
require_once(APPPATH . 'config/base_enum.php');

class Promocode_Type_Enum extends Base_Enum {

	const
	GENERIC = 126,
	COMBINATION  = 127;
		
}