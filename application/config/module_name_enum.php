<?php
require_once(APPPATH . 'config/base_enum.php');

class Module_Name_Enum extends Base_Enum {

	const
	CUSTOMER = 1,
	DRIVER =  2,
	TAXI = 3,
	TAXI_DEVICE = 4,
	TAXI_OWNER =5,
	TRIP =6,
	PROMOCODE=7,
	USERS=8,
	REPORTS=9,
	TAXI_TARIFF=10,
	SOS=11,
	ENTITY=12,
	ZONE=13,
	COUNTRY=14,
	NOTIFICATION=15,
	MODULE=16,
	ADMIN=17;
	
}