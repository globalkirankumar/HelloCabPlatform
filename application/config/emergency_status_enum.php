<?php
require_once(APPPATH . 'config/base_enum.php');

class Emergency_Status_Enum extends Base_Enum {

	const
	OPEN =  95,
	INACTION = 96,
	PENDING =  97,
	WRONGREQUEST =  98,
	CLOSED = 99;
	
		
}