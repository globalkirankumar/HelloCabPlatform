<?php
require_once(APPPATH . 'config/base_enum.php');

class Device_Type_Enum extends Base_Enum {

	const
	NO_DEVICE = 12,
	ANDROID  = 13,
	IPHONE = 14,
	WINDOWS = 15,
	OTHERS = 16;
	
}