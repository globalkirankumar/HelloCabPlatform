<?php
require_once(APPPATH . 'config/base_enum.php');

class License_Type_Enum extends Base_Enum {

	const
	NONE = 26,
	NONTRANSPORT  =27,
	TRANSPORT = 28;
	
}