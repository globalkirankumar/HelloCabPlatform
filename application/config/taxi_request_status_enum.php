<?php
require_once(APPPATH . 'config/base_enum.php');

class Taxi_Request_Status_Enum extends Base_Enum {

	const
	AVAILABLE_TRIP =   33,
	DRIVER_NOT_FOUND  =   34,
	DRIVER_ACCEPTED = 35,
	DRIVER_REJECTED = 36,
	PASSENGER_CANCELLED = 37,
	COMPLETED_TRIP = 38;
	
}