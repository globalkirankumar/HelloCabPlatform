<?php
require_once(APPPATH . 'config/base_enum.php');

class Trip_Type_Enum extends Base_Enum {

	const
	NOW =   44,
	LATER  =   45;
	
}