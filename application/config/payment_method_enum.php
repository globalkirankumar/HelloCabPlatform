<?php
require_once(APPPATH . 'config/base_enum.php');

class Payment_Method_Enum extends Base_Enum {

	const
	NONE =  68,
	DIRECT =  69,
	INDIRECT = 70;
	
		
}