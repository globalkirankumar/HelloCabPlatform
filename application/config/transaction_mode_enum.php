<?php
require_once(APPPATH . 'config/base_enum.php');

class Transaction_Mode_Enum extends Base_Enum {

	const
	NONE = 58,
	CREDIT  = 59,
	DEBIT = 60;
}