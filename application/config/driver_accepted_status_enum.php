<?php
require_once(APPPATH . 'config/base_enum.php');

class Driver_Accepted_Status_Enum extends Base_Enum {

	const
	NONE = 29,
	ACCEPTED =   30,
	REJECTED  =   31,
	TIMEOUT = 32;
	
}