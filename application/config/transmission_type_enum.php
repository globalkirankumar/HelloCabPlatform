<?php
require_once(APPPATH . 'config/base_enum.php');

class Transmission_Type_Enum extends Base_Enum {

	const
	NONE = 87,
	MANUAL =   88,
	AUTOMATIC = 89;
	
}