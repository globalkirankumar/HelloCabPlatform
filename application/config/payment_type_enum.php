<?php
require_once(APPPATH . 'config/base_enum.php');

class Payment_Type_Enum extends Base_Enum {

	const
	NONE = 71,
	AMOUNT = 72,
	PERCENTAGE  = 73;
	
		
}