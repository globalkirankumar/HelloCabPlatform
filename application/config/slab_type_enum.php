<?php
require_once (APPPATH . 'config/base_enum.php');
class Slab_Type_Enum extends Base_Enum {
	const NONE = 105, 
	DISTANCE = 106, 
	TIME = 107, 
	SURGE = 108;
}