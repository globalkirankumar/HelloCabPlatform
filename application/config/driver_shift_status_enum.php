<?php
require_once(APPPATH . 'config/base_enum.php');

class Driver_Shift_Status_Enum extends Base_Enum {

	const
	IN='IN',
	OUT='OUT',
	SHIFTIN = 42,
	SHIFTOUT  = 43;
	
	
}