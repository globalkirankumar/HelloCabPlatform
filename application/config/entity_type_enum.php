<?php
require_once(APPPATH . 'config/base_enum.php');

class Entity_Type_Enum extends Base_Enum {

	const
	ENTITY  = 10,
	PARTNER = 11;
}