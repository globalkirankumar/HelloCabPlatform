<?php
require_once(APPPATH . 'config/base_enum.php');

class Taxi_Available_Status_Enum extends Base_Enum {

	const
	NOTAVAILABLE = 39,
	FREE  = 40,
	BUSY = 41,
	FREESTATUS  = 'F',
	BUSYSTATUS = 'B';
	
}