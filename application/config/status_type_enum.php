<?php
require_once(APPPATH . 'config/base_enum.php');

class Status_Type_Enum extends Base_Enum {

	const
	ACTIVE =   1,
	INACTIVE  =   0;
	
}