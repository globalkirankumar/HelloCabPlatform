<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Image file upload deafult configuration 
|--------------------------------------------------------------------------
|
*/
$config['image']=array('allowed_types'=>'png|jpg|jpeg',
		'max_size'=>'5120',
		'max_width'=>'',
		'max_height'=>''
);
/*
 |--------------------------------------------------------------------------
 | Video file upload default configuration
 |--------------------------------------------------------------------------
 |
 */
$config['video']=array('allowed_types'=>'mp4|mpeg',
		'max_size'=>'15360',
		'max_width'=>'',
		'max_height'=>''
);

/*
|--------------------------------------------------------------------------
| Audio file upload deafult configuration
|--------------------------------------------------------------------------
|
*/

$config['audio']=array('allowed_types'=>'mp3|wav|amr',
		'max_size'=>'5120',
		'max_width'=>'',
		'max_height'=>''
);

/*
|--------------------------------------------------------------------------
| Document file upload deafult configuration
|--------------------------------------------------------------------------
|
*/

$config['doc']=array('allowed_types'=>'doc|docx|ppt|pptx|xls|xlsx|ods',
		'max_size'=>'500',
		'max_width'=>'',
		'max_height'=>''
);

/*
|--------------------------------------------------------------------------
| PDF file upload deafult configuration
|--------------------------------------------------------------------------
|
*/

$config['pdf']=array('allowed_types'=>'pdf',
		'max_size'=>'5120',
		'max_width'=>'',
		'max_height'=>''
);

