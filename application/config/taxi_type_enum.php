<?php
require_once(APPPATH . 'config/base_enum.php');

class Taxi_Type_Enum extends Base_Enum {

	const
	NONE = 79,
	STANDARD = 80,
	HATCHBACKS  =  81,
	SEDAN = 82,
	MUV = 83,
	SUV = 84,
	LUXURY = 85,
	OTHERS = 86;
		
}