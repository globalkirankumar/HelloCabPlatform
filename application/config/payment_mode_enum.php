<?php
require_once(APPPATH . 'config/base_enum.php');

class Payment_Mode_Enum extends Base_Enum {

	const
	NONE = 74,
	CASH = 75,
	WALLET = 76,
	AGD = 77,
	INVOICE = 78;
		
}