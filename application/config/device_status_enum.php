<?php
require_once(APPPATH . 'config/base_enum.php');

class Device_Status_Enum extends Base_Enum {

	const
	NONE = 90,
	OPEN = 91,
	ONSERVICE = 92,
	ONREPAIR = 93,
	DAMAGED = 94;
		
}