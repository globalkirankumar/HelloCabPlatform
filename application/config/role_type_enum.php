<?php
require_once(APPPATH . 'config/base_enum.php');

class Role_Type_Enum extends Base_Enum {

	const
	STAFF =  5,
	SUPERVISOR  =  4,
	MANAGER = 3,
	ACCOUNTANT = 6,
	PARTNER = 7,
	ADMIN = 2,
	SUPER_ADMIN = 1;
	
}