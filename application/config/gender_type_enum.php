<?php
require_once(APPPATH . 'config/base_enum.php');

class Gender_Type_Enum extends Base_Enum {

	const
	MALE = 9,
	FEMALE  =  8;
		
}