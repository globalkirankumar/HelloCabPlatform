<?php
require_once(APPPATH . 'config/base_enum.php');

class Experience_Enum extends Base_Enum {

	const
	BELOW_ONE = 100,
	ONE_TO_THREE = 101,
	THREE_TO_FIVE = 102,
	FIVE_TO_TEN = 103,
	TEN_MORE = 104;
}