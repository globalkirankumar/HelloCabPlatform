<?php
class User_Login_Details_Model extends MY_Model {

	protected $_table = 'userlogindetails';//model table_name
	//set column field database for datatable orderable
	protected $_column_order = array('profileImage','firstName','lastName','loginEmail','designation','mobile','email','roleTypeName','qualification','experience','communicationAddress','alternateMobile','dob','doj','gender','zoneName','status',null); 
	//set column field database for datatable searchable
	protected $_column_search = array('u.firstName','u.lastName','u.loginEmail','u.designation','up.mobile','up.email','dart.description','up.qualification','up.experience','up.communicationAddress','up.alternateMobile','up.dob','u.doj','up.gender','z.name'); 
	// default order
	protected $_order = array('u.id' => 'desc'); 
	
	/**
	 *  Default Constructor
	 */
	function __construct($args=NULL)
	{
		parent::__construct();
		if( is_object($args))   $args = get_object_vars($args);
		if( is_array($args)){
			foreach( $args AS $key => $value ){
				$this->{$key} = $value;
			}
		}

	}
	
	public function getUserListQuery()
	{
		$query = "SELECT u.id As 'userId',u.userIdentity As 'userIdentity',u.firstName As 'firstName',u.lastName As 'lastName',u.loginEmail As 'loginEmail',
				u.designation As 'designation',u.doj As 'doj',u.status As 'status',u.profileImage As 'profileImage',
				up.mobile As 'mobile',up.email As 'email',up.communicationAddress As 'communicationAddress',up.alternateMobile As 'alternateMobile',
				up.qualification As 'qualification',up.experience As 'experience',up.gender As 'gender',up.dob As 'dob',
				dart.description As 'roleTypeName',u.roleType As 'roleType',z.name As 'zoneName',e.name As 'entityName'
				from  userlogindetails As u
				left join userpersonaldetails As up on up.userId=u.id
				left join zonedetails as z on z.id =u.zoneId
				left join entitydetails as e on e.id =u.entityId
				left join dataattributes as dart on u.roleType =dart.id
				where u.isDeleted=".Status_Type_Enum::INACTIVE;
		return $query;
	}
	
	/* private function _get_datatables_query()
	{
		 
		$this->db->from($this->_table);
	
		$i = 0;
		 
		foreach ($this->_column_search as $item) // loop column
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				 
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}
	
				if(count($this->_column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		 
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}
		else if(isset($this->_order))
		{
			$order = $this->_order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}
	
	function get_datatables()
	{
		$this->_get_datatables_query();
		
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		//debug_exit($query);
		return $query->result();
	} 
	function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    */
	
}