<?php
require_once (APPPATH . 'config/device_status_enum.php');
require_once (APPPATH . 'config/device_type_enum.php');
require_once (APPPATH . 'config/dispatch_type_enum.php');
require_once (APPPATH . 'config/driver_accepted_status_enum.php');
require_once (APPPATH . 'config/driver_shift_status_enum.php');
require_once (APPPATH . 'config/emergency_status_enum.php');
require_once (APPPATH . 'config/entity_type_enum.php');
require_once (APPPATH . 'config/experience_enum.php');
require_once (APPPATH . 'config/gender_type_enum.php');
require_once (APPPATH . 'config/license_type_enum.php');
require_once (APPPATH . 'config/module_name_enum.php');
require_once (APPPATH . 'config/payment_method_enum.php');
require_once (APPPATH . 'config/payment_mode_enum.php');
require_once (APPPATH . 'config/payment_type_enum.php');
require_once (APPPATH . 'config/register_type_enum.php');
require_once (APPPATH . 'config/role_type_enum.php');
require_once (APPPATH . 'config/signup_type_enum.php');
require_once (APPPATH . 'config/slab_type_enum.php');
require_once (APPPATH . 'config/status_type_enum.php');
require_once (APPPATH . 'config/taxi_available_status_enum.php');
require_once (APPPATH . 'config/taxi_request_status_enum.php');
require_once (APPPATH . 'config/taxi_type_enum.php');
require_once (APPPATH . 'config/transaction_mode_enum.php');
require_once (APPPATH . 'config/transaction_type_enum.php');
require_once (APPPATH . 'config/transaction_from_enum.php');
require_once (APPPATH . 'config/transmission_type_enum.php');
require_once (APPPATH . 'config/trip_status_enum.php');
require_once (APPPATH . 'config/trip_type_enum.php');
require_once (APPPATH . 'config/user_type_enum.php');

class Driver_Available_Report_Query_Model extends MY_Model {
	
	// set column field database for datatable orderable
	protected $_column_order = array (
			'driverCode',
			'driverName',
			'driverMobile',
			'driverWallet',
			'appVersion',
			'logStatusName',
			'lastLoggedIn',
			'availabilityStatusName',
			'jobCardId',
			'taxiRegistrationNo',
			'taxiDeviceCode',
			'taxiDeviceMobile'
	);
	
	// set column field database for datatable searchable
	protected $_column_search = array (
			'd.driverCode',
			'd.firstName',
			'd.lastName',
			'd.appVersion',
			'd.mobile',
			'd.driverWallet',
			'd.lastLoggedIn',
			'tpd.jobCardId',
			'daas.description',
			'td.registrationNo',
			'tdd.deviceCode',
			'tdd.mobile'
	);
	// default order
	protected $_order = array (
			'd.id' => 'desc' 
	);
	/**
	 * Default Constructor
	 */
	function __construct($args = NULL) {
		parent::__construct ();
		if (is_object ( $args ))
			$args = get_object_vars ( $args );
		if (is_array ( $args )) {
			foreach ( $args as $key => $value ) {
				$this->{$key} = $value;
			}
		}
	}
	public function getDriverAvailableQuery($time_period=200,$log_status=1,$available_status='') {
		
		$where_caluse = '';
		$time_period_where = '';
		$log_status_where = '';
		$available_status_where = '';
		if ($time_period != '') {
		//$time_period_where = " AND ddd.updatedDatetime BETWEEN ADDTIME('".getCurrentDateTime()."' ,-".$time_period.") AND '".getCurrentDateTime()."'";
		}
		if ($available_status==Taxi_Available_Status_Enum::FREE)
		{
			$available_status_where=" AND ddd.availabilityStatus=" . $available_status. " AND ddd.updatedDatetime BETWEEN ADDTIME('".getCurrentDateTime()."' ,-".$time_period.") AND '".getCurrentDateTime()."'";
		}
		else if ($available_status==Taxi_Available_Status_Enum::BUSY)
		{
			$available_status_where=" AND ddd.availabilityStatus=" . $available_status;
		}
		
		if ($log_status != '') {
			$log_status_where = 'AND d.loginStatus=' . $log_status;
		}
		$where_caluse = $time_period_where . " " . $available_status_where . " " . $log_status_where;
		
		/* $query = "SELECT d.id As 'driverId',d.driverCode As 'driverCode',CONCAT(d.firstName,' ',d.lastName) As 'driverName',d.mobile As 'driverMobile',d.driverWallet As 'driverWallet',
				d.email As 'driverEmail',d.profileImage As 'profileImage',d.status As 'status',
				(CASE WHEN (ddd.availabilityStatus=".Taxi_Available_Status_Enum::BUSY." AND ddd.tripId>=".Status_Type_Enum::ACTIVE.")  THEN 'Busy On Trip' ELSE daas.description  END) As 'availabilityStatusName',
				(CASE WHEN d.loginStatus  THEN 'In' ELSE 'Out' END) As 'logStatusName',tpd.jobCardId as 'jobCardId',
				td.id As 'taxiId',td.registrationNo As 'taxiRegistrationNo',tdd.id As 'taxiDeviceId',tdd.deviceCode As 'taxiDeviceCode',tdd.mobile As 'taxiDeviceMobile',tdd.imeiNo As 'taxiDeviceImeiNo'
				from  driver As d
				left join driverdispatchdetails As ddd on ddd.driverId=d.id
				left join drivertaximapping as dtm on dtm.driverId =d.id
				left join taxidevicedetails as tdd on dtm.taxiDeviceId =tdd.id
				left join taxidetails as td on dtm.taxiId =td.id
				left join tripdetails as tpd on ddd.tripId =tpd.id
				LEFT JOIN dataattributes daas on ddd.availabilityStatus=daas.id
				where d.isDeleted=" . Status_Type_Enum::INACTIVE . " AND d.loginStatus=" . $log_status." AND d.status=".Status_Type_Enum::ACTIVE." ".$where_caluse."  GROUP BY d.id";
		 */
		
		$query = "SELECT d.id As 'driverId',d.driverCode As 'driverCode',CONCAT(d.firstName,' ',d.lastName) As 'driverName',d.mobile As 'driverMobile',d.driverWallet As 'driverWallet',
				d.email As 'driverEmail',d.profileImage As 'profileImage',d.status As 'status',d.lastLoggedIn As 'lastLoggedIn',d.appVersion As 'appVersion',
				(CASE WHEN ((d.loginStatus=".Status_Type_Enum::ACTIVE." AND ddd.availabilityStatus=".Taxi_Available_Status_Enum::BUSY." AND ddd.tripId>=".Status_Type_Enum::ACTIVE." AND ddd.updatedDatetime NOT BETWEEN ADDTIME('".getCurrentDateTime()."' ,-".$time_period.") AND '".getCurrentDateTime()."') OR 
				(d.loginStatus=".Status_Type_Enum::ACTIVE." AND ddd.availabilityStatus=".Taxi_Available_Status_Enum::FREE." AND ddd.updatedDatetime NOT BETWEEN ADDTIME('".getCurrentDateTime()."' ,-".$time_period.") AND '".getCurrentDateTime()."'))  THEN 'Offline Status' ELSE 
				(CASE WHEN (d.loginStatus=".Status_Type_Enum::ACTIVE." AND ddd.availabilityStatus=".Taxi_Available_Status_Enum::BUSY." AND ddd.tripId>=".Status_Type_Enum::ACTIVE." AND ddd.updatedDatetime BETWEEN ADDTIME('".getCurrentDateTime()."' ,-".$time_period.") AND '".getCurrentDateTime()."') THEN 'Busy On Trip' ELSE 
				(CASE WHEN (d.loginStatus=".Status_Type_Enum::ACTIVE." AND ddd.availabilityStatus=".Taxi_Available_Status_Enum::FREE." AND ddd.updatedDatetime BETWEEN ADDTIME('".getCurrentDateTime()."' ,-".$time_period.") AND '".getCurrentDateTime()."') THEN 'Free' ELSE 'Busy' END)  END) END) As 'availabilityStatusName',
				(CASE WHEN d.loginStatus  THEN 'In' ELSE 'Out' END) As 'logStatusName',tpd.jobCardId as 'jobCardId',
				td.id As 'taxiId',td.registrationNo As 'taxiRegistrationNo',tdd.id As 'taxiDeviceId',tdd.deviceCode As 'taxiDeviceCode',tdd.mobile As 'taxiDeviceMobile',tdd.imeiNo As 'taxiDeviceImeiNo'
				from  driver As d
				left join driverdispatchdetails As ddd on ddd.driverId=d.id
				left join drivertaximapping as dtm on dtm.driverId =d.id
				left join taxidevicedetails as tdd on dtm.taxiDeviceId =tdd.id
				left join taxidetails as td on dtm.taxiId =td.id
				left join tripdetails as tpd on ddd.tripId =tpd.id
				LEFT JOIN dataattributes daas on ddd.availabilityStatus=daas.id
				where d.isDeleted=" . Status_Type_Enum::INACTIVE . " AND d.status=".Status_Type_Enum::ACTIVE." ".$where_caluse."  GROUP BY ddd.driverId";
		
		return $query;
	}
}