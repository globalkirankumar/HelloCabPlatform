<?php
class Entity_Model extends MY_Model {
	protected $_table = 'entitydetails'; // model table_name
	public $_keyName = 'id';
	public $_valueName = 'name';
	/**
	 * Default Constructor
	 */
	protected $_column_order = array (
			null,
			'entityCode',
			'name',
			'description',
			'metaKeyword',
			'tagLine',
			'email',
			'phone',
			'websiteUrl',
			'address',
			'emergencyEmail',
			'emergencyPhone',
			'entityTypeName',
			'copyRights',
			'tollFreeNo',
			'tax',
			'adminCommission',
			'passengerReferralDiscount',
			'driverReferralDiscount',
			'passengerRejectionCharge',
			'driverMinWallet',
			'driverMaxWallet',
			'driverSecurityAmount',
			'drtPromoAmount',
			'drtPromoDaysLimit',
			'drtFirstLimit',
			'drtFirstlLimitCharge',
			'drtSecondLimit',
			'adminBookingCharge',
			'sendSms',
			'sendEmail',//27
			'passengerAndroidAppUrl',//28
			'passengerIphoneAppUrl',
			'passengerAndroidAppVer', //30
			'passengerIphoneAppVer',
			'driverAndroidAppUrl',
			'driverIphoneAppUrl',
			'driverAndroidAppVer',
			'driverIphoneAppVer',
			'zoneName',
			'countryName',
			'status'
			 
	); // set column field database for datatable orderable
	protected $_column_search = array (
			'e.entityCode',
			'e.name',
			'e.description',
			'e.metaKeyword',
			'e.tagLine',
			'e.email',
			'e.phone',
			'e.websiteUrl',
			'e.address',
			'e.emergencyEmail',
			'e.emergencyPhone',
			'daet.description',
			'e.copyRights',
			'z.name',
			'ct.name',
			'e.tollFreeNo',
			'ec.tax',
			'ec.adminCommission',
			'ec.passengerReferralDiscount',
			'ec.driverReferralDiscount',
			'ec.passengerRejectionCharge',
			'ec.driverMinWallet',
			'ec.driverMaxWallet',
			'ec.driverSecurityAmount',
			'ec.drtPromoAmount',
			'ec.drtPromoDaysLimit',
			'ec.drtFirstLimit',
			'ec.drtFirstlLimitCharge',
			'ec.drtSecondLimit',
			'ec.adminBookingCharge',
			'ec.sendSms',
			'ec.sendEmail',
			'ec.passengerAndroidAppUrl',
			'ec.passengerIphoneAppUrl',
			'ec.passengerAndroidAppVer',
			'ec.passengerIphoneAppVer',
			'ec.driverAndroidAppUrl',
			'ec.driverIphoneAppUrl',
			'ec.driverAndroidAppVer',
			'ec.driverIphoneAppVer', 
	); 
	// set column field database for datatable searchable just firstname , lastname , address are searchable
	protected $_order = array (
			'e.id ' => 'desc' 
	); // default order
	function __construct($args = NULL) {
		parent::__construct ();
		if (is_object ( $args ))
			$args = get_object_vars ( $args );
		if (is_array ( $args )) {
			foreach ( $args as $key => $value ) {
				$this->{$key} = $value;
			}
		}
	}
	
	/**
	 * helper method to load the key value into dropdown boxes
	 * 
	 * @return type
	 */
	public function getEntityListQuery() {
            
            $query = "SELECT e.id As 'entityId',e.entityCode As 'entityCode',e.name  As 'name',
                        e.description As 'description',e.metaKeyword As 'metaKeyword',
                        e.tagLine As 'tagLine',e.email As 'email',e.phone As 'phone',e.websiteUrl As 'websiteUrl',
                        e.address  As 'address',e.emergencyEmail As 'emergencyEmail',daet.description As 'entityTypeName',
                        e.emergencyPhone As 'emergencyPhone',e.tollFreeNo As 'tollFreeNo',e.copyRights As 'copyRights',
                        e.status As 'status',ec.tax As 'tax',ec.adminCommission As 'adminCommission',ec.passengerReferralDiscount As 'passengerReferralDiscount',
                        ec.driverReferralDiscount As 'driverReferralDiscount',ec.passengerRejectionCharge As 'passengerRejectionCharge',
                        ec.driverMinWallet As 'driverMinWallet',ec.driverMaxWallet As 'driverMaxWallet',ec.driverSecurityAmount As 'driverSecurityAmount',
                        ec.drtPromoAmount As 'drtPromoAmount',ec.drtPromoDaysLimit As 'drtPromoDaysLimit',ec.drtFirstLimit As 'drtFirstLimit',
            			ec.drtFirstlLimitCharge As 'drtFirstlLimitCharge',ec.drtSecondLimit As 'drtSecondLimit',
                        ec.adminBookingCharge As 'adminBookingCharge',ec.sendSms As 'sendSms',ec.sendEmail As 'sendEmail',
                        ec.passengerAndroidAppUrl As 'passengerAndroidAppUrl',ec.passengerIphoneAppUrl As 'passengerIphoneAppUrl',
                        ec.passengerAndroidAppVer As 'passengerAndroidAppVer',ec.passengerIphoneAppVer As 'passengerIphoneAppVer',
                        ec.driverAndroidAppUrl As 'driverAndroidAppUrl',ec.driverIphoneAppUrl As 'driverIphoneAppUrl',
                        ec.driverAndroidAppVer As 'driverAndroidAppVer',ec.driverIphoneAppVer As 'driverIphoneAppVer',
                        z.name As 'zoneName',ct.name As 'countryName'
                        from  entitydetails As e
                        left join entityconfigdetails As ec  on ec.id=ec.entityId
                        left join zonedetails As z  on z.id=e.zoneId
                        left join country As ct  on ct.id=e.countryId
                        left join dataattributes As daet  on daet.id=e.entityType
                        where e.isDeleted='".Status_Type_Enum::INACTIVE."' GROUP BY e.id ";
            
            
		
		return $query;
	}
	public function getKeyName() {
		return $this->_keyName;
	}
	public function getValueName() {
		return $this->_valueName;
	}
}