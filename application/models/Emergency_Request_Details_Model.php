<?php
class Emergency_Request_Details_Model extends MY_Model {

	protected $_table = 'emergencyrequestdetails';//model table_name
	protected $_column_order = array(null,'userName','userMobile','userEmail','userType','location','latitude','longitude','comments','emergencyStatusName','status'); //set column field database for datatable orderable
	protected $_column_search = array('p.firstName','p.lastName','p.mobile','p.email','d.firstName','d.lastName','d.mobile','d.email','daes.description','erd.location','erd.comments','erd.latitude','erd.longitude'); //set column field database for datatable searchable just firstname , lastname , address are searchable
	protected $_order = array('erd.id' => 'desc');
	
	/**
	 *  Default Constructor
	 */
	function __construct($args=NULL)
	{
		parent::__construct();
		if( is_object($args))   $args = get_object_vars($args);
		if( is_array($args)){
			foreach( $args AS $key => $value ){
				$this->{$key} = $value;
			}
		}

	}
	
	public function getEmergencyListQuery()
	{
		$query = "SELECT erd.id As 'EmergencyId',erd.location As 'location',erd.latitude As 'latitude',daes.description As 'emergencyStatusName',erd.comments As 'comments',
				erd.status As 'status',erd.emergencyStatus As 'emergencyStatus',erd.userType As 'userType',
				erd.longitude As 'longitude',
				p.id As 'passengerId',CONCAT(p.firstName,' ',p.lastName) As 'passengerName',p.mobile As 'passengerMobile',p.email As 'passengerEmail',
				d.id As 'driverId',CONCAT(d.firstName,' ',d.lastName) As 'driverName',d.mobile As 'driverMobile',d.email As 'driverEmail'
				from  emergencyrequestdetails As erd
				left join dataattributes as daes On daes.id=erd.emergencyStatus
				left join passenger as p on (p.id = erd.userId AND erd.userType='".User_Type_Enum::PASSENGER."')
				left join driver as d on (d.id = erd.userId AND erd.userType='".User_Type_Enum::DRIVER."')
				where erd.status=".Status_Type_Enum::ACTIVE;
	
		return $query;
	}
	
	public function getDriverSmsSentDetails($driver_id,$start_date,$end_date)
	{
		$date_range_where='';
		if (strtotime($start_date)==strtotime($end_date))
		{
			$date_range_where=" AND erd.createdDatetime>='".$start_date."'";
		}
		if (strtotime($start_date)!=strtotime($end_date))
		{
			$date_range_where=" AND erd.createdDatetime BETWEEN '".$start_date."' AND '".$end_date."'";
		}
		$query = "SELECT erd.id As 'EmergencyId',erd.userId As 'driverId',erd.createdDatetime As 'createdDatetime'
				from  emergencyrequestdetails As erd
				where erd.userId=".$driver_id." AND erd.userType='".User_Type_Enum::DRIVER."' AND erd.emergencyStatus=".Emergency_Status_Enum::CLOSED." 
						AND erd.comments='Driver on free no heartbeat' ".$date_range_where;
		
		$result = $this->db->query ( $query );
		return $this->fetchAll ( $result );
	}
}