<?php
require_once (APPPATH . 'config/device_status_enum.php');
require_once (APPPATH . 'config/device_type_enum.php');
require_once (APPPATH . 'config/dispatch_type_enum.php');
require_once (APPPATH . 'config/driver_accepted_status_enum.php');
require_once (APPPATH . 'config/driver_shift_status_enum.php');
require_once (APPPATH . 'config/emergency_status_enum.php');
require_once (APPPATH . 'config/entity_type_enum.php');
require_once (APPPATH . 'config/experience_enum.php');
require_once (APPPATH . 'config/gender_type_enum.php');
require_once (APPPATH . 'config/license_type_enum.php');
require_once (APPPATH . 'config/module_name_enum.php');
require_once (APPPATH . 'config/payment_method_enum.php');
require_once (APPPATH . 'config/payment_mode_enum.php');
require_once (APPPATH . 'config/payment_type_enum.php');
require_once (APPPATH . 'config/register_type_enum.php');
require_once (APPPATH . 'config/role_type_enum.php');
require_once (APPPATH . 'config/signup_type_enum.php');
require_once (APPPATH . 'config/slab_type_enum.php');
require_once (APPPATH . 'config/status_type_enum.php');
require_once (APPPATH . 'config/taxi_available_status_enum.php');
require_once (APPPATH . 'config/taxi_request_status_enum.php');
require_once (APPPATH . 'config/taxi_type_enum.php');
require_once (APPPATH . 'config/transaction_mode_enum.php');
require_once (APPPATH . 'config/transaction_type_enum.php');
require_once (APPPATH . 'config/transmission_type_enum.php');
require_once (APPPATH . 'config/trip_status_enum.php');
require_once (APPPATH . 'config/trip_type_enum.php');
require_once (APPPATH . 'config/user_type_enum.php');

class User_Access_Model extends My_Model {
	protected $_table = 'moduleaccesspermission'; // model table_name
    private $role = null;
    private $userid = null;

    
    /**
     * Default Constructor
     */
    function __construct($args = NULL) {
    	parent::__construct ();
    	if (is_object ( $args ))
    		$args = get_object_vars ( $args );
    	if (is_array ( $args )) {
    		foreach ( $args as $key => $value ) {
    			$this->{$key} = $value;
    		}
    	}
    	$this->role   = $this->session->userdata('role_type');
    	$this->userid = $this->session->userdata('user_id');
    }
   

    private function getRole() {
        return $this->role;
    }

    private function getUserid() {
        return $this->userid;
    }

    public function showModule($role_type,$module_id)
    {
    	$isAccess = FALSE;
    	$access_permission=$this->getRoleAcceesPermission($role_type,$module_id);
    	
    	if ($access_permission[0]->isAccess)
    	{
    		$isAccess = TRUE;
    	}
    	
    	return $isAccess;
    }
    public function createModule($role_type,$module_id)
    {
    	$isCreate = FALSE;
    	$access_permission=$this->getRoleAcceesPermission($role_type,$module_id);
    	if ($access_permission[0]->isCreate)
    	{
    		$isCreate = TRUE;
    	}
    	 
    	return $isCreate;
    }
    public function readModule($role_type,$module_id)
    {
    	$isRead = FALSE;
    	$access_permission=$this->getRoleAcceesPermission($role_type,$module_id);
    	if ($access_permission[0]->isRead)
    	{
    		$isRead = TRUE;
    	}
    	 
    	return $isRead;
    }
    public function writeModule($role_type,$module_id)
    {
    	$isWrite = FALSE;
    	$access_permission=$this->getRoleAcceesPermission($role_type,$module_id);
    	if ($access_permission[0]->isWrite)
    	{
    		$isWrite = TRUE;
    	}
    	 
    	return $isWrite;
    }
    public function deleteModule($role_type,$module_id)
    {
    	$isDelete = FALSE;
    	$access_permission=$this->getRoleAcceesPermission($role_type,$module_id);
    	if ($access_permission[0]->isDelete)
    	{
    		$isDelete = TRUE;
    	}
    
    	return $isDelete;
    }
    public function fullAccessModule($role_type,$module_id)
    {
    	$isFullAccess = FALSE;
    	$access_permission=$this->getRoleAcceesPermission($role_type,$module_id);
    	if ($access_permission[0]->isDelete)
    	{
    		$isFullAccess = TRUE;
    	}
    
    	return $isFullAccess;
    }
    public function showReports() {
        $showReports = FALSE;
        switch ($this->getRole()) {
            case Role_Type_Enum::ACCOUNTANT     :
            case Role_Type_Enum::SUPER_ADMIN    :
            case Role_Type_Enum::MANAGER        :
            case Role_Type_Enum::ADMIN          :
                $showReports = TRUE;
                break;
            default:
                $showReports = TRUE;
                break;
        }

        return $showReports;
    }

    public function showDriver() {
        $showDriver = FALSE;
        switch ($this->getRole()) {
            case Role_Type_Enum::SUPER_ADMIN :
            case Role_Type_Enum::MANAGER     :
            case Role_Type_Enum::ADMIN       :
            case Role_Type_Enum::SUPERVISOR  :
            case Role_Type_Enum::STAFF       :
                $showDriver = TRUE;
                break;
            default:
                $showDriver = TRUE;
                break;
        }

        return $showDriver;
    }
    
    public function deleteDriver() {
    	$deleteDriver = FALSE;
    	switch ($this->getRole()) {
    		case Role_Type_Enum::SUPER_ADMIN :
    		case Role_Type_Enum::MANAGER     :
    		case Role_Type_Enum::ADMIN       :
    		case Role_Type_Enum::SUPERVISOR       :
    			$deleteDriver = TRUE;
    			break;
    		default:
    			$deleteDriver = TRUE;
    			break;
    	}
    
    	return $deleteDriver;
    }
    
    public function verificationDriver() {
    	$verificationDriver = FALSE;
    	switch ($this->getRole()) {
    		case Role_Type_Enum::SUPER_ADMIN :
    		case Role_Type_Enum::MANAGER     :
    		case Role_Type_Enum::ADMIN       :
    			$verificationDriver = TRUE;
    			break;
    		default:
    			$verificationDriver = TRUE;
    			break;
    	}
    
    	return $verificationDriver;
    }
    
    public function showPassenger() {
        $showPassenger = FALSE;
        switch ($this->getRole()) {
            case Role_Type_Enum::SUPER_ADMIN :
            case Role_Type_Enum::MANAGER     :
            case Role_Type_Enum::ADMIN       :
            case Role_Type_Enum::SUPERVISOR  :
            case Role_Type_Enum::STAFF:
                $showPassenger = TRUE;
                break;
            default:
                $showPassenger = TRUE;
                break;
        }

        return $showPassenger;
    }
	public function showUser() {
        $showUser = FALSE;
        switch ($this->getRole()) {
            case Role_Type_Enum::SUPER_ADMIN :
            case Role_Type_Enum::MANAGER     :
            case Role_Type_Enum::ADMIN       :
            case Role_Type_Enum::SUPERVISOR  :
            case Role_Type_Enum::STAFF:
                $showUser = TRUE;
                break;
            default:
                $showUser = TRUE;
                break;
        }

        return $showUser;
    }
    
	public function showTaxi() {
        $showTaxi = FALSE;
        switch ($this->getRole()) {
            case Role_Type_Enum::SUPER_ADMIN :
            case Role_Type_Enum::MANAGER     :
            case Role_Type_Enum::ADMIN       :
            case Role_Type_Enum::SUPERVISOR  :
            case Role_Type_Enum::STAFF:
                $showTaxi  = TRUE;
                break;
            default:
                $showTaxi  = TRUE;
                break;
        }

        return $showTaxi;
    }
	
	public function showTaxiOwner() {
        $showTaxiOwner = FALSE;
        switch ($this->getRole()) {
            case Role_Type_Enum::SUPER_ADMIN :
            case Role_Type_Enum::MANAGER     :
            case Role_Type_Enum::ADMIN       :
            case Role_Type_Enum::SUPERVISOR  :
            case Role_Type_Enum::STAFF:
                $showTaxiOwner  = TRUE;
                break;
            default:
                $showTaxiOwner  = TRUE;
                break;
        }

        return $showTaxiOwner;
    }
    
    public function showTaxiDevice() {
    	$showTaxiDevice = FALSE;
    	switch ($this->getRole()) {
    		case Role_Type_Enum::SUPER_ADMIN :
    		case Role_Type_Enum::MANAGER     :
    		case Role_Type_Enum::ADMIN       :
    		case Role_Type_Enum::SUPERVISOR  :
    		case Role_Type_Enum::STAFF:
    			$showTaxiDevice  = TRUE;
    			break;
    		default:
    			$showTaxiDevice  = TRUE;
    			break;
    	}
    
    	return $showTaxiDevice;
    }
    
	
    public function showPromoCode() {
        $showPromoCode = FALSE;
        switch ($this->getRole()) {
            case Role_Type_Enum::SUPER_ADMIN :
            case Role_Type_Enum::MANAGER     :
            case Role_Type_Enum::ADMIN       :
            case Role_Type_Enum::SUPERVISOR  :
            case Role_Type_Enum::STAFF  :
                $showPromoCode = TRUE;
                break;
            default:
                $showPromoCode = TRUE;
                break;
        }

        return $showPromoCode;
    }
    
     public function showRateCard() {
        $showRateCard = FALSE;
        switch ($this->getRole()) {
            case Role_Type_Enum::SUPER_ADMIN :
            case Role_Type_Enum::MANAGER     :
            case Role_Type_Enum::ADMIN       :
            case Role_Type_Enum::STAFF       :  // only view
                $showRateCard = TRUE;
                break;
            default:
                $showRateCard = TRUE;
                break;
        }

        return $showRateCard;
    }
    
    
    public function showTrips() {
        $showTrips = FALSE;
        switch ($this->getRole()) {
            case Role_Type_Enum::SUPER_ADMIN :
            case Role_Type_Enum::MANAGER     :
            case Role_Type_Enum::ADMIN       :
            case Role_Type_Enum::STAFF       :  
            case Role_Type_Enum::SUPERVISOR  :    
            case Role_Type_Enum::PARTNER    :    
                $showTrips = TRUE;
                break;
            default:
                $showTrips = TRUE;
                break;
        }

        return $showTrips;
    }
    
    public function showSettings() {
        $showSettings = FALSE;
        switch ($this->getRole()) {
            case Role_Type_Enum::SUPER_ADMIN :
            case Role_Type_Enum::MANAGER     :
            case Role_Type_Enum::ADMIN       :
                $showSettings = TRUE;
                break;
            default:
                $showSettings = TRUE;
                break;
        }

        return $showSettings;
    }
    public function getAccessLevelForTrip() {

        $accessLevel = User_Access_Level_Enum::NO_ACCESS;

        //$userRole = $this->getRoleInRelease($releaseModel->get('id'));

        switch ($this->getRole()) {
            case Role_Type_Enum::SUPER_ADMIN:
                $accessLevel = User_Access_Level_Enum::EDIT;
                break;
            case Role_Type_Enum::STAFF:
                $accessLevel = User_Access_Level_Enum::EDIT;
                  break;
            case Role_Type_Enum::ADMIN:
                $accessLevel = User_Access_Level_Enum::EDIT;
                break;
            case Role_Type_Enum::MANAGER:
                $accessLevel = User_Access_Level_Enum::EDIT;
                break;
            default:
                $accessLevel = User_Access_Level_Enum::NO_ACCESS;
                break;
        }

        return $accessLevel;
    }
    
    
    public function getAccessLevelForRateCard() {

        $accessLevel = User_Access_Level_Enum::NO_ACCESS;

        //$userRole = $this->getRoleInRelease($releaseModel->get('id'));

        switch ($this->getRole()) {
            case Role_Type_Enum::SUPER_ADMIN:
                $accessLevel = User_Access_Level_Enum::EDIT;
                break;
            case Role_Type_Enum::ADMIN:
                $accessLevel = User_Access_Level_Enum::EDIT;
                break;
            case Role_Type_Enum::MANAGER:
                $accessLevel = User_Access_Level_Enum::EDIT;
                break;
            case Role_Type_Enum::STAFF:
                $accessLevel = User_Access_Level_Enum::READ_ONLY;
                break;
            default:
                $accessLevel = User_Access_Level_Enum::NO_ACCESS;
                break;
        }

        return $accessLevel;
    }
    
    public function getAccessLevelForPromoCode() {

        $accessLevel = User_Access_Level_Enum::NO_ACCESS;

        //$userRole = $this->getRoleInRelease($releaseModel->get('id'));

        switch ($this->getRole()) {
            case Role_Type_Enum::SUPER_ADMIN:
                $accessLevel = User_Access_Level_Enum::EDIT;
                break;
            case Role_Type_Enum::ADMIN:
                $accessLevel = User_Access_Level_Enum::EDIT;
                break;
            case Role_Type_Enum::MANAGER:
                $accessLevel = User_Access_Level_Enum::EDIT;
                break;
            case Role_Type_Enum::SUPERVISOR:
                $accessLevel = User_Access_Level_Enum::EDIT;
                break;
            case Role_Type_Enum::STAFF:
                $accessLevel = User_Access_Level_Enum::READ_ONLY;
                break;
            default:
                $accessLevel = User_Access_Level_Enum::NO_ACCESS;
                break;
        }

        return $accessLevel;
    }
    
    public function getAccessLevelForTripEdit() {

        $accessLevel = User_Access_Level_Enum::NO_ACCESS;


        switch ($this->getRole()) {
            case Role_Type_Enum::SUPER_ADMIN:
                $accessLevel = User_Access_Level_Enum::EDIT;
                break;
            case Role_Type_Enum::ADMIN:
                $accessLevel = User_Access_Level_Enum::EDIT;
                break;
            case Role_Type_Enum::MANAGER:
                $accessLevel = User_Access_Level_Enum::EDIT;
                break;
            case Role_Type_Enum::SUPERVISOR:
                $accessLevel = User_Access_Level_Enum::EDIT;
                break;
            case Role_Type_Enum::STAFF:
                $accessLevel = User_Access_Level_Enum::EDIT;
                break;
            case Role_Type_Enum::PARTNER:
                $accessLevel = User_Access_Level_Enum::READ_ONLY;
                break;
            default:
                $accessLevel = User_Access_Level_Enum::NO_ACCESS;
                break;
        }

        return $accessLevel;
    }
    public function getAccessLevelForAdvtApproval() {
    
    	$accessLevel = User_Access_Level_Enum::NO_ACCESS;
    
    
    	switch ($this->getRole()) {
    		case Role_Type_Enum::SUPER_ADMIN:
    			$accessLevel = User_Access_Level_Enum::EDIT;
    			break;
    		case Role_Type_Enum::ADMIN:
    			$accessLevel = User_Access_Level_Enum::EDIT;
    			break;
    		case Role_Type_Enum::MANAGER:
    			$accessLevel = User_Access_Level_Enum::EDIT;
    			break;
    		case Role_Type_Enum::SUPERVISOR:
    			$accessLevel = User_Access_Level_Enum::EDIT;
    			break;
    		default:
    			$accessLevel = User_Access_Level_Enum::NO_ACCESS;
    			break;
    	}
    
    	return $accessLevel;
    }
    
     public function showZone() {
        $showZone = FALSE;
        switch ($this->getRole()) {
            case Role_Type_Enum::SUPER_ADMIN :
            case Role_Type_Enum::MANAGER     :
            case Role_Type_Enum::ADMIN       :
            case Role_Type_Enum::SUPERVISOR  :
            case Role_Type_Enum::STAFF       :
                $showZone = TRUE;
                break;
            default:
                $showZone = TRUE;
                break;
        }

        return $showZone;
    }
    
    private function getRoleAcceesPermission($role_type,$module_id)
    {
    	$query = "SELECT map.isAccess As 'isAccess',map.isRead As 'isRead',map.isWrite As 'isWrite',
    			map.isCreate As 'isCreate',map.isDelete As 'isDelete',map.isFullAccess As 'isFullAccess',
    			map.roleType As 'roleType',map.moduleId As 'moduleId',map.id As 'moduleAccessId'
				from moduleaccesspermission As map
				where map.roleType=".$role_type." AND map.moduleId=".$module_id;
		
    	$result = $this->db->query ( $query );
    	
    	return $this->fetchAll ( $result );
    	
    } 

}//end of class