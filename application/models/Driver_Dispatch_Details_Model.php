<?php
class Driver_Dispatch_Details_Model extends MY_Model {

	protected $_table = 'driverdispatchdetails';//model table_name
	
	// set column field database for datatable orderable
	protected $_column_order = array (
			'driverName',
			'driverMobile',
			'driverWallet',
			'WalletPercentage'
	);
	// set column field database for datatable searchable
	protected $_column_search = array (
			'd.firstName',
			'd.lastName',
			'd.mobile',
			'd.driverWallet'
	);
	
	/**
	 *  Default Constructor
	 */
	function __construct($args=NULL)
	{
		parent::__construct();
		if( is_object($args))   $args = get_object_vars($args);
		if( is_array($args)){
			foreach( $args AS $key => $value ){
				$this->{$key} = $value;
			}
		}

	}
        
        
        public function getLatLongQueryByStatus($availability_status,$is_ontrip=0){
        	$where_clause=" ";
        	if ($availability_status==Taxi_Available_Status_Enum::FREE)
        	{
        		$where_clause=" AND ddd.updatedDatetime BETWEEN ADDTIME('".getCurrentDateTime()."' ,-200) AND '".getCurrentDateTime()."'";
        	}
        	else if ($availability_status==Taxi_Available_Status_Enum::BUSY && $is_ontrip==0)
        	{
        		$where_clause=" AND ddd.tripId=".$is_ontrip;
        	}
        	else if ($availability_status==Taxi_Available_Status_Enum::BUSY && $is_ontrip==1)
        	{
        		$where_clause=" AND ddd.tripId>=".$is_ontrip." AND ddd.updatedDatetime BETWEEN ADDTIME('".getCurrentDateTime()."' ,-200) AND '".getCurrentDateTime()."'";
        	}
            $availablility_count = 0;
            $sql = "SELECT ddd.driverId,ddd.availabilityStatus,ddd.driverLatitude,ddd.driverLongitude 
            		FROM driverdispatchdetails as ddd
                    LEFT JOIN driver d on ddd.driverId = d.id
                    WHERE ddd.availabilityStatus = $availability_status 
                    AND d.loginStatus = ".Status_Type_Enum::ACTIVE." AND d.status = ".Status_Type_Enum::ACTIVE." AND d.isDeleted = ".Status_Type_Enum::INACTIVE." ".$where_clause." GROUP BY ddd.driverId";
            $result = $this->db->query($sql)->result();
            return $result;    
            
        }
        
        public function getLoggedInDriverDetails(){
        	$sql = "SELECT ddd.driverId As 'driverId',ddd.driverLatitude As 'driverLatitude',ddd.driverLongitude As 'driverLongitude'
        	FROM driverdispatchdetails as ddd
        	LEFT JOIN driver d on ddd.driverId = d.id
        	WHERE d.loginStatus = ".Status_Type_Enum::ACTIVE." AND d.isDeleted = ".Status_Type_Enum::INACTIVE;
        	$result = $this->db->query($sql)->result();
        	return $result;
        
        }
        
        public function getAvailablityCount($availability_status,$is_ontrip=0){
        	$where_clause=" ";
        	if ($availability_status==Taxi_Available_Status_Enum::FREE)
        	{
        		$where_clause=" AND ddd.updatedDatetime BETWEEN ADDTIME('".getCurrentDateTime()."' ,-200) AND '".getCurrentDateTime()."'";
        	}
        	else if ($availability_status==Taxi_Available_Status_Enum::BUSY && $is_ontrip==Status_Type_Enum::INACTIVE)
        	{
        		$where_clause=" AND ddd.tripId=".$is_ontrip;
        	}
        	else if ($availability_status==Taxi_Available_Status_Enum::BUSY && $is_ontrip==Status_Type_Enum::ACTIVE)
        	{
        		//$where_clause=" AND ddd.tripId>=".$is_ontrip;
        		$where_clause=" AND ddd.tripId>=".$is_ontrip." AND ddd.updatedDatetime BETWEEN ADDTIME('".getCurrentDateTime()."' ,-200) AND '".getCurrentDateTime()."'";
        	}
            $availablility_count = 0;
            $sql = "SELECT ddd.driverId,ddd.availabilityStatus,d.id 
            		FROM driverdispatchdetails as ddd
                    LEFT JOIN driver d on ddd.driverId = d.id
                    WHERE ddd.availabilityStatus = $availability_status 
                    AND d.loginStatus = ".Status_Type_Enum::ACTIVE." AND d.status = ".Status_Type_Enum::ACTIVE." AND d.isDeleted = ".Status_Type_Enum::INACTIVE." ".$where_clause;
            $result = $this->db->query($sql)->result();
                if($result){
                    $availablility_count = count($result);
                }else{
                    $availablility_count = 0;
                }
            return $availablility_count;
            
        }
        
       public function getLogStatusCabs($log_status){
           $logged_count = 0;
            $sql = "SELECT ddd.driverId,ddd.availabilityStatus,d.id 
            		FROM driverdispatchdetails as ddd
                    LEFT JOIN driver d on ddd.driverId = d.id
                    WHERE d.loginStatus =  ".$log_status."
                    AND d.status = ".Status_Type_Enum::ACTIVE." AND d.isDeleted = ".Status_Type_Enum::INACTIVE;
            $result = $this->db->query($sql)->result();
                if($result){
                    $logged_count = count($result);
                }else{
                    $logged_count = 0;
                }
            return $logged_count;
       } 
       
       public function getLogStatusCabsByZone($log_status){
           $logged_count = 0;
            $sql = "SELECT ddd.driverId,ddd.availabilityStatus,ddd.driverLatitude as driverLatitude,ddd.driverLongitude as driverLongitude
                    FROM driverdispatchdetails as ddd
                    LEFT JOIN driver d on ddd.driverId = d.id
                    WHERE d.loginStatus =  ".$log_status."
                    AND d.status = ".Status_Type_Enum::ACTIVE." AND d.isDeleted = ".Status_Type_Enum::INACTIVE;
            $result = $this->db->query($sql)->result();
            return $result;
       }
       
       public function getConsecutiveRejectCount($count){
           $status_count = 0;
           $sql = "SELECT ddd.driverId,ddd.availabilityStatus,ddd.driverLatitude as driverLatitude,ddd.driverLongitude as driverLongitude
                    FROM driverdispatchdetails ddd
                    LEFT JOIN driver d on ddd.driverId = d.id
                    WHERE ddd.consecutiveRejectCount = '$count'
                    AND d.status = ".Status_Type_Enum::ACTIVE." AND d.isDeleted = ".Status_Type_Enum::INACTIVE;
            $result = $this->db->query($sql)->result();
                if($result){
                    $status_count = count($result);
                }else{
                    $status_count = 0;
                }
            return $status_count;
       } 
       
       public function getDriverMinWalletDetailsForEntity($entity_id){
           $sql     = "SELECT driverMinWallet FROM entityconfigdetails WHERE entityId=$entity_id";
           $result  = $this->db->query($sql)->row();
            if($result){
                $driver_min_wallet = $result->driverMinWallet;
            }else{
               $driver_min_wallet  = 0;  
            } 
            return $driver_min_wallet;
            
       }
       
       public function getDriverWalletDetailsForEntity($entity_id){
           $sql     = "SELECT CONCAT(`firstName` ,' ', `lastName`) as drivername,`mobile`,`email`,`driverWallet` FROM `driver` WHERE `entityId` ='$entity_id' ";
           $result  = $this->db->query($sql)->result();
           return $result;
       }
       
	public function getFreeAndBusyDriver(){
          $sql = "SELECT ddd.driverId As 'driverId', ddd.driverLatitude As 'driverLatitude',ddd.driverLongitude As 'driverLongitude',
                    ddd.availabilityStatus As 'availabilityStatus',CONCAT(d.firstName ,' ',d.lastName) As 'driverName',d.mobile As 'driverMobile',
          			td.registrationNo As 'taxiRegistrationNo',tdd.mobile As 'taxiDeviceMobile'
                    FROM  driverdispatchdetails ddd
                    LEFT JOIN driver d ON d.id=ddd.driverID
          			left join drivertaximapping as dtm on dtm.driverId =d.id
					left join taxidevicedetails as tdd on dtm.taxiDeviceId =tdd.id
					left join taxidetails as td on dtm.taxiId =td.id
                    WHERE (((ddd.availabilityStatus = ".Taxi_Available_Status_Enum::FREE.") AND ddd.updatedDatetime BETWEEN ADDTIME('".getCurrentDateTime()."' ,-500) AND '".getCurrentDateTime()."') OR 
                    (ddd.availabilityStatus=".Taxi_Available_Status_Enum::BUSY." AND ddd.tripId=".Status_Type_Enum::INACTIVE.") OR (ddd.availabilityStatus = ".Taxi_Available_Status_Enum::BUSY." AND ddd.tripId>=".Status_Type_Enum::ACTIVE.")) 
                    AND d.status = ".Status_Type_Enum::ACTIVE." 
                    AND d.isDeleted = ".Status_Type_Enum::INACTIVE." AND d.loginStatus=".Status_Type_Enum::ACTIVE." GROUP BY ddd.driverId";
          /* 
		  $sql = "SELECT ddd.driverId as 'driverId', ddd.driverLatitude as 'driverLatitude',
		 ddd.driverLongitude as 'driverLongitude',
		 ddd.availabilityStatus as 'availabilityStatus',CONCAT(d.firstName ,' ',d.lastName) as 'drivername',d.mobile as 'mobile'
		 FROM  driverdispatchdetails ddd
		 LEFT JOIN driver d ON d.id=ddd.driverID
		 WHERE (ddd.availabilityStatus = ".Taxi_Available_Status_Enum::FREE." OR ddd.availabilityStatus = ".Taxi_Available_Status_Enum::BUSY.") AND d.status = ".Status_Type_Enum::ACTIVE."
		 AND d.isDeleted = ".Status_Type_Enum::INACTIVE." AND d.loginStatus=".Status_Type_Enum::ACTIVE." GROUP BY ddd.driverId";
		 */
           $result  = $this->db->query($sql)->result();
           return $result;
       }
       public function getDriverWalletDetailsQuery($entity_id = NULL)
       {
       	$query="SELECT d.id as 'driverId',CONCAT(d.firstName,' ',d.lastName) As 'driverName',d.mobile As 'driverMobile',d.driverwallet As 'driverWallet',(d.driverWallet/ecd.driverMinWallet)*100 As 'WalletPercentage',
				(CASE WHEN ((d.driverWallet/ecd.driverMinWallet)*100 >=150 AND (d.driverWallet/ecd.driverMinWallet)*100 <=180) THEN 'TOP'
 				ELSE (CASE WHEN ((d.driverWallet/ecd.driverMinWallet)*100 >=120 AND (d.driverWallet/ecd.driverMinWallet)*100 <=150) THEN 'MID' ELSE 'LOW' END)END) AS 'walletClass'
				FROM driver as d
				left join entityconfigdetails as ecd on d.entityId=ecd.entityId
				WHERE ecd.entityId=".$entity_id." AND d.isDeleted=".Status_Type_Enum::INACTIVE." AND d.status=".Status_Type_Enum::ACTIVE."
				GROUP BY d.id
				HAVING WalletPercentage<=180
				ORDER BY WalletPercentage";
       	
       	return $query;
       }
}// end of model