<?php
class Rate_Card_Details_Model extends MY_Model {

	protected $_table = 'ratecarddetails';//model table_name
	
	/**
	 *  Default Constructor
	 */
	 
	public $_keyName = 'id';
	public $_valueName = 'name';
	/**
	 *  Default Constructor
	 */
	 
	protected $_column_order = array(null,'name','taxiCategoryTypeName','dayConvenienceCharge','nightConvenienceCharge','zoneName','entityName','status'); //set column field database for datatable orderable
	protected $_column_search = array('rcd.name','rcd.dayConvenienceCharge','rcd.nightConvenienceCharge','datct.description','z.name','e.name'); //set column field database for datatable searchable just firstname , lastname , address are searchable
	protected $_order = array('rcd.id ' => 'desc'); // default order
	function __construct($args=NULL)
	{
		parent::__construct();
		if( is_object($args))   $args = get_object_vars($args);
		if( is_array($args)){
			foreach( $args AS $key => $value ){
				$this->{$key} = $value;
			}
		}

	}
	
	public function getTariffListQuery()
	{
		$query = "SELECT rcd.id As 'tariffId',rcd.name As 'name',rcd.dayConvenienceCharge As 'dayConvenienceCharge',rcd.nightConvenienceCharge As 'nightConvenienceCharge',
				datct.description As 'taxiCategoryTypeName',rcd.status As 'status',rcd.zoneId As 'zoneId',z.name As 'zoneName',e.name As 'entityName',rcd.entityId As 'entityId'
				from  ratecarddetails As rcd
				left join zonedetails As z  on z.id=rcd.zoneId
				left join entitydetails As e  on e.id=rcd.entityId
				left join dataattributes as datct on rcd.taxiCategoryType =datct.id
				Where rcd.isDeleted=".Status_Type_Enum::INACTIVE;
		return $query;
	}
	public function getKeyName(){
		return $this->_keyName;
	}
	
	public function getValueName(){
		return $this->_valueName;
	}
	
}