<?php
class Country_Model extends MY_Model {

	protected $_table = 'country';//model table_name
	public $_keyName = 'id';
	public $_valueName = 'name';
	/**
	 *  Default Constructor
	 */
	 
	//protected $_table = 'userlogindetails';//model table_name
	protected $_column_order = array(null,'name','isoCode','telephoneCode','currencyCode','currencySymbol','timeZone','status'); //set column field database for datatable orderable
	protected $_column_search = array('c.name','c.isoCode','c.telephoneCode','c.currencyCode','c.currencySymbol','c.timeZone'); //set column field database for datatable searchable just firstname , lastname , address are searchable
	protected $_order = array('c.id' => 'desc'); // default order

	function __construct($args=NULL)
	{
		parent::__construct();
		if( is_object($args))   $args = get_object_vars($args);
		if( is_array($args)){
			foreach( $args AS $key => $value ){
				$this->{$key} = $value;
			}
		}

	}
	
	
	public function getCountryListQuery()
	{
		
		$query="SELECT c.id As 'countryId',c.name As 'name',
				c.isoCode As 'isoCode',c.telephoneCode As 'telephoneCode',c.currencyCode As 'currencyCode',
				c.currencySymbol As 'currencySymbol',c.timeZone As 'timeZone',c.status As 'status'
				from  country As c";
		return $query;		
	}
	/**
	 *  helper method to load the key value into dropdown boxes
	 * @return type
	 */
	public function getKeyName(){
		return $this->_keyName;
	}
	
	public function getValueName(){
		return $this->_valueName;
	}

}