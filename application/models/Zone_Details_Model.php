<?php
class Zone_Details_Model extends MY_Model {

	protected $_table = 'zonedetails';//model table_name
	public $_keyName = 'id';
	public $_valueName = 'name';
	/**
	 *  Default Constructor
	 */
	protected $_column_order = array(null,'name','polygonPoints','parentZoneName','countryName','dispatchTypeName','status');
	//set column field database for datatable searchable
	protected $_column_search = array('z.name','z.polygonPoints','pz.name','c.name','dadt.desription');
	protected $_order = array('z.id' => 'desc');	
	
	function __construct($args=NULL)
	{
		parent::__construct();
		if( is_object($args))   $args = get_object_vars($args);
		if( is_array($args)){
			foreach( $args AS $key => $value ){
				$this->{$key} = $value;
			}
		}

	}
	/**
	 *  helper method to load the key value into dropdown boxes
	 * @return type
	 */
	public function getKeyName(){
		return $this->_keyName;
	}
	
	public function getValueName(){
		return $this->_valueName;
	}
        
        
        public function getZoneListQuery(){
            $query = "SELECT z.id As 'zoneId',z.name As 'name',z.polygonPoints As 'polygonPoints',pz.name As 'parentZoneName',
            		c.name As 'countryName',z.status As 'status',dadt.description As 'dispatchTypeName'
            		FROM zonedetails as z
            		left join zonedetails as pz on pz.id=z.zoneId
            		left join country as c on c.id=z.countryId
            		left join dataattributes as dadt on dadt.id=z.dispatchType
            		where z.isDeleted=".Status_Type_Enum::INACTIVE;
            return $query;
            
        }
        public function getZoneListQueryForTripCreation(){
            $query = "SELECT z.id As 'zoneId',z.name As 'name',z.polygonPoints As 'polygonPoints',pz.name As 'parentZoneName',
            		c.name As 'countryName',z.status As 'status',dadt.description As 'dispatchTypeName'
            		FROM zonedetails as z
            		left join zonedetails as pz on pz.id=z.zoneId
            		left join country as c on c.id=z.countryId
            		left join dataattributes as dadt on dadt.id=z.dispatchType
            		where z.isDeleted=".Status_Type_Enum::INACTIVE." AND z.zoneId =".Status_Type_Enum::INACTIVE." ";
            $result    = $this->db->query ( $query );
            return $this->fetchAll ( $result );
            
        }
        public function getSubZoneListQueryForTripCreation(){
            $query = "SELECT z.id As 'zoneId',z.name As 'name',z.polygonPoints As 'polygonPoints',pz.name As 'parentZoneName',
            		c.name As 'countryName',z.status As 'status',dadt.description As 'dispatchTypeName'
            		FROM zonedetails as z
            		left join zonedetails as pz on pz.id=z.zoneId
            		left join country as c on c.id=z.countryId
            		left join dataattributes as dadt on dadt.id=z.dispatchType
            		where z.isDeleted=".Status_Type_Enum::INACTIVE." AND z.zoneId>".Status_Type_Enum::INACTIVE;
           $result    = $this->db->query ( $query );
            return $this->fetchAll ( $result );
        }
        
        public function getFullZoneListDetail(){
            $query  = "SELECT z.id As 'zoneId',z.name As 'name',z.polygonPoints As 'polygonPoints',pz.name As 'parentZoneName',
            		c.name As 'countryName',z.status As 'status',dadt.description As 'dispatchTypeName'
            		FROM zonedetails as z
            		left join zonedetails as pz on pz.id=z.zoneId
            		left join country as c on c.id=z.countryId
            		left join dataattributes as dadt on dadt.id=z.dispatchType
            		where z.isDeleted=".Status_Type_Enum::INACTIVE;
           $result    = $this->db->query ( $query );
            return $this->fetchAll ( $result );
        }
        
        public function getSubZoneListByZoneId($zone_id=NULL){
            $query = "SELECT z.id As 'zoneId',z.name As 'name',z.polygonPoints As 'polygonPoints',pz.name As 'parentZoneName',
            		c.name As 'countryName',z.status As 'status',dadt.description As 'dispatchTypeName'
            		FROM zonedetails as z
            		left join zonedetails as pz on pz.id=z.zoneId
            		left join country as c on c.id=z.countryId
            		left join dataattributes as dadt on dadt.id=z.dispatchType
            		where z.isDeleted=".Status_Type_Enum::INACTIVE." AND z.zoneId =".$zone_id." ";
           $result    = $this->db->query ( $query );
            return $this->fetchAll ( $result );
        }

        public function checkZoneHasParentByZoneId($zone_id=NULL){
        	$query = "SELECT z.id As 'subzoneId',z.name As 'subzone_name',z.zoneId as zoneId,pz.name As 'parentZoneName',
            		c.name As 'countryName',z.status As 'status',dadt.description As 'dispatchTypeName'
            		FROM zonedetails as z
            		left join zonedetails as pz on pz.id=z.zoneId
            		left join country as c on c.id=z.countryId
            		left join dataattributes as dadt on dadt.id=z.dispatchType
            		where z.isDeleted=".Status_Type_Enum::INACTIVE." AND z.id=".$zone_id."  AND z.zoneId >".Status_Type_Enum::INACTIVE." ";
        	$result    = $this->db->query ( $query );
        	return $this->fetchAll ( $result );
        }
}