<?php
class Driver_Shift_History_Model extends MY_Model {

	protected $_table = 'drivershifthistory';//model table_name
	// set column field database for datatable orderable
	protected $_column_order = array (
			null,
			'tripId',
			'transactionAmount',
			'previousAmount',
			'currentAmount',
			'transactionStatus',
			'transactionFrom',
			'transactionType',
			'transactionMode',
			'transactionId',
			'comments'
	);
	// set column field database for datatable searchable
	protected $_column_search = array (
			'daas.description',
			'dass.description',
	);
	// default order
	protected $_order = array (
			'dsh.id' => 'desc'
	);
	/**
	 *  Default Constructor
	 */
	function __construct($args=NULL)
	{
		parent::__construct();
		if( is_object($args))   $args = get_object_vars($args);
		if( is_array($args)){
			foreach( $args AS $key => $value ){
				$this->{$key} = $value;
			}
		}

	}
	
	public function getDriverLogHistoryListQuery($driver_id) {
		$query = "SELECT dsh.id As 'driverShiftId',dsh.driverId As 'driverId',dsh.inLatitude As 'inLatitude',dsh.inLongitude As 'inLongitude',
		dsh.outLatitude As 'outLatitude',dsh.outLongitude As 'outLongitude',daas.description As 'availabilityStatusName',dass.description As 'shiftStatusName',
		dsh.availabilityStatus As 'availabilityStatus',dsh.shiftStatus As 'shiftStatus',dsh.createdDatetime As 'createdDatetime',dsh.updatedDatetime As 'updatedDatetime'
		FROM drivershifthistory as dsh
		LEFT JOIN dataattributes daas on  dsh.availabilityStatus=daas.id
		LEFT JOIN dataattributes dass on dsh.shiftStatus=dass.id
		WHERE dsh.driverId=".$driver_id;
		return $query;
	}
}