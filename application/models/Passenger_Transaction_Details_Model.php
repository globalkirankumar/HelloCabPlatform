<?php
class Passenger_Transaction_Details_Model extends MY_Model {
	protected $_table = 'passengertransactiondetails'; // model table_name
	                                                   
	// set column field database for datatable orderable
	protected $_column_order = array (
			null,
			'tripId',
			'transactionAmount',
			'previousAmount',
			'currentAmount',
			'transactionStatus',
			'transactionFrom',
			'transactionType',
			'transactionMode',
			'transactionId',
			'comments' 
	);
	// set column field database for datatable searchable
	protected $_column_search = array (
			'ptd.tripId',
			'ptd.transactionAmount',
			'ptd.previousAmount',
			'ptd.currentAmount',
			'ptd.transactionStatus',
			'datf.description',
			'datt.description',
			'datm.description',
			'ptd.transactionId',
			'ptd.transactionStatus',
			'ptd.tripId' 
	);
	// default order
	protected $_order = array (
			'p.id' => 'desc' 
	);
	/**
	 * Default Constructor
	 */
	function __construct($args = NULL) {
		parent::__construct ();
		if (is_object ( $args ))
			$args = get_object_vars ( $args );
		if (is_array ( $args )) {
			foreach ( $args as $key => $value ) {
				$this->{$key} = $value;
			}
		}
	}
	public function getPassengerTransactionListQuery($passenger_id) {
		$query = "SELECT ptd.id As 'id',CONCAT(p.firstName ,' ', p.lastName) As 'passengerName',ptd.passengerId As 'passengerId',ptd.tripId,ptd.transactionAmount,ptd.previousAmount,ptd.currentAmount,
                        ptd.transactionStatus,datt.description As 'transactionType',datm.description AS  transactionMode,datf.description AS  transactionFrom,
                        ptd.transactionId,ptd.comments,ptd.createdDatetime As 'createdDatetime'
                        FROM passengertransactiondetails ptd
                        LEFT JOIN passenger as p on ptd.passengerId=p.id
                        LEFT JOIN dataattributes datt on  ptd.transactionType=datt.id
                        LEFT JOIN dataattributes datm on ptd.transactionMode=datm.id 
                        LEFT JOIN dataattributes datf on ptd.transactionFrom=datf.id 
                        WHERE ptd.passengerId=$passenger_id";
		return $query;
	}
}