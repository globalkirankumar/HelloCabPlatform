<?php
class User_Emergency_Contacts_Model extends MY_Model {

	protected $_table = 'useremergencycontacts';//model table_name
	
	/**
	 *  Default Constructor
	 */
	function __construct($args=NULL)
	{
		parent::__construct();
		if( is_object($args))   $args = get_object_vars($args);
		if( is_array($args)){
			foreach( $args AS $key => $value ){
				$this->{$key} = $value;
			}
		}

	}
	
}