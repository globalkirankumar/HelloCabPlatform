<?php
require_once (APPPATH . 'config/device_status_enum.php');
require_once (APPPATH . 'config/device_type_enum.php');
require_once (APPPATH . 'config/dispatch_type_enum.php');
require_once (APPPATH . 'config/driver_accepted_status_enum.php');
require_once (APPPATH . 'config/driver_shift_status_enum.php');
require_once (APPPATH . 'config/emergency_status_enum.php');
require_once (APPPATH . 'config/entity_type_enum.php');
require_once (APPPATH . 'config/experience_enum.php');
require_once (APPPATH . 'config/gender_type_enum.php');
require_once (APPPATH . 'config/license_type_enum.php');
require_once (APPPATH . 'config/module_name_enum.php');
require_once (APPPATH . 'config/payment_method_enum.php');
require_once (APPPATH . 'config/payment_mode_enum.php');
require_once (APPPATH . 'config/payment_type_enum.php');
require_once (APPPATH . 'config/register_type_enum.php');
require_once (APPPATH . 'config/role_type_enum.php');
require_once (APPPATH . 'config/signup_type_enum.php');
require_once (APPPATH . 'config/slab_type_enum.php');
require_once (APPPATH . 'config/status_type_enum.php');
require_once (APPPATH . 'config/taxi_available_status_enum.php');
require_once (APPPATH . 'config/taxi_request_status_enum.php');
require_once (APPPATH . 'config/taxi_type_enum.php');
require_once (APPPATH . 'config/transaction_mode_enum.php');
require_once (APPPATH . 'config/transaction_type_enum.php');
require_once (APPPATH . 'config/transaction_from_enum.php');
require_once (APPPATH . 'config/transmission_type_enum.php');
require_once (APPPATH . 'config/trip_status_enum.php');
require_once (APPPATH . 'config/trip_type_enum.php');
require_once (APPPATH . 'config/user_type_enum.php');

class Promocode_Report_Query_Model extends MY_Model {
	
	// set column field database for datatable orderable
	protected $_column_order = array (
			'invoiceNo',
			'jobCardId',
			'totalTripCharge',
			'promoDiscountAmount',
			'paidTripCharge',
			'passengerCode',
			'passengerName',
			'driverCode',
			'driverName'
	);
	
	// set column field database for datatable searchable
	protected $_column_search = array (
			'ttd.invoiceNo',
			'td.jobCardId',
			'ttd.totalTripCharge',
			'ttd.promoDiscountAmount',
			'd.driverCode',
			'd.firstName',
			'd.lastName',
			'd.mobile',
			'p.passengerCode',
			'p.firstName',
			'p.lastName',
			'p.mobile'
	);
	// default order
	protected $_order = array (
			'td.id' => 'Desc' 
	);
	/**
	 * Default Constructor
	 */
	function __construct($args = NULL) {
		parent::__construct ();
		if (is_object ( $args ))
			$args = get_object_vars ( $args );
		if (is_array ( $args )) {
			foreach ( $args as $key => $value ) {
				$this->{$key} = $value;
			}
		}
	}
	public function getPromocodeReportQuery($promocode) {
		
		$query = "select td.jobCardId As 'jobCardId',d.driverCode As 'driverCode',CONCAT(d.firstname,'',d.lastName) As 'driverName',d.mobile As 'driverMobile',
				CONCAT(p.firstname,'',p.lastName) As 'passengerName',p.mobile As 'passengerMobile',p.passengerCode As 'passengerCode',ttd.invoiceNo As 'invoiceNo',
				ttd.id As 'transactionId',ttd.promoDiscountAmount As 'promoDiscountAmount',ttd.totalTripCharge As 'totalTripCharge',ttd.totalTripCharge As 'paidTripCharge'
				from promocodedetails as pd 
				left join tripdetails as td on td.promoCode=pd.promoCode
				left join triptransactiondetails as ttd on ttd.tripId=td.id
				left join driver as d on d.id=td.driverId 
				left join passenger as p on p.id=td.passengerId 
				where ttd.promoDiscountAmount>".Status_Type_Enum::INACTIVE." AND pd.id=".$promocode." AND td.tripStatus=".Trip_Status_Enum::TRIP_COMPLETED;
		
		return $query;
	}
}