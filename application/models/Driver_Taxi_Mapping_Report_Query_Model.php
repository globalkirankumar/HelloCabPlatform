<?php
require_once (APPPATH . 'config/device_status_enum.php');
require_once (APPPATH . 'config/device_type_enum.php');
require_once (APPPATH . 'config/dispatch_type_enum.php');
require_once (APPPATH . 'config/driver_accepted_status_enum.php');
require_once (APPPATH . 'config/driver_shift_status_enum.php');
require_once (APPPATH . 'config/emergency_status_enum.php');
require_once (APPPATH . 'config/entity_type_enum.php');
require_once (APPPATH . 'config/experience_enum.php');
require_once (APPPATH . 'config/gender_type_enum.php');
require_once (APPPATH . 'config/license_type_enum.php');
require_once (APPPATH . 'config/module_name_enum.php');
require_once (APPPATH . 'config/payment_method_enum.php');
require_once (APPPATH . 'config/payment_mode_enum.php');
require_once (APPPATH . 'config/payment_type_enum.php');
require_once (APPPATH . 'config/register_type_enum.php');
require_once (APPPATH . 'config/role_type_enum.php');
require_once (APPPATH . 'config/signup_type_enum.php');
require_once (APPPATH . 'config/slab_type_enum.php');
require_once (APPPATH . 'config/status_type_enum.php');
require_once (APPPATH . 'config/taxi_available_status_enum.php');
require_once (APPPATH . 'config/taxi_request_status_enum.php');
require_once (APPPATH . 'config/taxi_type_enum.php');
require_once (APPPATH . 'config/transaction_mode_enum.php');
require_once (APPPATH . 'config/transaction_type_enum.php');
require_once (APPPATH . 'config/transaction_from_enum.php');
require_once (APPPATH . 'config/transmission_type_enum.php');
require_once (APPPATH . 'config/trip_status_enum.php');
require_once (APPPATH . 'config/trip_type_enum.php');
require_once (APPPATH . 'config/user_type_enum.php');

class Driver_Taxi_Mapping_Report_Query_Model extends MY_Model {
	
	// set column field database for datatable orderable
	protected $_column_order = array (
			'driverCode',
			'driverName',
			'driverMobile',
			'zoneName',
			'taxiRegistrationNo',
			'totalCompletedTrip',
			'totalRejectedTrip',
			'weekCompletedTrip',
			'weekRejectedTrip',
			'taxiOwnerCode',
			'taxiOwnerName',
			'taxiOwnerMobile',
			'taxiDeviceCode',
			'taxiDeviceImei',
			'taxiDeviceMobile'
	);
	
	// set column field database for datatable searchable
	protected $_column_search = array (
			'd.driverCode',
			'd.firstName',
			'd.lastName',
			'd.mobile',
			'td.registrationNo',
			'ddd.totalCompletedTrip',
			'ddd.totalRejectedTrip',
			'ddd.weekCompletedTrip',
			'ddd.weekRejectedTrip',
			'z.name',
			'tod.taxiOwnerCode',
			'tod.firstName',
			'tod.lastName',
			'tod.mobile',
			'tdd.deviceCode',
			'tdd.imeiNo',
			'tdd.mobile'
	);
	// default order
	protected $_order = array (
			'd.id' => 'desc' 
	);
	/**
	 * Default Constructor
	 */
	function __construct($args = NULL) {
		parent::__construct ();
		if (is_object ( $args ))
			$args = get_object_vars ( $args );
		if (is_array ( $args )) {
			foreach ( $args as $key => $value ) {
				$this->{$key} = $value;
			}
		}
	}
	public function getDriverTaxiMappingQuery() {
		$query = "SELECT d.id As 'driverId',d.driverCode As 'driverCode',CONCAT(d.firstName,' ',d.lastName) As 'driverName',d.mobile As 'driverMobile',
				d.email As 'driverEmail',d.profileImage As 'profileImage',d.status As 'status',dae.description As 'experience',
				dp.gender As 'gender',dp.dob As 'dob',dp.address As 'address',dp.qualification As 'qualification',dp.bankName As 'bankName',dp.bankBranch As 'bankBranch',
				dp.bankAccountNo As 'bankAccountNo',z.name As 'zoneName',ddd.weekCompletedTrip As 'weekCompletedTrip',ddd.weekRejectedTrip As 'weekRejectedTrip',ddd.totalCompletedTrip As 'totalCompletedTrip',ddd.totalRejectedTrip As 'totalRejectedTrip',
				dtm.taxiId As 'taxiId',dtm.taxiDeviceId As 'taxiDeviceId',tdd.deviceCode As 'taxiDeviceCode',tdd.mobile As 'taxiDeviceMobile',tdd.imeiNo As 'taxiDeviceImeiNo',
				td.registrationNo As 'taxiRegistrationNo',tod.taxiOwnerCode As 'taxiOwnerCode',CONCAT(tod.firstName,' ',tod.lastName) As 'taxiOwnerName',tod.mobile As 'taxiOwnerMobile',
				tod.email As 'taxiOwnerEmail'
				from  driver As d
				left join driverpersonaldetails As dp on dp.driverId=d.id
				left join driverdispatchdetails As ddd on ddd.driverId=d.id
				left join zonedetails as z on z.id =d.zoneId 
				left join dataattributes as dae on d.experience =dae.id
				left join drivertaximapping as dtm on dtm.driverId =d.id
				left join taxidevicedetails as tdd on dtm.taxiDeviceId =tdd.id
				left join taxidetails as td on dtm.taxiId =td.id
				left join taxiownerdetails as tod on tod.id =td.taxiOwnerId
				where d.isDeleted=" . Status_Type_Enum::INACTIVE . "  GROUP BY d.id";
		
		return $query;
	}
}