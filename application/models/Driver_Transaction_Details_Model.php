<?php
class Driver_Transaction_Details_Model extends MY_Model {
	protected $_table = 'drivertransactiondetails'; // model table_name
	// set column field database for datatable orderable
	protected $_column_order = array (
			null,
			'tripId',
			'transactionAmount',
			'previousAmount',
			'currentAmount',
			'transactionStatus',
			'transactionFrom',
			'transactionType',
			'transactionMode',
			'transactionId',
			'comments' 
	);
	// set column field database for datatable searchable
	protected $_column_search = array (
			'dt.tripId',
			'dt.transactionAmount',
			'dt.previousAmount',
			'dt.currentAmount',
			'dt.transactionStatus',
			'datt.description',
			'datm.description',
			'datf.description',
			'dt.transactionId',
			'dt.transactionStatus',
			'dt.tripId' 
	);
	// default order
	protected $_order = array (
			'd.id' => 'desc' 
	);
	/**
	 * Default Constructor
	 */
	function __construct($args = NULL) {
		parent::__construct ();
		if (is_object ( $args ))
			$args = get_object_vars ( $args );
		if (is_array ( $args )) {
			foreach ( $args as $key => $value ) {
				$this->{$key} = $value;
			}
		}
	}
	public function getDriverTransactionListQuery($driver_id) {
		$query = "SELECT dt.id,CONCAT(firstName ,' ', lastName) as driverName,dt.driverId,dt.tripId,dt.transactionAmount,dt.previousAmount,dt.currentAmount,
                        dt.transactionStatus,datt.description As 'transactionType',datm.description AS  transactionMode,datf.description AS  transactionFrom,
                        dt.transactionId,dt.comments,dt.createdDatetime As 'createdDatetime'
                        FROM drivertransactiondetails dt
                        LEFT JOIN driver d on dt.driverId=d.id
                        LEFT JOIN dataattributes datt on  dt.transactionType=datt.id
                        LEFT JOIN dataattributes datm on dt.transactionMode=datm.id 
                        LEFT JOIN dataattributes datf on dt.transactionFrom=datf.id  
                        WHERE dt.driverId=$driver_id";
		return $query;
	}
} // end of model class