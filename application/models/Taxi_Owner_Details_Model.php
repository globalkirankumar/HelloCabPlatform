<?php
class Taxi_Owner_Details_Model extends MY_Model {
	protected $_table = 'taxiownerdetails'; // model table_name
	public $_keyName = 'id';
	public $_valueName = 'firstName';
	/**
	 * Default Constructor
	 */
	protected $_column_order = array (
			null,
			'taxiOwnerCode',
			'firstName',
			'lastName',
			'email',
			'mobile',
			'address',
			'isDriver',
			'zoneName',
			'status'
	); // set column field database for datatable orderable
	protected $_column_search = array (
			't.taxiOwnerCode',
			't.firstName',
			't.lastName',
			't.email',
			't.mobile',
			't.address',
			't.isDriver',
			'z.name'
			 
	); // set column field database for datatable searchable just firstname , lastname , address are searchable
	protected $_order = array (
			't.id' => 'desc' 
	); // default order
	function __construct($args = NULL) {
		parent::__construct ();
		if (is_object ( $args ))
			$args = get_object_vars ( $args );
		if (is_array ( $args )) {
			foreach ( $args as $key => $value ) {
				$this->{$key} = $value;
			}
		}
	}
	/**
	 * helper method to load the key value into dropdown boxes
	 * 
	 * @return type
	 */
	public function getKeyName() {
		return $this->_keyName;
	}
	public function getValueName() {
		return $this->_valueName;
	}
	public function getTaxiOwnerListQuery() {
		$query = "SELECT t.id As 'taxiOwnerId',t.taxiOwnerCode As 'taxiOwnerCode',t.firstName As 'firstName',t.lastName As 'lastName',t.email As 'email',t.mobile As 'mobile',t.isDriver As 'isDriver',
				t.address As 'address',t.status As 'status',z.name As 'zoneName'
				from  taxiownerdetails As t
				left join zonedetails as z on z.id=t.zoneId";
		return $query;
	}
	public function getTaxiOwnerDetails($key)
	{
	
		if ($key != '') {
			$query = "SELECT id as taxiOwnerId,CONCAT(`firstName` ,' ',`lastName`) As 'taxiOwnerFullName',taxiOwnerCode As 'taxiOwnerCode',mobile As 'taxiOwnerMobile'
					FROM taxiownerdetails WHERE (mobile like '" . $key . "%' OR email like '" . $key . "%' OR firstName like '" . $key . "%'
					OR lastName like '" . $key . "%' OR taxiOwnerCode like '" . $key . "%')
					ORDER BY firstName,lastName LIMIT 0,10";
			$result = $this->db->query($query);
			return $this->fetchAll($result);
		}
	
	}
}