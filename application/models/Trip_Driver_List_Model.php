<?php
require_once (APPPATH . 'config/device_status_enum.php');
require_once (APPPATH . 'config/device_type_enum.php');
require_once (APPPATH . 'config/dispatch_type_enum.php');
require_once (APPPATH . 'config/driver_accepted_status_enum.php');
require_once (APPPATH . 'config/driver_shift_status_enum.php');
require_once (APPPATH . 'config/emergency_status_enum.php');
require_once (APPPATH . 'config/entity_type_enum.php');
require_once (APPPATH . 'config/experience_enum.php');
require_once (APPPATH . 'config/gender_type_enum.php');
require_once (APPPATH . 'config/license_type_enum.php');
require_once (APPPATH . 'config/module_name_enum.php');
require_once (APPPATH . 'config/payment_method_enum.php');
require_once (APPPATH . 'config/payment_mode_enum.php');
require_once (APPPATH . 'config/payment_type_enum.php');
require_once (APPPATH . 'config/register_type_enum.php');
require_once (APPPATH . 'config/role_type_enum.php');
require_once (APPPATH . 'config/signup_type_enum.php');
require_once (APPPATH . 'config/slab_type_enum.php');
require_once (APPPATH . 'config/status_type_enum.php');
require_once (APPPATH . 'config/taxi_available_status_enum.php');
require_once (APPPATH . 'config/taxi_request_status_enum.php');
require_once (APPPATH . 'config/taxi_type_enum.php');
require_once (APPPATH . 'config/transaction_mode_enum.php');
require_once (APPPATH . 'config/transaction_type_enum.php');
require_once (APPPATH . 'config/transaction_from_enum.php');
require_once (APPPATH . 'config/transmission_type_enum.php');
require_once (APPPATH . 'config/trip_status_enum.php');
require_once (APPPATH . 'config/trip_type_enum.php');
require_once (APPPATH . 'config/user_type_enum.php');

class Trip_Driver_List_Model extends MY_Model {
	
	// set column field database for datatable orderable
	protected $_column_order = array (
			null,
			'driverCode',
			'driverName',
			'driverMobile',
			'driverWallet',
			'distance',
			'taxiRegistrationNo',
			'avgRating'
			
	);
	
	// set column field database for datatable searchable
	protected $_column_search = array (
			'd.driverCode',
			'd.firstName',
			'd.lastName',
			'd.mobile',
			'txd.registrationNo',
			'ddd.avgRating',
	);
	
	/**
	 * Default Constructor
	 */
	function __construct($args = NULL) {
		parent::__construct ();
		if (is_object ( $args ))
			$args = get_object_vars ( $args );
		if (is_array ( $args )) {
			foreach ( $args as $key => $value ) {
				$this->{$key} = $value;
			}
		}
	}
	public function getDriverAvailableListQuery($trip_id,$distance=NULL) {
		
		if ($distance===NULL)
		{
			$distance=5;
		}	
		$query="SELECT GROUP_CONCAT(trd.selectedDriverId)  As 'selectedDriverId' FROM taxirequestdetails As trd where trd.taxiRequestStatus IN (".Taxi_Request_Status_Enum::AVAILABLE_TRIP.",".Taxi_Request_Status_Enum::DRIVER_ACCEPTED.") GROUP BY trd.selectedDriverId";
		$result = $this->db->query($query);
		$assigned_driver= $this->fetchAll($result);
		$assigned_driver_where='';
	
		$query="SELECT trd.rejectedDriverList  As 'rejectedDrivers' FROM taxirequestdetails As trd where trd.tripid=".$trip_id." ORDER BY trd.tripid DESC LIMIT 0,1";
		$result = $this->db->query($query);
		$rejected_driver= $this->fetchAll($result);
		$rejected_driver_where='';
	
		if ($rejected_driver && $rejected_driver[0]->rejectedDrivers)
		{
			$rejected_driver_where=" AND ddd.driverId NOT IN (".$rejected_driver[0]->rejectedDrivers.")";
		}
	
		if ($assigned_driver && $assigned_driver[0]->selectedDriverId)
		{
			$assigned_driver_where=" AND ddd.driverId NOT IN (".$assigned_driver[0]->selectedDriverId.")";
		}
		
		$query = "SELECT td.id As 'tripId',ddd.driverId As 'driverId',CONCAT(d.firstName,' ',d.lastName) As 'driverName',d.mobile As 'driverMobile',d.driverCode As 'driverCode',ddd.driverLatitude As 'driverLatitude',ddd.driverLongitude As 'driverLongitude',
				Round(6371 * acos( cos( radians(td.pickupLatitude) ) * cos( radians(ddd.driverLatitude) ) * cos( radians(ddd.driverLongitude) - radians(td.pickupLongitude) ) + sin( radians(td.pickupLatitude) ) * sin( radians(ddd.driverLatitude))),2) AS 'distance',
				ddd.avgRating As 'driverRating',ddd.walletBalance As 'driverWallet', ddd.avgTripDistance As 'avgTripDistance',ddd.totalCompletedTrip As 'totalCompletedTrip',ddd.weekCompletedTrip As 'weekCompletedTrip',ddd.totalRejectedTrip As 'totalRejectedTrip',
				ddd.weekRejectedTrip As 'weekRejectedTrip',txd.id As 'taxiId',txd.name As 'taxiName',txd.model As 'taxiModel',txd.manufacturer As 'taxiManufacturer',td.pickupLatitude As 'pickupLatitude',td.pickupLongitude As 'pickupLongitude',
				txd.colour As 'taxiColour',txd.registrationNo As 'taxiRegistrationNo',txd.taxiCategoryType As 'taxiCategoryType',datct.description As 'taxiCategoryName'
 				FROM tripdetails As td
				left join entityconfigdetails AS ecd ON ecd.entityId=td.entityId
				left join driverdispatchdetails AS ddd ON ddd.status=".Status_Type_Enum::ACTIVE." ".$rejected_driver_where." ".$assigned_driver_where."
				left join driver As d ON d.id=ddd.driverId
				left join drivertaximapping as dtm ON dtm.driverId=d.id
				left join taxidetails as txd ON txd.id=dtm.taxiId AND td.taxiCategoryType=txd.taxiCategoryType
				left join dataattributes as datct on td.taxiCategoryType =datct.id		
				Where td.id=".$trip_id." AND ddd.status=".Status_Type_Enum::ACTIVE." AND ddd.availabilityStatus=" . Taxi_Available_Status_Enum::FREE . "
				AND d.loginStatus=".Status_Type_Enum::ACTIVE." AND d.isDeleted=".Status_Type_Enum::INACTIVE."
				GROUP BY ddd.driverId
				
				ORDER BY distance,ddd.walletBalance desc,ddd.avgRating desc,ddd.weekCompletedTrip desc,ddd.weekRejectedTrip,ddd.totalCompletedTrip desc,ddd.totalRejectedTrip,ddd.avgTripDistance desc";
		//HAVING distance < ".$distance."
		
		return $query;
	}
}