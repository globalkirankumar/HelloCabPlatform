<?php
class Trip_Temp_Tracking_Details_Model extends MY_Model {
	protected $_table = 'triptemptrackingdetails'; // model table_name
	                                   // set column field database for datatable orderable
	
	
	/**
	 * Default Constructor
	 */
	function __construct($args = NULL) {
		parent::__construct ();
		if (is_object ( $args ))
			$args = get_object_vars ( $args );
		if (is_array ( $args )) {
			foreach ( $args as $key => $value ) {
				$this->{$key} = $value;
			}
		}
	}
	
        /**
	 *  helper method to load the key value into dropdown boxes
	 * @return type
	 */
	public function getKeyName(){
		return $this->_keyName;
	}
	
	public function getValueName(){
		return $this->_valueName;
	}
        
        public function getDriverRecentLatLong($trip_id){
            $sql    = "SELECT * FROM `triptemptrackingdetails` WHERE `tripId` = $trip_id ORDER BY `createdDatetime` DESC LIMIT 0,1";
            $result = $this->db->query($sql)->row();
            return $result;
        }
        
        
        
}// end of model