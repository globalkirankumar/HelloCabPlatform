<?php
require_once (APPPATH . 'config/device_status_enum.php');
require_once (APPPATH . 'config/device_type_enum.php');
require_once (APPPATH . 'config/dispatch_type_enum.php');
require_once (APPPATH . 'config/driver_accepted_status_enum.php');
require_once (APPPATH . 'config/driver_shift_status_enum.php');
require_once (APPPATH . 'config/emergency_status_enum.php');
require_once (APPPATH . 'config/entity_type_enum.php');
require_once (APPPATH . 'config/experience_enum.php');
require_once (APPPATH . 'config/gender_type_enum.php');
require_once (APPPATH . 'config/license_type_enum.php');
require_once (APPPATH . 'config/module_name_enum.php');
require_once (APPPATH . 'config/payment_method_enum.php');
require_once (APPPATH . 'config/payment_mode_enum.php');
require_once (APPPATH . 'config/payment_type_enum.php');
require_once (APPPATH . 'config/register_type_enum.php');
require_once (APPPATH . 'config/role_type_enum.php');
require_once (APPPATH . 'config/signup_type_enum.php');
require_once (APPPATH . 'config/slab_type_enum.php');
require_once (APPPATH . 'config/status_type_enum.php');
require_once (APPPATH . 'config/taxi_available_status_enum.php');
require_once (APPPATH . 'config/taxi_request_status_enum.php');
require_once (APPPATH . 'config/taxi_type_enum.php');
require_once (APPPATH . 'config/transaction_mode_enum.php');
require_once (APPPATH . 'config/transaction_type_enum.php');
require_once (APPPATH . 'config/transaction_from_enum.php');
require_once (APPPATH . 'config/transmission_type_enum.php');
require_once (APPPATH . 'config/trip_status_enum.php');
require_once (APPPATH . 'config/trip_type_enum.php');
require_once (APPPATH . 'config/user_type_enum.php');

class Driver_Wallet_Report_Query_Model extends MY_Model {
	
	// set column field database for datatable orderable
	protected $_column_order = array (
			'driverCode',
			'driverName',
			'driverMobile',
			'driverwallet',
			'WalletPercentage',
			'taxiRegistrationNo',
			'taxiDeviceCode',
			'taxiDeviceMobile'
	);
	
	// set column field database for datatable searchable
	protected $_column_search = array (
			'd.driverCode',
			'd.firstName',
			'd.lastName',
			'd.mobile',
			'd.driverwallet',
			'td.registrationNo',
			'tdd.deviceCode',
			'tdd.mobile'
	);
	// default order
	
	/**
	 * Default Constructor
	 */
	function __construct($args = NULL) {
		parent::__construct ();
		if (is_object ( $args ))
			$args = get_object_vars ( $args );
		if (is_array ( $args )) {
			foreach ( $args as $key => $value ) {
				$this->{$key} = $value;
			}
		}
	}
	public function getDriverWalletWarningQuery($entity_id = NULL) {
		$query="SELECT d.id as 'driverId',d.driverCode As 'driverCode',CONCAT(d.firstName,' ',d.lastName) As 'driverName',d.mobile As 'driverMobile',d.driverwallet As 'driverWallet',(d.driverWallet/ecd.driverMinWallet)*100 As 'WalletPercentage',
				(CASE WHEN ((d.driverWallet/ecd.driverMinWallet)*100 >=150 AND (d.driverWallet/ecd.driverMinWallet)*100 <=180) THEN 'TOP'
 				ELSE (CASE WHEN ((d.driverWallet/ecd.driverMinWallet)*100 >=120 AND (d.driverWallet/ecd.driverMinWallet)*100 <=150) THEN 'MID' ELSE 'LOW' END)END) AS 'walletClass',
				td.id As 'taxiId',td.registrationNo As 'taxiRegistrationNo',tdd.id As 'taxiDeviceId',tdd.deviceCode As 'taxiDeviceCode',tdd.mobile As 'taxiDeviceMobile',tdd.imeiNo As 'taxiDeviceImeiNo'
				FROM driver as d
				left join entityconfigdetails as ecd on d.entityId=ecd.entityId
				left join driverdispatchdetails As ddd on ddd.driverId=d.id
				left join drivertaximapping as dtm on dtm.driverId =d.id
				left join taxidevicedetails as tdd on dtm.taxiDeviceId =tdd.id
				left join taxidetails as td on dtm.taxiId =td.id
				WHERE ecd.entityId=".$entity_id." AND d.isDeleted=".Status_Type_Enum::INACTIVE." AND d.status=".Status_Type_Enum::ACTIVE."
				GROUP BY d.id
				HAVING WalletPercentage<=180
				ORDER BY WalletPercentage";
       	return $query;
	}
}