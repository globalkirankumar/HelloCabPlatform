<?php
require_once (APPPATH . 'config/device_status_enum.php');
require_once (APPPATH . 'config/device_type_enum.php');
require_once (APPPATH . 'config/dispatch_type_enum.php');
require_once (APPPATH . 'config/driver_accepted_status_enum.php');
require_once (APPPATH . 'config/driver_shift_status_enum.php');
require_once (APPPATH . 'config/emergency_status_enum.php');
require_once (APPPATH . 'config/entity_type_enum.php');
require_once (APPPATH . 'config/experience_enum.php');
require_once (APPPATH . 'config/gender_type_enum.php');
require_once (APPPATH . 'config/license_type_enum.php');
require_once (APPPATH . 'config/module_name_enum.php');
require_once (APPPATH . 'config/payment_method_enum.php');
require_once (APPPATH . 'config/payment_mode_enum.php');
require_once (APPPATH . 'config/payment_type_enum.php');
require_once (APPPATH . 'config/register_type_enum.php');
require_once (APPPATH . 'config/role_type_enum.php');
require_once (APPPATH . 'config/signup_type_enum.php');
require_once (APPPATH . 'config/slab_type_enum.php');
require_once (APPPATH . 'config/status_type_enum.php');
require_once (APPPATH . 'config/taxi_available_status_enum.php');
require_once (APPPATH . 'config/taxi_request_status_enum.php');
require_once (APPPATH . 'config/taxi_type_enum.php');
require_once (APPPATH . 'config/transaction_mode_enum.php');
require_once (APPPATH . 'config/transaction_type_enum.php');
require_once (APPPATH . 'config/transaction_from_enum.php');
require_once (APPPATH . 'config/transmission_type_enum.php');
require_once (APPPATH . 'config/trip_status_enum.php');
require_once (APPPATH . 'config/trip_type_enum.php');
require_once (APPPATH . 'config/user_type_enum.php');

class Driver_Reject_Report_Query_Model extends MY_Model {
	
	// set column field database for datatable orderable
	protected $_column_order = array (
			'driverCode',
			'driverName',
			'driverMobile',
			'statusName',
			'consecutiveRejectCount',
			'taxiRegistrationNo',
			'taxiDeviceCode',
			'taxiDeviceMobile'
	);
	
	// set column field database for datatable searchable
	protected $_column_search = array (
			'd.driverCode',
			'd.firstName',
			'd.lastName',
			'd.mobile',
			'ddd.consecutiveRejectCount',
			'td.registrationNo',
			'tdd.deviceCode',
			'tdd.mobile'
	);
	// default order
	protected $_order = array (
			'd.id' => 'desc' 
	);
	/**
	 * Default Constructor
	 */
	function __construct($args = NULL) {
		parent::__construct ();
		if (is_object ( $args ))
			$args = get_object_vars ( $args );
		if (is_array ( $args )) {
			foreach ( $args as $key => $value ) {
				$this->{$key} = $value;
			}
		}
	}
	public function getDriverRejectQuery($time_period,$consecutive_reject='') {
		$where_caluse = '';
		$time_period_where = '';
		$consecutive_reject_where = '';
		
		if ($time_period != '') {
		$time_period_where = " AND ddd.updatedDatetime BETWEEN ADDTIME('".getCurrentDateTime()."' ,-".$time_period.") AND '".getCurrentDateTime()."'";
		}
		if ($consecutive_reject != '') {
			$consecutive_reject_where = ' AND ddd.consecutiveRejectCount=' . $consecutive_reject;
		}
		
		$where_caluse = $time_period_where . " " . $consecutive_reject_where ;
		
		$query = "SELECT d.id As 'driverId',d.driverCode As 'driverCode',CONCAT(d.firstName,' ',d.lastName) As 'driverName',d.mobile As 'driverMobile',
				d.email As 'driverEmail',d.profileImage As 'profileImage',d.status As 'status',ddd.consecutiveRejectCount As 'consecutiveRejectCount',
				(CASE WHEN d.status  THEN 'Active' ELSE 'Inactive' END) As 'statusName',
				td.id As 'taxiId',td.registrationNo As 'taxiRegistrationNo',tdd.id As 'taxiDeviceId',tdd.deviceCode As 'taxiDeviceCode',tdd.mobile As 'taxiDeviceMobile',tdd.imeiNo As 'taxiDeviceImeiNo'
				from  driver As d
				left join driverdispatchdetails As ddd on ddd.driverId=d.id
				left join drivertaximapping as dtm on dtm.driverId =d.id
				left join taxidevicedetails as tdd on dtm.taxiDeviceId =tdd.id
				left join taxidetails as td on dtm.taxiId =td.id
				where d.isDeleted=" . Status_Type_Enum::INACTIVE . " AND d.status=".Status_Type_Enum::ACTIVE." ".$where_caluse."  GROUP BY d.id";
		
		return $query;
	}
}