<?php
require_once (APPPATH . 'config/device_status_enum.php');
require_once (APPPATH . 'config/device_type_enum.php');
require_once (APPPATH . 'config/dispatch_type_enum.php');
require_once (APPPATH . 'config/driver_accepted_status_enum.php');
require_once (APPPATH . 'config/driver_shift_status_enum.php');
require_once (APPPATH . 'config/emergency_status_enum.php');
require_once (APPPATH . 'config/entity_type_enum.php');
require_once (APPPATH . 'config/experience_enum.php');
require_once (APPPATH . 'config/gender_type_enum.php');
require_once (APPPATH . 'config/license_type_enum.php');
require_once (APPPATH . 'config/module_name_enum.php');
require_once (APPPATH . 'config/payment_method_enum.php');
require_once (APPPATH . 'config/payment_mode_enum.php');
require_once (APPPATH . 'config/payment_type_enum.php');
require_once (APPPATH . 'config/register_type_enum.php');
require_once (APPPATH . 'config/role_type_enum.php');
require_once (APPPATH . 'config/signup_type_enum.php');
require_once (APPPATH . 'config/slab_type_enum.php');
require_once (APPPATH . 'config/status_type_enum.php');
require_once (APPPATH . 'config/taxi_available_status_enum.php');
require_once (APPPATH . 'config/taxi_request_status_enum.php');
require_once (APPPATH . 'config/taxi_type_enum.php');
require_once (APPPATH . 'config/transaction_mode_enum.php');
require_once (APPPATH . 'config/transaction_type_enum.php');
require_once (APPPATH . 'config/transaction_from_enum.php');
require_once (APPPATH . 'config/transmission_type_enum.php');
require_once (APPPATH . 'config/trip_status_enum.php');
require_once (APPPATH . 'config/trip_type_enum.php');
require_once (APPPATH . 'config/user_type_enum.php');
class Driver_Daily_Report_Query_Model extends MY_Model {
	
	// set column field database for datatable orderable
	protected $_column_order = array (
			'driverCode',
			'driverName',
			'driverMobile',
			'taxiRegistrationNo',
			'hour',
			'minute' 
	);
	
	// set column field database for datatable searchable
	protected $_column_search = array (
			'd.driverCode',
			'd.firstName',
			'd.lastName',
			'd.mobile',
			'td.registrationNo' 
	);
	// default order
	/*
	 * protected $_order = array (
	 * 'd.driverCredit' => 'desc'
	 * );
	 */
	/**
	 * Default Constructor
	 */
	function __construct($args = NULL) {
		parent::__construct ();
		if (is_object ( $args ))
			$args = get_object_vars ( $args );
		if (is_array ( $args )) {
			foreach ( $args as $key => $value ) {
				$this->{$key} = $value;
			}
		}
	}
	public function getDriverDailyReportQuery($report_date) {
		
		
		/* $query = "SELECT d.driverCode As 'driverCode',CONCAT(d.firstName,' ',d.lastName) As 'driverName',d.mobile As 'driverMobile',td.registrationNo as 'taxiRegistrationNo',
		 
		 (CASE WHEN(dsh.availabilityStatus=".Taxi_Available_Status_Enum::FREE." AND dsh.shiftStatus=".Driver_Shift_Status_Enum::SHIFTIN." AND DATE(dsh.createddatetime)=DATE('".$report_date."') AND DATE(dsh.updatedDatetime)=DATE('".$report_date."')) THEN (SUM(TIMESTAMPDIFF(HOUR,dsh.createddatetime,'".getCurrentDateTime()."')%24)) ELSE
		 
		  (CASE WHEN (dsh.availabilityStatus=".Taxi_Available_Status_Enum::BUSY." AND dsh.shiftStatus=".Driver_Shift_Status_Enum::SHIFTIN." AND DATE(dsh.createddatetime)=DATE('".$report_date."') AND DATE(dsh.updatedDatetime)=DATE('".$report_date."')) THEN (SUM(TIMESTAMPDIFF(HOUR,dsh.createddatetime,dsh.updatedDatetime))%24) ELSE
		  (CASE WHEN(dsh.availabilityStatus=".Taxi_Available_Status_Enum::BUSY." AND dsh.shiftStatus=".Driver_Shift_Status_Enum::SHIFTIN." AND DATE(dsh.createddatetime)<=DATE('".$report_date."') AND DATE(dsh.updatedDatetime)>=DATE('".$report_date."')) THEN
		  (CASE WHEN(((TIMESTAMPDIFF(HOUR,dsh.createddatetime,dsh.updatedDatetime)))>24) THEN
		 
		  ((CASE WHEN(DATE(dsh.createdDatetime)<DATE('".$report_date."') AND DATE(dsh.updatedDatetime) >DATE('".$report_date."')) THEN (SUM(TIMESTAMPDIFF(HOUR,CONCAT(DATE('".$report_date."'),' ','00:00:00'),CONCAT(DATE('".$report_date."'),' ','23:59:59')))%24) ELSE
		 
		  (CASE WHEN(DATE(dsh.createdDatetime)=DATE('".$report_date."') AND DATE(dsh.updatedDatetime) >DATE('".$report_date."')) THEN (SUM(TIMESTAMPDIFF(HOUR,dsh.createdDatetime,CONCAT(DATE(dsh.createdDatetime),' ','23:59:59')))%24) ELSE
		 
		  (CASE WHEN(DATE(dsh.updatedDatetime)=DATE('".$report_date."') AND DATE(dsh.createdDatetime) <DATE('".$report_date."')) THEN (SUM(TIMESTAMPDIFF(HOUR,CONCAT(DATE(dsh.updatedDatetime),' ','00:00:00'),dsh.updatedDatetime))%24)
		 
		  END)END)END)) ELSE
		 
		  ((CASE WHEN(DATE(dsh.createdDatetime)=DATE('".$report_date."') AND DATE(dsh.updatedDatetime) >DATE('".$report_date."')) THEN (SUM(TIMESTAMPDIFF(HOUR,dsh.createdDatetime,CONCAT(DATE(dsh.createdDatetime),' ','23:59:59')))%24) ELSE
		  (CASE WHEN(DATE(dsh.updatedDatetime)=DATE('".$report_date."') AND DATE(dsh.createdDatetime) <DATE('".$report_date."')) THEN (SUM(TIMESTAMPDIFF(HOUR,CONCAT(DATE(dsh.updatedDatetime),' ','00:00:00'),dsh.updatedDatetime))%24) ELSE '24'
		  END)END))
		 
		  END)END)END)END) AS 'hour',
		 
		  (CASE WHEN(dsh.availabilityStatus=".Taxi_Available_Status_Enum::FREE." AND dsh.shiftStatus=".Driver_Shift_Status_Enum::SHIFTIN." AND DATE(dsh.createddatetime)=DATE('".$report_date."') AND DATE(dsh.updatedDatetime)=DATE('".$report_date."')) THEN (SUM(TIMESTAMPDIFF(HOUR,dsh.createddatetime,'".getCurrentDateTime()."')%24)) ELSE
		 
		  (CASE WHEN (dsh.availabilityStatus=".Taxi_Available_Status_Enum::BUSY." AND dsh.shiftStatus=".Driver_Shift_Status_Enum::SHIFTIN." AND DATE(dsh.createddatetime)=DATE('".$report_date."') AND DATE(dsh.updatedDatetime)=DATE('".$report_date."')) THEN (SUM(TIMESTAMPDIFF(MINUTE,dsh.createddatetime,dsh.updatedDatetime))%60) ELSE
		  (CASE WHEN(dsh.availabilityStatus=".Taxi_Available_Status_Enum::BUSY." AND dsh.shiftStatus=".Driver_Shift_Status_Enum::SHIFTIN." AND DATE(dsh.createddatetime)<=DATE('".$report_date."') AND DATE(dsh.updatedDatetime)>=DATE('".$report_date."')) THEN
		  (CASE WHEN(((TIMESTAMPDIFF(HOUR,dsh.createddatetime,dsh.updatedDatetime)))>24) THEN
		 
		  ((CASE WHEN(DATE(dsh.createdDatetime)<DATE('".$report_date."') AND DATE(dsh.updatedDatetime) >DATE('".$report_date."')) THEN (SUM(TIMESTAMPDIFF(MINUTE,CONCAT(DATE('".$report_date."'),' ','00:00:00'),CONCAT(DATE('".$report_date."'),' ','23:59:59')))%60) ELSE
		 
		  (CASE WHEN(DATE(dsh.createdDatetime)=DATE('".$report_date."') AND DATE(dsh.updatedDatetime) >DATE('".$report_date."')) THEN (SUM(TIMESTAMPDIFF(MINUTE,dsh.createdDatetime,CONCAT(DATE(dsh.createdDatetime),' ','23:59:59')))%60) ELSE
		 
		  (CASE WHEN(DATE(dsh.updatedDatetime)=DATE('".$report_date."') AND DATE(dsh.createdDatetime) <DATE('".$report_date."')) THEN (SUM(TIMESTAMPDIFF(MINUTE,CONCAT(DATE(dsh.updatedDatetime),' ','00:00:00'),dsh.updatedDatetime))%60)
		 
		  END)END)END)) ELSE
		 
		  ((CASE WHEN(DATE(dsh.createdDatetime)=DATE('".$report_date."') AND DATE(dsh.updatedDatetime) >DATE('".$report_date."')) THEN (SUM(TIMESTAMPDIFF(MINUTE,dsh.createdDatetime,CONCAT(DATE(dsh.createdDatetime),' ','23:59:59')))%60) ELSE
		  (CASE WHEN(DATE(dsh.updatedDatetime)=DATE('".$report_date."') AND DATE(dsh.createdDatetime) <DATE('".$report_date."')) THEN (SUM(TIMESTAMPDIFF(MINUTE,CONCAT(DATE(dsh.updatedDatetime),' ','00:00:00'),dsh.updatedDatetime))%60) ELSE '0'
		  END)END))
		 
		  END)END)END)END) AS 'minute'
		 
		 
		  FROM drivershifthistory as dsh
		  right join driver as d On d.id=dsh.driverId
		  left join drivertaximapping as dtm on dtm.driverId=d.id
		  left join taxidetails as td on td.id=dtm.taxiId
		  where DATE(dsh.createddatetime)='".$report_date."' OR DATE(dsh.updatedDatetime)='".$report_date."' OR (DATE(dsh.createdDatetime)<DATE('".$report_date."') AND DATE(dsh.updatedDatetime) >DATE('".$report_date."')) group by dsh.driverid";
		
		*/$query = "SELECT d.driverCode As 'driverCode',CONCAT(d.firstName,' ',d.lastName) As 'driverName',d.mobile As 'driverMobile',td.registrationNo as 'taxiRegistrationNo',
		 	
		(CASE WHEN(dsh.availabilityStatus=" . Taxi_Available_Status_Enum::FREE . " AND dsh.shiftStatus=" . Driver_Shift_Status_Enum::SHIFTIN . ") THEN 
		
		((CASE WHEN(DATE('" . getCurrentDate () . "')=DATE('" . $report_date . "') AND DATE(dsh.createdDatetime)=DATE('" . $report_date . "') AND DATE(dsh.updatedDatetime)=DATE('" . $report_date . "')) THEN  (SUM(TIMESTAMPDIFF(HOUR,dsh.createddatetime,'" . getCurrentDateTime () . "')%24)) ELSE 
		 
		 (CASE WHEN(DATE('" . getCurrentDate () . "')!=DATE('" . $report_date . "') AND DATE(dsh.createdDatetime)!=DATE('" . $report_date . "') AND DATE(dsh.updatedDatetime)!=DATE('" . $report_date . "')) THEN (SUM(TIMESTAMPDIFF(HOUR,CONCAT(DATE('" . $report_date . "'),' ','00:00:00'),CONCAT(DATE('" . $report_date . "'),' ','23:59:59')))%24) ELSE 
		 
		 (CASE WHEN(DATE('" . getCurrentDate () . "')=DATE('" . $report_date . "') AND DATE(dsh.createdDatetime)!=DATE('" . $report_date . "') AND DATE(dsh.updatedDatetime)!=DATE('" . $report_date . "')) THEN
		 (SUM(TIMESTAMPDIFF(HOUR,CONCAT(DATE('" . getCurrentDate () . "'),' ','00:00:00'),'" . getCurrentDateTime () . "'))%24) END)END)END)) ELSE
			
		(CASE WHEN (dsh.availabilityStatus=" . Taxi_Available_Status_Enum::BUSY . " AND dsh.shiftStatus=" . Driver_Shift_Status_Enum::SHIFTIN . " AND DATE(dsh.createddatetime)=DATE('" . $report_date . "') AND DATE(dsh.updatedDatetime)=DATE('" . $report_date . "')) THEN (SUM(TIMESTAMPDIFF(HOUR,dsh.createddatetime,dsh.updatedDatetime))%24) ELSE
		(CASE WHEN(dsh.availabilityStatus=" . Taxi_Available_Status_Enum::BUSY . " AND dsh.shiftStatus=" . Driver_Shift_Status_Enum::SHIFTIN . " AND DATE(dsh.createddatetime)<=DATE('" . $report_date . "') AND DATE(dsh.updatedDatetime)>=DATE('" . $report_date . "')) THEN
		(CASE WHEN(((TIMESTAMPDIFF(HOUR,dsh.createddatetime,dsh.updatedDatetime)))>24) THEN
			
		((CASE WHEN(DATE(dsh.createdDatetime)<DATE('" . $report_date . "') AND DATE(dsh.updatedDatetime) >DATE('" . $report_date . "')) THEN (SUM(TIMESTAMPDIFF(HOUR,CONCAT(DATE('" . $report_date . "'),' ','00:00:00'),CONCAT(DATE('" . $report_date . "'),' ','23:59:59')))%24) ELSE
			
		(CASE WHEN(DATE(dsh.createdDatetime)=DATE('" . $report_date . "') AND DATE(dsh.updatedDatetime) >DATE('" . $report_date . "')) THEN (SUM(TIMESTAMPDIFF(HOUR,dsh.createdDatetime,CONCAT(DATE(dsh.createdDatetime),' ','23:59:59')))%24) ELSE
			
		(CASE WHEN(DATE(dsh.updatedDatetime)=DATE('" . $report_date . "') AND DATE(dsh.createdDatetime) <DATE('" . $report_date . "')) THEN (SUM(TIMESTAMPDIFF(HOUR,CONCAT(DATE(dsh.updatedDatetime),' ','00:00:00'),dsh.updatedDatetime))%24)
			
		END)END)END)) ELSE
			
		((CASE WHEN(DATE(dsh.createdDatetime)=DATE('" . $report_date . "') AND DATE(dsh.updatedDatetime) >DATE('" . $report_date . "')) THEN (SUM(TIMESTAMPDIFF(HOUR,dsh.createdDatetime,CONCAT(DATE(dsh.createdDatetime),' ','23:59:59')))%24) ELSE
		(CASE WHEN(DATE(dsh.updatedDatetime)=DATE('" . $report_date . "') AND DATE(dsh.createdDatetime) <DATE('" . $report_date . "')) THEN (SUM(TIMESTAMPDIFF(HOUR,CONCAT(DATE(dsh.updatedDatetime),' ','00:00:00'),dsh.updatedDatetime))%24) ELSE '24'
		END)END))
			
		END)END)END)END) AS 'hour',
		
		(CASE WHEN(dsh.availabilityStatus=" . Taxi_Available_Status_Enum::FREE . " AND dsh.shiftStatus=" . Driver_Shift_Status_Enum::SHIFTIN . " ) THEN
		
		((CASE WHEN(DATE('" . getCurrentDate () . "')=DATE('" . $report_date . "') AND DATE(dsh.createdDatetime)!=DATE('" . $report_date . "') AND DATE(dsh.updatedDatetime)=DATE('" . $report_date . "')) THEN  (SUM(TIMESTAMPDIFF(MINUTE,dsh.createddatetime,'" . getCurrentDateTime () . "')%60)) ELSE 
		 
		 (CASE WHEN(DATE('" . getCurrentDate () . "')!=DATE('" . $report_date . "') AND DATE(dsh.createdDatetime)!=DATE('" . $report_date . "') AND DATE(dsh.updatedDatetime)!=DATE('" . $report_date . "')) THEN (SUM(TIMESTAMPDIFF(MINUTE,CONCAT(DATE('" . $report_date . "'),' ','00:00:00'),CONCAT(DATE('" . $report_date . "'),' ','23:59:59')))%60) ELSE 
		 
		 (CASE WHEN(DATE('" . getCurrentDate () . "')=DATE('" . $report_date . "') AND DATE(dsh.createdDatetime)!=DATE('" . $report_date . "') AND DATE(dsh.updatedDatetime)!=DATE('" . $report_date . "')) THEN
		 (SUM(TIMESTAMPDIFF(MINUTE,CONCAT(DATE('" . getCurrentDate () . "'),' ','00:00:00'),'" . getCurrentDateTime () . "'))%60) END)END)END)) ELSE
		 
		(CASE WHEN (dsh.availabilityStatus=" . Taxi_Available_Status_Enum::BUSY . " AND dsh.shiftStatus=" . Driver_Shift_Status_Enum::SHIFTIN . " AND DATE(dsh.createddatetime)=DATE('" . $report_date . "') AND DATE(dsh.updatedDatetime)=DATE('" . $report_date . "')) THEN (SUM(TIMESTAMPDIFF(MINUTE,dsh.createddatetime,dsh.updatedDatetime))%60) ELSE
		(CASE WHEN(dsh.availabilityStatus=" . Taxi_Available_Status_Enum::BUSY . " AND dsh.shiftStatus=" . Driver_Shift_Status_Enum::SHIFTIN . " AND DATE(dsh.createddatetime)<=DATE('" . $report_date . "') AND DATE(dsh.updatedDatetime)>=DATE('" . $report_date . "')) THEN
		(CASE WHEN(((TIMESTAMPDIFF(HOUR,dsh.createddatetime,dsh.updatedDatetime)))>24) THEN
			
		((CASE WHEN(DATE(dsh.createdDatetime)<DATE('" . $report_date . "') AND DATE(dsh.updatedDatetime) >DATE('" . $report_date . "')) THEN (SUM(TIMESTAMPDIFF(MINUTE,CONCAT(DATE('" . $report_date . "'),' ','00:00:00'),CONCAT(DATE('" . $report_date . "'),' ','23:59:59')))%60) ELSE
			
		(CASE WHEN(DATE(dsh.createdDatetime)=DATE('" . $report_date . "') AND DATE(dsh.updatedDatetime) >DATE('" . $report_date . "')) THEN (SUM(TIMESTAMPDIFF(MINUTE,dsh.createdDatetime,CONCAT(DATE(dsh.createdDatetime),' ','23:59:59')))%60) ELSE
			
		(CASE WHEN(DATE(dsh.updatedDatetime)=DATE('" . $report_date . "') AND DATE(dsh.createdDatetime) <DATE('" . $report_date . "')) THEN (SUM(TIMESTAMPDIFF(MINUTE,CONCAT(DATE(dsh.updatedDatetime),' ','00:00:00'),dsh.updatedDatetime))%60)
			
		END)END)END)) ELSE
			
		((CASE WHEN(DATE(dsh.createdDatetime)=DATE('" . $report_date . "') AND DATE(dsh.updatedDatetime) >DATE('" . $report_date . "')) THEN (SUM(TIMESTAMPDIFF(MINUTE,dsh.createdDatetime,CONCAT(DATE(dsh.createdDatetime),' ','23:59:59')))%60) ELSE
		(CASE WHEN(DATE(dsh.updatedDatetime)=DATE('" . $report_date . "') AND DATE(dsh.createdDatetime) <DATE('" . $report_date . "')) THEN (SUM(TIMESTAMPDIFF(MINUTE,CONCAT(DATE(dsh.updatedDatetime),' ','00:00:00'),dsh.updatedDatetime))%60) ELSE '0'
		END)END))
			
		END)END)END)END) AS 'minute'
			
			
		FROM drivershifthistory as dsh
		right join driver as d On d.id=dsh.driverId
		left join drivertaximapping as dtm on dtm.driverId=d.id
		left join taxidetails as td on td.id=dtm.taxiId
		where DATE(dsh.createddatetime)>='".$report_date."' OR DATE(dsh.updatedDatetime)<='".$report_date."' OR (DATE(dsh.createdDatetime)<DATE('".$report_date."') AND DATE(dsh.updatedDatetime) >DATE('".$report_date."')) group by d.id"; 
		 
		return $query;
		// (CASE WHEN(dsh.availabilityStatus=40 AND dsh.shiftStatus=42 AND DATE(dsh.createddatetime)=DATE(dsh.updatedDatetime)AND dsh.createddatetime!=dsh.updatedDatetime) THEN (FLOOR((SUM(TIMESTAMPDIFF(MINUTE,dsh.createddatetime,'2018-01-01 23:59:59'))/60))) ELSE
		// (CASE WHEN(dsh.availabilityStatus=40 AND dsh.shiftStatus=42 AND DATE(dsh.createddatetime)<DATE(dsh.updatedDatetime)) THEN (FLOOR((SUM(TIMESTAMPDIFF(MINUTE,'2018-01-01 00:00:00',dsh.updatedDatetime))/60)))END)END)
	}
}