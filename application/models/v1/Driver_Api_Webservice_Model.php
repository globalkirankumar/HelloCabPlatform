<?php
require_once (APPPATH . 'config/device_status_enum.php');
require_once (APPPATH . 'config/device_type_enum.php');
require_once (APPPATH . 'config/dispatch_type_enum.php');
require_once (APPPATH . 'config/driver_accepted_status_enum.php');
require_once (APPPATH . 'config/dispatch_type_enum.php');
require_once (APPPATH . 'config/driver_shift_status_enum.php');
require_once (APPPATH . 'config/emergency_status_enum.php');
require_once (APPPATH . 'config/entity_type_enum.php');
require_once (APPPATH . 'config/experience_enum.php');
require_once (APPPATH . 'config/gender_type_enum.php');
require_once (APPPATH . 'config/license_type_enum.php');
require_once (APPPATH . 'config/module_name_enum.php');
require_once (APPPATH . 'config/payment_method_enum.php');
require_once (APPPATH . 'config/payment_mode_enum.php');
require_once (APPPATH . 'config/payment_type_enum.php');
require_once (APPPATH . 'config/promocode_type_enum.php');
require_once (APPPATH . 'config/register_type_enum.php');
require_once (APPPATH . 'config/role_type_enum.php');
require_once (APPPATH . 'config/signup_type_enum.php');
require_once (APPPATH . 'config/slab_type_enum.php');
require_once (APPPATH . 'config/status_type_enum.php');
require_once (APPPATH . 'config/taxi_available_status_enum.php');
require_once (APPPATH . 'config/taxi_request_status_enum.php');
require_once (APPPATH . 'config/taxi_type_enum.php');
require_once (APPPATH . 'config/transaction_mode_enum.php');
require_once (APPPATH . 'config/transaction_type_enum.php');
require_once (APPPATH . 'config/transaction_from_enum.php');
require_once (APPPATH . 'config/transmission_type_enum.php');
require_once (APPPATH . 'config/trip_status_enum.php');
require_once (APPPATH . 'config/trip_type_enum.php');
require_once (APPPATH . 'config/user_type_enum.php');

class Driver_Api_Webservice_Model extends MY_Model {
	
	/**
	 * Default Constructor
	 */
	function __construct($args = NULL) {
		parent::__construct ();
		if (is_object ( $args ))
			$args = get_object_vars ( $args );
		if (is_array ( $args )) {
			foreach ( $args as $key => $value ) {
				$this->{$key} = $value;
			}
		}
	}
	
	/**
	 *To get the driver earning details
	 * @param unknown $driver_id
	 * @return Ambigous <multitype:, multitype:object >|boolean
	 */
	public function getDriverEarningDetails($driver_id,$current_date=NULL,$start_date=NULL,$end_date=NULL) {
		$where_clause='';
		if ($current_date)
		{
			$where_clause="AND td.actualPickupDatetime  LIKE '" . getCurrentDate () . "%'";
		}
		if ($start_date && $end_date)
		{
			$where_clause="AND td.actualPickupDatetime >='" . $start_date . "' AND td.actualPickupDatetime <='" . $end_date . "'";
		}
		if ($driver_id) {
			$query = "SELECT td.id As 'tripId',td.driverId As 'driverId',td.passengerRating As 'driverRating',td.actualPickupDatetime As 'actualPickupDatetime',
					td.dropDatetime As 'dropDatetime',ttd.totalTripCharge As 'totalTripCost',ttd.driverEarning As 'driverEarning'
					from tripdetails as td
					left join triptransactiondetails as ttd on ttd.tripId=td.id
					where td.driverId=" . $driver_id . " AND td.tripStatus='" . Trip_Status_Enum::TRIP_COMPLETED . "' ".$where_clause;
			$result = $this->db->query ( $query );
			return $this->fetchAll ( $result );
		} else {
			return FALSE;
		}
	}
	
	public function getBillDetails($transaction_id)
	{
		$query = "SELECT CONCAT(p.firstName ,' ',p.lastName) As 'passengerFullName',p.email As passengerEmail,
				 CONCAT(d.firstName ,' ',d.lastName) As 'driverFullName',d.id As 'driverId',d.profileImage As 'driverProfileImage',ttd.invoiceNo As 'invoiceNo',ttd.createdDatetime As 'invoiceDatetime',
				 td.actualPickupDatetime As 'actualPickupDatetime',td.dropDatetime As 'dropDatetime',td.createdDatetime As 'bookedDatetime',td.paymentMode As 'paymentMode',td.pickupLocation As 'pickupLocation',td.dropLocation As 'dropLocation',
				td.passengerRating As 'driverRated',
				 datt.description As 'tripTypeName',ttd.travelledDistance As 'travelledDistance',ttd.travelledPeriod As 'travelledPeriod',ttd.surgeCharge As 'surgeCharge',ttd.penaltyCharge As 'penaltyCharge',
				ttd.bookingCharge As 'bookingCharge',td.jobCardId As 'jobCardId',td.customerReferenceCode As 'customerReferenceCode',ttd.taxCharge As 'taxCharge',ttd.totalTripCharge As 'totalTripCost',ttd.convenienceCharge As 'convenienceCharge',
				ttd.travelCharge As 'travelCharge', ttd.parkingCharge As 'parkingCharge', ttd.tollCharge As 'tollCharge',ttd.promoDiscountAmount As 'promoDiscountAmount', ttd.adminAmount As 'adminAmount',ttd.walletPaymentAmount As 'walletPaymentAmount',
				ttd.travelledPeriod As 'travelledPeriod',ttd.taxPercentage As 'taxPercentage',ct.currencyCode As 'currencyCode',ct.currencySymbol As 'currencySymbol',datct.description As 'taxiCategoryTypeName',t.model As 'taxiModel',t.registrationNo As 'taxiRegistrationNo'
				from triptransactiondetails As ttd
				left join tripdetails As td on ttd.tripId=td.id
				left join passenger As p on td.passengerId=p.id
				left join driver As d on td.driverId=d.id
				left join taxidetails As t on td.taxiId=t.id
				left join entitydetails As ed on td.entityId
				left join country As ct on ct.id=ed.countryId
				left join dataattributes As datt on datt.id = td.tripType
				left join dataattributes As datct on datct.id = td.taxiCategoryType
				where ttd.id=".$transaction_id." AND td.tripStatus=" . Trip_Status_Enum::TRIP_COMPLETED ;
	
		$result = $this->db->query ( $query );
		return $this->fetchAll ( $result );
	}
	
	/**
	 * To get pending trip deatils for particular driver
	 * @param unknown $driver_id
	 * @param number $pagination
	 * @param unknown $start_offset
	 * @param unknown $end_limit
	 * @param string $start_date
	 * @param string $end_date
	 * @return Ambigous <multitype:, multitype:object >|boolean
	 */
	public function getDriverPendingTripList($driver_id, $start_offset, $end_limit=20, $start_date = NULL, $end_date = NULL) {
		if ($driver_id) {
			$limit = '';
			$date_range = "AND td.pickupDatetime >='" . getCurrentDateTime () . "'";
			if ($start_date && $end_date) {
				//$date_range = " AND td.pickupDatetime >='" . date ( 'Y-m-d H:i:s', strtotime ( $start_date ) ) . "' AND td.pickupDatetime <='" . date ( 'Y-m-d H:i:s', strtotime ( $end_date ) ) . "'";
				$date_range = " AND date(td.pickupDatetime) between date ('".$start_date."') AND date ('".$end_date."')";
			}
			if ($start_offset && $end_limit) {
				$limit = ' LIMIT ' . $start_offset . ',' . $end_limit;
			}
			$query = "SELECT td.id As 'tripId',td.jobCardId As 'jobCardId',td.driverId As 'driverId',td.passengerId As 'passengerId',td.pickupDatetime As 'pickupDatetime',td.pickupLatitude As 'pickupLatitude',td.pickupLongitude As 'pickupLongitude',
					td.dropDatetime As 'dropDatetime',td.dropLatitude As 'dropLatitude',td.dropLongitude As 'dropLongitude',td.pickupLocation As 'pickupLocation',td.dropLocation As 'dropLocation',td.tripStatus As 'tripStatus',dats.description As 'tripStatusName',
					p.firstName As 'passengerFirstName',p.lastName As 'passengerLastName',d.firstName As 'driverFirstName',d.lastName As 'driverLastName',
					td.customerReferenceCode As 'customerReferenceCode',td.taxiCategoryType As 'taxiCategoryType',datct.description As 'taxiCategoryTypeName'
					from tripdetails as td
					left join passenger as p on td.passengerId=p.id
					left join driver as d on td.driverId=d.id
					left join dataattributes as datct on td.taxiCategoryType =datct.id
					left join dataattributes as dats on td.tripStatus =dats.id
					where td.driverId=" . $driver_id . " AND td.tripStatus IN ('" . Trip_Status_Enum::DRIVER_ACCEPTED . "','" . Trip_Status_Enum::IN_PROGRESS . "','" . Trip_Status_Enum::DRIVER_ARRIVED . "','" . Trip_Status_Enum::WAITING_FOR_PAYMENT . "') " . $date_range . " order by td.pickupDatetime DESC,td.tripStatus DESC " . $limit;
			$result = $this->db->query ( $query );
			return $this->fetchAll ( $result );
		} else {
			return FALSE;
		}
	}
	/**
	 * To get pending trip deatils for particular driver
	 * @param unknown $driver_id
	 * @param number $pagination
	 * @param unknown $start_offset
	 * @param unknown $end_limit
	 * @param string $start_date
	 * @param string $end_date
	 * @return Ambigous <multitype:, multitype:object >|boolean
	 */
	public function getDriverCancelledTripList($driver_id, $start_offset, $end_limit=20, $start_date = NULL, $end_date = NULL) {
		if ($driver_id) {
			$limit = '';
			//to get from current date & furture cancelled trip list
			//$date_range = "AND td.pickupDatetime >='" . getCurrentDateTime () . "'";
			//to get all cancelled trip list
			$date_range = "";
			if ($start_date && $end_date) {
				//$date_range = " AND td.pickupDatetime >='" . date ( 'Y-m-d H:i:s', strtotime ( $start_date ) ) . "' AND td.pickupDatetime <='" . date ( 'Y-m-d H:i:s', strtotime ( $end_date ) ) . "'";
				$date_range = " AND date(td.pickupDatetime) between date ('".$start_date."') AND date ('".$end_date."')";
			}
			if ($start_offset && $end_limit) {
				$limit = ' LIMIT ' . $start_offset . ',' . $end_limit;
			}
			$query = "SELECT td.id As 'tripId',td.jobCardId As 'jobCardId',td.driverId As 'driverId',td.passengerId As 'passengerId',td.pickupDatetime As 'pickupDatetime',td.pickupLatitude As 'pickupLatitude',td.pickupLongitude As 'pickupLongitude',
					td.dropDatetime As 'dropDatetime',td.dropLatitude As 'dropLatitude',td.dropLongitude As 'dropLongitude',td.pickupLocation As 'pickupLocation',td.dropLocation As 'dropLocation',td.tripStatus As 'tripStatus',dats.description As 'tripStatusName',
					p.firstName As 'passengerFirstName',p.lastName As 'passengerLastName',d.firstName As 'driverFirstName',d.lastName As 'driverLastName',
					td.customerReferenceCode As 'customerReferenceCode',td.taxiCategoryType As 'taxiCategoryType',datct.description As 'taxiCategoryTypeName'
					from taxirejectedtripdetails as drtd
					left join tripdetails as td on td.id=drtd.tripId
					left join passenger as p on drtd.passengerId=p.id
					left join driver as d on drtd.driverId=d.id
					left join dataattributes as datct on td.taxiCategoryType =datct.id
					left join dataattributes as dats on dats.id =td.tripStatus
					where drtd.driverId=" . $driver_id . " " . $date_range . " order by td.pickupDatetime DESC,td.tripStatus DESC " . $limit;
			$result = $this->db->query ( $query );
			return $this->fetchAll ( $result );
		} else {
			return FALSE;
		}
	}
	/**
	 * To get complete trip details for particular driver
	 * @param unknown $driver_id
	 * @param number $pagination
	 * @param string $start_offset
	 * @param string $end_limit
	 * @param string $start_date
	 * @param string $end_date
	 * @return Ambigous <multitype:, multitype:object >|boolean
	 */
	public function getDriverPastTripList($driver_id, $start_offset, $end_limit = 20, $start_date = NULL, $end_date = NULL) {
		if ($driver_id) {
			$limit = '';
			$date_range = "";
			if ($start_date && $end_date) {
				//$date_range = " AND td.pickupDatetime >='" . date ( 'Y-m-d H:i:s', strtotime ( $start_date ) ) . "' AND td.pickupDatetime <='" . date ( 'Y-m-d H:i:s', strtotime ( $end_date ) ) . "'";
				$date_range = " AND date(td.pickupDatetime) between date ('".$start_date."') AND date ('".$end_date."')";
			}
			if ($start_offset && $end_limit) {
				$limit = ' LIMIT ' . $start_offset . ',' . $end_limit;
			}
			$query = "SELECT td.id As 'tripId',td.jobCardId As 'jobCardId',td.driverId As 'driverId',td.passengerId As 'passengerId',
					td.actualPickupDatetime As 'actualPickupDatetime',td.pickupLatitude As 'pickupLatitude',td.pickupLongitude As 'pickupLongitude',
					td.dropDatetime As 'dropDatetime',td.dropLatitude As 'dropLatitude',td.dropLongitude As 'dropLongitude',td.pickupLocation As 'pickupLocation',
					td.dropLocation As 'dropLocation',td.tripStatus As 'tripStatus',dats.description As 'tripStatusName',td.tripType As 'tripType',td.paymentMode As 'paymentMode',
					p.firstName As 'passengerFirstName',p.lastName As 'passengerLastName',p.profileImage As 'passengerProfileImage',
					d.firstName As 'driverFirstName',d.lastName As 'driverLastName',d.profileImage As 'driverProfileImage',
					TIMEDIFF(td.dropDatetime,td.actualPickupDatetime) As 'tripDuration',td.customerReferenceCode As 'customerReferenceCode',
					ttd.travelledPeriod As 'travelledPeriod',ttd.travelledDistance As 'travelledDistance',ttd.convenienceCharge As 'convenienceCharge',ttd.travelCharge As 'travelCharge',ttd.driverEarning As 'driverEarning',
					ttd.parkingCharge As 'parkingCharge',ttd.tollCharge As 'tollCharge',ttd.taxCharge As 'taxCharge',ttd.totalTripCharge As 'totalTripCharge',ttd.adminAmount As 'adminAmount',ttd.invoiceNo As 'invoiceNo',
					ttd.promoDiscountAmount As 'promoDiscountAmount',ttd.walletPaymentAmount As 'walletPaymentAmount',td.taxiCategoryType As 'taxiCategoryType',datct.description As 'taxiCategoryTypeName'
					from tripdetails as td
					left join passenger as p on td.passengerId=p.id
					left join driver as d on td.driverId=d.id
					left join triptransactiondetails as ttd on ttd.tripId=td.id
					left join dataattributes as datct on td.taxiCategoryType =datct.id
					left join dataattributes as dats on td.tripStatus =dats.id
					where td.driverId=" . $driver_id . " AND td.tripStatus='" . Trip_Status_Enum::TRIP_COMPLETED . "' " . $date_range . " order by td.id DESC " . $limit;
			$result = $this->db->query ( $query );
			return $this->fetchAll ( $result );
		} else {
			return FALSE;
		}
	}
	public function getDriverAverageRating($driver_id) {
		$query = "SELECT AVG(td.passengerRating) As 'driverRating'
				  FROM tripdetails AS td
				  where td.driverId=" . $driver_id . " AND td.tripStatus='" . Trip_Status_Enum::TRIP_COMPLETED . "'";
		$result = $this->db->query ( $query );
		return $this->fetchAll ( $result );
	
	
	}
	public function getDriverListForTrip($trip_id,$distance=NULL) {
		sleep(2);
		if ($distance===NULL)
		{
			$distance=1;
		}	
		$query="SELECT GROUP_CONCAT(trd.selectedDriverId)  As 'selectedDriverId' FROM taxirequestdetails As trd where trd.taxiRequestStatus IN (".Taxi_Request_Status_Enum::AVAILABLE_TRIP.",".Taxi_Request_Status_Enum::DRIVER_ACCEPTED.") GROUP BY trd.selectedDriverId";
		$result = $this->db->query($query);
		$assigned_driver= $this->fetchAll($result);
		$assigned_driver_where='';
	
		$query="SELECT trd.rejectedDriverList  As 'rejectedDrivers' FROM taxirequestdetails As trd where trd.tripid=".$trip_id." ORDER BY trd.tripid DESC LIMIT 0,1";
		$result = $this->db->query($query);
		$rejected_driver= $this->fetchAll($result);
		$rejected_driver_where='';
	
		if ($rejected_driver && $rejected_driver[0]->rejectedDrivers)
		{
			$rejected_driver_where=" AND ddd.driverId NOT IN (".$rejected_driver[0]->rejectedDrivers.")";
		}
	
		if ($assigned_driver && $assigned_driver[0]->selectedDriverId)
		{
			$assigned_driver_where=" AND ddd.driverId NOT IN (".$assigned_driver[0]->selectedDriverId.")";
		}
		
		$query = "SELECT td.id As 'tripId',ddd.driverId As 'driverId',CONCAT(d.firstName,' ',d.lastName) As 'driverName',d.mobile As 'driverMobile',
				Round(6371 * acos( cos( radians(td.pickupLatitude) ) * cos( radians(ddd.driverLatitude) ) * cos( radians(ddd.driverLongitude) - radians(td.pickupLongitude) ) + sin( radians(td.pickupLatitude) ) * sin( radians(ddd.driverLatitude))),2) AS 'distance',
				ddd.avgRating As 'avgRating',ddd.walletBalance As 'walletBalance', ddd.avgTripDistance As 'avgTripDistance',ddd.totalCompletedTrip As 'totalCompletedTrip',ddd.weekCompletedTrip As 'weekCompletedTrip',ddd.totalRejectedTrip As 'totalRejectedTrip',
				ddd.weekRejectedTrip As 'weekRejectedTrip',txd.id As 'taxiId',txd.name As 'taxiName',txd.model As 'taxiModel',txd.manufacturer As 'taxiManufacturer',ddd.driverLatitude As driverLatitude,ddd.driverLongitude As driverLongitude,td.pickupLatitude As pickupLatitude,td.pickupLongitude as pickupLongitude,
				txd.colour As 'taxiColour',txd.registrationNo As 'taxiRegistrationNo',txd.taxiCategoryType As 'taxiCategoryType',datct.description As 'taxiCategoryName',
				(Select Count(td1.id) from tripdetails as td1 where td1.driverId=ddd.driverId AND td1.tripStatus=".Trip_Status_Enum::TRIP_COMPLETED." AND td1.updatedDatetime BETWEEN ADDTIME('".getCurrentDateTime()."' ,-20000) AND '".getCurrentDateTime()."') As 'hourCompletedTrip'
 				FROM tripdetails As td
				left join triptransactiondetails As ttd ON ttd.tripId=td.id
				left join entityconfigdetails AS ecd ON ecd.entityId=td.entityId
				left join driverdispatchdetails AS ddd ON ddd.status=".Status_Type_Enum::ACTIVE." ".$rejected_driver_where." ".$assigned_driver_where."
				left join driver As d ON d.id=ddd.driverId
				left join drivertaximapping as dtm ON dtm.driverId=d.id
				left join taxidetails as txd ON txd.id=dtm.taxiId AND td.taxiCategoryType=txd.taxiCategoryType
				left join dataattributes as datct on td.taxiCategoryType =datct.id		
				Where td.id=".$trip_id." AND ddd.status=".Status_Type_Enum::ACTIVE." AND ddd.availabilityStatus=" . Taxi_Available_Status_Enum::FREE . "
				AND ddd.walletBalance>=ecd.driverMinWallet AND ddd.driverLatitude >".Status_Type_Enum::INACTIVE." AND ddd.driverLongitude>".Status_Type_Enum::INACTIVE." 
				AND d.loginStatus=".Status_Type_Enum::ACTIVE." AND d.isDeleted=".Status_Type_Enum::INACTIVE." AND ddd.isFifo=".Status_Type_Enum::INACTIVE." AND ddd.updatedDatetime BETWEEN ADDTIME('".getCurrentDateTime()."' ,-25) AND '".getCurrentDateTime()."'
				GROUP BY ddd.driverId
				HAVING distance < ".$distance."
				ORDER BY distance,ddd.walletBalance desc,ddd.avgRating desc,ddd.weekCompletedTrip desc,ddd.weekRejectedTrip,ddd.totalCompletedTrip desc,ddd.totalRejectedTrip,ddd.avgTripDistance desc";
		$result = $this->db->query($query);
		return $this->fetchAll($result);
	}
	public function getDriverListForTrip_old($trip_id,$distance=NULL) {
		if ($distance===NULL)
		{
			$distance=1;
		}
		$query="SELECT GROUP_CONCAT(trd.selectedDriverId)  As 'selectedDriverId' FROM taxirequestdetails As trd where trd.taxiRequestStatus IN (".Taxi_Request_Status_Enum::AVAILABLE_TRIP.",".Taxi_Request_Status_Enum::DRIVER_ACCEPTED.") GROUP BY trd.selectedDriverId";
		$result = $this->db->query($query);
		$assigned_driver= $this->fetchAll($result);
		$assigned_driver_where='';
	
		$query="SELECT trd.rejectedDriverList  As 'rejectedDrivers' FROM taxirequestdetails As trd where trd.tripid=".$trip_id." ORDER BY trd.tripid DESC LIMIT 0,1";
		$result = $this->db->query($query);
		$rejected_driver= $this->fetchAll($result);
		$rejected_driver_where='';
	
		if ($rejected_driver && $rejected_driver[0]->rejectedDrivers)
		{
			$rejected_driver_where=" AND ddd.driverId NOT IN (".$rejected_driver[0]->rejectedDrivers.")";
		}
	
		if ($assigned_driver && $assigned_driver[0]->selectedDriverId)
		{
			$assigned_driver_where=" AND ddd.driverId NOT IN (".$assigned_driver[0]->selectedDriverId.")";
		}
	
		$query = "SELECT td.id As 'tripId',ddd.driverId As 'driverId',CONCAT(d.firstName,' ',d.lastName) As 'driverName',d.mobile As 'driverMobile',
				Round(6371 * acos( cos( radians(td.pickupLatitude) ) * cos( radians(ddd.driverLatitude) ) * cos( radians(ddd.driverLongitude) - radians(td.pickupLongitude) ) + sin( radians(td.pickupLatitude) ) * sin( radians(ddd.driverLatitude))),2) AS 'distance',
				ddd.avgRating As 'avgRating',ddd.walletBalance As 'walletBalance', ddd.avgTripDistance As 'avgTripDistance',ddd.totalCompletedTrip As 'totalCompletedTrip',ddd.weekCompletedTrip As 'weekCompletedTrip',ddd.totalRejectedTrip As 'totalRejectedTrip',
				ddd.weekRejectedTrip As 'weekRejectedTrip',txd.id As 'taxiId',txd.name As 'taxiName',txd.model As 'taxiModel',txd.manufacturer As 'taxiManufacturer',ddd.driverLatitude As driverLatitude,ddd.driverLongitude As driverLongitude,td.pickupLatitude As pickupLatitude,td.pickupLongitude as pickupLongitude,
				txd.colour As 'taxiColour',txd.registrationNo As 'taxiRegistrationNo',txd.taxiCategoryType As 'taxiCategoryType',datct.description As 'taxiCategoryName',
				(Select Count(td1.id) from tripdetails as td1 where td1.driverId=ddd.driverId AND td1.tripStatus=".Trip_Status_Enum::TRIP_COMPLETED." AND td1.updatedDatetime BETWEEN ADDTIME('".getCurrentDateTime()."' ,-20000) AND '".getCurrentDateTime()."') As 'hourCompletedTrip'
 				FROM tripdetails As td
				left join triptransactiondetails As ttd ON ttd.tripId=td.id
				left join entityconfigdetails AS ecd ON ecd.entityId=td.entityId
				left join driverdispatchdetails AS ddd ON ddd.status=".Status_Type_Enum::ACTIVE." ".$rejected_driver_where." ".$assigned_driver_where."
				left join driver As d ON d.id=ddd.driverId
				left join drivertaximapping as dtm ON dtm.driverId=d.id
				left join taxidetails as txd ON txd.id=dtm.taxiId AND td.taxiCategoryType=txd.taxiCategoryType
				left join dataattributes as datct on td.taxiCategoryType =datct.id
				Where td.id=".$trip_id." AND ddd.status=".Status_Type_Enum::ACTIVE." AND ddd.availabilityStatus=" . Taxi_Available_Status_Enum::FREE . "
				AND ddd.walletBalance>=ecd.driverMinWallet AND ddd.driverLatitude >".Status_Type_Enum::INACTIVE." AND ddd.driverLongitude>".Status_Type_Enum::INACTIVE."
				AND d.loginStatus=".Status_Type_Enum::ACTIVE." AND ddd.isFifo=".Status_Type_Enum::INACTIVE." AND d.isDeleted=".Status_Type_Enum::INACTIVE." AND ddd.updatedDatetime BETWEEN ADDTIME('".getCurrentDateTime()."' ,-25) AND '".getCurrentDateTime()."'
				GROUP BY ddd.driverId
				HAVING distance < ".$distance."
				ORDER BY hourCompletedTrip,distance,ddd.walletBalance desc,ddd.avgRating desc,ddd.weekCompletedTrip desc,ddd.weekRejectedTrip,ddd.totalCompletedTrip desc,ddd.totalRejectedTrip,ddd.avgTripDistance desc";
		$result = $this->db->query($query);
		return $this->fetchAll($result);
	}
	
	public function getDriverListForFifoTrip($trip_id,$distance=NULL) {
		sleep(2);
		if ($distance===NULL)
		{
			$distance=1;
		}
		$query="SELECT GROUP_CONCAT(trd.selectedDriverId)  As 'selectedDriverId' FROM taxirequestdetails As trd where trd.taxiRequestStatus IN (".Taxi_Request_Status_Enum::AVAILABLE_TRIP.",".Taxi_Request_Status_Enum::DRIVER_ACCEPTED.") GROUP BY trd.selectedDriverId";
		$result = $this->db->query($query);
		$assigned_driver= $this->fetchAll($result);
		$assigned_driver_where='';
	
		$query="SELECT trd.rejectedDriverList  As 'rejectedDrivers' FROM taxirequestdetails As trd where trd.tripid=".$trip_id." ORDER BY trd.tripid DESC LIMIT 0,1";
		$result = $this->db->query($query);
		$rejected_driver= $this->fetchAll($result);
		$rejected_driver_where='';
	
		if ($rejected_driver && $rejected_driver[0]->rejectedDrivers)
		{
			$rejected_driver_where=" AND ddd.driverId NOT IN (".$rejected_driver[0]->rejectedDrivers.")";
		}
	
		if ($assigned_driver && $assigned_driver[0]->selectedDriverId)
		{
			$assigned_driver_where=" AND ddd.driverId NOT IN (".$assigned_driver[0]->selectedDriverId.")";
		} 
	
		$query = "SELECT td.id As 'tripId',ddd.driverId As 'driverId',Round(6371 * acos( cos( radians(td.pickupLatitude) ) * cos( radians(ddd.driverLatitude) ) * cos( radians(ddd.driverLongitude) - radians(td.pickupLongitude) ) + sin( radians(td.pickupLatitude) ) * sin( radians(ddd.driverLatitude))),2) AS 'distance',
				ddd.avgRating As 'avgRating',ddd.walletBalance As 'walletBalance', ddd.avgTripDistance As 'avgTripDistance',ddd.totalCompletedTrip As 'totalCompletedTrip',ddd.weekCompletedTrip As 'weekCompletedTrip',ddd.totalRejectedTrip As 'totalRejectedTrip',
				ddd.weekRejectedTrip As 'weekRejectedTrip',txd.id As 'taxiId',txd.name As 'taxiName',txd.model As 'taxiModel',txd.manufacturer As 'taxiManufacturer',ddd.driverLatitude As driverLatitude,ddd.driverLongitude As driverLongitude,td.pickupLatitude As pickupLatitude,td.pickupLongitude as pickupLongitude,
				txd.colour As 'taxiColour',txd.registrationNo As 'taxiRegistrationNo',txd.taxiCategoryType As 'taxiCategoryType',datct.description As 'taxiCategoryName'
 				FROM tripdetails As td
				left join entityconfigdetails AS ecd ON ecd.entityId=td.entityId
				left join driverdispatchdetails AS ddd ON ddd.status=".Status_Type_Enum::ACTIVE." AND ddd.zoneIdFifo=td.zoneId ".$rejected_driver_where." ".$assigned_driver_where."
				left join driver As d ON d.id=ddd.driverId
				left join drivertaximapping as dtm ON dtm.driverId=d.id
				left join taxidetails as txd ON txd.id=dtm.taxiId AND td.taxiCategoryType=txd.taxiCategoryType
				left join dataattributes as datct on td.taxiCategoryType =datct.id
				Where td.id=".$trip_id." AND ddd.status=".Status_Type_Enum::ACTIVE." AND ddd.availabilityStatus=" . Taxi_Available_Status_Enum::FREE . "
				AND ddd.walletBalance>=ecd.driverMinWallet AND ddd.driverLatitude >".Status_Type_Enum::INACTIVE." AND ddd.driverLongitude>".Status_Type_Enum::INACTIVE."
				AND ddd.isEligible=".Status_Type_Enum::ACTIVE." AND ddd.isFifo=".Status_Type_Enum::ACTIVE."
				AND ddd.updatedDatetime BETWEEN ADDTIME('".getCurrentDateTime()."' ,-25) AND '".getCurrentDateTime()."'
				GROUP BY ddd.driverId
				HAVING distance < ".$distance."
				ORDER BY ddd.loginFifo ASC,distance ASC";
		
		$result = $this->db->query($query);
		return $this->fetchAll($result);
	}	
	
	public function checkEasyPoints($digital_code=NULL,$transaction_amt=NULL,$message_id=NULL)
	{
		$version='1.1';
		//add mico time to cuurent datatime
		$microtime = floatval(substr((string)microtime(), 1, 8));
		$rounded = round($microtime, 3);
		$date_time= getCurrentDateTime(). substr((string)$rounded, 1, strlen($rounded));
		$time_stamp=str_replace('.',':',$date_time);
		
		$message_id=getCurrentUnixDateTime().$digital_code;
		$partner_id='P00014';
		//$digital_code='527544700000001651';
		$secret_key='OATBHND9W2V7MI5HH480DD5W51L6WTKV';
		
		$transaction_amt = $transaction_amt*100;
		$transaction_amt = str_pad((string)$transaction_amt, 12, "0", STR_PAD_LEFT);
		
		$signature_string=$version.$time_stamp.$message_id.$partner_id.$digital_code.$transaction_amt;
		$signature_data = hash_hmac('sha1', $signature_string, $secret_key, false);
		$signature_data = strtoupper($signature_data);
		$hash_key=urlencode($signature_data);
		
		$input_xml = '<ConfirmReq>
					  <Version>'.$version.'</Version>
                      <TimeStamp>'.$time_stamp.'</TimeStamp>
					  <MessageID>'.$message_id.'</MessageID>
					  <PartnerID>'.$partner_id.'</PartnerID>
					  <DigitalCode>'.$digital_code.'</DigitalCode>
					  <TranAmount>'.$transaction_amt.'</TranAmount>
					  <HashValue>'.$hash_key.'</HashValue>
					  </ConfirmReq>';
		
		//$url = "https://demo3.2c2p.com/eppartnerapi/partner.aspx";//devlopment url
		$url="https://www.easypoints.asia/mm/partner/partner.aspx";//production url
		
		
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, $url );
		curl_setopt( $ch, CURLOPT_POST, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $input_xml );
		$response  = curl_exec($ch);
		curl_close($ch);
		
		//$response="<ConfirmRes><Version>1.1</Version><TimeStamp>2015-01-21 08:34:47:131</TimeStamp><MessageID>314lfncdon1a1txicuykw4uo </MessageID><PartnerID>000001</PartnerID><AgentID>000001</AgentID><DigitalCode>234223344444433335</DigitalCode><CodeDesc>Game</CodeDesc><ResCode>'00'</ResCode><ResDesc>Approved</ResDesc><TranAmount>000000100000</TranAmount><TranRef>000000100000</TranRef><HashValue>E446E7EC601D8A8A4FF0CFEC4AA4B675A083A81F</HashValue></ConfirmRes>";
		//convert the XML result into array
		return  $response_data = json_decode(json_encode(simplexml_load_string($response)), true);
		
		
	}
	
public function getWeekCompletedTripCount($driver_id)
	{
		$query = "SELECT Count(td.id) As 'weekCompletedTripCount'
				from tripdetails As td
				where td.driverId=".$driver_id." AND td.tripStatus=" . Trip_Status_Enum::TRIP_COMPLETED." AND 
				td.updatedDatetime BETWEEN DATE_SUB('".getCurrentDate()."', INTERVAL 7 DAY) AND '".getCurrentDate()."'
				GROUP BY DATE(updatedDatetime)";
		
		$result = $this->db->query ( $query );
		return $this->fetchAll ( $result );
		
	}
	
	public function getWeekRejectedTripCount($driver_id)
	{
		$query = "SELECT Count(drtd.id) As 'weekRejectedTripCount'
				from taxirejectedtripdetails as drtd
				where drtd.driverId=".$driver_id." AND drtd.updatedDatetime BETWEEN DATE_SUB('".getCurrentDate()."', INTERVAL 7 DAY) AND '".getCurrentDate()."'
				GROUP BY DATE(drtd.updatedDatetime)";
	
		$result = $this->db->query ( $query );
		return $this->fetchAll ( $result );
	
	}
	public function getHourCompletedTripCount($driver_id)
	{
		$query = "SELECT Count(td.id) As 'hourCompletedTripCount'
				from tripdetails As td
				where td.driverId=".$driver_id." AND td.tripStatus=" . Trip_Status_Enum::TRIP_COMPLETED." AND
				td.updatedDatetime BETWEEN ADDTIME('".getCurrentDateTime()."' ,'-02:00:00') AND '".getCurrentDateTime()."' )
				GROUP BY DATE(updatedDatetime)";
	
		$result = $this->db->query ( $query );
		return $this->fetchAll ( $result );
	
	}
	
	public function getHourRejectedTripCount($driver_id)
	{
		$query = "SELECT Count(drtd.id) As 'hourRejectedTripCount'
				from taxirejectedtripdetails as drtd
				where drtd.driverId=".$driver_id." AND drtd.updatedDatetime BETWEEN ADDTIME('".getCurrentDateTime()."' ,'-02:00:00') AND '".getCurrentDateTime()."' )
				GROUP BY DATE(drtd.updatedDatetime)";
	
		$result = $this->db->query ( $query );
		return $this->fetchAll ( $result );
	
	}
		
}