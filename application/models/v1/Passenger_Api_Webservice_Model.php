<?php
require_once (APPPATH . 'config/device_status_enum.php');
require_once (APPPATH . 'config/device_type_enum.php');
require_once (APPPATH . 'config/dispatch_type_enum.php');
require_once (APPPATH . 'config/driver_accepted_status_enum.php');
require_once (APPPATH . 'config/dispatch_type_enum.php');
require_once (APPPATH . 'config/driver_shift_status_enum.php');
require_once (APPPATH . 'config/emergency_status_enum.php');
require_once (APPPATH . 'config/entity_type_enum.php');
require_once (APPPATH . 'config/experience_enum.php');
require_once (APPPATH . 'config/gender_type_enum.php');
require_once (APPPATH . 'config/license_type_enum.php');
require_once (APPPATH . 'config/module_name_enum.php');
require_once (APPPATH . 'config/payment_method_enum.php');
require_once (APPPATH . 'config/payment_mode_enum.php');
require_once (APPPATH . 'config/payment_type_enum.php');
require_once (APPPATH . 'config/promocode_type_enum.php');
require_once (APPPATH . 'config/register_type_enum.php');
require_once (APPPATH . 'config/role_type_enum.php');
require_once (APPPATH . 'config/signup_type_enum.php');
require_once (APPPATH . 'config/slab_type_enum.php');
require_once (APPPATH . 'config/status_type_enum.php');
require_once (APPPATH . 'config/taxi_available_status_enum.php');
require_once (APPPATH . 'config/taxi_request_status_enum.php');
require_once (APPPATH . 'config/taxi_type_enum.php');
require_once (APPPATH . 'config/transaction_mode_enum.php');
require_once (APPPATH . 'config/transaction_type_enum.php');
require_once (APPPATH . 'config/transaction_from_enum.php');
require_once (APPPATH . 'config/transmission_type_enum.php');
require_once (APPPATH . 'config/trip_status_enum.php');
require_once (APPPATH . 'config/trip_type_enum.php');
require_once (APPPATH . 'config/user_type_enum.php');

class Passenger_Api_Webservice_Model extends MY_Model {
	
	/**
	 * Default Constructor
	 */
	function __construct($args = NULL) {
		parent::__construct ();
		if (is_object ( $args ))
			$args = get_object_vars ( $args );
		if (is_array ( $args )) {
			foreach ( $args as $key => $value ) {
				$this->{$key} = $value;
			}
		}
	}
	
	/**
	 * To get the trip details based on Company,city & trip type for particular passenger/driver
	 *
	 * @param unknown $company_id
	 * @param string $city_id
	 * @return Ambigous <multitype:, multitype:object >
	 */
	
	public function getRateCardDetails($company_id, $city_id = NULL) {
		$where_city_clause = '';
		if ($city_id) {
			$where_city_clause = " AND rc.cityId=" . $city_id;
		}
	
		$query = "SELECT rc.id As 'rateCardId',rc.name As 'rateCardName',rc.dayBaseCharge As 'dayBaseCharge',
				rc.nightBaseCharge As 'nightBaseCharge',rc.perKmCharge As 'perKmCharge',rc.waitingCharge As 'waitingCharge',
				rc.taxiCategoryType As 'taxiCategoryType',rc.fixedTripCharge As 'fixedTripCharge',
				rc.entityId As 'entityId',datct.description As 'taxiCategoryTypeName'
				from  ratecarddetails As rc
				left join dataattributes as datct on rc.taxiCategoryType =datct.id
				where rc.entityId=" . $company_id . " " . $where_city_clause. "  AND rc.status='" . Status_Type_Enum::ACTIVE . "'";
		$result = $this->db->query ( $query );
		return $this->fetchAll ( $result );
	}
	
	/**
	 * To get pending trip deatils for particular passenger
	 * @param unknown $passenger_id
	 * @param unknown $pagination
	 * @param unknown $start_offset
	 * @param unknown $end_limit
	 * @return Ambigous <multitype:, multitype:object >|boolean
	 */
	public function getPendingTripList($passenger_id, $start_offset, $end_limit=20,$start_date = NULL, $end_date = NULL) {
		if ($passenger_id) {
			$limit = '';
			$date_range = "AND date(td.pickupDatetime) >=date('" . getCurrentDateTime () . "')";
			if ($start_date && $end_date) {
				//$date_range = " AND td.pickupDatetime >='" . date ( 'Y-m-d H:i:s', strtotime ( $start_date ) ) . "' AND td.pickupDatetime <='" . date ( 'Y-m-d H:i:s', strtotime ( $end_date ) ) . "'";
				$date_range = " AND date(td.pickupDatetime) between date ('".$start_date."') AND date ('".$end_date."')";
			}
			if ($start_offset && $end_limit) {
				$limit = ' LIMIT ' . $start_offset . ',' . $end_limit;
			}
			
			$query = "SELECT td.id As 'tripId',td.jobCardId As 'jobCardId',td.driverId As 'driverId',td.passengerId As 'passengerId',td.pickupDatetime As 'pickupDatetime',td.pickupLatitude As 'pickupLatitude',td.pickupLongitude As 'pickupLongitude',
			td.dropDatetime As 'dropDatetime',td.dropLatitude As 'dropLatitude',td.dropLongitude As 'dropLongitude',td.pickupLocation As 'pickupLocation',td.dropLocation As 'dropLocation',td.tripStatus As 'tripStatus',dats.description As 'tripStatusName',
			td.tripType As	'tripType',datt.description As 'tripTypeName',td.paymentMode As 'paymentMode',dapm.description As 'paymentModeName',
			p.firstName As 'passengerFirstName',p.lastName As 'passengerLastName',d.firstName As 'driverFirstName',d.lastName As 'driverLastName',
			td.customerReferenceCode As 'customerReferenceCode',td.taxiCategoryType As 'taxiCategoryType',datct.description As 'taxiCategoryTypeName'
					from tripdetails as td
					left join passenger as p on td.passengerId=p.id
					left join driver as d on td.driverId=d.id
					left join dataattributes as datct on td.taxiCategoryType =datct.id
					left join dataattributes as datt on td.tripType =datt.id
					left join dataattributes as dapm on td.paymentMode =dapm.id
					left join dataattributes as dats on td.tripStatus =dats.id
					where td.passengerId=" . $passenger_id . " ".$date_range." AND td.tripStatus IN ('" . Trip_Status_Enum::BOOKED . "','" . Trip_Status_Enum::DRIVER_ACCEPTED . "','" . Trip_Status_Enum::IN_PROGRESS . "','" . Trip_Status_Enum::DRIVER_ARRIVED . "','" . Trip_Status_Enum::WAITING_FOR_PAYMENT . "') order by td.pickupDatetime DESC " . $limit;
			$result = $this->db->query ( $query );
			return $this->fetchAll ( $result );
		} else {
			return FALSE;
		}
	}
	/**
	 * To get complete trip details for particular passenger
	 * @param unknown $passenger_id
	 * @param unknown $pagination
	 * @param string $start_offset
	 * @param string $end_limit
	 * @return Ambigous <multitype:, multitype:object >|boolean
	 */
	public function getPastTripList($passenger_id, $start_offset, $end_limit = 20,$start_date = NULL, $end_date = NULL) {
		if ($passenger_id) {
		$date_range = "";
			if ($start_date && $end_date) {
				//$date_range = " AND td.pickupDatetime >='" . date ( 'Y-m-d H:i:s', strtotime ( $start_date ) ) . "' AND td.pickupDatetime <='" . date ( 'Y-m-d H:i:s', strtotime ( $end_date ) ) . "'";
				$date_range = " AND date(td.pickupDatetime) between date ('".$start_date."') AND date ('".$end_date."')";
			}
			$limit = '';
			if ($start_offset && $end_limit) {
				$limit = ' LIMIT ' . $start_offset . ',' . $end_limit;
			}
			$query = "SELECT td.id As 'tripId',td.jobCardId As 'jobCardId',td.driverId As 'driverId',td.passengerId As 'passengerId',
					td.actualPickupDatetime As 'actualPickupDatetime',td.pickupLatitude As 'pickupLatitude',td.pickupLongitude As 'pickupLongitude',
					td.dropDatetime As 'dropDatetime',td.dropLatitude As 'dropLatitude',td.dropLongitude As 'dropLongitude',td.pickupLocation As 'pickupLocation',
					td.dropLocation As 'dropLocation',td.tripStatus As 'tripStatus',dats.description As 'tripStatusName',
					td.tripType As	'tripType',datt.description As 'tripTypeName',td.paymentMode As 'paymentMode',dapm.description As 'paymentModeName',
					p.firstName As 'passengerFirstName',p.lastName As 'passengerLastName',p.profileImage As 'passengerProfileImage',
					d.firstName As 'driverFirstName',d.lastName As 'driverLastName',d.profileImage As 'driverProfileImage',
					TIMEDIFF(td.dropDatetime,td.actualPickupDatetime) As 'tripDuration',td.customerReferenceCode As 'customerReferenceCode',
					ttd.travelledPeriod As 'travelledPeriod',ttd.travelledDistance As 'travelledDistance',ttd.convenienceCharge As 'convenienceCharge',ttd.travelCharge As 'travelCharge',ttd.driverEarning As 'driverEarning',
					ttd.parkingCharge As 'parkingCharge',ttd.tollCharge As 'tollCharge',ttd.taxCharge As 'taxCharge',ttd.totalTripCharge As 'totalTripCharge',ttd.adminAmount As 'adminAmount',ttd.invoiceNo As 'invoiceNo',
					ttd.promoDiscountAmount As 'promoDiscountAmount',ttd.walletPaymentAmount As 'walletPaymentAmount',td.taxiCategoryType As 'taxiCategoryType',datct.description As 'taxiCategoryTypeName'
					from tripdetails as td
					left join passenger as p on td.passengerId=p.id
					left join driver as d on td.driverId=d.id
					left join triptransactiondetails as ttd on ttd.tripId=td.id
					left join dataattributes as datct on td.taxiCategoryType =datct.id
					left join dataattributes as datt on td.tripType =datt.id
					left join dataattributes as dapm on td.paymentMode =dapm.id
					left join dataattributes as dats on td.tripStatus =dats.id
					where td.passengerId=" . $passenger_id . " ".$date_range." AND td.tripStatus='" . Trip_Status_Enum::TRIP_COMPLETED . "' order by td.id DESC " . $limit;
			$result = $this->db->query ( $query );
			return $this->fetchAll ( $result );
		} else {
			return FALSE;
		}
	}
	
	/**
	 * To get cancelled trip details for particular passenger
	 * @param unknown $passenger_id
	 * @param unknown $pagination
	 * @param string $start_offset
	 * @param string $end_limit
	 * @return Ambigous <multitype:, multitype:object >|boolean
	 */
	public function getCancelledTripList($passenger_id, $start_offset , $end_limit = 20,$start_date = NULL, $end_date = NULL) {
		if ($passenger_id) {
			$date_range = "";
			if ($start_date && $end_date) {
				//$date_range = " AND td.pickupDatetime >='" . date ( 'Y-m-d H:i:s', strtotime ( $start_date ) ) . "' AND td.pickupDatetime <='" . date ( 'Y-m-d H:i:s', strtotime ( $end_date ) ) . "'";
				$date_range = " AND date(td.pickupDatetime) between date ('".$start_date."') AND date ('".$end_date."')";
			}
			$limit = '';
			if ($start_offset && $end_limit) {
				$limit = ' LIMIT ' . $start_offset . ',' . $end_limit;
			}
			
			$query = "SELECT td.id As 'tripId',td.jobCardId As 'jobCardId',td.driverId As 'driverId',td.passengerId As 'passengerId',td.pickupDatetime As 'pickupDatetime',td.pickupLatitude As 'pickupLatitude',td.pickupLongitude As 'pickupLongitude',
					td.dropDatetime As 'dropDatetime',td.dropLatitude As 'dropLatitude',td.dropLongitude As 'dropLongitude',td.pickupLocation As 'pickupLocation',td.dropLocation As 'dropLocation',td.tripStatus As 'tripStatus',dats.description As 'tripStatusName',
					td.tripType As	'tripType',datt.description As 'tripTypeName',td.paymentMode As 'paymentMode',dapm.description As 'paymentModeName',
					p.firstName As 'passengerFirstName',p.lastName As 'passengerLastName',d.firstName As 'driverFirstName',d.lastName As 'driverLastName',
					td.customerReferenceCode As 'customerReferenceCode',td.taxiCategoryType As 'taxiCategoryType',datct.description As 'taxiCategoryTypeName'
					from taxirejectedtripdetails as drtd
					left join tripdetails as td on td.id=drtd.tripId
					left join passenger as p on drtd.passengerId=p.id
					left join driver as d on drtd.driverId=d.id
					left join dataattributes as datt on td.tripType =datt.id
					left join dataattributes as dapm on td.paymentMode =dapm.id
					left join dataattributes as datct on td.taxiCategoryType =datct.id
					left join dataattributes as dats on td.tripStatus =dats.id
					where td.passengerId=" . $passenger_id . " ".$date_range." AND td.tripStatus='" . Trip_Status_Enum::CANCELLED_BY_PASSENGER . "' order by td.id DESC " . $limit;
			$result = $this->db->query ( $query );
			return $this->fetchAll ( $result );
		} else {
			return FALSE;
		}
	}
	public function getTripHistoryList($passenger_id, $start_offset=0, $end_limit = 20,$start_date = NULL, $end_date = NULL) {
		if ($passenger_id) {
			$date_range = "";
			if ($start_date) {
				$date_range = " AND td.pickupDatetime <'" . date ( 'Y-m-d H:i:s', strtotime ( $start_date ) )."'";
				//$date_range = " AND date(td.pickupDatetime) between date ('".$start_date."') AND date ('".$end_date."')";
			}
			$limit = '';
			if ($start_offset && $end_limit) {
				$limit = ' LIMIT ' . $start_offset . ',' . $end_limit;
			}
			$query = "SELECT td.id As 'tripId',td.jobCardId As 'jobCardId',td.driverId As 'driverId',td.passengerId As 'passengerId',td.pickupDatetime As 'pickupDatetime',
					td.actualPickupDatetime As 'actualPickupDatetime',td.pickupLatitude As 'pickupLatitude',td.pickupLongitude As 'pickupLongitude',
					td.dropDatetime As 'dropDatetime',td.dropLatitude As 'dropLatitude',td.dropLongitude As 'dropLongitude',td.pickupLocation As 'pickupLocation',
					td.dropLocation As 'dropLocation',td.tripStatus As 'tripStatus',dats.description As 'tripStatusName',
					td.tripType As	'tripType',datt.description As 'tripTypeName',td.paymentMode As 'paymentMode',dapm.description As 'paymentModeName',
					p.firstName As 'passengerFirstName',p.lastName As 'passengerLastName',p.profileImage As 'passengerProfileImage',
					d.firstName As 'driverFirstName',d.lastName As 'driverLastName',d.profileImage As 'driverProfileImage',
					TIMEDIFF(td.dropDatetime,td.actualPickupDatetime) As 'tripDuration',td.customerReferenceCode As 'customerReferenceCode',
					ttd.travelledPeriod As 'travelledPeriod',ttd.travelledDistance As 'travelledDistance',ttd.convenienceCharge As 'convenienceCharge',ttd.travelCharge As 'travelCharge',ttd.driverEarning As 'driverEarning',
					ttd.parkingCharge As 'parkingCharge',ttd.tollCharge As 'tollCharge',ttd.taxCharge As 'taxCharge',ttd.totalTripCharge As 'totalTripCharge',ttd.adminAmount As 'adminAmount',ttd.invoiceNo As 'invoiceNo',
					ttd.promoDiscountAmount As 'promoDiscountAmount',ttd.walletPaymentAmount As 'walletPaymentAmount',td.taxiCategoryType As 'taxiCategoryType',datct.description As 'taxiCategoryTypeName'
					from tripdetails as td
					left join passenger as p on td.passengerId=p.id
					left join driver as d on td.driverId=d.id
					left join triptransactiondetails as ttd on ttd.tripId=td.id
					left join dataattributes as datct on td.taxiCategoryType =datct.id
					left join dataattributes as datt on td.tripType =datt.id
					left join dataattributes as dapm on td.paymentMode =dapm.id
					left join dataattributes as dats on td.tripStatus =dats.id
					where td.passengerId=" . $passenger_id . " ".$date_range." AND td.tripStatus!='" . Trip_Status_Enum::DRIVER_NOT_FOUND . "' AND td.tripStatus!='" . Trip_Status_Enum::CANCELLED_BY_DRIVER . "' AND td.tripStatus!='" . Trip_Status_Enum::DRIVER_REJECTED_TRIP . "' order by td.pickupDatetime DESC " . $limit;
			$result = $this->db->query ( $query );
			return $this->fetchAll ( $result );
		} else {
			return FALSE;
		}
	}
	
	
	public function getNearestTaxiList($latitude, $longitude, $entity_id=DEFAULT_COMPANY_ID,$distance=NULL) {
		$query="SELECT GROUP_CONCAT(trd.selectedDriverId)  As 'selectedDriverId' FROM taxirequestdetails As trd where trd.taxiRequestStatus IN (".Taxi_Request_Status_Enum::AVAILABLE_TRIP.",".Taxi_Request_Status_Enum::DRIVER_ACCEPTED.")";
		$result = $this->db->query($query);
		$assigned_driver= $this->fetchAll($result);
		$assigned_driver_where='';
		
		if ($assigned_driver && $assigned_driver[0]->selectedDriverId)
		{
			$assigned_driver_where=" AND ddd.driverId NOT IN (".$assigned_driver[0]->selectedDriverId.")";
		}
		
		$query = "SELECT ddd.driverId As 'driverId',Round(6371 * acos( cos( radians(".$latitude.") ) * cos( radians(ddd.driverLatitude) ) * cos( radians(ddd.driverLongitude) - radians(".$longitude.") ) + sin( radians(".$latitude.") ) * sin( radians(ddd.driverLatitude))),2) AS 'distance',
				ddd.driverLatitude As 'currentLatitude',ddd.driverLongitude As 'currentLongitude',d.currentBearing As 'currentBearing',d.firstName As 'firstName',d.lastName As 'lastName',d.mobile As 'mobile',
				ddd.avgRating As 'avgRating',ddd.walletBalance As 'walletBalance', ddd.avgTripDistance As 'avgTripDistance',ddd.totalCompletedTrip As 'totalCompletedTrip',ddd.weekCompletedTrip As 'weekCompletedTrip',ddd.totalRejectedTrip As 'totalRejectedTrip',
				ddd.weekRejectedTrip As 'weekRejectedTrip',ddd.availabilityStatus As 'availabilityStatus'
 				FROM driverdispatchdetails AS ddd
				left join entityconfigdetails AS ecd ON ecd.entityId=".$entity_id."
				left join driver As d ON d.id=ddd.driverId
				Where ddd.status=".Status_Type_Enum::ACTIVE." ".$assigned_driver_where." AND (ddd.availabilityStatus=" . Taxi_Available_Status_Enum::FREE . " OR (ddd.availabilityStatus=" . Taxi_Available_Status_Enum::BUSY . " AND ddd.tripId=".Status_Type_Enum::INACTIVE."))
				AND ddd.walletBalance>=ecd.driverMinWallet AND ddd.driverLatitude >".Status_Type_Enum::INACTIVE." AND ddd.driverLongitude>".Status_Type_Enum::INACTIVE."
				AND d.loginStatus=".Status_Type_Enum::ACTIVE." AND d.isDeleted=".Status_Type_Enum::INACTIVE." 
				GROUP BY ddd.driverId
				HAVING distance <= ".$distance."
				ORDER BY distance,ddd.walletBalance desc,ddd.avgRating desc,ddd.weekCompletedTrip desc,ddd.weekRejectedTrip,ddd.totalCompletedTrip desc,ddd.totalRejectedTrip,ddd.avgTripDistance desc LIMIT 0,20";
		
		/* $query = "SELECT ddd.driverId As 'driverId',Round(6371 * acos( cos( radians(".$latitude.") ) * cos( radians(ddd.driverLatitude) ) * cos( radians(ddd.driverLongitude) - radians(".$longitude.") ) + sin( radians(".$latitude.") ) * sin( radians(ddd.driverLatitude))),2) AS 'distance',
				ddd.driverLatitude As 'currentLatitude',ddd.driverLongitude As 'currentLongitude',d.currentBearing As 'currentBearing',d.firstName As 'firstName',d.lastName As 'lastName',d.mobile As 'mobile',
				ddd.avgRating As 'avgRating',ddd.walletBalance As 'walletBalance', ddd.avgTripDistance As 'avgTripDistance',ddd.totalCompletedTrip As 'totalCompletedTrip',ddd.weekCompletedTrip As 'weekCompletedTrip',ddd.totalRejectedTrip As 'totalRejectedTrip',
				ddd.weekRejectedTrip As 'weekRejectedTrip',ddd.availabilityStatus As 'availabilityStatus'
 				FROM driverdispatchdetails AS ddd
				left join entityconfigdetails AS ecd ON ecd.entityId=".$entity_id."
				left join driver As d ON d.id=ddd.driverId
				Where ddd.status=".Status_Type_Enum::ACTIVE." ".$assigned_driver_where." AND (ddd.availabilityStatus=" . Taxi_Available_Status_Enum::FREE . " OR ddd.availabilityStatus=" . Taxi_Available_Status_Enum::BUSY . ")
				AND ddd.walletBalance>=ecd.driverMinWallet AND ddd.driverLatitude >".Status_Type_Enum::INACTIVE." AND ddd.driverLongitude>".Status_Type_Enum::INACTIVE."
				AND d.loginStatus=".Status_Type_Enum::ACTIVE." AND d.isDeleted=".Status_Type_Enum::INACTIVE." AND ddd.updatedDatetime BETWEEN ADDTIME('".getCurrentDateTime()."' ,-15) AND '".getCurrentDateTime()."'
				GROUP BY ddd.driverId
				HAVING distance < ".$distance."
				ORDER BY distance,ddd.walletBalance desc,ddd.avgRating desc,ddd.weekCompletedTrip desc,ddd.weekRejectedTrip,ddd.totalCompletedTrip desc,ddd.totalRejectedTrip,ddd.avgTripDistance desc LIMIT 0,20";
		 */
		$result = $this->db->query ( $query );
			return $this->fetchAll ( $result );
	
			/*
			* Here latitude = 37 & longitude = -122. So you just pass your own
		*
		* SELECT d.id As 'driverId',d.currentLatitude As 'currentLatitude',d.currentLongitude As 'currentLongitude', ( 3959 * acos( cos( radians(".$latitude.") ) * cos( radians( d.currentLatitude ) ) *
		* cos( radians( d.currentLongitude) - radians(".$longitude.") ) + sin( radians(".$latitude.") ) *
		* sin( radians( d.currentLatitude) ) ) ) As 'distance_in_km' FROM driver As d left join drivershifthistory as dsh on dsh.driverId=d.id
		* where d.loginStatus='".Status_Type_Enum::ACTIVE."' AND d.status='".Status_Type_Enum::ACTIVE."'
		* AND d.cityId=".$city_id." AND dsh.availabilityStatus='".Driver_Available_Status_Enum::FREE."' AND dsh.shiftStatus='".Driver_Shift_Status_Enum::SHIFTIN."' AND d.isDeleted='".Status_Type_Enum::INACTIVE."'
		* HAVING distance < 25 GROUP BY d.id ORDER BY distance ASC,dsh.id DESC LIMIT 0 , 20;
		*/
		}
		
		public function checkEasyPoints($digital_code=NULL,$transaction_amt=NULL,$message_id=NULL)
		{
			$version='1.1';
			//add mico time to cuurent datatime
			$microtime = floatval(substr((string)microtime(), 1, 8));
			$rounded = round($microtime, 3);
			$date_time= getCurrentDateTime(). substr((string)$rounded, 1, strlen($rounded));
			$time_stamp=str_replace('.',':',$date_time);
		
			$message_id='KKD23099dfdjasRIERI';
			$partner_id='P08021';
			//$digital_code='527544700000001651';
			$secret_key='1NDHNMY5KXMKGWMQVCZ27DVEXEK8OUH5';
		
			$transaction_amt = $transaction_amt*100;
			$transaction_amt = str_pad((string)$transaction_amt, 12, "0", STR_PAD_LEFT);
		
			$signature_string=$version.$time_stamp.$message_id.$partner_id.$digital_code.$transaction_amt;
			$signature_data = hash_hmac('sha1', $signature_string, $secret_key, false);
			$signature_data = strtoupper($signature_data);
			$hash_key=urlencode($signature_data);
		
			$input_xml = '<ConfirmReq>
					  <Version>'.$version.'</Version>
                      <TimeStamp>'.$time_stamp.'</TimeStamp>
					  <MessageID>'.$message_id.'</MessageID>
					  <PartnerID>'.$partner_id.'</PartnerID>
					  <DigitalCode>'.$digital_code.'</DigitalCode>
					  <TranAmount>'.$transaction_amt.'</TranAmount>
					  <HashValue>'.$hash_key.'</HashValue>
					  </ConfirmReq>';
			$xml = "<PaymentRequest>
			<version>".$version."</version>
			<merchantID>".$message_id."</merchantID>
			<uniqueTransactionCode>".$uniqueTransactionCode."</uniqueTransactionCode>
			<desc>".$desc."</desc>
			<amt>".$transaction_amt."</amt>
			<currencyCode>".$currencyCode."</currencyCode>
			<panCountry>".$country."</panCountry>
			<cardholderName>".$cardholderName."</cardholderName>
			<secureHash>".$hash."</secureHash>
			<encCardData>".$encryptedCardInfo."</encCardData>
			</PaymentRequest>";
		
			$url = "https://demo3.2c2p.com/eppartnerapi/partner.aspx";
		
			$ch = curl_init();
			curl_setopt( $ch, CURLOPT_URL, $url );
			curl_setopt( $ch, CURLOPT_POST, true );
			curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch, CURLOPT_POSTFIELDS, $input_xml );
			$response  = curl_exec($ch);
			curl_close($ch);
		
			//return  $response;
			//convert the XML result into array
			return  $response_data = json_decode(json_encode(simplexml_load_string($response)), true);
		
		
		}
		
		
}