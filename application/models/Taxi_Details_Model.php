<?php
class Taxi_Details_Model extends MY_Model {
	protected $_table = 'taxidetails'; // model table_name
	public $_keyName = 'id';
	public $_valueName = 'registrationNo';
	protected $_column_order = array (
			'registrationNo',
			'name',
			'model',
			'manufacturer',
			'colour',
			'insuranceNo',
			'insuranceExpiryDate',
			'nextFcdate',
			'taxiCategoryType',
			'minSpeed',
			'maxSpeed',
			'transmissionType',
			'serviceStartDate',
			'zoneName',
			'isAllocated',
			'status',
			null 
	); // set column field database for datatable orderable
	protected $_column_search = array (
			't.registrationNo',
			't.name',
			't.model',
			't.manufacturer',
			't.colour',
			't.insuranceNo',
			't.insuranceExpiryDate',
			't.nextFcdate',
			'datct.description',
			't.minSpeed',
			't.maxSpeed',
			'datt.description',
			't.serviceStartDate',
			'z.name' 
	); // set column field database for datatable searchable just firstname , lastname , address are searchable
	protected $_order = array (
			't.id' => 'desc' 
	); // default order
	/**
	 * Default Constructor
	 */
	function __construct($args = NULL) {
		parent::__construct ();
		if (is_object ( $args ))
			$args = get_object_vars ( $args );
		if (is_array ( $args )) {
			foreach ( $args as $key => $value ) {
				$this->{$key} = $value;
			}
		}
	}
	/**
	 * helper method to load the key value into dropdown boxes
	 * 
	 * @return type
	 */
	public function getTaxiListQuery() {
		$query = "SELECT t.id As 'taxiId',t.name As 'name',t.model As 'model',t.manufacturer As 'manufacturer',t.colour As 'colour',
				t.registrationNo As 'registrationNo',t.seatCapacity as 'seatCapacity',
				t.insuranceNo as 'insuranceNo',t.insuranceExpiryDate As 'insuranceExpiryDate',t.serviceStartDate As 'serviceStartDate',
				t.isAllocated As 'isAllocated',t.status As 'status',t.nextFcdate As 'nextFcdate',t.minSpeed As 'minSpeed',t.maxSpeed As 'maxSpeed',
				
				datt.description As 'transmissionType',datct.description As 'taxiCategoryType',z.name As 'zoneName'
				from  taxidetails As t
				left join dataattributes as datct on t.taxiCategoryType =datct.id 
				left join dataattributes as datt on t.transmissionType=datt.id
				left join zonedetails as z on z.id=t.zoneId";
		return $query;
	}
	public function getKeyName() {
		return $this->_keyName;
	}
	public function getValueName() {
		return $this->_valueName;
	}
}