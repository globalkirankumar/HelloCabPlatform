<?php
class Driver_Model extends MY_Model {
	protected $_table = 'driver'; // model table_name
	public $_keyName = 'id';
	public $_valueName = 'mobile';
	
	// set column field database for datatable orderable
	protected $_column_order = array (
			null,
			'profileImage',
			'driverCode',
			'firstName',
			'lastName',
			'mobile',
			'driverWallet',
			'driverCredit',
			'taxiRegistrationNo',
			'deviceMobile',
			'driverAvgRating',
			'experience',
			'languageKnown',
			'qualification',
			'email',
			'licenseNumber',
			'licenseExpiryDate',
			'licenseType',
			'address',
			'dob',
			'gender',
			'bankName',
			'bankBranch',
			'bankAccountNo',
			'groupType',
			'behaviourType',
			'zoneName',
			'lastLoggedIn',
			'loginStatus',
			'isVerified',
			'appVersion',
			'driverJoinedDate',
			'driverResignededDate',
			'status',
			 null,
			null
	);
	// set column field database for datatable searchable
	protected $_column_search = array (
			'd.driverCode',
			'd.firstName',
			'd.lastName',
			'd.mobile',
			'd.email',
			'd.licenseNo',
			'd.appVersion',
			'd.createdDatetime',
			'd.updatedDatetime',
			'td.registrationNo',
			'tdd.mobile',
			'dalt.description',
			'ddd.avgRating',
			'd.driverCredit',
			'dae.description',
			'd.driverWallet',
			'd.licenseExpiryDate',
			'dp.bankName',
			'dp.bankAccountNo',
			'dp.bankBranch',
			'd.languageKnown',
			'dp.address',
			'dp.qualification',
			'dp.dob',
			'd.lastLoggedIn',
			'z.name',
			'dp.gender',
			'dp.behaviourType',
			'dp.groupType' 
	);
	// default order
	protected $_order = array (
			'd.id' => 'desc' 
	);
	/**
	 * Default Constructor
	 */
	function __construct($args = NULL) {
		parent::__construct ();
		if (is_object ( $args ))
			$args = get_object_vars ( $args );
		if (is_array ( $args )) {
			foreach ( $args as $key => $value ) {
				$this->{$key} = $value;
			}
		}
	}
	/**
	 * helper method to load the key value into dropdown boxes
	 * 
	 * @return type
	 */
	public function getKeyName() {
		return $this->_keyName;
	}
	public function getValueName() {
		return $this->_valueName;
	}
	public function getDriverListQuery($entity_id = NULL) {
		$query = "SELECT d.id As 'driverId',d.driverCode As 'driverCode',d.firstName As 'firstName',d.lastName As 'lastName',d.mobile As 'mobile',d.appVersion As 'appVersion',d.createdDatetime As 'driverJoinedDate',
				d.email As 'email',d.profileImage As 'profileImage',d.lastLoggedIn As 'lastLoggedIn',d.licenseNo As 'licenseNumber',d.licenseExpiryDate As 'licenseExpiryDate',d.updatedDatetime As 'driverResignededDate',
				d.status As 'status',dae.description As 'experience',d.languageKnown As 'languageKnown',d.driverWallet As 'driverWallet',d.loginStatus As 'loginStatus',d.isVerified As 'isVerified',
				dalt.description As 'licenseType',dp.groupType As 'groupType',dp.behaviourType As 'behaviourType',d.driverCredit As 'driverCredit',ddd.avgRating As 'driverAvgRating',
				dp.gender As 'gender',dp.dob As 'dob',dp.address As 'address',dp.qualification As 'qualification',dp.bankName As 'bankName',dp.bankBranch As 'bankBranch',
				dp.bankAccountNo As 'bankAccountNo',z.name As 'zoneName',(CASE WHEN COUNT(dtm.id) Then 1 else 0 end) As 'driverAllocated',
				dtm.taxiId As 'taxiId',dtm.taxiDeviceId As 'taxiDeviceId',tdd.deviceCode As 'taxiDeviceCode',tdd.mobile As 'deviceMobile',td.registrationNo As 'taxiRegistrationNo'
				from  driver As d
				left join driverpersonaldetails As dp on dp.driverId=d.id
				left join zonedetails as z on z.id =d.zoneId 
				left join dataattributes as dalt on d.licenseType =dalt.id
				left join dataattributes as dae on d.experience =dae.id
				left join drivertaximapping as dtm on dtm.driverId =d.id
				left join taxidevicedetails as tdd on dtm.taxiDeviceId =tdd.id
				left join taxidetails as td on dtm.taxiId =td.id
				left join driverdispatchdetails as ddd on ddd.driverId =d.id
				where d.isDeleted=" . Status_Type_Enum::INACTIVE . " AND d.entityId='$entity_id' GROUP BY d.id";
		
		return $query;
	}
	
	public function getDriverDetails($keyword = '') {
		if ($keyword != '') {
			$filter_qry = " WHERE (mobile like '" . $keyword . "%' OR )";
			
			$query = "SELECT CONCAT(d.firstName ,' ',d.lastName) As 'driverName',d.id as 'driverId',d.driverCode As 'driverCode',d.mobile As 'driverMobile',d.email As 'driverEmail'
					 FROM driver as d WHERE (d.firstName like '".$keyword."%' OR d.lastName like '".$keyword."%' OR d.mobile like '".$keyword."%' OR d.driverCode like '".$keyword."%' OR d.email like '".$keyword."%')
					 AND d.status=" . Status_Type_Enum::ACTIVE . " AND d.isDeleted=" . Status_Type_Enum::INACTIVE . " ORDER BY firstName LIMIT 0,10";
			$result = $this->db->query($query);
			return $this->fetchAll($result);
		}
	}
	
}