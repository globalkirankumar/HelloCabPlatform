<?php
class Module_Access_Permission_Model extends MY_Model {

	protected $_table = 'moduleaccesspermission';//model table_name
	public $_keyName = 'id';
	public $_valueName = 'moduleId';
	
	
	/**
	 *  Default Constructor
	 */
	function __construct($args=NULL)
	{
		parent::__construct();
		if( is_object($args))   $args = get_object_vars($args);
		if( is_array($args)){
			foreach( $args AS $key => $value ){
				$this->{$key} = $value;
			}
		}

	}
	
	
	public function getModuleAccessList($role_id)
	{
		$query = "SELECT map.id As 'moduleAccessId',map.moduleId As 'moduleId',m.moduleName As 'moduleName',m.description As 'moduleDescription',
				map.roleType As 'roleType',map.isAccess As 'isAccess',map.isRead As 'isRead',map.isWrite As 'isWrite',map.isCreate As 'isCreate',
				map.isDelete As 'isDelete',map.isFullAccess As 'isFullAccess',dart.description As 'roleTypeName'
				from  moduleaccesspermission As map
				left join moduledetails As m  on m.id=map.moduleId
				left join dataattributes as dart on  map.roleType =dart.id
				where map.roleType=".$role_id." AND m.status=".Status_Type_Enum::ACTIVE." order by m.id";
		$result = $this->db->query($query);
		return $this->fetchAll($result);
	}
	/**
	 *  helper method to load the key value into dropdown boxes
	 * @return type
	 */
	public function getKeyName(){
		return $this->_keyName;
	}
	
	public function getValueName(){
		return $this->_valueName;
	}
}