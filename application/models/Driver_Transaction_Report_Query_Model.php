<?php
require_once (APPPATH . 'config/device_status_enum.php');
require_once (APPPATH . 'config/device_type_enum.php');
require_once (APPPATH . 'config/dispatch_type_enum.php');
require_once (APPPATH . 'config/driver_accepted_status_enum.php');
require_once (APPPATH . 'config/driver_shift_status_enum.php');
require_once (APPPATH . 'config/emergency_status_enum.php');
require_once (APPPATH . 'config/entity_type_enum.php');
require_once (APPPATH . 'config/experience_enum.php');
require_once (APPPATH . 'config/gender_type_enum.php');
require_once (APPPATH . 'config/license_type_enum.php');
require_once (APPPATH . 'config/module_name_enum.php');
require_once (APPPATH . 'config/payment_method_enum.php');
require_once (APPPATH . 'config/payment_mode_enum.php');
require_once (APPPATH . 'config/payment_type_enum.php');
require_once (APPPATH . 'config/register_type_enum.php');
require_once (APPPATH . 'config/role_type_enum.php');
require_once (APPPATH . 'config/signup_type_enum.php');
require_once (APPPATH . 'config/slab_type_enum.php');
require_once (APPPATH . 'config/status_type_enum.php');
require_once (APPPATH . 'config/taxi_available_status_enum.php');
require_once (APPPATH . 'config/taxi_request_status_enum.php');
require_once (APPPATH . 'config/taxi_type_enum.php');
require_once (APPPATH . 'config/transaction_mode_enum.php');
require_once (APPPATH . 'config/transaction_type_enum.php');
require_once (APPPATH . 'config/transaction_from_enum.php');
require_once (APPPATH . 'config/transmission_type_enum.php');
require_once (APPPATH . 'config/trip_status_enum.php');
require_once (APPPATH . 'config/trip_type_enum.php');
require_once (APPPATH . 'config/user_type_enum.php');

class Driver_Transaction_Report_Query_Model extends MY_Model {
	
	// set column field database for datatable orderable
	protected $_column_order = array (
			null,
			'driverAccountNo',
			'driverCode',
			'driverName',
			'driverMobile',
			'taxiRegistrationNo',
			'jobCardId',
			'transactionAmount',
			'previousAmount',
			'currentAmount',
			'transactionStatus',
			'transactionFrom',
			'transactionTypeName',
			'promoCode',
			'transactionMode',
			'createdDatetime',
			'transactionId',
			'comments',
			'zoneName' 
	);
	
	// set column field database for datatable searchable
	protected $_column_search = array (
			'dpd.bankAccountNo',
			'd.driverCode',
			'd.firstName',
			'd.lastName',
			'd.mobile',
			'txd.registrationNo',
			'td.jobCardId',
			'dt.tripId',
			'dt.transactionAmount',
			'dt.previousAmount',
			'dt.currentAmount',
			'dt.transactionStatus',
			'datm.description',
			'datt.description',
			'td.promoCode',
			'datf.description',
			'dt.createdDatetime',
			'dt.transactionId',
			'dt.comments',
			'z.name'
	);
	// default order
	protected $_order = array (
			'd.id' => 'desc' 
	);
	/**
	 * Default Constructor
	 */
	function __construct($args = NULL) {
		parent::__construct ();
		if (is_object ( $args ))
			$args = get_object_vars ( $args );
		if (is_array ( $args )) {
			foreach ( $args as $key => $value ) {
				$this->{$key} = $value;
			}
		}
	}
	public function getDriverTransactionListQuery($driver_id, $start_date, $end_date, $transaction_from = '', $transaction_mode = '',$transaction_type='', $zone_id = '') {
		$where_caluse = '';
		$transaction_mode_where = '';
		$transaction_from_where = '';
		$transaction_type_where = '';
		$driver_id_where = 'dt.driverId!=' . Status_Type_Enum::INACTIVE;
		$zone_id_where = '';
		
		$date_range_where = " AND dt.createdDatetime >='" . date ( 'Y-m-d H:i:s', strtotime ( $start_date ) ) . "' AND dt.createdDatetime <='" . date ( 'Y-m-d H:i:s', strtotime ( $end_date ) ) . "'";
		if ($transaction_mode != '') {
			$transaction_mode_where = ' AND dt.transactionMode=' . $transaction_mode;
		}
		if ($transaction_from != '') {
			$transaction_from_where = 'AND dt.transactionFrom=' . $transaction_from;
		}
		if ($transaction_type != '') {
			$transaction_type_where = 'AND dt.transactionType=' . $transaction_type;
		}
		if ($driver_id != '') {
			$driver_id_where = 'dt.driverId=' . $driver_id;
		}
		if ($zone_id != '') {
			$zone_id_where = 'AND td.zoneId=' . $zone_id;
		}
		$where_caluse = $driver_id_where . " " . $transaction_mode_where . " " . $transaction_from_where . " " .$transaction_type_where." ". $date_range_where;
		
		$query = "SELECT dt.id AS 'driverTransactionId',dt.driverId As 'driverId',dt.tripId As 'tripId',dt.transactionAmount As 'transactionAmount',dt.previousAmount As 'previousAmount',dt.currentAmount As 'currentAmount',
		dt.transactionStatus,datt.description As 'transactionTypeName',dt.transactionType As 'transactionType',datm.description AS  transactionMode,datf.description AS  transactionFrom,
		dt.transactionId,dt.comments,dt.createdDatetime As 'createdDatetime',CONCAT(d.firstName,' ',d.lastName) As 'driverName',d.mobile As 'driverMobile',
		d.email As 'driverEmail',d.driverCode As 'driverCode',dpd.bankAccountNo As 'driverAccountNo',txd.registrationNo As 'taxiRegistrationNo',
		td.jobCardId As 'jobCardId',td.promoCode As 'promoCode',z.name As 'zoneName'
		FROM drivertransactiondetails dt
		LEFT JOIN driver d on dt.driverId=d.id
		LEFT JOIN driverpersonaldetails dpd on dpd.driverId=d.id
		LEFT JOIN drivertaximapping dtm on dtm.driverId=d.id
		LEFT JOIN taxidetails txd on txd.id=dtm.taxiId
		LEFT JOIN tripdetails td on td.id=dt.tripId
		LEFT JOIN zonedetails z on z.id=td.zoneId
		LEFT JOIN dataattributes datt on  dt.transactionType=datt.id
		LEFT JOIN dataattributes datm on dt.transactionMode=datm.id
		LEFT JOIN dataattributes datf on dt.transactionFrom=datf.id
		WHERE " . $where_caluse." Group By dt.id";
		return $query;
	}
}