<?php
include(APPPATH . 'libraries/PHPMailer.php');

require_once (APPPATH . 'config/device_status_enum.php');
require_once (APPPATH . 'config/device_type_enum.php');
require_once (APPPATH . 'config/dispatch_type_enum.php');
require_once (APPPATH . 'config/driver_accepted_status_enum.php');
require_once (APPPATH . 'config/driver_shift_status_enum.php');
require_once (APPPATH . 'config/emergency_status_enum.php');
require_once (APPPATH . 'config/entity_type_enum.php');
require_once (APPPATH . 'config/experience_enum.php');
require_once (APPPATH . 'config/gender_type_enum.php');
require_once (APPPATH . 'config/license_type_enum.php');
require_once (APPPATH . 'config/module_name_enum.php');
require_once (APPPATH . 'config/payment_method_enum.php');
require_once (APPPATH . 'config/payment_mode_enum.php');
require_once (APPPATH . 'config/payment_type_enum.php');
require_once (APPPATH . 'config/register_type_enum.php');
require_once (APPPATH . 'config/role_type_enum.php');
require_once (APPPATH . 'config/signup_type_enum.php');
require_once (APPPATH . 'config/slab_type_enum.php');
require_once (APPPATH . 'config/status_type_enum.php');
require_once (APPPATH . 'config/taxi_available_status_enum.php');
require_once (APPPATH . 'config/taxi_request_status_enum.php');
require_once (APPPATH . 'config/taxi_type_enum.php');
require_once (APPPATH . 'config/transaction_mode_enum.php');
require_once (APPPATH . 'config/transaction_type_enum.php');
require_once (APPPATH . 'config/transmission_type_enum.php');
require_once (APPPATH . 'config/trip_status_enum.php');
require_once (APPPATH . 'config/trip_type_enum.php');
require_once (APPPATH . 'config/user_type_enum.php');

class Common_Api_Webservice_Model extends MY_Model {
	
	/**
	 * Default Constructor
	 */
	function __construct($args = NULL) {
		parent::__construct ();
		if (is_object ( $args ))
			$args = get_object_vars ( $args );
		if (is_array ( $args )) {
			foreach ( $args as $key => $value ) {
				$this->{$key} = $value;
			}
		}
	}
	
	/**
	 * To send SMS
	 * @param unknown $mobile
	 * @param unknown $message
	 * @return boolean
	 */
	public function sendSMS($mobile, $message) {
		static $port_telenor=7;
		static $port_other=1;
		
		$port=0;
		$msg = urlencode ( $message );
		$user_name='apiuser';
		$password='secapidlc';
		$mobile=$mobile;
		$mtp_other_identifier=array('942','996');
		$telenor_identifier=array('979','978','977','976','975','974');
		if(in_array(substr($mobile, 0, 3),$telenor_identifier))
		{
			$port=$port_telenor;
				
			if ($port_telenor==7)
			{
				$port_telenor=8;
			}
			else 
			{
				$port_telenor=7;
			}
			
		}
		else if(in_array(substr($mobile, 0, 3),$mtp_other_identifier)){
			$port=$port_other;
			
			if ($port_other==6)
			{
				$port_other=1;
			}
			else 
			{
				$port_other++;
			}
			
		}
		else
		{
			$port=$port_other;
		
			if ($port_other==6)
			{
				$port_other=1;
			}
			else
			{
				$port_other++;
			}
				
		}
		$mobile='0'.$mobile;
		$URL = "http://smsapi.hellocabsmyanmar.com:8008/cgi/WebCGI?1500101=account=".$user_name."&amp;password=".$password."&amp;port=".$port."&amp;destination=".$mobile."&amp;content=".$msg;
		$curl = curl_init ();
		curl_setopt ( $curl, CURLOPT_URL, $URL );
		curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $curl, CURLOPT_HEADER, false );
		$result = curl_exec ( $curl );
		curl_close ( $curl );
		return $result;		
	}
	
	/**
	 * Send Email using SMTP
	 * @param unknown $to
	 * @param string $subject
	 * @param string $message
	 * @param string $cc
	 * @return boolean
	 */
	public function sendEmail($to, $subject = '', $message = '', $cc = NULL) {
	
		/*
		 $query = "SELECT s.* from smtpsetting as s
		 where s.id=1 AND s.status='" . Status_Type_Enum::ACTIVE."'";
		 $result = $this->db->query ( $query );
		 $smtp_details = $this->fetchAll ( $result );
		 // Email configuration
		 $config = Array (
		 'protocol' => 'smtp',
		 'smtp_host' => $smtp_details [0]->smtpHost,
		 'smtp_port' => $smtp_details [0]->smtpPort,
		 'smtp_user' => $smtp_details [0]->smtpUsername, // change it to yours
		 'smtp_pass' => $smtp_details [0]->smtpPassword, // change it to yours
		 // 'smtp_crypto' => $smtp_details [0]->smtpTransportLayerSecurity,
		 'mailtype' => 'html',
		 'charset' => 'iso-8859-1',
		 'wordwrap' => TRUE
		 );
		 if (is_array ( $to ) && empty ( $to )) {
		 $to = implode ( ',', $to );
		 }
		 if (is_array ( $cc ) && empty ( $cc )) {
		 $cc = implode ( ',', $cc );
		 }
		 $this->load->library ( 'email', $config );
		 $this->email->from ( SMTP_EMAIL_ID, SMTP_EMAIL_NAME );
		 $this->email->to ( $to );
		 $this->email->cc ( $cc );
		 $this->email->subject ( $subject );
		 $this->email->message ( $message );
		 	
		 // $data['message'] = "Sorry Unable to send email...";
		 if ($this->email->send ()) {
		 // $data['message'] = "Mail sent...";
		 return TRUE;
		 } else {
		 return FALSE;
		 }
	
	
		 $config = Array (
		 'protocol' => 'smtp',
		 'smtp_host' => "smtp.gmail.com",
		 'smtp_port' => 465,
		 'smtp_user' => "administrator@cordl.in", // change it to yours
		 'smtp_pass' => "Iamadmin1@", // change it to yours
		 // 'smtp_crypto' => $smtp_details [0]->smtpTransportLayerSecurity,
		 'mailtype' => 'html',
		 'charset' => 'iso-8859-1',
		 'wordwrap' => TRUE
		 );
		 if (is_array ( $to ) && empty ( $to )) {
		 $to = implode ( ',', $to );
		 }
		 if (is_array ( $cc ) && empty ( $cc )) {
		 $cc = implode ( ',', $cc );
		 }
		 $this->load->library ( 'email', $config );
		 $this->email->from ( "administrator@cordl.in", SMTP_EMAIL_NAME );
		 $this->email->to ( $to );
		 $this->email->cc ( $cc );
		 $this->email->subject ( $subject );
		 $this->email->message ( $message );
		 	
		 if ($this->email->send ()) {
		 return TRUE;
		 } else {
		 return FALSE;
		 }
		 	
			*/
	
		$this->mail = new PHPMailer(true);
		$this->mail->ClearAllRecipients();
		$this->mail->IsSMTP(); // telling the class to use SMTP
		//$this->mail->Mailer = "smtp";
		$this->mail->Encoding	= "base64";
		$this->mail->CharSet 	= "iso-8859-1";                  // CharSet
		$this->mail->SMTPDebug  = 0;
		// enables SMTP debug information
		$this->mail->IsHTML(true);
		$this->mail->SMTPSecure = "ssl";                 	// sets the prefix to the servier
		$this->mail->Port       = 465;
		$this->mail->Host       = "smtp.gmail.com"; // sets GMAIL as the SMTP server
		$this->mail->Username   = SMTP_EMAIL_ID;
		$this->mail->Password   = SMTP_EMAIL_PASSWORD;
		$this->mail->SMTPAuth   = true;
		$setFrom =SMTP_EMAIL_ID;
		$FromName = SMTP_EMAIL_NAME;
		$this->mail->SetFrom($setFrom, $FromName);
		$to_name ="";
		$this->mail->AddAddress($to, $to_name);
	
		$this->mail->Subject = $subject;
		$this->mail->Body    = $message;
		$result = $this->mail->Send();
		if ($result)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
		//return TRUE;
	}
	
	/**
	 * To get trip details list including driver,passenger,trip & transaction
	 * @param string $trip_id
	 * @param string $passenger_id
	 * @return Ambigous <multitype:, multitype:object >
	 */
	
	public function getTripDetails($trip_id = NULL, $passenger_id = NULL) {
		if ($trip_id && ! $passenger_id) {
			$where = "td.id=" . $trip_id;
		}
		if ($passenger_id && $trip_id) {
			$where = "td.id=" . $trip_id . " AND td.passengerId=" . $passenger_id." group by td.id";
		} else if ($passenger_id && ! $trip_id) {
			$where = "td.passengerId=" . $passenger_id . ' AND DATE(td.createdDatetime)=' . getCurrentDate () . ' order by td.id DESC';
		}
	
		$query = "SELECT td.id As 'tripId',td.jobCardId as 'jobCardId',td.pickupLocation As 'pickupLocation',td.pickupLatitude As 'pickupLatitude',
				td.pickupLongitude As 'pickupLongitude',td.dropLocation As 'dropLocation',td.dropLatitude As 'dropLatitude',td.dropLongitude As 'dropLongitude',td.customerReferenceCode As 'customerReferenceCode',
				td.pickupDatetime As 'pickupDatetime',td.actualPickupDatetime As 'actualPickupDatetime',td.dropDatetime As 'dropDatetime',td.zoneId As 'bookingZoneId',td.dropZoneId As 'bookingDropZoneId',td.entityId As 'entityId',
				td.tripStatus As 'tripStatus',dats.description As 'tripStatusName',td.notificationStatus As 'notificationStatus',td.driverAcceptedStatus As 'driverAcceptedStatus',td.tripType As 'tripType',
				td.bookedFrom As 'bookedFrom',td.taxiCategoryType As 'taxiCategoryType',td.estimatedDistance As 'estimatedDistance',td.estimatedTime As 'estimatedTime',
				datt.description As 'tripTypeName',td.paymentMode As 'paymentMode',dapm.description As 'paymentModeName',
				td.promoCode As 'promoCode', td.landmark As 'landmark',td.passengerId As 'passengerId',td.driverId As 'driverId',td.driverRating As 'driverRating', td.driverComments As 'driverComments',
				td.passengerRating As 'passengerRating',td.passengerComments As 'passengerComments',td.createdBy As 'createdBy',
				ttd.id As 'transactionId',ttd.travelledDistance As 'travelledDistance',ttd.travelledPeriod As 'travelledPeriod',ttd.convenienceCharge As 'baseCharge',
				ttd.travelCharge As 'travelCharge',ttd.parkingCharge As 'parkingCharge',ttd.tollCharge As 'tollCharge',ttd.totalTripCharge As 'totalTripCharge',ttd.taxPercentage As 'taxPercentage',
				ttd.taxCharge As 'taxCharge',ttd.promoDiscountAmount As 'promoDiscountAmount',ttd.walletPaymentAmount As 'walletPaymentAmount',ttd.bookingCharge As 'bookingCharge',ttd.penaltyCharge As 'penaltyCharge',
				p.id As 'passengerId',p.firstName As 'passengerFirstName' ,p.lastName As 'passengerLastName' ,p.mobile As 'passengerMobile' ,p.profileImage As 'passengerProfileImage',
				p.currentLatitude As 'passengerCurrentLatitude',p.currentLongitude As 'passengerCurrentLongitude',p.penaltyTripCount As 'penaltyTripCount',p.walletAmount As 'walletAmount',
				
				
				
				d.id As 'driverId',d.firstName As 'driverFirstName' ,d.lastName As 'driverLastName' ,d.mobile As 'driverMobile' ,d.experience As 'driverExperience',
				d.profileImage As 'driverProfileImage',d.languageKnown As 'driverLanguagesKnown',d.currentLatitude As 'currentLatitude',d.currentLongitude As 'currentLongitude',d.updatedDatetime As 'driverLastUpdated',
				d.currentBearing As 'currentBearing',d.licenseNo As 'driverLicenseNo',dpd.dob As 'driverDob',
				txd.id As 'taxiId',txd.name As 'taxiName',txd.model As 'taxiModel',txd.manufacturer As 'taxiManufacturer',txd.colour as 'taxiColour',txd.registrationNo As 'taxiRegistrationNo',
				u.firstName As 'userFirstName' ,u.lastName As 'userLastName',
				dsh.availabilityStatus As 'availabilityStatus', dsh.shiftStatus As 'shiftStatus',dsh.inLatitude As 'inLatitude',dsh.inLongitude As 'inLongitude',
				dsh.outLatitude As 'outLatitude',dsh.outLongitude As 'outLongitude'
				from tripdetails as td
				left join passenger as p on td.passengerId=p.id
				left join driver as d on td.driverId=d.id
				left join driverpersonaldetails as dpd on dpd.driverId=td.driverId
				left join drivershifthistory as dsh on dsh.driverId=td.driverId
				left join drivertaximapping as dtm on dtm.driverId=d.id
				left join taxidetails as txd on dtm.taxiId=txd.id
				left join triptransactiondetails as ttd on ttd.tripId=td.id
				left join userlogindetails as u on td.createdBy=u.id
				left join dataattributes as datt on td.tripType=datt.id
				left join dataattributes as dapm on td.paymentMode=dapm.id
				left join dataattributes as dats on td.tripStatus=dats.id
				where " . $where;
		$result = $this->db->query ( $query );
		return $this->fetchAll ( $result );
	}
	
	/**
	 * To check promocode availabilty for particular passenger including in City
	 * @param unknown $promocode
	 * @param number $passenger_id
	 * @param number $city_id
	 * @return Ambigous <multitype:, multitype:object >|boolean
	 */
	/*public function checkPromocodeAvailablity($promocode, $passenger_id = 0, $city_id = 0) {
		if ($promocode) {
			$where_clause = "ppd.promoCode='" . $promocode."'";
			if ($city_id > 0) {
				$where_clause = "ppd.promoCode='" . $promocode . "' AND (ppd.passengerId=" . $passenger_id . " OR ppd.passengerId=0) AND ppd.cityId=" . $city_id;
			}
			if ($passenger_id > 0) {
				$where_clause = "ppd.promoCode='" . $promocode . "' AND ppd.passengerId=" . $passenger_id . " AND (ppd.cityId=" . $city_id . " OR ppd.cityId=0)";
			}
			$query = "SELECT ppd.* from promocodedetails as ppd where " . $where_clause;
			$result = $this->db->query ( $query );
			return $this->fetchAll ( $result );
		} else {
			return FALSE;
		}
	}*/
	
	/**
	 * To check promocode availabilty for particular passenger including in Zone
	 * @param unknown $promocode
	 * @param number $passenger_id
	 * @param number $zone_id
	 * @return Ambigous <multitype:, multitype:object >|boolean
	 */
	public function checkPromocodeAvailablity($promocode, $passenger_id = 0, $zone_id = 0) {
		if ($promocode) {
			$where_clause = "ppd.promoCode='" . $promocode."'";
			if ($zone_id > 0) {
				$where_clause = "ppd.promoCode='" . $promocode . "'  AND ppd.ZoneId=" . $zone_id;
			}
			if ($passenger_id > 0) {
				$where_clause = "ppd.promoCode='" . $promocode . "' AND ppd.passengerId=".$passenger_id;
			}
			if ($zone_id > 0 && $passenger_id > 0 )
			{
				$where_clause = "ppd.promoCode='" . $promocode . "' AND (ppd.passengerId=".$passenger_id." AND ppd.ZoneId=" . $zone_id.")" ;
			}
			else
			{
				$where_clause = "ppd.promoCode='" . $promocode."'";
			}
			$query = "SELECT ppd.* from promocodedetails as ppd where " . $where_clause." AND ppd.isDeleted=" . Status_Type_Enum::INACTIVE . " AND ppd.status=" . Status_Type_Enum::ACTIVE;
			
			$result = $this->db->query ( $query );
			return $this->fetchAll ( $result );
		} else {
			return FALSE;
		}
	}
}