<?php
class Trip_Transaction_Details_Model extends MY_Model {
	protected $_table = 'triptransactiondetails'; // model table_name
	                                              // set column field database for datatable orderable
	protected $_column_order = array (
			null,
			'jobCardId',
			'entityName',
			'invoiceNo',
			'pickupLocation',
			'pickupDatetime',
			'dispatchedDatetime',
			'driverAcceptedDatetime',
			'taxiArrivedDatetime',
			'actualPickupDatetime',
			'dropLocation',
			'dropDatetime',
			'taxiRegistrationNo',
			'taxiCategoryTypeName',
			'tripTypeName',
			'paymentModeName',
			'passengerInfo',
			'driverCode',
			'driverInfo',
			'promoCode',
			'totalTripCharge',
			//'estimatedDistance',
			'travelledDistance',
			'travelledPeriod',
			'estimatedPeriod',
			'convenienceCharge',
			'travelCharge',
			'promoDiscountAmount',
			'walletPaymentAmount',
			'bookingCharge',
			'penaltyCharge',
			'adminAmount',
			'driverEarning',
			'bookingZone'
	);
	// set column field database for datatable searchable
	protected $_column_search = array (
			'td.jobCardId',
			'ttd.invoiceNo',
			'td.pickupLocation',
			'td.pickupDatetime',
			'td.dispatchedDatetime',
			'td.driverAcceptedDatetime',
			'td.taxiArrivedDatetime',
			'td.actualPickupDatetime',
			'td.dropLocation',
			'td.dropDatetime',
			'txd.registrationNo',
			'datc.description',
			'datt.description',
			'dapm.description',
			'p.firstName',
			'p.lastName',
			'p.mobile',
			'd.driverCode',
			'd.firstName',
			'd.lastName',
			'd.mobile',
			'td.promoCode',
			'ttd.totalTripCharge',
			'ttd.travelledDistance',
			'ttd.travelledPeriod',
			'td.estimatedTime',
			'ttd.convenienceCharge',
			'ttd.travelCharge',
			//'td.estimatedDistance',
			'ttd.promoDiscountAmount',
			'ttd.walletPaymentAmount',
			'ttd.bookingCharge',
			'ttd.penaltyCharge',
			'ttd.adminAmount',
			'ttd.driverEarning',
			'z.name',
			'e.name' 
	);
	// default order
	protected $_order = array (
			'ttd.id' => 'desc' 
	);
	
	/**
	 * Default Constructor
	 */
	function __construct($args = NULL) {
		parent::__construct ();
		if (is_object ( $args ))
			$args = get_object_vars ( $args );
		if (is_array ( $args )) {
			foreach ( $args as $key => $value ) {
				$this->{$key} = $value;
			}
		}
	}
	public function getTripTransactionListQuery($start_date=NULL,$end_date=NULL,$zone_id=NULL,$entity_id=NULL) {
		$where_clause='';
		$date_range_where='';
		$zone_where='';
		$entity_where='';

		$date_range_where=" AND ttd.createdDatetime BETWEEN DATE_ADD('".getCurrentDateTime()."', INTERVAL -10 DAY) AND DATE_ADD('".getCurrentDateTime()."', INTERVAL +0 DAY)";
		
		if ($start_date!=''&& $end_date!='')
		{
			$date_range_where=" AND ttd.createdDatetime >='" . date('Y-m-d H:i:s', strtotime($start_date)) . "' AND ttd.createdDatetime <='" . date('Y-m-d H:i:s', strtotime($end_date)) . "'";
		}
		if ($zone_id!='')
		{
			$zone_where=' AND td.zoneId='.$zone_id;
		}
		if ($entity_id!='')
		{
			$entity_where=' AND td.entityId='.$entity_id;
		}
		
		$where_clause=$date_range_where.' '.$zone_where.' '.$entity_where;
		
		$query = "SELECT td.id As 'tripId',td.jobCardId as 'jobCardId',td.pickupLocation As 'pickupLocation',td.dropLocation As 'dropLocation',td.customerReferenceCode As 'customerReferenceCode',
				td.pickupDatetime As 'pickupDatetime',td.dispatchedDatetime As 'dispatchedDatetime',td.driverAcceptedDatetime As 'driverAcceptedDatetime',td.actualPickupDatetime As 'taxiArrivedDatetime',
				td.actualPickupDatetime As 'actualPickupDatetime',td.dropDatetime As 'dropDatetime',td.zoneId As 'bookingZoneId',td.entityId As 'entityId',
				td.tripStatus As 'tripStatus',td.tripType As 'tripType',datt.description As 'tripTypeName',td.paymentMode As 'paymentMode',dapm.description As 'paymentModeName',
				td.promoCode As 'promoCode', td.landmark As 'landmark',td.driverRating As 'driverRating', td.driverComments As 'driverComments',datc.description As 'taxiCategoryTypeName',
				td.passengerRating As 'passengerRating',td.passengerComments As 'passengerComments',td.createdBy As 'createdBy',dabf.description As 'bookedFromName',td.bookedFrom As 'bookedFrom',
				td.estimatedDistance As 'estimatedDistance',td.estimatedTime As 'estimatedPeriod',
				z.name As 'bookingZone',e.name As 'entityName',
				ttd.id As 'transactionId',ttd.travelledDistance As 'travelledDistance',ttd.travelledPeriod As 'travelledPeriod',ttd.convenienceCharge As 'convenienceCharge',
				ttd.travelCharge As 'travelCharge',ttd.parkingCharge As 'parkingCharge',ttd.tollCharge As 'tollCharge',ttd.totalTripCharge As 'totalTripCharge',ttd.taxPercentage As 'taxPercentage',
				ttd.taxCharge As 'taxCharge',ttd.promoDiscountAmount As 'promoDiscountAmount',ttd.walletPaymentAmount As 'walletPaymentAmount',ttd.bookingCharge As 'bookingCharge',ttd.adminAmount As 'adminAmount',
				ttd.driverEarning As 'driverEarning',ttd.invoiceNo As 'invoiceNo',ttd.penaltyCharge As 'penaltyCharge',
				p.id As 'passengerId',CONCAT(CONCAT(p.firstName,' ',p.lastName),' ',p.mobile) As 'passengerInfo',
				d.id As 'driverId',d.driverCode As 'driverCode',CONCAT(CONCAT(d.firstName,' ',d.lastName),' ',d.mobile) As 'driverInfo',
				txd.id As 'taxiId',txd.registrationNo As 'taxiRegistrationNo',
				u.firstName As 'userFirstName' ,u.lastName As 'userLastName'
				from tripdetails as td
				left join passenger as p on td.passengerId=p.id
				left join driver as d on td.driverId=d.id
				left join driverpersonaldetails as dpd on dpd.driverId=td.driverId
				left join taxidetails as txd on td.taxiId=txd.id
				left join zonedetails as z on z.id =td.zoneId
				left join entitydetails as e on e.id =td.entityId
				left join triptransactiondetails as ttd on ttd.tripId=td.id
				left join userlogindetails as u on td.createdBy=u.id
				left join dataattributes as datt on td.tripType=datt.id
				left join dataattributes as dapm on td.paymentMode=dapm.id
				left join dataattributes as dabf on td.bookedFrom=dabf.id
				left join dataattributes as datc on td.taxiCategoryType=datc.id where td.tripStatus=".Trip_Status_Enum::TRIP_COMPLETED." ".$where_clause." Group by ttd.id";
		
		return $query;
	}
}