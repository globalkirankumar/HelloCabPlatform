<?php
require_once (APPPATH . 'config/promocode_type_enum.php');
class Promocode_Details_Model extends MY_Model {

	protected $_table = 'promocodedetails';//model table_name
	public $_keyName = 'id';
	public $_valueName = 'promoCode';
	
	protected $_column_order = array(null,'promoCode','promoDiscountAmount','promoDiscountAmount2','promoStartDatetime','promoEndDatetime','promoCodeLimit','promoCodeUserLimit','isFirstFreeRide','zone1Name','zone2Name','entityName','status'); //set column field database for datatable orderable
	protected $_column_search = array('pcd.promoCode','pcd.promoDiscountAmount','pcd.promoDiscountAmount2','dapt2.description','dapt.description','pcd.promoStartDatetime','pcd.promoEndDatetime','pcd.promoCodeLimit','pcd.promoCodeUserLimit','z.name','z2.name','e.name'); //set column field database for datatable searchable just firstname , lastname , address are searchable
	protected $_order = array('pcd.id' => 'desc');
	/**
	 *  Default Constructor
	 */
	function __construct($args=NULL)
	{
		parent::__construct();
		if( is_object($args))   $args = get_object_vars($args);
		if( is_array($args)){
			foreach( $args AS $key => $value ){
				$this->{$key} = $value;
			}
			
		}

	}
	/**
	 *  helper method to load the key value into dropdown boxes
	 * @return type
	 */
	public function getKeyName(){
		return $this->_keyName;
	}
	
	public function getValueName(){
		return $this->_valueName;
	}
	/**
	 * To get first free ride Valid & available promocode for passenger first trip while trip start.
	 * @param string $passenger_id
	 * @return boolean
	 */
	public function getFreeRidePromoCode($passenger_id=NULL)
	{
		$promocode=FALSE;
		if ($passenger_id > 0)
		{
			$this->load->model('Trip_Details_Model');
			$is_first_ride=$this->Trip_Details_Model->getByKeyValueArray(array('passengerId'=>$passenger_id,
					'tripStatus'=>Trip_Status_Enum::TRIP_COMPLETED
			),'id');
			if (count($is_first_ride) <= 0)
			{
				$promocode_details=$this->getOneByKeyValueArray(array (
						'promoStartDatetime<=' => getCurrentDateTime(),'promoEndDatetime>='=>getCurrentDateTime(),'isFirstFreeRide'=>Status_Type_Enum::ACTIVE
				),'id');
				 
				if ($promocode_details)
				{
	
					$promocode= $promocode_details->promoCode;
	
				}
			}
		}
		return $promocode;
	}
	
	public function getPromocodeListQuery()
	{
		$query = "SELECT pcd.id As 'promoCodeId',pcd.promoCode As 'promoCode',pcd.promoDiscountAmount As 'promoDiscountAmount',pcd.promoDiscountAmount2 As 'promoDiscountAmount2',dapt.description As 'promoDiscountTypeName',dapt2.description As 'promoDiscountTypeName2',
				pcd.promoStartDatetime As 'promoStartDatetime',pcd.promoEndDatetime As 'promoEndDatetime',pcd.promoCodeLimit As 'promoCodeLimit',pcd.promoCodeUserLimit As 'promoCodeUserLimit',
				pcd.zoneId As 'promoZoneId',pcd.zoneId2 As 'promoZoneId2',z.name As 'zone1Name',z2.name As 'zone2Name',e.name As 'entityName',pcd.isFirstFreeRide As 'isFirstFreeRide',pcd.status As 'status'
				from  promocodedetails As pcd
				left join dataattributes as dapt On dapt.id=pcd.promoDiscountType
				left join dataattributes as dapt2 On dapt2.id=pcd.promoDiscountType2
				left join zonedetails as z on z.id =pcd.zoneId
				left join zonedetails as z2 on z2.id =pcd.zoneId2
				left join entitydetails as e on e.id =pcd.entityId Where pcd.isDeleted=".Status_Type_Enum::INACTIVE;
	
		return $query;
	}
	
	
	
	
}