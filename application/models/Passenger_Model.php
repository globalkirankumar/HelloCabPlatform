<?php
require_once (APPPATH . 'config/device_type_enum.php');
require_once (APPPATH . 'config/signup_type_enum.php');
class Passenger_Model extends MY_Model {
	protected $_table = 'passenger'; // model table_name
	protected $_column_order = array (
			null,
			'profileImage',
			'passengerCode',
			'firstName',
			'lastName',
			'mobile',
			'email',
			'passengerAvgRating',
			'walletAmount',
			'joinedDatetime',
			'zoneName',
			'lastLoggedIn',
			'loginStatus',
			'status' 
	); // set column field database for datatable orderable
	protected $_column_search = array (
			'p.passengerCode',
			'p.firstName',
			'p.lastName',
			'p.mobile',
			'p.walletAmount',
			'p.email',
			'z.name',
			'p.lastLoggedIn',
			'p.createdDatetime' 
	); // set column field database for datatable searchable just firstname , lastname , address are searchable
	protected $_order = array (
			'p.id' => 'desc' 
	);
	/**
	 * Default Constructor
	 */
	function __construct($args = NULL) {
		parent::__construct ();
		if (is_object ( $args ))
			$args = get_object_vars ( $args );
		if (is_array ( $args )) {
			foreach ( $args as $key => $value ) {
				$this->{$key} = $value;
			}
		}
	}
	public function getPassengerListQuery() {
		$query = "SELECT p.id As 'passengerId',p.passengerCode As 'passengerCode',p.firstName As 'firstName',p.lastName As 'lastName',
				p.mobile As 'mobile',p.email As 'email',p.profileImage As 'profileImage',p.lastLoggedIn As 'lastLoggedIn',z.name As 'zoneName',
				p.loginStatus As 'loginStatus',p.status As 'status',p.walletAmount As 'walletAmount',p.createdDatetime As 'joinedDatetime',
				AVG(CASE WHEN td.tripStatus='" . Trip_Status_Enum::TRIP_COMPLETED . "' THEN td.driverRating ELSE 0 END) As 'passengerAvgRating'
				from  passenger As p
				left join tripdetails As td on td.passengerId =p.id AND td.tripStatus='" . Trip_Status_Enum::TRIP_COMPLETED . "' 
				left join zonedetails as z on z.id =p.zoneId group by p.id";
		
		return $query;
	}
	public function getPassengerInfoForTripBooking($keyword = '', $type = '') {
		if ($keyword != '') {
			if ($type == 'mobile') {
				$filter_qry = " WHERE (mobile like '" . $keyword . "%')";
			}
			if ($type == 'email') {
				$filter_qry = " WHERE (email like '" . $keyword . "%')";
			}
			$query = "SELECT CONCAT(`firstName` ,' ',`lastName`) as passenger_name,id as passenger_id,walletAmount as previous_wallet_amt," . " email as passenger_email,mobile as passenger_mobile " . " FROM passenger" . " $filter_qry" . " AND status='" . Status_Type_Enum::ACTIVE . "'" . " ORDER BY firstName LIMIT 0,6";
			$result = $this->db->query ( $query );
			return $this->fetchAll ( $result );
		}
	}
	
	public function checkPassengerHasTrip($passenger_id,$trip_type) {
		$ride_later_period_limit=6000;
		$ride_now_period_limit=200;
		$where_clause=" td.passengerId=".$passenger_id." AND td.tripStatus NOT IN (" . Trip_Status_Enum::TRIP_COMPLETED . ",".Trip_Status_Enum::CANCELLED_BY_PASSENGER.",".Trip_Status_Enum::CANCELLED_BY_DRIVER.",".Trip_Status_Enum::DRIVER_NOT_FOUND.",".Trip_Status_Enum::DRIVER_REJECTED_TRIP.")";
		if ($trip_type==Trip_Type_Enum::NOW)
		{
			$where_clause.=" AND td.tripType=".$trip_type;
		}
		else if ($trip_type==Trip_Type_Enum::LATER)
		{
			$where_clause.=" AND td.tripType=".$trip_type." AND td.pickupDatetime BETWEEN '".getCurrentDateTime()."' AND ADDTIME('".getCurrentDateTime()."' ,$ride_later_period_limit)";
		}
		$query = "SELECT td.id As 'tripId',
				from  tripdetails As td where ".$where_clause." ORDER BY td.pickupDatetime";
	
		return $query;
	}
}