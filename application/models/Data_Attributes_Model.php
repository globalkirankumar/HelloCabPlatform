<?php
class Data_Attributes_Model extends MY_Model {

	protected $_table = 'dataattributes';//model table_name
	public $_keyName = 'id';
	public $_valueName = 'description';
	public $_intervalName = 'internal_name';
	
	/**
	 *  Default Constructor
	 */
	function __construct($args=NULL)
	{
		parent::__construct();
		if( is_object($args))   $args = get_object_vars($args);
		if( is_array($args)){
			foreach( $args AS $key => $value ){
				$this->{$key} = $value;
			}
		}

	}
	
	/**
	 *  helper method to load the key value into dropdown boxes
	 * @return type
	 */
	public function getKeyName(){
		return $this->_keyName;
	}
	
	public function getValueName(){
		return $this->_valueName;
	}
	public function getInternalName(){
		return $this->_intervalName;
	}
	
	/**
	 * Retrieves Table as an array
	 * @param $internal_name     database internal_name value
	 * @return array
	 */
	public function getTableArray($table_name,$where_clauses= NULL){
			
		$key_cache = $this->_table."_".$table_name;
			
		//Fetch the result from the cache
		$result = $this->cachedata->get($key_cache);
	
		if(!$result)
		{
			$this->db->select('id');
			$this->db->select('internal_name');
			$this->db->where('table_name',$table_name);
	
			if( !(is_null($where_clauses)) )
			{
				foreach ($where_clauses AS $key => $value) {
					if (gettype($value) == 'array') {
						$this->db->where_in($key, $value);
					} else {
						$this->db->where($key, $value);
					}
				}
			}else{
				$this->db->where('visible',1);
			}
	
			$query = $this->db->get( $this->_table );
			foreach ($query->result() as $row)
			{
				$table[$row->internal_name]=$row->id;
			}
			$this->cachedata->storeData($key_cache,$table);
			$result = $table;
		}
		return $result;
	}
	/**
	 * Retrieves id
	 * @param $internal_name     database internal_name value
	 * @param $table_name    	 database table_name value
	 * @return mixed
	 */
	public function getIdByTableName($internal_name,$table_name){
	
		$where =array('internal_name' => $internal_name,'table_name' =>$table_name);
		$result = $this->getByKeyValueArray($where);
			
		if( count($result) > 0 ){
			return $result[0]->id;
		}
		return FALSE;
		 
	}
	/**
	 * The  parent method has been overridden to make use of cache
	 * If data is available in the cache it is immediately fetched from cache and returned
	 * Else data is first fetched from db and then stored in cache and returned
	 *
	 * @param key-value array $where_clauses
	 * @param string $orderby :: column name
	 * @return mixed
	 */
	public function getByKeyValueArray( $where_clauses='' , $orderby=''){
	
		$key_cache = $this->_table."_";
	
		if($orderby != '')
		{
			$key_cache.= $orderby."_";
		}
	
		$key_cache.= (is_array($where_clauses) ) ? implode('_', $where_clauses):$where_clauses;
	
		//Fetch the result from the cache
		$result = $this->cachedata->get($key_cache);
	
		if(!$result)
		{
			//Result is not available in the cache so fetch it from db and cache it
			$result = parent::getByKeyValueArray($where_clauses,$orderby);
			$this->cachedata->storeData($key_cache,$result);
		}
		return $result;
	}
	
	/**
	 * The  parent method has been overridden to make use of cache
	 * If data is available in the cache it is immediately fetched from cache and returned
	 * Else data is first fetched from db and then stored in cache and returned
	 *
	 * Retrieves by a single where clause
	 * @param $key      database key
	 * @param $value    Value of where clause (can even be an array of values)
	 * @return array
	 */
	public function getByKeyValue( $key , $value , $orderby='' ){
		$where_clause = array($key=>$value);
		return $this->getByKeyValueArray($where_clause,$orderby);
	}
	
	
	/**
	 * If the result set is expected to have one row in the result set, then it returns the object and not an array
	 * @param $key      Database key
	 * @param $value    Value of where clause
	 * @return mixed
	 */
	public function getOneByKeyValue( $key , $value , $orderby='' ){
		$where_clause = array($key=>$value);
		$result = $this->getByKeyValueArray($where_clause,$orderby);
		if( count($result) > 0 ){
			return $result[0];
		}
		return FALSE;
	}
	
	/**
	 * Returns the single row as object by common primary key
	 * @param $enumName       Value of id field
	 * @return mixed
	 */
	public function getById( $enumName ){
	
		$where_clause = array('enumName'=>$enumName);
		$result = $this->getByKeyValueArray($where_clause);
	
		//print_R($result[0]);exit;
	
		if( count($result) > 0 ){
			return $result[0];
		}
		return NULL;
	}
	
}