<?php
require_once (APPPATH . 'config/device_status_enum.php');
require_once (APPPATH . 'config/device_type_enum.php');
require_once (APPPATH . 'config/dispatch_type_enum.php');
require_once (APPPATH . 'config/driver_accepted_status_enum.php');
require_once (APPPATH . 'config/driver_shift_status_enum.php');
require_once (APPPATH . 'config/emergency_status_enum.php');
require_once (APPPATH . 'config/entity_type_enum.php');
require_once (APPPATH . 'config/experience_enum.php');
require_once (APPPATH . 'config/gender_type_enum.php');
require_once (APPPATH . 'config/license_type_enum.php');
require_once (APPPATH . 'config/module_name_enum.php');
require_once (APPPATH . 'config/payment_method_enum.php');
require_once (APPPATH . 'config/payment_mode_enum.php');
require_once (APPPATH . 'config/payment_type_enum.php');
require_once (APPPATH . 'config/register_type_enum.php');
require_once (APPPATH . 'config/role_type_enum.php');
require_once (APPPATH . 'config/signup_type_enum.php');
require_once (APPPATH . 'config/slab_type_enum.php');
require_once (APPPATH . 'config/status_type_enum.php');
require_once (APPPATH . 'config/taxi_available_status_enum.php');
require_once (APPPATH . 'config/taxi_request_status_enum.php');
require_once (APPPATH . 'config/taxi_type_enum.php');
require_once (APPPATH . 'config/transaction_mode_enum.php');
require_once (APPPATH . 'config/transaction_type_enum.php');
require_once (APPPATH . 'config/transaction_from_enum.php');
require_once (APPPATH . 'config/transmission_type_enum.php');
require_once (APPPATH . 'config/trip_status_enum.php');
require_once (APPPATH . 'config/trip_type_enum.php');
require_once (APPPATH . 'config/user_type_enum.php');

class Trip_Details_Report_Query_Model extends MY_Model {
	
	// set column field database for datatable orderable
	protected $_column_order = array (
			'zoneName',
			'totalUnassignedTrip',
			'totalUpcomingTrip',
			'totalOngoingTrip',
			'totalCompletedTrip',
			'totalPassengerCancelledTrip',
			'totalDriverCancelledTrip',
			'totalDriverRejectedTrip',
			'totalDriverNotFoundTrip',
			'totalTripCount',
			'totalOpsBookingTrip',
			'totalMobileBookingTrip'
	);
	
	// set column field database for datatable searchable
	protected $_column_search = array (
			'z.name'
	);
	// default order
	protected $_order = array (
			'z.name' => 'desc' 
	);
	/**
	 * Default Constructor
	 */
	function __construct($args = NULL) {
		parent::__construct ();
		if (is_object ( $args ))
			$args = get_object_vars ( $args );
		if (is_array ( $args )) {
			foreach ( $args as $key => $value ) {
				$this->{$key} = $value;
			}
		}
	}
	public function getTripDetailsListQuery($start_date,$end_date,$entity_id='',$zone_id='') {
		
		$where_caluse = "z.isDeleted=" . Status_Type_Enum::INACTIVE . " AND z.status=" . Status_Type_Enum::ACTIVE;
		$zone_where = '';
		$entity_where = '';
		
		$date_range_where = " AND td.createdDatetime >='" . date ( 'Y-m-d H:i:s', strtotime ( $start_date ) ) . "' AND td.createdDatetime <='" . date ( 'Y-m-d H:i:s', strtotime ( $end_date ) ) . "'";
		if ($zone_id!= '') {
			$zone_where = " AND td.zoneId=" . $zone_id;
		}
		if ($entity_id!= '') {
			$entity_where = " AND td.entityId=" . $entity_id;
		}
		$where_caluse .= $zone_where . " " . $entity_where . " " . $date_range_where;
		
		$query = "SELECT z.name As 'zoneName',
				SUM(CASE WHEN td.tripStatus IN ('" . Trip_Status_Enum::BOOKED . "') THEN 1 ELSE 0 END) As 'totalUnassignedTrip',
				SUM(CASE WHEN td.tripStatus IN ('" . Trip_Status_Enum::DRIVER_DISPATCHED . "','" . Trip_Status_Enum::DRIVER_ACCEPTED . "')  THEN 1 ELSE 0 END) As 'totalUpcomingTrip',
				SUM(CASE WHEN td.tripStatus IN ('" . Trip_Status_Enum::IN_PROGRESS . "','" . Trip_Status_Enum::WAITING_FOR_PAYMENT . "','" . Trip_Status_Enum::DRIVER_ARRIVED . "')  THEN 1 ELSE 0 END) As 'totalOngoingTrip',
				SUM(CASE WHEN td.tripStatus IN ('" . Trip_Status_Enum::CANCELLED_BY_PASSENGER . "')  THEN 1 ELSE 0 END) As 'totalPassengerCancelledTrip',
				SUM(CASE WHEN td.tripStatus IN ('" . Trip_Status_Enum::DRIVER_NOT_FOUND . "')  THEN 1 ELSE 0 END) As 'totalDriverNotFoundTrip',
				SUM(CASE WHEN td.tripStatus IN ('" . Trip_Status_Enum::CANCELLED_BY_DRIVER . "')  THEN 1 ELSE 0 END) As 'totalDriverCancelledTrip',
				SUM(CASE WHEN td.tripStatus IN ('" . Trip_Status_Enum::DRIVER_REJECTED_TRIP . "')  THEN 1 ELSE 0 END) As 'totalDriverRejectedTrip',
				SUM(CASE WHEN td.tripStatus IN ('" . Trip_Status_Enum::TRIP_COMPLETED . "')  THEN 1 ELSE 0 END) As 'totalCompletedTrip',
				SUM(CASE WHEN td.tripStatus IN ('" . Trip_Status_Enum::BOOKED . "','" . Trip_Status_Enum::DRIVER_DISPATCHED . "','" . Trip_Status_Enum::DRIVER_ACCEPTED . "','" . Trip_Status_Enum::IN_PROGRESS . "','" . Trip_Status_Enum::DRIVER_ARRIVED . "','" . Trip_Status_Enum::WAITING_FOR_PAYMENT . "','" . Trip_Status_Enum::TRIP_COMPLETED . "','" . Trip_Status_Enum::CANCELLED_BY_DRIVER . "','" . Trip_Status_Enum::CANCELLED_BY_PASSENGER . "','" . Trip_Status_Enum::DRIVER_NOT_FOUND . "','" . Trip_Status_Enum::DRIVER_REJECTED_TRIP . "')  THEN 1 ELSE 0 END) As 'totalTripCount',
				SUM(CASE WHEN td.bookedFrom IN ('" . Signup_Type_Enum::BACKEND . "')  THEN 1 ELSE 0 END) As 'totalOpsBookingTrip',
				SUM(CASE WHEN td.bookedFrom IN ('" . Signup_Type_Enum::MOBILE . "')  THEN 1 ELSE 0 END) As 'totalMobileBookingTrip'
				from  zonedetails As z
				left join tripdetails As td on td.zoneId=z.id
				where ".$where_caluse. "  GROUP BY z.id";
		
		return $query;
	}
}