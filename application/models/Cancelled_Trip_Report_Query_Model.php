<?php
require_once (APPPATH . 'config/device_status_enum.php');
require_once (APPPATH . 'config/device_type_enum.php');
require_once (APPPATH . 'config/dispatch_type_enum.php');
require_once (APPPATH . 'config/driver_accepted_status_enum.php');
require_once (APPPATH . 'config/driver_shift_status_enum.php');
require_once (APPPATH . 'config/emergency_status_enum.php');
require_once (APPPATH . 'config/entity_type_enum.php');
require_once (APPPATH . 'config/experience_enum.php');
require_once (APPPATH . 'config/gender_type_enum.php');
require_once (APPPATH . 'config/license_type_enum.php');
require_once (APPPATH . 'config/module_name_enum.php');
require_once (APPPATH . 'config/payment_method_enum.php');
require_once (APPPATH . 'config/payment_mode_enum.php');
require_once (APPPATH . 'config/payment_type_enum.php');
require_once (APPPATH . 'config/register_type_enum.php');
require_once (APPPATH . 'config/role_type_enum.php');
require_once (APPPATH . 'config/signup_type_enum.php');
require_once (APPPATH . 'config/slab_type_enum.php');
require_once (APPPATH . 'config/status_type_enum.php');
require_once (APPPATH . 'config/taxi_available_status_enum.php');
require_once (APPPATH . 'config/taxi_request_status_enum.php');
require_once (APPPATH . 'config/taxi_type_enum.php');
require_once (APPPATH . 'config/transaction_mode_enum.php');
require_once (APPPATH . 'config/transaction_type_enum.php');
require_once (APPPATH . 'config/transaction_from_enum.php');
require_once (APPPATH . 'config/transmission_type_enum.php');
require_once (APPPATH . 'config/trip_status_enum.php');
require_once (APPPATH . 'config/trip_type_enum.php');
require_once (APPPATH . 'config/user_type_enum.php');

class Cancelled_Trip_Report_Query_Model extends MY_Model {
	
	// set column field database for datatable orderable
	protected $_column_order = array (
			'jobCardId',
			'driverName',
			'tripTypeName',
			'tripStatusName',
			'rejectedDrivers',
			'requestedPickUpDatetime',
			'dispatchedDatetime',
			'lastCronJob'
	);
	
	// set column field database for datatable searchable
	protected $_column_search = array (
			'td.jobCardId',
			'd.firstName',
			'd.lastName',
			'datt.description',
			'dats.description',
			'trd.rejecteddriverlist',
			'td.pickupDatetime',
			'td.dispatchedDatetime',
			'td.updatedDateTime'
	);
	// default order
	protected $_order = array (
			'd.id' => 'desc' 
	);
	/**
	 * Default Constructor
	 */
	function __construct($args = NULL) {
		parent::__construct ();
		if (is_object ( $args ))
			$args = get_object_vars ( $args );
		if (is_array ( $args )) {
			foreach ( $args as $key => $value ) {
				$this->{$key} = $value;
			}
		}
	}
	public function getCancelledTripReportQuery($start_date,$end_date,$trip_type=NULL, $trip_status=NULL) {
		$where_caluse = '';
		$date_range_where = '';
		$trip_type_where = '';
		$trip_status_where = '';
		$date_range_where = "td.pickupDatetime >='" . date ( 'Y-m-d H:i:s', strtotime ( $start_date ) ) . "' AND td.pickupDatetime <='" . date ( 'Y-m-d H:i:s', strtotime ( $end_date ) ) . "'";
		
		if ($trip_type != '') {
			$trip_type_where =" AND td.tripType=" . $trip_type;
		}
		if ($trip_status != '') {
			$trip_status_where =" AND td.tripStatus=" . $trip_status;
		}
		
		$where_caluse = $date_range_where . " " . $trip_type_where . " " . $trip_status_where;
		
		$query = "select td.jobCardId As 'jobCardId',d.firstname As 'driverName',datt.description As 'tripTypeName',dats.description As 'tripStatusName',trd.rejecteddriverlist As 'rejectedDrivers',
				td.pickupDatetime As 'requestedPickUpDatetime',td.dispatchedDatetime As 'dispatchedDatetime' ,td.updatedDateTime As 'lastCronJob',td.tripType As 'tripType',td.tripStatus As 'tripStatus'
				from tripdetails as td 
				left join taxirequestdetails as trd on td.id=trd.tripid 
				left join driver as d on d.id=td.driverid 
				left join dataattributes as datt on datt.id=td.tripType 
				left join dataattributes as dats on dats.id=td.tripStatus 
				where ".$where_caluse;
		return $query;
	}
}