<?php
class Entity_Config_Model extends MY_Model {

	protected $_table = 'entityconfigdetails';//model table_name
	public $_keyName = 'id';
	public $_valueName = 'name';
	/**
	 *  Default Constructor
	 */
	 
	protected $_column_order = array('name','description','metaKeyword','prefix','tagLine','email','phone','emergencyEmail','emergencyPhone','tollFreeNo','tax','adminCommission','referralDiscountAmount','copyRights','cityName','countryName','stateName',null); //set column field database for datatable orderable
	protected $_column_search = array('e.name','e.description',',e.metaKeyword','e.prefix','e.tagLine','e.email','e.phone','e.emergencyEmail','e.emergencyPhone','e.tollFreeNo','e.tax','e.adminCommission','e.referralDiscountAmount','e.copyRights','c.name','ct.name','s.name'); //set column field database for datatable searchable just firstname , lastname , address are searchable
	protected $_order = array('e.id ' => 'desc'); // default order
	function __construct($args=NULL)
	{
		parent::__construct();
		if( is_object($args))   $args = get_object_vars($args);
		if( is_array($args)){
			foreach( $args AS $key => $value ){
				$this->{$key} = $value;
			}
		}

	}
	
	/**
	 *  helper method to load the key value into dropdown boxes
	 * @return type
	 */
	 
	 public function getEntityListQuery()
	{
		$query = "SELECT e.id As 'entityId',e.name As 'name',e.description As 'description',e.metaKeyword As 'metaKeyword',
				e.prefix As 'prefix',e.tagLine As 'tagLine',e.email As 'email',e.phone As 'phone',e.emergencyEmail As 'emergencyEmail',e.emergencyPhone As 'emergencyPhone',e.tollFreeNo As 'tollFreeNo',e.tax As 'tax',e.adminCommission As 'adminCommission',e.referralDiscountAmount As 'referralDiscountAmount',e.copyRights As 'copyRights',c.name As 'cityName',ct.name As 'countryName',s.name As 'stateName'
				from  entitydetails As e
				left join city As c  on c.id=e.cityId
				left join state As s on s.id=e.stateId
				left join country As ct  on ct.id=e.countryId";
		return $query;
	}
	public function getKeyName(){
		return $this->_keyName;
	}
	
	public function getValueName(){
		return $this->_valueName;
	}
}