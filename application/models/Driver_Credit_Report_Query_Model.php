<?php
require_once (APPPATH . 'config/device_status_enum.php');
require_once (APPPATH . 'config/device_type_enum.php');
require_once (APPPATH . 'config/dispatch_type_enum.php');
require_once (APPPATH . 'config/driver_accepted_status_enum.php');
require_once (APPPATH . 'config/driver_shift_status_enum.php');
require_once (APPPATH . 'config/emergency_status_enum.php');
require_once (APPPATH . 'config/entity_type_enum.php');
require_once (APPPATH . 'config/experience_enum.php');
require_once (APPPATH . 'config/gender_type_enum.php');
require_once (APPPATH . 'config/license_type_enum.php');
require_once (APPPATH . 'config/module_name_enum.php');
require_once (APPPATH . 'config/payment_method_enum.php');
require_once (APPPATH . 'config/payment_mode_enum.php');
require_once (APPPATH . 'config/payment_type_enum.php');
require_once (APPPATH . 'config/register_type_enum.php');
require_once (APPPATH . 'config/role_type_enum.php');
require_once (APPPATH . 'config/signup_type_enum.php');
require_once (APPPATH . 'config/slab_type_enum.php');
require_once (APPPATH . 'config/status_type_enum.php');
require_once (APPPATH . 'config/taxi_available_status_enum.php');
require_once (APPPATH . 'config/taxi_request_status_enum.php');
require_once (APPPATH . 'config/taxi_type_enum.php');
require_once (APPPATH . 'config/transaction_mode_enum.php');
require_once (APPPATH . 'config/transaction_type_enum.php');
require_once (APPPATH . 'config/transaction_from_enum.php');
require_once (APPPATH . 'config/transmission_type_enum.php');
require_once (APPPATH . 'config/trip_status_enum.php');
require_once (APPPATH . 'config/trip_type_enum.php');
require_once (APPPATH . 'config/user_type_enum.php');

class Driver_Credit_Report_Query_Model extends MY_Model {
	
	// set column field database for datatable orderable
	protected $_column_order = array (
			null,
			null,
			'driverCode',
			'driverName',
			'driverMobile',
			'bankAccountNo',
			null,
			'bankBranch',
			'currencyCode',
			null,
			null,
			null,
			'driverCredit'
	);
	
	// set column field database for datatable searchable
	protected $_column_search = array (
			'd.driverCode',
			'd.firstName',
			'd.lastName',
			'd.mobile',
			'dp.bankAccountNo',
			'dp.bankBranch',
			'd.driverCredit',
			'c.currencyCode'
	);
	// default order
	protected $_order = array (
			'd.driverCredit' => 'desc' 
	);
	/**
	 * Default Constructor
	 */
	function __construct($args = NULL) {
		parent::__construct ();
		if (is_object ( $args ))
			$args = get_object_vars ( $args );
		if (is_array ( $args )) {
			foreach ( $args as $key => $value ) {
				$this->{$key} = $value;
			}
		}
	}
	public function getDriverCreditReportQuery() {
		$query = "SELECT d.id As 'driverId',d.driverCode As 'driverCode',CONCAT(d.firstName,' ',d.lastName) As 'driverName',d.mobile As 'driverMobile',
				d.driverWallet As 'driverWallet',d.driverCredit As 'driverCredit',dp.bankBranch As 'bankBranch',dp.bankAccountNo As 'bankAccountNo',c.currencyCode As 'currencyCode'
				from  driver As d
				left join driverpersonaldetails As dp on dp.driverId=d.id
				left join driverdispatchdetails As ddd on ddd.driverId=d.id
				left join entitydetails as e on e.id =d.entityId 
				left join country as c on c.id =e.countryId
				where d.isDeleted=" . Status_Type_Enum::INACTIVE . "  GROUP BY d.id";
		
		return $query;
	}
}