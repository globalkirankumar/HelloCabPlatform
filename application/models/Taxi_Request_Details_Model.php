<?php
class Taxi_Request_Details_Model extends MY_Model {

	protected $_table = 'taxirequestdetails';//model table_name
	
	/**
	 *  Default Constructor
	 */
	function __construct($args=NULL)
	{
		parent::__construct();
		if( is_object($args))   $args = get_object_vars($args);
		if( is_array($args)){
			foreach( $args AS $key => $value ){
				$this->{$key} = $value;
			}
		}

	}
	
	public function checkDriverTripAvilablity($driver_id)
	{
		$query = "SELECT trd.id As tripRequestId,trd.taxiRequestStatus As taxiRequestStatus,datr.description As taxiRequestStatusName
				from  taxirequestdetails As trd
				left join dataattributes as datr On datr.id=trd.taxiRequestStatus
				Where trd.selectedDriverId=".$driver_id." AND trd.taxiRequestStatus NOT IN (".Taxi_Request_Status_Enum::DRIVER_REJECTED.",".Taxi_Request_Status_Enum::PASSENGER_CANCELLED.",".Taxi_Request_Status_Enum::COMPLETED_TRIP.",".Taxi_Request_Status_Enum::DRIVER_NOT_FOUND.")";
		$result = $this->db->query ( $query );
		$output_result=$this->fetchAll ( $result);
		if($output_result)
		{
			return $output_result[0];
		}
		return false;
		
	}
	
}