<?php
class Taxi_Device_Details_Model extends MY_Model {
	protected $_table = 'taxidevicedetails'; // model table_name
	public $_keyName = 'id';
	public $_valueName = 'deviceCode';
	/**
	 * Default Constructor
	 */
	protected $_column_order = array (
			null,
			'taxiDeviceCode',
			'imeiNo',
			'mobile',
			'deviceStatus',
			'purchasedDate',
			'isWarranty',
			'isAllocated',
			'status' 
	); // set column field database for datatable orderable
	protected $_column_search = array (
			't.deviceCode',
			't.imeiNo',
			't.mobile',
			'dads.description',
			't.purchasedDate' 
	); // set column field database for datatable searchable just firstname , lastname , address are searchable
	protected $_order = array (
			't.id' => 'desc' 
	); // default order
	function __construct($args = NULL) {
		parent::__construct ();
		if (is_object ( $args ))
			$args = get_object_vars ( $args );
		if (is_array ( $args )) {
			foreach ( $args as $key => $value ) {
				$this->{$key} = $value;
			}
		}
	}
	/**
	 * helper method to load the key value into dropdown boxes
	 * 
	 * @return type
	 */
	public function getKeyName() {
		return $this->_keyName;
	}
	public function getValueName() {
		return $this->_valueName;
	}
	public function getTaxiDeviceListQuery() {
		$query = "SELECT t.id As 'taxiDeviceId',t.deviceCode As 'taxiDeviceCode',t.imeiNo As 'imeiNo',t.mobile As 'mobile',dads.description As 'deviceStatus',
				t.purchasedDate As 'purchasedDate',t.isAllocated As 'isAllocated',t.isWarranty As 'isWarranty',t.status As 'status'
				from  taxidevicedetails As t
				left join dataattributes As dads On dads.id=t.deviceStatus
				Where t.isDeleted=" . Status_Type_Enum::INACTIVE . " group by t.id";
		return $query;
	}
	public function getCurrentDeviceDetails() {
		$query = "SELECT tdd.id As 'taxiDeviceId',tdd.deviceCode As 'deviceCode' ,tdd.currentLatitude As 'deviceCurrentLatitude',
                    tdd.currentLongitude As 'deviceCurrentLongitude', d.driverCode As 'driverCode',
                    CONCAT(d.firstName ,' ',d.lastName) As 'driverFullName' , d.mobile As 'driverMobile'
                    FROM taxidevicedetails as tdd
					left join drivertaximapping as dtm ON dtm.taxiDeviceId=tdd.id
                    LEFT JOIN `driver` as  d ON dtm.driverId = d.id
                    WHERE tdd.`isAllocated`=" . Status_Type_Enum::ACTIVE . "
                    AND tdd.`status` =" . Status_Type_Enum::ACTIVE . "
                    AND tdd.`isDeleted`=" . Status_Type_Enum::INACTIVE . "
                    AND tdd.`deviceStatus`=" . Device_Status_Enum::OPEN;
		
		$result = $this->db->query ( $query );
		return $this->fetchAll ( $result );
	}
	public function getDeviceList($search = NULL) {
		if ($search) {
			$query = "SELECT tdd.id As 'id',tdd.deviceCode As 'deviceCode' ,tdd.currentLatitude As 'deviceCurrentLatitude',
                    tdd.currentLongitude As 'deviceCurrentLongitude', d.driverCode As 'driverCode',
                    CONCAT(d.firstName ,' ',d.lastName) As 'driverFullName' , d.mobile As 'driverMobile'
                    FROM taxidevicedetails as tdd
					left join drivertaximapping as dtm ON dtm.taxiDeviceId=tdd.id
                    LEFT JOIN driver as  d ON dtm.driverId = d.id
                    WHERE tdd.deviceCode LIKE '" . $search . "%'
                    AND tdd.status =" . Status_Type_Enum::ACTIVE . "
                    AND tdd.isDeleted=" . Status_Type_Enum::INACTIVE . "
                    AND tdd.deviceStatus=" . Device_Status_Enum::OPEN;
			
			$result = $this->db->query ( $query );
			return $this->fetchAll ( $result );
		} else {
			$query = "SELECT tdd.id As 'id',tdd.deviceCode As 'deviceCode' ,tdd.currentLatitude As 'deviceCurrentLatitude',
                    tdd.currentLongitude As 'deviceCurrentLongitude', d.driverCode As 'driverCode',
                    CONCAT(d.firstName ,' ',d.lastName) As 'driverFullName' , d.mobile As 'driverMobile'
                    FROM taxidevicedetails as tdd
					left join drivertaximapping as dtm ON dtm.taxiDeviceId=tdd.id
                    LEFT JOIN driver as  d ON dtm.driverId = d.id
                    WHERE tdd.status =" . Status_Type_Enum::ACTIVE . "
                    AND tdd.isDeleted=" . Status_Type_Enum::INACTIVE . "
                    AND tdd.deviceStatus=" . Device_Status_Enum::OPEN;
			
			$result = $this->db->query ( $query );
			return $this->fetchAll ( $result );
		}
	}
}