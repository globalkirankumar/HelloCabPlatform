<?php
class Trip_Details_Model extends MY_Model {
	protected $_table = 'tripdetails'; // model table_name
	                                   // set column field database for datatable orderable
	protected $_column_order = array (
			null,
			'jobCardId',
			'entityName',
			'pickupLocation',
			'pickupDatetime',
			'dispatchedDatetime',
			'driverAcceptedDatetime',
			'taxiArrivedDatetime',
			'actualPickupDatetime',
			'dropLocation',
			'taxiRegistrationNo',
			'passengerName',
			'passengerMobile',
			'driverCode',
			'driverName',
			'driverMobile',
			'taxiCategoryTypeName',
			'tripTypeName',
			'bookedFromName',
			'paymentModeName',
			'promoCode',
			'tripStatusName',
			'bookingZone' 
	);
	// set column field database for datatable searchable
	protected $_column_search = array (
			'td.jobCardId',
			'td.pickupLocation',
			'td.pickupDatetime',
			'td.actualPickupDatetime',
			'td.dispatchedDatetime',
			'td.driverAcceptedDatetime',
			'td.taxiArrivedDatetime',
			'td.actualPickupDatetime',
			'td.dropLocation',
			'txd.registrationNo',
			'p.firstName',
			'p.lastName',
			'p.mobile',
			'd.driverCode',
			'd.firstName',
			'd.lastName',
			'd.mobile',
			'datc.description',
			'datt.description',
			'dapm.description',
			'td.promoCode',
			'dats.description',
			'dabf.description',
			'z.name',
			'e.name'
	);
	// default order
	protected $_order = array (
			'td.tripType'=> 'desc',
			'td.id' => 'desc',
			'td.tripStatus'=>'asc'
			
	);
	
	/**
	 * Default Constructor
	 */
	function __construct($args = NULL) {
		parent::__construct ();
		if (is_object ( $args ))
			$args = get_object_vars ( $args );
		if (is_array ( $args )) {
			foreach ( $args as $key => $value ) {
				$this->{$key} = $value;
			}
		}
	}
	public function getTripListQuery($start_date=NULL,$end_date=NULL,$trip_type=NULL,$trip_status=array(),$zone_id=NULL,$entity_id=NULL) {
		$where_clause='';
		$date_range_where='';
		$trip_type_where='';
		$trip_status_where='';
		$zone_where='';
		$entity_where='';
		/* $date=getCurrentDateTime();
		$start_date = strtotime ("-10 day" , strtotime ( $date ) ) ;
		$end_date = strtotime ("8 day" , strtotime ( $date ) ) ; */
		
		$date_range_where=" AND td.pickupdatetime BETWEEN DATE_ADD('".getCurrentDateTime()."', INTERVAL -10 DAY) AND DATE_ADD('".getCurrentDateTime()."', INTERVAL +8 DAY)";
		
		if ($start_date!=''&& $end_date!='')
		{
			$date_range_where=" AND td.pickupdatetime >='" . date('Y-m-d H:i:s', strtotime($start_date)) . "' AND td.pickupdatetime <='" . date('Y-m-d H:i:s', strtotime($end_date)) . "'";
		}
		if ($trip_type!='')
		{
			$trip_type_where=' AND td.tripType='.$trip_type;
		}
		if ($trip_status!='')
		{
			$trip_status_where=' AND td.tripStatus IN ('.$trip_status.')';
		}
		if ($zone_id!='')
		{
			$zone_where=' AND td.zoneId='.$zone_id;
		}
		if ($entity_id!='')
		{
			$entity_where=' AND td.entityId='.$entity_id;
		}
		 
		$where_clause=$date_range_where.' '.$trip_type_where.' '.$trip_status_where.' '.$zone_where.' '.$entity_where;
		
		$query = "SELECT td.id As 'tripId',td.jobCardId as 'jobCardId',td.pickupLocation As 'pickupLocation',td.pickupLatitude As 'pickupLatitude',
				td.pickupLongitude As 'pickupLongitude',td.dropLocation As 'dropLocation',td.dropLatitude As 'dropLatitude',td.dropLongitude As 'dropLongitude',td.customerReferenceCode As 'customerReferenceCode',
				td.pickupDatetime As 'pickupDatetime',td.actualPickupDatetime As 'actualPickupDatetime',td.dispatchedDatetime As 'dispatchedDatetime',td.driverAcceptedDatetime As 'driverAcceptedDatetime',
				td.taxiArrivedDatetime As 'taxiArrivedDatetime',td.dropDatetime As 'dropDatetime',z.name As 'bookingZone',e.name As 'entityName',
				td.tripStatus As 'tripStatus',dats.description As 'tripStatusName',td.notificationStatus As 'notificationStatus',td.tripType As 'tripType',
				datt.description As 'tripTypeName',td.paymentMode As 'paymentMode',dapm.description As 'paymentModeName',td.taxiCategoryType As 'taxiCategoryType',datc.description As 'taxiCategoryTypeName',
				td.promoCode As 'promoCode', td.landmark As 'landmark',td.createdBy As 'createdBy',dabf.description As 'bookedFromName',td.bookedFrom As 'bookedFrom',
				p.id As 'passengerId',CONCAT(p.firstName,' ',p.lastName) As 'passengerName',p.mobile As 'passengerMobile' ,p.profileImage As 'passengerProfileImage',
				d.id As 'driverId',d.driverCode As 'driverCode',CONCAT(d.firstName,' ',d.lastName) As 'driverName',d.mobile As 'driverMobile' ,d.profileImage As 'driverProfileImage',d.licenseNo As 'driverLicenseNo',
				txd.id As 'taxiId',txd.name As 'taxiName',txd.model As 'taxiModel',txd.manufacturer As 'taxiManufacturer',txd.colour as 'taxiColour',txd.registrationNo As 'taxiRegistrationNo',
				u.firstName As 'userFirstName' ,u.lastName As 'userLastName'
				from tripdetails as td
				left join passenger as p on td.passengerId=p.id
				left join driver as d on td.driverId=d.id
				left join drivertaximapping as dtm on dtm.driverId=d.id
				left join taxidetails as txd on dtm.taxiId=txd.id
				left join zonedetails as z on z.id =td.zoneId
				left join entitydetails as e on e.id =td.entityId
				left join userlogindetails as u on td.createdBy=u.id
				left join dataattributes as datt on td.tripType=datt.id
				left join dataattributes as dapm on td.paymentMode=dapm.id
				left join dataattributes as dats on td.tripStatus=dats.id
				left join dataattributes as datc on td.taxiCategoryType=datc.id
				left join dataattributes as dabf on td.bookedFrom=dabf.id
				where td.tripStatus!=" . Trip_Status_Enum::TRIP_COMPLETED." ".$where_clause." Group By td.id";
		//debug_exit($query);
		return $query;
	}
	public function getUsedPromocodeLimitCount($promocode)
	{
		$query = "SELECT td.id As tripId,td.jobCardId As jobCardId,td.promoCode As promoCode,td.tripStatus,td.passengerId As passengerId
				from  tripdetails As td
				left join dataattributes as dats On dats.id=td.tripStatus
				Where td.promoCode='".$promocode."' AND td.tripStatus IN (".Trip_Status_Enum::TRIP_COMPLETED.",".Trip_Status_Enum::DRIVER_DISPATCHED.",".Trip_Status_Enum::DRIVER_ACCEPTED.")";
		$result = $this->db->query ( $query );
		$output_result=$this->fetchAll ( $result);
		return $output_result[0];
	
	}
	
	
}