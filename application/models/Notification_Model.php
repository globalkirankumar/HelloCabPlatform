<?php
class Notification_Model extends MY_Model {

	protected $_table = 'notification';//model table_name
	public $_keyName = 'id';
	public $_valueName = 'title';
	
	//set column field database for datatable orderable
	protected $_column_order = array(null,'title','description','notificationTo','notificationType','startDatetime','endDatetime','status');
	//set column field database for datatable searchable
	protected $_column_search = array('n.title','n.description','n.startDatetime','n.endDatetime');
	// default order
	protected $_order = array('n.id' => 'desc');
	/**
	 *  Default Constructor
	 */
	function __construct($args=NULL)
	{
		parent::__construct();
		if( is_object($args))   $args = get_object_vars($args);
		if( is_array($args)){
			foreach( $args AS $key => $value ){
				$this->{$key} = $value;
			}
		}

	}
	/**
	 *  helper method to load the key value into dropdown boxes
	 * @return type
	 */
	public function getKeyName(){
		return $this->_keyName;
	}
	
	public function getValueName(){
		return $this->_valueName;
	}
	public function getNotificationListQuery()
	{
		$query = "SELECT n.id As 'notificationId',n.title As 'title',n.description As 'description',n.notificationTo As 'notificationTo',n.notificationType As 'notificationType',n.startDatetime As 'startDatetime',n.endDatetime As 'endDatetime',n.status As 'status'
				from  notification As n
				where n.isDeleted=".Status_Type_Enum::INACTIVE." GROUP BY n.id";
				
		return $query;
	}
	
}