<?php
require_once (APPPATH . 'config/device_status_enum.php');
require_once (APPPATH . 'config/device_type_enum.php');
require_once (APPPATH . 'config/dispatch_type_enum.php');
require_once (APPPATH . 'config/driver_accepted_status_enum.php');
require_once (APPPATH . 'config/driver_shift_status_enum.php');
require_once (APPPATH . 'config/emergency_status_enum.php');
require_once (APPPATH . 'config/entity_type_enum.php');
require_once (APPPATH . 'config/experience_enum.php');
require_once (APPPATH . 'config/gender_type_enum.php');
require_once (APPPATH . 'config/license_type_enum.php');
require_once (APPPATH . 'config/module_name_enum.php');
require_once (APPPATH . 'config/payment_method_enum.php');
require_once (APPPATH . 'config/payment_mode_enum.php');
require_once (APPPATH . 'config/payment_type_enum.php');
require_once (APPPATH . 'config/register_type_enum.php');
require_once (APPPATH . 'config/role_type_enum.php');
require_once (APPPATH . 'config/signup_type_enum.php');
require_once (APPPATH . 'config/slab_type_enum.php');
require_once (APPPATH . 'config/status_type_enum.php');
require_once (APPPATH . 'config/taxi_available_status_enum.php');
require_once (APPPATH . 'config/taxi_request_status_enum.php');
require_once (APPPATH . 'config/taxi_type_enum.php');
require_once (APPPATH . 'config/transaction_mode_enum.php');
require_once (APPPATH . 'config/transaction_type_enum.php');
require_once (APPPATH . 'config/transaction_from_enum.php');
require_once (APPPATH . 'config/transmission_type_enum.php');
require_once (APPPATH . 'config/trip_status_enum.php');
require_once (APPPATH . 'config/trip_type_enum.php');
require_once (APPPATH . 'config/user_type_enum.php');

class Cronjob_Query_Model extends MY_Model {
	
	/**
	 * Default Constructor
	 */
	function __construct($args = NULL) {
		parent::__construct ();
		if (is_object ( $args ))
			$args = get_object_vars ( $args );
		if (is_array ( $args )) {
			foreach ( $args as $key => $value ) {
				$this->{$key} = $value;
			}
		}
	}
	
	public function getDriverLaterTripList()
	{
		
		$query = "SELECT td.id As 'tripId',td.zoneId As 'zoneId'
				from tripdetails As td
				where td.tripType=".Trip_Type_Enum::LATER." AND td.tripStatus=" . Trip_Status_Enum::BOOKED." AND
				td.pickupDatetime BETWEEN '".getCurrentDateTime()."' AND ADDTIME('".getCurrentDateTime()."' ,2500)
				ORDER BY td.pickupDatetime";
		
		$result = $this->db->query ( $query );
		return $this->fetchAll ( $result );
		
	}
	
	public function checkTripDispatchStatusUpdate()
	{
		
		$query = "SELECT trd.id As 'taxiRequestId',trd.tripId As 'tripId',,td.zoneId As 'zoneId'
				from taxirequestdetails As trd
				where trd.taxiRequestStatus=".Taxi_Request_Status_Enum::AVAILABLE_TRIP." AND trd.selectedDriverId!=".Status_Type_Enum::INACTIVE."
				AND trd.createdDatetime BETWEEN ADDTIME('".getCurrentDateTime()."' ,-22) AND '".getCurrentDateTime()."'
				ORDER BY trd.createdDatetime";
		
		$result = $this->db->query ( $query );
		return $this->fetchAll ( $result );
	
	}
	
	public function getDriverOnTripOffineStatus_old()
	{
		$query = "SELECT trd.id As 'taxiRequestId',trd.tripId As 'tripId',d.id As 'driverId',d.mobile As 'driverMobile',CONCAT(d.firstName,'',d.lastName) As 'driverName'
				from taxirequestdetails As trd
				left join driver as d on trd.selectedDriverId=d.id
				where trd.taxiRequestStatus=".Taxi_Request_Status_Enum::DRIVER_ACCEPTED." AND trd.selectedDriverId!=".Status_Type_Enum::INACTIVE."
				AND trd.updatedDatetime BETWEEN ADDTIME('".getCurrentDateTime()."' ,-300) AND '".getCurrentDateTime()."'
				ORDER BY trd.updatedDatetime";
	
		$result = $this->db->query ( $query );
		return $this->fetchAll ( $result );
	
	}
	
	public function getDriverOnTripOffineStatus()
	{
		$query = "SELECT ddd.tripId As 'tripId',d.id As 'driverId',d.mobile As 'driverMobile',CONCAT(d.firstName,'',d.lastName) As 'driverName'
				from driverdispatchdetails As ddd
				left join driver as d on ddd.driverId=d.id
				where ddd.availabilityStatus=".Taxi_Available_Status_Enum::BUSY." AND ddd.tripId!=".Status_Type_Enum::INACTIVE."
				AND ddd.updatedDatetime NOT BETWEEN ADDTIME('".getCurrentDateTime()."' ,-300) AND '".getCurrentDateTime()."' 
				AND d.isNotified=".Status_Type_Enum::INACTIVE." AND ddd.status=".Status_Type_Enum::ACTIVE." 
				AND d.loginStatus=".Status_Type_Enum::ACTIVE." AND d.isDeleted=".Status_Type_Enum::INACTIVE."
				ORDER BY ddd.updatedDatetime";
		
		$result = $this->db->query ( $query );
		return $this->fetchAll ( $result );
	
	}
	
	public function getDriverOnFreeOffineStatus()
	{
		$query = "SELECT ddd.tripId As 'tripId',d.id As 'driverId',d.mobile As 'driverMobile',CONCAT(d.firstName,'',d.lastName) As 'driverName'
				from driverdispatchdetails As ddd
				left join driver as d on ddd.driverId=d.id
				where ddd.availabilityStatus=".Taxi_Available_Status_Enum::FREE." AND ddd.tripId=".Status_Type_Enum::INACTIVE."
				AND ddd.updatedDatetime NOT BETWEEN ADDTIME('".getCurrentDateTime()."' ,-200) AND '".getCurrentDateTime()."' 
				AND d.isNotified=".Status_Type_Enum::INACTIVE." AND ddd.status=".Status_Type_Enum::ACTIVE." 
				AND d.loginStatus=".Status_Type_Enum::ACTIVE." AND d.isDeleted=".Status_Type_Enum::INACTIVE."
				ORDER BY ddd.updatedDatetime";
	
		$result = $this->db->query ( $query );
		return $this->fetchAll ( $result );
	
	}
	
	public function getDriverOnFreeStatus()
	{
		$query = "SELECT d.id As 'driverId',d.mobile As 'driverMobile',CONCAT(d.firstName,'',d.lastName) As 'driverName',d.gcmId As 'gcmId'
				from driverdispatchdetails As ddd
				left join driver as d on ddd.driverId=d.id
				where ddd.availabilityStatus=".Taxi_Available_Status_Enum::FREE." AND d.status=".Status_Type_Enum::ACTIVE."
				AND d.loginStatus=".Status_Type_Enum::ACTIVE." AND d.isDeleted=".Status_Type_Enum::INACTIVE." AND d.gcmId!=''
				ORDER BY ddd.updatedDatetime";
	
		$result = $this->db->query ( $query );
		return $this->fetchAll ( $result );
	
	}
	
	public function getDriverDailyWorkReport()
	{
		/* $query = "SELECT d.driverCode As 'driverCode',CONCAT(d.firstName,' ',d.lastName) As 'driverName',d.mobile As 'driverMobile',td.registrationNo as 'taxiRegistrationNo', 
				(FLOOR((SUM(TIMESTAMPDIFF(MINUTE,dsh.createddatetime,dsh.updatedDatetime))/60))) As 'hour',
				(SUM(TIMESTAMPDIFF(MINUTE,dsh.createddatetime,dsh.updatedDatetime))%60) As 'minute' 
				FROM drivershifthistory as dsh 
				left join driver as d on d.id=dsh.driverId 
				left join drivertaximapping as dtm on dtm.driverId=d.id 
				left join taxidetails as td on td.id=dtm.taxiId 
				where dsh.shiftStatus=".Driver_Shift_Status_Enum::SHIFTIN." AND 
				DATE(dsh.createddatetime)='".getCurrentDate()."' group by dsh.driverid
				INTO OUTFILE '/var/lib/mysql-files/driver_report_".getCurrentDate().".csv'
				FIELDS TERMINATED BY ','
				ENCLOSED BY '\"'
				LINES TERMINATED BY '\n'"; */
		/* $query = "SELECT d.driverCode As 'driverCode',CONCAT(d.firstName,' ',d.lastName) As 'driverName',d.mobile As 'driverMobile',td.registrationNo as 'taxiRegistrationNo',
				(FLOOR((SUM(TIMESTAMPDIFF(MINUTE,dsh.createddatetime,dsh.updatedDatetime))/60))) As 'hour',
				(SUM(TIMESTAMPDIFF(MINUTE,dsh.createddatetime,dsh.updatedDatetime))%60) As 'minute'
				FROM drivershifthistory as dsh
				left join driver as d on d.id=dsh.driverId
				left join drivertaximapping as dtm on dtm.driverId=d.id
				left join taxidetails as td on td.id=dtm.taxiId
				where dsh.shiftStatus=".Driver_Shift_Status_Enum::SHIFTIN." AND
				DATE(dsh.createddatetime)='".getCurrentDate()."' group by dsh.driverid"; */
		
		$report_date=getCurrentDate();
		$query = "SELECT d.driverCode As 'driverCode',CONCAT(d.firstName,' ',d.lastName) As 'driverName',d.mobile As 'driverMobile',td.registrationNo as 'taxiRegistrationNo',
		 
		 (CASE WHEN(dsh.availabilityStatus=".Taxi_Available_Status_Enum::FREE." AND dsh.shiftStatus=".Driver_Shift_Status_Enum::SHIFTIN." AND DATE(dsh.createddatetime)=DATE('".$report_date."') AND DATE(dsh.updatedDatetime)=DATE('".$report_date."')) THEN (SUM(TIMESTAMPDIFF(HOUR,dsh.createddatetime,'".getCurrentDateTime()."')%24)) ELSE
		 
		  (CASE WHEN (dsh.availabilityStatus=".Taxi_Available_Status_Enum::BUSY." AND dsh.shiftStatus=".Driver_Shift_Status_Enum::SHIFTIN." AND DATE(dsh.createddatetime)=DATE('".$report_date."') AND DATE(dsh.updatedDatetime)=DATE('".$report_date."')) THEN (SUM(TIMESTAMPDIFF(HOUR,dsh.createddatetime,dsh.updatedDatetime))%24) ELSE
		  (CASE WHEN(dsh.availabilityStatus=".Taxi_Available_Status_Enum::BUSY." AND dsh.shiftStatus=".Driver_Shift_Status_Enum::SHIFTIN." AND DATE(dsh.createddatetime)<=DATE('".$report_date."') AND DATE(dsh.updatedDatetime)>=DATE('".$report_date."')) THEN
		  (CASE WHEN(((TIMESTAMPDIFF(HOUR,dsh.createddatetime,dsh.updatedDatetime)))>24) THEN
		 
		  ((CASE WHEN(DATE(dsh.createdDatetime)<DATE('".$report_date."') AND DATE(dsh.updatedDatetime) >DATE('".$report_date."')) THEN (SUM(TIMESTAMPDIFF(HOUR,CONCAT(DATE('".$report_date."'),' ','00:00:00'),CONCAT(DATE('".$report_date."'),' ','23:59:59')))%24) ELSE
		 
		  (CASE WHEN(DATE(dsh.createdDatetime)=DATE('".$report_date."') AND DATE(dsh.updatedDatetime) >DATE('".$report_date."')) THEN (SUM(TIMESTAMPDIFF(HOUR,dsh.createdDatetime,CONCAT(DATE(dsh.createdDatetime),' ','23:59:59')))%24) ELSE
		 
		  (CASE WHEN(DATE(dsh.updatedDatetime)=DATE('".$report_date."') AND DATE(dsh.createdDatetime) <DATE('".$report_date."')) THEN (SUM(TIMESTAMPDIFF(HOUR,CONCAT(DATE(dsh.updatedDatetime),' ','00:00:00'),dsh.updatedDatetime))%24)
		 
		  END)END)END)) ELSE
		 
		  ((CASE WHEN(DATE(dsh.createdDatetime)=DATE('".$report_date."') AND DATE(dsh.updatedDatetime) >DATE('".$report_date."')) THEN (SUM(TIMESTAMPDIFF(HOUR,dsh.createdDatetime,CONCAT(DATE(dsh.createdDatetime),' ','23:59:59')))%24) ELSE
		  (CASE WHEN(DATE(dsh.updatedDatetime)=DATE('".$report_date."') AND DATE(dsh.createdDatetime) <DATE('".$report_date."')) THEN (SUM(TIMESTAMPDIFF(HOUR,CONCAT(DATE(dsh.updatedDatetime),' ','00:00:00'),dsh.updatedDatetime))%24) ELSE '24'
		  END)END))
		 
		  END)END)END)END) AS 'hour',
		 
		  (CASE WHEN(dsh.availabilityStatus=".Taxi_Available_Status_Enum::FREE." AND dsh.shiftStatus=".Driver_Shift_Status_Enum::SHIFTIN." AND DATE(dsh.createddatetime)=DATE('".$report_date."') AND DATE(dsh.updatedDatetime)=DATE('".$report_date."')) THEN (SUM(TIMESTAMPDIFF(HOUR,dsh.createddatetime,'".getCurrentDateTime()."')%24)) ELSE
		 
		  (CASE WHEN (dsh.availabilityStatus=".Taxi_Available_Status_Enum::BUSY." AND dsh.shiftStatus=".Driver_Shift_Status_Enum::SHIFTIN." AND DATE(dsh.createddatetime)=DATE('".$report_date."') AND DATE(dsh.updatedDatetime)=DATE('".$report_date."')) THEN (SUM(TIMESTAMPDIFF(MINUTE,dsh.createddatetime,dsh.updatedDatetime))%60) ELSE
		  (CASE WHEN(dsh.availabilityStatus=".Taxi_Available_Status_Enum::BUSY." AND dsh.shiftStatus=".Driver_Shift_Status_Enum::SHIFTIN." AND DATE(dsh.createddatetime)<=DATE('".$report_date."') AND DATE(dsh.updatedDatetime)>=DATE('".$report_date."')) THEN
		  (CASE WHEN(((TIMESTAMPDIFF(HOUR,dsh.createddatetime,dsh.updatedDatetime)))>24) THEN
		 
		  ((CASE WHEN(DATE(dsh.createdDatetime)<DATE('".$report_date."') AND DATE(dsh.updatedDatetime) >DATE('".$report_date."')) THEN (SUM(TIMESTAMPDIFF(MINUTE,CONCAT(DATE('".$report_date."'),' ','00:00:00'),CONCAT(DATE('".$report_date."'),' ','23:59:59')))%60) ELSE
		 
		  (CASE WHEN(DATE(dsh.createdDatetime)=DATE('".$report_date."') AND DATE(dsh.updatedDatetime) >DATE('".$report_date."')) THEN (SUM(TIMESTAMPDIFF(MINUTE,dsh.createdDatetime,CONCAT(DATE(dsh.createdDatetime),' ','23:59:59')))%60) ELSE
		 
		  (CASE WHEN(DATE(dsh.updatedDatetime)=DATE('".$report_date."') AND DATE(dsh.createdDatetime) <DATE('".$report_date."')) THEN (SUM(TIMESTAMPDIFF(MINUTE,CONCAT(DATE(dsh.updatedDatetime),' ','00:00:00'),dsh.updatedDatetime))%60)
		 
		  END)END)END)) ELSE
		 
		  ((CASE WHEN(DATE(dsh.createdDatetime)=DATE('".$report_date."') AND DATE(dsh.updatedDatetime) >DATE('".$report_date."')) THEN (SUM(TIMESTAMPDIFF(MINUTE,dsh.createdDatetime,CONCAT(DATE(dsh.createdDatetime),' ','23:59:59')))%60) ELSE
		  (CASE WHEN(DATE(dsh.updatedDatetime)=DATE('".$report_date."') AND DATE(dsh.createdDatetime) <DATE('".$report_date."')) THEN (SUM(TIMESTAMPDIFF(MINUTE,CONCAT(DATE(dsh.updatedDatetime),' ','00:00:00'),dsh.updatedDatetime))%60) ELSE '0'
		  END)END))
		 
		  END)END)END)END) AS 'minute'
		 
		 
		  FROM drivershifthistory as dsh
		  right join driver as d On d.id=dsh.driverId
		  left join drivertaximapping as dtm on dtm.driverId=d.id
		  left join taxidetails as td on td.id=dtm.taxiId
		  where DATE(dsh.createddatetime)='".$report_date."' OR DATE(dsh.updatedDatetime)='".$report_date."' OR (DATE(dsh.createdDatetime)<DATE('".$report_date."') AND DATE(dsh.updatedDatetime) >DATE('".$report_date."')) group by dsh.driverid";
		
		
		$result = $this->db->query ( $query );
		return $this->fetchAll ( $result );
		//return $result->result();
	}
	public function getDataAnalytics()
	{
		$query = "SELECT td.id As tripId,td.passengerId as passengerId,td.driverId as driverId,td.taxiId As taxiId,td.jobCardId As jobCardId,td.zoneId As zoneId,td.dropZoneId As dropZoneId,td.entityId As entityId,td.estimatedDistance As estimatedDistance,td.estimatedTime As estimatedTime,td.pickupLocation As pickupLocation,td.pickupLatitude As pickupLatitude,td.pickupLongitude As pickupLongitude,td.pickupDatetime As pickupDatetime,td.dispatchedDatetime As dispatchedDatetime,td.driverAcceptedDatetime As driverAcceptedDatetime,td.taxiArrivedDatetime As taxiArrivedDatetime,td.actualPickupDatetime As actualPickupDatetime,td.estimatedPickupDistance As estimatedPickupDistance,td.estimatedPickupMinute As estimatedPickupMinute,td.dropLocation As dropLocation,td.dropLatitude As dropLatitude,td.dropLongitude As dropLongitude,td.actualDropLocation As actualDropLocation,td.actualDropLatitude As actualDropLatitude,td.actualDropLongitude As actualDropLongitude,td.dropDatetime As dropDatetime,td.tripStatus As tripStatus,td.driverAcceptedStatus As driverAcceptedStatus,td.taxiCategoryType As taxiCategoryType,td.tripType As tripType,td.paymentMode As paymentMode,td.bookedFrom As bookedFrom,td.driverRating As driverRating,td.driverComments As driverComments,td.passengerRating As passengerRating,td.passengerComments As passengerComments,td.passengerRejectComments As passengerRejectComments,td.landmark As landmark,td.promoCode As promoCode,td.createdBy As createdBy,td.updatedBy As updatedBy,td.createdDatetime As createdDatetime,td.updatedDatetime As updatedDatetime,
				ttd.travelledDistance As travelledDistance,ttd.actualTravelledDistance As actualTravelledDistance,ttd.travelledPeriod As travelledPeriod,ttd.convenienceCharge As convenienceCharge,ttd.travelCharge As travelCharge,ttd.parkingCharge As parkingCharge,ttd.tollCharge As tollCharge,ttd.surgeCharge As surgeCharge,ttd.penaltyCharge As penaltyCharge,ttd.bookingCharge As bookingCharge,ttd.taxCharge As taxCharge,ttd.totalTripCharge As totalTripCharge,ttd.promoDiscountAmount As promoDiscountAmount,ttd.walletPaymentAmount As walletPaymentAmount,ttd.adminAmount As adminAmount,ttd.taxPercentage As taxPercentage,ttd.paymentStatus As paymentStatus,ttd.transactionId As transactionId,ttd.invoiceNo As invoiceNo,ttd.driverEarning As driverEarning,
				pcd.promoCodeLimit As promoCodeLimit,pcd.promoEndDatetime As promoEndDatetime,pcd.promoStartDatetime As promoStartDatetime,pcd.promoCodeUserLimit As promoCodeUserLimit,pcd.zoneId As promoZoneId,pcd.zoneId2 As promoZoneId2,pcd.entityId As promoEntityId,pcd.promoDiscountAmount As promoDiscountPercentage,pcd.promoDiscountAmount2 As promoDiscountValue,
				txd.registrationNo As taxiRegistrationNo,
				CONCAT(p.firstName,' ',p.lastName) As passengerName,p.mobile As passengerMobile,p.email As passengerEmail,p.gender As passengerGender,p.dob As passengerDob,
				CONCAT(d.firstName,' ',d.lastName) As driverName,d.mobile As driverMobile,d.email As driverEmail,dpd.gender As driverGender,dpd.dob As driverDob,dpd.address As driverAddress,d.driverWallet As driverWallet,d.driverCredit As driverCredit,d.experience As driverExperience,
				trd.rejectedDriverList As rejectedDriverList,trd.totalAvailableDriver As totalDriversFoundOnDispatch
				from tripdetails As td
				left join triptransactiondetails as ttd ON ttd.tripId=td.id
				left join passenger as p ON p.id=td.passengerId
				left join driver as d ON d.id=td.driverId
				left join driverpersonaldetails as dpd ON dpd.driverId=d.id
				left join taxidetails as txd ON txd.id=td.taxiId
				left join promocodedetails as pcd ON pcd.promoCode=td.promoCode
				left join taxirequestdetails as trd ON trd.tripId=td.id
				where td.createdDatetime BETWEEN SUBDATE('".getCurrentDateTime()."' ,INTERVAL 24 HOUR) AND '".getCurrentDateTime()."'";
		$result = $this->db->query ( $query );
		return $this->fetchAll ( $result );
	
	}
}